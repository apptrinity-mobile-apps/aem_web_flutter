importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-auth.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-storage.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-firestore.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js");

  // staging
  const firebaseConfig = {
    apiKey: "AIzaSyBZNxU1YbIKFChXnkn4GGxBYjFlzvcxCKY",
    authDomain: "brio-one-chat-108b6.firebaseapp.com",
    projectId: "brio-one-chat-108b6",
    storageBucket: "brio-one-chat-108b6.appspot.com",
    messagingSenderId: "983226336452",
    appId: "1:983226336452:web:84448cabe07561628f7591"
  };

  // live
  /*const firebaseConfig = {
    apiKey: "AIzaSyAK_WgJrvPifVFqZNsjiL1u5F3BFkhOx2E",
    authDomain: "brio-one-notifications.firebaseapp.com",
    projectId: "brio-one-notifications",
    storageBucket: "brio-one-notifications.appspot.com",
    messagingSenderId: "616334268353",
    appId: "1:616334268353:web:b6f7bbaa605db23f644140",
    measurementId: "G-B7Y4DT4D23"
  };*/
  firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();

  /*messaging.onMessage((payload) => {
  console.log('Message received. ', payload);*/
  messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
      notificationOptions);
  });