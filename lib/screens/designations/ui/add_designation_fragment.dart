import 'package:aem/screens/designations/view_model/designation_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddDesignationFragment extends StatefulWidget {
  final String? designationId;

  const AddDesignationFragment({this.designationId, Key? key}) : super(key: key);

  @override
  _AddDesignationFragmentState createState() => _AddDesignationFragmentState();
}

class _AddDesignationFragmentState extends State<AddDesignationFragment> {
  DateTime selectedDate = DateTime.now();
  TextEditingController dateController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late DesignationNotifier viewModel;
  String employeeId = "", designationId = "";

  @override
  void initState() {
    viewModel = Provider.of<DesignationNotifier>(context, listen: false);

    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    if (widget.designationId != null) {
      getDesignation();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.designationHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuDesignations", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: dashBoardCardBorderTile, width: 1),
                                  color: addNewContainerBg,
                                ),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        addDesignationName(),
                                        // designationCreateField(),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.designationId == null ? addDesignation : updateDesignation, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddDesignation, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            if (widget.designationId != null) {
                              await viewModel.updateDesignationAPI(designationId, nameController.text.trim()).then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            } else {
                              await viewModel.addDesignationAPI(nameController.text.trim(), employeeId).then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget addDesignationName() {
    return Form(
      key: _formKey,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(designationName, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: nameController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.text,
                        inputFormatters: [UpperCaseTextFormatter()],
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: false,
                        decoration: editTextDesignationNameDecoration,
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget designationCreateField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(date, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                      maxLines: 1,
                      controller: dateController,
                      textInputAction: TextInputAction.done,
                      obscureText: false,
                      decoration: editTextDateDecoration,
                      style: textFieldStyle,
                      onTap: () {
                        showCalendar();
                      },
                      enableInteractiveSelection: false,
                      focusNode: FocusNode(),
                      readOnly: true),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> showCalendar() async {
    final DateTime? picked = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(2100));
    if (picked != null) {
      setState(() {
        selectedDate = picked;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputDate = inputFormat.parse(selectedDate.toLocal().toString().split(' ')[0]); // removing time from date
        var outputFormat = DateFormat('dd/MM/yyyy');
        var outputDate = outputFormat.format(inputDate);
        dateController.text = outputDate;
      });
    }
  }

  void getDesignation() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewDesignationAPI(widget.designationId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              designationId = value.designation!.id!;
              nameController.text = value.designation!.name!;
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
