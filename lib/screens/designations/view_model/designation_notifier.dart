import 'package:aem/model/all_designations_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DesignationNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<AllDesignationsResponse?> viewAllDesignationsAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllDesignationsResponse? _allDesignationsResponse = AllDesignationsResponse();

    try {
      dynamic response = await Repository().viewAllDesignations(employeeId);
      if (response != null) {
        _allDesignationsResponse = AllDesignationsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allDesignationsResponse");
    notifyListeners();
    return _allDesignationsResponse;
  }

  Future<AllDesignationsResponse?> getDesignationsAPI() async {
    _isFetching = true;
    _isHavingData = false;
    AllDesignationsResponse? _allDesignationsResponse = AllDesignationsResponse();

    try {
      dynamic response = await Repository().getDesignations();
      if (response != null) {
        _allDesignationsResponse = AllDesignationsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allDesignationsResponse");
    notifyListeners();
    return _allDesignationsResponse;
  }

  Future<BaseResponse?> addDesignationAPI(String designationName, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createDesignation(designationName, employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateDesignationAPI(String designationId, String designationName) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateDesignation(designationId, designationName);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteDesignationAPI(String designationId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteDesignation(designationId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<AllDesignationsResponse?> viewDesignationAPI(String designationId) async {
    _isFetching = true;
    _isHavingData = false;
    AllDesignationsResponse? __allDesignationsResponse = AllDesignationsResponse();

    try {
      dynamic response = await Repository().viewDesignation(designationId);
      if (response != null) {
        __allDesignationsResponse = AllDesignationsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $__allDesignationsResponse");
    notifyListeners();
    return __allDesignationsResponse;
  }
}
