import 'package:aem/model/all_roles_response_model.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/roles/ui/add_roles_fragment.dart';
import 'package:aem/screens/roles/view_model/roles_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RolesFragment extends StatefulWidget {
  const RolesFragment({Key? key}) : super(key: key);

  @override
  _RolesFragmentState createState() => _RolesFragmentState();
}

class _RolesFragmentState extends State<RolesFragment> {
  late RolesNotifier viewModel;
  String employeeId = "";
  bool editPermission = false, addPermission = false;

  @override
  void initState() {
    viewModel = Provider.of<RolesNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.roleEditPermission;
    addPermission = userViewModel.roleAddPermission;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("$home > $navMenuRoles", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(child: Padding(padding: const EdgeInsets.fromLTRB(0, 15, 0, 10), child: allRolesList())),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuRoles, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionRole, softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: buttonDecorationGreen,
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: TextButton(
                    onPressed: () {
                      if (addPermission) {
                        Navigator.push(
                                context,
                                PageRouteBuilder(
                                    pageBuilder: (context, animation1, animation2) => const AddRolesFragment(),
                                    transitionDuration: const Duration(seconds: 0),
                                    reverseTransitionDuration: const Duration(seconds: 0)))
                            .then((value) {
                          // set state to reload data , reloads data in futureBuilder
                          setState(() {});
                        });
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(addRole, style: newButtonsStyle),
                      ),
                    ),
                  ),
                ),
                /*InkWell(
                  onTap: () {},
                  child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                )*/
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget allRolesList() {
    return FutureBuilder(
        future: viewModel.getRolesAPI(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loader());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(child: Text('error ${snapshot.error}', style: logoutHeader));
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllRolesResponse;
              if (data.responseStatus == 1) {
                if (data.rolesDetails!.isNotEmpty) {
                  return Column(
                    children: [
                      listHeader(),
                      Flexible(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            controller: ScrollController(),
                            itemCount: data.rolesDetails!.length,
                            itemBuilder: (context, index) {
                              return listItems(data.rolesDetails![index], index);
                            }),
                      )
                    ],
                  );
                } else {
                  return Center(child: Text(noDataAvailable, style: logoutHeader));
                }
              } else {
                return Center(child: Text(data.result!, style: logoutHeader));
              }
            } else {
              return Center(child: Text(pleaseTryAgain, style: logoutHeader));
            }
          } else {
            return Center(child: loader());
          }
        });
  }

  Widget listHeader() {
    return Container(
      decoration: buttonTopCircularDecorationGreen,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(roleName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(action, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
            ),
          ),
        ],
      ),
    );
  }

  Widget listItems(RolesDetails itemData, int index) {
    var alternate = index % 2;
    return Container(
      color: alternate == 0 ? listItemColor : listItemColor1,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 25, 5),
              child: Text(itemData.role!.inCaps, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 10, 5),
              child: Wrap(
                // mainAxisAlignment: MainAxisAlignment.end,
                alignment: WrapAlignment.end,
                crossAxisAlignment: WrapCrossAlignment.end,
                runAlignment: WrapAlignment.end,
                runSpacing: 4,
                spacing: 4,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: edit,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/edit_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (editPermission) {
                                Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            pageBuilder: (context, animation1, animation2) => AddRolesFragment(roleId: itemData.id!),
                                            transitionDuration: const Duration(seconds: 0),
                                            reverseTransitionDuration: const Duration(seconds: 0)))
                                    .then((value) {
                                  // set state to reload data , reloads data in futureBuilder
                                  setState(() {});
                                });
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: delete,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/delete_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (editPermission) {
                                showDeleteDialog(itemData);
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  showDeleteDialog(RolesDetails itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.role} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: '$role?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteRoleAPI(itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // to reload data
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
}
