import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/roles/view_model/roles_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/model/check_box_state.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddRolesFragment extends StatefulWidget {
  final String? roleId;

  const AddRolesFragment({this.roleId, Key? key}) : super(key: key);

  @override
  _AddRolesFragmentState createState() => _AddRolesFragmentState();
}

class _AddRolesFragmentState extends State<AddRolesFragment> {
  List<CheckBoxState> dashboardPermissions = [],
      sprintPermissions = [],
      scrumPermissions = [],
      stagesPermissions = [],
      employeePermissions = [],
      projectPermissions = [],
      boardingPermissions = [],
      designationPermissions = [],
      technologyPermissions = [],
      rolePermissions = [],
      interviewTypesPermissions = [],
      profileStatusPermissions = [],
      interviewStatusPermissions = [],
      jobRolePermissions = [],
      profilesPermissions = [],
      taskPermissions = [],
      bugPermissions = [],
      modulePermissions = [];
  TextEditingController nameController = TextEditingController();
  final List<String> _dashboardPermissions = [],
      _sprintPermissions = [],
      _scrumPermissions = [],
      _stagesPermissions = [],
      _employeePermissions = [],
      _projectPermissions = [],
      _boardingPermissions = [],
      _designationPermissions = [],
      _technologyPermissions = [],
      _rolePermissions = [],
      _interviewTypesPermissions = [],
      _jobRolePermissions = [],
      _profileStatusPermissions = [],
      _interviewStatusPermissions = [],
      _profilesPermissions = [],
      _taskPermissions = [],
      _bugPermissions = [],
      _modulePermissions = [];

  final _formKey = GlobalKey<FormState>();
  late RolesNotifier viewModel;
  String employeeId = "", roleId = "";
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    viewModel = Provider.of<RolesNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    dashboardPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    dashboardPermissions.add(CheckBoxState(displayTitle: fullAccess, displayValue: fullAccess, value: false));

    sprintPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    sprintPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    stagesPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    stagesPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    stagesPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    scrumPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    scrumPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    scrumPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));
    scrumPermissions.add(CheckBoxState(displayTitle: addTask, displayValue: addTask, value: false));
    scrumPermissions.add(CheckBoxState(displayTitle: fullAccess, displayValue: fullAccess, value: false));

    employeePermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    employeePermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    employeePermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));
    employeePermissions.add(CheckBoxState(displayTitle: project, displayValue: project, value: false));
    employeePermissions.add(CheckBoxState(displayTitle: hr, displayValue: hr, value: false));

    projectPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    projectPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    projectPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    boardingPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    boardingPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    boardingPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    designationPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    designationPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    designationPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    technologyPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    technologyPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    technologyPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    rolePermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    rolePermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    rolePermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    taskPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    taskPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    taskPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));
    taskPermissions.add(CheckBoxState(displayTitle: estimation, displayValue: estimation, value: false));

    bugPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    bugPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    modulePermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    modulePermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    modulePermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    interviewTypesPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    interviewTypesPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    interviewTypesPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    profileStatusPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    profileStatusPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    profileStatusPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    interviewStatusPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    interviewStatusPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    interviewStatusPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    jobRolePermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    jobRolePermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    jobRolePermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));

    profilesPermissions.add(CheckBoxState(displayTitle: add, displayValue: add, value: false));
    profilesPermissions.add(CheckBoxState(displayTitle: view, displayValue: view, value: false));
    profilesPermissions.add(CheckBoxState(displayTitle: edit, displayValue: edit, value: false));
    profilesPermissions.add(CheckBoxState(displayTitle: assign, displayValue: assign, value: false));
    profilesPermissions.add(CheckBoxState(displayTitle: fullAccess, displayValue: fullAccess, value: false));
    profilesPermissions.add(CheckBoxState(displayTitle: updateStatus, displayValue: updateStatus, value: false));

    if (widget.roleId != null) {
      getRole();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.roleHomeRoute}-innerpage",
                  ),
                  Flexible(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuRoles", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: cardBorder,
                                child: Scrollbar(
                                  isAlwaysShown: true,
                                  controller: scrollController,
                                  child: SingleChildScrollView(
                                    controller: scrollController,
                                    child: Container(
                                      padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          addRoleName(),
                                          FittedBox(
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(8, 30, 8, 10),
                                              child: SizedBox(
                                                width: ScreenConfig.width(context) / 2,
                                                child: Text(permissions, style: addRoleSubBoldStyle),
                                              ),
                                            ),
                                          ),
                                          setPermissions()
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.roleId == null ? addRole : updateRole, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddRole, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            for (var element in sprintPermissions) {
                              if (element.value!) {
                                _sprintPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in stagesPermissions) {
                              if (element.value!) {
                                _stagesPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }

                            for (var element in scrumPermissions) {
                              if (element.value!) {
                                _scrumPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in employeePermissions) {
                              if (element.value!) {
                                _employeePermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in projectPermissions) {
                              if (element.value!) {
                                _projectPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in boardingPermissions) {
                              if (element.value!) {
                                _boardingPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in designationPermissions) {
                              if (element.value!) {
                                _designationPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in technologyPermissions) {
                              if (element.value!) {
                                _technologyPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in rolePermissions) {
                              if (element.value!) {
                                _rolePermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in profilesPermissions) {
                              if (element.value!) {
                                _profilesPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in interviewTypesPermissions) {
                              if (element.value!) {
                                _interviewTypesPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in profileStatusPermissions) {
                              if (element.value!) {
                                _profileStatusPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in interviewStatusPermissions) {
                              if (element.value!) {
                                _interviewStatusPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in jobRolePermissions) {
                              if (element.value!) {
                                _jobRolePermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in taskPermissions) {
                              if (element.value!) {
                                _taskPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in bugPermissions) {
                              if (element.value!) {
                                _bugPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in modulePermissions) {
                              if (element.value!) {
                                _modulePermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            for (var element in dashboardPermissions) {
                              if (element.value!) {
                                _dashboardPermissions.add(element.displayTitle!.toLowerCase());
                              }
                            }
                            if (widget.roleId != null) {
                              await viewModel
                                  .updateRoleAPI(
                                      roleId,
                                      nameController.text.trim(),
                                      _dashboardPermissions,
                                      _sprintPermissions,
                                      _scrumPermissions,
                                      _employeePermissions,
                                      _projectPermissions,
                                      _boardingPermissions,
                                      _designationPermissions,
                                      _technologyPermissions,
                                      _rolePermissions,
                                      _interviewTypesPermissions,
                                      _profileStatusPermissions,
                                      _interviewStatusPermissions,
                                      _jobRolePermissions,
                                      _profilesPermissions,
                                      _taskPermissions,
                                      _bugPermissions,
                                      _modulePermissions,
                                      _stagesPermissions)
                                  .then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            } else {
                              await viewModel
                                  .addRoleAPI(
                                      employeeId,
                                      nameController.text.trim(),
                                      _dashboardPermissions,
                                      _sprintPermissions,
                                      _scrumPermissions,
                                      _employeePermissions,
                                      _projectPermissions,
                                      _boardingPermissions,
                                      _designationPermissions,
                                      _technologyPermissions,
                                      _rolePermissions,
                                      _interviewTypesPermissions,
                                      _profileStatusPermissions,
                                      _interviewStatusPermissions,
                                      _jobRolePermissions,
                                      _profilesPermissions,
                                      _taskPermissions,
                                      _bugPermissions,
                                      _modulePermissions,
                                      _stagesPermissions)
                                  .then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget addRoleName() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Form(
        key: _formKey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(roleName, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                      maxLines: 1,
                      controller: nameController,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                      inputFormatters: [UpperCaseTextFormatter()],
                      textCapitalization: TextCapitalization.sentences,
                      obscureText: false,
                      decoration: editTextRoleNameDecoration,
                      style: textFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget setPermissions() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
              child: SizedBox(
                width: ScreenConfig.width(context) / 2,
                child: Text(navMenuMainProjectMgmt, style: addRoleSubBoldStyle),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuDashboard, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: dashboardPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(dashboardPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuSprint, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: sprintPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(sprintPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuScrum, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: scrumPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(scrumPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuEmployees, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: employeePermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(employeePermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuProjects, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: projectPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(projectPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuBoarding, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: boardingPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(boardingPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuDesignations, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: designationPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(designationPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuTechnologies, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: technologyPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(technologyPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuRoles, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: rolePermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(rolePermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(tasks, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: taskPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(taskPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(bugs, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: bugPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(bugPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(modules, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  width: 400,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: modulePermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(modulePermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuStages, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: stagesPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(stagesPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 20, 8, 10),
              child: SizedBox(
                width: ScreenConfig.width(context) / 2,
                child: Text(navMenuMainHRMgmt, style: addRoleSubBoldStyle),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuProfiles, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: profilesPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(profilesPermissions[index]);
                    },
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuProfileStatus, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: profileStatusPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(profileStatusPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuInterviewTypes, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: interviewTypesPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(interviewTypesPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuInterviewerStatus, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: interviewStatusPermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(interviewStatusPermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(navMenuJobRole, textAlign: TextAlign.start, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: SizedBox(
                  height: 50,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemCount: jobRolePermissions.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return checkBox(jobRolePermissions[index]);
                    },
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget checkBox(CheckBoxState checkBox) {
    return InkWell(
      onTap: () {
        setState(() {
          checkBox.value = !checkBox.value!;
        });
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Checkbox(
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: checkBox.value!,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) {
                setState(() {
                  checkBox.value = value!;
                });
              }),
          Padding(
            padding: const EdgeInsets.fromLTRB(4, 0, 16, 0),
            child: Text(
              checkBox.displayTitle!,
              textAlign: TextAlign.start,
              style: addRoleSubStyle,
            ),
          )
        ],
      ),
    );
  }

  void getRole() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewRoleAPI(widget.roleId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              roleId = value.roleData!.id!;
              nameController.text = value.roleData!.role!;
              for (var element in value.roleData!.sprintPermissions!) {
                for (var item in sprintPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.stagesPermissions!) {
                for (var item in stagesPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.scrumPermissions!) {
                for (var item in scrumPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.employeePermissions!) {
                for (var item in employeePermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.projectPermissions!) {
                for (var item in projectPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.boardingPermissions!) {
                for (var item in boardingPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.designationPermissions!) {
                for (var item in designationPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.technologyPermissions!) {
                for (var item in technologyPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.rolePermissions!) {
                for (var item in rolePermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.interviewTypesPermissions!) {
                for (var item in interviewTypesPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.profileStatusPermissions!) {
                for (var item in profileStatusPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.interviewStatusPermissions!) {
                for (var item in interviewStatusPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.profilePermissions!) {
                for (var item in profilesPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.taskPermissions!) {
                for (var item in taskPermissions) {
                  print("---" + element.toLowerCase());
                  print("---" + item.displayValue!.toLowerCase());
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.bugPermissions!) {
                for (var item in bugPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.modulePermissions!) {
                for (var item in modulePermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
              for (var element in value.roleData!.dashboardPermissions!) {
                for (var item in dashboardPermissions) {
                  if (element.toLowerCase() == item.displayValue!.toLowerCase()) {
                    item.value = true;
                  }
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
