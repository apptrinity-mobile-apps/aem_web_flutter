import 'package:aem/model/all_roles_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RolesNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<AllRolesResponse?> viewAllRolesAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllRolesResponse? _allRolesResponse = AllRolesResponse();

    try {
      dynamic response = await Repository().viewAllRoles(employeeId);
      if (response != null) {
        _allRolesResponse = AllRolesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allRolesResponse");
    notifyListeners();
    return _allRolesResponse;
  }

  Future<AllRolesResponse?> getRolesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    AllRolesResponse? _allRolesResponse = AllRolesResponse();

    try {
      dynamic response = await Repository().getRoles();
      if (response != null) {
        _allRolesResponse = AllRolesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allRolesResponse");
    notifyListeners();
    return _allRolesResponse;
  }

  Future<BaseResponse?> addRoleAPI(
      String employeeId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,


      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions,) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createRole(
          employeeId,
          roleName,
          dashboardPermissions,
          sprintPermissions,
          scrumPermissions,
          employeePermissions,
          projectPermissions,
          boardingPermissions,
          designationPermissions,
          technologyPermissions,
          rolePermissions,
          interviewTypesPermissions,
          profileStatusPermissions,
          interviewStatusPermissions,
          jobRolePermissions,
          profilePermissions,

          taskPermissions,
          bugPermissions,
          modulePermissions,
          stagespermissions);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateRoleAPI(
      String roleId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,

      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateRole(
          roleId,
          roleName,
          dashboardPermissions,
          sprintPermissions,
          scrumPermissions,
          employeePermissions,
          projectPermissions,
          boardingPermissions,
          designationPermissions,
          technologyPermissions,
          rolePermissions,
          interviewTypesPermissions,
          profileStatusPermissions,
          interviewStatusPermissions,
          jobRolePermissions,
          profilePermissions,

          taskPermissions,
          bugPermissions,
          modulePermissions,
          stagespermissions);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteRoleAPI(String roleId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteRole(roleId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<AllRolesResponse?> viewRoleAPI(String roleId) async {
    _isFetching = true;
    _isHavingData = false;
    AllRolesResponse? _allRolesResponse = AllRolesResponse();

    try {
      dynamic response = await Repository().viewRole(roleId);
      if (response != null) {
        _allRolesResponse = AllRolesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allRolesResponse");
    notifyListeners();
    return _allRolesResponse;
  }
}
