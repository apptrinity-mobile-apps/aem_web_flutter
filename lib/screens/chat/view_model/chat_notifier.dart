import 'package:aem/model/employee_associated_chats_response.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class ChatNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  EmployeeAssociatedChatsResponse? _employeeAssociatedChatsResponse;

  List<AssociatedEmployeeDetails>? get getAssociatedEmployeesList {
    List<AssociatedEmployeeDetails>? list = [];
    if (_employeeAssociatedChatsResponse != null) {
      if (_employeeAssociatedChatsResponse!.responseStatus == 1) {
        if (_employeeAssociatedChatsResponse!.employeeDetails!.isNotEmpty) {
          list = _employeeAssociatedChatsResponse!.employeeDetails;
        }
      }
    }
    return list;
  }

  Future<EmployeeAssociatedChatsResponse?> getEmployeeAssociatedChatsAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    _employeeAssociatedChatsResponse = EmployeeAssociatedChatsResponse();

    try {
      dynamic response = await Repository().getEmployeeAssociatedChats(employeeId);
      if (response != null) {
        _employeeAssociatedChatsResponse = EmployeeAssociatedChatsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeeAssociatedChatsResponse");
    notifyListeners();
    return _employeeAssociatedChatsResponse;
  }
}
