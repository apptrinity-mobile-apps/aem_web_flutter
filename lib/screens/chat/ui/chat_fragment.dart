import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:http/http.dart' as http;
import 'package:aem/screens/chat/view_model/chat_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/chat/firebase_chat_core.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/loader.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  ScrollController chatListController = ScrollController();
  String _employeeId = "";
  bool _isChatListLoaded = false;
  int _selectedIndex = -1;
  types.Room? _room;
  bool _isAttachmentUploading = false;

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    _employeeId = userViewModel.employeeId!;
    getData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Row(
        children: [chatBody(), chatsList()],
      ),
    );
  }

  Widget chatBody() {
    var vModel = Provider.of<ChatNotifier>(context, listen: true);
    return Expanded(
      flex: 3,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Container(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(5),
          child: /*_selectedIndex == -1*/_room == null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Choose a conversation',
                      style: fragmentHeaderStyle.copyWith(letterSpacing: 0.5, fontWeight: FontWeight.w600, fontSize: 18),
                    ),
                    Text(
                      'Click on an existing chat to continue a conversation or to create a new conversation',
                      style: fragmentHeaderStyle.copyWith(letterSpacing: 0.5, fontWeight: FontWeight.w400, fontSize: 14),
                    ),
                  ],
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      vModel.getAssociatedEmployeesList![_selectedIndex].name!,
                      style: fragmentHeaderStyle,
                    ),
                    Expanded(
                      child: StreamBuilder<types.Room>(
                        initialData: _room,
                        stream: FirebaseChatCore.instance.room(_room!.id),
                        builder: (context, snapshot) => StreamBuilder<List<types.Message>>(
                          initialData: const [],
                          stream: FirebaseChatCore.instance.messages(snapshot.data!),
                          builder: (context, snapshot) => Chat(
                            isAttachmentUploading: _isAttachmentUploading,
                            messages: snapshot.data ?? [],
                            // onAttachmentPressed: _handleAttachmentPressed,
                            onMessageTap: _handleMessageTap,
                            onPreviewDataFetched: _handlePreviewDataFetched,
                            onSendPressed: _handleSendPressed,
                            user: types.User(
                              id: FirebaseChatCore.instance.firebaseUser?.uid ?? '',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  Widget chatsList() {
    var vModel = Provider.of<ChatNotifier>(context, listen: true);
    return Expanded(
      flex: 1,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Container(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "${navMenuChat}s",
                style: fragmentHeaderStyle,
              ),
              _isChatListLoaded
                  ? vModel.getAssociatedEmployeesList!.isNotEmpty
                      ? Flexible(
                          child: ListView.separated(
                            controller: chatListController,
                            shrinkWrap: true,
                            itemCount: vModel.getAssociatedEmployeesList!.length,
                            padding: const EdgeInsets.only(right: 5),
                            itemBuilder: (context, index) {
                              return Card(
                                elevation: 2,
                                margin: const EdgeInsets.only(top: 2, bottom: 2),
                                color: _selectedIndex == index ? newCompleted : Colors.white,
                                child: InkWell(
                                  onTap: () async {
                                      FirebaseChatCore.instance.getUserDetails(vModel.getAssociatedEmployeesList![index].chatId!).then((value) async {
                                          debugPrint(value.toString());
                                          final createRoom = await FirebaseChatCore.instance.createRoom(value);
                                          debugPrint(createRoom.toString());
                                          setState(() {
                                            _selectedIndex = index;
                                            _room = createRoom;
                                          });
                                      });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(bottom: 0),
                                          child: Text(
                                            vModel.getAssociatedEmployeesList![index].name!,
                                            style: loginInfoStyle.copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: _selectedIndex == index ? Colors.white : editText,
                                              fontSize: 14,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 0),
                                          child: Text(
                                            vModel.getAssociatedEmployeesList![index].email!,
                                            style: loginInfoStyle.copyWith(
                                              fontWeight: FontWeight.w400,
                                              color: _selectedIndex == index ? Colors.white : editText,
                                              fontSize: 13,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (context, index) {
                              return const Divider(
                                color: Colors.transparent,
                                height: 2,
                              );
                            },
                          ),
                        )
                      : Expanded(
                          child: Center(
                            child: Text(
                              'No Chats Available',
                              style: fragmentHeaderStyle.copyWith(letterSpacing: 0.5, fontWeight: FontWeight.w600, fontSize: 16),
                            ),
                          ),
                        )
                  : loader(),
            ],
          ),
        ),
      ),
    );
  }

  void _handleAttachmentPressed() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) => SafeArea(
        child: SizedBox(
          height: 144,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  _handleImageSelection();
                },
                child: const Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Photo'),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  _handleFileSelection();
                },
                child: const Align(
                  alignment: Alignment.centerLeft,
                  child: Text('File'),
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Cancel'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _handleFileSelection() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.any,
    );

    if (result != null) {
      _setAttachmentUploading(true);
      final filePath = result.files.single.bytes!;
      try {
        final message = types.PartialFile(
          mimeType: result.files.first.extension.toString(),
          name: result.files.single.name,
          size: result.files.single.size,
          uri: Uri.dataFromBytes(filePath).toString(),
        );
        FirebaseChatCore.instance.sendMessage(message, _room!.id);
        _setAttachmentUploading(false);
      } catch (exception) {
        print("file exception: " + exception.toString());
      } finally {
        _setAttachmentUploading(false);
      }
    }
  }

  void _handleImageSelection() async {
    final result = await ImagePicker().pickImage(
      imageQuality: 70,
      maxWidth: 1440,
      source: ImageSource.gallery,
    );
    if (result != null) {
      _setAttachmentUploading(true);
      final bytes = await result.readAsBytes();
      final image = await decodeImageFromList(bytes);
      final name = result.name;
      try {
        final reference = FirebaseStorage.instance.ref(name);
        await reference.putFile(File.fromRawPath(bytes));
        final uri = await reference.getDownloadURL();
        print(uri);
        final message = types.PartialImage(
          height: image.height.toDouble(),
          name: name,
          size: bytes.length,
          uri: uri,
          width: image.width.toDouble(),
        );
        FirebaseChatCore.instance.sendMessage(
          message,
          _room!.id,
        );
        _setAttachmentUploading(false);
      } catch (exception) {
        print("image exception: " + exception.toString());
      } finally {
        _setAttachmentUploading(false);
      }
    }
  }

  void _handleMessageTap( types.Message message) async {
    if (message is types.FileMessage) {
      var localPath = message.uri;
      if (message.uri.startsWith('http')) {
        try {
          final updatedMessage = message.copyWith(isLoading: true);
          FirebaseChatCore.instance.updateMessage(
            updatedMessage,
            _room!.id,
          );
          final client = http.Client();
          final request = await client.get(Uri.parse(message.uri));
          final bytes = request.bodyBytes;
          final documentsDir = (await getApplicationDocumentsDirectory()).path;
          localPath = '$documentsDir/${message.name}';
          if (!File(localPath).existsSync()) {
            final file = File(localPath);
            await file.writeAsBytes(bytes);
          }
        } finally {
          final updatedMessage = message.copyWith(isLoading: false);
          FirebaseChatCore.instance.updateMessage(
            updatedMessage,
            _room!.id,
          );
        }
      }
      await OpenFile.open(localPath);
    }
  }

  void _handlePreviewDataFetched(types.TextMessage message, types.PreviewData previewData) {
    final updatedMessage = message.copyWith(previewData: previewData);

    FirebaseChatCore.instance.updateMessage(updatedMessage, _room!.id);
  }

  void _handleSendPressed(types.PartialText message) {
    FirebaseChatCore.instance.sendMessage(
      message,
      _room!.id,
    );
  }

  void _setAttachmentUploading(bool uploading) {
    setState(() {
      _isAttachmentUploading = uploading;
    });
  }

  getData() {
    Provider.of<ChatNotifier>(context, listen: false).getEmployeeAssociatedChatsAPI(_employeeId).then((value) {
      setState(() {
        _isChatListLoaded = true;
      });
    });
  }
}
