import 'package:aem/screens/login/ui/login_screen.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool isLargeScreen = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  final _emailFormKey = GlobalKey<FormState>();
  final _formKey = GlobalKey<FormState>();
  late LoginNotifier viewModel;
  bool isOtpSent = false;

  @override
  void initState() {
    viewModel = Provider.of<LoginNotifier>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (ScreenConfig.width(context) > 1000) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: Scaffold(
          backgroundColor: appBackground,
          body: Row(
            children: [
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  color: loginPaneLeft,
                  child: Column(
                    children: [
                      Expanded(
                        child: Image.asset("assets/images/logo.png"),
                      ),
                      Expanded(
                        child: Image.asset("assets/images/logo_bg.png"),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: ScreenConfig.height(context),
                  color: loginPaneRight,
                  padding: isLargeScreen ? const EdgeInsets.fromLTRB(60, 0, 60, 0) : const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Center(
                    child: ListView(
                        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(),
                        controller: ScrollController(),
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              loginInfo(),
                              Visibility(
                                visible: isOtpSent,
                                replacement: emailField(),
                                child: formFields(),
                              )
                            ],
                          ),
                        ]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget loginInfo() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Text(isOtpSent ? resetPassword : forgotPassword, style: loginStyle),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Text(
                isOtpSent
                    ? 'Enter the verification code sent to your email.'
                    : 'Enter the email address. We will send you a verification code to your email.',
                style: loginInfoStyle),
          )
        ],
      ),
    );
  }

  Widget emailField() {
    return Form(
      key: _emailFormKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: TextFormField(
                controller: emailController,
                textInputAction: TextInputAction.next,
                maxLines: 1,
                autofocus: true,
                decoration: editTextUserNameDecoration,
                style: editTextUserNameStyle,
                validator: emailValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 30, 0, 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Container(
                width: double.infinity,
                decoration: buttonCircularDecorationGreen,
                child: TextButton(
                  onPressed: () async {
                    if (_emailFormKey.currentState!.validate()) {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return loader();
                          });
                      await viewModel.requestResetPasswordOTPAPI(emailController.text.trim().toString()).then((value) {
                        Navigator.of(context).pop();
                        if (viewModel.resetOtpResponse != null) {
                          if (viewModel.resetOtpResponse!.responseStatus == 1) {
                            setState(() {
                              isOtpSent = true;
                            });
                            showToast(viewModel.resetOtpResponse!.result!);
                          } else {
                            showToast(viewModel.resetOtpResponse!.result!);
                          }
                        } else {
                          showToast(pleaseTryAgain);
                        }
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text('Request OTP'.toUpperCase(), style: submitButtonStyle),
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Text(
                  'Back to login',
                  style: editTextUserNameStyle,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget formFields() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Pinput(
                animationCurve: Curves.linear,
                animationDuration: const Duration(milliseconds: 160),
                pinAnimationType: PinAnimationType.slide,
                separator: const SizedBox(width: 2),
                showCursor: false,
                length: 6,
                closeKeyboardWhenCompleted: true,
                autofocus: true,
                textInputAction: TextInputAction.next,
                controller: otpController,
                defaultPinTheme: defaultPinTheme,
                focusedPinTheme: defaultPinTheme.copyWith(height: 58, width: 58),
                errorPinTheme: defaultPinTheme.copyWith(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: newReOpened.withOpacity(0.8), width: 1.5),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
                inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(6)],
                validator: pinPutValidator,
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                obscuringCharacter: '*',
                obscureText: true,
                onChanged: (value) {},
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: TextFormField(
                maxLines: 1,
                controller: passwordController,
                textInputAction: TextInputAction.next,
                obscureText: true,
                obscuringCharacter: '*',
                decoration: editTextPasswordDecoration,
                style: editTextPasswordStyle,
                validator: passwordValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: TextFormField(
                maxLines: 1,
                controller: confirmPasswordController,
                textInputAction: TextInputAction.next,
                obscureText: true,
                obscuringCharacter: '*',
                decoration: editTextPasswordDecoration.copyWith(hintText: confirmPassword),
                style: editTextPasswordStyle,
                validator: passwordValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 30, 0, 10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Container(
                width: double.infinity,
                decoration: buttonCircularDecorationGreen,
                child: TextButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return loader();
                          });
                      await viewModel
                          .resetPasswordAPI(otpController.text.trim().toString(), emailController.text.trim().toString(),
                              passwordController.text.trim().toString(), confirmPasswordController.text.trim().toString())
                          .then((value) {
                        Navigator.of(context).pop();
                        if (viewModel.resetPasswordResponse != null) {
                          if (viewModel.resetPasswordResponse!.responseStatus == 1) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(
                                  pageBuilder: (context, animation1, animation2) => const LoginPage(),
                                  transitionDuration: const Duration(seconds: 0),
                                  reverseTransitionDuration: const Duration(seconds: 0),
                                ),
                                (route) => false);
                            showToast(viewModel.resetPasswordResponse!.result!);
                          } else {
                            showToast(viewModel.resetPasswordResponse!.result!);
                          }
                        } else {
                          showToast(pleaseTryAgain);
                        }
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(resetPassword.toUpperCase(), style: submitButtonStyle),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<bool> _onBackPressed() async {
    if (isOtpSent) {
      setState(() {
        isOtpSent = false;
      });
      return false;
    } else {
      Navigator.of(context).pop();
      return true;
    }
  }
}
