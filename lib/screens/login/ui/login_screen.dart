import 'dart:convert';

import 'package:aem/global_view_model/global_notifier.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/login/ui/forgot_password.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/chat/firebase_chat_core.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/navigation_service.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLargeScreen = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late LoginNotifier viewModel;
  late SessionManager sessionManager;
  String _token = "";

  @override
  void initState() {
    viewModel = Provider.of<LoginNotifier>(context, listen: false);
    sessionManager = SessionManager();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (ScreenConfig.width(context) > 1000) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Row(
          children: [
            Expanded(
              child: Container(
                height: ScreenConfig.height(context),
                color: loginPaneLeft,
                child: Column(
                  children: [
                    Expanded(
                      child: Image.asset("assets/images/logo.png"),
                    ),
                    Expanded(
                      child: Image.asset("assets/images/logo_bg.png"),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: ScreenConfig.height(context),
                color: loginPaneRight,
                padding: isLargeScreen ? const EdgeInsets.fromLTRB(60, 0, 60, 0) : const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Center(
                  child: ListView(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      controller: ScrollController(),
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [loginInfo(), formFields(), submitButton(), forgotPasswordButton()],
                        ),
                      ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget loginInfo() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(padding: const EdgeInsets.fromLTRB(0, 5, 0, 5), child: Text(adminLogin, style: loginStyle)),
          Padding(padding: const EdgeInsets.fromLTRB(0, 5, 0, 5), child: Text(dummy, style: loginInfoStyle))
        ],
      ),
    );
  }

  Widget formFields() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: TextFormField(
                controller: emailController,
                textInputAction: TextInputAction.next,
                maxLines: 1,
                autofocus: true,
                decoration: editTextUserNameDecoration,
                style: editTextUserNameStyle,
                validator: emailValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: TextFormField(
                maxLines: 1,
                controller: passwordController,
                textInputAction: TextInputAction.next,
                obscureText: true,
                obscuringCharacter: '*',
                decoration: editTextPasswordDecoration,
                style: editTextPasswordStyle,
                validator: passwordValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          )
        ],
      ),
    );
  }

  Widget submitButton() {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 30, 0, 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: Container(
          width: double.infinity,
          decoration: buttonCircularDecorationGreen,
          child: TextButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return loader();
                    });
                _token = Provider.of<GlobalNotifier>(context, listen: false).getSavedToken();
                await viewModel
                    .employeeLoginAPI(emailController.text..toString().trim(), passwordController.text.toString().trim(), _token)
                    .then((value) async {
                  Navigator.of(context).pop();
                  if (value != null) {
                    if (value.responseStatus == 1) {
                      sessionManager.isEmployeeLogin(true);
                      sessionManager.saveLoginEmployeeData(jsonEncode(value.employeeData!));
                      if (value.employeeData!.chatId == "") {
                        CreateUserChatwithFirebase(value.employeeData!.email!, value.employeeData!.name!).then((chat_result) async {
                          print("chatid- " + chat_result);
                          await FirebaseAuth.instance.signInWithEmailAndPassword(
                            email: value.employeeData!.email!,
                            password: "brioone",
                          );
                          viewModel.createChatIdAPI(value.employeeData!.id!, chat_result);
                        });
                      } else {
                        await FirebaseAuth.instance.signInWithEmailAndPassword(
                          email: value.employeeData!.email!,
                          password: "brioone",
                        );
                      }
                      Provider.of<LoginNotifier>(context, listen: false).getEmployeeDetails();
                      Provider.of<GlobalNotifier>(context, listen: false).setExpandableTilesStates();
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) => HomeScreen(menu: RouteNames.dashBoardRoute),
                            transitionDuration: const Duration(seconds: 0),
                            reverseTransitionDuration: const Duration(seconds: 0),
                          ),
                          (route) => false);
                    } else {
                      showToast(value.result!);
                    }
                  } else {
                    showToast(pleaseTryAgain);
                  }
                });
              }
            },
            child: Padding(padding: const EdgeInsets.all(10), child: Text(login.toUpperCase(), style: submitButtonStyle)),
          ),
        ),
      ),
    );
  }

  Widget forgotPasswordButton() {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: TextButton(
        onPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => const ForgotPassword(),
              transitionDuration: const Duration(seconds: 0),
              reverseTransitionDuration: const Duration(seconds: 0),
            ),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Text(
            '$forgotPassword?',
            style: editTextUserNameStyle,
          ),
        ),
      ),
    );
  }

  Future<String> CreateUserChatwithFirebase(String email, String name) async {
    final credential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: "brioone",
    );
    await FirebaseChatCore.instance.createUserInFirestore(
      types.User(
        firstName: name,
        id: credential.user!.uid,
        imageUrl: "",
        lastName: "",
      ),
    );
    return credential.user!.uid;
  }
}
