import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/login_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class LoginNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  // permissions
  bool _hasPermissions = false, isLogged = false;
  bool _employeeView = false, _employeeAdd = false, _employeeEdit = false;
  bool _designationView = false, _designationAdd = false, _designationEdit = false;
  bool _projectView = false, _projectAdd = false, _projectEdit = false;
  bool _boardingView = false, _boardingAdd = false, _boardingEdit = false;

  bool _technologyView = false, _technologyAdd = false, _technologyEdit = false;
  bool _roleView = false, _roleAdd = false, _roleEdit = false;
  bool _interviewTypeView = false, _interviewTypeAdd = false, _interviewTypeEdit = false;
  bool _profileStatusView = false, _profileStatusAdd = false, _profileStatusEdit = false;
  bool _interviewStatusView = false, _interviewStatusAdd = false, _interviewStatusEdit = false;
  bool _jobRoleView = false, _jobRoleAdd = false, _jobRoleEdit = false;
  bool _tasksView = false, _tasksAdd = false, _tasksEdit = false, _tasksEstimation = false;
  bool _modulesView = false, _modulesAdd = false, _modulesEdit = false;

  bool _stagesView = false, _stagesAdd = false, _stagesEdit = false;
  bool _bugsEdit = false, _bugsAdd = false;
  bool _sprintView = false, _sprintAdd = false, _sprintEdit = false;
  bool _scrumView = false, _scrumtAdd = false, _scrumEdit = false, _scrumtAddTask = false, _scrumFullAccess = false;
  bool _profileView = false,
      _profileAdd = false,
      _profileEdit = false,
      _profileAssign = false,
      _profileFullAccess = false,
      _profileUpdateStatus = false;
  String? _employeeId, _employeeName;
  bool? _isSuperAdmin;
  bool? _isSuperiorEmployee;
  SessionManager sessionManager = SessionManager();

  bool? get isSuperAdmin => _isSuperAdmin;

  bool? get isSuperiorEmployee => _isSuperiorEmployee;

  String? get employeeId => _employeeId;

  String? get employeeName => _employeeName;

  bool get employeeViewPermission => _employeeView;

  bool get employeeAddPermission => _employeeAdd;

  bool get employeeEditPermission => _employeeEdit;

  bool get stagesViewPermission => _stagesView;

  bool get stagesAddPermission => _stagesAdd;

  bool get stagesEditPermission => _stagesEdit;

  bool get designationViewPermission => _designationView;

  bool get designationAddPermission => _designationAdd;

  bool get designationEditPermission => _designationEdit;

  bool get bugsEditPermission => _bugsEdit;

  bool get bugsAddPermission => _bugsAdd;

  bool get projectViewPermission => _projectView;

  bool get projectAddPermission => _projectAdd;

  bool get projectEditPermission => _projectEdit;

  bool get boardingViewPermission => _boardingView;

  bool get boardingAddPermission => _boardingAdd;

  bool get boardingEditPermission => _boardingEdit;

  bool get technologyViewPermission => _technologyView;

  bool get technologyAddPermission => _technologyAdd;

  bool get technologyEditPermission => _technologyEdit;

  bool get roleViewPermission => _roleView;

  bool get roleAddPermission => _roleAdd;

  bool get roleEditPermission => _roleEdit;

  bool get interviewTypeViewPermission => _interviewTypeView;

  bool get interviewTypeAddPermission => _interviewTypeAdd;

  bool get interviewTypeEditPermission => _interviewTypeEdit;

  bool get profileStatusViewPermission => _profileStatusView;

  bool get profileStatusAddPermission => _profileStatusAdd;

  bool get profileStatusEditPermission => _profileStatusEdit;

  bool get interviewStatusViewPermission => _interviewStatusView;

  bool get interviewStatusAddPermission => _interviewStatusAdd;

  bool get interviewStatusEditPermission => _interviewStatusEdit;

  bool get jobRoleViewPermission => _jobRoleView;

  bool get jobRoleAddPermission => _jobRoleAdd;

  bool get jobRoleEditPermission => _jobRoleEdit;

  bool get profileViewPermission => _profileView;

  bool get profileAddPermission => _profileAdd;

  bool get profileEditPermission => _profileEdit;

  bool get profileAssignPermission => _profileAssign;

  bool get profileFullAccessPermission => _profileFullAccess;

  bool get profileUpdateStatusPermission => _profileUpdateStatus;

  bool get tasksViewPermission => _tasksView;

  bool get tasksAddPermission => _tasksAdd;

  bool get tasksEditPermission => _tasksEdit;

  bool get tasksEstimationPermission => _tasksEstimation;

  bool get modulesViewPermission => _modulesView;

  bool get modulesAddPermission => _modulesAdd;

  bool get modulesEditPermission => _modulesEdit;

  bool get sprintViewPermission => _sprintView;

  bool get sprintAddPermission => _sprintAdd;

  bool get sprintEditPermission => _sprintEdit;

  bool get employeeHasPermissions => _hasPermissions;

  bool get scrumViewPermission => _scrumView;

  bool get scrumAddPermission => _scrumtAdd;

  bool get scrumEditPermission => _scrumEdit;

  bool get scrumAddTaskPermission => _scrumtAddTask;

  bool get scrumFullAccessPermission => _scrumFullAccess;

  Future<void> getEmployeeDetails() async {
    await sessionManager.isEmployeeLoggedIn().then((value) {
      if (value == null) {
        isLogged = false;
      } else {
        isLogged = value;
      }
    });
    await sessionManager.getLoginEmployeeData().then((value) {
      if (value.id != null) {
        _employeeId = value.id!;
        _isSuperAdmin = value.isSuperAdmin;
        _isSuperiorEmployee = value.isSuperiorEmployee;
        _employeeName = value.name!;
      }
    });
    await sessionManager.getEmployeePermissions().then((value) {
      if (value.employeeId != null) {
        print("bug permissions ${value.bugsPermissions}");
        if (value.employeePermissions!.isNotEmpty) {
          for (var element in value.employeePermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _employeeView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _employeeAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _employeeEdit = true;
            }
          }
        }

        if (value.developmentStatgesPermissions!.isNotEmpty) {
          for (var element in value.developmentStatgesPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _stagesView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _stagesAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _stagesEdit = true;
            }
          }
        }
        if (value.projectPermissions!.isNotEmpty) {
          for (var element in value.projectPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _projectView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _projectAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _projectEdit = true;
            }
          }
        }

        if (value.boardingPermissions!.isNotEmpty) {
          for (var element in value.boardingPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _boardingView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _boardingAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _boardingEdit = true;
            }
          }
        }
        if (value.designationPermissions!.isNotEmpty) {
          for (var element in value.designationPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _designationView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _designationAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _designationEdit = true;
            }
          }
        }
        if (value.technologyPermissions!.isNotEmpty) {
          for (var element in value.technologyPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _technologyView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _technologyAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _technologyEdit = true;
            }
          }
        }
        if (value.bugsPermissions!.isNotEmpty) {
          print("bug -- ${value.bugsPermissions}");
          for (var element in value.bugsPermissions!) {
            if (element.toLowerCase() == add.toLowerCase()) {
              _bugsAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _bugsEdit = true;
            }
          }
        }
        if (value.rolePermissions!.isNotEmpty) {
          for (var element in value.rolePermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _roleView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _roleAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _roleEdit = true;
            }
          }
        }
        if (value.interviewTypesPermissions!.isNotEmpty) {
          for (var element in value.interviewTypesPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _interviewTypeView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _interviewTypeAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _interviewTypeEdit = true;
            }
          }
        }
        if (value.profileStatusPermissions!.isNotEmpty) {
          for (var element in value.profileStatusPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _profileStatusView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _profileStatusAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _profileStatusEdit = true;
            }
          }
        }
        if (value.interviewStatusPermissions!.isNotEmpty) {
          for (var element in value.interviewStatusPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _interviewStatusView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _interviewStatusAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _interviewStatusEdit = true;
            }
          }
        }
        if (value.jobRolePermissions!.isNotEmpty) {
          for (var element in value.interviewStatusPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _jobRoleView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _jobRoleAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _jobRoleEdit = true;
            }
          }
        }
        if (value.profilePermissions!.isNotEmpty) {
          for (var element in value.profilePermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _profileView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _profileAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _profileEdit = true;
            } else if (element.toLowerCase() == assign.toLowerCase()) {
              _profileAssign = true;
            } else if (element.toLowerCase() == fullAccess.toLowerCase()) {
              _profileFullAccess = true;
            } else if (element.toLowerCase() == updateStatus.toLowerCase()) {
              _profileUpdateStatus = true;
            }
          }
        }

        if (value.scrumPermissions!.isNotEmpty) {
          for (var element in value.scrumPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _scrumView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _scrumtAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _scrumEdit = true;
            } else if (element.toLowerCase() == addTask.toLowerCase()) {
              _scrumtAddTask = true;
            } else if (element.toLowerCase() == fullAccess.toLowerCase()) {
              _scrumFullAccess = true;
            }
          }
        }
        if (value.taskPermissions!.isNotEmpty) {
          for (var element in value.taskPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _tasksView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _tasksAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _tasksEdit = true;
            } else if (element.toLowerCase() == estimation.toLowerCase()) {
              _tasksEstimation = true;
            }
          }
        }
        if (value.modulePermissions!.isNotEmpty) {
          for (var element in value.modulePermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _modulesView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _modulesAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _modulesEdit = true;
            }
          }
        }

        if (value.sprintPermissions!.isNotEmpty) {
          for (var element in value.sprintPermissions!) {
            if (element.toLowerCase() == view.toLowerCase()) {
              _sprintView = true;
            } else if (element.toLowerCase() == add.toLowerCase()) {
              _sprintAdd = true;
            } else if (element.toLowerCase() == edit.toLowerCase()) {
              _sprintEdit = true;
            }
          }
        }
        _hasPermissions = true;
      } else {
        _hasPermissions = false;
      }
    });
    notifyListeners();
  }

  Future<LoginResponse?> employeeLoginAPI(String email, String password, String token) async {
    _isFetching = true;
    _isHavingData = false;
    LoginResponse? _loginResponse = LoginResponse();

    try {
      dynamic response = await Repository().employeeLogin(email, password, token);
      if (response != null) {
        _loginResponse = LoginResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_loginResponse");
    notifyListeners();
    return _loginResponse;
  }

  BaseResponse? _logoutResponse;

  BaseResponse? get logoutResponse => _logoutResponse;

  Future<BaseResponse?> employeeLogoutAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    _logoutResponse = BaseResponse();

    try {
      dynamic response = await Repository().employeeLogout(employeeId);
      if (response != null) {
        _logoutResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_logoutResponse");
    notifyListeners();
    return _logoutResponse;
  }

  BaseResponse? _updateTokenResponse;

  BaseResponse? get updateTokenResponse => _updateTokenResponse;

  Future<BaseResponse?> updateTokenAPI(String employeeId, String token) async {
    _isFetching = true;
    _isHavingData = false;
    _updateTokenResponse = BaseResponse();

    try {
      dynamic response = await Repository().tokenUpdate(employeeId, token);
      if (response != null) {
        _updateTokenResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateTokenResponse");
    notifyListeners();
    return _updateTokenResponse;
  }

  BaseResponse? _resetOtpResponse;

  BaseResponse? get resetOtpResponse => _resetOtpResponse;

  Future<BaseResponse?> requestResetPasswordOTPAPI(String email) async {
    _isFetching = true;
    _isHavingData = false;
    _resetOtpResponse = BaseResponse();

    try {
      dynamic response = await Repository().requestResetPasswordOTP(email);
      if (response != null) {
        _resetOtpResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_resetOtpResponse");
    notifyListeners();
    return _resetOtpResponse;
  }

  BaseResponse? _resetPasswordResponse;

  BaseResponse? get resetPasswordResponse => _resetPasswordResponse;

  Future<BaseResponse?> resetPasswordAPI(String otp, String email, String newPassword, String confirmNewPassword) async {
    _isFetching = true;
    _isHavingData = false;
    _resetPasswordResponse = BaseResponse();

    try {
      dynamic response = await Repository().resetPassword(otp, email, newPassword, confirmNewPassword);
      if (response != null) {
        _resetPasswordResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_resetPasswordResponse");
    notifyListeners();
    return _resetPasswordResponse;
  }

  BaseResponse? _chatidResponse;

  BaseResponse? get chatidResponse => _chatidResponse;

  Future<BaseResponse?> createChatIdAPI(String employeeId, String chatId) async {
    _isFetching = true;
    _isHavingData = false;
    _chatidResponse = BaseResponse();

    try {
      dynamic response = await Repository().createChatId(employeeId, chatId);
      if (response != null) {
        _chatidResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_chatidResponse");
    notifyListeners();
    return _chatidResponse;
  }

  resetValues() {
    _hasPermissions = false;
    isLogged = false;
    _employeeView = false;
    _employeeAdd = false;
    _employeeEdit = false;
    _designationView = false;
    _designationAdd = false;
    _designationEdit = false;
    _projectView = false;
    _projectAdd = false;
    _projectEdit = false;
    _technologyView = false;
    _technologyAdd = false;
    _technologyEdit = false;
    _roleView = false;
    _roleAdd = false;
    _roleEdit = false;
    _interviewTypeView = false;
    _interviewTypeAdd = false;
    _interviewTypeEdit = false;
    _profileStatusView = false;
    _profileStatusAdd = false;
    _profileStatusEdit = false;
    _interviewStatusView = false;
    _interviewStatusAdd = false;
    _interviewStatusEdit = false;
    _stagesView = false;
    _stagesEdit = false;
    _stagesAdd = false;
    _tasksView = false;
    _tasksAdd = false;
    _tasksEdit = false;
    _modulesView = false;
    _modulesAdd = false;
    _modulesEdit = false;
    _profileView = false;
    _profileAdd = false;
    _profileEdit = false;
    _profileAssign = false;
    _profileFullAccess = false;
    _profileUpdateStatus = false;
    _bugsEdit = false;
    _bugsAdd = false;
    _tasksEstimation = false;
    _isSuperAdmin = false;
    notifyListeners();
  }
}
