import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/model/scrum_counts_response.dart' as my_scrum_counts;
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:aem/widgets/scrum_status_widget.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../model/all_team_wise_scrum_list_response_model.dart';
class TeamWiseScrumFragment extends StatefulWidget {
  final String? employeeId;
  final String? employeeName;
  const TeamWiseScrumFragment({Key? key, this.employeeId, this.employeeName}) : super(key: key);
  @override
  State<TeamWiseScrumFragment> createState() => _TeamWiseScrumFragmentState();
}
class _TeamWiseScrumFragmentState extends State<TeamWiseScrumFragment> {
  late ProjectNotifier viewModel;
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  String employeeId = "", _startDate = "", _endDate = "", _year = "";
  bool viewMyScrum = true, isTableView = true, isApiRunning = true,isMonthGridView = false;
  int selectedButton = 0;
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  ScrollController listScrollController = ScrollController();
  ScrollController gridScrollController = ScrollController();
  ScrollController tableScrollController = ScrollController();
  DateTime? initialDate, pastDate;
  String selectedType = 'today';
  String? filterMonth = "";
  String? filterYear = "";
  @override
  void initState() {
    viewModel = Provider.of<ProjectNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    if (widget.employeeId! == "") {
      employeeId = userViewModel.employeeId!;
      viewMyScrum = true;
    } else {
      employeeId = widget.employeeId!;
      viewMyScrum = false;
    }
    initialDate = DateTime.now();
    // allowing only last two months
    pastDate = DateTime(initialDate!.year, initialDate!.month - 2, initialDate!.day);
    _startDate = getTodayDateDDMMYYYY();
    _endDate = _startDate;
    getTeamwiseScrums();



    super.initState();
  }
  void getData() {
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });



    });
  }
  void getModulesata(String id) {
    Future.delayed(const Duration(milliseconds: 00), () async {

    });
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {},
                    selectedMenu: "${RouteNames.scrumTeamRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }
  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 25, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuScrum", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(5, 15, 5, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [header(), _body()],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () {

              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(viewMyScrum ? "My $navMenuScrum" : "${widget.employeeName}  Team $navMenuScrum",
                    maxLines: 2, softWrap: true, style: fragmentHeaderStyle),
              ),


            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: fromDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(labelText: "From Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(fromDateController);
                          },
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            _startDate = fromDateController.text.trim();
                            _endDate = toDateController.text.trim();

                            if (_startDate == "") {

                              showToast("Select date");
                            } else {
                              selectedType = "custom";
                              selectedButton = 4;
                              getTeamwiseScrums();
                            }
                          });
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            searchGo,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: dropDownColor,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(searchGoButtonBg),
                          overlayColor: MaterialStateProperty.all(dropDownColor.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ),
                    /*(fromDateController.text.trim() != "" || toDateController.text.trim() != "")
                        ? */
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          resetTextFields();
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            reset,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(dashBoardCardOverDueText),
                          overlayColor: MaterialStateProperty.all(dashBoardCardOverDueText.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ) /*: const SizedBox()*/,
                  ],
                ),
              ),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.end,
                  spacing: 5,
                  runSpacing: 5,
                  children: [
                    _customOutlinedRadioButton(today, 0, buttonBg),
                    _customOutlinedRadioButton(monthly, 2, newOnGoing),
                    _customOutlinedRadioButton(yearly, 3, buttonraisebug),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget _body() {
    var scrumList = Provider.of<ScrumNotifier>(context, listen: true).getTeamWiseScrumsList;
    print(scrumList.length);
    var scrumCountList = Provider.of<ScrumNotifier>(context, listen: true).getMyScrumCountsList;
    var scrumYearsCountList = Provider.of<ScrumNotifier>(context, listen: true).getMyYearScrumsList;
    return Expanded(
        child: isTableView
            ? scrumList.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    controller: ScrollController(),
                    itemCount: scrumList.length,
                    itemBuilder: (context, index) {
                      return Column( mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 0, 7),
                          child: Text(
                            scrumList[index].heading.toString(),
                            maxLines: 2,
                            softWrap: true,
                            style: fragmentHeaderStyle.copyWith(
                              color: buttonBg,
                              fontSize: 16,
                              letterSpacing: 1.25,
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            listHeader(),
                            ListView.separated(
                              //padding: const EdgeInsets.only(left: 10, right: 10),
                              physics: const ScrollPhysics(),
                              controller: listScrollController,
                              itemCount: scrumList[index].tasksData!.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index1) {

                                return listItems(scrumList[index].tasksData!, index1);
                              },
                              separatorBuilder: (context, index) {
                                return const Divider(
                                  color:Colors.transparent,
                                  height: 0.5,
                                );
                              },
                            )
                          ],
                        )
                      ],);
                    }

                )
                : SizedBox(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    child: Center(
                      child: Text(isApiRunning ? "" : noDataAvailable, style: logoutHeader),
                    ),
                  )
            : scrumCountList.isNotEmpty
            ? GridView.builder(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            controller: gridScrollController,
            physics: const ScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: scrumCountList.length,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                childAspectRatio: 2.35, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 450),
            itemBuilder: (BuildContext context, int index) {
              return countsListItems(scrumCountList[index]);
            })
            : SizedBox(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          child: Center(
            child: Text(isApiRunning ? "" : noDataAvailable, style: logoutHeader),
          ),
        ),
    );
    /* : scrumCountList.isNotEmpty
        ? GridView.builder(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        controller: gridScrollController,
        physics: const ScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: scrumCountList.length,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            childAspectRatio: 2.35, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 450),
        itemBuilder: (BuildContext context, int index) {
          return countsListItems(scrumCountList[index]);
        })
        : SizedBox(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      child: Center(
        child: Text(isApiRunning ? "" : noDataAvailable, style: logoutHeader),
      ),
    ))*/
    ;
  }
  Widget _holdStatusButton(int percent,String comment) {

    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 5, 5, 5),
          child: OnHoverCard(builder: (isHovered) {
            return Container(
              width: 90,
              decoration: BoxDecoration(
                  color: CupertinoColors.white, borderRadius: BorderRadius.circular(0), border: Border.all(color: holdBgColor, width: 1.5)),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: DecoratedBox(
                      decoration: const BoxDecoration(color: holdBgColor),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                        child: Text(
                          hold,
                          style: const TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: CupertinoColors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Text(
                        '${percent} %',
                        style: TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: addRoleText),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),

        Padding(
          padding: const EdgeInsets.only(left: 3),
          child: Tooltip(
            message: comment,
            decoration: toolTipDecoration,
            textStyle: toolTipStyle,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.info_outline, color: timeText),
          ),
        )
      ],
    );

  }

  Widget taskslistItems(EmpTasksData itemData, int index) {
    Color color_style;
    String status_text = "";
    if (itemData.taskStatus == 0) {
      status_text = inActive;
      color_style = footerText;
    } else if (itemData.taskStatus == 1) {
      status_text = newStr;
      color_style = newTaskText;
    } else if (itemData.taskStatus == 2) {
      status_text = open;
      color_style = newOnGoing;
    } else if (itemData.taskStatus == 3) {
      status_text = projectCompleted;
      color_style = newCompleted;
    } else if (itemData.taskStatus == 4) {
      status_text = close;
      color_style = newCompleted;
    } else if (itemData.taskStatus == 5) {
      status_text = reOpen;
      color_style = newReOpened;
    } else if (itemData.taskStatus == 6) {
      status_text = delete;
      color_style = newCompleted;
    } else if (itemData.taskStatus == 7) {
      status_text = hold;
      color_style = dashBoardCardOverDueText;
    } else {
      status_text = inActive;
      color_style = footerText;
    }

    return InkWell(
      onTap: (){
        print("-- ${itemData.projectId.toString()} -${itemData.moduleId.toString()} - ${itemData.taskId.toString()}");

        Navigator.push(
          context,
          PageRouteBuilder(
            pageBuilder:
                (context, animation1, animation2) =>
                ViewTaskScreen(
                    projectId: itemData.projectId!,
                    moduleId: itemData.moduleId!,
                    taskId: itemData.taskId!),
            transitionDuration:
            const Duration(seconds: 0),
            reverseTransitionDuration:
            const Duration(seconds: 0),
          ),
        ).then((value) {

        });
      },
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(0), border: Border.all(color: dashBoardCardBorderTile)),
        child: Material(
          color: Colors.transparent,
          child: Row(
            children: [
             // Text("dsf"),
              if(itemData.taskName != "") ...[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: "${(index + 1).toString() + ") "} " +  itemData.taskName!,
                          style: listItemsStyle,

                        ),
                        TextSpan(text: otherTaskIdentification(itemData.taskAddedIn!) ? ' *' : '', style: logoutContentHeader.copyWith(color: Colors.blue)) ,
                      ]),
                    ),
                  ),
                ),
              ]else ...[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: "${(index + 1).toString() + ") "} " +  itemData.bugTitle!,
                          style: listItemsStyle,

                        ),
                        TextSpan(text: otherTaskIdentification(itemData.taskAddedIn!) ? ' *' : '', style: logoutContentHeader.copyWith(color: Colors.blue)) ,
                      ]),
                    ),
                  ),
                ),
              ],
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text(itemData.moduleName!,textAlign: TextAlign.start, softWrap: true,  style: listItemsStyle,),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text(itemData.projectName!,textAlign: TextAlign.start, softWrap: true,  style: listItemsStyle),
                ),
              ),
              Expanded(
                flex: 1,
                child: itemData.taskStatus == 7 ?  _holdStatusButton(itemData.percentageComplete!,itemData.holdComment!) :Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text(status_text,textAlign: TextAlign.center, softWrap: true,  style: TextStyle(letterSpacing: 0.51, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: color_style, fontSize: 14),),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text(minutesToHrs(itemData.estimationTime!).toString(), softWrap: true,  style: listItemsStyle, textAlign: TextAlign.center),
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Text(minutesToHrs(Duration(seconds: itemData.involvedTime!).inMinutes).toString(), softWrap: true,  style: listItemsStyle.copyWith(color: checkEstimationTime(itemData.involvedTime!, itemData.estimationTime!)), textAlign: TextAlign.center),
                    ),
                    !checkTimeforInformation(itemData.involvedTime!, itemData.estimationTime!) ? Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: Tooltip(
                        message: itemData.closeComment,
                        decoration: toolTipDecoration,
                        textStyle: toolTipStyle,
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Icon(Icons.info_outline, color: timeText),
                      ),
                    ) : SizedBox(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
   bool otherTaskIdentification(String taskAddedIn){
    if(taskAddedIn == 'scrum'){

      return true;

    }else{
      return false ;
    }

  }
  bool checkTimeforInformation(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return false;
    } else {
      return true;
    }
  }
  Color checkEstimationTime(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return newOverDue;
    } else {
      return newCompleted;
    }
  }
  Widget listItems(List<TasksData> taskdata, int pos) {
    //int alternate = index % 2;
    /* return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(0), border: Border.all(color: dashBoardCardBorderTile)),
      child: Material(elevation: 0,type: MaterialType.transparency,
        color: Colors.transparent,
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text(taskdata.employeeName!,
                    softWrap: true,   style: listItemsStyle, textAlign: TextAlign.start),
              ),
            ),
            Expanded(
              flex: 7,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: ListView.separated(
                  physics: const ScrollPhysics(),
                  controller: listScrollController,
                  itemCount: taskdata.empTasksData!.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return taskslistItems(taskdata.empTasksData![index], index);
                  },
                  separatorBuilder: (context, index) {
                    return const Divider(
                      color: Colors.transparent,
                      height: 0,
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    )*/;
    print("count ${taskdata[pos].empTasksData!.length}");
    return Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(0), border: Border.all(color: dashBoardCardBorderTile)),
        child:Row(
      children: [
      SizedBox(
        width: 150,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Text(taskdata[pos].employeeName!,
              softWrap: true,   style: listItemsStyle, textAlign: TextAlign.start),
        ),
      ),
        Expanded(
             flex: 7,
             child: Padding(
               padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
               child: ListView.separated(
                 physics: const ScrollPhysics(),
                 controller: listScrollController,
                 itemCount: taskdata[pos].empTasksData!.length,
                 shrinkWrap: true,
                 itemBuilder: (context, index) {
                  // return Text("sddfds");

                   return taskslistItems(taskdata[pos].empTasksData![index], index);
                 },
                 separatorBuilder: (context, index) {
                   return const Divider(
                     color: Colors.transparent,
                     height: 0,
                   );
                 },
               ),
             ),
           ),
       /* ListView.separated(
        //padding: const EdgeInsets.only(left: 10, right: 10),
        physics: const ScrollPhysics(),
        controller: listScrollController,
        itemCount: taskdata[pos].empTasksData!.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Text("sdf");

          return taskslistItems(taskdata[pos].empTasksData![index], index);
        },
        separatorBuilder: (context, index) {
          return const Divider(
            color:Colors.transparent,
            height: 0.5,
          );
        },
      ),*/
      ]
    ))
    ;
  }
  Widget subListItems(TasksData taskdata, int index) {
    //int alternate = index % 2;
     return Material(elevation: 0,type: MaterialType.transparency,
       color: Colors.transparent,
       child: Row(
         children: [
           Expanded(
             flex: 1,
             child: Padding(
               padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
               child: Text(taskdata.employeeName!,
                   softWrap: true,   style: listItemsStyle, textAlign: TextAlign.start),
             ),
           ),
           Expanded(
             flex: 7,
             child: Padding(
               padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
               child: ListView.separated(
                 physics: const ScrollPhysics(),
                 controller: listScrollController,
                 itemCount: taskdata.empTasksData!.length,
                 shrinkWrap: true,
                 itemBuilder: (context, index) {
                   return taskslistItems(taskdata.empTasksData![index], index);
                 },
                 separatorBuilder: (context, index) {
                   return const Divider(
                     color: Colors.transparent,
                     height: 0,
                   );
                 },
               ),
             ),
           ),
         ],
       ),
     );
   
    
  }
  Widget countsListItems(my_scrum_counts.ScrumData scrumData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: isHovered ? 2 : 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              if (getScrumFilterType() == daily) {
                setState(() {
                  _startDate = scrumData.heading!;
                  _endDate = scrumData.heading!;
                  getTeamwiseScrums();
                });
              } else if (getScrumFilterType() == monthly) {
                setState(() {
                  isMonthGridView = true;
                  DateTime tempDate = DateFormat("MMMM yyyy").parse(scrumData.heading!, true).toLocal();
                  var monthFormat = DateFormat('MM'); // get month
                  var yearFormat = DateFormat('yyyy'); // get year
                  var _month = monthFormat.format(tempDate);
                  var _year = yearFormat.format(tempDate);
                  _startDate = "01-$_month-$_year"; // start date is 01
                  DateTime _lastDayOfMonth = DateTime(int.parse(_year), int.parse(_month) + 1, 0); // end date is calculated as total number of days
                  _endDate = "${_lastDayOfMonth.day}-$_month-$_year";
                  filterMonth = _month;
                  filterYear = _year;
                  getTeamwiseScrums();
                });
              } else if (getScrumFilterType() == yearly) {
                setState(() {
                  selectedButton = 2;
                  getMyScrumCounts();
                });
                /*setState(() {
                  isMonthGridView = false;
                  _startDate = "";
                  _endDate = "";
                  _year = scrumData.heading!;
                  filterYear = _year;
                  print('selected year -- ${_year}');
                  getTeamwiseScrums();
                });*/
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: [
                Text(
                  scrumData.heading!,
                  maxLines: 2,
                  softWrap: true,
                  style: fragmentHeaderStyle.copyWith(
                    color: isHovered ? Colors.white : editText,
                    fontSize: 16,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(10, 10, 5, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  newStr,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: buttonBg,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.newTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  pending,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: newOnGoing,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.pendingTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(5, 10, 10, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  complete,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: buttonraisebug,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.completedTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
        ),
      );
    });
  }
  Widget listHeader() {
    return Container(
      decoration: buttonTopCircularDecorationGreen,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(name, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(tasks, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(modules, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(project, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(status, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(estimation, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(taskTimeScrum, softWrap: true,  style: listHeaderStyle, textAlign: TextAlign.center),
            ),
          ),
        ],
      ),
    );
  }


  Widget _customOutlinedRadioButton(String text, int index, Color border) {
    return OutlinedButton(
      onPressed: () {
        setState(() {
          selectedButton = index;
          resetTextFields();
          if (selectedButton == 0) {
            setState(() {
              _startDate = getTodayDateDDMMYYYY();
              _endDate = _startDate;
            });
            getTeamwiseScrums();
          } else {
            getMyScrumCounts();
          }
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          text,
          style: TextStyle(
            letterSpacing: 0.9,
            fontWeight: FontWeight.w600,
            color: (selectedButton == index) ? Colors.white : border,
          ),
        ),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all((selectedButton == index) ? border : Colors.white),
        overlayColor: MaterialStateProperty.all(border.withOpacity(0.1)),
        side: MaterialStateProperty.all(BorderSide(color: border)),
        elevation: MaterialStateProperty.all((selectedButton == index) ? 2 : 1),
      ),
    );
  }

  Future<void> showCalendar(TextEditingController textController) async {
    DateTime? tempDate;
    if (fromDateController.text != "") {
      tempDate = DateFormat("dd-MM-yyyy").parse(fromDateController.text);
      if (textController == fromDateController) {
        tempDate = null;
      }
    }
    DateTime? picked = await showDatePicker(context: context, initialDate: initialDate!, firstDate: tempDate ?? pastDate!, lastDate: initialDate!);
    if (picked != null) {
      setState(() {
        var inputFormat = DateFormat('yyyy-MM-dd');
        // removing time from date
        var inputDate = inputFormat.parse(picked.toLocal().toString().split(' ')[0]);
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      });
    }
  }

  String getScrumFilterType() {
    switch (selectedButton) {
      case 0:
        {
          return today;
        }
      case 1:
        {
          return daily;
        }
      case 2:
        {
          return monthly;
        }
      case 3:
        {
          return yearly;
        }
      default:
        return today;
    }
  }

  resetTextFields() {
    _startDate = "";
    _endDate = "";
    fromDateController.clear();
    toDateController.clear();
  }

  getTeamwiseScrums() {
    Future.delayed(Duration.zero, () {
      setState(() {
        isApiRunning = true;
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      if (selectedButton == 0) {
        selectedType = "today";
      } else if (selectedButton == 2) {
        selectedType = "monthly";
        _startDate = "";
      } else if (selectedButton == 3) {
        selectedType = "yearly";
        _startDate = "";
      } else if (selectedButton == 4) {
        selectedType = "custom";
      }
      final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
        viewModel.viewteamwiseScrumsApi(employeeId, selectedType, _startDate,filterMonth!,filterYear!).then((value) {
          Navigator.of(context).pop();
          setState(() {
            isTableView = true;
            isApiRunning = false;
          });
        });
    });
  }

  getMyYearScrums() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel.viewMyScrumYearlyApi(employeeId, _year).then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = true;
        isApiRunning = false;
      });
    });
  }

  getMyScrumCounts() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel.viewMyScrumCountsApi(employeeId, getScrumFilterType().toLowerCase()).then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = false;
        isApiRunning = false;
      });
    });
  }
  updateTaskStatus(String taskId, String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateTasksBasedOnStatusAPI(employeeId, taskId, status,"",0).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getTeamwiseScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }

  updateBugStatus(String bugId, String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateBugStatusAPI(bugId, employeeId, status).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getTeamwiseScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }

}
