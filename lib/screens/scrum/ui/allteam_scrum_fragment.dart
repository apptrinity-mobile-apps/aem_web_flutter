import 'package:aem/model/all_team_scrum_list_model.dart';
import 'package:aem/model/team_scrum_response_model.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/scrum/ui/employee_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/teamwise_scrum_fragment.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';

import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/loader.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AllTeamScrumFragment extends StatefulWidget {
  final String? employeeId;

  const AllTeamScrumFragment({Key? key, required this.employeeId}) : super(key: key);

  @override
  _AllTeamScrumFragmentState createState() => _AllTeamScrumFragmentState();
}

class _AllTeamScrumFragmentState extends State<AllTeamScrumFragment> {
  late ScrumNotifier viewModel;
  String employeeId = "";
  bool viewMyScrum = true;
  int selectedButton = 0;
  final isSelected = <bool>[true, false, false, false];
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  DateTime? initialDate, pastDate;
  List<EmployeesList> superioremployeelistList = [];
  String selectedType = 'today';
  ScrollController listScrollController = ScrollController();
  ScrollController tableScrollController = ScrollController();

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    if (widget.employeeId! == "") {
      employeeId = userViewModel.employeeId!;
      viewMyScrum = true;
    } else {
      employeeId = widget.employeeId!;
      viewMyScrum = false;
    }
    viewModel = Provider.of<ScrumNotifier>(context, listen: false);

    initialDate = DateTime.now();
    // allowing last two months date
    pastDate = DateTime(initialDate!.year, initialDate!.month - 2, initialDate!.day);
    getData();
    super.initState();
  }

  void getData() {
    Future.delayed(Duration.zero, () async {
      /* await employeeViewModel.getEmployeesAPI().then((value) {
        if (value!.employeeDetails!.isNotEmpty) {
          employeesList = value.employeeDetails!;
        }
      });*/
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      if (selectedButton == 0) {
        selectedType = "today";
      } else if (selectedButton == 2) {
        selectedType = "monthly";
      } else if (selectedButton == 3) {
        selectedType = "yearly";
      } else if (selectedButton == 4) {
        selectedType = "custom";
      }
      await viewModel.getAllTeamScrumCountapi(employeeId).then((value) {
        Navigator.pop(context);
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeesList!.isNotEmpty) {
                superioremployeelistList = value.employeesList!;
              }
            });
          }
        }
        // hasTechnologies = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuScrum", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
                          child: SizedBox(height: ScreenConfig.height(context), width: ScreenConfig.width(context), child: body()),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text("Superior Employee List", maxLines: 2, softWrap: true, style: fragmentHeaderStyle),
          ),
        ],
      ),
    );
  }

  Widget body() {
    return listItems(superioremployeelistList);
  }

  Widget listItems(List<EmployeesList> superioremployeedata) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          DataTable2(
            columnSpacing: 14,
            dividerThickness: 1,
            scrollController: tableScrollController,
            decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
            headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
            headingRowHeight: 46,
            dataRowHeight: 60,
            smRatio: 0.4,
            lmRatio: 1.5,
            columns: tableColumnHeader(),
            rows: tableRows(superioremployeedata),
            empty: Center(
              child: Text(noDataAvailable, style: logoutHeader),
            ),
          )
        ],
      ),
    );
  }

  List<DataColumn2> tableColumnHeader() {
    return <DataColumn2>[
      DataColumn2(label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.S),
      DataColumn2(label: Text(employee + " " + name, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(teamcount, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
    ];
  }

  List<DataRow> tableRows(List<EmployeesList> emplist) {
    /* actions -
    * task type = "task" show buttons
    * 1 (new)- start
    * 2 (open)- hold, complete
    * 3 (completed)- reopen, close
    * 4 (close) - empty
    * 5 (reopen)- start
    * 6 (delete) - empty
    * 7 (hold)- start, complete */

    return emplist.map<DataRow2>((e) {
      return DataRow2(
          color: MaterialStateColor.resolveWith((states) => CupertinoColors.white),
          specificRowHeight: 60,
          cells: [
            DataCell(Text((emplist.indexOf(e) + 1).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
            DataCell(Text(e.employeeName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
            DataCell(Text(e.employeesCount!.toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
          ],
          onTap: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => TeamWiseScrumFragment(
                  employeeId: e.employeeId,
                  employeeName: e.employeeName,
                ),
                transitionDuration: const Duration(seconds: 0),
                reverseTransitionDuration: const Duration(seconds: 0),
              ),
            ).then((value) {
              // set state to reload data , reloads data in futureBuilder
              setState(() {});
            });
          });
    }).toList();
  }

  Widget sprintsSubList(int position, List<TasksCount> data, String date) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable2(
                  columnSpacing: (MediaQuery.of(context).size.width / 10) * 0.5,
                  dividerThickness: 1,
                  decoration: BoxDecoration(border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  columns: <DataColumn>[
                    DataColumn2(
                      size: ColumnSize.S,
                      label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text(employee + " " + name, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text("# of Tasks", softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                    ),
                    DataColumn2(
                      label: Text(projectNew, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text(inProgress, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(projectCompleted, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                          /*Image.asset('assets/images/edit_icon.png',
                              width: 12, height: 12, color: CupertinoColors.black),*/
                          selectedButton == 0 || selectedButton == 4
                              ? InkWell(
                                  onTap: () {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        PageRouteBuilder(
                                            pageBuilder: (context, animation1, animation2) =>
                                                HomeScreen(menu: RouteNames.scrumAddRoute, scrumDate: date),
                                            transitionDuration: const Duration(seconds: 0),
                                            reverseTransitionDuration: const Duration(seconds: 0)),
                                        (route) => false);
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    size: 18,
                                    color: Colors.black,
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ],
                  rows: data.map<DataRow>((e) {
                    return DataRow2(
                        /*color: MaterialStateColor.resolveWith(
                                (states) => data.!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1 CupertinoColors.white),*/
                        onTap: () {
                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => EmployeeScrumFragment(
                                employeeId: e.employeeId,
                                employeeName: e.employeeName,
                              ),
                              transitionDuration: const Duration(seconds: 0),
                              reverseTransitionDuration: const Duration(seconds: 0),
                            ),
                          ).then((value) {
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        },
                        cells: [
                          DataCell(Text((data.indexOf(e) + 1).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
                          DataCell(Text(e.employeeName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.totalTasksCount!.toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.newTasksCount!.toString(), softWrap: true, style: projectAddedByStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.pendingTasksCount!.toString(),
                              softWrap: true, style: projectAddedByStyle.copyWith(color: newOnGoing), textAlign: TextAlign.center)),
                          DataCell(Text(e.completedTasksCount!.toString(),
                              softWrap: true, style: projectAddedByStyle.copyWith(color: buttonBg), textAlign: TextAlign.center)),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Future<void> showCalendar(TextEditingController textController) async {
    DateTime? tempDate;
    if (fromDateController.text != "") {
      tempDate = new DateFormat("dd-MM-yyyy").parse(fromDateController.text);
      if (textController == fromDateController) {
        tempDate = null;
      }
    }
    final DateTime? picked =
        await showDatePicker(context: context, initialDate: initialDate!, firstDate: tempDate != null ? tempDate : pastDate!, lastDate: initialDate!);
    if (picked != null) {
      setState(() {
        var inputFormat = DateFormat('yyyy-MM-dd');
        // removing time from date
        var inputDate = inputFormat.parse(picked.toLocal().toString().split(' ')[0]);
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      });
    }
  }
}
