import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/model/team_scrum_response_model.dart';
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/scrum/ui/employee_scrum_fragment.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/model/task_list_model.dart' as taskList;

import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TeamScrumFragment extends StatefulWidget {
  final String? employeeId;

  const TeamScrumFragment({Key? key, required this.employeeId}) : super(key: key);

  @override
  _TeamScrumFragmentState createState() => _TeamScrumFragmentState();
}

class _TeamScrumFragmentState extends State<TeamScrumFragment> {
  late ScrumNotifier viewModel;
  String employeeId = "";
  bool viewMyScrum = true;
  int selectedButton = 0;
  late ScrumNotifier scrumModel = ScrumNotifier();
  late ProjectNotifier projectModel = ProjectNotifier();
  late ProjectDetailsNotifier viewModelforModule = ProjectDetailsNotifier();
  final isSelected = <bool>[true, false, false, false];
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  DateTime? initialDate, pastDate;
  List<ScrumData> scrumDataList = [];
  String selectedType = 'today';
  final employeeKey = GlobalKey<FormFieldState>();
  final projectKey = GlobalKey<FormState>();
  final moduleKey = GlobalKey<FormState>();
  final taskKey = GlobalKey<FormState>();
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems = [];
  EmployeeDetails employeeIntialValue = EmployeeDetails(name: selectEmployee);
  bool otherSelected = false;
  List<ProjectList> projectList = [], selectedProject = [];
  List<DropdownMenuItem<ProjectList>> projectMenuItems = [];
  ProjectList projectIntialValue = ProjectList(name: selectProject);

  List<ModuleDetails> moduleDetails = [];
  List<DropdownMenuItem<ModuleDetails>> moduleMenuItems = [];
  ModuleDetails moduleIntialValue = ModuleDetails(name: selectModule);

  List<taskList.TasksData> tasksList = [];
  List<DropdownMenuItem<taskList.TasksData>> taskMenuItems = [];
  taskList.TasksData taskInitialValue = taskList.TasksData(name: selectTask);
  taskList.TasksData taskotherValue = taskList.TasksData(name: other);

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    if (widget.employeeId! == "") {
      employeeId = userViewModel.employeeId!;
      viewMyScrum = true;
    } else {
      employeeId = widget.employeeId!;
      viewMyScrum = false;
    }
    viewModel = Provider.of<ScrumNotifier>(context, listen: false);

    initialDate = DateTime.now();
    // allowing last two months date
    pastDate = DateTime(initialDate!.year, initialDate!.month - 2, initialDate!.day);
    getData();
    super.initState();
  }

  void getData() {
    Future.delayed(Duration.zero, () async {
      /* await employeeViewModel.getEmployeesAPI().then((value) {
        if (value!.employeeDetails!.isNotEmpty) {
          employeesList = value.employeeDetails!;
        }
      });*/
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });

      print("selected --- ${selectedButton}");
      if (selectedButton == 0) {
        selectedType = "today";
      } else if (selectedButton == 2) {
        selectedType = "monthly";
      } else if (selectedButton == 3) {
        selectedType = "yearly";
      } else if (selectedButton == 4) {
        selectedType = "custom";
      }
      await viewModel.getTeamScrumCountapi(employeeId, selectedType, fromDateController.text, toDateController.text).then((value) {
        Navigator.pop(context);
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              scrumDataList.clear();
              if (value.scrumData!.isNotEmpty) {
                scrumDataList = value.scrumData!;
              }
            });
          }
        }
        // hasTechnologies = true;
      });


    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuScrum", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      scrumDataList.length != 0
                          ? Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 15, 0, 5),
                                child: SizedBox(height: ScreenConfig.height(context), width: ScreenConfig.width(context), child: body()),
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.only(top: 200),
                              child: Center(
                                child: Text(noDataAvailable, style: logoutHeader),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text("Employee $navMenuScrum", maxLines: 2, softWrap: true, style: fragmentHeaderStyle),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: fromDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(labelText: "From Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(fromDateController);
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: toDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(labelText: "To Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(toDateController);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            selectedButton = 4;
                            getData();
                          });
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            searchGo,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: dropDownColor,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.resolveWith((states) => searchGoButtonBg),
                          overlayColor: MaterialStateProperty.resolveWith((states) => dropDownColor.withOpacity(0.1)),
                          elevation: MaterialStateProperty.resolveWith((states) => 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.end,
                  spacing: 5,
                  runSpacing: 5,
                  children: [
                    _customRadioButton(today, 0, buttonBg),
                    // _customRadioButton(daily, 1, buttonBg),
                    _customRadioButton(monthly, 2, newOnGoing),
                    _customRadioButton(yearly, 3, buttonraisebug),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget body() {
    return ListView.separated(
      padding: const EdgeInsets.only(left: 35, right: 35),
      physics: const ScrollPhysics(),
      controller: ScrollController(),
      itemCount: scrumDataList.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(scrumDataList[index].heading!, softWrap: true, style: projectAddedByStyle.copyWith(color: buttonBg), textAlign: TextAlign.center),
            sprintsSubList(index, scrumDataList[index].tasksCount!, scrumDataList[index].heading!),
          ],
        );
      },
      separatorBuilder: (context, index) {
        return const Divider(
          color: Colors.transparent,
          height: 10,
        );
      },
    );
  }



  Widget sprintsSubList(int position, List<TasksCount> data, String date) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable2(
                  columnSpacing: (MediaQuery.of(context).size.width / 10) * 0.5,
                  dividerThickness: 1,
                  decoration: BoxDecoration(border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  columns: <DataColumn>[
                    DataColumn2(
                      size: ColumnSize.S,
                      label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text(employee + " " + name, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text("# of Tasks", softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                    ),
                    DataColumn2(
                      label: Text(projectNew, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Text(inProgress, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    ),
                    DataColumn2(
                      label: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(projectCompleted, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                          /*Image.asset('assets/images/edit_icon.png',
                              width: 12, height: 12, color: CupertinoColors.black),*/
                          selectedButton == 0 || selectedButton == 4
                              ? InkWell(
                                  onTap: () {
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        PageRouteBuilder(
                                            pageBuilder: (context, animation1, animation2) =>
                                                HomeScreen(menu: RouteNames.scrumAddRoute, scrumDate: date),
                                            transitionDuration: const Duration(seconds: 0),
                                            reverseTransitionDuration: const Duration(seconds: 0)),
                                        (route) => false);
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    size: 18,
                                    color: Colors.black,
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ],
                  rows: data.map<DataRow>((e) {
                    return DataRow2(
                        specificRowHeight: 60,
                        /*color: MaterialStateColor.resolveWith(
                                (states) => data.!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1 CupertinoColors.white),*/
                        onTap: () {
                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => EmployeeScrumFragment(
                                employeeId: e.employeeId,
                                employeeName: e.employeeName,
                              ),
                              transitionDuration: const Duration(seconds: 0),
                              reverseTransitionDuration: const Duration(seconds: 0),
                            ),
                          ).then((value) {
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        },
                        cells: [
                          DataCell(Text((data.indexOf(e) + 1).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
                          DataCell(Text(e.employeeName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.totalTasksCount!.toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.newTasksCount!.toString(), softWrap: true, style: projectAddedByStyle, textAlign: TextAlign.center)),
                          DataCell(Text(e.pendingTasksCount!.toString(),
                              softWrap: true, style: projectAddedByStyle.copyWith(color: newOnGoing), textAlign: TextAlign.center)),
                          DataCell(Text(e.completedTasksCount!.toString(),
                              softWrap: true, style: projectAddedByStyle.copyWith(color: buttonBg), textAlign: TextAlign.center)),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget _customRadioButton(String text, int index, Color border) {
    return OutlinedButton(
      onPressed: () {
        setState(() {
          selectedButton = index;
          print("selectedButton -- ${selectedButton}");


          getData();
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          text,
          style: TextStyle(
            letterSpacing: 0.9,
            fontWeight: FontWeight.w600,
            color: (selectedButton == index) ? Colors.white : border,
          ),
        ),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith((states) => (selectedButton == index) ? border : Colors.white),
        overlayColor: MaterialStateProperty.resolveWith((states) => border.withOpacity(0.1)),
        side: MaterialStateProperty.resolveWith((states) => BorderSide(color: border)),
        elevation: MaterialStateProperty.resolveWith((states) => (selectedButton == index) ? 2 : 1),
      ),
    );
  }

  Future<void> showCalendar(TextEditingController textController) async {
    DateTime? tempDate;
    if (fromDateController.text != "") {
      tempDate = DateFormat("dd-MM-yyyy").parse(fromDateController.text);
      if (textController == fromDateController) {
        tempDate = null;
      }
    }
    final DateTime? picked =
        await showDatePicker(context: context, initialDate: initialDate!, firstDate: tempDate != null ? tempDate : pastDate!, lastDate: initialDate!);
    if (picked != null) {
      setState(() {
        var inputFormat = DateFormat('yyyy-MM-dd');
        // removing time from date
        var inputDate = inputFormat.parse(picked.toLocal().toString().split(' ')[0]);
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      });
    }
  }
}
