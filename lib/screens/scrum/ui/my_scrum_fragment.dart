import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/model/my_scrum_response_model.dart' as my_scrum;
import 'package:aem/model/projectwise_bugcount_response_model.dart';
import 'package:aem/model/scrum_counts_response.dart' as my_scrum_counts;
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/bugs_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/view_project_screen.dart';
import 'package:aem/model/task_list_model.dart' as taskList;
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/employee_scrum_report_widget.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:aem/widgets/scrum_status_widget.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
class MyScrumFragment extends StatefulWidget {
  final String? employeeId;
  const MyScrumFragment({Key? key, required this.employeeId}) : super(key: key);
  @override
  _MyScrumFragmentState createState() => _MyScrumFragmentState();
}
class _MyScrumFragmentState extends State<MyScrumFragment> {
  late ProjectNotifier viewModel;
  bool otherSelected = false;
  String employeeId = "", _startDate = "", _endDate = "", _year = "";
  late ScrumNotifier scrumModel = ScrumNotifier();
  late ProjectNotifier projectModel = ProjectNotifier();
  bool viewMyScrum = true,
      isTableView = true,
      isApiRunning = true,
      isPopupOpened = false,
      isSuperiorEmployee = false,
      addTaskPermission = false,
      closeday = false;
  late ProjectDetailsNotifier viewModelforModule = ProjectDetailsNotifier();
  final employeeKey = GlobalKey<FormFieldState>();
  final projectKey = GlobalKey<FormState>();
  final moduleKey = GlobalKey<FormState>();
  final taskKey = GlobalKey<FormState>();
  List<ProjectList> projectList = [], selectedProject = [];
  List<DropdownMenuItem<ProjectList>> projectMenuItems = [];
  ProjectList projectIntialValue = ProjectList(name: selectProject);

  List<ModuleDetails> moduleDetails = [];
  List<DropdownMenuItem<ModuleDetails>> moduleMenuItems = [];
  ModuleDetails moduleIntialValue = ModuleDetails(name: selectModule);

  List<taskList.TasksData> tasksList = [];
  List<DropdownMenuItem<taskList.TasksData>> taskMenuItems = [];
  taskList.TasksData taskInitialValue = taskList.TasksData(name: selectTask);
  taskList.TasksData taskotherValue = taskList.TasksData(name: other);
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems = [];
  EmployeeDetails employeeIntialValue = EmployeeDetails(name: selectEmployee);
  int selectedButton = 0, bugsCount = 0;
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  ScrollController listScrollController = ScrollController();
  ScrollController gridScrollController = ScrollController();
  ScrollController tableScrollController = ScrollController();
  ScrollController table2ScrollController = ScrollController();
  ScrollController singleChildScrollController = ScrollController();
  DateTime? initialDate, pastDate;
  TextEditingController taskCommentController = TextEditingController();
  TextEditingController holdCommentController = TextEditingController();
  TextEditingController holdPercentageController = TextEditingController();
  bool bug_view_hide = false;
  List<ProjectsList> bugCountList = [];
  int bugsTotalCount = 0;
  @override
  void initState() {
    viewModel = Provider.of<ProjectNotifier>(context, listen: false);
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    isSuperiorEmployee = userViewModel.isSuperiorEmployee!;
    addTaskPermission = userViewModel.scrumAddTaskPermission;


    print("addTaskPermission ${addTaskPermission}");

    if (widget.employeeId! == "") {
      employeeId = userViewModel.employeeId!;
      viewMyScrum = true;
    } else {
      employeeId = widget.employeeId!;
      viewMyScrum = false;
    }
    initialDate = DateTime.now();
    // allowing only last two months
    pastDate =
        DateTime(initialDate!.year, initialDate!.month - 2, initialDate!.day);
    _startDate = getTodayDateDDMMYYYY();
    _endDate = _startDate;
    getMyScrums();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuScrum",
              maxLines: 1,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 15, 10, 5),
                          child: SizedBox(
                              height: ScreenConfig.height(context),
                              width: ScreenConfig.width(context),
                              child: body()),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                    viewMyScrum ? "My $navMenuScrum" : "Employee $navMenuScrum",
                    maxLines: 2,
                    softWrap: true,
                    style: fragmentHeaderStyle),
              ),
              Row(
                children: [
                  addTaskPermission ? Container(
                    decoration: buttonBorderGray,
                    margin: const EdgeInsets.fromLTRB(10, 10, 20, 10),
                    child: TextButton(
                      onPressed: () {
                        showAddTaskDialog();

                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text('+ ' + addNewTask, style: cancelButtonStyle),
                        ),
                      ),
                    ),
                  ) : SizedBox(),
                  selectedButton == 0 ? closeday ?  SizedBox(): Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: TextButton(
                      onPressed: () {
                        var scrumList =
                            Provider.of<ScrumNotifier>(context, listen: false).getMyScrumsList;
                        if(scrumList.length == 0){
                          showToast('No scrum today.');
                        }else{
                          showTaskCommentDialog('','closeDay');
                        }
                      },
                      child: const Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          closeDay,
                          style: TextStyle(
                            letterSpacing: 0.9,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                            newCompleted),
                        overlayColor: MaterialStateProperty.all(
                            dashBoardCardOverDueText.withOpacity(0.1)),
                        elevation: MaterialStateProperty.all(1),
                      ),
                    ),
                  )  : SizedBox()
                ],
              ),

            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: fromDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(
                              labelText: "From Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(fromDateController);
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: toDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(
                              labelText: "To Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(toDateController);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            _startDate = fromDateController.text.trim();
                            _endDate = toDateController.text.trim();
                            if (_startDate == "" || _endDate == "") {
                              showToast("Both From and To dates are required.");
                            } else {
                              getMyScrums();
                            }
                          });
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            searchGo,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: dropDownColor,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(searchGoButtonBg),
                          overlayColor: MaterialStateProperty.all(
                              dropDownColor.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ),
                    /*(fromDateController.text.trim() != "" || toDateController.text.trim() != "")
                        ? */
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          resetTextFields();
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            reset,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              dashBoardCardOverDueText),
                          overlayColor: MaterialStateProperty.all(
                              dashBoardCardOverDueText.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ) /*: const SizedBox()*/,
                  ],
                ),
              ),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.end,
                  spacing: 5,
                  runSpacing: 5,
                  children: [
                    _customOutlinedRadioButton(today, 0, buttonBg),
                    _customOutlinedRadioButton(daily, 1, buttonBg),
                    _customOutlinedRadioButton(monthly, 2, newOnGoing),
                    _customOutlinedRadioButton(yearly, 3, buttonraisebug),
                  ],
                ),
              ),
            ],
          ),
          isSuperiorEmployee ?  Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                OutlinedButton(
                  onPressed: () {
                    showWorkingHoursDialog();
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(8),
                    child: Text(
                      empScrumReport,
                      style: TextStyle(
                        letterSpacing: 0.9,
                        fontWeight: FontWeight.w600,
                        color: editText,
                      ),
                    ),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(tabsborder),
                    side: MaterialStateProperty.all(
                        const BorderSide(color: tabsborder)),
                    elevation: MaterialStateProperty.all(2),
                  ),
                ),
                Container(margin: EdgeInsets.only(left:25),width: 500,
                  child: OutlinedButton(
                    onPressed: () {
                      showMemberMenu();
                      if(bug_view_hide){
                        setState(() {
                          bug_view_hide = false;
                        });
                      }else{
                        setState(() {
                          bug_view_hide = true;

                        });
                      }

                    },
                    child:  Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,children: [ Padding(
                      padding: EdgeInsets.all(8),
                      child: Text(
                        "$bugs ($bugsTotalCount)",
                        style: TextStyle(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w600,
                          color: editText,
                        ),
                      ),
                    ),Icon(bug_view_hide==true?Icons.keyboard_arrow_down_outlined:Icons.keyboard_arrow_up_outlined, color: buttonBg, size: 24)],

                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(tabsborder),
                      side: MaterialStateProperty.all(const BorderSide(color: tabsborder)),
                      elevation: MaterialStateProperty.all(2),
                    ),
                  ),
                )
                /*Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: PopupMenuButton<int>(
                      offset: const Offset(0, 30),
                      shape: const RoundedRectangleBorder(
                        side: BorderSide(color: tabsborder),
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                      elevation: 2,
                      tooltip: "",
                      onCanceled: () {
                        setState(() {
                          isPopupOpened = false;
                        });
                        debugPrint("PopupMenuButton onCanceled");
                      },
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                          color: tabsborder,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "$bugs ($bugsCount)",
                              style: const TextStyle(
                                letterSpacing: 0.9,
                                fontWeight: FontWeight.w600,
                                color: editText,
                              ),
                            ),
                            const Icon(Icons.keyboard_arrow_down, color: buttonBg)
                          ],
                        ),
                      ),
                      onSelected: (value) => onSelectedItem(context, value),
                      itemBuilder: (context) => [
                            PopupMenuItem(padding: const EdgeInsets.fromLTRB(10, 5, 10, 5), child: Text(myProfile, style: popMenuHeader), value: 0),
                            PopupMenuItem(padding: const EdgeInsets.fromLTRB(10, 5, 10, 5), child: Text(logout, style: popMenuHeader), value: 1),
                          ]),
                ),*/
              ],
            ),
          ) : SizedBox(),
        ],
      ),
    );
  }
  showAddTaskDialog() {
    showDialog(
      context: context,
      builder: (context) {
        final _formKey = GlobalKey<FormState>();
        TextEditingController taskController = TextEditingController();
        TextEditingController taskdesController = TextEditingController();
        TextEditingController estimationController = TextEditingController();
        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(addNewTask, style: profilepopHeader),
                  ),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();

                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                width: ScreenConfig.width(context) / 3,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: dashBoardCardBorderTile)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: Text(employee, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: DropdownButtonFormField<EmployeeDetails>(
                                            key: employeeKey,
                                            items: employeeMenuItems,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: employee),
                                            style: textFieldStyle,
                                            //  value: employeeIntialValue,
                                            // validator: employeeDropDownValidatorTask,
                                            autovalidateMode: AutovalidateMode.onUserInteraction,
                                            onChanged: (EmployeeDetails? value) {
                                              setState(() {
                                                employeeIntialValue = value!;

                                              });
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: Text(project, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: DropdownButtonFormField<ProjectList>(
                                            key: projectKey,
                                            isExpanded: true,
                                            items: projectMenuItems,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: project),
                                            style: textFieldStyle,
                                            // value: projectIntialValue,
                                            //validator: employeeDropDownValidatorTask,
                                            autovalidateMode: AutovalidateMode.onUserInteraction,
                                            onChanged: (ProjectList? value) {
                                              assignState(() {
                                                projectIntialValue = value!;
                                                // getModulesata(projectIntialValue.id!);
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: false,
                                                    builder: (BuildContext context) {
                                                      return loader();
                                                    });
                                                moduleMenuItems = [];
                                                moduleMenuItems.add(DropdownMenuItem(child: const Text(selectModule), value: moduleIntialValue));
                                                viewModelforModule.viewAllTodosAPI(value.id!, employeeId).then((value) {
                                                  Navigator.of(context).pop();
                                                  if (value != null) {
                                                    if (value.responseStatus == 1) {
                                                      // setState(() {
                                                      if (value.moduleDetails!.isNotEmpty) {
                                                        moduleDetails = value.moduleDetails!;
                                                        for (var element in value.moduleDetails!) {
                                                          moduleMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                                                          print("moduleMenuItems -- ${element.name!}");
                                                        }
                                                      }
                                                      // });
                                                    } else {
                                                      moduleDetails = [];
                                                      // showToast(value.result!);
                                                    }
                                                  } else {
                                                    moduleDetails = [];
                                                    // showToast(pleaseTryAgain);
                                                  }
                                                });
                                              });
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: Text(modules, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: DropdownButtonFormField<ModuleDetails>(
                                            key: moduleKey,
                                            items: moduleMenuItems,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: modules),
                                            style: textFieldStyle,
                                            //value: moduleIntialValue,
                                            //validator: employeeDropDownValidatorTask,
                                            autovalidateMode: AutovalidateMode.onUserInteraction,
                                            onChanged: (ModuleDetails? value) {
                                              assignState(() {
                                                moduleIntialValue = value!;
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: false,
                                                    builder: (BuildContext context) {
                                                      return loader();
                                                    });
                                                taskMenuItems = [];
                                                taskMenuItems.add(DropdownMenuItem(child: const Text(selectTask), value: taskInitialValue));
                                                scrumModel.moduleBasedTaskListApi(employeeId, value.id!).then((value) {
                                                  Navigator.of(context).pop();
                                                  if (value != null) {
                                                    if (value.responseStatus == 1) {
                                                      // setState(() {
                                                      if (value.tasksData!.isNotEmpty) {
                                                        tasksList = value.tasksData!;
                                                        for (var element in value.tasksData!) {
                                                          taskMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                                                        }

                                                      }
                                                      taskMenuItems.add(DropdownMenuItem(child: const Text('Others'), value: taskotherValue));
                                                      // });
                                                    } else {
                                                      tasksList = [];
                                                      // showToast(value.result!);
                                                    }
                                                  } else {
                                                    tasksList = [];
                                                    // showToast(pleaseTryAgain);
                                                  }
                                                });
                                              });
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: Text(tasks, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: DropdownButtonFormField<taskList.TasksData>(
                                            key: taskKey,
                                            items: taskMenuItems,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: tasks),
                                            style: textFieldStyle,
                                            //value: taskInitialValue,
                                            // validator: employeeDropDownValidatorTask,
                                            autovalidateMode: AutovalidateMode.onUserInteraction,
                                            onChanged: (taskList.TasksData? value) {
                                              assignState(() {
                                                taskInitialValue = value!;
                                                print("sel ${taskInitialValue.name}");
                                                if (taskInitialValue.name == "Other") {
                                                  otherSelected = true;
                                                } else {
                                                  otherSelected = false;
                                                }
                                              });
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                otherSelected
                                    ? Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(flex: 1, child: SizedBox()),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: TextFormField(
                                            maxLines: 1,
                                            controller: taskController,
                                            textInputAction: TextInputAction.done,
                                            validator: emptyTextValidator,
                                            obscureText: false,
                                            decoration: editTextProjectDescriptionDecoration.copyWith(hintText: taskNameStr),
                                            style: textFieldStyle,
                                            // validator: emptyTextValidator,
                                            // autovalidateMode: AutovalidateMode.onUserInteraction
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                                    : SizedBox(),
                                otherSelected
                                    ? Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(flex: 1, child: SizedBox()),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: TextFormField(
                                            maxLines: 3,
                                            controller: taskdesController,
                                            textInputAction: TextInputAction.done,
                                            validator: emptyTextValidator,
                                            obscureText: false,
                                            decoration: editTextProjectDescriptionDecoration.copyWith(hintText: description),
                                            style: textFieldStyle,
                                            // validator: emptyTextValidator,
                                            // autovalidateMode: AutovalidateMode.onUserInteraction
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                                    : SizedBox(),
                                otherSelected
                                    ? Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(flex: 1, child: SizedBox()),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: TextFormField(
                                            maxLines: 1,
                                            controller: estimationController,
                                            textInputAction: TextInputAction.done,
                                            validator: emptyTextValidator,
                                            obscureText: false,
                                            decoration: editTextProjectDescriptionDecoration.copyWith(hintText: estimation),
                                            style: textFieldStyle,
                                            // validator: emptyTextValidator,
                                            // autovalidateMode: AutovalidateMode.onUserInteraction
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                                    : SizedBox(),
                                Container(
                                  alignment: Alignment.center,
                                  decoration: buttonDecorationGreen,
                                  width: 120,
                                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: TextButton(
                                    onPressed: () async {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return loader();
                                          });
                                      var now = DateTime.now();
                                      var formatter = DateFormat('dd-MM-yyyy');
                                      String formattedDate = formatter.format(now);
                                      scrumModel
                                          .addNewtaskToScrum(
                                          employeeId,
                                          employeeIntialValue.id!,
                                          projectIntialValue.id!,
                                          moduleIntialValue.id!,
                                          !otherSelected ? taskInitialValue.id! : '',
                                          !otherSelected ? 'old' : 'new',
                                          formattedDate,
                                          taskController.text,
                                          taskdesController.text,
                                          estimationController.text)
                                          .then((value) {
                                        Navigator.of(context).pop();
                                        if (value != null) {
                                          if (value.responseStatus == 1) {
                                            // setState(() {
                                            showToast(value.result!);
                                            Navigator.of(context).pop();
                                            getMyScrums();
                                            assignState(() {
                                              // resetting filter
                                              otherSelected = false;
                                              employeeKey.currentState!.reset();
                                              projectKey.currentState!.reset();
                                              moduleKey.currentState!.reset();
                                              taskKey.currentState!.reset();
                                            });

                                            // });
                                          } else {
                                            tasksList = [];
                                            Navigator.of(context).pop();
                                            showToast(value.result!);
                                          }
                                        } else {
                                          tasksList = [];
                                          Navigator.of(context).pop();
                                          showToast(pleaseTryAgain);
                                        }
                                      });
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Text(submit.toUpperCase(), style: newButtonsStyle),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
  Widget body() {
    var scrumList =
        Provider.of<ScrumNotifier>(context, listen: true).getMyScrumsList;

    if(selectedButton == 0){


      for (var element in scrumList) {
        print("comment ${scrumList[0].comment}");
        setState(() {
          if(scrumList[0].comment == ''){
            closeday = false;
          }else{
            closeday = true;
          }
        });

      }







    }
    //var scrumothersList = Provider.of<ScrumNotifier>(context, listen: true).getMyScrumsOthersList;
    var scrumCountList =
        Provider.of<ScrumNotifier>(context, listen: true).getMyScrumCountsList;
    return isTableView
        ? scrumList.isNotEmpty
            ? SingleChildScrollView(
                controller: singleChildScrollController,
                child: Column(
                  children: [
                    ListView.separated(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      physics: const ScrollPhysics(),
                      controller: listScrollController,
                      itemCount: scrumList.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {




                        return listItems(scrumList[index]);
                      },
                      separatorBuilder: (context, index) {
                        return const Divider(
                          color: Colors.transparent,
                          height: 10,
                        );
                      },
                    ),


                  ],
                ),
              )
            : SizedBox(
                height: ScreenConfig.height(context),
                width: ScreenConfig.width(context),
                child: Center(
                  child: Text(isApiRunning ? "" : noDataAvailable,
                      style: logoutHeader),
                ),
              )
        : scrumCountList.isNotEmpty
            ? GridView.builder(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                controller: gridScrollController,
                physics: const ScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: scrumCountList.length,
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    childAspectRatio: 2.35,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                    maxCrossAxisExtent: 450),
                itemBuilder: (BuildContext context, int index) {
                  return countsListItems(scrumCountList[index]);
                })
            : SizedBox(
                height: ScreenConfig.height(context),
                width: ScreenConfig.width(context),
                child: Center(
                  child: Text(isApiRunning ? "" : noDataAvailable,
                      style: logoutHeader),
                ),
              );
  }
  Widget countsListItems(my_scrum_counts.ScrumData scrumData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: isHovered ? 2 : 1,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
              side:
                  const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              if (getScrumFilterType() == daily) {
                setState(() {
                  _startDate = scrumData.heading!;
                  _endDate = scrumData.heading!;
                  getMyScrums();
                });
              } else if (getScrumFilterType() == monthly) {
                setState(() {
                  DateTime tempDate = DateFormat("MMMM yyyy")
                      .parse(scrumData.heading!, true)
                      .toLocal();
                  var monthFormat = DateFormat('MM'); // get month
                  var yearFormat = DateFormat('yyyy'); // get year
                  var _month = monthFormat.format(tempDate);
                  var _year = yearFormat.format(tempDate);
                  _startDate = "01-$_month-$_year"; // start date is 01
                  DateTime _lastDayOfMonth = DateTime(
                      int.parse(_year),
                      int.parse(_month) + 1,
                      0); // end date is calculated as total number of days
                  _endDate = "${_lastDayOfMonth.day}-$_month-$_year";
                  getMyScrums();
                });
              } else if (getScrumFilterType() == yearly) {
                setState(() {
                  _startDate = "";
                  _endDate = "";
                  _year = scrumData.heading!;
                  print('selected year -- ${_year}');
                  getMyYearScrums();
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      scrumData.heading!,
                      maxLines: 2,
                      softWrap: true,
                      style: fragmentHeaderStyle.copyWith(
                        color: isHovered ? Colors.white : editText,
                        fontSize: 16,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              height: ScreenConfig.height(context),
                              width: ScreenConfig.width(context),
                              margin: const EdgeInsets.fromLTRB(10, 10, 5, 10),
                              color: Colors.transparent,
                              child: Card(
                                elevation: 2,
                                margin: const EdgeInsets.all(0),
                                color: CupertinoColors.white,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      newStr,
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: buttonBg,
                                        fontSize: 14,
                                        letterSpacing: 0.25,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Text(
                                      scrumData.tasksCount!.newTasksCount!
                                          .toString(),
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: editText,
                                        fontSize: 18,
                                        letterSpacing: 1.45,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: ScreenConfig.height(context),
                              width: ScreenConfig.width(context),
                              margin: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                              color: Colors.transparent,
                              child: Card(
                                elevation: 2,
                                margin: const EdgeInsets.all(0),
                                color: CupertinoColors.white,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      pending,
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: newOnGoing,
                                        fontSize: 14,
                                        letterSpacing: 0.25,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Text(
                                      scrumData.tasksCount!.pendingTasksCount!
                                          .toString(),
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: editText,
                                        fontSize: 18,
                                        letterSpacing: 1.45,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: ScreenConfig.height(context),
                              width: ScreenConfig.width(context),
                              margin: const EdgeInsets.fromLTRB(5, 10, 10, 10),
                              color: Colors.transparent,
                              child: Card(
                                elevation: 2,
                                margin: const EdgeInsets.all(0),
                                color: CupertinoColors.white,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      complete,
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: buttonraisebug,
                                        fontSize: 14,
                                        letterSpacing: 0.25,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Text(
                                      scrumData.tasksCount!.completedTasksCount!
                                          .toString(),
                                      maxLines: 2,
                                      softWrap: true,
                                      style: fragmentHeaderStyle.copyWith(
                                        color: editText,
                                        fontSize: 18,
                                        letterSpacing: 1.45,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ]),
            ),
          ),
        ),
      );
    });
  }
  Widget listItems(my_scrum.ScrumData scrumData) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 7),
          child: Text(
            scrumData.heading!,
            maxLines: 2,
            softWrap: true,
            style: fragmentHeaderStyle.copyWith(
              color: buttonBg,
              fontSize: 16,
              letterSpacing: 1.25,
            ),
          ),
        ),
        DataTable2(
          columnSpacing: 14,
          dividerThickness: 1,
          scrollController: tableScrollController,
          decoration: BoxDecoration(
              border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
          headingRowHeight: 46,
          dataRowHeight: 60,
          smRatio: 0.4,
          lmRatio: 1.5,
          columns: tableColumnHeader(),
          rows: tableRows(scrumData.tasksList! + scrumData.othertasksList!),
          empty: Center(
            child: Text(noDataAvailable, style: logoutHeader),
          ),
        ),
        /*Padding(
          padding: const EdgeInsets.only(bottom: 7),
          child: Text(
            'Other Tasks',
            maxLines: 2,
            softWrap: true,
            style: fragmentHeaderStyle.copyWith(
              color: buttonBg,
              fontSize: 16,
              letterSpacing: 1.25,
            ),
          ),
        ),
        DataTable2(
          columnSpacing: 14,
          dividerThickness: 1,
          scrollController: table2ScrollController,
          decoration: BoxDecoration(
              border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
          headingRowHeight: 46,
          dataRowHeight: 60,
          smRatio: 0.4,
          lmRatio: 1.5,
          columns: tableColumnHeader(),
          rows: tableRows(scrumData.othertasksList!),
          empty: Center(
            child: Text(noDataAvailable, style: logoutHeader),
          ),
        ),*/
        scrumData.comment != "" ? Padding(
          padding: const EdgeInsets.only(bottom: 7,top: 7),
          child: RichText(
            textAlign: TextAlign.start,
            text: TextSpan(children: <TextSpan>[
              TextSpan(
                text: closeDayReason,
                style: logoutContentHeader,

              ),
              TextSpan(text: scrumData.comment, style: listItemsStyle) ,
            ]),
          ),
        ) : SizedBox(),

      ],
    );
  }
  /*Widget listItemsothers(my_scrum.OtherTaskScrumData scrumData) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 7),
          child: Text(
            scrumData.heading!,
            maxLines: 2,
            softWrap: true,
            style: fragmentHeaderStyle.copyWith(
              color: buttonBg,
              fontSize: 16,
              letterSpacing: 1.25,
            ),
          ),
        ),
        DataTable2(
          columnSpacing: 14,
          dividerThickness: 1,
          scrollController: tableScrollController,
          decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
          headingRowHeight: 46,
          dataRowHeight: 60,
          smRatio: 0.4,
          lmRatio: 1.5,
          columns: tableColumnHeader(),
          rows: tableRows(scrumData.tasksList!),
          empty: Center(
            child: Text(noDataAvailable, style: logoutHeader),
          ),
        )
      ],
    );
  }*/
  List<DataColumn2> tableColumnHeader() {
    return <DataColumn2>[
      DataColumn2(
          label: Text(tableSNo,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.S),
      DataColumn2(
          label: Text(taskNameStr,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(tableModuleName,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(projectName,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(bugName,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(tableStatus,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(estimationTimeScrum,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Text(taskTimeScrum,
              softWrap: true,
              style: listHeaderStyle,
              textAlign: TextAlign.start),
          size: ColumnSize.M),
      DataColumn2(
          label: Center(
            child: Text(tableActions,
                softWrap: true,
                style: listHeaderStyle,
                textAlign: TextAlign.start),
          ),
          size: ColumnSize.L),
    ];
  }
  List<DataRow> tableRows(List<my_scrum.TasksList> scrums) {
    /* actions -
    * task type = "task" show buttons
    * 1 (new)- start
    * 2 (open)- hold, complete
    * 3 (completed)- reopen, close
    * 4 (close) - empty
    * 5 (reopen)- start
    * 6 (delete) - empty
    * 7 (hold)- start, complete */
    String bugStatusInitialValue = "Status";


    return scrums.map<DataRow2>((e) {

      print('task type -- ${e.taskAddType}');
      for (var status in bugStatuses) {
        if (status == e.bugStatus!) {
          bugStatusInitialValue = status;
        }
      }
      return DataRow2(
          onTap: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) =>
                    ViewTaskScreen(
                        projectId: e.projectId!,
                        moduleId: e.moduleId!,
                        taskId: e.taskId!),
                transitionDuration: const Duration(seconds: 0),
                reverseTransitionDuration: const Duration(seconds: 0),
              ),
            ).then((value) {
              getMyScrums();
            });
          },
          color:
              MaterialStateColor.resolveWith((states) => e.taskAddType == "other" ? filterBg : CupertinoColors.white),
          specificRowHeight: 60,
          cells: [
            DataCell(Text((scrums.indexOf(e) + 1).toString(),
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(Text(e.taskName!,
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(Text(e.moduleName!,
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(Text(e.projectName!,
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(Text(e.bugTitle!,
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(
              e.taskType == "task"
                  ? e.taskStatus! == 7 ? _holdStatusButton(e.percentageComplete!,e.holdComment!): getTextFromScrumStatusWidget(e.taskStatus!)
                  : Text(e.bugStatus!.toString(),
                      softWrap: true,
                      style:
                          listItemsStyle.copyWith(fontWeight: FontWeight.w700),
                      textAlign: TextAlign.start),
            ),
            DataCell(Text(minutesToHrs(e.estimationTime!).toString(),
                softWrap: true,
                style: listItemsStyle,
                textAlign: TextAlign.start)),
            DataCell(
                Row(
              children: [
                Text(
                    minutesToHrs(Duration(seconds: e.involvedTime!).inMinutes)
                        .toString(),
                    softWrap: true,
                    style: listItemsStyle.copyWith(
                        color: checkEstimationTime(
                            e.involvedTime!, e.estimationTime!)
                    ),
                    textAlign: TextAlign.start),
                !checkTimeforInformation(e.involvedTime!, e.estimationTime!) ? Padding(
                  padding: const EdgeInsets.only(left: 3),
                  child: Tooltip(
                    message: e.closeComment!,
                    decoration: toolTipDecoration,
                    textStyle: toolTipStyle,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Icon(Icons.info_outline, color: timeText),
                  ),
                ) : SizedBox(),
              ],
            )),
            DataCell(
              e.taskType == "task"
                  ? Center(
                      child: Wrap(
                        runAlignment: WrapAlignment.center,
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          if (e.taskStatus! == 1) ...[
                            _startButton(e),
                          ] else if (e.taskStatus! == 2) ...[
                            _holdButton(e),
                            _completedButton(e)
                          ] else if (e.taskStatus! == 3) ...[
                            _reOpenButton(e),
                           // _closeButton(e),
                          ] else if (e.taskStatus! == 4) ...[
                            const SizedBox(),
                          ] else if (e.taskStatus! == 5) ...[
                            _startButton(e),
                          ] else if (e.taskStatus! == 6) ...[
                            const SizedBox(),
                          ] else if (e.taskStatus! == 7) ...[
                            _startButton(e),
                            _completedButton(e),
                          ]
                        ],
                      ),
                    )
                  : DropdownButtonFormField<String>(
                      items: bugStatuses.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      decoration: editTextStatusDecoration,
                      style: textFieldStyle,
                      value: bugStatusInitialValue,
                      onChanged: (String? value) {
                        setState(() {
                          bugStatusInitialValue = value!;
                          if (bugStatusInitialValue != 'Status') {
                            updateBugStatus(e.bugId!, bugStatusInitialValue);
                          }
                        });
                      },
                    ),
            ),
          ]);
    }).toList();
  }
  Color checkEstimationTime(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return newOverDue;
    } else {
      return newCompleted;
    }
  }
  bool checkTimeforInformation(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return false;
    } else {
      return true;
    }
  }
  Widget _startButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.openStr.name!, '',0);
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            start,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonBg,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(buttonBg.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _closeButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.closeStr.name!, '',0);
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            close,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonBg,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(buttonBg.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  showHoldTaskCommentDialog(String taskId,String status) {
    final _formKey = GlobalKey<FormState>();
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$commentHold', style: newCompletedHeaderStyle),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: 700,
                    maxWidth: 700,
                    minHeight: 9 * 24,
                    maxHeight: 9 * 24),
                child: IntrinsicWidth(
                  child: Column(
                    children: [
                      SizedBox(height: 10,),
                      TextFormField(
                        maxLines: 5,
                        controller: holdCommentController,
                        textInputAction: TextInputAction.done,
                        validator: emptyTextValidator,
                        obscureText: false,
                        decoration: editTextProjectDescriptionDecoration.copyWith(
                            hintText: comment),
                        style: textFieldStyle,
                        // validator: emptyTextValidator,
                        // autovalidateMode: AutovalidateMode.onUserInteraction
                      ),

                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      children: [

                        Text(
                          'Percentage',
                          style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                          textAlign: TextAlign.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 5, 5, 5),
                          child: OnHoverCard(builder: (isHovered) {
                            return Container(
                                width: 120,
                                decoration: BoxDecoration(
                                    color: CupertinoColors.white, borderRadius: BorderRadius.circular(5), border: Border.all(color: editTextHint, width: 1.5)),
                                child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child: TextFormField(
                                      controller: holdPercentageController,
                                      maxLines: 1,
                                      textInputAction: TextInputAction.done,
                                      obscureText: false,
                                      decoration: editTextProjectDescriptionDecoration.copyWith(hintText: ''),
                                      style: textFieldStyle,
                                      // validator: emptyTextValidator,
                                      // autovalidateMode: AutovalidateMode.onUserInteraction
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                    child: Text(
                                      '%',
                                      style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            ));
                          }),
                        ),
                      ],
                    ),
                  ),
                    ],
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          if(holdPercentageController.text != ""){
                            updateTaskStatus(taskId, StatusText.holdStr.name!, holdCommentController.text,int.parse(holdPercentageController.text));
                            Navigator.of(context).pop();
                            holdCommentController.text = "";
                            holdPercentageController.text = "";

                          }else{
                            showToast("enter percentage");
                          }

                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  showTaskCommentDialog(String taskId,String from) {
    final _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: 700,
                    maxWidth: 700,
                    minHeight: 5 * 24,
                    maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    textInputAction: TextInputAction.done,
                    validator: emptyTextValidator,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(
                        hintText: from == 'task' ? reasonExtraTime : commentForcloseDay),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {

                        if (_formKey.currentState!.validate()) {

                          if(from == 'task'){
                            updateTaskStatus(taskId, StatusText.completeStr.name!,
                                taskCommentController.text,0);
                            Navigator.of(context).pop();
                          }else{
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
                            viewModel.closeEmployeeScrumTasksApi(employeeId,taskCommentController.text).then((value) {
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                              if(value!.responseStatus! == 1){
                                showToast(value.result!);
                                getMyScrums();
                                taskCommentController.text = "";
                              }else{
                                showToast(value.result!);
                              }
                            });
                          }



                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  Widget _holdStatusButton(int percent,String comment) {

    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 5, 5, 5),
          child: OnHoverCard(builder: (isHovered) {
            return Container(
              width: 90,
              decoration: BoxDecoration(
                  color: CupertinoColors.white, borderRadius: BorderRadius.circular(0), border: Border.all(color: holdBgColor, width: 1.5)),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: DecoratedBox(
                      decoration: const BoxDecoration(color: holdBgColor),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                        child: Text(
                          hold,
                          style: const TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: CupertinoColors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Text(
                        '${percent} %',
                        style: TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: addRoleText),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),

        Padding(
          padding: const EdgeInsets.only(left: 3),
          child: Tooltip(
            message: comment,
            decoration: toolTipDecoration,
            textStyle: toolTipStyle,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.info_outline, color: timeText),
          ),
        )
      ],
    );

  }
  Widget _holdButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          //updateTaskStatus(e.taskId!, StatusText.holdStr.name!, '');
          showHoldTaskCommentDialog(e.taskId!,StatusText.holdStr.name!);
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            hold,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: newOnGoing,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(newOnGoing.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: newOnGoing)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _reOpenButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.reopenStr.name!, '',0);
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            reOpen,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: newOnGoing,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(newOnGoing.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: newOnGoing)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _completedButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          Future.delayed(Duration.zero, () {
            setState(() {
              isApiRunning = true;
            });
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return loader();
                });
            final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
            viewModel.gettasktimingsApi(employeeId, e.taskId!).then((value) {
              Navigator.of(context).pop();
              if(value!.responseStatus! == 1){
                int involvedMins = Duration(seconds: value.taskTimings!.involvedTime!).inMinutes;

                print("involvedMins -- ${value.taskTimings!.involvedTime!}  estimationTime -- ${value.taskTimings!.estimationTime!}");

                if (value.taskTimings!.estimationTime! < involvedMins) {
                  taskCommentController.text = "";

                  showTaskCommentDialog(e.taskId!,'task');
                } else {
                  updateTaskStatus(e.taskId!, StatusText.completeStr.name!, '',0);
                }
              }
            });

          });
          },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            complete,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonraisebug,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor:
              MaterialStateProperty.all(buttonraisebug.withOpacity(0.1)),
          side: MaterialStateProperty.all(
              const BorderSide(color: buttonraisebug)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _customOutlinedRadioButton(String text, int index, Color border) {
    return OutlinedButton(
      onPressed: () {
        setState(() {
          selectedButton = index;
          resetTextFields();
          if (selectedButton == 0) {
            setState(() {
              _startDate = getTodayDateDDMMYYYY();
              _endDate = _startDate;
            });
            getMyScrums();
          } else {
            getMyScrumCounts();
          }
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          text,
          style: TextStyle(
            letterSpacing: 0.9,
            fontWeight: FontWeight.w600,
            color: (selectedButton == index) ? Colors.white : border,
          ),
        ),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(
            (selectedButton == index) ? border : Colors.white),
        overlayColor: MaterialStateProperty.all(border.withOpacity(0.1)),
        side: MaterialStateProperty.all(BorderSide(color: border)),
        elevation: MaterialStateProperty.all((selectedButton == index) ? 2 : 1),
      ),
    );
  }
  showWorkingHoursDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    Provider.of<ScrumNotifier>(context, listen: false)
        .getEmployeeScrumReportAPI(employeeId, _startDate)
        .then((value) {
      Navigator.of(context).pop();
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return Dialog(
              backgroundColor: Colors.transparent,
              elevation: 10,
              insetPadding:
                  const EdgeInsets.symmetric(horizontal: 200, vertical: 150),
              child: EmployeeScrumReportWidget(
                employeeId: employeeId,
              ),
            );
          });
    });
  }
  onSelectedItem(BuildContext context, int item) {
    switch (item) {
      case 0:
        break;
      case 1:
        break;
    }
  }
  Future<void> showCalendar(TextEditingController textController) async {
    DateTime? tempDate;
    if (fromDateController.text != "") {
      tempDate = DateFormat("dd-MM-yyyy").parse(fromDateController.text);
      if (textController == fromDateController) {
        tempDate = null;
      }
    }
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: initialDate!,
        firstDate: tempDate ?? pastDate!,
        lastDate: initialDate!);
    if (picked != null) {
      setState(() {
        var inputFormat = DateFormat('yyyy-MM-dd');
        // removing time from date
        var inputDate =
            inputFormat.parse(picked.toLocal().toString().split(' ')[0]);
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      });
    }
  }
  String getScrumFilterType() {
    switch (selectedButton) {
      case 0:
        {
          return today;
        }
      case 1:
        {
          return daily;
        }
      case 2:
        {
          return monthly;
        }
      case 3:
        {
          return yearly;
        }
      default:
        return today;
    }
  }
  resetTextFields() {
    _startDate = "";
    _endDate = "";
    fromDateController.clear();
    toDateController.clear();
  }
  getMyScrums() {
    Future.delayed(Duration.zero, () {
      setState(() {
        isApiRunning = true;
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
      viewModel.viewMyScrumsApi(employeeId, _startDate, _endDate).then((value) {
        Navigator.of(context).pop();

        setState(() {
          if(selectedButton == 0){
            for (var element in value!.scrumData!) {
              print("comment ${element.comment}");
              setState(() {
                if(value.scrumData![0].comment == ''){
                  closeday = false;
                }else{
                  closeday = true;
                }
              });
            }
          }
          isTableView = true;
          isApiRunning = false;
        });
      });

      viewModel.viewProjectWiseBugsCountApi(employeeId).then((value) {

        if(value!.responseStatus! == 1){
          bugCountList = value.projectsList!;
          bugsTotalCount = 0;
          for (var element in bugCountList) {

            bugsTotalCount = bugsTotalCount + element.totalBugsCount!;
          }
        }else{
          showToast(value.result!);
        }


      });
      employeeMenuItems = [];
      employeeMenuItems.add(DropdownMenuItem(child: const Text(selectEmployee), value: employeeIntialValue));
       employeeViewModel.inferioremployeeslistApi(employeeId).then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
                for (var element in value.employeeDetails!) {
                  employeeMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
      projectMenuItems = [];
      projectMenuItems.add(DropdownMenuItem(child: const Text(selectProject), value: projectIntialValue));
       projectModel.getAllProjectsAPI(employeeId, 'scrum').then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.projectList!.isNotEmpty) {
                projectList = value.projectList!;
                for (var element in value.projectList!) {
                  projectMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                }
              }
            });
          }
        }
        // hasTechnologies = true;
      });
    });
  }
  void showMemberMenu() async {

    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(200, 265, 50, 100),
      constraints: BoxConstraints(
        minWidth: 500,
        maxWidth: 500,
      ),
      items: List.generate(
        bugCountList.length + 1,
            (index) => PopupMenuItem(
          value: index,
          child: Column(children: [
            index==0?Container(
              child: Row(children: [Expanded(flex:2,
                child: Text(
                  projectName,
                  style: listItemsheaderStyle,
                ),
              ),Expanded(flex:1,
                child: Container(padding: EdgeInsets.only(left: 0,right: 0),
                  child: Text(
                    bug_high,
                    textAlign: TextAlign.center,
                    style: listItemsheaderStyle,
                  ),
                ),
              ),Expanded(flex:1,
                child: Container(
                    child: Text(
                      bug_medium,
                      textAlign: TextAlign.center,
                      style: listItemsheaderStyle,
                    )),
              ),Expanded(flex: 1,
                child: Container(
                    child: Text(
                      bug_low,
                      textAlign: TextAlign.center,
                      style:listItemsheaderStyle,
                    )),
              ),Expanded(flex:1,child: Container(
                  child: Text(
                    bug_total,
                    textAlign: TextAlign.center,
                    style:listItemsheaderStyle,
                  )),)],
              ),
            )
                :Container(
              child: Row(
                children: [
                Expanded(flex:2,
                child: Text(
                  bugCountList[index-1].projectName!,
                  style: listItemsStyle,
                ),
              ),Expanded(flex: 1,
                child: Container(
                  child: Text(
                    bugCountList[index-1].highBugsCount!.toString(),
                    textAlign: TextAlign.center,
                    style: listItemsStyle,
                  ),
                ),
              ),Expanded(flex: 1,
                child: Container(
                    child: Text(
                      bugCountList[index-1].mediumBugsCount!.toString(),
                      textAlign: TextAlign.center,
                      style: listItemsStyle,
                    )),
              ),Expanded(flex:1,
                child: Container(
                    child: Text(
                      bugCountList[index-1].lowBugsCount!.toString(),
                      textAlign: TextAlign.center,
                      style: listItemsStyle,
                    )),
              ),Expanded(flex: 1,
                child: Container(
                  width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                        border: Border.all(
                          color: circleBorder,
                        ),
                       // borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                    child: Center(
                      child: Text(
                        bugCountList[index-1].totalBugsCount!.toString(),
                        textAlign: TextAlign.center,
                        style: listItemsStyle.copyWith(color: newOverDue,fontWeight: FontWeight.w600),
                      ),
                    )),
              ),],

              ),
            ),
          ],
          ),
        ),
      ),
      elevation: 8.0,
    ).then((value) {
      if (value != null){
        print(value);
        if(value > 0){
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => BugsTasksScreen(projectId: bugCountList[value-1].projectId!,projectName: bugCountList[value-1].projectName!,),
              transitionDuration: const Duration(seconds: 0),
              reverseTransitionDuration: const Duration(seconds: 0),
            ),
          ).then((value) {
            // set state to reload data
            setState(() {


            });
          });
        }
      }
    });
  }
  getMyYearScrums() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel.viewMyScrumYearlyApi(employeeId, _year).then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = true;
        isApiRunning = false;
      });
    });
  }
  getMyScrumCounts() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel
        .viewMyScrumCountsApi(employeeId, getScrumFilterType().toLowerCase())
        .then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = false;
        isApiRunning = false;
      });
    });
  }
  updateTaskStatus(String taskId, String status, String comment,int percentage) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel =
        Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel
        .updateTasksBasedOnStatusAPI(employeeId, taskId, status, comment,percentage)
        .then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          taskCommentController.text = "";
          getMyScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }
  updateBugStatus(String bugId, String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel =
        Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateBugStatusAPI(bugId, employeeId, status).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getMyScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }
}
