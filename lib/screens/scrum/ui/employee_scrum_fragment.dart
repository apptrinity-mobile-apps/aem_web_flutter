import 'package:aem/model/my_scrum_response_model.dart' as my_scrum;
import 'package:aem/model/scrum_counts_response.dart' as my_scrum_counts;
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:aem/widgets/scrum_status_widget.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EmployeeScrumFragment extends StatefulWidget {
  final String? employeeId;
  final String? employeeName;

  const EmployeeScrumFragment({Key? key, this.employeeId, this.employeeName}) : super(key: key);

  @override
  State<EmployeeScrumFragment> createState() => _EmployeeScrumFragmentState();
}
class _EmployeeScrumFragmentState extends State<EmployeeScrumFragment> {
  late ProjectNotifier viewModel;
  String employeeId = "", _startDate = "", _endDate = "", _year = "";
  bool viewMyScrum = true, isTableView = true, isApiRunning = true;
  int selectedButton = 0;
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  ScrollController listScrollController = ScrollController();
  ScrollController gridScrollController = ScrollController();
  ScrollController tableScrollController = ScrollController();
  ScrollController table2ScrollController = ScrollController();
  DateTime? initialDate, pastDate;
  bool? isEmployee = false;
  TextEditingController taskCommentController = TextEditingController();
  @override
  void initState() {
    viewModel = Provider.of<ProjectNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);

    if (userViewModel.employeeId! == widget.employeeId) {
      isEmployee = true;
    }

    if (widget.employeeId! == "") {
      employeeId = userViewModel.employeeId!;
      viewMyScrum = true;
    } else {
      employeeId = widget.employeeId!;
      viewMyScrum = false;
    }
    initialDate = DateTime.now();
    // allowing only last two months
    pastDate = DateTime(initialDate!.year, initialDate!.month - 2, initialDate!.day);
    _startDate = getTodayDateDDMMYYYY();
    _endDate = _startDate;
    getMyScrums();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {},
                    selectedMenu: "${RouteNames.scrumEmployeeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }
  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 25, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuScrum", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(5, 15, 5, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [header(), _body()],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(viewMyScrum ? "My $navMenuScrum" : "${widget.employeeName}  $navMenuScrum",
                maxLines: 2, softWrap: true, style: fragmentHeaderStyle),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: fromDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(labelText: "From Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(fromDateController);
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: TextFormField(
                          maxLines: 1,
                          controller: toDateController,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.sentences,
                          obscureText: false,
                          decoration: editTextDateDecoration.copyWith(labelText: "To Date"),
                          readOnly: true,
                          enabled: true,
                          style: textFieldStyle.copyWith(fontSize: 15),
                          onTap: () {
                            showCalendar(toDateController);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            _startDate = fromDateController.text.trim();
                            _endDate = toDateController.text.trim();
                            if (_startDate == "" || _endDate == "") {
                              showToast("Both From and To dates are required.");
                            } else {
                              getMyScrums();
                            }
                          });
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            searchGo,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: dropDownColor,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(searchGoButtonBg),
                          overlayColor: MaterialStateProperty.all(dropDownColor.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ),
                    /*(fromDateController.text.trim() != "" || toDateController.text.trim() != "")
                        ? */
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                        onPressed: () {
                          resetTextFields();
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            reset,
                            style: TextStyle(
                              letterSpacing: 0.9,
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(dashBoardCardOverDueText),
                          overlayColor: MaterialStateProperty.all(dashBoardCardOverDueText.withOpacity(0.1)),
                          elevation: MaterialStateProperty.all(1),
                        ),
                      ),
                    ) /*: const SizedBox()*/,
                  ],
                ),
              ),
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.end,
                  spacing: 5,
                  runSpacing: 5,
                  children: [
                    _customOutlinedRadioButton(today, 0, buttonBg),
                    _customOutlinedRadioButton(daily, 1, buttonBg),
                    _customOutlinedRadioButton(monthly, 2, newOnGoing),
                    _customOutlinedRadioButton(yearly, 3, buttonraisebug),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget _body() {
    var scrumList = Provider.of<ScrumNotifier>(context, listen: true).getMyScrumsList;
    var scrumCountList = Provider.of<ScrumNotifier>(context, listen: true).getMyScrumCountsList;
    return Expanded(
        child: isTableView
            ? scrumList.isNotEmpty
                ? ListView.separated(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    physics: const ScrollPhysics(),
                    controller: listScrollController,
                    itemCount: scrumList.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return listItems(scrumList[index]);
                    },
                    separatorBuilder: (context, index) {
                      return const Divider(
                        color: Colors.transparent,
                        height: 10,
                      );
                    },
                  )
                : SizedBox(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    child: Center(
                      child: Text(isApiRunning ? "" : noDataAvailable, style: logoutHeader),
                    ),
                  )
            : scrumCountList.isNotEmpty
                ? GridView.builder(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    controller: gridScrollController,
                    physics: const ScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: scrumCountList.length,
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                        childAspectRatio: 2.35, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 450),
                    itemBuilder: (BuildContext context, int index) {
                      return countsListItems(scrumCountList[index]);
                    })
                : SizedBox(
                    height: ScreenConfig.height(context),
                    width: ScreenConfig.width(context),
                    child: Center(
                      child: Text(isApiRunning ? "" : noDataAvailable, style: logoutHeader),
                    ),
                  ));
  }
  Widget countsListItems(my_scrum_counts.ScrumData scrumData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: isHovered ? 2 : 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              if (getScrumFilterType() == daily) {
                setState(() {
                  _startDate = scrumData.heading!;
                  _endDate = scrumData.heading!;
                  getMyScrums();
                });
              } else if (getScrumFilterType() == monthly) {
                setState(() {
                  DateTime tempDate = DateFormat("MMMM yyyy").parse(scrumData.heading!, true).toLocal();
                  var monthFormat = DateFormat('MM'); // get month
                  var yearFormat = DateFormat('yyyy'); // get year
                  var _month = monthFormat.format(tempDate);
                  var _year = yearFormat.format(tempDate);
                  _startDate = "01-$_month-$_year"; // start date is 01
                  DateTime _lastDayOfMonth = DateTime(int.parse(_year), int.parse(_month) + 1, 0); // end date is calculated as total number of days
                  _endDate = "${_lastDayOfMonth.day}-$_month-$_year";
                  getMyScrums();
                });
              } else if (getScrumFilterType() == yearly) {
                setState(() {
                  _startDate = "";
                  _endDate = "";
                  _year = scrumData.heading!;
                  getMyYearScrums();
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: [
                Text(
                  scrumData.heading!,
                  maxLines: 2,
                  softWrap: true,
                  style: fragmentHeaderStyle.copyWith(
                    color: isHovered ? Colors.white : editText,
                    fontSize: 16,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(10, 10, 5, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  newStr,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: buttonBg,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.newTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  pending,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: newOnGoing,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.pendingTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: ScreenConfig.height(context),
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(5, 10, 10, 10),
                          color: Colors.transparent,
                          child: Card(
                            elevation: 2,
                            margin: const EdgeInsets.all(0),
                            color: CupertinoColors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  complete,
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: buttonraisebug,
                                    fontSize: 14,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  scrumData.tasksCount!.completedTasksCount!.toString(),
                                  maxLines: 2,
                                  softWrap: true,
                                  style: fragmentHeaderStyle.copyWith(
                                    color: editText,
                                    fontSize: 18,
                                    letterSpacing: 1.45,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
        ),
      );
    });
  }
  Widget listItems(my_scrum.ScrumData scrumData) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 7),
          child: Text(
            scrumData.heading!,
            maxLines: 2,
            softWrap: true,
            style: fragmentHeaderStyle.copyWith(
              color: buttonBg,
              fontSize: 16,
              letterSpacing: 1.25,
            ),
          ),
        ),
        DataTable2(
          columnSpacing: 14,
          dividerThickness: 1,
          scrollController: tableScrollController,
          decoration: BoxDecoration(
              border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
          headingRowHeight: 46,
          dataRowHeight: 60,
          smRatio: 0.4,
          lmRatio: 1.5,
          columns: tableColumnHeader(),
          rows: tableRows(scrumData.tasksList! + scrumData.othertasksList!),
          empty: Center(
            child: Text(noDataAvailable, style: logoutHeader),
          ),
        ),
       /* Padding(
          padding: const EdgeInsets.only(bottom: 7),
          child: Text(
            'Other Tasks',
            maxLines: 2,
            softWrap: true,
            style: fragmentHeaderStyle.copyWith(
              color: buttonBg,
              fontSize: 16,
              letterSpacing: 1.25,
            ),
          ),
        ),
        DataTable2(
          columnSpacing: 14,
          dividerThickness: 1,
          scrollController: table2ScrollController,
          decoration: BoxDecoration(
              border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
          headingRowHeight: 46,
          dataRowHeight: 60,
          smRatio: 0.4,
          lmRatio: 1.5,
          columns: tableColumnHeader(),
          rows: tableRows(scrumData.othertasksList!),
          empty: Center(
            child: Text(noDataAvailable, style: logoutHeader),
          ),
        ),*/
      ],
    );
  }
  List<DataColumn2> tableColumnHeader() {
    return <DataColumn2>[
      DataColumn2(label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.S),
      DataColumn2(label: Text(taskNameStr, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(tableModuleName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(projectName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(bugName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(tableStatus, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(estimationTimeScrum, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      DataColumn2(label: Text(taskTimeScrum, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
      if (isEmployee!) ...[
        DataColumn2(
            label: Center(
              child: Text(tableActions, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
            size: ColumnSize.L),
      ]
    ];
  }
  List<DataRow> tableRows(List<my_scrum.TasksList> scrums) {
    /* actions -
    * task type = "task" show buttons
    * 1 (new)- start
    * 2 (open)- hold, complete
    * 3 (completed)- reopen, close
    * 4 (close) - empty
    * 5 (reopen)- start
    * 6 (delete) - empty
    * 7 (hold)- start, complete */
    String bugStatusInitialValue = "Status";
    return scrums.map<DataRow2>((e) {
      for (var status in bugStatuses) {
        if (status == e.bugStatus!) {
          bugStatusInitialValue = status;
        }
      }
      return DataRow2(onTap: () {
        Navigator.push(
          context,
          PageRouteBuilder(
            pageBuilder:
                (context, animation1, animation2) =>
                ViewTaskScreen(
                    projectId: e.projectId!,
                    moduleId: e.moduleId!,
                    taskId: e.taskId!),
            transitionDuration:
            const Duration(seconds: 0),
            reverseTransitionDuration:
            const Duration(seconds: 0),
          ),
        ).then((value) {
          getMyScrums();
        });
      },color: MaterialStateColor.resolveWith((states) => e.taskAddType == "other" ? filterBg : CupertinoColors.white), specificRowHeight: 60, cells: [
        DataCell(Text((scrums.indexOf(e) + 1).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(Text(e.taskName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(Text(e.moduleName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(Text(e.projectName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(Text(e.bugTitle!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(
          e.taskType == "task"
              ? e.taskStatus! == 7 ? _holdStatusButton(e.percentageComplete!,e.holdComment!):getTextFromScrumStatusWidget(e.taskStatus!)
              : Text(e.bugStatus!.toString(),
                  softWrap: true, style: listItemsStyle.copyWith(fontWeight: FontWeight.w700), textAlign: TextAlign.start),
        ),
        DataCell(Text(minutesToHrs(e.estimationTime!).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start)),
        DataCell(Row(
          children: [
            Text(minutesToHrs(Duration(seconds: e.involvedTime!).inMinutes).toString(),
                softWrap: true,
                style: listItemsStyle.copyWith(color: checkEstimationTime(e.involvedTime!, e.estimationTime!)),
                textAlign: TextAlign.start),
            !checkTimeforInformation(e.involvedTime!, e.estimationTime!) ? Padding(
              padding: const EdgeInsets.only(left: 3),
              child: Tooltip(
                message: e.closeComment,
                decoration: toolTipDecoration,
                textStyle: toolTipStyle,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Icon(Icons.info_outline, color: timeText),
              ),
            ) : SizedBox(),
          ],
        )),
        if (isEmployee!) ...[
          DataCell(
            e.taskType == "task"
                ? Center(
                    child: Wrap(
                      runAlignment: WrapAlignment.center,
                      alignment: WrapAlignment.center,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        if (e.taskStatus! == 1) ...[
                          _startButton(e),
                        ] else if (e.taskStatus! == 2) ...[
                          _holdButton(e),
                          _completedButton(e)
                        ] else if (e.taskStatus! == 3) ...[
                          _reOpenButton(e),
                          _closeButton(e),
                        ] else if (e.taskStatus! == 4) ...[
                          const SizedBox(),
                        ] else if (e.taskStatus! == 5) ...[
                          _startButton(e),
                        ] else if (e.taskStatus! == 6) ...[
                          const SizedBox(),
                        ] else if (e.taskStatus! == 7) ...[
                          _startButton(e),
                          _completedButton(e),
                        ]
                      ],
                    ),
                  )
                : DropdownButtonFormField<String>(
                    items: bugStatuses.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: editTextStatusDecoration,
                    style: textFieldStyle,
                    value: bugStatusInitialValue,
                    onChanged: (String? value) {
                      setState(() {
                        bugStatusInitialValue = value!;
                        updateBugStatus(e.bugId!, bugStatusInitialValue);
                      });
                    },
                  ),
          ),
        ]
      ]);
    }).toList();
  }


  bool checkTimeforInformation(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return false;
    } else {
      return true;
    }
  }
  Color checkEstimationTime(int involvedTime, int estimationTime) {
    int involvedMins = Duration(seconds: involvedTime).inMinutes;
    if (estimationTime < involvedMins) {
      return newOverDue;
    } else {
      return newCompleted;
    }
  }
  Widget _startButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.openStr.name!, '');
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            start,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonBg,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(buttonBg.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _closeButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.closeStr.name!, '');
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            close,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonBg,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(buttonBg.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  showTaskCommentDialog(String taskId) {
    final _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 700, maxWidth: 700, minHeight: 5 * 24, maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    validator: emptyTextValidator,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(hintText: reasonExtraTime),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          updateTaskStatus(taskId, StatusText.completeStr.name!, taskCommentController.text);
                          Navigator.of(context).pop();
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  Widget _holdButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.holdStr.name!, '');
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            hold,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: newOnGoing,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(newOnGoing.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: newOnGoing)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _reOpenButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          updateTaskStatus(e.taskId!, StatusText.reopenStr.name!, '');
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            reOpen,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: newOnGoing,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(newOnGoing.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: newOnGoing)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _completedButton(my_scrum.TasksList e) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, right: 2),
      child: TextButton(
        onPressed: () {
          int involvedMins = Duration(seconds: e.involvedTime!).inMinutes;
          if (e.estimationTime! < involvedMins) {
            taskCommentController.text = "";
            showTaskCommentDialog(e.taskId!);
          } else {
            updateTaskStatus(e.taskId!, StatusText.completeStr.name!, '');
          }
        },
        child: const Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            complete,
            style: TextStyle(
              letterSpacing: 0.9,
              fontWeight: FontWeight.w600,
              color: buttonraisebug,
            ),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(buttonraisebug.withOpacity(0.1)),
          side: MaterialStateProperty.all(const BorderSide(color: buttonraisebug)),
          elevation: MaterialStateProperty.all(2),
        ),
      ),
    );
  }
  Widget _customOutlinedRadioButton(String text, int index, Color border) {
    return OutlinedButton(
      onPressed: () {
        setState(() {
          selectedButton = index;
          resetTextFields();
          if (selectedButton == 0) {
            setState(() {
              _startDate = getTodayDateDDMMYYYY();
              _endDate = _startDate;
            });
            getMyScrums();
          } else {
            getMyScrumCounts();
          }
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          text,
          style: TextStyle(
            letterSpacing: 0.9,
            fontWeight: FontWeight.w600,
            color: (selectedButton == index) ? Colors.white : border,
          ),
        ),
      ),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all((selectedButton == index) ? border : Colors.white),
        overlayColor: MaterialStateProperty.all(border.withOpacity(0.1)),
        side: MaterialStateProperty.all(BorderSide(color: border)),
        elevation: MaterialStateProperty.all((selectedButton == index) ? 2 : 1),
      ),
    );
  }

  Future<void> showCalendar(TextEditingController textController) async {
    DateTime? tempDate;
    if (fromDateController.text != "") {
      tempDate = DateFormat("dd-MM-yyyy").parse(fromDateController.text);
      if (textController == fromDateController) {
        tempDate = null;
      }
    }
    DateTime? picked = await showDatePicker(context: context, initialDate: initialDate!, firstDate: tempDate ?? pastDate!, lastDate: initialDate!);
    if (picked != null) {
      setState(() {
        var inputFormat = DateFormat('yyyy-MM-dd');
        // removing time from date
        var inputDate = inputFormat.parse(picked.toLocal().toString().split(' ')[0]);
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      });
    }
  }

  String getScrumFilterType() {
    switch (selectedButton) {
      case 0:
        {
          return today;
        }
      case 1:
        {
          return daily;
        }
      case 2:
        {
          return monthly;
        }
      case 3:
        {
          return yearly;
        }
      default:
        return today;
    }
  }

  resetTextFields() {
    _startDate = "";
    _endDate = "";
    fromDateController.clear();
    toDateController.clear();
  }

  getMyScrums() {
    Future.delayed(Duration.zero, () {
      setState(() {
        isApiRunning = true;
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
      viewModel.viewMyScrumsApi(employeeId, _startDate, _endDate).then((value) {
        Navigator.of(context).pop();
        setState(() {
          isTableView = true;
          isApiRunning = false;
        });
      });
    });
  }

  getMyYearScrums() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel.viewMyScrumYearlyApi(employeeId, _year).then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = true;
        isApiRunning = false;
      });
    });
  }

  getMyScrumCounts() {
    setState(() {
      isApiRunning = true;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ScrumNotifier>(context, listen: false);
    viewModel.viewMyScrumCountsApi(employeeId, getScrumFilterType().toLowerCase()).then((value) {
      Navigator.of(context).pop();
      setState(() {
        isTableView = false;
        isApiRunning = false;
      });
    });
  }

  updateTaskStatus(String taskId, String status, comment) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateTasksBasedOnStatusAPI(employeeId, taskId, status, comment,0).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getMyScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }
  Widget _holdStatusButton(int percent,String comment) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 5, 5, 5),
          child: OnHoverCard(builder: (isHovered) {
            return Container(
              width: 100,
              decoration: BoxDecoration(
                  color: CupertinoColors.white, borderRadius: BorderRadius.circular(0), border: Border.all(color: holdBgColor, width: 1.5)),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: DecoratedBox(
                      decoration: const BoxDecoration(color: holdBgColor),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                        child: Text(
                          hold,
                          style: const TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: CupertinoColors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Text(
                        '${percent} %',
                        style: TextStyle(fontSize: 12, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: addRoleText),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3),
          child: Tooltip(
            message: comment,
            decoration: toolTipDecoration,
            textStyle: toolTipStyle,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Icon(Icons.info_outline, color: timeText),
          ),
        )
      ],
    );
  }

  updateBugStatus(String bugId, String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateBugStatusAPI(bugId, employeeId, status).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getMyScrums();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }
}
