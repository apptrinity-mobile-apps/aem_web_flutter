import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/model/get_multiple_sprints_data_model.dart';
import 'package:aem/model/project_sprints_response_model.dart' as sprintData;
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddScrumFragement extends StatefulWidget {
  final String? scrumDate;

  AddScrumFragement({this.scrumDate, Key? key}) : super(key: key);

  @override
  State<AddScrumFragement> createState() => _AddScrumState();
}

class _AddScrumState extends State<AddScrumFragement> {
  EmployeeDetails employeeInitialValue = EmployeeDetails(name: selectEmployee);
  List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems = [];
  List<EmployeeDetails> employeeList = [], selectedEmployees = [];
  List projectEmployee = [];
  List selectedAssainEmployee = [];
  List selectedAssainEmployeeTaskList = [];
  final _formKey = GlobalKey<FormState>();
  final dateController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  String apiDate = "";
  final descriptionController = TextEditingController();
  bool assainTask = false;
  final _multiKey = GlobalKey<DropdownSearchState<ProjectList>>();
  List<ProjectList> projectList = [], selectedProject = [];
  List selectedProjectIds = [];
  List<sprintData.ProjectsData> selectedProjectswithData = [];
  late ProjectNotifier viewModel = ProjectNotifier();
  late ScrumNotifier scrumViewModel = ScrumNotifier();
  int projectSelected = -1;
  int sprintSelected = -1;
  int taskSelected = -1;
  int bugSelected = -1;
  String employeeId = "";
  SprintsList itemData = SprintsList();
  final scrollController = ScrollController();

  @override
  void initState() {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy');

    String formattedDate = formatter.format(now);

    apiDate = formattedDate;
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;

    print("scrum date is ${widget.scrumDate}");

    if (widget.scrumDate == null) {
      dateController.text = formattedDate;
    } else {
      dateController.text = widget.scrumDate!;
    }

    Future.delayed(Duration.zero, () async {
      await viewModel.getAllProjectsAPI(employeeId, 'scrum').then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.projectList!.isNotEmpty) {
                projectList = value.projectList!;
              }
            });
          }
        }
        // hasTechnologies = true;
      });
      await scrumViewModel.assianableEmployeesapi(employeeId).then((value) {
        employeeMenuItems = [];
        employeeMenuItems.add(DropdownMenuItem(
            child: const Text(selectEmployee), value: employeeInitialValue));
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeeList = value.employeeDetails!;
                for (var element in value.employeeDetails!) {
                  employeeMenuItems.add(DropdownMenuItem(
                      child: Text(element.name!), value: element));
                }
              }
            });
          }
        }
        // hasTechnologies = true;
      });
      if (widget.scrumDate != null) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return loader();
            });
        await scrumViewModel
            .getScrumDataapi(employeeId, widget.scrumDate!)
            .then((value) {
          Navigator.pop(context);
          employeeMenuItems = [];
          employeeMenuItems.add(DropdownMenuItem(
              child: const Text(selectEmployee), value: employeeInitialValue));
          if (value != null) {
            if (value.responseStatus == 1) {
              setState(() {
                dateController.text = value.scrumData!.scrumDate!;
                descriptionController.text = value.scrumData!.description!;
                if (value.scrumData!.projectsList!.isNotEmpty) {
                  for (var element in value.scrumData!.projectsList!) {
                    selectedProjectIds.add(element.id!);
                    for (var project in projectList) {
                      if (element.id! == project.id) {
                        selectedProject.add(project);
                      }
                    }
                  }
                  _multiKey.currentState!.changeSelectedItems(selectedProject);
                }
                if (value.projectsData!.isNotEmpty) {
                  for (var element in value.projectsData!) {
                    var otherTaskList = element.otherTasksList!;
                    var sprintList = element.sprintsList!;
                    var bugsList = element.bugsList!;
                    for (var element in sprintList) {
                      var tasksData = element.tasksData!;
                      for (var element in tasksData) {
                        print("employId -- ${element.employeeId}");
                        if (element.employeeId != "") {
                          var selected = {
                            "taskId": element.taskId,
                            "employeeId": element.employeeId,
                            "employeeName": element.employeeName,
                            "type": "task"
                          };
                          selectedAssainEmployee.add(selected);
                        }
                      }
                    }
                    for (var element in otherTaskList) {
                      if (element.employeeId != "") {
                        var selected = {
                          "taskId": element.taskId,
                          "employeeId": element.employeeId,
                          "employeeName": element.employeeName,
                          "type": "task"
                        };
                        selectedAssainEmployee.add(selected);
                      }
                    }

                    for (var element in bugsList) {
                      if (element.employeeId != "") {
                        print("bug -- ${element.bugId}");

                        var selected = {
                          "taskId": element.bugId,
                          "employeeId": element.employeeId,
                          "employeeName": element.employeeName,
                          "type": "bug"
                        };
                        selectedAssainEmployee.add(selected);
                      }
                    }

                    print(
                        "selectedAssainEmployee -- ${selectedAssainEmployee}");
                  }
                }
              });
            }
          }
          // hasTechnologies = true;
        });
      }
    });

    print(formattedDate);
    super.initState();
  }

  Widget addScrumWidget() {
    return Column(
      children: [
        header(),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 10, left: 30, right: 30, bottom: 30),
            width: ScreenConfig.width(context),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: dashBoardCardBorderTile)),
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  scrumDateField(),
                  descriptionFiled(),
                  scrumProjectFiled(),
                  Container(
                    decoration: buttonDecorationGreen,
                    margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: TextButton(
                      onPressed: () async {
                        setState(() {
                          if (selectedProject.length != 0) {
                            selectedProjectIds = [];
                            for (var element in selectedProject) {
                              selectedProjectIds.add(element.id);
                            }
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            Future.delayed(Duration.zero, () async {
                              await scrumViewModel
                                  .projectBasedSprintapi(
                                      employeeId, selectedProjectIds)
                                  .then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    setState(() {
                                      if (value.projectsData!.isNotEmpty) {
                                        selectedProjectswithData =
                                            value.projectsData!;
                                      }
                                      assainTask = true;
                                    });
                                  }
                                }
                                // hasTechnologies = true;
                              });
                            });
                          }
                          // currentRoute = value;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child:
                            Text(submit.toUpperCase(), style: newButtonsStyle),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget assainTaskWidget() {
    return Column(
      children: [
        assainTaskheader(),
        const Padding(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
          child: Divider(color: dashBoardCardBorderTile),
        ),
        Flexible(
          child: Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: Column(
                children: [
                  Visibility(
                      child: //moduleDetails.isNotEmpty?
                          ListView.builder(
                              key: Key('builder ${projectSelected.toString()}'),
                              padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                              shrinkWrap: true,
                              physics: const ScrollPhysics(),
                              itemCount: selectedProjectswithData.length,
                              itemBuilder: (context, index) {
                                return ExpansionTile(
                                  key: Key(index.toString()),
                                  //attention
                                  initiallyExpanded: index == projectSelected,
                                  title: Row(
                                    children: [
                                      CircleAvatar(
                                          backgroundColor:
                                              newCompleted.withOpacity(0.3),
                                          maxRadius: 13,
                                          minRadius: 10),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 15),
                                        child: Text(
                                            "${selectedProjectswithData[index].name}"),
                                      ),
                                    ],
                                  ),
                                  children: [
                                    sprintListData(
                                        index,
                                        selectedProjectswithData[index]
                                            .sprintsList),
                                    taskListData(
                                        index,
                                        selectedProjectswithData[index]
                                            .otherTasksList),
                                    bugsListData(
                                        index,
                                        selectedProjectswithData[index]
                                            .bugsList)
                                  ],

                                  onExpansionChanged: ((newState) {
                                    sprintSelected = -1;
                                    if (newState)
                                      setState(() {
                                        Duration(seconds: 20000);
                                        projectSelected = index;
                                      });
                                    else
                                      setState(() {
                                        projectSelected = -1;
                                      });
                                  }),
                                );

                                /* return todosList(index, moduleDetails[index]);*/
                              })
                      /*: Center(
                      child: Text(noDataAvailable, style: logoutHeader),*/
                      // ),
                      )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget sprintListData(int position, List<sprintData.SprintsList>? data) {
    return Column(
      children: [
        Visibility(
            child: //moduleDetails.isNotEmpty?
                ListView.builder(
                    key: Key('builder ${sprintSelected.toString()}'),
                    padding: const EdgeInsets.fromLTRB(40, 0, 55, 0),
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    itemCount: data!.length,
                    itemBuilder: (context, index) {
                      return ExpansionTile(
                        // iconColor: Colors.transparent,
                        // collapsedIconColor: Colors.transparent,
                        key: Key(index.toString()),
                        //attention
                        maintainState: true,
                        initiallyExpanded: index == sprintSelected,
                        title: Row(
                          children: [
                            // Image.asset(sprintSelected == index ? "assets/images/upArraow.png" : "assets/images/downArraow.png", color: appBackground, height: 15, width: 15),
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Text("${data[index].name}"),
                            ),
                          ],
                        ),
                        children: [
                          sprintsSubList(index, data[index].tasksData)
                        ],
                        onExpansionChanged: ((newState) {
                          taskSelected = -1;
                          bugSelected = -1;
                          if (newState)
                            setState(() {
                              Duration(seconds: 20000);
                              sprintSelected = index;
                            });
                          else
                            setState(() {
                              sprintSelected = -1;
                            });
                        }),
                      );

                      /* return todosList(index, moduleDetails[index]);*/
                    })
            /*: Center(
                      child: Text(noDataAvailable, style: logoutHeader),*/
            // ),
            )
      ],
    );
  }

  Widget taskListData(int position, List<sprintData.OtherTasksList>? data) {
    return Column(
      children: [
        Visibility(
            child: //moduleDetails.isNotEmpty?
                ListView.builder(
                    key: Key('builder ${taskSelected.toString()}'),
                    padding: const EdgeInsets.fromLTRB(40, 0, 55, 0),
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return ExpansionTile(
                        // iconColor: Colors.transparent,
                        // collapsedIconColor: Colors.transparent,
                        key: Key(index.toString()),
                        //attention
                        initiallyExpanded: index == taskSelected,
                        title: Row(
                          children: [
                            // Image.asset(sprintSelected == index ? "assets/images/upArraow.png" : "assets/images/downArraow.png", color: appBackground, height: 15, width: 15),
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Text(tasks),
                            ),
                          ],
                        ),
                        children: [taskSubList(index, data)],
                        onExpansionChanged: ((newState) {
                          sprintSelected = -1;
                          bugSelected = -1;
                          if (newState)
                            setState(() {
                              Duration(seconds: 20000);
                              taskSelected = index;
                            });
                          else
                            setState(() {
                              taskSelected = -1;
                            });
                        }),
                      );

                      /* return todosList(index, moduleDetails[index]);*/
                    })
            /*: Center(
                      child: Text(noDataAvailable, style: logoutHeader),*/
            // ),
            )
      ],
    );
  }

  Widget bugsListData(int position, List<sprintData.BugsList>? data) {
    return Column(
      children: [
        Visibility(
            child: //moduleDetails.isNotEmpty?
                ListView.builder(
                    key: Key('builder ${bugSelected.toString()}'),
                    padding: const EdgeInsets.fromLTRB(40, 0, 55, 0),
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return ExpansionTile(
                        // iconColor: Colors.transparent,
                        // collapsedIconColor: Colors.transparent,
                        key: Key(index.toString()),
                        //attention
                        initiallyExpanded: index == bugSelected,
                        title: Row(
                          children: [
                            // Image.asset(sprintSelected == index ? "assets/images/upArraow.png" : "assets/images/downArraow.png", color: appBackground, height: 15, width: 15),
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Text(bugs),
                            ),
                          ],
                        ),
                        children: [bugSubList(index, data)],
                        onExpansionChanged: ((newState) {
                          sprintSelected = -1;
                          taskSelected = -1;
                          if (newState)
                            setState(() {
                              Duration(seconds: 20000);
                              bugSelected = index;
                            });
                          else
                            setState(() {
                              bugSelected = -1;
                            });
                        }),
                      );

                      /* return todosList(index, moduleDetails[index]);*/
                    })
            /*: Center(
                      child: Text(noDataAvailable, style: logoutHeader),*/
            // ),
            )
      ],
    );
  }

  Widget sprintsSubList(int position, List<sprintData.TasksData>? data) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable2(
                  columnSpacing: (MediaQuery.of(context).size.width / 10) * 0.5,
                  dividerThickness: 1,
                  decoration: BoxDecoration(
                      border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor:
                      MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  lmRatio: 2,
                  columns: <DataColumn2>[
                    DataColumn2(
                        label: Text(tableModuleName,
                            softWrap: true,
                            style: listHeaderStyle,
                            textAlign: TextAlign.start),
                        size: ColumnSize.M),
                    DataColumn2(
                        label: Text(taskNameStr,
                            softWrap: true,
                            style: listHeaderStyle,
                            textAlign: TextAlign.start),
                        size: ColumnSize.L),
                    DataColumn2(
                        label: Text(assign,
                            softWrap: true,
                            style: listHeaderStyle,
                            textAlign: TextAlign.start),
                        size: ColumnSize.M),
                  ],
                  rows: data!.map<DataRow>((e) {
                    for (var element in selectedAssainEmployee) {
                      var taskId = element["taskId"];
                      var employeeId = element["employeeId"];

                      if (taskId == e.taskId) {
                        e.assianEmployeeId = employeeId;
                      }
                    }

                    var employeeInitialValue =
                        EmployeeDetails(name: selectEmployee);
                    List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems =
                        [];
                    employeeMenuItems.add(DropdownMenuItem(
                        child: const Text(selectEmployee),
                        value: employeeInitialValue));
                    for (var element in employeeList) {
                      employeeMenuItems.add(DropdownMenuItem(
                          child: Text(element.name!), value: element));
                    }
                    for (var employee in employeeList) {
                      if (employee.id == e.assianEmployeeId) {
                        employeeInitialValue = employee;
                      }
                    }
                    return DataRow2(specificRowHeight: 60,
                        /*color: MaterialStateColor.resolveWith(
                                (states) => data.!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1 CupertinoColors.white),*/
                        cells: [
                          DataCell(Text(e.moduleName!,
                              softWrap: true,
                              style: projectAddedByStyle,
                              textAlign: TextAlign.start)),
                          DataCell(Text(e.taskName!,
                              softWrap: true,
                              style: listItemsStyle,
                              textAlign: TextAlign.start)),
                          DataCell(Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  minWidth: 180, maxWidth: 180),
                              child: IntrinsicWidth(
                                child: DropdownButtonFormField<EmployeeDetails>(
                                  // key: _employeeKey,

                                  items: employeeMenuItems,
                                  decoration: editTextEmployeesDecoration,
                                  style: textFieldStyle,
                                  value: employeeInitialValue,
                                  //validator: employeeDropDownValidator,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  onChanged: (EmployeeDetails? value) {
                                    setState(() {
                                      e.assianEmployeeId = value!.id;
                                      var selected = {
                                        "taskId": e.taskId,
                                        "employeeId": value.id,
                                        "employeeName": value.name,
                                        "type": "task"
                                      };
                                      if (selectedAssainEmployee.length == 0) {
                                        selectedAssainEmployee.add(selected);
                                      } else {
                                        var contain = selectedAssainEmployee
                                            .where((element) =>
                                                element['taskId'] == e.taskId);
                                        if (contain.isEmpty) {
                                          selectedAssainEmployee.add(selected);
                                        } else {
                                          for (int i = 0;
                                              i < selectedAssainEmployee.length;
                                              i++) {
                                            var obj = selectedAssainEmployee[i];
                                            var taskId = obj["taskId"];
                                            if (taskId == e.taskId) {
                                              obj["employeeId"] = value.id;
                                              obj["employeeName"] = value.name;
                                            }
                                          }
                                        }
                                      }
                                      print(
                                          "selectedAssainEmployee -- ${selectedAssainEmployee}");
                                    });
                                  },
                                ),
                              ),
                            ),
                          )),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget taskSubList(int position, List<sprintData.OtherTasksList>? data) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable(
                  columnSpacing: (MediaQuery.of(context).size.width / 10) * 0.5,
                  dividerThickness: 1,
                  decoration: BoxDecoration(
                      border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor:
                      MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  columns: <DataColumn>[
                    DataColumn(
                      label: Text(tableModuleName,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start),
                    ),
                    DataColumn(
                      label: Text(taskNameStr,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start),
                    ),
                    DataColumn(
                      label: Text(assign,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.center),
                    ),
                  ],
                  /*rows: const <DataRow>[
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Sarah')),
                        DataCell(Text('19')),
                        DataCell(Text('Student')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Janine')),
                        DataCell(Text('43')),
                        DataCell(Text('Professor')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('William')),
                        DataCell(Text('27')),
                        DataCell(Text('Associate Professor')),
                      ],
                    ),
                  ],*/
                  rows: data!.map<DataRow>((e) {
                    for (var element in selectedAssainEmployee) {
                      var taskId = element["taskId"];
                      var employeeId = element["employeeId"];

                      if (taskId == e.taskId) {
                        e.assianEmployeeId = employeeId;
                      }
                    }

                    var employeeInitialValue =
                        EmployeeDetails(name: selectEmployee);
                    List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems =
                        [];
                    employeeMenuItems.add(DropdownMenuItem(
                        child: const Text(selectEmployee),
                        value: employeeInitialValue));

                    for (var element in employeeList) {
                      employeeMenuItems.add(DropdownMenuItem(
                          child: Text(element.name!), value: element));
                    }

                    for (var employee in employeeList) {
                      if (employee.id == e.assianEmployeeId) {
                        employeeInitialValue = employee;
                      }
                    }

                    return DataRow(
                        /*color: MaterialStateColor.resolveWith(
                                (states) => data.!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1 CupertinoColors.white),*/
                        cells: [
                          DataCell(Text(e.moduleName!,
                              softWrap: true,
                              style: projectAddedByStyle,
                              textAlign: TextAlign.start)),
                          DataCell(Text(e.taskName!,
                              softWrap: true,
                              style: listItemsStyle,
                              textAlign: TextAlign.center)),
                          DataCell(Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  minWidth: 180, maxWidth: 180),
                              child: IntrinsicWidth(
                                child: DropdownButtonFormField<EmployeeDetails>(
                                  // key: _employeeKey,

                                  items: employeeMenuItems,
                                  decoration: editTextEmployeesDecoration,
                                  style: textFieldStyle,

                                  value: employeeInitialValue,
                                  //validator: employeeDropDownValidator,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  onChanged: (EmployeeDetails? value) {
                                    setState(() {
                                      e.assianEmployeeId = value!.id;

                                      var selected = {
                                        "taskId": e.taskId,
                                        "employeeId": value.id,
                                        "employeeName": value.name,
                                        "type": "task"
                                      };
                                      if (selectedAssainEmployee.length == 0) {
                                        selectedAssainEmployee.add(selected);
                                      } else {
                                        var contain = selectedAssainEmployee
                                            .where((element) =>
                                                element['taskId'] == e.taskId);
                                        if (contain.isEmpty) {
                                          selectedAssainEmployee.add(selected);
                                        } else {
                                          for (int i = 0;
                                              i < selectedAssainEmployee.length;
                                              i++) {
                                            var obj = selectedAssainEmployee[i];
                                            var taskId = obj["taskId"];
                                            if (taskId == e.taskId) {
                                              obj["employeeId"] = value.id;
                                              obj["employeeName"] = value.name;
                                            } else {}
                                          }
                                        }
                                      }

                                      print(
                                          "selected employee ${selectedAssainEmployee}");

                                      // print("selected value is ${e.employeeInitialValue!.name}");
                                      // employeeInitialValue = value!;
                                    });
                                  },
                                ),
                              ),
                            ),
                          )),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget bugSubList(int position, List<sprintData.BugsList>? data) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable(
                  columnSpacing: (MediaQuery.of(context).size.width / 10) * 0.5,
                  dividerThickness: 1,
                  decoration: BoxDecoration(
                      border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor:
                      MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  columns: <DataColumn>[
                    DataColumn(
                      label: Text(tableModuleName,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start),
                    ),
                    DataColumn(
                      label: Text(taskNameStr,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start),
                    ),
                    DataColumn(
                      label: Text(bugName,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start),
                    ),
                    DataColumn(
                      label: Text(assign,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.center),
                    ),
                  ],
                  /*rows: const <DataRow>[
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Sarah')),
                        DataCell(Text('19')),
                        DataCell(Text('Student')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('Janine')),
                        DataCell(Text('43')),
                        DataCell(Text('Professor')),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text('William')),
                        DataCell(Text('27')),
                        DataCell(Text('Associate Professor')),
                      ],
                    ),
                  ],*/
                  rows: data!.map<DataRow>((e) {
                    for (var element in selectedAssainEmployee) {
                      var employeeId = element["employeeId"];
                      var bugId = element["taskId"];

                      if (bugId == e.bugId) {
                        print("bug id -- ${bugId} - ${e.bugId}");
                        e.assianEmployeeId = employeeId;
                      }
                    }

                    var employeeInitialValue =
                        EmployeeDetails(name: selectEmployee);
                    List<DropdownMenuItem<EmployeeDetails>> employeeMenuItems =
                        [];
                    employeeMenuItems.add(DropdownMenuItem(
                        child: const Text(selectEmployee),
                        value: employeeInitialValue));

                    for (var element in employeeList) {
                      employeeMenuItems.add(DropdownMenuItem(
                          child: Text(element.name!), value: element));
                    }
                    for (var employee in employeeList) {
                      if (employee.id == e.assianEmployeeId) {
                        employeeInitialValue = employee;
                      }
                    }
                    return DataRow(
                        /*color: MaterialStateColor.resolveWith(
                                (states) => data.!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1 CupertinoColors.white),*/
                        cells: [
                          DataCell(Text(e.moduleName!,
                              softWrap: true,
                              style: projectAddedByStyle,
                              textAlign: TextAlign.start)),
                          DataCell(Text(e.taskName!,
                              softWrap: true,
                              style: listItemsStyle,
                              textAlign: TextAlign.center)),
                          DataCell(Text(e.bugTitle!,
                              softWrap: true,
                              style: listItemsStyle,
                              textAlign: TextAlign.center)),
                          DataCell(Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  minWidth: 180, maxWidth: 180),
                              child: IntrinsicWidth(
                                child: DropdownButtonFormField<EmployeeDetails>(
                                  // key: _employeeKey,

                                  items: employeeMenuItems,
                                  decoration: editTextEmployeesDecoration,
                                  style: textFieldStyle,

                                  value: employeeInitialValue,
                                  //validator: employeeDropDownValidator,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  onChanged: (EmployeeDetails? value) {
                                    setState(() {
                                      e.assianEmployeeId = value!.id;

                                      var selected = {
                                        "taskId": e.bugId,
                                        "employeeId": value.id,
                                        "employeeName": value.name,
                                        "type": "bug"
                                      };
                                      if (selectedAssainEmployee.length == 0) {
                                        selectedAssainEmployee.add(selected);
                                      } else {
                                        var contain = selectedAssainEmployee
                                            .where((element) =>
                                                element['taskId'] == e.taskId);
                                        if (contain.isEmpty) {
                                          selectedAssainEmployee.add(selected);
                                        } else {
                                          for (int i = 0;
                                              i < selectedAssainEmployee.length;
                                              i++) {
                                            var obj = selectedAssainEmployee[i];
                                            var taskId = obj["taskId"];
                                            if (taskId == e.bugId) {
                                              obj["employeeId"] = value.id;
                                              obj["employeeName"] = value.name;
                                            } else {}
                                          }
                                        }
                                      }

                                      print(
                                          "selectedAssainEmployee -- ${selectedAssainEmployee}");

                                      // print("selected value is ${e.employeeInitialValue!.name}");
                                      // employeeInitialValue = value!;
                                    });
                                  },
                                ),
                              ),
                            ),
                          )),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    widget.scrumDate == null
                        ? add + " " + navMenuScrum
                        : edit + " " + navMenuScrum,
                    maxLines: 1,
                    softWrap: true,
                    style: fragmentHeaderStyle),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget assainTaskheader() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(assign + " " + tableTask,
                        maxLines: 1,
                        softWrap: true,
                        style: fragmentHeaderStyle),
                    Text("Scrum Date: ${dateController.text}", style: addRoleSubStyle)
                  ],
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                decoration: buttonDecorationGreen,
                margin: const EdgeInsets.fromLTRB(0, 30, 20, 0),
                child: TextButton(
                  onPressed: () async {
                    setState(() {
                      if (selectedProject.length != 0) {
                        selectedAssainEmployeeTaskList = [];
                        for (var element in selectedAssainEmployee) {
                          var obj;
                          if (element["type"] == "task") {
                            obj = {
                              "taskType": element["type"],
                              "taskId": element["taskId"],
                              "assignedEmployeeId": element["employeeId"]
                            };
                          } else {
                            obj = {
                              "taskType": element["type"],
                              "bugId": element["taskId"],
                              "assignedEmployeeId": element["employeeId"]
                            };
                          }
                          selectedAssainEmployeeTaskList.add(obj);
                        }
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        String type;
                        if (widget.scrumDate == null) {
                          type = "add";
                        } else {
                          type = "edit";
                        }

                        Future.delayed(Duration.zero, () async {
                          await scrumViewModel
                              .addScrumapi(
                                  employeeId,
                                  apiDate,
                                  descriptionController.text,
                                  selectedProjectIds,
                                  selectedAssainEmployeeTaskList,
                                  type)
                              .then((value) {
                            if (value != null) {
                              Navigator.pop(context);
                              if (value.responseStatus == 1) {
                                setState(() {
                                  showToast(value.result!);
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(
                                          pageBuilder: (context, animation1,
                                                  animation2) =>
                                              HomeScreen(
                                                  menu: RouteNames
                                                      .scrumEmployeeRoute),
                                          transitionDuration:
                                              const Duration(seconds: 0),
                                          reverseTransitionDuration:
                                              const Duration(seconds: 0)),
                                      (route) => false);
                                });
                              } else {
                                showToast(value.result!);
                              }
                            }
                            // hasTechnologies = true;
                          });
                        });
                      }
                      // currentRoute = value;
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                    child: Text(submit.toUpperCase(), style: newButtonsStyle),
                  ),
                ),
              ),
              Container(
                decoration: buttonBorderGreen,
                margin: const EdgeInsets.fromLTRB(0, 30, 30, 0),
                child: TextButton(
                  onPressed: () {
                    setState(() {
                      assainTask = false;
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                    child: Text(back,
                        style: cancelButtonStyle.copyWith(color: newCompleted)),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget scrumDateField() {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Wrap(
        children: [
          Container(
            width: 180,
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 0),
              child: Text("Scrum Date", style: addRoleSubStyle),
            ),
          ),
          Container(
            width: 300,
            child: TextFormField(
                maxLines: 1,
                controller: dateController,
                textInputAction: TextInputAction.done,
                obscureText: false,
                decoration: InputDecoration(
                  fillColor: CupertinoColors.white,
                  filled: true,
                  border: border,
                  isDense: true,
                  enabledBorder: border,
                  focusedBorder: border,
                  errorBorder: errorBorder,
                  focusedErrorBorder: errorBorder,
                  contentPadding: editTextPadding,
                  hintStyle: textFieldHintStyle,
                  hintText: 'DD / MM / YYYY',
                  suffixIconConstraints:
                      const BoxConstraints(maxHeight: 40, maxWidth: 40),
                  // suffixIcon: Padding(child: Image.asset("images/calendar.png", color: textFieldBorder), padding: dateUploadIconsPadding),
                ),
                style: textFieldStyle,
                onTap: () {
                  // showCalendar();
                },
                enableInteractiveSelection: false,
                focusNode: FocusNode(),
                readOnly: true,
                validator: emptyTextValidator),
          ),
        ],
      ),
    );
  }

  Widget descriptionFiled() {
    return Container(
        margin: EdgeInsets.only(top: 30),
        child: Wrap(
          children: [
          Container(
          width: 180,
          alignment: Alignment.topRight,
          child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 0),
              child: Text(description, style: addRoleSubStyle),
            )),
            Container(
              width: 300,
              child: TextFormField(
                  key: _formKey,
                  minLines: 4,
                  maxLines: 4,
                  controller: descriptionController,
                  obscureText: false,
                  keyboardType: TextInputType.multiline,
                  decoration: editTextProjectDescriptionDecoration,
                  style: addRoleSubStyle,
                  validator: emptyTextValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
          ],
        ));
  }

  Widget scrumProjectFiled() {
    return Container(
        margin: EdgeInsets.only(top: 30),
        child: Wrap(
          children: [
            Container(
              width: 180,
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 0),
                child: Text(navMenuScrum + " " + project, style: addRoleSubStyle),
              ),
            ),
            Container(
              width: 300,
              child: IntrinsicWidth(
                child: DropdownSearch<ProjectList>.multiSelection(
                  key: _multiKey,
                  mode: Mode.MENU,
                  showSelectedItems: selectedProject.isNotEmpty ? true : false,
                  compareFn: (item, selectedItem) =>
                      item?.id == selectedItem?.id,
                  items: projectList,
                  showClearButton: true,
                  onChanged: (data) {
                    print("selcted");
                    selectedProject = data;
                  },
                  clearButtonSplashRadius: 10,
                  selectedItems: selectedProject,
                  showSearchBox: true,
                  dropdownSearchDecoration: editTextEmployeesDecoration,
                  //validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                  dropdownBuilder: (context, selectedItems) {
                    return Wrap(
                        children: selectedItems
                            .map(
                              (e) => selectedItem(e.name!),
                            )
                            .toList());
                  },
                  filterFn: (ProjectList? employee, name) {
                    return employee!.name!
                            .toLowerCase()
                            .contains(name!.toLowerCase())
                        ? true
                        : false;
                  },
                  popupItemBuilder: (_, text, isSelected) {
                    return Container(
                      padding: const EdgeInsets.all(10),
                      child: Text(text.name!,
                          style: isSelected ? dropDownSelected : dropDown),
                    );
                  },
                  popupSelectionWidget: (cnt, item, bool isSelected) {
                    return Checkbox(
                        activeColor: buttonBg,
                        hoverColor: buttonBg.withOpacity(0.2),
                        value: isSelected,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4)),
                        side: const BorderSide(color: buttonBg),
                        onChanged: (value) {});
                  },
                  onPopupDismissed: () {
                    setState(() {
                      //isVisible = true;
                    });
                  },
                ),
              ),
            ),
          ],
        ));
  }

  Future<void> showCalendar() async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2023));
    if (picked != null) {
      setState(() {
        selectedDate = picked;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputDate = inputFormat.parse(selectedDate
            .toLocal()
            .toString()
            .split(' ')[0]); // removing time from date
        var outputFormat = DateFormat('dd-MM-yyyy');
        var outputDate = outputFormat.format(inputDate);
        var outputFormatServer = DateFormat('dd-MM-yyyy');
        var outputDateServer = outputFormatServer.format(inputDate);
        // careerStartDate = outputDateServer;
        // exp = Age.dateDifference(fromDate: selectedDate, toDate: DateTime.now(), includeToDate: false);
        dateController.text = outputDate;
        apiDate = outputDateServer;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuScrum",
              maxLines: 1,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: assainTask ? assainTaskWidget() : addScrumWidget(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
