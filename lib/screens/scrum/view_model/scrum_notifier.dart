import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_team_scrum_list_model.dart';
import 'package:aem/model/all_team_wise_scrum_list_response_model.dart' as teamwise_scrum;
import 'package:aem/model/employee_scrum_report.dart';
import 'package:aem/model/my_scrum_response_model.dart' as my_scrum;
import 'package:aem/model/project_sprints_response_model.dart';
import 'package:aem/model/projectwise_bugcount_response_model.dart';
import 'package:aem/model/scrum_counts_response.dart' as my_scrum_counts;
import 'package:aem/model/task_list_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/team_scrum_response_model.dart';
import 'package:aem/model/viw_scrumData_model.dart';

class ScrumNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<ProjectSprintsResponse?> projectBasedSprintapi(String employeeId,  List projectId) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectSprintsResponse? _projectSprintsResponse = ProjectSprintsResponse();

    try {
      dynamic response = await Repository().projectBasedSprintapi(employeeId,projectId);
      if (response != null) {
        _projectSprintsResponse = ProjectSprintsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectSprintsResponse");
    notifyListeners();
    return _projectSprintsResponse;
  }
  Future<AllEmployeesResponse?> assianableEmployeesapi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllEmployeesResponse? _employeeResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().assianableEmployeesapi(employeeId);
      if (response != null) {
        _employeeResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeeResponse");
    notifyListeners();
    return _employeeResponse;
  }

  Future<BaseResponse?> addScrumapi(String employeeId,String scrumDate,String description, List projectId,List scrumList,String type) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().addScrumapi( employeeId, scrumDate, description,  projectId, scrumList,type);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<TeamScrumCountResponseModel?> getTeamScrumCountapi(String employeeId,String filterType,String startDate, String endDate) async {
    _isFetching = true;
    _isHavingData = false;
    TeamScrumCountResponseModel? _teamScrumCountResponseModel = TeamScrumCountResponseModel();

    try {
      dynamic response = await Repository().getTeamScrumCountapi( employeeId, filterType, startDate,  endDate);
      if (response != null) {
        _teamScrumCountResponseModel = TeamScrumCountResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_teamScrumCountResponseModel");
    notifyListeners();
    return _teamScrumCountResponseModel;
  }

  Future<ScrumTeamListResponseModel?> getAllTeamScrumCountapi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    ScrumTeamListResponseModel? _allteamScrumCountResponseModel = ScrumTeamListResponseModel();

    try {
      dynamic response = await Repository().getAllTeamScrumCountapi(employeeId);
      if (response != null) {
        _allteamScrumCountResponseModel = ScrumTeamListResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allteamScrumCountResponseModel");
    notifyListeners();
    return _allteamScrumCountResponseModel;
  }

  Future<ViewScrumDataModel?> getScrumDataapi(String employeeId,String scrumDate) async {
    _isFetching = true;
    _isHavingData = false;
    ViewScrumDataModel? _viewScrumResponseModel = ViewScrumDataModel();

    try {
      dynamic response = await Repository().getScrumDataapi( employeeId, scrumDate);
      if (response != null) {
        _viewScrumResponseModel = ViewScrumDataModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_viewScrumResponseModel");
    notifyListeners();
    return _viewScrumResponseModel;
  }
  my_scrum.MyScrumResponse? _myScrumResponse = my_scrum.MyScrumResponse();

  List<my_scrum.ScrumData> get getMyScrumsList {
    List<my_scrum.ScrumData> list = [];
    if (_myScrumResponse != null) {
      if (_myScrumResponse!.responseStatus == 1) {
        if (_myScrumResponse!.scrumData!.isNotEmpty) {
          list = _myScrumResponse!.scrumData!;
        }
      }
    }
    return list;
  }



  Future<my_scrum.MyScrumResponse?> viewMyScrumsApi(String employeeId, String startDate, String endDate) async {
    _isFetching = true;
    _isHavingData = false;
    _myScrumResponse = my_scrum.MyScrumResponse();
    try {
      dynamic response = await Repository().viewMyScrum(employeeId, startDate, endDate);
      if (response != null) {
        _myScrumResponse = my_scrum.MyScrumResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_myScrumResponse");
    notifyListeners();
    return _myScrumResponse;
  }



  my_scrum.MyScrumResponse? _myScrumYearlyResponse;

  List<my_scrum.ScrumData> get getMyYearScrumsList {
    List<my_scrum.ScrumData> list = [];
    if (_myScrumYearlyResponse != null) {
      if (_myScrumYearlyResponse!.responseStatus == 1) {
        if (_myScrumYearlyResponse!.scrumData!.isNotEmpty) {
          list = _myScrumYearlyResponse!.scrumData!;
        }
      }
    }
    return list;
  }

  Future<my_scrum.MyScrumResponse?> viewMyScrumYearlyApi(String employeeId, String year) async {
    _isFetching = true;
    _isHavingData = false;
    _myScrumResponse = my_scrum.MyScrumResponse();
    try {
      dynamic response = await Repository().viewMyScrumYearly(employeeId, year);
      if (response != null) {
        _myScrumResponse = my_scrum.MyScrumResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_myScrumResponse");
    notifyListeners();
    return _myScrumResponse;
  }

  my_scrum_counts.ScrumCountsResponse? _scrumCountsResponse;

  List<my_scrum_counts.ScrumData> get getMyScrumCountsList {
    List<my_scrum_counts.ScrumData> list = [];
    if (_scrumCountsResponse != null) {
      if (_scrumCountsResponse!.responseStatus == 1) {
        if (_scrumCountsResponse!.scrumData!.isNotEmpty) {
          list = _scrumCountsResponse!.scrumData!;
        }
      }
    }
    return list;
  }

  Future<my_scrum_counts.ScrumCountsResponse?> viewMyScrumCountsApi(String employeeId, String filterType) async {
    _isFetching = true;
    _isHavingData = false;
    _scrumCountsResponse = my_scrum_counts.ScrumCountsResponse();
    try {
      dynamic response = await Repository().viewMyScrumCounts(employeeId, filterType);
      if (response != null) {
        _scrumCountsResponse = my_scrum_counts.ScrumCountsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_scrumCountsResponse");
    notifyListeners();
    return _scrumCountsResponse;
  }
  Future<BaseResponse?> addNewtaskToScrum(String  employeeId, String assignedEmployeeId,String projectId,String moduleId,String taskId,String taskType,String scrumDate,String taskName,String taskDescription,String estimationTime) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    try {
      dynamic response = await Repository().addNewtaskToScrum(employeeId, assignedEmployeeId, projectId, moduleId, taskId, taskType, scrumDate, taskName, taskDescription,estimationTime);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_scrumCountsResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<ModuleBasedTaskListResponse?> moduleBasedTaskListApi(String  employeeId,String moduleId) async {
    _isFetching = true;
    _isHavingData = false;
    ModuleBasedTaskListResponse? _moduleBasedTaskListResponse = ModuleBasedTaskListResponse();

    try {
      dynamic response = await Repository().moduleBasedTaskListApi(employeeId, moduleId);
      if (response != null) {
        _moduleBasedTaskListResponse = ModuleBasedTaskListResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_moduleBasedTaskListResponse");
    notifyListeners();
    return _moduleBasedTaskListResponse;
  }

  Future<teamwise_scrum.ScrumTeamwiseListResponseModel?> viewteamwiseScrumsApi(String employeeId, String startDate, String endDate,String filterMonth,String filterYear) async {
    _isFetching = true;
    _isHavingData = false;
     _teamwiseScrumResponse = teamwise_scrum.ScrumTeamwiseListResponseModel();
    try {
      dynamic response = await Repository().viewteamwiseScrum(employeeId, startDate, endDate, filterMonth, filterYear);
      if (response != null) {
        _teamwiseScrumResponse = teamwise_scrum.ScrumTeamwiseListResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_teamwiseScrumResponse");
    notifyListeners();
    return _teamwiseScrumResponse;
  }

  teamwise_scrum.ScrumTeamwiseListResponseModel? _teamwiseScrumResponse;

  List<teamwise_scrum.ScrumData> get getTeamWiseScrumsList {
    List<teamwise_scrum.ScrumData> list = [];
    if (_teamwiseScrumResponse != null) {
      if (_teamwiseScrumResponse!.responseStatus == 1) {
        if (_teamwiseScrumResponse!.scrumData!.isNotEmpty) {
          list = _teamwiseScrumResponse!.scrumData!;
        }
      }
    }
    return list;
  }

  EmployeeScrumReportResponse? _employeeScrumReportResponse;

  List<TasksCounts> get getEmployeeScrumReport {
    List<TasksCounts> list = [];
    if (_employeeScrumReportResponse != null) {
      if (_employeeScrumReportResponse!.responseStatus == 1) {
        if (_employeeScrumReportResponse!.tasksCount!.isNotEmpty) {
          list = _employeeScrumReportResponse!.tasksCount!;
        }
      }
    }
    return list;
  }

  Future<EmployeeScrumReportResponse?> getEmployeeScrumReportAPI(String employeeId, String filterType) async {
    _isFetching = true;
    _isHavingData = false;
    _employeeScrumReportResponse = EmployeeScrumReportResponse();
    try {
      dynamic response = await Repository().getEmployeeScrumReport(employeeId, filterType);
      if (response != null) {
        _employeeScrumReportResponse = EmployeeScrumReportResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeeScrumReportResponse");
    notifyListeners();
    return _employeeScrumReportResponse;
  }
  Future<BaseResponse?> closeEmployeeScrumTasksApi(String employeeId,String comment) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().closeEmployeeScrumTasksApi( employeeId,comment);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<ProjectwiseBugCountResponse?> viewProjectWiseBugsCountApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectwiseBugCountResponse? _projectwiseBugCountResponse = ProjectwiseBugCountResponse();
    try {
      dynamic response = await Repository().viewProjectWiseBugsCountApi( employeeId,);
      if (response != null) {
        _projectwiseBugCountResponse = ProjectwiseBugCountResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectwiseBugCountResponse");
    notifyListeners();
    return _projectwiseBugCountResponse;
  }
  Future<TaskTimingsResponse?> gettasktimingsApi(String employeeId,String taskId) async {
    _isFetching = true;
    _isHavingData = false;
    TaskTimingsResponse? _taskTimingsResponse = TaskTimingsResponse();
    try {
      dynamic response = await Repository().gettasktimingsApi( employeeId,taskId);
      if (response != null) {
        _taskTimingsResponse = TaskTimingsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_taskTimingsResponse");
    notifyListeners();
    return _taskTimingsResponse;
  }

}