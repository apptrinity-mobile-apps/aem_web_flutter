import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/technologies/ui/add_technology_fragment.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/adaptable_text.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TechnologyFragment extends StatefulWidget {
  const TechnologyFragment({Key? key}) : super(key: key);

  @override
  _TechnologyFragmentState createState() => _TechnologyFragmentState();
}

class _TechnologyFragmentState extends State<TechnologyFragment> {
  late TechnologyNotifier viewModel;
  String employeeId = "", technologyId = "";
  bool editPermission = false, addPermission = false;

  @override
  void initState() {
    viewModel = Provider.of<TechnologyNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.technologyEditPermission;
    addPermission = userViewModel.technologyAddPermission;
    PaintingBinding.instance!.imageCache!.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuTechnologies", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                          child: Column(children: [
                            const Divider(color: dashBoardCardBorderTile),
                            Expanded(
                              child: allTechnologiesList(),
                            )
                          ]),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuTechnologies, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionTechnology, softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: buttonDecorationGreen,
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: TextButton(
                    onPressed: () {
                      if (addPermission) {
                        Navigator.push(
                                context,
                                PageRouteBuilder(
                                    pageBuilder: (context, animation1, animation2) => const AddTechnologyFragment(),
                                    transitionDuration: const Duration(seconds: 0),
                                    reverseTransitionDuration: const Duration(seconds: 0)))
                            .then((value) {
                          imageCache!.clear();
                          // set state to reload data , reloads data in futureBuilder
                          setState(() {});
                        });
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text(addNewTechnology, style: newButtonsStyle),
                      ),
                    ),
                  ),
                ),
                /*InkWell(
                  onTap: () {},
                  child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                )*/
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget allTechnologiesList() {
    return FutureBuilder(
        future: viewModel.getTechnologiesAPI(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(child: Text('error ${snapshot.error}', style: logoutHeader));
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllTechnologiesResponse;
              if (data.responseStatus == 1) {
                if (data.technologyList!.isNotEmpty) {
                  return GridView.builder(
                      controller: ScrollController(),
                      physics: const ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: data.technologyList!.length,
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          childAspectRatio: 2.5, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 300),
                      itemBuilder: (BuildContext context, int index) {
                        return technologiesList(data.technologyList![index]);
                      });
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget technologiesList(TechnologiesList itemData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardBorderTile.withOpacity(1) : CupertinoColors.white,
          child: InkWell(
            onTap: () {
              if (editPermission) {
                Navigator.push(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) => AddTechnologyFragment(technologyId: itemData.id!),
                            transitionDuration: const Duration(seconds: 0),
                            reverseTransitionDuration: const Duration(seconds: 0)))
                    .then((value) {
                  // set state to reload data , reloads data in futureBuilder
                  setState(() {});
                });
              } else {
                showToast(noPermissionToAccess);
              }
            },
            child: Stack(
              children: [
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: itemData.image! == ""
                              ? Image.asset("assets/images/logo_2x.png", width: 30, height: 30)
                              : Image.network(
                                  itemData.image!,
                                  width: 30,
                                  height: 30,
                                  filterQuality: FilterQuality.medium,
                                  cacheHeight: 10,
                                  cacheWidth: 10,
                                ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                            child: AdaptableText(
                              itemData.name!.inCaps,
                              style: isHovered ? technologyItemHoveredStyle : technologyItemStyle,
                              textMaxLines: 2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                ),
                Positioned(
                  child: IconButton(
                    onPressed: () {
                      if (editPermission) {
                        showDeleteDialog(itemData);
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    icon: const Icon(Icons.delete_forever_outlined, color: newReOpened),
                  ),
                  right: 0,
                  top: 0,
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  showDeleteDialog(TechnologiesList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: '$technology?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteTechnologyAPI(itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
}
