import 'dart:convert';
import 'dart:typed_data';

import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/snack_bars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class AddTechnologyFragment extends StatefulWidget {
  final String? technologyId;

  const AddTechnologyFragment({this.technologyId, Key? key}) : super(key: key);

  @override
  _AddTechnologyFragmentState createState() => _AddTechnologyFragmentState();
}

class _AddTechnologyFragmentState extends State<AddTechnologyFragment> {
  DateTime selectedDate = DateTime.now();
  TextEditingController imageUploadController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late TechnologyNotifier viewModel;
  String employeeId = "", technologyId = "", image = "", technologyLogoImage = "";
  final _picker = ImagePicker();

  @override
  void initState() {
    viewModel = Provider.of<TechnologyNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    if (widget.technologyId != null) {
      getTechnology();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.technologyHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuTechnologies", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: cardBorder,
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                    child: Form(
                                      key: _formKey,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          addTechnologyName(),
                                          addTechnologyLogo(),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.technologyId == null ? addNewTechnology : updateTechnology, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddTechnology, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            if (widget.technologyId != null) {
                              await viewModel.updateTechnologyAPI(technologyId, nameController.text.trim(), image).then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            } else {
                              await viewModel.addTechnologyAPI(employeeId, nameController.text.trim(), image).then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget addTechnologyName() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(technologyName, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                      maxLines: 1,
                      controller: nameController,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                      inputFormatters: [UpperCaseTextFormatter()],
                      textCapitalization: TextCapitalization.sentences,
                      obscureText: false,
                      decoration: editTextTechnologyNameDecoration,
                      style: textFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget addTechnologyLogo() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(uploadImage, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 1,
                    controller: imageUploadController,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextTechnologyImageDecoration,
                    style: textFieldStyle,
                    onTap: () {
                      _uploadImage();
                    },
                    enableInteractiveSelection: false,
                    focusNode: FocusNode(),
                    readOnly: true, /*validator: emptyTextValidator*/
                  ),
                ),
              ),
            ),
            widget.technologyId != null
                ? technologyLogoImage != ""
                    ? SizedBox(
                        width: 80,
                        height: 80,
                        child: Image.network(technologyLogoImage, width: 80, height: 80, fit: BoxFit.fill, filterQuality: FilterQuality.medium),
                      )
                    : const SizedBox()
                : const SizedBox()
          ],
        ),
      ),
    );
  }

  Future<void> _uploadImage() async {
    final XFile? pickedImage = await _picker.pickImage(source: ImageSource.gallery, imageQuality: 100);
    if (pickedImage != null) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      debugPrint("path----" + pickedImage.name + "--" + pickedImage.mimeType!);
      if (pickedImage.mimeType!.contains("png") || pickedImage.mimeType!.contains("jpeg") || pickedImage.mimeType!.contains("jpg")) {
        // allow only jpg/jpeg/png
        imageUploadController.text = pickedImage.name;
        // convert to bytes
        Uint8List imageBytes = await pickedImage.readAsBytes();
        convertToBase64(imageBytes);
      } else {
        snackBarDark(context, unsupportedImageFormat);
      }
      Navigator.of(context).pop();
    }
  }

  Future<void> convertToBase64(Uint8List imageBytes) async {
    //convert bytes to base64 string
    String base64string = base64Encode(imageBytes);
    image = base64string;
    debugPrint("base64string $base64string");
    setState(() {});
  }

  void getTechnology() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewTechnologyAPI(widget.technologyId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              technologyId = value.technology!.id!;
              nameController.text = value.technology!.name!;
              technologyLogoImage = value.technology!.image!;
              List<int> list = technologyLogoImage.codeUnits;
              Uint8List bytes = Uint8List.fromList(list);
              convertToBase64(bytes);
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
