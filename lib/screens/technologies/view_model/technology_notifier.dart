import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TechnologyNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<AllTechnologiesResponse?> viewAllTechnologiesAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllTechnologiesResponse? _allTechnologiesResponse = AllTechnologiesResponse();

    try {
      dynamic response = await Repository().viewAllTechnologies(employeeId);
      if (response != null) {
        _allTechnologiesResponse = AllTechnologiesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allTechnologiesResponse");
    notifyListeners();
    return _allTechnologiesResponse;
  }

  Future<AllTechnologiesResponse?> getTechnologiesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    AllTechnologiesResponse? _allTechnologiesResponse = AllTechnologiesResponse();

    try {
      dynamic response = await Repository().getTechnologies();
      if (response != null) {
        _allTechnologiesResponse = AllTechnologiesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allTechnologiesResponse");
    notifyListeners();
    return _allTechnologiesResponse;
  }

  Future<BaseResponse?> addTechnologyAPI(String employeeId, String name, String image) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createTechnology(employeeId, name, image);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateTechnologyAPI(String technologyId, String name, String image) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateTechnology(technologyId, name, image);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteTechnologyAPI(String technologyId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteTechnology(technologyId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<AllTechnologiesResponse?> viewTechnologyAPI(String technologyId) async {
    _isFetching = true;
    _isHavingData = false;
    AllTechnologiesResponse? _allTechnologiesResponse = AllTechnologiesResponse();

    try {
      dynamic response = await Repository().viewTechnology(technologyId);
      if (response != null) {
        _allTechnologiesResponse = AllTechnologiesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allTechnologiesResponse");
    notifyListeners();
    return _allTechnologiesResponse;
  }
}
