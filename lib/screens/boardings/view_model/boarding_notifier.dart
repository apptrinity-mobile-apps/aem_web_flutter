import 'package:aem/model/all_boardings_response_model.dart';
import 'package:flutter/cupertino.dart';

import '../../../model/base_response_model.dart';
import '../../../services/repositories.dart';

class BoardingNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<BaseResponse?> addBoardingAPI(String name, String description, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createBoarding(name, description, employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<AllBoardingsResponse?> getAllBoardingsAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllBoardingsResponse? _baseResponse = AllBoardingsResponse();

    try {
      dynamic response = await Repository().getAllBoardings(employeeId);
      if (response != null) {
        _baseResponse = AllBoardingsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteBoardingApi(String boardingId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteBoarding(boardingId, employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateBoardingAPI(String boardingId, String employeeId, String boardName, String boardDescription) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateBoarding(boardingId, employeeId, boardName, boardDescription);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
}
