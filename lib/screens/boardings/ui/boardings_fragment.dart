import 'package:aem/screens/boardings/view_model/boarding_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';

import '../../../model/all_boardings_response_model.dart';
import '../../../utils/colors.dart';
import '../../../utils/constants.dart';
import '../../../utils/screen_config.dart';
import '../../../utils/strings.dart';
import '../../../widgets/adaptable_text.dart';
import '../../../widgets/custom_toast.dart';
import '../../../widgets/loader.dart';
import '../../../widgets/on_hover_card.dart';
import '../../login/view_model/login_notifier.dart';
import 'add_boarding_fragment.dart';

class BoardingsFragment extends StatefulWidget {
  const BoardingsFragment({Key? key}) : super(key: key);

  @override
  State<BoardingsFragment> createState() => _boardingsFragmentState();
}

class _boardingsFragmentState extends State<BoardingsFragment> {
  late BoardingNotifier viewModel = BoardingNotifier();
  String employeeId = "";
  bool editPermission = false, addPermission = false;

  @override
  void initState() {
    //viewModel = Provider.of<BoardingNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.boardingEditPermission;
    addPermission = userViewModel.boardingAddPermission;
    /* editPermission = userViewModel.projectEditPermission;
    addPermission = userViewModel.projectAddPermission;*/
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuBoarding", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: SizedBox(height: ScreenConfig.height(context), width: ScreenConfig.width(context), child: allBoardingsList()),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget allBoardingsList() {
    return FutureBuilder(
        future: viewModel.getAllBoardingsAPI(employeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllBoardingsResponse;
              if (data.responseStatus == 1) {
                if (data.boardingsList!.isNotEmpty) {
                  return GridView.builder(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      controller: ScrollController(),
                      physics: const ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: data.boardingsList!.length,
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 250),
                      itemBuilder: (BuildContext context, int index) {
                        return boardingList(data.boardingsList![index]);
                      });
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget boardingList(BoardingsList itemData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              if (editPermission) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => AddBoardingFragment(
                      boardingId: itemData.id!,
                      name: itemData.boardName,
                      description: itemData.boardDescription,
                    ),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  // set state to reload data , reloads data in futureBuilder
                  setState(() {});
                });
              } else {
                showToast(noPermissionToAccess);
              }
            },
            child: Stack(
              children: [
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 25, 20, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: AdaptableText(
                            itemData.boardName!.inCaps,
                            style: isHovered ? projectTitleHoveredStyle : projectTitleStyle,
                            textMaxLines: 3,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: HtmlWidget(
                            itemData.boardDescription!,
                            buildAsync: true,
                            enableCaching: false,
                            renderMode: RenderMode.column,
                            textStyle: isHovered ? projectDescHoveredStyle : projectDescStyle,
                          ),
                        ),
                        /* Expanded(
                          child: itemData.employees!.isNotEmpty
                              ? ListView.builder(
                              padding: const EdgeInsets.only(top: 5),
                              scrollDirection: Axis.horizontal,
                              itemCount: itemData.employees!.length > 4 ? 4 : itemData.employees!.length,
                              itemBuilder: (context, index) {
                                return index == 0
                                    ? Align(
                                  widthFactor: 1,
                                  child: Tooltip(
                                    message: itemData.employees![index].name!,
                                    decoration: toolTipDecoration,
                                    textStyle: toolTipStyle,
                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                        child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                      ),
                                    ),
                                  ),
                                )
                                    : Align(
                                  widthFactor: 0.8,
                                  child: Tooltip(
                                    message: itemData.employees![index].name!,
                                    decoration: toolTipDecoration,
                                    textStyle: toolTipStyle,
                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                        child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                      ),
                                    ),
                                  ),
                                );
                              })
                              : const SizedBox(),
                        ),*/
                      ],
                    ),
                  ),
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                ),
                Positioned(
                  child: IconButton(
                    onPressed: () {
                      if (editPermission) {
                        showDeleteDialog(itemData);
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    icon: Icon(Icons.delete_forever_outlined, color: isHovered ? dashBoardCardBorderTile : newReOpened),
                  ),
                  right: 0,
                  top: 0,
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  showDeleteDialog(BoardingsList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.boardName} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: '$boarding?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteBoardingApi(itemData.id!, employeeId).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(navMenuBoarding, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Text(dummy, softWrap: true, style: fragmentDescStyle),
                )
              ],
            ),
          ),
          Expanded(
            child: FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.scaleDown,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: buttonDecorationGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () {
                        if (addPermission) {
                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => AddBoardingFragment(),
                              transitionDuration: const Duration(seconds: 0),
                              reverseTransitionDuration: const Duration(seconds: 0),
                            ),
                          ).then((value) {
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            addNewBoarding,
                            style: newButtonsStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  /*InkWell(
                    onTap: () {},
                    child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                  )*/
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
