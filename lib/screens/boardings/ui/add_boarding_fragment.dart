import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';
import 'package:provider/provider.dart';

import '../../../utils/colors.dart';
import '../../../utils/constants.dart';
import '../../../utils/routes.dart';
import '../../../utils/screen_config.dart';
import '../../../utils/upper_case_formatter.dart';
import '../../../utils/validators.dart';
import '../../../widgets/custom_toast.dart';
import '../../../widgets/footer.dart';
import '../../../widgets/header.dart';
import '../../../widgets/left_drawer_panel.dart';
import '../../../widgets/loader.dart';
import '../../login/view_model/login_notifier.dart';
import '../view_model/boarding_notifier.dart';

class AddBoardingFragment extends StatefulWidget {
  final String? boardingId;

  final String? name;
  final String? description;

  const AddBoardingFragment({Key? key, this.boardingId, this.name, this.description}) : super(key: key);

  @override
  State<AddBoardingFragment> createState() => _AddBoardingFragmentState();
}

class _AddBoardingFragmentState extends State<AddBoardingFragment> {
  late BoardingNotifier viewModel = BoardingNotifier();
  String employeeId = "", projectId = "";
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  bool isVisible = true;
  HtmlEditorController htmlController = HtmlEditorController();
  String descriptionEdit = "";

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;

    print("des is ${widget.description}");

    if (widget.boardingId != null) {
      nameController.text = widget.name!;

      descriptionEdit = widget.description!;

      // htmlController.insertText(widget.description!);
      Future.delayed(const Duration(milliseconds: 1500), () {
        htmlController.insertHtml(widget.description!);
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {},
                    selectedMenu: "${RouteNames.boardingHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuBoarding", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                    child: Form(
                                      key: _formKey,
                                      child: PointerInterceptor(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [addboardingNameField(), addProjectDescriptionField()],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.boardingId == null ? project : updateBoarding, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(dummy, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            htmlController.getText().then((value) async {
                              if (widget.boardingId != null) {
                                await viewModel.updateBoardingAPI(widget.boardingId!, employeeId, nameController.text, value).then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              } else {
                                await viewModel.addBoardingAPI(nameController.text.trim(), value, employeeId).then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              }
                            });
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget addboardingNameField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(boardingName, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                      maxLines: 1,
                      controller: nameController,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      inputFormatters: [UpperCaseTextFormatter()],
                      textCapitalization: TextCapitalization.sentences,
                      obscureText: false,
                      decoration: editTextBoardingNameDecoration,
                      style: textFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget addProjectDescriptionField() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: SizedBox(
              width: 150,
              child: Text(description, style: addRoleSubStyle),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Visibility(
                visible: isVisible,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5), border: Border.all(color: textFieldBorder, width: 1.5), color: Colors.white),
                  height: 300,
                  child: HtmlEditor(
                    controller: htmlController,
                    htmlEditorOptions: const HtmlEditorOptions(
                      hint: 'Your text here...',
                      // initialText: descriptionEdit,
                      shouldEnsureVisible: true,
                      spellCheck: true,
                    ),
                    htmlToolbarOptions: HtmlToolbarOptions(
                      toolbarPosition: ToolbarPosition.aboveEditor,
                      toolbarType: ToolbarType.nativeExpandable,
                      initiallyExpanded: true,
                      defaultToolbarButtons: const [
                        StyleButtons(),
                        FontSettingButtons(fontSizeUnit: false),
                        FontButtons(clearAll: false),
                        ColorButtons(),
                        ListButtons(listStyles: false),
                        InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                      ],
                      onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                        return true;
                      },
                      onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                        return true;
                      },
                      mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                        return true;
                      },
                      mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                        return true;
                      },
                    ),
                    otherOptions: const OtherOptions(height: 250),
                    callbacks: Callbacks(
                        onBeforeCommand: (String? currentHtml) {},
                        onChangeContent: (String? changed) {},
                        onChangeCodeview: (String? changed) {},
                        onChangeSelection: (EditorSettings settings) {},
                        onDialogShown: () {},
                        onEnter: () {},
                        onFocus: () {},
                        onBlur: () {},
                        onBlurCodeview: () {},
                        onInit: () {},
                        onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                        onKeyDown: (int? keyCode) {},
                        onKeyUp: (int? keyCode) {},
                        onMouseDown: () {},
                        onMouseUp: () {},
                        onNavigationRequestMobile: (String url) {
                          return NavigationActionPolicy.ALLOW;
                        },
                        onPaste: () {},
                        onScroll: () {}),
                    plugins: [
                      SummernoteAtMention(
                          getSuggestionsMobile: (String value) {
                            var mentions = <String>['test1', 'test2', 'test3'];
                            return mentions
                                .where(
                                  (element) => element.contains(value),
                                )
                                .toList();
                          },
                          mentionsWeb: ['test1', 'test2', 'test3'],
                          onSelect: (String value) {}),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
