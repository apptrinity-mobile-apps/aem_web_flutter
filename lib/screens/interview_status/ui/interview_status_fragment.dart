import 'package:aem/model/all_interview_status_response_model.dart';
import 'package:aem/screens/interview_status/view_model/interview_status_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/snack_bars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InterviewStatusFragment extends StatefulWidget {
  const InterviewStatusFragment({Key? key}) : super(key: key);

  @override
  _InterviewStatusFragmentState createState() => _InterviewStatusFragmentState();
}

class _InterviewStatusFragmentState extends State<InterviewStatusFragment> {
  late InterviewStatusNotifier viewModel;
  String employeeId = "";
  bool editPermission = false, addPermission = false, viewPermission = false;

  @override
  void initState() {
    viewModel = Provider.of<InterviewStatusNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.interviewStatusEditPermission;
    addPermission = userViewModel.interviewStatusAddPermission;
    viewPermission = userViewModel.interviewStatusViewPermission;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("$home > $navMenuInterviewerStatus", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                          child: allStatusesList(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuInterviewerStatus, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionInterviewStatus, softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                addPermission
                    ? Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () {
                            showAddInterviewTypeDialog(null);
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(addInterviewerStatus, style: newButtonsStyle),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                /*InkWell(
                  onTap: () {},
                  child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                )*/
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget allStatusesList() {
    return FutureBuilder(
        future: viewModel.viewAllInterviewStatusesAPI(employeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllInterviewStatusResponseModel;
              if (data.responseStatus == 1) {
                if (data.interviewStatusList!.isNotEmpty) {
                  return Column(
                    children: [
                      listHeader(),
                      Flexible(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            controller: ScrollController(),
                            itemCount: data.interviewStatusList!.length,
                            itemBuilder: (context, index) {
                              return listItems(data.interviewStatusList![index], index);
                            }),
                      )
                    ],
                  );
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget listHeader() {
    return Container(
      decoration: buttonTopCircularDecorationGreen,
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(statusName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(status, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(action, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
            ),
          ),
        ],
      ),
    );
  }

  Widget listItems(InterviewStatusList itemData, int index) {
    var alternate = index % 2;
    bool isActivated = false;
    if (itemData.status == 1) {
      isActivated = true;
    } else if (itemData.status == 0) {
      isActivated = false;
    } else {
      isActivated = false;
    }
    return Container(
      color: alternate == 0 ? listItemColor : listItemColor1,
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 25, 5),
              child: Text(itemData.statusName!.inCaps, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: StatefulBuilder(
                builder: (context, _setState) {
                  return /*AnimatedToggleSwitch(
                      initialValue: isActivated,
                      width: ScreenConfig.width(context) / 9.5,
                      values: const ['Active', 'Inactive'],
                      onToggleCallback: (bool value) {
                        if (editPermission) {
                          _setState(() {
                            isActivated = value;
                          });
                          Future.delayed(const Duration(milliseconds: 160), () async {
                            await viewModel.activateOrDeActivateInterviewStatusesAPI(itemData.id!, itemData.createdBy!).then((value) {
                              if (viewModel.activateDeActivateInterviewStatusResponse != null) {
                                if (viewModel.activateDeActivateInterviewStatusResponse!.responseStatus == 1) {
                                  showToast(viewModel.activateDeActivateInterviewStatusResponse!.result!);
                                } else {
                                  showToast(viewModel.activateDeActivateInterviewStatusResponse!.result!);
                                }
                              } else {
                                showToast(pleaseTryAgain);
                              }
                            });
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                      buttonColor: buttonBg,
                      backgroundColor: sideMenuSelected,
                      textColor: CupertinoColors.white,
                    );*/
                      OutlinedButton(
                    onPressed: () {
                      if (editPermission) {
                        _setState(() {
                          isActivated = !isActivated;
                        });
                        Future.delayed(const Duration(milliseconds: 160), () async {
                          await viewModel.activateOrDeActivateInterviewStatusesAPI(itemData.id!, itemData.createdBy!).then((value) {
                            if (viewModel.activateDeActivateInterviewStatusResponse != null) {
                              if (viewModel.activateDeActivateInterviewStatusResponse!.responseStatus == 1) {
                                showToast(viewModel.activateDeActivateInterviewStatusResponse!.result!);
                              } else {
                                showToast(viewModel.activateDeActivateInterviewStatusResponse!.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                          // set state to reload data , reloads data in futureBuilder
                          setState(() {});
                        });
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        isActivated ? "Active" : "Inactive",
                        style: const TextStyle(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(isActivated ? buttonBg : dashBoardCardOverDueText),
                      overlayColor: MaterialStateProperty.all(isActivated ? buttonBg.withOpacity(0.1) : dashBoardCardOverDueText.withOpacity(0.1)),
                      side: MaterialStateProperty.all(BorderSide(color: isActivated ? buttonBg : dashBoardCardOverDueText)),
                      elevation: MaterialStateProperty.all(2),
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 5, 10, 5),
              child: Wrap(
                // mainAxisAlignment: MainAxisAlignment.end,
                alignment: WrapAlignment.end,
                crossAxisAlignment: WrapCrossAlignment.end,
                runAlignment: WrapAlignment.end,
                runSpacing: 4,
                spacing: 4,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: edit,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/edit_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (itemData.status == 1) {
                                if (editPermission) {
                                  showAddInterviewTypeDialog(itemData);
                                } else {
                                  showToast(noPermissionToAccess);
                                }
                              } else {
                                snackBarDark(context, "Interviewer status must be 'active' for updating.");
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: delete,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/delete_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (editPermission) {
                                showDeleteDialog(itemData);
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  showAddInterviewTypeDialog(InterviewStatusList? itemData) {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController nameController = TextEditingController();
        final _formKey = GlobalKey<FormState>();
        if (itemData != null) {
          nameController.text = itemData.statusName!;
        }
        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      itemData == null ? Text(addInterviewerStatus, style: profilepopHeader) : Text(updateInterviewerStatus, style: profilepopHeader),
                      IconButton(
                          icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                  Text(descriptionAddInterviewStatus, softWrap: true, style: fragmentDescStyle),
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 2.3,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text('Interviewer Status Name', style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: TextFormField(
                                                maxLines: 1,
                                                controller: nameController,
                                                textInputAction: TextInputAction.next,
                                                keyboardType: TextInputType.text,
                                                inputFormatters: [UpperCaseTextFormatter()],
                                                textCapitalization: TextCapitalization.sentences,
                                                obscureText: false,
                                                decoration: InputDecoration(
                                                    fillColor: CupertinoColors.white,
                                                    filled: true,
                                                    border: border,
                                                    isDense: true,
                                                    enabledBorder: border,
                                                    focusedBorder: border,
                                                    errorBorder: errorBorder,
                                                    focusedErrorBorder: errorBorder,
                                                    contentPadding: editTextPadding,
                                                    hintStyle: textFieldHintStyle,
                                                    hintText: 'Interviewer status name'),
                                                style: textFieldStyle,
                                                validator: emptyTextValidator,
                                                autovalidateMode: AutovalidateMode.onUserInteraction),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) {
                                                return loader();
                                              });
                                          if (itemData == null) {
                                            await viewModel.addInterviewStatusesAPI(nameController.text.trim(), employeeId).then((value) {
                                              Navigator.of(context).pop();
                                              if (viewModel.addInterviewStatusesResponse!.responseStatus == 1) {
                                                showToast(viewModel.addInterviewStatusesResponse!.result!);
                                                // set state to reload data , reloads data in futureBuilder
                                                setState(() {});
                                                Navigator.of(context).pop();
                                              } else {
                                                showToast(viewModel.addInterviewStatusesResponse!.result!);
                                              }
                                            });
                                          } else {
                                            await viewModel
                                                .updateInterviewStatusesAPI(nameController.text.trim(), itemData.id!, itemData.createdBy!)
                                                .then((value) {
                                              Navigator.of(context).pop();
                                              if (viewModel.updateInterviewStatusesResponse!.responseStatus == 1) {
                                                showToast(viewModel.updateInterviewStatusesResponse!.result!);
                                                // set state to reload data , reloads data in futureBuilder
                                                setState(() {});
                                                Navigator.of(context).pop();
                                              } else {
                                                showToast(viewModel.updateInterviewStatusesResponse!.result!);
                                              }
                                            });
                                          }
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(submit.toUpperCase(), style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  showDeleteDialog(InterviewStatusList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.statusName} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'Interviewer Status?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteInterviewStatusesAPI(itemData.id!, itemData.createdBy!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
}
