import 'package:aem/model/all_interview_status_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/interview_status_details_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class InterviewStatusNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  AllInterviewStatusResponseModel? _allInterviewStatusResponseModel;
  AllInterviewStatusResponseModel? _interviewStatusesResponseModel;

  List<InterviewStatusList>? get viewAllInterviewStatuses {
    if (_allInterviewStatusResponseModel != null) {
      if (_allInterviewStatusResponseModel!.responseStatus == 1) {
        return _allInterviewStatusResponseModel!.interviewStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
  List<InterviewStatusList>? get getInterviewStatuses {
    if (_interviewStatusesResponseModel != null) {
      if (_interviewStatusesResponseModel!.responseStatus == 1) {
        return _interviewStatusesResponseModel!.interviewStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<AllInterviewStatusResponseModel?> viewAllInterviewStatusesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allInterviewStatusResponseModel = AllInterviewStatusResponseModel();
    try {
      dynamic response = await Repository().viewAllInterviewStatuses(createdBy);
      if (response != null) {
        _allInterviewStatusResponseModel = AllInterviewStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allInterviewStatusResponseModel");
    notifyListeners();
    return _allInterviewStatusResponseModel;
  }

  Future<AllInterviewStatusResponseModel?> getInterviewStatusesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _interviewStatusesResponseModel = AllInterviewStatusResponseModel();
    try {
      dynamic response = await Repository().getInterviewerStatuses();
      if (response != null) {
        _interviewStatusesResponseModel = AllInterviewStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_interviewStatusesResponseModel");
    notifyListeners();
    return _interviewStatusesResponseModel;
  }

  AllInterviewStatusResponseModel? _allActiveInterviewStatusResponseModel;

  List<InterviewStatusList>? get getAllActiveInterviewStatuses {
    if (_allActiveInterviewStatusResponseModel != null) {
      if (_allActiveInterviewStatusResponseModel!.responseStatus == 1) {
        return _allActiveInterviewStatusResponseModel!.interviewStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<AllInterviewStatusResponseModel?> getAllActiveInterviewStatusesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allActiveInterviewStatusResponseModel = AllInterviewStatusResponseModel();
    try {
      dynamic response = await Repository().getAllActiveInterviewStatuses(createdBy);
      if (response != null) {
        _allActiveInterviewStatusResponseModel = AllInterviewStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allActiveInterviewStatusResponseModel");
    notifyListeners();
    return _allActiveInterviewStatusResponseModel;
  }

  BaseResponse? _addInterviewStatusesResponse;

  BaseResponse? get addInterviewStatusesResponse {
    return _addInterviewStatusesResponse;
  }

  Future<BaseResponse?> addInterviewStatusesAPI(String statusName, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _addInterviewStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().addInterviewStatuses(statusName, createdBy);
      if (response != null) {
        _addInterviewStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addInterviewStatusesResponse");
    notifyListeners();
    return _addInterviewStatusesResponse;
  }

  InterviewStatusDetailsResponseModel? _interviewStatusDetailsResponseModel;

  InterviewStatusDetails? get getInterviewStatusDetails {
    if (_interviewStatusDetailsResponseModel != null) {
      if (_interviewStatusDetailsResponseModel!.responseStatus == 1) {
        return _interviewStatusDetailsResponseModel!.interviewStatusDetails;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  Future<InterviewStatusDetailsResponseModel?> viewInterviewStatusesAPI(String interviewStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _interviewStatusDetailsResponseModel = InterviewStatusDetailsResponseModel();
    try {
      dynamic response = await Repository().viewInterviewStatuses(interviewStatusId, createdBy);
      if (response != null) {
        _interviewStatusDetailsResponseModel = InterviewStatusDetailsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_interviewStatusDetailsResponseModel");
    notifyListeners();
    return _interviewStatusDetailsResponseModel;
  }

  BaseResponse? _updateInterviewStatusesResponse;

  BaseResponse? get updateInterviewStatusesResponse {
    return _updateInterviewStatusesResponse;
  }

  Future<BaseResponse?> updateInterviewStatusesAPI(String statusName, String interviewStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _updateInterviewStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().updateInterviewStatuses(statusName, interviewStatusId, createdBy);
      if (response != null) {
        _updateInterviewStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateInterviewStatusesResponse");
    notifyListeners();
    return _updateInterviewStatusesResponse;
  }

  BaseResponse? _deleteInterviewStatusesResponse;

  BaseResponse? get deleteInterviewStatusesResponse {
    return _deleteInterviewStatusesResponse;
  }

  Future<BaseResponse?> deleteInterviewStatusesAPI(String interviewStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _deleteInterviewStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().deleteInterviewStatuses(interviewStatusId, createdBy);
      if (response != null) {
        _deleteInterviewStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteInterviewStatusesResponse");
    notifyListeners();
    return _deleteInterviewStatusesResponse;
  }

  BaseResponse? _activateDeActivateInterviewStatusResponse;

  BaseResponse? get activateDeActivateInterviewStatusResponse {
    return _activateDeActivateInterviewStatusResponse;
  }

  Future<BaseResponse?> activateOrDeActivateInterviewStatusesAPI(String interviewStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _activateDeActivateInterviewStatusResponse = BaseResponse();
    try {
      dynamic response = await Repository().activateOrDeActivateInterviewStatuses(interviewStatusId, createdBy);
      if (response != null) {
        _activateDeActivateInterviewStatusResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_activateDeActivateInterviewStatusResponse");
    notifyListeners();
    return _activateDeActivateInterviewStatusResponse;
  }
}
