import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class MyProfileNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  BaseResponse? _changePasswordResponse;

  BaseResponse? get changePasswordResponse => _changePasswordResponse;

  Future<BaseResponse?> changePasswordAPI(String employeeId, String password, String newPassword, String confirmNewPassword) async {
    _isFetching = true;
    _isHavingData = false;
    _changePasswordResponse = BaseResponse();

    try {
      dynamic response = await Repository().changePassword(employeeId, password, newPassword, confirmNewPassword);
      if (response != null) {
        _changePasswordResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_changePasswordResponse");
    notifyListeners();
    return _changePasswordResponse;
  }

  BaseResponse? _updateProfileResponse;

  BaseResponse? get updateProfileResponse => _updateProfileResponse;

  Future<BaseResponse?> loginProfileUpdateAPI(
      String name, String employeeId, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration, String durationType) async {
    _isFetching = true;
    _isHavingData = false;
    _updateProfileResponse = BaseResponse();

    try {
      dynamic response = await Repository().loginProfileUpdate(name, employeeId, phoneNumber, availabilitySchedule, duration, durationType);
      if (response != null) {
        _updateProfileResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateProfileResponse");
    notifyListeners();
    return _updateProfileResponse;
  }
}
