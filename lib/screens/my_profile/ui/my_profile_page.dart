import 'package:aem/screens/my_profile/view_model/my_profile_notifier.dart';
import 'package:aem/screens/splash/splash_screen.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:flutter/material.dart';

import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/employee_interview_duration_model.dart';
import 'package:aem/model/employee_availability_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class MyProfilePage extends StatefulWidget {
  const MyProfilePage({Key? key}) : super(key: key);

  @override
  State<MyProfilePage> createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  DateTime selectedDate = DateTime.now();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController rolesController = TextEditingController();
  TextEditingController designationsController = TextEditingController();
  TextEditingController technologiesController = TextEditingController();
  TextEditingController customDurationController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  ScrollController schedulesController = ScrollController();

  EmployeeInterviewDurationModel durationInitialValue = EmployeeInterviewDurationModel(displayTitle: select, value: 0);

  final _formKey = GlobalKey<FormState>();
  final _passwordFormKey = GlobalKey<FormState>();
  final _durationDropDownKey = GlobalKey<FormFieldState>();

  late EmployeeNotifier employeesViewModel;
  late MyProfileNotifier viewModel;
  String employeeId = "";
  List<DropdownMenuItem<EmployeeInterviewDurationModel>> durationMenuItems = [];
  List<EmployeeInterviewDurationModel> durationsList = [];
  List<EmployeeAvailabilityModel> schedulesList = [];
  TimeOfDay selectedTime = TimeOfDay.now();

  @override
  void initState() {
    employeesViewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    viewModel = Provider.of<MyProfileNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    // schedules
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: sun, displayValue: sun, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: mon, displayValue: mon, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: tue, displayValue: tue, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: wed, displayValue: wed, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: thu, displayValue: thu, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: fri, displayValue: fri, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: sat, displayValue: sat, durationsList: [], value: false));
    // adding durations
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min15, value: 15));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min30, value: 30));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min45, value: 45));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min60, value: 60));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: custom, value: 0));
    for (var element in durationsList) {
      durationMenuItems.add(DropdownMenuItem(child: Text(element.displayTitle!), value: element));
    }
    getEmployeeData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $myProfile", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        height: ScreenConfig.height(context),
                        margin: const EdgeInsets.fromLTRB(5, 15, 5, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: ListView(
                                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                controller: schedulesController,
                                shrinkWrap: true,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    decoration: cardBorder,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(15, 15, 15, 25),
                                      child: Form(
                                        key: _formKey,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                                              child: SizedBox(
                                                child: Text(
                                                  profile,
                                                  style: TextStyle(
                                                      letterSpacing: 0,
                                                      fontFamily: "Poppins",
                                                      fontWeight: FontWeight.w700,
                                                      color: addRoleText,
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [nameField(), /*designationField()*/ designationFieldMulti()],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [emailField(), /*technologyField()*/ technologyFieldMulti()],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [phoneField(), /*roleFieldMulti()*/ roleField()],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    decoration: cardBorder,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(15, 15, 15, 25),
                                      child: Form(
                                        key: _passwordFormKey,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                const Padding(
                                                  padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                                                  child: SizedBox(
                                                    child: Text(
                                                      'Change Password',
                                                      style: TextStyle(
                                                          letterSpacing: 0,
                                                          fontFamily: "Poppins",
                                                          fontWeight: FontWeight.w700,
                                                          color: addRoleText,
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  decoration: buttonDecorationGreen,
                                                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                  child: TextButton(
                                                    onPressed: () async {
                                                      if (_passwordFormKey.currentState!.validate()) {
                                                        if (newPasswordController.text.toString().trim() ==
                                                            confirmPasswordController.text.toString().trim()) {
                                                          showDialog(
                                                              context: context,
                                                              barrierDismissible: false,
                                                              builder: (BuildContext context) {
                                                                return loader();
                                                              });
                                                          await viewModel
                                                              .changePasswordAPI(
                                                                  employeeId,
                                                                  passwordController.text.trim().toString(),
                                                                  newPasswordController.text.trim().toString(),
                                                                  confirmPasswordController.text.trim().toString())
                                                              .then((value) {
                                                            Navigator.of(context).pop();
                                                            if (viewModel.changePasswordResponse != null) {
                                                              if (viewModel.changePasswordResponse!.responseStatus == 1) {
                                                                showToast(viewModel.changePasswordResponse!.result!);
                                                                SessionManager().clearSession();
                                                                Provider.of<LoginNotifier>(context, listen: false).resetValues();
                                                                Navigator.of(context).pop();
                                                                Navigator.pushAndRemoveUntil(
                                                                    context,
                                                                    PageRouteBuilder(
                                                                      pageBuilder: (context, animation1, animation2) => const SplashScreen(),
                                                                      transitionDuration: const Duration(seconds: 0),
                                                                      reverseTransitionDuration: const Duration(seconds: 0),
                                                                    ),
                                                                    (route) => false);
                                                              } else {
                                                                showToast(viewModel.changePasswordResponse!.result!);
                                                              }
                                                            } else {
                                                              showToast(pleaseTryAgain);
                                                            }
                                                          });
                                                        } else {
                                                          showToast("New passwords doesn't match");
                                                        }
                                                      }
                                                    },
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
                                                      child: FittedBox(
                                                        fit: BoxFit.scaleDown,
                                                        child: Text(update, style: newButtonsStyle),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [currentPasswordField(), const Expanded(child: SizedBox())],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [newPasswordField(), confirmNewPasswordField()],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    decoration: cardBorder,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(15, 15, 15, 25),
                                      child: Form(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                                              child: SizedBox(
                                                child: Text(
                                                  'Interview Duration',
                                                  style: TextStyle(
                                                      letterSpacing: 0,
                                                      fontFamily: "Poppins",
                                                      fontWeight: FontWeight.w700,
                                                      color: addRoleText,
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [durationField(), const Expanded(child: SizedBox())],
                                              ),
                                            ),
                                            durationInitialValue.displayTitle!.toLowerCase() == custom.toLowerCase()
                                                ? Padding(
                                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [customDurationField(), const Expanded(child: SizedBox())],
                                                    ),
                                                  )
                                                : const SizedBox(),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 5),
                                      child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                                              child: SizedBox(
                                                child: Text(
                                                  'Interview Schedule',
                                                  style: TextStyle(
                                                      letterSpacing: 0,
                                                      fontFamily: "Poppins",
                                                      fontWeight: FontWeight.w700,
                                                      color: addRoleText,
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ),
                                            ListView.builder(
                                              physics: const ClampingScrollPhysics(),
                                              scrollDirection: Axis.vertical,
                                              itemCount: schedulesList.length,
                                              shrinkWrap: true,
                                              itemBuilder: (context, index) {
                                                return schedulesWidget(schedulesList[index]);
                                              },
                                            ),
                                          ]),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(myProfile, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 3),
                      child: Text(dummy, softWrap: true, style: fragmentDescStyle),
                    )
                  ],
                ),
              ),
              Expanded(
                child: FittedBox(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.scaleDown,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              var schedules = AvailabilitySchedules();
                              int duration = 0;
                              String type = '';
                              for (var element in schedulesList) {
                                if (element.displayValue == sun) {
                                  schedules.sundayAvailability = element.value;
                                  schedules.sundayTimings = element.durationsList;
                                } else if (element.displayValue == mon) {
                                  schedules.mondayAvailability = element.value;
                                  schedules.mondayTimings = element.durationsList;
                                } else if (element.displayValue == tue) {
                                  schedules.tuesdayAvailability = element.value;
                                  schedules.tuesdayTimings = element.durationsList;
                                } else if (element.displayValue == wed) {
                                  schedules.wednesdayAvailability = element.value;
                                  schedules.wednesdayTimings = element.durationsList;
                                } else if (element.displayValue == thu) {
                                  schedules.thursdayAvailability = element.value;
                                  schedules.thursdayTimings = element.durationsList;
                                } else if (element.displayValue == fri) {
                                  schedules.fridayAvailability = element.value;
                                  schedules.fridayTimings = element.durationsList;
                                } else if (element.displayValue == sat) {
                                  schedules.saturdayAvailability = element.value;
                                  schedules.saturdayTimings = element.durationsList;
                                }
                              }
                              setState(() {
                                if (durationInitialValue.displayTitle!.toLowerCase() != custom.toLowerCase()) {
                                  duration = durationInitialValue.value!;
                                  type = fixed.toLowerCase();
                                } else {
                                  duration = int.parse(customDurationController.text.toString());
                                  type = durationInitialValue.displayTitle!.toLowerCase();
                                }
                              });
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              await viewModel
                                  .loginProfileUpdateAPI(nameController.text.toString().trim(), employeeId, phoneController.text.toString().trim(),
                                      schedules, duration, type)
                                  .then((value) {
                                Navigator.of(context).pop();
                                if (viewModel.updateProfileResponse != null) {
                                  if (viewModel.updateProfileResponse!.responseStatus == 1) {
                                    showToast(viewModel.updateProfileResponse!.result!);
                                    // getEmployeeData();
                                  } else {
                                    showToast(viewModel.updateProfileResponse!.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(save, style: newButtonsStyle),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget nameField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(name, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: nameController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.text,
                        inputFormatters: [UpperCaseTextFormatter()],
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: false,
                        decoration: editTextNameDecoration,
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget emailField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(email, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                      maxLines: 1,
                      readOnly: true,
                      toolbarOptions: const ToolbarOptions(copy: true, selectAll: true),
                      enabled: true,
                      controller: emailController,
                      textInputAction: TextInputAction.done,
                      obscureText: false,
                      decoration: editTextEmailDecoration,
                      style: textFieldStyle,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget phoneField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(phone, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: phoneController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          LengthLimitingTextInputFormatter(10)
                        ],
                        obscureText: false,
                        decoration: editTextPhoneNumberDecoration,
                        style: textFieldStyle,
                        validator: phoneValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget designationFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(designation, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                      maxLines: 5,
                      minLines: 1,
                      readOnly: true,
                      toolbarOptions: const ToolbarOptions(copy: true, selectAll: true),
                      enabled: true,
                      controller: designationsController,
                      textInputAction: TextInputAction.done,
                      obscureText: false,
                      decoration: editTextDesignationDecoration,
                      style: const TextStyle(
                          letterSpacing: 0,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w400,
                          color: editText,
                          fontSize: 16,
                          overflow: TextOverflow.ellipsis),
                    ),
                    /*DropdownSearch<DesignationsList>.multiSelection(
                        key: _multiDesignationKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedDesignations.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: designationsList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedDesignations = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedDesignations,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map((item) => Container(
                                        padding: const EdgeInsets.all(5),
                                        margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                        decoration:
                                            BoxDecoration(borderRadius: BorderRadius.circular(10), color: Theme.of(context).primaryColorLight),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(item.name!, textAlign: TextAlign.center, style: textFieldStyle),
                                            MaterialButton(
                                              height: 20,
                                              shape: const CircleBorder(),
                                              focusColor: Colors.red[200],
                                              hoverColor: Colors.red[200],
                                              padding: const EdgeInsets.all(0),
                                              minWidth: 34,
                                              onPressed: () {
                                                _multiDesignationKey.currentState?.removeItem(item);
                                              },
                                              child: const Icon(Icons.close_outlined, size: 20),
                                            )
                                          ],
                                        ),
                                      ))
                                  .toList());
                        },
                        filterFn: (DesignationsList? designation, name) {
                          return designation!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                              padding: const EdgeInsets.all(10), child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown));
                        },
                        popupSelectionWidget: (cnt, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),*/
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget technologyFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(technology, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                      maxLines: 5,
                      minLines: 1,
                      readOnly: true,
                      toolbarOptions: const ToolbarOptions(copy: true, selectAll: true),
                      enabled: true,
                      controller: technologiesController,
                      textInputAction: TextInputAction.done,
                      obscureText: false,
                      decoration: editTextTechnologyDecoration,
                      style: const TextStyle(
                          letterSpacing: 0,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w400,
                          color: editText,
                          fontSize: 16,
                          overflow: TextOverflow.ellipsis),
                    ),
                    /*DropdownSearch<TechnologiesList>.multiSelection(
                        key: _multiTechnologyKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedTechnologies.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: technologiesList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedTechnologies = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedTechnologies,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map((item) => Container(
                                        padding: const EdgeInsets.all(5),
                                        margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                        decoration:
                                            BoxDecoration(borderRadius: BorderRadius.circular(10), color: Theme.of(context).primaryColorLight),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(item.name!, textAlign: TextAlign.center, style: textFieldStyle),
                                            MaterialButton(
                                              height: 20,
                                              shape: const CircleBorder(),
                                              focusColor: Colors.red[200],
                                              hoverColor: Colors.red[200],
                                              padding: const EdgeInsets.all(0),
                                              minWidth: 34,
                                              onPressed: () {
                                                _multiTechnologyKey.currentState?.removeItem(item);
                                              },
                                              child: const Icon(Icons.close_outlined, size: 20),
                                            )
                                          ],
                                        ),
                                      ))
                                  .toList());
                        },
                        filterFn: (TechnologiesList? designation, name) {
                          return designation!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                              padding: const EdgeInsets.all(10), child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown));
                        },
                        popupSelectionWidget: (cnt, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),*/
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget roleField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(role, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                      maxLines: 5,
                      minLines: 1,
                      readOnly: true,
                      toolbarOptions: const ToolbarOptions(copy: true, selectAll: true),
                      enabled: true,
                      controller: rolesController,
                      textInputAction: TextInputAction.done,
                      obscureText: false,
                      decoration: editTextRoleDecoration,
                      style: const TextStyle(
                          letterSpacing: 0,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w400,
                          color: editText,
                          fontSize: 16,
                          overflow: TextOverflow.ellipsis),
                    ),
                    /*DropdownButtonFormField<RolesDetails>(
                      items: rolesMenuItems,
                      decoration: editTextRoleDecoration,
                      style: textFieldStyle,
                      value: roleInitialValue,
                      validator: rolesDropDownValidator,
                      onChanged: (RolesDetails? value) {
                        setState(() {
                          roleInitialValue = value!;
                        });
                      },
                    ),*/
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget currentPasswordField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(currentPassword, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: passwordController,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: true,
                        obscuringCharacter: '*',
                        decoration: InputDecoration(
                            fillColor: CupertinoColors.white,
                            filled: true,
                            isDense: true,
                            border: border,
                            enabledBorder: border,
                            focusedBorder: border,
                            errorBorder: errorBorder,
                            focusedErrorBorder: errorBorder,
                            contentPadding: editTextPadding,
                            hintStyle: textFieldHintStyle,
                            hintText: currentPassword),
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget newPasswordField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(newPassword, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: newPasswordController,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: true,
                        obscuringCharacter: '*',
                        decoration: InputDecoration(
                            fillColor: CupertinoColors.white,
                            filled: true,
                            isDense: true,
                            border: border,
                            enabledBorder: border,
                            focusedBorder: border,
                            errorBorder: errorBorder,
                            focusedErrorBorder: errorBorder,
                            contentPadding: editTextPadding,
                            hintStyle: textFieldHintStyle,
                            hintText: newPassword),
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget confirmNewPasswordField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(confirmPassword, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: confirmPasswordController,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: true,
                        obscuringCharacter: '*',
                        decoration: InputDecoration(
                            fillColor: CupertinoColors.white,
                            filled: true,
                            isDense: true,
                            border: border,
                            enabledBorder: border,
                            focusedBorder: border,
                            errorBorder: errorBorder,
                            focusedErrorBorder: errorBorder,
                            contentPadding: editTextPadding,
                            hintStyle: textFieldHintStyle,
                            hintText: confirmPassword),
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  /* multi select roles - uncomment this */
  /*Widget roleFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(role, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownSearch<RolesDetails>.multiSelection(
                        key: _multiRoleKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedRoles.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: rolesList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedRoles = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedRoles,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map((item) => Container(
                                        padding: const EdgeInsets.all(5),
                                        margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: buttonBg),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(item.role!, textAlign: TextAlign.center, style: textFieldStyle),
                                            MaterialButton(
                                              height: 20,
                                              shape: const CircleBorder(),
                                              focusColor: Colors.red[200],
                                              hoverColor: Colors.red[200],
                                              padding: const EdgeInsets.all(0),
                                              minWidth: 34,
                                              onPressed: () {
                                                _multiRoleKey.currentState?.removeItem(item);
                                              },
                                              child: const Icon(Icons.close_outlined, size: 20),
                                            )
                                          ],
                                        ),
                                      ))
                                  .toList());
                        },
                        filterFn: (RolesDetails? role, name) {
                          return role!.role!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                              padding: const EdgeInsets.all(10), child: Text(text.role!, style: isSelected ? dropDownSelected : dropDown));
                        },
                        popupSelectionWidget: (_, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }*/

  Widget durationField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text('Duration', style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownButtonFormField<EmployeeInterviewDurationModel>(
                      key: _durationDropDownKey,
                      items: durationMenuItems,
                      decoration: InputDecoration(
                          fillColor: CupertinoColors.white,
                          filled: true,
                          border: border,
                          isDense: true,
                          enabledBorder: border,
                          focusedBorder: border,
                          errorBorder: errorBorder,
                          focusedErrorBorder: errorBorder,
                          contentPadding: editTextPadding,
                          hintStyle: textFieldHintStyle,
                          hintText: 'Duration'),
                      style: textFieldStyle,
                      // value: roleInitialValue,
                      // validator: rolesDropDownValidator,
                      onChanged: (EmployeeInterviewDurationModel? value) {
                        setState(() {
                          durationInitialValue = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget customDurationField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text('Minutes', style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: customDurationController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          LengthLimitingTextInputFormatter(10)
                        ],
                        obscureText: false,
                        decoration: InputDecoration(
                            fillColor: CupertinoColors.white,
                            filled: true,
                            border: border,
                            isDense: true,
                            enabledBorder: border,
                            focusedBorder: border,
                            errorBorder: errorBorder,
                            focusedErrorBorder: errorBorder,
                            contentPadding: editTextPadding,
                            hintStyle: textFieldHintStyle,
                            hintText: 'Enter value in minutes'),
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget schedulesWidget(EmployeeAvailabilityModel scheduleModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
            activeColor: buttonBg,
            hoverColor: buttonBg.withOpacity(0.2),
            value: scheduleModel.value!,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            side: const BorderSide(color: buttonBg),
            onChanged: (value) {
              WidgetsBinding.instance!.addPostFrameCallback((_) {
                schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                    duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
              });
              setState(() {
                scheduleModel.value = value!;
                if (scheduleModel.value!) {
                  if (scheduleModel.durationsList!.isEmpty) {
                    scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                  }
                } else {
                  scheduleModel.durationsList!.clear();
                }
              });
            }),
        Padding(
          padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
          child: SizedBox(
            width: 150,
            child: Text(scheduleModel.displayTitle!.substring(0, 3).toUpperCase(), textAlign: TextAlign.start, style: addRoleSubStyle),
          ),
        ),
        scheduleModel.value!
            ? Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: scheduleModel.durationsList!.length,
                      itemBuilder: (context, index) {
                        List<TextEditingController> startDateControllers = [];
                        List<TextEditingController> endDateControllers = [];
                        for (var e in scheduleModel.durationsList!) {
                          startDateControllers.add(TextEditingController(text: e.startTime));
                          endDateControllers.add(TextEditingController(text: e.endTime));
                        }
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  width: 180,
                                  alignment: Alignment.center,
                                  child: TextFormField(
                                    controller: startDateControllers[index],
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textCapitalization: TextCapitalization.sentences,
                                    obscureText: false,
                                    // validator: emptyTextValidator,
                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                    decoration: InputDecoration(
                                        fillColor: CupertinoColors.white,
                                        filled: true,
                                        border: border,
                                        isDense: true,
                                        enabledBorder: border,
                                        focusedBorder: border,
                                        errorBorder: errorBorder,
                                        focusedErrorBorder: errorBorder,
                                        contentPadding: editTextPadding,
                                        hintStyle: textFieldHintStyle,
                                        hintText: '00:00 AM'),
                                    style: textFieldStyle,
                                    textAlignVertical: TextAlignVertical.center,
                                    readOnly: true,
                                    enabled: true,
                                    onTap: () async {
                                      final TimeOfDay? timeOfDay = await showTimePicker(
                                        context: context,
                                        initialTime: selectedTime,
                                        initialEntryMode: TimePickerEntryMode.dial,
                                        confirmText: "CONFIRM",
                                        cancelText: "NOT NOW",
                                        helpText: "AVAILABLE TIME",
                                      );
                                      if (timeOfDay != null) {
                                        setState(() {
                                          scheduleModel.durationsList![index].startTime = formatTimeOfDay(timeOfDay);
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 5, right: 5),
                                alignment: Alignment.center,
                                child: const Text(
                                  '-',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 16),
                                ),
                              ),
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  width: 180,
                                  alignment: Alignment.center,
                                  child: TextFormField(
                                    controller: endDateControllers[index],
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textCapitalization: TextCapitalization.sentences,
                                    obscureText: false,
                                    // validator: emptyTextValidator,
                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                    decoration: InputDecoration(
                                        fillColor: CupertinoColors.white,
                                        filled: true,
                                        border: border,
                                        isDense: true,
                                        enabledBorder: border,
                                        focusedBorder: border,
                                        errorBorder: errorBorder,
                                        focusedErrorBorder: errorBorder,
                                        contentPadding: editTextPadding,
                                        hintStyle: textFieldHintStyle,
                                        hintText: '00:00 AM'),
                                    style: textFieldStyle,
                                    textAlignVertical: TextAlignVertical.center,
                                    readOnly: true,
                                    enabled: true,
                                    onTap: () async {
                                      final TimeOfDay? timeOfDay = await showTimePicker(
                                        context: context,
                                        initialTime: selectedTime,
                                        initialEntryMode: TimePickerEntryMode.dial,
                                        confirmText: "CONFIRM",
                                        cancelText: "NOT NOW",
                                        helpText: "AVAILABLE TIME",
                                      );
                                      if (timeOfDay != null) {
                                        setState(() {
                                          scheduleModel.durationsList![index].endTime = formatTimeOfDay(timeOfDay);
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                              scheduleModel.durationsList!.length == 1
                                  ? Container(
                                      margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                      child: OnHoverCard(builder: (onHovered) {
                                        return InkWell(
                                          onTap: () {
                                            WidgetsBinding.instance!.addPostFrameCallback((_) {
                                              schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                                                  duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
                                            });
                                            setState(() {
                                              scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                                            });
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                borderRadius: BorderRadius.circular(5),
                                                color: onHovered ? buttonBg : CupertinoColors.white),
                                            child: Icon(
                                              Icons.add,
                                              size: 24,
                                              color: onHovered ? CupertinoColors.white : addScheduleButton,
                                            ),
                                          ),
                                        );
                                      }),
                                    )
                                  : scheduleModel.durationsList!.length - 1 == index
                                      ? Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(15, 0, 7, 0),
                                              child: OnHoverCard(builder: (onHovered) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      scheduleModel.durationsList!.removeAt(index);
                                                      startDateControllers.removeAt(index);
                                                      endDateControllers.removeAt(index);
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: onHovered ? buttonBg : CupertinoColors.white),
                                                    child: Icon(
                                                      Icons.remove,
                                                      size: 24,
                                                      color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(7, 0, 15, 0),
                                              child: OnHoverCard(builder: (onHovered) {
                                                return InkWell(
                                                  onTap: () {
                                                    WidgetsBinding.instance!.addPostFrameCallback((_) {
                                                      schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                                                          duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
                                                    });
                                                    setState(() {
                                                      scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: onHovered ? buttonBg : CupertinoColors.white),
                                                    child: Icon(
                                                      Icons.add,
                                                      size: 24,
                                                      color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                          ],
                                        )
                                      : Container(
                                          margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                          child: OnHoverCard(builder: (onHovered) {
                                            return InkWell(
                                              onTap: () {
                                                setState(() {
                                                  scheduleModel.durationsList!.removeAt(index);
                                                  startDateControllers.removeAt(index);
                                                  endDateControllers.removeAt(index);
                                                });
                                              },
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                    borderRadius: BorderRadius.circular(5),
                                                    color: onHovered ? buttonBg : CupertinoColors.white),
                                                child: Icon(
                                                  Icons.remove,
                                                  size: 24,
                                                  color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                ),
                                              ),
                                            );
                                          }),
                                        )
                            ],
                          ),
                        );
                      }),
                ),
              )
            : Padding(
                padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                child: Text(
                  unavailable,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText.withOpacity(0.58), fontSize: 16),
                ),
              )
      ],
    );
  }

  void getEmployeeData() {
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await employeesViewModel.viewEmployeeProfileAPI(employeeId).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            nameController.text = value.employeeData!.name!;
            emailController.text = value.employeeData!.email!;
            phoneController.text = value.employeeData!.phoneNumber!;
            designationsController.text = commaSeparatedDesignationsString(value.employeeData!.designationsList!);
            technologiesController.text = commaSeparatedTechnologiesString(value.employeeData!.technologiesList!);
            rolesController.text = value.employeeData!.roleName!;
            for (var element in schedulesList) {
              if (element.displayTitle == sun) {
                element.durationsList = value.employeeData!.availabilitySchedule!.sundayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.sundayAvailability;
              } else if (element.displayTitle == mon) {
                element.durationsList = value.employeeData!.availabilitySchedule!.mondayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.mondayAvailability;
              } else if (element.displayTitle == tue) {
                element.durationsList = value.employeeData!.availabilitySchedule!.tuesdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.tuesdayAvailability;
              } else if (element.displayTitle == wed) {
                element.durationsList = value.employeeData!.availabilitySchedule!.wednesdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.wednesdayAvailability;
              } else if (element.displayTitle == thu) {
                element.durationsList = value.employeeData!.availabilitySchedule!.thursdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.thursdayAvailability;
              } else if (element.displayTitle == fri) {
                element.durationsList = value.employeeData!.availabilitySchedule!.fridayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.fridayAvailability;
              } else if (element.displayTitle == sat) {
                element.durationsList = value.employeeData!.availabilitySchedule!.saturdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.saturdayAvailability;
              }
            }
            if (value.employeeData!.durationType!.toLowerCase() == fixed.toLowerCase()) {
              for (var element in durationsList) {
                if (element.value == value.employeeData!.duration!) {
                  durationInitialValue = element;
                  _durationDropDownKey.currentState!.didChange(durationInitialValue);
                }
              }
            } else if (value.employeeData!.durationType!.toLowerCase() == custom.toLowerCase()) {
              customDurationController.text = value.employeeData!.duration!.toString();
              for (var element in durationsList) {
                if (element.value == 0) {
                  durationInitialValue = element;
                  _durationDropDownKey.currentState!.didChange(durationInitialValue);
                }
              }
            }
            setState(() {});
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
