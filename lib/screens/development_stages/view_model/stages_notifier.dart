import 'package:aem/model/all_designations_response_model.dart';
import 'package:aem/model/all_stages_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/view_developmentstage_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StagesNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  Future<AllStagesResponseModel?> viewAllStages(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllStagesResponseModel? _allStagesResponseModel = AllStagesResponseModel();
    try {
      dynamic response = await Repository().viewAllStages(employeeId);
      if (response != null) {
        _allStagesResponseModel = AllStagesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allStagesResponseModel");
    notifyListeners();
    return _allStagesResponseModel;
  }



  Future<BaseResponse?> createStage(String name, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createStage(name, createdBy);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateStage(String stageId, String name,String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateStage(stageId, name,employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteStage(String stageId,String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteStage(stageId,employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<SingleViewDevelopmentStageModel?> viewStage(String stageId,String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    SingleViewDevelopmentStageModel? _singleViewDevelopmentStageModel = SingleViewDevelopmentStageModel();

    try {
      dynamic response = await Repository().viewStage(stageId,employeeId);
      if (response != null) {
        _singleViewDevelopmentStageModel = SingleViewDevelopmentStageModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_singleViewDevelopmentStageModel");
    notifyListeners();
    return _singleViewDevelopmentStageModel;
  }
  Future<BaseResponse?> activateDeactivateDevelopmentStageApi(String stageId,String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().activateDeactivateDevelopmentStageApi(stageId,employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

}