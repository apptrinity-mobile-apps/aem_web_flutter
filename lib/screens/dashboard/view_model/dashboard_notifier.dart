import 'package:aem/model/dashboard_projects_response_model.dart';
import 'package:aem/model/employee_permissions_response.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../model/all_dashboard_boardings_model.dart';

class DashBoardNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<dynamic?> getDashboardAPI(String employeeId,String boardingId) async {
    _isFetching = true;
    _isHavingData = false;
    DashboardProjectsResponse? _dashboardProjectsResponse = DashboardProjectsResponse();

    try {
      dynamic response = await Repository().getDashboard(employeeId,boardingId);
      if (response != null) {
        _dashboardProjectsResponse = DashboardProjectsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_dashboardProjectsResponse");
    notifyListeners();
    return _dashboardProjectsResponse;
  }

  Future<EmployeePermissionsResponse?> employeePermissionsAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    EmployeePermissionsResponse? _employeePermissionsResponse = EmployeePermissionsResponse();

    try {
      dynamic response = await Repository().employeePermissions(employeeId);
      if (response != null) {
        _employeePermissionsResponse = EmployeePermissionsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeePermissionsResponse");
    notifyListeners();
    return _employeePermissionsResponse;
  }

  Future<DashBoardBoardings?> getDashboardBoardingsApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    DashBoardBoardings? _dashBoardBoardings = DashBoardBoardings();

    try {
      dynamic response = await Repository().getDashboardBoardingsApi(employeeId);
      if (response != null) {
        _dashBoardBoardings = DashBoardBoardings.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_dashBoardBoardings");
    notifyListeners();
    return _dashBoardBoardings;
  }
}
