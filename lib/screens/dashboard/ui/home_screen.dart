import 'package:aem/screens/chat/ui/chat_fragment.dart';
import 'package:aem/screens/interview_status/ui/interview_status_fragment.dart';
import 'package:aem/screens/interview_types/ui/interview_types_fragment.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/profile_status/ui/profile_statuses_fragment.dart';
import 'package:aem/screens/profiles/ui/profiles_dashboard_fragment.dart';
import 'package:aem/screens/profiles/ui/profiles_fragment.dart';
import 'package:aem/screens/scrum/ui/add_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/allteam_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/my_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/team_scrum_fragment.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:flutter/material.dart';
import 'package:aem/screens/dashboard/ui/dashboard_fragment.dart';
import 'package:aem/screens/designations/ui/designations_fragment.dart';
import 'package:aem/screens/employees/ui/employees_fragment.dart';
import 'package:aem/screens/projects/ui/projects_fragment.dart';
import 'package:aem/screens/roles/ui/roles_fragment.dart';
import 'package:aem/screens/technologies/ui/technologies_fragment.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  final String? menu;

  final String? scrumDate;
  final String? projectId;
  final String? moduleId;
  final String? taskId;

  HomeScreen({this.menu, this.scrumDate,this.projectId,this.moduleId,this.taskId, Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String currentRoute = "";
  Widget? home;
  String employeeId = "";



  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    currentRoute = widget.menu!;
    // print("widget.menu--"+widget.menu!);
    if (widget.menu == RouteNames.dashBoardRoute) {
      home = const DashboardFragment();
    } else if (widget.menu == RouteNames.employeesHomeRoute) {
      home = const EmployeeFragment();
    } else if (widget.menu == RouteNames.technologyHomeRoute) {
      home = const TechnologyFragment();
    } else if (widget.menu == RouteNames.projectHomeRoute) {
      home = const ProjectFragment();
    } else if (widget.menu == RouteNames.designationHomeRoute) {
      home = const DesignationsFragment();
    } else if (widget.menu == RouteNames.roleHomeRoute) {
      home = const RolesFragment();
    } else if (widget.menu == RouteNames.profileHomeRoute) {
      home = const ProfilesFragment();
    } else if (widget.menu == RouteNames.profileStatusesHomeRoute) {
      home = const ProfileStatusesFragment();
    } else if (widget.menu == RouteNames.interviewerStatusHomeRoute) {
      home = const InterviewStatusFragment();
    }else if (widget.menu == RouteNames.profilesDashboardHomeRoute) {
      home = const ProfilesDashboardFragment();
    }  else if (widget.menu == RouteNames.interviewTypesHomeRoute) {
      home = const InterviewTypesFragment();
    } else if (widget.menu == RouteNames.scrumEmployeeRoute) {
      home = const TeamScrumFragment(employeeId: "");
    } else if (widget.menu == RouteNames.scrumTeamRoute) {
      home = const AllTeamScrumFragment(employeeId: "");
    } else if (widget.menu == RouteNames.scrumAddRoute) {
      home = AddScrumFragement(
        scrumDate: widget.scrumDate,
      );
    } else if (widget.menu == RouteNames.scrumMyScrumRoute) {
      home = const MyScrumFragment(employeeId: "");
    }  else if (widget.menu == RouteNames.chatRoute) {
      home = const ChatScreen();
    } else {
      home = const DashboardFragment();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                        home = value;
                      });
                    },
                    // selectedMenu: widget.menu! + "-",
                    selectedMenu: currentRoute + "-",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(elevation: 2, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)), color: cardBg, child: home),
      ),
    );
  }
}
