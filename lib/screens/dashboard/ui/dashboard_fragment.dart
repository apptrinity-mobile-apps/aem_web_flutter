import 'package:aem/model/dashboard_projects_response_model.dart';
import 'package:aem/screens/dashboard/view_model/dashboard_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_project_screen.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/adaptable_text.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';

import '../../../model/all_dashboard_boardings_model.dart';
import '../../../utils/validators.dart';

class DashboardFragment extends StatefulWidget {
  const DashboardFragment({Key? key}) : super(key: key);

  @override
  _DashboardFragmentState createState() => _DashboardFragmentState();
}
class _DashboardFragmentState extends State<DashboardFragment> {
  late DashBoardNotifier viewModel;
  String employeeId = "";
  bool projViewPermission = false;
  GlobalKey<FormFieldState> newKey = GlobalKey<FormFieldState>();
  late bool hint = false;
  late String selectedBoardingId;
  List<DropdownMenuItem<BoardingsData>> boardingMenuItems = [];
  BoardingsData boardingInitialValue = BoardingsData(boardName: selectBoarding);
  List<BoardingsData> boardingsList = [], selectedBoardings = [];
  @override
  void initState() {
    viewModel = Provider.of<DashBoardNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    projViewPermission = userViewModel.projectViewPermission;
    boardingMenuItems = [];
    boardingMenuItems.add(DropdownMenuItem(child: const Text(selectBoarding), value: boardingInitialValue));
    /*Future.delayed(Duration.zero, () async {
      await viewModel.getDashboardBoardingsApi(employeeId).then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.boardingsData!.isNotEmpty) {
                boardingsList = value.boardingsData!;

                for (var element in value.boardingsData!) {
                  boardingMenuItems.add(DropdownMenuItem(child: Text(element.boardName!), value: element));
                }
              }
            });
          }
        }
        // hasTechnologies = true;
      });
    });*/
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(navMenuDashboard, style: fragmentHeaderStyle),
                  Text("$home > $navMenuDashboard", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                ],
              ),
              addBoardingField()
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: SizedBox(
                              height: ScreenConfig.height(context),
                              width: ScreenConfig.width(context),
                              child: hint ? projectsList(selectedBoardingId) : allBoardingsList()),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                hint
                    ? InkWell(
                        onTap: () {
                          setState(() {
                            // boardingInitialValue = BoardingsData(boardName: selectBoarding);
                            newKey.currentState!.reset();
                            hint = false;
                          });
                        },
                        child: backButton(),
                      )
                    : const SizedBox(),
                Text(hint ? boardingInitialValue.boardName! + " " + navMenuProjects : navMenuBoarding, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                hint ? Text(descriptionDashboard, softWrap: true, style: fragmentDescStyle) : const SizedBox()
              ],
            ),
          ),
        ],
      ),
    );
  }
  Widget addBoardingField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /* Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(boarding, style: addRoleSubStyle),
              ),
            ),*/
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownButtonFormField<BoardingsData>(
                    items: boardingMenuItems,
                    key: newKey,
                    decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: editTextPadding,
                        hintStyle: textFieldHintStyle,
                        hintText: status),
                    style: textFieldStyle,
                    // value: boardingInitialValue,
                    validator: dashBoardBoardingDropDownValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (BoardingsData? value) {
                      setState(() {
                        hint = true;
                        boardingInitialValue = value!;
                        selectedBoardingId = boardingInitialValue.id!;
                      });
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget projectsList(String boardingId) {
    return FutureBuilder(
        future: viewModel.getDashboardAPI(employeeId, boardingId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as DashboardProjectsResponse;
              if (data.responseStatus == 1) {
                if (data.projectsData!.isNotEmpty) {
                  return GridView.builder(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      controller: ScrollController(),
                      physics: const ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: data.projectsData!.length,
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 250),
                      itemBuilder: (BuildContext context, int index) {
                        return projectsListItem(data.projectsData![index]);
                      });
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget projectsListItem(ProjectsData itemData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              // if (Provider.of<LoginNotifier>(context, listen: false).projectViewPermission) {
              Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) => ViewProjectDetailedScreen(projectId: itemData.id),
                  transitionDuration: const Duration(seconds: 0),
                  reverseTransitionDuration: const Duration(seconds: 0),
                ),
              ).then((value) {
                // set state to reload data
                setState(() {});
              });
              /*} else {
                showToast(noPermissionToAccess);
              }*/
            },
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                      child: Center(
                        child: AdaptableText(
                          itemData.name!.inCaps,
                          textAlign: TextAlign.center,
                          style: isHovered ? dashBoardTileHoveredStyle : dashBoardTileStyle,
                          textMaxLines: 3,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Container(
                            height: ScreenConfig.height(context),
                            width: ScreenConfig.width(context),
                            margin: const EdgeInsets.fromLTRB(10, 0, 5, 10),
                            child: Card(
                              elevation: 2,
                              color: CupertinoColors.white,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Image.asset("assets/images/overdue.png", height: 40, width: 40),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                      child: Text(
                                        itemData.overDueCount! > 99 ? maxCount : itemData.overDueCount!.toString(),
                                        style: tileCountStyle,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                      child: Text(
                                        overDue,
                                        style: tileOverDueStyle,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: ScreenConfig.height(context),
                            width: ScreenConfig.width(context),
                            margin: const EdgeInsets.fromLTRB(5, 0, 10, 10),
                            child: Card(
                              elevation: 2,
                              color: CupertinoColors.white,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                      child: FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Image.asset("assets/images/new.png", height: 40, width: 40),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                      child: Text(
                                        itemData.newCount! > 99 ? maxCount : itemData.newCount!.toString(),
                                        style: tileCountStyle,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                      child: Text(
                                        newStr,
                                        style: tileNewStyle,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget allBoardingsList() {
    return FutureBuilder(
        future: viewModel.getDashboardBoardingsApi(employeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as DashBoardBoardings;
              if (data.responseStatus == 1) {
                if (data.boardingsData!.isNotEmpty) {
                  boardingsList = data.boardingsData!;
                  // clearing the list and adding new data
                  boardingMenuItems.clear();
                  boardingMenuItems.add(DropdownMenuItem(child: const Text(selectBoarding), value: boardingInitialValue));
                  for (var element in boardingsList) {
                    boardingMenuItems.add(DropdownMenuItem(child: Text(element.boardName!), value: element));
                  }
                  return GridView.builder(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      controller: ScrollController(),
                      physics: const ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: data.boardingsData!.length,
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 250),
                      itemBuilder: (BuildContext context, int index) {
                        return boardingList(data.boardingsData![index]);
                      });
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget boardingList(BoardingsData itemData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              setState(() {
                hint = true;
                selectedBoardingId = itemData.id!;
                for (var element in boardingsList) {
                  if (element.id == selectedBoardingId) {
                    boardingInitialValue = element;
                    newKey.currentState!.didChange(boardingInitialValue);
                  }
                }
                // currentRoute = value;
              });

              /*if (editPermission) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => AddBoardingFragment(boardingId: itemData.id!,name: itemData.boardName,description: itemData.boardDescription,),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  // set state to reload data , reloads data in futureBuilder
                  setState(() {});
                });
              } else {
                showToast(noPermissionToAccess);
              }*/
            },
            child: Stack(
              children: [
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 25, 20, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: AdaptableText(itemData.boardName!.inCaps,
                              style: isHovered ? projectTitleHoveredStyle : projectTitleStyle, textMaxLines: 3, textOverflow: TextOverflow.ellipsis),
                        ),
                        Expanded(
                          flex: 2,
                          child: HtmlWidget(
                            itemData.boardDescription!,
                            buildAsync: true,
                            enableCaching: false,
                            renderMode: RenderMode.column,
                            textStyle: isHovered ? projectDescHoveredStyle : projectDescStyle,
                          ),
                        ),
                        /* Expanded(
                          child: itemData.employees!.isNotEmpty
                              ? ListView.builder(
                              padding: const EdgeInsets.only(top: 5),
                              scrollDirection: Axis.horizontal,
                              itemCount: itemData.employees!.length > 4 ? 4 : itemData.employees!.length,
                              itemBuilder: (context, index) {
                                return index == 0
                                    ? Align(
                                  widthFactor: 1,
                                  child: Tooltip(
                                    message: itemData.employees![index].name!,
                                    decoration: toolTipDecoration,
                                    textStyle: toolTipStyle,
                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                        child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                      ),
                                    ),
                                  ),
                                )
                                    : Align(
                                  widthFactor: 0.8,
                                  child: Tooltip(
                                    message: itemData.employees![index].name!,
                                    decoration: toolTipDecoration,
                                    textStyle: toolTipStyle,
                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: CircleAvatar(
                                        backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                        child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                      ),
                                    ),
                                  ),
                                );
                              })
                              : const SizedBox(),
                        ),*/
                      ],
                    ),
                  ),
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
