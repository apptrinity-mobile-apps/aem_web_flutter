import 'package:aem/model/all_designations_response_model.dart';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_roles_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/employee_interview_duration_model.dart';
import 'package:aem/model/employee_availability_model.dart';
import 'package:aem/screens/designations/view_model/designation_notifier.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/roles/view_model/roles_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AddEmployeeFragment extends StatefulWidget {
  final String? employeeId;

  const AddEmployeeFragment({this.employeeId, Key? key}) : super(key: key);

  @override
  _AddEmployeeFragmentState createState() => _AddEmployeeFragmentState();
}

class _AddEmployeeFragmentState extends State<AddEmployeeFragment> {
  DateTime selectedDate = DateTime.now();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController customDurationController = TextEditingController();
  final _multiKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<String> employeeIds = [];
  RolesDetails roleInitialValue = RolesDetails(role: selectRole);
  EmployeeInterviewDurationModel durationInitialValue = EmployeeInterviewDurationModel(displayTitle: select, value: 0);
  DesignationsList designationInitialValue = DesignationsList(name: selectDesignation);
  TechnologiesList technologyInitialValue = TechnologiesList(name: selectTechnology);
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  final _formKey = GlobalKey<FormState>();
  final _durationDropDownKey = GlobalKey<FormFieldState>();
  final _multiDesignationKey = GlobalKey<DropdownSearchState<DesignationsList>>();
  final _multiTechnologyKey = GlobalKey<DropdownSearchState<TechnologiesList>>();
  bool isVisible = true;
  bool isSuperior = false;
  // final _multiRoleKey = GlobalKey<DropdownSearchState<RolesDetails>>();
  late EmployeeNotifier viewModel;
  late RolesNotifier rolesViewModel;
  late TechnologyNotifier techViewModel;
  late DesignationNotifier designViewModel;
  String employeeId = "", userId = "";
  bool hasRoles = false, hasDesignations = false, hasTechnologies = false;
  List<DropdownMenuItem<RolesDetails>> rolesMenuItems = [];
  List<DropdownMenuItem<EmployeeInterviewDurationModel>> durationMenuItems = [];
  List<DropdownMenuItem<DesignationsList>> designationsMenuItems = [];
  List<DropdownMenuItem<TechnologiesList>> technologiesMenuItems = [];
  List<DesignationsList> designationsList = [], selectedDesignations = [];
  List<TechnologiesList> technologiesList = [], selectedTechnologies = [];
  List<RolesDetails> rolesList = [], selectedRoles = [];
  List<String> technologyIds = [], designationIds = [];
  List<EmployeeInterviewDurationModel> durationsList = [];
  List<EmployeeAvailabilityModel> schedulesList = [];
  ScrollController schedulesController = ScrollController();
  TimeOfDay selectedTime = TimeOfDay.now();

  @override
  void initState() {
    viewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    rolesViewModel = Provider.of<RolesNotifier>(context, listen: false);
    techViewModel = Provider.of<TechnologyNotifier>(context, listen: false);
    designViewModel = Provider.of<DesignationNotifier>(context, listen: false);
    employeesList.add(EmployeeDetails(name: selectEmployee));
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    // schedules
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: sun, displayValue: sun, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: mon, displayValue: mon, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: tue, displayValue: tue, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: wed, displayValue: wed, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: thu, displayValue: thu, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: fri, displayValue: fri, durationsList: [], value: false));
    schedulesList.add(EmployeeAvailabilityModel(displayTitle: sat, displayValue: sat, durationsList: [], value: false));
    // adding durations
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min15, value: 15));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min30, value: 30));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min45, value: 45));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: min60, value: 60));
    durationsList.add(EmployeeInterviewDurationModel(displayTitle: custom, value: 0));
    durationMenuItems.add(DropdownMenuItem(child: Text("Select Duration"), value: EmployeeInterviewDurationModel(displayTitle: "Select Duration",value: -1)));
    for (var element in durationsList) {
      durationMenuItems.add(DropdownMenuItem(child: Text(element.displayTitle!), value: element));
    }
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.employeesHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuEmployees", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        height: ScreenConfig.height(context),
                        margin: const EdgeInsets.fromLTRB(5, 15, 5, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Form(
                                key: _formKey,
                                child: ListView(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  controller: schedulesController,
                                  shrinkWrap: true,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                      decoration: cardBorder,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [nameField(), designationFieldMulti()],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [emailField(), technologyFieldMulti()],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [phoneField(), roleField()],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [ issuperiorField(),isSuperior?assignEmployeeField():Expanded(child: SizedBox())],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                      decoration: cardBorder,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                        child: Form(
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    durationField(),
                                                    durationInitialValue.displayTitle!.toLowerCase() == custom.toLowerCase()
                                                        ?customDurationField():const Expanded(
                                                      child: SizedBox(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            /*  durationInitialValue.displayTitle!.toLowerCase() == custom.toLowerCase()
                                                  ? Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          customDurationField(),
                                                          const Expanded(
                                                            child: SizedBox(),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : const SizedBox(),*/
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 5),
                                        child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              const Padding(
                                                padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                                                child: SizedBox(
                                                  child: Text(
                                                    'Schedule',
                                                    style: TextStyle(
                                                        letterSpacing: 0,
                                                        fontFamily: "Poppins",
                                                        fontWeight: FontWeight.w700,
                                                        color: addRoleText,
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ),
                                              ListView.builder(
                                                physics: const ClampingScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                itemCount: schedulesList.length,
                                                shrinkWrap: true,
                                                itemBuilder: (context, index) {
                                                  return schedulesWidget(schedulesList[index]);
                                                },
                                              ),
                                            ]),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.employeeId == null ? createEmployee : updateEmployee, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 3),
                      child: Text(descriptionAddEmployee, softWrap: true, style: fragmentDescStyle),
                    )
                  ],
                ),
              ),
              Expanded(
                child: FittedBox(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.scaleDown,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              technologyIds = [];
                              designationIds = [];

                              employeeIds = [];
                              for (var element in selectedEmployees) {
                                employeeIds.add(element.id!);
                              }
                              var schedules = AvailabilitySchedules();
                              int duration = 0;
                              String type = '';
                              for (var element in schedulesList) {
                                if (element.displayValue == sun) {
                                  schedules.sundayAvailability = element.value;
                                  schedules.sundayTimings = element.durationsList;
                                } else if (element.displayValue == mon) {
                                  schedules.mondayAvailability = element.value;
                                  schedules.mondayTimings = element.durationsList;
                                } else if (element.displayValue == tue) {
                                  schedules.tuesdayAvailability = element.value;
                                  schedules.tuesdayTimings = element.durationsList;
                                } else if (element.displayValue == wed) {
                                  schedules.wednesdayAvailability = element.value;
                                  schedules.wednesdayTimings = element.durationsList;
                                } else if (element.displayValue == thu) {
                                  schedules.thursdayAvailability = element.value;
                                  schedules.thursdayTimings = element.durationsList;
                                } else if (element.displayValue == fri) {
                                  schedules.fridayAvailability = element.value;
                                  schedules.fridayTimings = element.durationsList;
                                } else if (element.displayValue == sat) {
                                  schedules.saturdayAvailability = element.value;
                                  schedules.saturdayTimings = element.durationsList;
                                }
                              }
                              print('duration -- ${customDurationController.text.toString()}');
                              setState(() {
                                if (durationInitialValue.displayTitle!.toLowerCase() != custom.toLowerCase()) {
                                  duration = durationInitialValue.value!;
                                  type = fixed.toLowerCase();
                                } else {
                                  duration = int.parse(customDurationController.text.toString());

                                  type = durationInitialValue.displayTitle!.toLowerCase();
                                }
                              });
                              for (var element in selectedTechnologies) {
                                technologyIds.add(element.id!);
                              }
                              for (var element in selectedDesignations) {
                                designationIds.add(element.id!);
                              }
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              if (widget.employeeId != null) {
                                await viewModel
                                    .updateEmployeeAPI(nameController.text.trim(), emailController.text.trim(), designationIds, technologyIds,
                                        roleInitialValue.id!, employeeId, userId, phoneController.text.trim(), schedules, duration, type,employeeIds,isSuperior)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              } else {
                                await viewModel
                                    .addEmployeeAPI(nameController.text.trim(), emailController.text.trim(), "", designationIds, technologyIds,
                                        roleInitialValue.id!, employeeId, phoneController.text.trim(), schedules, duration, type,employeeIds,isSuperior)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              }
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(submit.toUpperCase(), style: newButtonsStyle),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget nameField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(name, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: nameController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.text,
                        inputFormatters: [UpperCaseTextFormatter()],
                        textCapitalization: TextCapitalization.sentences,
                        obscureText: false,
                        decoration: editTextNameDecoration,
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget assignEmployeeField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(assignEmployee, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownSearch<EmployeeDetails>.multiSelection(
                      key: _multiKey,
                      mode: Mode.MENU,
                      showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
                      compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                      items: employeesList,
                      showClearButton: true,
                      onChanged: (data) {
                        selectedEmployees = data;
                      },
                      clearButtonSplashRadius: 10,
                      selectedItems: selectedEmployees,
                      showSearchBox: true,
                      dropdownSearchDecoration: editTextEmployeesDecoration,
                      //validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                      dropdownBuilder: (context, selectedItems) {
                        return Wrap(
                            children: selectedItems
                                .map(
                                  (e) => selectedItem(e.name!),
                            )
                                .toList());
                      },
                      filterFn: (EmployeeDetails? employee, name) {
                        return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                      },
                      popupItemBuilder: (_, text, isSelected) {
                        return Container(
                          padding: const EdgeInsets.all(10),
                          child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                        );
                      },
                      popupSelectionWidget: (cnt, item, bool isSelected) {
                        return Checkbox(
                            activeColor: buttonBg,
                            hoverColor: buttonBg.withOpacity(0.2),
                            value: isSelected,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                            side: const BorderSide(color: buttonBg),
                            onChanged: (value) {});
                      },
                      onPopupDismissed: () {
                        setState(() {
                          isVisible = true;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget emailField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(email, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: emailController,
                        textInputAction: TextInputAction.done,
                        obscureText: false,
                        decoration: editTextEmailDecoration,
                        style: textFieldStyle,
                        validator: emailValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget phoneField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(phone, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: phoneController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          LengthLimitingTextInputFormatter(10)
                        ],
                        obscureText: false,
                        decoration: editTextPhoneNumberDecoration,
                        style: textFieldStyle,
                        validator: phoneValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget issuperiorField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(is_superior, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 10, 20, 0),
                child: Checkbox(
                    visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
                    activeColor: buttonBg,
                    hoverColor: buttonBg.withOpacity(0.2),
                    value:  isSuperior,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                    side: const BorderSide(color: buttonBg),
                    onChanged: (value)  {
                      setState(() {
                        isSuperior = value!;
                      });


                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget designationField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(designation, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownButtonFormField(
                      items: designationsMenuItems,
                      decoration: editTextDesignationDecoration,
                      style: textFieldStyle,
                      value: designationInitialValue,
                      validator: designationsDropDownValidator,
                      onChanged: (DesignationsList? value) {
                        setState(() {
                          designationInitialValue = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget designationFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(designation, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownSearch<DesignationsList>.multiSelection(
                        key: _multiDesignationKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedDesignations.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: designationsList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedDesignations = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedDesignations,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map(
                                    (item) => Container(
                                      padding: const EdgeInsets.all(5),
                                      margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Theme.of(context).primaryColorLight),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(item.name!, textAlign: TextAlign.center, style: textFieldStyle),
                                          MaterialButton(
                                            height: 20,
                                            shape: const CircleBorder(),
                                            focusColor: Colors.red[200],
                                            hoverColor: Colors.red[200],
                                            padding: const EdgeInsets.all(0),
                                            minWidth: 34,
                                            onPressed: () {
                                              _multiDesignationKey.currentState?.removeItem(item);
                                            },
                                            child: const Icon(Icons.close_outlined, size: 20),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                  .toList());
                        },
                        filterFn: (DesignationsList? designation, name) {
                          return designation!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                            padding: const EdgeInsets.all(10),
                            child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                          );
                        },
                        popupSelectionWidget: (cnt, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget technologyField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(technology, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownButtonFormField(
                      items: technologiesMenuItems,
                      decoration: editTextTechnologyDecoration,
                      style: textFieldStyle,
                      value: technologyInitialValue,
                      validator: technologiesDropDownValidator,
                      onChanged: (TechnologiesList? value) {
                        setState(() {
                          technologyInitialValue = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget technologyFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(technology, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownSearch<TechnologiesList>.multiSelection(
                        key: _multiTechnologyKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedTechnologies.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: technologiesList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedTechnologies = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedTechnologies,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map(
                                    (item) => Container(
                                      padding: const EdgeInsets.all(5),
                                      margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Theme.of(context).primaryColorLight),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(item.name!, textAlign: TextAlign.center, style: textFieldStyle),
                                          MaterialButton(
                                            height: 20,
                                            shape: const CircleBorder(),
                                            focusColor: Colors.red[200],
                                            hoverColor: Colors.red[200],
                                            padding: const EdgeInsets.all(0),
                                            minWidth: 34,
                                            onPressed: () {
                                              _multiTechnologyKey.currentState?.removeItem(item);
                                            },
                                            child: const Icon(Icons.close_outlined, size: 20),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                  .toList());
                        },
                        filterFn: (TechnologiesList? designation, name) {
                          return designation!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                            padding: const EdgeInsets.all(10),
                            child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                          );
                        },
                        popupSelectionWidget: (cnt, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget roleField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(role, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownButtonFormField<RolesDetails>(
                      items: rolesMenuItems,
                      decoration: editTextRoleDecoration,
                      style: textFieldStyle,
                      value: roleInitialValue,
                      validator: rolesDropDownValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (RolesDetails? value) {
                        setState(() {
                          roleInitialValue = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  /* multi select roles - uncomment this */
  /*Widget roleFieldMulti() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(role, style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownSearch<RolesDetails>.multiSelection(
                        key: _multiRoleKey,
                        mode: Mode.MENU,
                        maxHeight: 400,
                        showSelectedItems: selectedRoles.isNotEmpty ? true : false,
                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                        items: rolesList,
                        showClearButton: true,
                        onChanged: (data) {
                          selectedRoles = data;
                        },
                        clearButtonSplashRadius: 10,
                        selectedItems: selectedRoles,
                        showSearchBox: true,
                        dropdownSearchDecoration: editTextEmployeesDecoration,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                        dropdownBuilder: (context, selectedItems) {
                          return Wrap(
                              children: selectedItems
                                  .map((item) => Container(
                                        padding: const EdgeInsets.all(5),
                                        margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: buttonBg),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(item.role!, textAlign: TextAlign.center, style: textFieldStyle),
                                            MaterialButton(
                                              height: 20,
                                              shape: const CircleBorder(),
                                              focusColor: Colors.red[200],
                                              hoverColor: Colors.red[200],
                                              padding: const EdgeInsets.all(0),
                                              minWidth: 34,
                                              onPressed: () {
                                                _multiRoleKey.currentState?.removeItem(item);
                                              },
                                              child: const Icon(Icons.close_outlined, size: 20),
                                            )
                                          ],
                                        ),
                                      ))
                                  .toList());
                        },
                        filterFn: (RolesDetails? role, name) {
                          return role!.role!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                        },
                        popupItemBuilder: (_, text, isSelected) {
                          return Container(
                              padding: const EdgeInsets.all(10), child: Text(text.role!, style: isSelected ? dropDownSelected : dropDown));
                        },
                        popupSelectionWidget: (_, item, bool isSelected) {
                          return Checkbox(
                              activeColor: buttonBg,
                              hoverColor: buttonBg.withOpacity(0.2),
                              value: isSelected,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                              side: const BorderSide(color: buttonBg),
                              onChanged: (value) {});
                        }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }*/

  Widget durationField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text(
                    'Duration',
                    style: TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: addRoleText, fontSize: 16),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: DropdownButtonFormField<EmployeeInterviewDurationModel>(
                      key: _durationDropDownKey,
                      items: durationMenuItems,
                      decoration: InputDecoration(
                          fillColor: CupertinoColors.white,
                          filled: true,
                          border: border,
                          isDense: true,
                          enabledBorder: border,
                          focusedBorder: border,
                          errorBorder: errorBorder,
                          focusedErrorBorder: errorBorder,
                          contentPadding: editTextPadding,
                          hintStyle: textFieldHintStyle,
                          hintText: 'Duration'),
                      style: textFieldStyle,
                      // value: roleInitialValue,
                      // validator: rolesDropDownValidator,
                      onChanged: (EmployeeInterviewDurationModel? value) {
                        setState(() {
                          durationInitialValue = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget customDurationField() {
    return Expanded(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                child: SizedBox(
                  width: 150,
                  child: Text('Minutes', style: addRoleSubStyle),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                  child: IntrinsicWidth(
                    child: TextFormField(
                        maxLines: 1,
                        controller: customDurationController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          LengthLimitingTextInputFormatter(10)
                        ],
                        obscureText: false,
                        decoration: InputDecoration(
                            fillColor: CupertinoColors.white,
                            filled: true,
                            border: border,
                            isDense: true,
                            enabledBorder: border,
                            focusedBorder: border,
                            errorBorder: errorBorder,
                            focusedErrorBorder: errorBorder,
                            contentPadding: editTextPadding,
                            hintStyle: textFieldHintStyle,
                            hintText: 'Enter value in minutes'),
                        style: textFieldStyle,
                        validator: emptyTextValidator,
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget schedulesWidget(EmployeeAvailabilityModel scheduleModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
            activeColor: buttonBg,
            hoverColor: buttonBg.withOpacity(0.2),
            value: scheduleModel.value!,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            side: const BorderSide(color: buttonBg),
            onChanged: (value) {
              WidgetsBinding.instance!.addPostFrameCallback((_) {
                schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                    duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
              });
              setState(() {
                scheduleModel.value = value!;
                if (scheduleModel.value!) {
                  if (scheduleModel.durationsList!.isEmpty) {
                    scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                  }
                } else {
                  scheduleModel.durationsList!.clear();
                }
              });
            }),
        Padding(
          padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
          child: SizedBox(
            width: 150,
            child: Text(scheduleModel.displayTitle!.substring(0, 3).toUpperCase(), textAlign: TextAlign.start, style: addRoleSubStyle),
          ),
        ),
        scheduleModel.value!
            ? Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: scheduleModel.durationsList!.length,
                      itemBuilder: (context, index) {
                        List<TextEditingController> startDateControllers = [];
                        List<TextEditingController> endDateControllers = [];
                        for (var e in scheduleModel.durationsList!) {
                          startDateControllers.add(TextEditingController(text: e.startTime));
                          endDateControllers.add(TextEditingController(text: e.endTime));
                        }
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            textBaseline: TextBaseline.ideographic,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                width: 180,
                                alignment: Alignment.center,
                                child: TextFormField(
                                  controller: startDateControllers[index],
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  textInputAction: TextInputAction.done,
                                  textCapitalization: TextCapitalization.sentences,
                                  obscureText: false,
                                  validator: timeScheduleValidator,
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  decoration: InputDecoration(
                                      fillColor: CupertinoColors.white,
                                      filled: true,
                                      border: border,
                                      isDense: true,
                                      enabledBorder: border,
                                      focusedBorder: border,
                                      errorBorder: errorBorder,
                                      focusedErrorBorder: errorBorder,
                                      contentPadding: editTextPadding,
                                      hintStyle: textFieldHintStyle,
                                      hintText: '00:00 AM'),
                                  style: textFieldStyle,
                                  textAlignVertical: TextAlignVertical.center,
                                  readOnly: true,
                                  enabled: true,
                                  onTap: () async {
                                    final TimeOfDay? timeOfDay = await showTimePicker(
                                      context: context,
                                      initialTime: selectedTime,
                                      initialEntryMode: TimePickerEntryMode.dial,
                                      confirmText: "CONFIRM",
                                      cancelText: "NOT NOW",
                                      helpText: "AVAILABLE TIME",
                                    );
                                    if (timeOfDay != null) {
                                      setState(() {
                                        scheduleModel.durationsList![index].startTime = formatTimeOfDay(timeOfDay);
                                      });
                                    }
                                  },
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 5, right: 5),
                                alignment: Alignment.center,
                                child: const Text(
                                  '-',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 16),
                                ),
                              ),
                              Container(
                                width: 180,
                                alignment: Alignment.center,
                                child: TextFormField(
                                  controller: endDateControllers[index],
                                  textAlign: TextAlign.center,
                                  maxLines: 1,
                                  textInputAction: TextInputAction.done,
                                  textCapitalization: TextCapitalization.sentences,
                                  obscureText: false,
                                  validator: timeScheduleValidator,
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  decoration: InputDecoration(
                                      fillColor: CupertinoColors.white,
                                      filled: true,
                                      border: border,
                                      isDense: true,
                                      enabledBorder: border,
                                      focusedBorder: border,
                                      errorBorder: errorBorder,
                                      focusedErrorBorder: errorBorder,
                                      contentPadding: editTextPadding,
                                      hintStyle: textFieldHintStyle,
                                      hintText: '00:00 AM'),
                                  style: textFieldStyle,
                                  textAlignVertical: TextAlignVertical.center,
                                  readOnly: true,
                                  enabled: true,
                                  onTap: () async {
                                    final TimeOfDay? timeOfDay = await showTimePicker(
                                      context: context,
                                      initialTime: selectedTime,
                                      initialEntryMode: TimePickerEntryMode.dial,
                                      confirmText: "CONFIRM",
                                      cancelText: "NOT NOW",
                                      helpText: "AVAILABLE TIME",
                                    );
                                    if (timeOfDay != null) {
                                      setState(() {
                                        scheduleModel.durationsList![index].endTime = formatTimeOfDay(timeOfDay);
                                      });
                                    }
                                  },
                                ),
                              ),
                              scheduleModel.durationsList!.length == 1
                                  ? Container(
                                      margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                      child: OnHoverCard(builder: (onHovered) {
                                        return InkWell(
                                          onTap: () {
                                            WidgetsBinding.instance!.addPostFrameCallback((_) {
                                              schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                                                  duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
                                            });
                                            setState(() {
                                              scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                                            });
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                borderRadius: BorderRadius.circular(5),
                                                color: onHovered ? buttonBg : CupertinoColors.white),
                                            child: Icon(
                                              Icons.add,
                                              size: 24,
                                              color: onHovered ? CupertinoColors.white : addScheduleButton,
                                            ),
                                          ),
                                        );
                                      }),
                                    )
                                  : scheduleModel.durationsList!.length - 1 == index
                                      ? Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(15, 0, 7, 0),
                                              child: OnHoverCard(builder: (onHovered) {
                                                return InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      scheduleModel.durationsList!.removeAt(index);
                                                      startDateControllers.removeAt(index);
                                                      endDateControllers.removeAt(index);
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: onHovered ? buttonBg : CupertinoColors.white),
                                                    child: Icon(
                                                      Icons.remove,
                                                      size: 24,
                                                      color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(7, 0, 15, 0),
                                              child: OnHoverCard(builder: (onHovered) {
                                                return InkWell(
                                                  onTap: () {
                                                    WidgetsBinding.instance!.addPostFrameCallback((_) {
                                                      schedulesController.animateTo(schedulesController.position.maxScrollExtent,
                                                          duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
                                                    });
                                                    setState(() {
                                                      scheduleModel.durationsList!.add(Durations(startTime: '00:00 AM', endTime: '00:00 AM'));
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: onHovered ? buttonBg : CupertinoColors.white),
                                                    child: Icon(
                                                      Icons.add,
                                                      size: 24,
                                                      color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                          ],
                                        )
                                      : Container(
                                          margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                          child: OnHoverCard(builder: (onHovered) {
                                            return InkWell(
                                              onTap: () {
                                                setState(() {
                                                  scheduleModel.durationsList!.removeAt(index);
                                                  startDateControllers.removeAt(index);
                                                  endDateControllers.removeAt(index);
                                                });
                                              },
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(color: onHovered ? buttonBg : addScheduleButton, width: 1.5),
                                                    borderRadius: BorderRadius.circular(5),
                                                    color: onHovered ? buttonBg : CupertinoColors.white),
                                                child: Icon(
                                                  Icons.remove,
                                                  size: 24,
                                                  color: onHovered ? CupertinoColors.white : addScheduleButton,
                                                ),
                                              ),
                                            );
                                          }),
                                        )
                            ],
                          ),
                        );
                      }),
                ),
              )
            : Padding(
                padding: const EdgeInsets.fromLTRB(4, 5, 4, 5),
                child: Text(
                  unavailable,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText.withOpacity(0.58), fontSize: 16),
                ),
              )
      ],
    );
  }

  void getData() {
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await rolesViewModel.getRolesAPI().then((value) {
        rolesMenuItems = [];
        rolesMenuItems.add(DropdownMenuItem(child: const Text(selectRole), value: roleInitialValue));
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.rolesDetails!.isNotEmpty) {
                rolesList = value.rolesDetails!;
                for (var element in value.rolesDetails!) {
                  rolesMenuItems.add(DropdownMenuItem(child: Text(element.role!), value: element));
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
        hasRoles = true;
      });
      await employeeViewModel.getEmployeesAPI("project").then((value) {
       // Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
      await designViewModel.getDesignationsAPI().then((value) {
        designationsMenuItems = [];
        designationsMenuItems.add(DropdownMenuItem(child: const Text(selectDesignation), value: designationInitialValue));
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.designationsList!.isNotEmpty) {
                designationsList = value.designationsList!;
                for (var element in value.designationsList!) {
                  designationsMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
        hasDesignations = true;
      });
      await techViewModel.getTechnologiesAPI().then((value) {
        technologiesMenuItems = [];
        technologiesMenuItems.add(DropdownMenuItem(child: const Text(selectTechnology), value: technologyInitialValue));
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.technologyList!.isNotEmpty) {
                technologiesList = value.technologyList!;
                for (var element in value.technologyList!) {
                  technologiesMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
        hasTechnologies = true;
      });
      checkData();
      if (widget.employeeId != null) {
        getEmployeeData();
      }
    });
  }

  void getEmployeeData() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      selectedDesignations = [];
      selectedTechnologies = [];
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewEmployeeAPI(employeeId, widget.employeeId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            userId = value.employeeData!.id!;
            nameController.text = value.employeeData!.name!;
            emailController.text = value.employeeData!.email!;
            phoneController.text = value.employeeData!.phoneNumber!;
            isSuperior = value.employeeData!.isSuperiorEmployee!;
            for (var element in rolesMenuItems) {
              if (element.value!.id == value.employeeData!.roleId!) {
                roleInitialValue = element.value!;
              }
            }
            for (var element in designationsMenuItems) {
              for (var item in value.employeeData!.designationsList!) {
                if (element.value!.id == item.designationId) {
                  designationInitialValue = element.value!;
                }
              }
            }
            for (var element in technologiesMenuItems) {
              for (var item in value.employeeData!.technologiesList!) {
                if (element.value!.id == item.technologyId) {
                  technologyInitialValue = element.value!;
                }
              }
            }

            for (var element in employeesList) {
              for (var item in value.employeeData!.employees!) {


                if (element.id == item.id) {
                  selectedEmployees.add(element);
                }
              }
            }
            for (var element in designationsList) {
              for (var item in value.employeeData!.designationsList!) {
                if (element.id == item.designationId) {
                  selectedDesignations.add(element);
                }
              }
            }
            for (var element in technologiesList) {
              for (var item in value.employeeData!.technologiesList!) {
                if (element.id == item.technologyId) {
                  selectedTechnologies.add(element);
                }
              }
            }
            for (var element in rolesList) {
              if (element.id == value.employeeData!.roleId) {
                selectedRoles.add(element);
              }
            }
            for (var element in schedulesList) {
              if (element.displayTitle == sun) {
                element.durationsList = value.employeeData!.availabilitySchedule!.sundayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.sundayAvailability;
              } else if (element.displayTitle == mon) {
                element.durationsList = value.employeeData!.availabilitySchedule!.mondayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.mondayAvailability;
              } else if (element.displayTitle == tue) {
                element.durationsList = value.employeeData!.availabilitySchedule!.tuesdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.tuesdayAvailability;
              } else if (element.displayTitle == wed) {
                element.durationsList = value.employeeData!.availabilitySchedule!.wednesdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.wednesdayAvailability;
              } else if (element.displayTitle == thu) {
                element.durationsList = value.employeeData!.availabilitySchedule!.thursdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.thursdayAvailability;
              } else if (element.displayTitle == fri) {
                element.durationsList = value.employeeData!.availabilitySchedule!.fridayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.fridayAvailability;
              } else if (element.displayTitle == sat) {
                element.durationsList = value.employeeData!.availabilitySchedule!.saturdayTimings!;
                element.value = value.employeeData!.availabilitySchedule!.saturdayAvailability;
              }
            }
            if (value.employeeData!.durationType!.toLowerCase() == fixed.toLowerCase()) {
              for (var element in durationsList) {
                if (element.value == value.employeeData!.duration!) {
                  durationInitialValue = element;
                  _durationDropDownKey.currentState!.didChange(durationInitialValue);
                }
              }
            } else if (value.employeeData!.durationType!.toLowerCase() == custom.toLowerCase()) {
              customDurationController.text = value.employeeData!.duration!.toString();
              for (var element in durationsList) {
                if (element.value == 0) {
                  durationInitialValue = element;
                  _durationDropDownKey.currentState!.didChange(durationInitialValue);
                }
              }
            }
            print('selectedEmployees ${selectedEmployees}');
            if(selectedEmployees.length != 0){
             // _multiKey.currentState!.changeSelectedItems(selectedEmployees);
            }
            _multiDesignationKey.currentState!.changeSelectedItems(selectedDesignations);
            _multiTechnologyKey.currentState!.changeSelectedItems(selectedTechnologies);
            // _multiRoleKey.currentState!.changeSelectedItems(selectedRoles);
            setState(() {});
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }

  checkData() {
    if (hasRoles && hasDesignations && hasTechnologies) {
      Navigator.of(context).pop();
    }
  }
}
