import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/day_based_employee_schedule_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmployeeNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  AllEmployeesResponse? _allEmployeesResponse;
  AllEmployeesResponse? _employeesResponse;

  AllEmployeesResponse? get allEmployeesResponse => _allEmployeesResponse;

  AllEmployeesResponse? get employeesResponse => _employeesResponse;

  List<EmployeeDetails> get viewAllEmployees {
    List<EmployeeDetails> list = [];
    if (allEmployeesResponse != null) {
      if (allEmployeesResponse!.responseStatus == 1) {
        if (allEmployeesResponse!.employeeDetails!.isNotEmpty) {
          list = allEmployeesResponse!.employeeDetails!;
        }
      }
    }
    return list;
  }

  List<EmployeeDetails> get getEmployees {
    List<EmployeeDetails> list = [];
    if (employeesResponse != null) {
      if (employeesResponse!.responseStatus == 1) {
        if (employeesResponse!.employeeDetails!.isNotEmpty) {
          list = employeesResponse!.employeeDetails!;
        }
      }
    }
    return list;
  }

  Future<AllEmployeesResponse?> viewAllEmployeesAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    _allEmployeesResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().viewAllEmployees(employeeId);
      if (response != null) {
        _allEmployeesResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allEmployeesResponse");
    notifyListeners();
    return _allEmployeesResponse;
  }

  Future<AllEmployeesResponse?> getEmployeesAPI(String type) async {
    _isFetching = true;
    _isHavingData = false;
    _employeesResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().getEmployees(type);
      if (response != null) {
        _employeesResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeesResponse");
    notifyListeners();
    return _employeesResponse;
  }
  Future<AllEmployeesResponse?> inferioremployeeslistApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    _employeesResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().inferioremployeeslistApi(employeeId);
      if (response != null) {
        _employeesResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_employeesResponse");
    notifyListeners();
    return _employeesResponse;
  }

  Future<BaseResponse?> addEmployeeAPI(String name, String email, String password, List<String> designationId, List<String> technologyId,
      String roleId, String createdBy, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration, String durationType,List<String> assignableEmployees, bool isSuperior) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().createEmployee(
          name, email, password, designationId, technologyId, roleId, createdBy, phoneNumber, availabilitySchedule, duration, durationType,assignableEmployees,isSuperior);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateEmployeeAPI(String name, String email, List<String> designationId, List<String> technologyId, String roleId,
      String employeeId, String userId, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration, String durationType,List<String> assignableEmployees, bool isSuperior) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateEmployee(
          name, email, designationId, technologyId, roleId, employeeId, userId, phoneNumber, availabilitySchedule, duration, durationType,assignableEmployees,isSuperior);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteEmployeeAPI(String loginEmployeeId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteEmployee(loginEmployeeId, employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> activateEmployeeAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().activateEmployee(employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deActivateEmployeeAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deActivateEmployee(employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<AllEmployeesResponse?> viewEmployeeAPI(String loginEmployeeId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllEmployeesResponse? _allEmployeesResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().viewEmployee(loginEmployeeId, employeeId);
      if (response != null) {
        _allEmployeesResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allEmployeesResponse");
    notifyListeners();
    return _allEmployeesResponse;
  }

  DayBasedEmployeeSchedulesModel? _dayBasedEmployeeSchedulesModel;

  DayBasedEmployeeSchedulesModel? get dayBasedEmployeeSchedulesModel {
    return _dayBasedEmployeeSchedulesModel;
  }
  DayBasedEmployeeSchedulesModel? get dayBasedEmployeeSchedulesResponse => _dayBasedEmployeeSchedulesModel;
  List<String?> get getAvailableSchedules {
    List<String?> list = [];
    if (dayBasedEmployeeSchedulesModel != null) {
      if (dayBasedEmployeeSchedulesModel!.responseStatus == 1) {
        if (dayBasedEmployeeSchedulesModel!.availableSchedule!.isNotEmpty) {
          list = dayBasedEmployeeSchedulesModel!.availableSchedule!;
        }
      }
    }
    return list;
  }

  Future<DayBasedEmployeeSchedulesModel?> viewDayBasedEmployeeScheduleAPI(String employeeId, String selectedDate) async {
    _isFetching = true;
    _isHavingData = false;
    _dayBasedEmployeeSchedulesModel = DayBasedEmployeeSchedulesModel();

    try {
      dynamic response = await Repository().viewDayBasedEmployeeSchedule(employeeId, selectedDate);
      if (response != null) {
        _dayBasedEmployeeSchedulesModel = DayBasedEmployeeSchedulesModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_dayBasedEmployeeSchedulesModel");
    notifyListeners();
    return _dayBasedEmployeeSchedulesModel;
  }

  Future<AllEmployeesResponse?> viewEmployeeProfileAPI(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    AllEmployeesResponse? _allEmployeesResponse = AllEmployeesResponse();

    try {
      dynamic response = await Repository().viewEmployeeProfile(employeeId);
      if (response != null) {
        _allEmployeesResponse = AllEmployeesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allEmployeesResponse");
    notifyListeners();
    return _allEmployeesResponse;
  }
}
