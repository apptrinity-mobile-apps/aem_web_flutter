import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/projects/ui/add_project_fragment.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/adaptable_text.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';

class ProjectFragment extends StatefulWidget {
  const ProjectFragment({Key? key}) : super(key: key);

  @override
  _ProjectFragmentState createState() => _ProjectFragmentState();
}

class _ProjectFragmentState extends State<ProjectFragment> {
  late ProjectNotifier viewModel;
  String employeeId = "";
  bool editPermission = false, addPermission = false;

  @override
  void initState() {
    viewModel = Provider.of<ProjectNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.projectEditPermission;
    addPermission = userViewModel.projectAddPermission;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuProjects", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: SizedBox(height: ScreenConfig.height(context), width: ScreenConfig.width(context), child: allProjectsList()),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(navMenuProjects, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Text(descriptionProject, softWrap: true, style: fragmentDescStyle),
                )
              ],
            ),
          ),
          Expanded(
            child: FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.scaleDown,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: buttonDecorationGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () {
                        if (addPermission) {
                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const AddProjectFragment(),
                              transitionDuration: const Duration(seconds: 0),
                              reverseTransitionDuration: const Duration(seconds: 0),
                            ),
                          ).then((value) {
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            addNewProject,
                            style: newButtonsStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  /*InkWell(
                    onTap: () {},
                    child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                  )*/
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget allProjectsList() {
    return FutureBuilder(
        future: viewModel.getAllProjectsAPI(employeeId, ''),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllProjectsResponse;
              if (data.responseStatus == 1) {
                if (data.projectList!.isNotEmpty) {
                  return GridView.builder(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      controller: ScrollController(),
                      physics: const ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: data.projectList!.length,
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 250),
                      itemBuilder: (BuildContext context, int index) {
                        return projectsList(data.projectList![index]);
                      });
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget projectsList(ProjectList itemData) {
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0), side: const BorderSide(color: dashBoardCardBorderTile, width: 0.4)),
          color: isHovered ? dashBoardCardHoverTile : dashBoardCardTile,
          child: InkWell(
            onTap: () {
              if (editPermission) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => AddProjectFragment(projectId: itemData.id!),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  // set state to reload data , reloads data in futureBuilder
                  setState(() {});
                });
              } else {
                showToast(noPermissionToAccess);
              }
            },
            child: Stack(
              children: [
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 25, 20, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: AdaptableText(
                            itemData.name!.inCaps,
                            style: isHovered ? projectTitleHoveredStyle : projectTitleStyle,
                            textMaxLines: 3,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: HtmlWidget(
                            parseHtmlString(itemData.description!),
                            buildAsync: true,
                            enableCaching: false,
                            renderMode: RenderMode.column,
                            textStyle: isHovered ? projectDescHoveredStyle : projectDescStyle,
                          ),
                        ),
                        Expanded(
                          child: itemData.employees!.isNotEmpty
                              ? ListView.builder(
                                  padding: const EdgeInsets.only(top: 5),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: itemData.employees!.length > 4 ? 4 : itemData.employees!.length,
                                  itemBuilder: (context, index) {
                                    return index == 0
                                        ? Align(
                                            widthFactor: 1,
                                            child: Tooltip(
                                              message: itemData.employees![index].name!,
                                              decoration: toolTipDecoration,
                                              textStyle: toolTipStyle,
                                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                              child: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                child: CircleAvatar(
                                                  backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                                  child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Align(
                                            widthFactor: 0.8,
                                            child: Tooltip(
                                              message: itemData.employees![index].name!,
                                              decoration: toolTipDecoration,
                                              textStyle: toolTipStyle,
                                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                              child: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                child: CircleAvatar(
                                                  backgroundColor: getNameBasedColor(itemData.employees![index].name!.toUpperCase()[0]),
                                                  child: Text(itemData.employees![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                                ),
                                              ),
                                            ),
                                          );
                                  })
                              : const SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                ),
                Positioned(
                  child: IconButton(
                    onPressed: () {
                      if (editPermission) {
                        showDeleteDialog(itemData);
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    icon: Icon(Icons.delete_forever_outlined, color: isHovered ? dashBoardCardBorderTile : newReOpened),
                  ),
                  right: 0,
                  top: 0,
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  showDeleteDialog(ProjectList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: '$project?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteProjectAPI(itemData.id!, employeeId).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
}
