import 'package:aem/model/all_boardings_response_model.dart';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';
import 'package:provider/provider.dart';

class AddProjectFragment extends StatefulWidget {
  final String? projectId;

  const AddProjectFragment({this.projectId, Key? key}) : super(key: key);

  @override
  _AddProjectFragmentState createState() => _AddProjectFragmentState();
}

class _AddProjectFragmentState extends State<AddProjectFragment> {
  List<BoardingsList> boardingsList = [], selectedBoardings = [];
  DateTime selectedDate = DateTime.now();
  List<DropdownMenuItem<BoardingsList>> boardingMenuItems = [];
  late TechnologyNotifier techViewModel = TechnologyNotifier();
  TextEditingController nameController = TextEditingController();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<TechnologiesList> technologyList = [], selectedTechnology = [];
  final _technologyKey = GlobalKey<DropdownSearchState<TechnologiesList>>();
  final _multiKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  List<String> employeeIds = [];
  List<String> technologyIds = [];
  late EmployeeNotifier employeeViewModel;
  late ProjectNotifier viewModel;
  String employeeId = "", projectId = "";
  final _formKey = GlobalKey<FormState>();
  final HtmlEditorController htmlController = HtmlEditorController();
  bool isVisible = true, hasTechnologies = false;
  BoardingsList boardingInitialValue = BoardingsList(boardName: selectTechnology);

  @override
  void initState() {
    employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    viewModel = Provider.of<ProjectNotifier>(context, listen: false);
    employeesList.add(EmployeeDetails(name: selectEmployee));
    technologyList.add(TechnologiesList(name: technology));
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    getData();
    Future.delayed(Duration.zero, () async {
      boardingMenuItems = [];
      boardingMenuItems.add(DropdownMenuItem(child: const Text(selectBoarding), value: boardingInitialValue));
      await viewModel.getBoardingsAPI().then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.boardingsList!.isNotEmpty) {
                boardingsList = value.boardingsList!;
                for (var element in value.boardingsList!) {
                  boardingMenuItems.add(DropdownMenuItem(child: Text(element.boardName!), value: element));
                }
              }
            });
          }
        }
        // hasTechnologies = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.projectHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuProjects", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                    child: Form(
                                      key: _formKey,
                                      child: PointerInterceptor(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            addProjectNameField(),
                                            addProjectDescriptionField(),
                                            assignEmployeeField(),
                                            addTechnologyField(),
                                            addBoardingField()
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.projectId == null ? project : updateProject, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddProject, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            employeeIds = [];
                            for (var element in selectedEmployees) {
                              employeeIds.add(element.id!);
                            }
                            technologyIds = [];
                            for (var element in selectedTechnology) {
                              technologyIds.add(element.id!);
                            }
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            htmlController.getText().then((value) async {
                              if (widget.projectId != null) {
                                await viewModel
                                    .updateProjectAPI(
                                        nameController.text.trim(), value, employeeIds, projectId, boardingInitialValue.id!, technologyIds)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              } else {
                                await viewModel
                                    .addProjectAPI(
                                        nameController.text.trim(), value, employeeIds, employeeId, boardingInitialValue.id!, technologyIds)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (value != null) {
                                    if (value.responseStatus == 1) {
                                      showToast(value.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(value.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              }
                            });
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget addProjectNameField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(projectName, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: TextFormField(
                      maxLines: 1,
                      controller: nameController,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      inputFormatters: [UpperCaseTextFormatter()],
                      textCapitalization: TextCapitalization.sentences,
                      obscureText: false,
                      decoration: editTextProjectNameDecoration,
                      style: textFieldStyle,
                      validator: emptyTextValidator,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget addProjectDescriptionField() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: SizedBox(
              width: 150,
              child: Text(description, style: addRoleSubStyle),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Visibility(
                visible: isVisible,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5), border: Border.all(color: textFieldBorder, width: 1.5), color: Colors.white),
                  height: 300,
                  child: HtmlEditor(
                    controller: htmlController,
                    htmlEditorOptions: const HtmlEditorOptions(
                      hint: 'Your text here...',
                      shouldEnsureVisible: true,
                      spellCheck: true,
                    ),
                    htmlToolbarOptions: HtmlToolbarOptions(
                      toolbarPosition: ToolbarPosition.aboveEditor,
                      toolbarType: ToolbarType.nativeExpandable,
                      initiallyExpanded: true,
                      defaultToolbarButtons: const [
                        StyleButtons(),
                        FontSettingButtons(fontSizeUnit: false),
                        FontButtons(clearAll: false),
                        ColorButtons(),
                        ListButtons(listStyles: false),
                        InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                      ],
                      onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                        return true;
                      },
                      onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                        return true;
                      },
                      mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                        return true;
                      },
                      mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                        return true;
                      },
                    ),
                    otherOptions: const OtherOptions(height: 250),
                    callbacks: Callbacks(
                        onBeforeCommand: (String? currentHtml) {},
                        onChangeContent: (String? changed) {},
                        onChangeCodeview: (String? changed) {},
                        onChangeSelection: (EditorSettings settings) {},
                        onDialogShown: () {},
                        onEnter: () {},
                        onFocus: () {},
                        onBlur: () {},
                        onBlurCodeview: () {},
                        onInit: () {},
                        onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                        onKeyDown: (int? keyCode) {},
                        onKeyUp: (int? keyCode) {},
                        onMouseDown: () {},
                        onMouseUp: () {},
                        onNavigationRequestMobile: (String url) {
                          return NavigationActionPolicy.ALLOW;
                        },
                        onPaste: () {},
                        onScroll: () {}),
                    plugins: [
                      SummernoteAtMention(
                          getSuggestionsMobile: (String value) {
                            var mentions = <String>['test1', 'test2', 'test3'];
                            return mentions
                                .where(
                                  (element) => element.contains(value),
                                )
                                .toList();
                          },
                          mentionsWeb: ['test1', 'test2', 'test3'],
                          onSelect: (String value) {}),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget assignEmployeeField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(assignEmployee, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownSearch<EmployeeDetails>.multiSelection(
                    key: _multiKey,
                    mode: Mode.MENU,
                    showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
                    compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                    items: employeesList,
                    showClearButton: true,
                    onChanged: (data) {
                      selectedEmployees = data;
                    },
                    clearButtonSplashRadius: 10,
                    selectedItems: selectedEmployees,
                    showSearchBox: true,
                    dropdownSearchDecoration: editTextEmployeesDecoration,
                    //validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                    dropdownBuilder: (context, selectedItems) {
                      return Wrap(
                          children: selectedItems
                              .map(
                                (e) => selectedItem(e.name!),
                              )
                              .toList());
                    },
                    filterFn: (EmployeeDetails? employee, name) {
                      return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                    },
                    popupItemBuilder: (_, text, isSelected) {
                      return Container(
                        padding: const EdgeInsets.all(10),
                        child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                      );
                    },
                    popupSelectionWidget: (cnt, item, bool isSelected) {
                      return Checkbox(
                          activeColor: buttonBg,
                          hoverColor: buttonBg.withOpacity(0.2),
                          value: isSelected,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                          side: const BorderSide(color: buttonBg),
                          onChanged: (value) {});
                    },
                    onPopupDismissed: () {
                      setState(() {
                        isVisible = true;
                      });
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget addTechnologyField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(technology, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownSearch<TechnologiesList>.multiSelection(
                    key: _technologyKey,
                    mode: Mode.MENU,
                    showSelectedItems: selectedTechnology.isNotEmpty ? true : false,
                    compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                    items: technologyList,
                    showClearButton: true,
                    onChanged: (data) {
                      selectedTechnology = data;
                    },
                    clearButtonSplashRadius: 10,
                    selectedItems: selectedTechnology,
                    showSearchBox: true,
                    dropdownSearchDecoration: editTextEmployeesDecoration,
                    // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                    dropdownBuilder: (context, selectedItems) {
                      return Wrap(
                          children: selectedItems
                              .map(
                                (e) => selectedItem(e.name!),
                              )
                              .toList());
                    },
                    filterFn: (TechnologiesList? employee, name) {
                      return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                    },
                    popupItemBuilder: (_, text, isSelected) {
                      return Container(
                        padding: const EdgeInsets.all(10),
                        child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                      );
                    },
                    popupSelectionWidget: (cnt, item, bool isSelected) {
                      return Checkbox(
                          activeColor: buttonBg,
                          hoverColor: buttonBg.withOpacity(0.2),
                          value: isSelected,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                          side: const BorderSide(color: buttonBg),
                          onChanged: (value) {});
                    },
                    onPopupDismissed: () {
                      setState(() {
                        isVisible = true;
                      });
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /*Widget addTechnologyField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(technology, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownButtonFormField<TechnologiesList>(
                items: technologiesMenuItems,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border,
                    isDense: true,
                    enabledBorder: border,
                    focusedBorder: border,
                    errorBorder: errorBorder,
                    focusedErrorBorder: errorBorder,
                    contentPadding: editTextPadding,
                    hintStyle: textFieldHintStyle,
                    hintText: status),
                style: textFieldStyle,
                value: technologyInitialValue,
                validator: technologiesDropDownValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (TechnologiesList? value) {
                  setState(() {
                    technologyInitialValue = value!;
                  });
                },
              ),
                ),),
            ),


          ],
        ),
      ),

    );
  }*/
  Widget addBoardingField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(boarding, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownButtonFormField<BoardingsList>(
                    items: boardingMenuItems,
                    decoration: InputDecoration(
                        fillColor: CupertinoColors.white,
                        filled: true,
                        border: border,
                        isDense: true,
                        enabledBorder: border,
                        focusedBorder: border,
                        errorBorder: errorBorder,
                        focusedErrorBorder: errorBorder,
                        contentPadding: editTextPadding,
                        hintStyle: textFieldHintStyle,
                        hintText: status),
                    style: textFieldStyle,
                    value: boardingInitialValue,
                    validator: boardingDropDownValidator,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (BoardingsList? value) {
                      setState(() {
                        boardingInitialValue = value!;
                      });
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getData() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await employeeViewModel.getEmployeesAPI("project").then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
      await techViewModel.getTechnologiesAPI().then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.technologyList!.isNotEmpty) {
                technologyList = value.technologyList!;
              }
            });
          }
        }
        hasTechnologies = true;
      });

      if (widget.projectId != null) {
        getProjectData();
      }
    });
  }

  void getProjectData() {
    Future.delayed(const Duration(milliseconds: 100), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewProjectAPI(widget.projectId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              projectId = value.projectDict!.id!;
              nameController.text = value.projectDict!.name!;
              htmlController.insertHtml(value.projectDict!.description!);

              if (value.projectDict!.technologies!.isNotEmpty) {
                for (var element in value.projectDict!.technologies!) {
                  technologyIds.add(element.id!);
                  for (var employee in technologyList) {
                    if (element.id! == employee.id) {
                      selectedTechnology.add(employee);
                    }
                  }
                }

                _technologyKey.currentState!.changeSelectedItems(selectedTechnology);

                print("selected technologies -- ${selectedTechnology}");
              } else {
                technologyIds = [];
              }

              if (value.projectDict!.employees!.isNotEmpty) {
                for (var element in value.projectDict!.employees!) {
                  employeeIds.add(element.id!);
                  for (var employee in employeesList) {
                    if (element.id! == employee.id) {
                      selectedEmployees.add(employee);
                    }
                  }
                }

                _multiKey.currentState!.changeSelectedItems(selectedEmployees);
              } else {
                employeeIds = [];
              }
              for (var element in boardingsList) {
                if (element.id == value.projectDict!.boardingId!) {
                  boardingInitialValue = element;
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
