import 'package:aem/model/all_projects_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/project_detail_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../model/all_boardings_response_model.dart';

class ProjectNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  Future<AllProjectsResponse?> getAllProjectsAPI(String employeeId,String from) async {
    _isFetching = true;
    _isHavingData = false;
    AllProjectsResponse? _allProjectsResponse = AllProjectsResponse();
    
    try {
      dynamic response = await Repository().getAllProjects(employeeId,from);
      if (response != null) {
        _allProjectsResponse = AllProjectsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allProjectsResponse");
    notifyListeners();
    return _allProjectsResponse;
  }

  Future<BaseResponse?> addProjectAPI(String name, String description, List<String> employees, String createdBy,String boardingId,List<String> technologyId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    
    try {
      dynamic response = await Repository().createProject(name, description, employees, createdBy,boardingId,technologyId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateProjectAPI(String name, String description, List<String> employees, String projectId,String boardingId,List<String> technologyId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    
    try {
      dynamic response = await Repository().updateProject(name, description, employees, projectId,boardingId,technologyId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deleteProjectAPI(String projectId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    
    try {
      dynamic response = await Repository().deleteProject(projectId, employeeId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> activateProjectAPI(String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    
    try {
      dynamic response = await Repository().activateProject(projectId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> deActivateProjectAPI(String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    
    try {
      dynamic response = await Repository().deActivateProject(projectId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<ProjectDetailModel?> viewProjectAPI(String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectDetailModel? _allProjectsResponse = ProjectDetailModel();
    
    try {
      dynamic response = await Repository().viewProject(projectId);
      if (response != null) {
        _allProjectsResponse = ProjectDetailModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allProjectsResponse");
    notifyListeners();
    return _allProjectsResponse;
  }
  Future<AllBoardingsResponse?> getBoardingsAPI() async {
    _isFetching = true;
    _isHavingData = false;
    AllBoardingsResponse? _allTechnologiesResponse = AllBoardingsResponse();

    try {
      dynamic response = await Repository().getBoardings();
      if (response != null) {
        _allTechnologiesResponse = AllBoardingsResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allTechnologiesResponse");
    notifyListeners();
    return _allTechnologiesResponse;
  }
}
