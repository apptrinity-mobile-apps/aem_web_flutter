import 'package:aem/model/all_profiles_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/job_roles_list_response_mode.dart';
import 'package:aem/model/profiles_dashboard_response.dart';
import 'package:aem/model/view_profile_details_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  AllProfilesResponse? _allProfilesResponse;

  List<ProfilesList>? get getAllProfiles {
    List<ProfilesList>? list = [];
    if (_allProfilesResponse != null) {
      if (_allProfilesResponse!.responseStatus == 1) {
        if (_allProfilesResponse!.profilesList!.isNotEmpty) {
          list = _allProfilesResponse!.profilesList!;
        }
      }
    }
    return list;
  }

  Future<AllProfilesResponse?> getAllProfilesAPI(String createdBy, String name, String profileStatusId, List employeesList, List technologiesList,
      int pageCount, String startDate, String endDate,List jobRolesList) async {
    _isFetching = true;
    _isHavingData = false;
    _allProfilesResponse = AllProfilesResponse();

    try {
      dynamic response =
          await Repository().getAllProfiles(createdBy, name, profileStatusId, employeesList, technologiesList, pageCount, startDate, endDate,jobRolesList);
      if (response != null) {
        _allProfilesResponse = AllProfilesResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allProfilesResponse");
    notifyListeners();
    return _allProfilesResponse;
  }
  Future<JobRolesResponseModel?> viewactivejobrolesApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    JobRolesResponseModel? _jobRolesResponseModel = JobRolesResponseModel();

    try {
      dynamic response = await Repository().viewactivejobrolesApi(employeeId);
      if (response != null) {
        _jobRolesResponseModel = JobRolesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_jobRolesResponseModel");
    notifyListeners();
    return _jobRolesResponseModel;
  }
  Future<BaseResponse?> deleteProfileAPI(String designationId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteProfile(designationId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  BaseResponse? _addProfileResponse;

  BaseResponse? get addProfileResponse {
    return _addProfileResponse;
  }

  Future<BaseResponse?> addProfileAPI(
      String createdBy,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    _isFetching = true;
    _isHavingData = false;
    _addProfileResponse = BaseResponse();
    try {
      dynamic response = await Repository().addProfile(createdBy, name, phoneNumber, careerStartedOn, experienceStatus, employmentStatus, email,
          mainTechnology, technology, superiorEmployees, resume, fileExtension, source, resumeFileName, jobRoleId);
      if (response != null) {
        _addProfileResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addProfileResponse");
    notifyListeners();
    return _addProfileResponse;
  }

  BaseResponse? _updateProfileResponse;

  BaseResponse? get updateProfileResponse {
    return _updateProfileResponse;
  }

  Future<BaseResponse?> updateProfileAPI(
      String createdBy,
      String profileId,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    _isFetching = true;
    _isHavingData = false;
    _updateProfileResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateProfile(createdBy, profileId, name, phoneNumber, careerStartedOn, experienceStatus,
          employmentStatus, email, mainTechnology, technology, superiorEmployees, resume, fileExtension, source, resumeFileName, jobRoleId);
      if (response != null) {
        _updateProfileResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateProfileResponse");
    notifyListeners();
    return _updateProfileResponse;
  }

  ViewProfileDetailsModel? _getProfileResponse;

  ProfilesDetails? get getProfileResponse {
    if (_getProfileResponse != null) {
      if (_getProfileResponse!.responseStatus == 1) {
        return _getProfileResponse!.profilesDetails;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  List<InterviewSchedulesList> get getInterviewSchedulesList {
    List<InterviewSchedulesList> list = [];
    if (_getProfileResponse != null) {
      if (_getProfileResponse!.responseStatus == 1) {
        if (_getProfileResponse!.profilesDetails != null) {
          if (_getProfileResponse!.profilesDetails!.interviewSchedulesList!.isNotEmpty) {
            list = getProfileResponse!.interviewSchedulesList!;
          }
        }
      }
    }
    return list;
  }

  Future<ViewProfileDetailsModel?> viewProfileAPI(String profileId) async {
    _isFetching = true;
    _isHavingData = false;
    _getProfileResponse = ViewProfileDetailsModel();
    try {
      dynamic response = await Repository().viewProfile(profileId);
      if (response != null) {
        _getProfileResponse = ViewProfileDetailsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_getProfileResponse");
    notifyListeners();
    return _getProfileResponse;
  }

  BaseResponse? _interviewScheduleResponse;

  BaseResponse? get interviewScheduleResponse {
    return _interviewScheduleResponse;
  }

  Future<BaseResponse?> addInterviewScheduleAPI(String employeeId, String scheduledBy, String profileId, String interviewType, String interviewTime,
      String comments, String mailSubject, String mailBody) async {
    _isFetching = true;
    _isHavingData = false;
    _interviewScheduleResponse = BaseResponse();
    try {
      dynamic response =
          await Repository().addInterviewSchedule(employeeId, scheduledBy, profileId, interviewType, interviewTime, comments, mailSubject, mailBody);
      if (response != null) {
        _interviewScheduleResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_interviewScheduleResponse");
    notifyListeners();
    return _interviewScheduleResponse;
  }

  BaseResponse? _updateInterviewScheduleResponse;

  BaseResponse? get updateInterviewScheduleResponse {
    return _updateInterviewScheduleResponse;
  }

  Future<BaseResponse?> updateInterviewScheduleAPI(String employeeId, String scheduledId, String interviewStatusId, String comments) async {
    _isFetching = true;
    _isHavingData = false;
    _updateInterviewScheduleResponse = BaseResponse();
    try {
      dynamic response = await Repository().updateInterviewSchedule(employeeId, scheduledId, interviewStatusId, comments);
      if (response != null) {
        _updateInterviewScheduleResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateInterviewScheduleResponse");
    notifyListeners();
    return _updateInterviewScheduleResponse;
  }

  BaseResponse? _addCommentResponse;

  BaseResponse? get addCommentResponse {
    return _addCommentResponse;
  }

  Future<BaseResponse?> addProfileCommentAPI(String employeeId, String profileId, String comment) async {
    _isFetching = true;
    _isHavingData = false;
    _addCommentResponse = BaseResponse();
    try {
      dynamic response = await Repository().addProfileComment(employeeId, profileId, comment);
      if (response != null) {
        _addCommentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addCommentResponse");
    notifyListeners();
    return _addCommentResponse;
  }

  BaseResponse? _statusUpdateResponse;

  BaseResponse? get statusUpdateResponse {
    return _statusUpdateResponse;
  }

  Future<BaseResponse?> updateProfileStatusAPI(String employeeId, String profileId, String employmentStatus, String comment) async {
    _isFetching = true;
    _isHavingData = false;
    _statusUpdateResponse = BaseResponse();
    try {
      dynamic response = await Repository().updateProfileStatus(employeeId, profileId, employmentStatus, comment);
      if (response != null) {
        _statusUpdateResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_statusUpdateResponse");
    notifyListeners();
    return _statusUpdateResponse;
  }

  BaseResponse? _requestResumeResponse;

  BaseResponse? get requestResumeResponse {
    return _requestResumeResponse;
  }

  Future<BaseResponse?> requestResumeAPI(String profileId) async {
    _isFetching = true;
    _isHavingData = false;
    _requestResumeResponse = BaseResponse();
    try {
      dynamic response = await Repository().requestResume(profileId);
      if (response != null) {
        _requestResumeResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_requestResumeResponse");
    notifyListeners();
    return _requestResumeResponse;
  }

  BaseResponse? _addSystemTestResponse;

  BaseResponse? get addSystemTestResponse {
    return _addSystemTestResponse;
  }

  Future<BaseResponse?> addSystemTestAPI(
      String employeeId, String scheduledBy, String profileId, String comment, String systemTestTime, List<String> supportingDocs) async {
    _isFetching = true;
    _isHavingData = false;
    _addSystemTestResponse = BaseResponse();
    try {
      dynamic response = await Repository().addSystemTest(employeeId, scheduledBy, profileId, comment, systemTestTime, supportingDocs);
      if (response != null) {
        _addSystemTestResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addSystemTestResponse");
    notifyListeners();
    return _addSystemTestResponse;
  }

  ProfilesDashboardResponse? _profilesDashboardResponse;

  List<InterviewsList>? get getTodayInterviewProfiles {
    List<InterviewsList>? list = [];
    if (_profilesDashboardResponse != null) {
      if (_profilesDashboardResponse!.responseStatus == 1) {
        if (_profilesDashboardResponse!.interviewsList!.isNotEmpty) {
          list = _profilesDashboardResponse!.interviewsList!;
        }
      }
    }
    return list;
  }

  CountsData? get countsData {
    CountsData? data = CountsData(profilesCount: 0,todayInterviewsCount: 0,upcomingInterviewsCount: 0);
    if (_profilesDashboardResponse != null) {
      if (_profilesDashboardResponse!.responseStatus == 1) {
        data = _profilesDashboardResponse!.countsData;
      }
    }
   return data;
  }

  Future<ProfilesDashboardResponse?> getTodayInterviewDashboardAPI(
      String employeeId,String name, List<String> technologiesList, List<String> employeesList, int pageNumber)  async {
    _isFetching = true;
    _isHavingData = false;
    _profilesDashboardResponse = ProfilesDashboardResponse();

    try {
      dynamic response =
      await Repository().getTodayInterviewDashboard(employeeId, name, technologiesList,  employeesList,  pageNumber);
      if (response != null) {
        _profilesDashboardResponse = ProfilesDashboardResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_profilesDashboardResponse");
    notifyListeners();
    return _profilesDashboardResponse;
  }
}
