import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/profiles_dashboard_response.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/profiles/ui/view_profile_fragment.dart';
import 'package:aem/screens/profiles/view_model/profile_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class ProfilesDashboardFragment extends StatefulWidget {
  const ProfilesDashboardFragment({Key? key}) : super(key: key);
  @override
  _ProfilesDashboardFragmentState createState() => _ProfilesDashboardFragmentState();
}
class _ProfilesDashboardFragmentState extends State<ProfilesDashboardFragment> {
  String employeeId = "";
  bool isFiltersSelected = false;
  final _technologyKey = GlobalKey<DropdownSearchState<TechnologiesList>>();
  final _employeeKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  List<TechnologiesList> technologyList = [], selectedTechnologies = [];
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  late TechnologyNotifier techViewModel = TechnologyNotifier();
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  final scrollController = ScrollController();
  final tableScrollController = ScrollController();
  int pageCount = 0;
  List<String> employeeIds = [], technologyIds = [];
  late ProfileNotifier viewModel;
  List<InterviewsList> candidateProfilesList = [];
  TextEditingController searchController = TextEditingController();
  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    viewModel = Provider.of<ProfileNotifier>(context, listen: false);
    getData();
    tableScrollController.addListener(() {
      if (tableScrollController.position.pixels == tableScrollController.position.maxScrollExtent) {
        getTodayInterviews(pageCount);
      }
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuDashboard", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [header(), body()],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(navMenuDashboard, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              ],
            ),
          ),
        ],
      ),
    );
  }
  Widget body() {
    return Flexible(
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        child: SingleChildScrollView(
          controller: scrollController,
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              interviewCountsWidget(),
              filterWidget(),
              interviewsTable(),
            ],
          ),
        ),
      ),
    );
  }
  Widget interviewCountsWidget() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: true);
    return GridView(
      shrinkWrap: true,
      gridDelegate:
          const SliverGridDelegateWithMaxCrossAxisExtent(childAspectRatio: 3.7, mainAxisSpacing: 4, crossAxisSpacing: 4, maxCrossAxisExtent: 500),
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: const BorderSide(color: totalProfilesCardColor, width: 1),
          ),
          color: totalProfilesCardBgColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Wrap(
              runAlignment: WrapAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                  child: Image.asset(
                    "assets/images/total_profiles_2x.png",
                    fit: BoxFit.fill,
                    height: 60,
                    width: 60,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        totalProfiles,
                        softWrap: true,
                        style: TextStyle(letterSpacing: 0.66, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 18),
                      ),
                      Text(
                        viewModel.countsData!.profilesCount.toString(),
                        softWrap: true,
                        style:
                            const TextStyle(letterSpacing: 1.26, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 24),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: const BorderSide(color: todayInterviewsCardColor, width: 1),
          ),
          color: todayInterviewsCardBgColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Wrap(
              runAlignment: WrapAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                  child: Image.asset(
                    "assets/images/today_profiles_2x.png",
                    fit: BoxFit.fill,
                    height: 60,
                    width: 60,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        todayInterviews,
                        softWrap: true,
                        overflow: TextOverflow.clip,
                        style: TextStyle(letterSpacing: 0.66, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 18),
                      ),
                      Text(
                        viewModel.countsData!.todayInterviewsCount.toString(),
                        softWrap: true,
                        style:
                            const TextStyle(letterSpacing: 1.26, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 24),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: const BorderSide(color: totalProfilesCardColor, width: 1),
          ),
          color: totalProfilesCardBgColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Wrap(
              runAlignment: WrapAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                  child: Image.asset(
                    "assets/images/upcoming_interview_2x.png",
                    fit: BoxFit.fill,
                    height: 60,
                    width: 60,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        upcomingInterviews,
                        softWrap: true,
                        overflow: TextOverflow.clip,
                        style: TextStyle(letterSpacing: 0.66, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 18),
                      ),
                      Text(
                        viewModel.countsData!.upcomingInterviewsCount.toString(),
                        softWrap: true,
                        style:
                            const TextStyle(letterSpacing: 1.26, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 24),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
  Widget filterWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
      child: Wrap(
        alignment: WrapAlignment.start,
        crossAxisAlignment: WrapCrossAlignment.center,
        runAlignment: WrapAlignment.start,
        spacing: 10,
        runSpacing: 10,
        children: [
          const Text(
            todaysInterviews,
            softWrap: true,
            style: TextStyle(letterSpacing: 0.84, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 18),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: 300, maxWidth: 300),
            child:  TextFormField(
                maxLines: 1,
                controller: searchController,
                textInputAction: TextInputAction.done,
                textCapitalization: TextCapitalization.sentences,
                obscureText: false,
                decoration: editTextNameDecoration.copyWith(
                  labelText: search,
                  hintText: "$name, $email or $phone",
                  contentPadding: const EdgeInsets.fromLTRB(10, 13, 10, 13),
                ),
                style: textFieldStyle),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: 300, maxWidth: 300),
            child: DropdownSearch<EmployeeDetails>.multiSelection(
              key: _employeeKey,
              mode: Mode.MENU,
              showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
              compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
              items: employeesList,
              showClearButton: true,
              onChanged: (data) {
                selectedEmployees = data;
              },
              clearButtonSplashRadius: 10,
              selectedItems: selectedEmployees,
              showSearchBox: true,
              dropdownSearchDecoration: editTextEmployeesDecoration.copyWith(labelText: tableInterviewer),
              validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
              dropdownBuilder: (context, selectedItems) {
                return Wrap(children: selectedItems.map((e) => selectedItem(e.name!)).toList());
              },
              filterFn: (EmployeeDetails? employee, name) {
                return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
              },
              popupItemBuilder: (_, text, isSelected) {
                return Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                );
              },
              popupSelectionWidget: (cnt, item, bool isSelected) {
                return Checkbox(
                    activeColor: buttonBg,
                    hoverColor: buttonBg.withOpacity(0.2),
                    value: isSelected,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                    side: const BorderSide(color: buttonBg),
                    onChanged: (value) {});
              },
              onPopupDismissed: () {},
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: 300, maxWidth: 300),
            child: DropdownSearch<TechnologiesList>.multiSelection(
              key: _technologyKey,
              mode: Mode.MENU,
              showSelectedItems: selectedTechnologies.isNotEmpty ? true : false,
              compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
              items: technologyList,
              showClearButton: true,
              onChanged: (data) {
                selectedTechnologies = data;
              },
              clearButtonSplashRadius: 10,
              selectedItems: selectedTechnologies,
              showSearchBox: true,
              dropdownSearchDecoration: editTextEmployeesDecoration.copyWith(labelText: technology),
              validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
              dropdownBuilder: (context, selectedItems) {
                return Wrap(children: selectedItems.map((e) => selectedItem(e.name!)).toList());
              },
              filterFn: (TechnologiesList? employee, name) {
                return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
              },
              popupItemBuilder: (_, text, isSelected) {
                return Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                );
              },
              popupSelectionWidget: (cnt, item, bool isSelected) {
                return Checkbox(
                    activeColor: buttonBg,
                    hoverColor: buttonBg.withOpacity(0.2),
                    value: isSelected,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                    side: const BorderSide(color: buttonBg),
                    onChanged: (value) {});
              },
              onPopupDismissed: () {},
            ),
          ),
          TextButton(
            onPressed: () {
              if (searchController.text.trim().toString() != "" || selectedTechnologies.isNotEmpty || selectedEmployees.isNotEmpty) {
                setState(() {
                  candidateProfilesList = [];
                  isFiltersSelected = true;
                  pageCount = 0;
                  getTodayInterviews(pageCount);
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Wrap(
                alignment: WrapAlignment.spaceBetween,
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 10,
                children: [
                  Image.asset(
                    "assets/images/filter_2x.png",
                    height: 15,
                    width: 15,
                    fit: BoxFit.fill,
                  ),
                  const Text(
                    filter,
                    softWrap: true,
                    style: TextStyle(letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: buttonBg, fontSize: 14),
                  ),
                ],
              ),
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(filterBg),
              overlayColor: MaterialStateProperty.all(buttonBg.withOpacity(0.1)),
              side: MaterialStateProperty.all(const BorderSide(color: filterBg)),
              elevation: MaterialStateProperty.all(1),
            ),
          ),
          (isFiltersSelected)
              ? TextButton(
                  onPressed: () {
                    setState(() {
                      isFiltersSelected = false;
                      selectedEmployees = [];
                      selectedTechnologies = [];
                      candidateProfilesList = [];
                      _technologyKey.currentState!.clear();
                      _employeeKey.currentState!.clear();
                      searchController.text = "";
                      pageCount = 0;
                      getTodayInterviews(pageCount);
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Wrap(
                      alignment: WrapAlignment.spaceBetween,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 10,
                      children: [
                        const Text(
                          reset,
                          softWrap: true,
                          style: TextStyle(
                              letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: dashBoardCardOverDueText, fontSize: 14),
                        ),
                        Image.asset(
                          "assets/images/cancel_red_2x.png",
                          height: 15,
                          width: 15,
                          fit: BoxFit.fill,
                        ),
                      ],
                    ),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(resetBg),
                    overlayColor: MaterialStateProperty.all(dashBoardCardOverDueText.withOpacity(0.1)),
                    side: MaterialStateProperty.all(const BorderSide(color: resetBg)),
                    elevation: MaterialStateProperty.all(1),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
  Widget interviewsTable() {
    return Flexible(
      child: ClipRRect(
        borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        child: Theme(
          data: Theme.of(context).copyWith(dividerColor: buttonBg.withOpacity(0.2)),
          child: DataTable2(
            columnSpacing: 14,
            dividerThickness: 1,
            scrollController: tableScrollController,
            decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
            headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
            headingRowHeight: 46,
            smRatio: 0.4,
            lmRatio: 1.3,
            columns: <DataColumn2>[
              DataColumn2(label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.S),
              DataColumn2(label: Text(tableName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.L),
              DataColumn2(label: Text(tableEmail, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.L),
              DataColumn2(label: Text(technology, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.L),
              DataColumn2(label: Text(tableInterviewer, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.L),
              DataColumn2(label: Text("$date & Time", softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
              DataColumn2(label: Text(tableRounds, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.M),
              DataColumn2(label: Text(tableActions, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start), size: ColumnSize.S),
            ],
            rows: listItems(candidateProfilesList),
            empty: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 50, bottom: 50),
                child: Text(noInterviewsToday, style: logoutHeader),
              ),
            ),
          ),
        ),
      ),
    );
  }
  List<DataRow> listItems(List<InterviewsList?> profilesList) {
    return profilesList.map<DataRow2>((e) {
      return DataRow2(
          color: MaterialStateColor.resolveWith((states) => CupertinoColors.white),
          specificRowHeight: 66,
          onTap: () {
            Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (context, animation1, animation2) => ViewProfileFragment(profileId: e!.profileId),
                        transitionDuration: const Duration(seconds: 0),
                        reverseTransitionDuration: const Duration(seconds: 0)))
                .then((value) {
              setState(() {
                candidateProfilesList = [];
                pageCount = 0;
                getTodayInterviews(pageCount);
              });
            });
          },
          cells: [
            DataCell(
              Text((profilesList.indexOf(e) + 1).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e!.name!.inCaps, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.email!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.mainTechnology!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.interviewerName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.interviewTime!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.round!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
            DataCell(
              Wrap(
                alignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.center,
                runAlignment: WrapAlignment.start,
                runSpacing: 4,
                spacing: 4,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: view,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/view_rounded.png'),
                          fit: BoxFit.cover,
                          width: 28,
                          height: 28,
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => ViewProfileFragment(profileId: e.profileId),
                                          transitionDuration: const Duration(seconds: 0),
                                          reverseTransitionDuration: const Duration(seconds: 0)))
                                  .then((value) {
                                setState(() {
                                  candidateProfilesList = [];
                                  pageCount = 0;
                                  getTodayInterviews(pageCount);
                                });
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]);
    }).toList();
  }
  void getData() {
    Future.delayed(Duration.zero, () async {
      getTodayInterviews(pageCount);
      employeesList = [];
      technologyList = [];
      await techViewModel.getTechnologiesAPI().then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.technologyList!.isNotEmpty) {
                technologyList.addAll(value.technologyList!);
              }
            });
          }
        }
      });
      await employeeViewModel.getEmployeesAPI("").then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList.addAll(value.employeeDetails!);
              }
            });
          }
        }
      });
    });
  }
  Future getTodayInterviews(int pageNo) async {
    employeeIds = [];
    technologyIds = [];
    for (var element in selectedEmployees) {
      employeeIds.add(element.id!);
    }
    for (var element in selectedTechnologies) {
      technologyIds.add(element.id!);
    }
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                loader(),
                pageCount != 0
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          profilesLoading,
                          style: newButtonsStyle,
                        ),
                      )
                    : const SizedBox()
              ],
            ),
          );
        });
    return await viewModel
        .getTodayInterviewDashboardAPI(employeeId, searchController.text.trim().toString(), technologyIds, employeeIds, pageNo)
        .then((value) {
      Navigator.of(context).pop();
      setState(() {
        candidateProfilesList.addAll(viewModel.getTodayInterviewProfiles!);
        pageCount++;
      });
    });
  }
}
