import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:aem/model/all_designations_response_model.dart';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/model/all_roles_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/job_roles_list_response_mode.dart';
import 'package:aem/screens/designations/view_model/designation_notifier.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/screens/profiles/view_model/profile_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddProfileFragment extends StatefulWidget {
  final String? profileId;

  const AddProfileFragment({this.profileId, Key? key}) : super(key: key);

  @override
  _AddProfileFragmentState createState() => _AddProfileFragmentState();
}

class _AddProfileFragmentState extends State<AddProfileFragment> {
  DateTime selectedDate = DateTime.now();
  TextEditingController dateController = TextEditingController();

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController resumeUploadController = TextEditingController();
  TextEditingController careerStartDateController = TextEditingController();
  TextEditingController sourceController = TextEditingController();

  DesignationsList designationInitialValue = DesignationsList(name: selectDesignation);
  TechnologiesList technologyInitialValue = TechnologiesList(name: selectTechnology);
  ProfilesStatusList profileStatusInitialValue = ProfilesStatusList(statusName: selectStatus, id: 'none');
  EmployeeDetails employeeInitialValue = EmployeeDetails(name: selectEmployee);
  List<DropdownMenuItem<DesignationsList>> designationsMenuItems = [];
  List<DropdownMenuItem<TechnologiesList>> technologiesMenuItems = [];
  List<DropdownMenuItem<ProfilesStatusList>> profileStatusesMenuItems = [];
  List<DesignationsList> designationsList = [], selectedDesignations = [];
  List<TechnologiesList> technologiesList = [], selectedTechnologies = [];
  List<RolesDetails> rolesList = [], selectedRoles = [];
  List<String> technologyIds = [], designationIds = [];

  late TechnologyNotifier techViewModel;
  late DesignationNotifier designViewModel;
  String employeeId = "", userId = "";
  bool hasEmployees = false, hasRoles = false, hasDesignations = false, hasTechnologies = false;

  JobRolesList jobRoleInitialValue = JobRolesList(role: selectjobRole, id: 'none');
  List<DropdownMenuItem<JobRolesList>> jobRoleMenuItems = [];


  final _formKey = GlobalKey<FormState>();
  final _multiKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _multiTechnologyKey = GlobalKey<DropdownSearchState<TechnologiesList>>();
  late ProfileNotifier viewModel;
  late EmployeeNotifier employeeViewModel;
  late ProfileStatusNotifier profileStatusViewModel;

  String designationId = "";
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<String> employeeIds = [];
  File? pdfFile;
  String uploadFile = "", fileExtension = "";
  List<DropdownMenuItem<EmployeeDetails>> employeesMenuItems = [];
  int experienceRadioValue = 0;
  AgeDuration? exp;
  var experienceStatus = "fresher", careerStartDate = "", createdBy = "";

  @override
  void initState() {
    employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    viewModel = Provider.of<ProfileNotifier>(context, listen: false);
    techViewModel = Provider.of<TechnologyNotifier>(context, listen: false);
    profileStatusViewModel = Provider.of<ProfileStatusNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    getData();
    /*if (widget.profileId != null) {
      getProfileData();
    }*/
    careerStartDateFresher();
    super.initState();
  }

  void getData() {
    Future.delayed(Duration.zero, () async {
      employeesMenuItems = [];
      employeesMenuItems.add(DropdownMenuItem(child: const Text(selectEmployee), value: employeeInitialValue));
      technologiesMenuItems = [];
      technologiesMenuItems.add(DropdownMenuItem(child: const Text(selectTechnology), value: technologyInitialValue));
      profileStatusesMenuItems = [];
      profileStatusesMenuItems.add(DropdownMenuItem(child: const Text(selectStatus), value: profileStatusInitialValue));
      await employeeViewModel.getEmployeesAPI("hr").then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
                for (var element in value.employeeDetails!) {
                  employeesMenuItems.add(
                    DropdownMenuItem(
                        child: RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(text: element.name!, style: projectAddedByStyle),
                            TextSpan(text: ' (${commaSeparatedTechnologiesString(element.technologiesList!)})', style: completedStyle)
                          ]),
                        ),
                        value: element),
                  );
                }
              }
            });
          }
        }
        hasEmployees = true;
      });
      await techViewModel.getTechnologiesAPI().then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.technologyList!.isNotEmpty) {
                technologiesList = value.technologyList!;
                for (var element in value.technologyList!) {
                  technologiesMenuItems.add(DropdownMenuItem(child: Text(element.name!), value: element));
                }
              }
            });
          }
        }
        hasTechnologies = true;
      });
      debugPrint(employeeId);
      await profileStatusViewModel.getProfileStatusesAPI().then((value) {
        for (var element in profileStatusViewModel.getProfileStatuses!) {
          profileStatusesMenuItems.add(DropdownMenuItem(child: Text(element.statusName!), value: element));
        }
      });
      jobRoleMenuItems = [];
      jobRoleMenuItems.add(DropdownMenuItem(child: const Text(selectjobRole), value: jobRoleInitialValue));
      await viewModel.viewactivejobrolesApi(employeeId).then((value) {
        setState(() {

        for (var element in value!.jobRolesList!) {

          jobRoleMenuItems.add(DropdownMenuItem(child: Text(element.role!), value: element));

        }
        print("jobRoleMenuItems ${jobRoleMenuItems}");
        });
      });
      checkData();
    });
  }

  checkData() {
    if (hasEmployees && hasTechnologies) {
      //Navigator.of(context).pop();
      if (widget.profileId != null) {
        getProfileData();
      }
    }
  }

  getProfileData() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: false);
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewProfileAPI(widget.profileId!).then((value) {
        if (viewModel.getProfileResponse != null) {
          if (viewModel.getProfileResponse!.technologyList!.isNotEmpty) {
            for (var element in viewModel.getProfileResponse!.technologyList!) {
              TechnologiesList technology = TechnologiesList(name: element.name, id: element.technologyId);
              selectedTechnologies.add(technology);
            }
          }
          if (viewModel.getProfileResponse!.superiorEmployees!.isNotEmpty) {
            for (var element in viewModel.getProfileResponse!.superiorEmployees!) {
              debugPrint(element.superiorEmployeeId);
              EmployeeDetails employee = EmployeeDetails(name: element.superiorEmployeeName, id: element.superiorEmployeeId);
              selectedEmployees.add(employee);
            }
          }
          setState(() {
            print("jobRoleId  -- ${viewModel.getProfileResponse!.jobRoleId}");

            for (var element in jobRoleMenuItems) {
              print("RoleId  -- ${element.value!.id}");
              if (element.value!.id == viewModel.getProfileResponse!.jobRoleId) {
                jobRoleInitialValue = element.value!;
              }
            }
            for (var element in profileStatusesMenuItems) {
              if (element.value!.id == viewModel.getProfileResponse!.employmentStatusId) {
                profileStatusInitialValue = element.value!;
              }
            }
            for (var element in technologiesMenuItems) {
              if (element.value!.id == viewModel.getProfileResponse!.mainTechnology) {
                technologyInitialValue = element.value!;
              }
            }
            if (viewModel.getProfileResponse!.experienceStatus!.toLowerCase() == 'experienced') {
              experienceRadioValue = 1;
              experienceStatus = viewModel.getProfileResponse!.experienceStatus!;
            } else if (viewModel.getProfileResponse!.experienceStatus!.toLowerCase() == 'fresher') {
              experienceRadioValue = 0;
              experienceStatus = viewModel.getProfileResponse!.experienceStatus!;
            } else {
              experienceRadioValue = 0;
              experienceStatus = "fresher";
            }
            createdBy = viewModel.getProfileResponse!.createdBy!;
          });
          nameController.text = viewModel.getProfileResponse!.name!;
          sourceController.text = viewModel.getProfileResponse!.source!;
          emailController.text = viewModel.getProfileResponse!.email!;
          phoneController.text = viewModel.getProfileResponse!.phoneNumber!;
          if (viewModel.getProfileResponse!.careerStartedOn! != "") {
            careerStartDate = dateFormatMDYHyphen(viewModel.getProfileResponse!.careerStartedOn!);
            careerStartDateController.text = dateFormatDMYSlash(viewModel.getProfileResponse!.careerStartedOn!);
          } else {
            careerStartDate = "";
            careerStartDateController.text = "";
          }
          _multiKey.currentState!.changeSelectedItems(selectedEmployees);
          _multiTechnologyKey.currentState!.changeSelectedItems(selectedTechnologies);
        }
        Navigator.of(context).pop();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.profileHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuProfiles", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: SingleChildScrollView(
                                            controller: ScrollController(),
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                              child: Form(
                                                key: _formKey,
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [nameField(), emailField()],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [phoneField(), uploadResumeField()],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [mainTechnologyField(), technologyFieldMulti()],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [assignEmployeeField(), statusField()],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [experienceField(), careerStartField()],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [sourceField(), jobRoleField()],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.profileId == null ? addProfile : updateProfile, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddProfile, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          technologyIds = [];
                          employeeIds = [];
                          for (var element in selectedTechnologies) {
                            technologyIds.add(element.id!);
                          }
                          for (var element in selectedEmployees) {
                            employeeIds.add(element.id!);
                          }
                          if (_formKey.currentState!.validate()) {
                            if (widget.profileId == null) {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              viewModel
                                  .addProfileAPI(
                                      employeeId,
                                      nameController.text.trim(),
                                      phoneController.text.trim(),
                                      careerStartDate,
                                      experienceStatus,
                                      profileStatusInitialValue.id!,
                                      emailController.text.trim(),
                                      technologyInitialValue.id!,
                                      technologyIds,
                                      employeeIds,
                                      uploadFile,
                                      fileExtension,
                                      sourceController.text,
                                      resumeUploadController.text,jobRoleInitialValue.id!)
                                  .then((value) {
                                Navigator.pop(context);
                                if (viewModel.addProfileResponse != null) {
                                  if (viewModel.addProfileResponse!.responseStatus == 1) {
                                    showToast(viewModel.addProfileResponse!.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(viewModel.addProfileResponse!.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            } else {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              viewModel
                                  .updateProfileAPI(
                                      createdBy,
                                      widget.profileId!,
                                      nameController.text.trim(),
                                      phoneController.text.trim(),
                                      careerStartDate,
                                      experienceStatus,
                                      profileStatusInitialValue.id!,
                                      emailController.text.trim(),
                                      technologyInitialValue.id!,
                                      technologyIds,
                                      employeeIds,
                                      uploadFile,
                                      fileExtension,
                                      sourceController.text,
                                      resumeUploadController.text,jobRoleInitialValue.id!)
                                  .then((value) {
                                Navigator.pop(context);
                                if (viewModel.updateProfileResponse != null) {
                                  if (viewModel.updateProfileResponse!.responseStatus == 1) {
                                    showToast(viewModel.updateProfileResponse!.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(viewModel.updateProfileResponse!.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget nameField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(name, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                  maxLines: 1,
                  controller: nameController,
                  textInputAction: TextInputAction.done,
                  textCapitalization: TextCapitalization.sentences,
                  obscureText: false,
                  decoration: editTextNameDecoration,
                  style: textFieldStyle,
                  validator: emptyTextValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
          )
        ],
      ),
    );
  }

  Widget sourceField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(source, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                  maxLines: 1,
                  controller: sourceController,
                  textInputAction: TextInputAction.done,
                  textCapitalization: TextCapitalization.sentences,
                  obscureText: false,
                  decoration: editTextNameDecoration.copyWith(hintText: source),
                  style: textFieldStyle,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
          )
        ],
      ),
    );
  }

  Widget emailField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(email, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                  maxLines: 1,
                  controller: emailController,
                  textInputAction: TextInputAction.done,
                  obscureText: false,
                  decoration: editTextEmailDecoration,
                  style: textFieldStyle,
                  validator: emailValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
          )
        ],
      ),
    );
  }

  Widget phoneField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(phone, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                  maxLines: 1,
                  controller: phoneController,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'[0-9]')), LengthLimitingTextInputFormatter(10)],
                  obscureText: false,
                  decoration: editTextPhoneNumberDecoration,
                  style: textFieldStyle,
                  validator: phoneValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
            ),
          )
        ],
      ),
    );
  }

  Widget technologyFieldMulti() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text("Additional technologies", style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: DropdownSearch<TechnologiesList>.multiSelection(
                  key: _multiTechnologyKey,
                  mode: Mode.MENU,
                  maxHeight: 400,
                  showSelectedItems: selectedTechnologies.isNotEmpty ? true : false,
                  compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                  items: technologiesList,
                  showClearButton: true,
                  onChanged: (data) {
                    selectedTechnologies = data;
                  },
                  clearButtonSplashRadius: 10,
                  selectedItems: selectedTechnologies,
                  showSearchBox: true,
                  dropdownSearchDecoration: editTextEmployeesDecoration,
                  // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                  dropdownBuilder: (context, selectedItems) {
                    return Wrap(
                        children: selectedItems
                            .map(
                              (item) => Container(
                                padding: const EdgeInsets.all(5),
                                margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Theme.of(context).primaryColorLight),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(item.name!, textAlign: TextAlign.center, style: textFieldStyle),
                                    MaterialButton(
                                      height: 20,
                                      shape: const CircleBorder(),
                                      focusColor: Colors.red[200],
                                      hoverColor: Colors.red[200],
                                      padding: const EdgeInsets.all(0),
                                      minWidth: 34,
                                      onPressed: () {
                                        _multiTechnologyKey.currentState?.removeItem(item);
                                      },
                                      child: const Icon(Icons.close_outlined, size: 20),
                                    )
                                  ],
                                ),
                              ),
                            )
                            .toList());
                  },
                  filterFn: (TechnologiesList? designation, name) {
                    return designation!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                  },
                  popupItemBuilder: (_, text, isSelected) {
                    return Container(
                      padding: const EdgeInsets.all(10),
                      child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                    );
                  },
                  popupSelectionWidget: (cnt, item, bool isSelected) {
                    return Checkbox(
                        activeColor: buttonBg,
                        hoverColor: buttonBg.withOpacity(0.2),
                        value: isSelected,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                        side: const BorderSide(color: buttonBg),
                        onChanged: (value) {});
                  }),
            ),
          )
        ],
      ),
    );
  }

  Widget uploadResumeField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text("Upload Resume", style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                maxLines: 1,
                controller: resumeUploadController,
                textInputAction: TextInputAction.done,
                obscureText: false,
                decoration: InputDecoration(
                  fillColor: CupertinoColors.white,
                  filled: true,
                  border: border,
                  isDense: true,
                  enabledBorder: border,
                  focusedBorder: border,
                  errorBorder: errorBorder,
                  focusedErrorBorder: errorBorder,
                  contentPadding: editTextPadding,
                  hintStyle: textFieldHintStyle,
                  hintText: "Upload resume (pdf or doc)",
                  suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                  suffixIcon: Padding(child: Image.asset("assets/images/upload.png", color: buttonBg), padding: dateUploadIconsPadding),
                ),
                style: textFieldStyle,
                onTap: () {
                  _uploadResume();
                },
                enableInteractiveSelection: false,
                focusNode: FocusNode(),
                readOnly: true, /*validator: widget.profileId == null ? emptyTextValidator : null*/
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget assignEmployeeField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text("Monitor", style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: DropdownSearch<EmployeeDetails>.multiSelection(
                  key: _multiKey,
                  mode: Mode.MENU,
                  showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
                  compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                  items: employeesList,
                  showClearButton: true,
                  onChanged: (data) {
                    selectedEmployees = data;
                  },
                  maxHeight: 400,
                  clearButtonSplashRadius: 10,
                  selectedItems: selectedEmployees,
                  showSearchBox: true,
                  dropdownSearchDecoration: editTextEmployeesDecoration,
                  // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                  dropdownBuilder: (context, selectedItems) {
                    return Wrap(
                        children: selectedItems
                            .map(
                              (e) => selectedItem(e.name!),
                            )
                            .toList());
                  },
                  filterFn: (EmployeeDetails? employee, name) {
                    return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                  },
                  popupItemBuilder: (_, text, isSelected) {
                    return Container(
                      padding: const EdgeInsets.all(10),
                      child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                    );
                  },
                  popupSelectionWidget: (cnt, item, bool isSelected) {
                    return Checkbox(
                        activeColor: buttonBg,
                        hoverColor: buttonBg.withOpacity(0.2),
                        value: isSelected,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                        side: const BorderSide(color: buttonBg),
                        onChanged: (value) {});
                  }),
            ),
          )
        ],
      ),
    );
  }

  Widget experienceField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(experience, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          experienceRadioValue = 0;
                          experienceStatus = "fresher";
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Radio<int>(
                              value: 0,
                              groupValue: experienceRadioValue,
                              onChanged: (value) {
                                setState(() {
                                  experienceRadioValue = value!;
                                  experienceStatus = "fresher";
                                });
                              }),
                          const Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Text("Fresher"),
                          )
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          experienceRadioValue = 1;
                          experienceStatus = "experienced";
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Radio<int>(
                              value: 1,
                              groupValue: experienceRadioValue,
                              onChanged: (value) {
                                setState(() {
                                  experienceRadioValue = value!;
                                  experienceStatus = "experienced";
                                });
                              }),
                          const Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Text("Experienced"),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget careerStartField() {
    return Expanded(
      child: experienceRadioValue == 1
          ? Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
                    child: SizedBox(
                      width: 150,
                      child: Text("Career start date", style: addRoleSubStyle),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                            maxLines: 1,
                            controller: careerStartDateController,
                            textInputAction: TextInputAction.done,
                            obscureText: false,
                            decoration: InputDecoration(
                              fillColor: CupertinoColors.white,
                              filled: true,
                              border: border,
                              isDense: true,
                              enabledBorder: border,
                              focusedBorder: border,
                              errorBorder: errorBorder,
                              focusedErrorBorder: errorBorder,
                              contentPadding: editTextPadding,
                              hintStyle: textFieldHintStyle,
                              hintText: 'DD / MM / YYYY',
                              suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                              suffixIcon: Padding(child: Image.asset("images/calendar.png", color: textFieldBorder), padding: dateUploadIconsPadding),
                            ),
                            style: textFieldStyle,
                            onTap: () {
                              showCalendar();
                            },
                            enableInteractiveSelection: false,
                            focusNode: FocusNode(),
                            readOnly: true,
                            validator: emptyTextValidator),
                        exp == null
                            ? const SizedBox()
                            : Padding(
                                padding: const EdgeInsets.only(left: 10, top: 5),
                                child: Text("${exp!.years.toString()} years ${double.parse(exp!.months.toString())} months"),
                              )
                      ],
                    ),
                  ),
                ),
              ],
            )
          : const SizedBox(),
    );
  }

  Widget statusField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text('$profile $status', style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: DropdownButtonFormField<ProfilesStatusList>(
                items: profileStatusesMenuItems,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border,
                    isDense: true,
                    enabledBorder: border,
                    focusedBorder: border,
                    errorBorder: errorBorder,
                    focusedErrorBorder: errorBorder,
                    contentPadding: editTextPadding,
                    hintStyle: textFieldHintStyle,
                    hintText: status),
                style: textFieldStyle,
                value: profileStatusInitialValue,
                validator: profileStatusDropDownValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (ProfilesStatusList? value) {
                  setState(() {
                    profileStatusInitialValue = value!;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget jobRoleField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text('$jobRole', style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: DropdownButtonFormField<JobRolesList>(
                items: jobRoleMenuItems,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border,
                    isDense: true,
                    enabledBorder: border,
                    focusedBorder: border,
                    errorBorder: errorBorder,
                    focusedErrorBorder: errorBorder,
                    contentPadding: editTextPadding,
                    hintStyle: textFieldHintStyle,
                    hintText: jobRole),
                style: textFieldStyle,
                value: jobRoleInitialValue,
                validator: jobRoleDropDownValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (JobRolesList? value) {
                  setState(() {
                    jobRoleInitialValue = value!;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget mainTechnologyField() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(technology, style: addRoleSubStyle),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: DropdownButtonFormField<TechnologiesList>(
                items: technologiesMenuItems,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border,
                    isDense: true,
                    enabledBorder: border,
                    focusedBorder: border,
                    errorBorder: errorBorder,
                    focusedErrorBorder: errorBorder,
                    contentPadding: editTextPadding,
                    hintStyle: textFieldHintStyle,
                    hintText: status),
                style: textFieldStyle,
                value: technologyInitialValue,
                validator: technologiesDropDownValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (TechnologiesList? value) {
                  setState(() {
                    technologyInitialValue = value!;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  careerStartDateFresher() {
    var outputFormatServer = DateFormat('MM-dd-yyyy');
    var outputDateServer = outputFormatServer.format(DateTime.now());
    setState(() {
      careerStartDate = outputDateServer;
    });
  }

  Future<void> showCalendar() async {
    final DateTime? picked = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(1950), lastDate: DateTime.now());
    if (picked != null) {
      setState(() {
        selectedDate = picked;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputDate = inputFormat.parse(selectedDate.toLocal().toString().split(' ')[0]); // removing time from date
        var outputFormat = DateFormat('dd/MM/yyyy');
        var outputDate = outputFormat.format(inputDate);
        var outputFormatServer = DateFormat('MM-dd-yyyy');
        var outputDateServer = outputFormatServer.format(inputDate);
        careerStartDate = outputDateServer;
        exp = Age.dateDifference(fromDate: selectedDate, toDate: DateTime.now(), includeToDate: false);
        careerStartDateController.text = outputDate;
      });
    }
  }

  Future<void> _uploadResume() async {
    try {
      await FilePicker.platform
          .pickFiles(
              type: FileType.custom,
              allowedExtensions: ['pdf', 'doc', 'docx', 'docs'],
              allowMultiple: false,
              onFileLoading: (FilePickerStatus status) => {
                    if (status == FilePickerStatus.picking)
                      {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            })
                      }
                    else if (status == FilePickerStatus.done)
                      {Navigator.of(context).pop()}
                    else
                      {Navigator.of(context).pop()}
                  })
          .then((value) {
        if (value != null) {
          final path = value.files.single.bytes;
          setState(() {
            String fileName = value.files.first.name;
            String ext = value.files.first.extension.toString();
            // if (fileName.contains(".pdf") || fileName.contains(".doc") || fileName.contains(".docx") || fileName.contains(".docs")) {
            if (ext == "pdf" || ext == "doc" || ext == "docx" || ext == "docs") {
              Uint8List? fileBytes = path;
              String base64string = base64Encode(fileBytes!);
              uploadFile = base64string;
              fileExtension = ext;
              resumeUploadController.text = fileName;
              showToast("Resume uploaded successfully!");
            } else {
              showToast("Upload resume in pdf or doc formats!");
            }
          });
        } else {
          Navigator.of(context).pop();
          showToast("Error uploading file, please try again.");
        }
      });
    } on PlatformException catch (e) {
      showToast("Unsupported platform, /'${e.toString()}./'");
    } catch (e) {
      showToast("Error uploading file, /'${e.toString()}/', please try again.");
    }
  }
}
