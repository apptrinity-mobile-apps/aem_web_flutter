import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_interview_status_response_model.dart';
import 'package:aem/model/all_interview_type_response_model.dart';
import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/model/all_profiles_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/job_roles_list_response_mode.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/interview_status/view_model/interview_status_notifier.dart';
import 'package:aem/screens/interview_types/view_model/interview_types_notifier.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/screens/profiles/ui/add_profile_fragment.dart';
import 'package:aem/screens/profiles/ui/view_profile_fragment.dart';
import 'package:aem/screens/profiles/view_model/profile_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/app_scroll_behaviour.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../widgets/multi_select_selected_item_widget.dart';

class ProfilesFragment extends StatefulWidget {
  const ProfilesFragment({Key? key}) : super(key: key);

  @override
  _ProfilesFragmentState createState() => _ProfilesFragmentState();
}

class _ProfilesFragmentState extends State<ProfilesFragment> {
  late ProfileNotifier viewModel;
  late InterviewTypeNotifier interviewTypeViewModel;
  late InterviewStatusNotifier interviewStatusViewModel;
  late ProfileStatusNotifier profileStatusViewModel;
  final _multiKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _jobRoleKey = GlobalKey<DropdownSearchState<JobRolesList>>();
  final _technologyKey = GlobalKey<DropdownSearchState<TechnologiesList>>();
  String employeeId = "", endDate = "", startDate = "";
  String displayActiveStatusId = "";
  List<String> employeeIds = [];
  JobRolesList jobRoleInitialValue = JobRolesList(role: selectjobRole, id: 'none');
  List<DropdownMenuItem<JobRolesList>> jobRoleMenuItems = [];
  bool editPermission = false,
      addPermission = false,
      viewPermission = false,
      assignPermission = false,
      fullAccessPermission = false,
      updateStatus = false,
      showReset = false,
      isLoading = false;
  late EmployeeNotifier employeeViewModel;
  TextEditingController nameController = TextEditingController();
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  ProfilesStatusList profileStatusInitialValue =
      ProfilesStatusList(statusName: selectStatus, id: 'none');
  List<DropdownMenuItem<ProfilesStatusList>> profileStatusesMenuItems = [];
  List<DisplayEnableProfilesStatusList> displayEnableProfilesStatusList = [];
  final _statusKey = GlobalKey<FormFieldState>();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  late TechnologyNotifier techViewModel = TechnologyNotifier();
  List<TechnologiesList> technologyList = [], selectedTechnologies = [];
  bool isVisible = true, hasTechnologies = false,hasJobRoles = false;
  List<String> technologyIds = [];
  List<ProfilesList> candidateProfilesList = [];
  final scrollController = ScrollController();
  int pageCount = 0;

  List<String> jobRoleIds = [];
  List<JobRolesList> jobRoleList = [], selectedJobRoles = [];

  @override
  void initState() {
    viewModel = Provider.of<ProfileNotifier>(context, listen: false);
    employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    interviewTypeViewModel =
        Provider.of<InterviewTypeNotifier>(context, listen: false);
    interviewStatusViewModel =
        Provider.of<InterviewStatusNotifier>(context, listen: false);
    profileStatusViewModel =
        Provider.of<ProfileStatusNotifier>(context, listen: false);
    employeesList.add(EmployeeDetails(name: selectEmployee));
    technologyList.add(TechnologiesList(name: selectTechnology1));
    jobRoleList.add(JobRolesList(role: selectjobRole));
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.profileEditPermission;
    addPermission = userViewModel.profileAddPermission;
    viewPermission = userViewModel.profileViewPermission;
    assignPermission = userViewModel.profileAssignPermission;
    fullAccessPermission = userViewModel.profileFullAccessPermission;
    updateStatus = userViewModel.profileUpdateStatusPermission;
    profileStatusesMenuItems = [];
    profileStatusesMenuItems.add(DropdownMenuItem(
        child: const Text(selectStatus), value: profileStatusInitialValue));
    getData();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        getAllCandidateProfiles(pageCount);
      }
    });
    super.initState();
  }

  void getData() {
    Future.delayed(Duration.zero, () async {
      getAllCandidateProfiles(pageCount);
      await employeeViewModel.getEmployeesAPI("hr").then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
      await techViewModel.getTechnologiesAPI().then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.technologyList!.isNotEmpty) {
                technologyList = value.technologyList!;
              }
            });
          }
        }
        hasTechnologies = true;
      });


      await viewModel.viewactivejobrolesApi(employeeId).then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.jobRolesList!.isNotEmpty) {
                jobRoleList = value.jobRolesList!;
              }
            });
          }
        }
        hasJobRoles = true;
      });
      await interviewTypeViewModel.getInterviewTypesAPI();
      await interviewStatusViewModel.getInterviewStatusesAPI();
      await profileStatusViewModel.getProfileStatusesAPI().then((value) {

        if(value!.responseStatus == 1){

          setState(() {
            if (value.displayEnableProfilesStatusList!.isNotEmpty) {
              displayEnableProfilesStatusList = value.displayEnableProfilesStatusList!;
            }
          });

        }



        print('display count ${displayEnableProfilesStatusList.length}');
        for (var element in profileStatusViewModel.getProfileStatuses!) {
          profileStatusesMenuItems.add(DropdownMenuItem(
              child: Text(element.statusNameWithCount!), value: element));
        }
      });
    });
  }

  Widget projectPlansList(int position,DisplayEnableProfilesStatusList itemdata) {
    var alternate = position % 2;
    return OnHoverCard(builder: (isHovered) {
      return InkWell(
        onTap: (){

          showReset = false;
          nameController.text = "";
          dateController.text = "";
          startDate = "";
          endDate = "";


          displayActiveStatusId = itemdata.id!;

          print('selected id ${displayActiveStatusId}');
          employeeIds = [];
          technologyIds = [];
          jobRoleIds = [];
          _statusKey.currentState!
              .reset();
          _technologyKey.currentState!
              .clear();
          _multiKey.currentState!.clear();
          candidateProfilesList = [];
          pageCount = 0;
          getAllCandidateProfiles(
              pageCount);
        },

        child: Card(

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: const BorderSide(color: totalProfilesCardColor, width: 1),
          ),
          color: alternate == 0 ? totalProfilesCardBgColor : todayInterviewsCardBgColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Wrap(
              runAlignment: WrapAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 0, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                       Text(
                         itemdata.statusName!,
                        softWrap: true,
                        style: TextStyle(
                            letterSpacing: 0.66,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            color: editText,
                            fontSize: 18),
                      ),
                      Text(
                        itemdata.profilesCount.toString(),
                        softWrap: true,
                        style: const TextStyle(
                            letterSpacing: 1.26,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w700,
                            color: editText,
                            fontSize: 24),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget activeStatusWidget() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: true);

    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          scrollBehavior: AppScrollBehavior(),
          home: GridView.builder(

              shrinkWrap: true,
              controller: ScrollController(),
              physics: const ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: displayEnableProfilesStatusList.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  childAspectRatio: 0.5,
                  mainAxisSpacing: 4,
                  crossAxisSpacing: 4,
                  maxCrossAxisExtent: 300),
              itemBuilder: (BuildContext context, int index) {
                return projectPlansList(index,displayEnableProfilesStatusList[index]);
              })),
    );
  }

  Future getAllCandidateProfiles(int pageNumber) async {
    /*if (nameController.text.trim() != "" && profileStatusInitialValue.id == 'none') {
      return viewModel.getAllProfilesAPI(employeeId, nameController.text.trim(), "",[],[]);
    } else if (nameController.text.trim() == "" && profileStatusInitialValue.id != 'none') {
      return viewModel.getAllProfilesAPI(employeeId, "", profileStatusInitialValue.id!,[],[]);
    } else if (nameController.text.trim() != "" && profileStatusInitialValue.id != 'none') {
      return viewModel.getAllProfilesAPI(employeeId, nameController.text.trim(), profileStatusInitialValue.id!);
    } else {
      return viewModel.getAllProfilesAPI(employeeId, "", "");
    }*/

    for (var element in selectedEmployees) {
      employeeIds.add(element.id!);
    }
    for (var element in selectedTechnologies) {
      technologyIds.add(element.id!);
    }

    for (var element in selectedJobRoles) {
      jobRoleIds.add(element.id!);
    }
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                loader(),
                pageCount != 0
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          profilesLoading,
                          style: newButtonsStyle,
                        ),
                      )
                    : const SizedBox()
              ],
            ),
          );
        });
    if (nameController.text.trim() == "" &&
        profileStatusInitialValue.id == 'none' &&
        employeeIds.isEmpty &&
        technologyIds.isEmpty && displayActiveStatusId == '' && jobRoleIds.isEmpty) {
      return await viewModel
          .getAllProfilesAPI(
              employeeId, "", "", [], [], pageNumber, startDate, endDate,[])
          .then((value) {
        Navigator.of(context).pop();
        setState(() {
          candidateProfilesList.addAll(viewModel.getAllProfiles!);
          pageCount++;
        });
      });
    } else if (profileStatusInitialValue.id != 'none') {
      return await viewModel
          .getAllProfilesAPI(
              employeeId,
              nameController.text.trim(),
              profileStatusInitialValue.id!,
              employeeIds,
              technologyIds,
              pageNumber,
              startDate,
              endDate,jobRoleIds)
          .then((value) {
        Navigator.of(context).pop();
        setState(() {
          candidateProfilesList.addAll(viewModel.getAllProfiles!);
          pageCount++;
        });
      });
    }else if (displayActiveStatusId != '') {
      return await viewModel
          .getAllProfilesAPI(
          employeeId,
          nameController.text.trim(),
          displayActiveStatusId,
          employeeIds,
          technologyIds,
          pageNumber,
          startDate,
          endDate,jobRoleIds)
          .then((value) {
        Navigator.of(context).pop();
        setState(() {
          candidateProfilesList.addAll(viewModel.getAllProfiles!);
          pageCount++;
          displayActiveStatusId = '';
          showReset = true;
        });
      });
    } else {
      return await viewModel
          .getAllProfilesAPI(employeeId, nameController.text.trim(), '',
              employeeIds, technologyIds, pageNumber, startDate, endDate,jobRoleIds)
          .then((value) {
        setState(() {
          Navigator.of(context).pop();

          candidateProfilesList.addAll(viewModel.getAllProfiles!);
          pageCount++;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$home > $navMenuProfiles",
              maxLines: 1,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: fragmentDescStyle),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                    displayEnableProfilesStatusList.length > 0 ? Container(height:100,child:activeStatusWidget() ) : SizedBox(),
                      Padding(
                        padding: const EdgeInsets.only(top: 15,),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 4,
                                child: Wrap(
                                  spacing: 5,
                                  runSpacing: 10,
                                  alignment: WrapAlignment.start,
                                  children: [
                                    nameField(),
                                    statusField(),
                                    technologyField(),
                                    assignEmployeeField(),

                                    dateField(),
                                    jobRoleField(),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Wrap(
                                  runSpacing: 5,
                                  spacing: 5,
                                  alignment: WrapAlignment.end,
                                  crossAxisAlignment: WrapCrossAlignment.start,
                                  runAlignment: WrapAlignment.start,
                                  children: [
                                    TextButton(
                                      onPressed: () {
                                        setState(() {
                                          showReset = true;
                                          employeeIds = [];
                                          technologyIds = [];
                                          jobRoleIds = [];
                                          candidateProfilesList = [];
                                          pageCount = 0;
                                          getAllCandidateProfiles(pageCount);
                                        });
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: Wrap(
                                          alignment: WrapAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          spacing: 10,
                                          children: [
                                            Image.asset(
                                              "assets/images/filter_2x.png",
                                              height: 15,
                                              width: 15,
                                              fit: BoxFit.fill,
                                            ),
                                            const Text(
                                              filter,
                                              softWrap: true,
                                              style: TextStyle(
                                                  letterSpacing: 0.9,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w600,
                                                  color: buttonBg,
                                                  fontSize: 14),
                                            ),
                                          ],
                                        ),
                                      ),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(filterBg),
                                        overlayColor: MaterialStateProperty.all(
                                            buttonBg.withOpacity(0.1)),
                                        side: MaterialStateProperty.all(
                                            const BorderSide(color: filterBg)),
                                        elevation: MaterialStateProperty.all(1),
                                      ),
                                    ),
                                    showReset
                                        ? TextButton(
                                            onPressed: () {
                                              setState(() {
                                                // resetting filter
                                                showReset = false;
                                                nameController.text = "";
                                                dateController.text = "";
                                                startDate = "";
                                                endDate = "";
                                                profileStatusInitialValue =
                                                    ProfilesStatusList(
                                                        statusName:
                                                            selectStatus,
                                                        id: 'none');
                                                employeeIds = [];
                                                technologyIds = [];
                                                jobRoleIds = [];
                                                _statusKey.currentState!
                                                    .reset();
                                                _technologyKey.currentState!
                                                    .clear();
                                                _multiKey.currentState!.clear();
                                                candidateProfilesList = [];
                                                pageCount = 0;
                                                _jobRoleKey.currentState!.clear();
                                                getAllCandidateProfiles(
                                                    pageCount);
                                              });
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.all(8),
                                              child: Wrap(
                                                alignment:
                                                    WrapAlignment.spaceBetween,
                                                crossAxisAlignment:
                                                    WrapCrossAlignment.center,
                                                spacing: 10,
                                                children: [
                                                  const Text(
                                                    reset,
                                                    softWrap: true,
                                                    style: TextStyle(
                                                        letterSpacing: 0.9,
                                                        fontFamily: "Poppins",
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            dashBoardCardOverDueText,
                                                        fontSize: 14),
                                                  ),
                                                  Image.asset(
                                                    "assets/images/cancel_red_2x.png",
                                                    height: 15,
                                                    width: 15,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.all(
                                                      resetBg),
                                              overlayColor:
                                                  MaterialStateProperty.all(
                                                      dashBoardCardOverDueText
                                                          .withOpacity(0.1)),
                                              side: MaterialStateProperty.all(
                                                  const BorderSide(
                                                      color: resetBg)),
                                              elevation:
                                                  MaterialStateProperty.all(1),
                                            ),
                                          )
                                        : const SizedBox()
                                  ],
                                ),
                              )
                            ]),
                      ),
                      Flexible(child: allProfilesList()),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuProfiles,
                  maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionProfile,
                    softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: buttonDecorationGreen,
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: TextButton(
                    onPressed: () async {
                      if (addPermission) {
                        Navigator.push(
                                context,
                                PageRouteBuilder(
                                    pageBuilder:
                                        (context, animation1, animation2) =>
                                            const AddProfileFragment(),
                                    transitionDuration:
                                        const Duration(seconds: 0),
                                    reverseTransitionDuration:
                                        const Duration(seconds: 0)))
                            .then((value) {
                          setState(() {
                            candidateProfilesList = [];
                            pageCount = 0;
                            getAllCandidateProfiles(pageCount);
                          });
                        });
                      } else {
                        showToast(noPermissionToAccess);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(addProfile, style: newButtonsStyle)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget allProfilesList() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        child: Theme(
          data: Theme.of(context)
              .copyWith(dividerColor: buttonBg.withOpacity(0.2)),
          child: DataTable2(
            columnSpacing: 14,
            dividerThickness: 1,
            scrollController: scrollController,
            decoration: BoxDecoration(
                border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
            headingRowColor:
                MaterialStateColor.resolveWith((states) => buttonBg),
            headingRowHeight: 46,
            smRatio: 0.4,
            lmRatio: 1.5,
            columns: <DataColumn2>[
              DataColumn2(
                  label: Text(tableSNo,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.S),
              DataColumn2(
                  label: Text(tableName,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Text(tableEmail,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.L),
              DataColumn2(
                  label: Text(jobRole,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Text(technology,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Text(tableCreatedBy,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Text(tableStatus,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Text(tableRounds,
                      softWrap: true,
                      style: listHeaderStyle,
                      textAlign: TextAlign.start),
                  size: ColumnSize.M),
              DataColumn2(
                  label: Center(
                      child: Text(tableActions,
                          softWrap: true,
                          style: listHeaderStyle,
                          textAlign: TextAlign.start)),
                  size: ColumnSize.L),
            ],
            rows: listItems(candidateProfilesList),
            empty: Center(
              child: Text(noDataAvailable, style: logoutHeader),
            ),
          ),
        ),
      ),
    );
  }

  List<DataRow> listItems(List<ProfilesList?> profilesList) {
    return profilesList.map<DataRow2>((e) {
      return DataRow2(
          color:
              MaterialStateColor.resolveWith((states) => CupertinoColors.white),
          specificRowHeight: 66,
          onTap: () {
            Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (context, animation1, animation2) =>
                            ViewProfileFragment(profileId: e!.id),
                        transitionDuration: const Duration(seconds: 0),
                        reverseTransitionDuration: const Duration(seconds: 0)))
                .then((value) {
              setState(() {
                candidateProfilesList = [];
                pageCount = 0;
                getAllCandidateProfiles(pageCount);
              });
            });
          },
          cells: [
            DataCell(
              Text((profilesList.indexOf(e) + 1).toString(),
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e!.name!.inCaps,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text("${e.email!}\n${e.phoneNumber!}",
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text("${e.jobRole!}",
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.mainTechnology!,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text("${e.createdByName!}\n${e.createdOn!}",
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.employmentStatusName!,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.interviewTypeName!,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
            DataCell(
              Center(
                child: Wrap(
                  alignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 4,
                  spacing: 4,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0),
                      child: Tooltip(
                        message: view,
                        preferBelow: false,
                        decoration: toolTipDecoration,
                        textStyle: toolTipStyle,
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Material(
                          elevation: 0.0,
                          shape: const CircleBorder(),
                          clipBehavior: Clip.hardEdge,
                          color: Colors.transparent,
                          child: Ink.image(
                            image: const AssetImage(
                                'assets/images/view_rounded.png'),
                            fit: BoxFit.cover,
                            width: 28,
                            height: 28,
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            pageBuilder: (context, animation1,
                                                    animation2) =>
                                                ViewProfileFragment(
                                                    profileId: e.id),
                                            transitionDuration:
                                                const Duration(seconds: 0),
                                            reverseTransitionDuration:
                                                const Duration(seconds: 0)))
                                    .then((value) {
                                  setState(() {
                                    candidateProfilesList = [];
                                    pageCount = 0;
                                    getAllCandidateProfiles(pageCount);
                                  });
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    fullAccessPermission
                        ? Padding(
                            padding: const EdgeInsets.all(0),
                            child: Tooltip(
                              message: assign,
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Ink.image(
                                  image: const AssetImage(
                                      'assets/images/assign_rounded.png'),
                                  fit: BoxFit.cover,
                                  width: 28,
                                  height: 28,
                                  child: InkWell(
                                    onTap: () {
                                      // showAssignDialog(e);
                                      showScheduleInterviewDialog(e);
                                    },
                                  ),
                                ),
                              ),
                            ),
                          )
                        : assignPermission
                            ? Padding(
                                padding: const EdgeInsets.all(0),
                                child: Tooltip(
                                  message: assign,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.hardEdge,
                                    color: Colors.transparent,
                                    child: Ink.image(
                                      image: const AssetImage(
                                          'assets/images/assign_rounded.png'),
                                      fit: BoxFit.cover,
                                      width: 28,
                                      height: 28,
                                      child: InkWell(
                                        onTap: () {
                                          // showAssignDialog(e);
                                          showScheduleInterviewDialog(e);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                    fullAccessPermission
                        ? Padding(
                            padding: const EdgeInsets.all(0),
                            child: Tooltip(
                              message: '$status $update',
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Ink.image(
                                  image: const AssetImage(
                                      'assets/images/status_update_rounded.png'),
                                  fit: BoxFit.cover,
                                  width: 28,
                                  height: 28,
                                  child: InkWell(
                                    onTap: () {
                                      showStatusUpdateDialog(e);
                                    },
                                  ),
                                ),
                              ),
                            ),
                          )
                        : updateStatus
                            ? Padding(
                                padding: const EdgeInsets.all(0),
                                child: Tooltip(
                                  message: '$status $update',
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.hardEdge,
                                    color: Colors.transparent,
                                    child: Ink.image(
                                      image: const AssetImage(
                                          'assets/images/status_update_rounded.png'),
                                      fit: BoxFit.cover,
                                      width: 28,
                                      height: 28,
                                      child: InkWell(
                                        onTap: () {
                                          showStatusUpdateDialog(e);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                    fullAccessPermission
                        ? Padding(
                            padding: const EdgeInsets.all(0),
                            child: Tooltip(
                              message: edit,
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Ink.image(
                                  image: const AssetImage(
                                      'assets/images/edit_rounded.png'),
                                  fit: BoxFit.cover,
                                  width: 28,
                                  height: 28,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                              context,
                                              PageRouteBuilder(
                                                  pageBuilder: (context,
                                                          animation1,
                                                          animation2) =>
                                                      AddProfileFragment(
                                                          profileId: e.id!),
                                                  transitionDuration:
                                                      const Duration(
                                                          seconds: 0),
                                                  reverseTransitionDuration:
                                                      const Duration(
                                                          seconds: 0)))
                                          .then((value) {
                                        setState(() {
                                          candidateProfilesList = [];
                                          pageCount = 0;
                                          getAllCandidateProfiles(pageCount);
                                        });
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ),
                          )
                        : editPermission
                            ? Padding(
                                padding: const EdgeInsets.all(0),
                                child: Tooltip(
                                  message: edit,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.hardEdge,
                                    color: Colors.transparent,
                                    child: Ink.image(
                                      image: const AssetImage(
                                          'assets/images/edit_rounded.png'),
                                      fit: BoxFit.cover,
                                      width: 28,
                                      height: 28,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                      pageBuilder: (context,
                                                              animation1,
                                                              animation2) =>
                                                          AddProfileFragment(
                                                              profileId: e.id!),
                                                      transitionDuration:
                                                          const Duration(
                                                              seconds: 0),
                                                      reverseTransitionDuration:
                                                          const Duration(
                                                              seconds: 0)))
                                              .then((value) {
                                            setState(() {
                                              candidateProfilesList = [];
                                              pageCount = 0;
                                              getAllCandidateProfiles(
                                                  pageCount);
                                            });
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                    fullAccessPermission
                        ? Padding(
                            padding: const EdgeInsets.all(0),
                            child: Tooltip(
                              message: delete,
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Ink.image(
                                  image: const AssetImage(
                                      'assets/images/delete_rounded.png'),
                                  fit: BoxFit.cover,
                                  width: 28,
                                  height: 28,
                                  child: InkWell(
                                    onTap: () {
                                      showDeleteDialog(e);
                                    },
                                  ),
                                ),
                              ),
                            ),
                          )
                        : editPermission
                            ? Padding(
                                padding: const EdgeInsets.all(0),
                                child: Tooltip(
                                  message: delete,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.hardEdge,
                                    color: Colors.transparent,
                                    child: Ink.image(
                                      image: const AssetImage(
                                          'assets/images/delete_rounded.png'),
                                      fit: BoxFit.cover,
                                      width: 28,
                                      height: 28,
                                      child: InkWell(
                                        onTap: () {
                                          showDeleteDialog(e);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                  ],
                ),
              ),
            ),
          ]);
    }).toList();
  }

  Widget nameField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 230),
      child: TextFormField(
          maxLines: 1,
          controller: nameController,
          textInputAction: TextInputAction.done,
          textCapitalization: TextCapitalization.sentences,
          obscureText: false,
          decoration: editTextNameDecoration.copyWith(
            labelText: name,
            contentPadding: const EdgeInsets.fromLTRB(10, 13, 10, 13),
          ),
          style: textFieldStyle),
    );
  }

  Widget statusField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 230),
      child: DropdownButtonFormField<ProfilesStatusList>(
        key: _statusKey,
        items: profileStatusesMenuItems,
        isExpanded: true,
        decoration: InputDecoration(
            fillColor: CupertinoColors.white,
            filled: true,
            border: border,
            isDense: true,
            enabledBorder: border,
            focusedBorder: border,
            errorBorder: errorBorder,
            focusedErrorBorder: errorBorder,
            contentPadding: const EdgeInsets.fromLTRB(10, 13, 10, 13),
            hintStyle: textFieldHintStyle,
            hintText: status,
            labelText: status),
        style: textFieldStyle,
        validator: profileStatusDropDownValidator,
        onChanged: (ProfilesStatusList? value) {
          setState(() {
            profileStatusInitialValue = value!;
          });
        },
      ),
    );
  }

  Widget technologyField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 230),
      child: DropdownSearch<TechnologiesList>.multiSelection(
        key: _technologyKey,
        mode: Mode.MENU,
        showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
        items: technologyList,
        showClearButton: true,
        onChanged: (data) {
          selectedTechnologies = data;
        },
        clearButtonSplashRadius: 10,
        selectedItems: selectedTechnologies,
        showSearchBox: true,
        dropdownSearchDecoration:
            editTextEmployeesDecoration.copyWith(labelText: technology),
        validator: (list) =>
            list == null || list.isEmpty ? emptyFieldError : null,
        dropdownBuilder: (context, selectedItems) {
          return Wrap(
              children: selectedItems
                  .map(
                    (e) => selectedItem(e.name!),
                  )
                  .toList());
        },
        filterFn: (TechnologiesList? employee, name) {
          return employee!.name!.toLowerCase().contains(name!.toLowerCase())
              ? true
              : false;
        },
        popupItemBuilder: (_, text, isSelected) {
          return Container(
            padding: const EdgeInsets.all(10),
            child: Text(text.name!,
                style: isSelected ? dropDownSelected : dropDown),
          );
        },
        popupSelectionWidget: (cnt, item, bool isSelected) {
          return Checkbox(
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: isSelected,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) {});
        },
        onPopupDismissed: () {},
      ),
    );
  }

  Widget assignEmployeeField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 230),
      child: DropdownSearch<EmployeeDetails>.multiSelection(
        key: _multiKey,
        mode: Mode.MENU,
        showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
        items: employeesList,
        showClearButton: true,
        onChanged: (data) {
          selectedEmployees = data;
        },
        clearButtonSplashRadius: 10,
        selectedItems: selectedEmployees,
        showSearchBox: true,
        dropdownSearchDecoration:
            editTextEmployeesDecoration.copyWith(labelText: employee),
        validator: (list) =>
            list == null || list.isEmpty ? emptyFieldError : null,
        dropdownBuilder: (context, selectedItems) {
          return Wrap(
              children: selectedItems
                  .map(
                    (e) => selectedItem(e.name!),
                  )
                  .toList());
        },
        filterFn: (EmployeeDetails? employee, name) {
          return employee!.name!.toLowerCase().contains(name!.toLowerCase())
              ? true
              : false;
        },
        popupItemBuilder: (_, text, isSelected) {
          return Container(
            padding: const EdgeInsets.all(10),
            child: Text(text.name!,
                style: isSelected ? dropDownSelected : dropDown),
          );
        },
        popupSelectionWidget: (cnt, item, bool isSelected) {
          return Checkbox(
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: isSelected,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) {});
        },
        onPopupDismissed: () {},
      ),
    );
  }

  Widget jobRoleField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 230),
      child: DropdownSearch<JobRolesList>.multiSelection(
        key: _jobRoleKey,
        mode: Mode.MENU,
        showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
        items: jobRoleList,
        showClearButton: true,
        onChanged: (data) {
          selectedJobRoles = data;
        },
        clearButtonSplashRadius: 10,
        selectedItems: selectedJobRoles,
        showSearchBox: true,
        dropdownSearchDecoration:
        editTextEmployeesDecoration.copyWith(labelText: jobRole),
        validator: (list) =>
        list == null || list.isEmpty ? emptyFieldError : null,
        dropdownBuilder: (context, selectedItems) {
          return Wrap(
              children: selectedItems
                  .map(
                    (e) => selectedItem(e.role!),
              )
                  .toList());
        },
        filterFn: (JobRolesList? employee, name) {
          return employee!.role!.toLowerCase().contains(name!.toLowerCase())
              ? true
              : false;
        },
        popupItemBuilder: (_, text, isSelected) {
          return Container(
            padding: const EdgeInsets.all(10),
            child: Text(text.role!,
                style: isSelected ? dropDownSelected : dropDown),
          );
        },
        popupSelectionWidget: (cnt, item, bool isSelected) {
          return Checkbox(
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: isSelected,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) {});
        },
        onPopupDismissed: () {},
      ),
    );
  }


  Widget dateField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 250),
      child: TextFormField(
        controller: dateController,
        maxLines: 1,
        textInputAction: TextInputAction.done,
        textCapitalization: TextCapitalization.sentences,
        obscureText: false,
        decoration: InputDecoration(
          fillColor: CupertinoColors.white,
          filled: true,
          border: border,
          isDense: true,
          enabledBorder: border,
          focusedBorder: border,
          errorBorder: errorBorder,
          focusedErrorBorder: errorBorder,
          contentPadding: const EdgeInsets.fromLTRB(10, 13, 0, 13),
          hintStyle: textFieldHintStyle,
          hintText: selectDate,
          suffixIconConstraints:
              const BoxConstraints(maxHeight: 40, maxWidth: 40),
          suffixIcon: Padding(
              child: Image.asset("images/calendar.png", color: textFieldBorder),
              padding: const EdgeInsets.fromLTRB(5, 5, 10, 5)),
        ),
        style: textFieldStyle,
        readOnly: true,
        enabled: true,
        onTap: () async {
          final DateTimeRange? result = await showDateRangePicker(
              context: context,
              firstDate: DateTime(1900),
              lastDate: DateTime(2100),
              builder: (context, child) {
                return Center(
                  child: ConstrainedBox(
                    constraints:
                        const BoxConstraints(maxWidth: 400, maxHeight: 500),
                    child: child,
                  ),
                );
              });
          setState(() {
            if (result != null) {
              DateTime _startDate = result.start;
              DateTime _endDate = result.end;
              var inputFormat = DateFormat('yyyy-MM-dd');
              var inputStartDate = inputFormat.parse(_startDate
                  .toLocal()
                  .toString()
                  .split(' ')[0]); // removing time from date
              var inputEndDate = inputFormat.parse(_endDate
                  .toLocal()
                  .toString()
                  .split(' ')[0]); // removing time from date
              startDate = DateFormat('dd/MM/yyyy').format(inputStartDate);
              endDate = DateFormat('dd/MM/yyyy').format(inputEndDate);
              dateController.text = "$startDate - $endDate";
            }
          });
        },
      ),
    );
  }

  showFilterDialog() {
    showDialog(
        context: context,
        builder: (context) {
          final _formKey = GlobalKey<FormState>();
          final _technologyKey =
              GlobalKey<DropdownSearchState<TechnologiesList>>();
          final _employeesKey =
              GlobalKey<DropdownSearchState<EmployeeDetails>>();
          TextEditingController _nameController = TextEditingController();
          TextEditingController _fromDateController = TextEditingController();
          TextEditingController _toDateController = TextEditingController();
          List<EmployeeDetails> _employeesList = [], _selectedEmployees = [];
          List<TechnologiesList> _technologyList = [],
              _selectedTechnologies = [];
          _employeesList = employeesList;
          _technologyList = technologyList;
          return StatefulBuilder(builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(filter, style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded,
                          color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              content: Container(
                width: ScreenConfig.width(context) / 2,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(
                    border:
                        Border.all(color: dashBoardCardBorderTile, width: 1),
                    color: addNewContainerBg),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: SingleChildScrollView(
                            controller: ScrollController(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(15, 15, 15, 15),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 30, 0),
                                              child: Text(search,
                                                  style: addRoleSubStyle),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              child: TextFormField(
                                                  maxLines: 1,
                                                  controller: _nameController,
                                                  textInputAction:
                                                      TextInputAction.done,
                                                  textCapitalization:
                                                      TextCapitalization
                                                          .sentences,
                                                  obscureText: false,
                                                  decoration:
                                                      editTextNameDecoration
                                                          .copyWith(),
                                                  style: textFieldStyle),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 30, 0),
                                              child: Text(tableCreatedBy,
                                                  style: addRoleSubStyle),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                                key: _employeesKey,
                                                mode: Mode.MENU,
                                                showSelectedItems:
                                                    _selectedEmployees
                                                            .isNotEmpty
                                                        ? true
                                                        : false,
                                                compareFn:
                                                    (item, selectedItem) =>
                                                        item?.id ==
                                                        selectedItem?.id,
                                                items: _employeesList,
                                                showClearButton: true,
                                                onChanged: (data) {
                                                  _selectedEmployees = data;
                                                },
                                                clearButtonSplashRadius: 10,
                                                selectedItems:
                                                    _selectedEmployees,
                                                showSearchBox: true,
                                                dropdownSearchDecoration:
                                                    editTextEmployeesDecoration
                                                        .copyWith(),
                                                validator: (list) =>
                                                    list == null || list.isEmpty
                                                        ? emptyFieldError
                                                        : null,
                                                dropdownBuilder:
                                                    (context, selectedItems) {
                                                  return Wrap(
                                                      children: selectedItems
                                                          .map((e) =>
                                                              selectedItem(
                                                                  e.name!))
                                                          .toList());
                                                },
                                                filterFn:
                                                    (EmployeeDetails? employee,
                                                        name) {
                                                  return employee!.name!
                                                          .toLowerCase()
                                                          .contains(name!
                                                              .toLowerCase())
                                                      ? true
                                                      : false;
                                                },
                                                popupItemBuilder:
                                                    (_, text, isSelected) {
                                                  return Container(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10),
                                                    child: Text(text.name!,
                                                        style: isSelected
                                                            ? dropDownSelected
                                                            : dropDown),
                                                  );
                                                },
                                                popupSelectionWidget: (cnt,
                                                    item, bool isSelected) {
                                                  return Checkbox(
                                                      activeColor: buttonBg,
                                                      hoverColor: buttonBg
                                                          .withOpacity(0.2),
                                                      value: isSelected,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4)),
                                                      side: const BorderSide(
                                                          color: buttonBg),
                                                      onChanged: (value) {});
                                                },
                                                onPopupDismissed: () {},
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 30, 0),
                                              child: Text(technology,
                                                  style: addRoleSubStyle),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              child: DropdownSearch<
                                                  TechnologiesList>.multiSelection(
                                                key: _technologyKey,
                                                mode: Mode.MENU,
                                                showSelectedItems:
                                                    _selectedTechnologies
                                                            .isNotEmpty
                                                        ? true
                                                        : false,
                                                compareFn:
                                                    (item, selectedItem) =>
                                                        item?.id ==
                                                        selectedItem?.id,
                                                items: _technologyList,
                                                showClearButton: true,
                                                onChanged: (data) {
                                                  _selectedTechnologies = data;
                                                },
                                                clearButtonSplashRadius: 10,
                                                selectedItems:
                                                    _selectedTechnologies,
                                                showSearchBox: true,
                                                dropdownSearchDecoration:
                                                    editTextEmployeesDecoration
                                                        .copyWith(),
                                                validator: (list) =>
                                                    list == null || list.isEmpty
                                                        ? emptyFieldError
                                                        : null,
                                                dropdownBuilder:
                                                    (context, selectedItems) {
                                                  return Wrap(
                                                      children: selectedItems
                                                          .map((e) =>
                                                              selectedItem(
                                                                  e.name!))
                                                          .toList());
                                                },
                                                filterFn:
                                                    (TechnologiesList? employee,
                                                        name) {
                                                  return employee!.name!
                                                          .toLowerCase()
                                                          .contains(name!
                                                              .toLowerCase())
                                                      ? true
                                                      : false;
                                                },
                                                popupItemBuilder:
                                                    (_, text, isSelected) {
                                                  return Container(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10),
                                                    child: Text(text.name!,
                                                        style: isSelected
                                                            ? dropDownSelected
                                                            : dropDown),
                                                  );
                                                },
                                                popupSelectionWidget: (cnt,
                                                    item, bool isSelected) {
                                                  return Checkbox(
                                                      activeColor: buttonBg,
                                                      hoverColor: buttonBg
                                                          .withOpacity(0.2),
                                                      value: isSelected,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4)),
                                                      side: const BorderSide(
                                                          color: buttonBg),
                                                      onChanged: (value) {});
                                                },
                                                onPopupDismissed: () {},
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 30, 0),
                                              child: Text("From Date",
                                                  style: addRoleSubStyle),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              child: TextFormField(
                                                controller: _fromDateController,
                                                maxLines: 1,
                                                textInputAction:
                                                    TextInputAction.done,
                                                textCapitalization:
                                                    TextCapitalization
                                                        .sentences,
                                                obscureText: false,
                                                validator: emptyTextValidator,
                                                decoration: InputDecoration(
                                                  fillColor:
                                                      CupertinoColors.white,
                                                  filled: true,
                                                  border: border,
                                                  isDense: true,
                                                  enabledBorder: border,
                                                  focusedBorder: border,
                                                  errorBorder: errorBorder,
                                                  focusedErrorBorder:
                                                      errorBorder,
                                                  contentPadding:
                                                      editTextPadding,
                                                  hintStyle: textFieldHintStyle,
                                                  hintText: selectDate,
                                                  suffixIconConstraints:
                                                      const BoxConstraints(
                                                          maxHeight: 40,
                                                          maxWidth: 40),
                                                  suffixIcon: Padding(
                                                      child: Image.asset(
                                                          "images/calendar.png",
                                                          color:
                                                              textFieldBorder),
                                                      padding:
                                                          dateUploadIconsPadding),
                                                ),
                                                style: textFieldStyle,
                                                readOnly: true,
                                                enabled: true,
                                                onTap: () async {
                                                  final DateTime? picked =
                                                      await showDatePicker(
                                                          context: context,
                                                          initialDate:
                                                              DateTime.now(),
                                                          firstDate:
                                                              DateTime(1900),
                                                          lastDate:
                                                              DateTime(2100));
                                                  if (picked != null) {
                                                    assignState(() {
                                                      var inputFormat =
                                                          DateFormat(
                                                              'yyyy-MM-dd');
                                                      var inputDate = inputFormat
                                                          .parse(picked
                                                                  .toLocal()
                                                                  .toString()
                                                                  .split(' ')[
                                                              0]); // removing time from date
                                                      var outputFormat =
                                                          DateFormat(
                                                              'dd/MM/yyyy');
                                                      var outputDate =
                                                          outputFormat.format(
                                                              inputDate);
                                                      _fromDateController.text =
                                                          outputDate;
                                                    });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 30, 0),
                                              child: Text("To Date",
                                                  style: addRoleSubStyle),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 0, 10, 0),
                                              child: TextFormField(
                                                controller: _toDateController,
                                                maxLines: 1,
                                                textInputAction:
                                                    TextInputAction.done,
                                                textCapitalization:
                                                    TextCapitalization
                                                        .sentences,
                                                obscureText: false,
                                                validator: emptyTextValidator,
                                                decoration: InputDecoration(
                                                  fillColor:
                                                      CupertinoColors.white,
                                                  filled: true,
                                                  border: border,
                                                  isDense: true,
                                                  enabledBorder: border,
                                                  focusedBorder: border,
                                                  errorBorder: errorBorder,
                                                  focusedErrorBorder:
                                                      errorBorder,
                                                  contentPadding:
                                                      editTextPadding,
                                                  hintStyle: textFieldHintStyle,
                                                  hintText: selectDate,
                                                  suffixIconConstraints:
                                                      const BoxConstraints(
                                                          maxHeight: 40,
                                                          maxWidth: 40),
                                                  suffixIcon: Padding(
                                                      child: Image.asset(
                                                          "images/calendar.png",
                                                          color:
                                                              textFieldBorder),
                                                      padding:
                                                          dateUploadIconsPadding),
                                                ),
                                                style: textFieldStyle,
                                                readOnly: true,
                                                enabled: true,
                                                onTap: () async {
                                                  final DateTime? picked =
                                                      await showDatePicker(
                                                          context: context,
                                                          initialDate:
                                                              DateTime.now(),
                                                          firstDate:
                                                              DateTime(1900),
                                                          lastDate:
                                                              DateTime(2100));
                                                  if (picked != null) {
                                                    assignState(() {
                                                      var inputFormat =
                                                          DateFormat(
                                                              'yyyy-MM-dd');
                                                      var inputDate = inputFormat
                                                          .parse(picked
                                                                  .toLocal()
                                                                  .toString()
                                                                  .split(' ')[
                                                              0]); // removing time from date
                                                      var outputFormat =
                                                          DateFormat(
                                                              'dd/MM/yyyy');
                                                      var outputDate =
                                                          outputFormat.format(
                                                              inputDate);
                                                      _toDateController.text =
                                                          outputDate;
                                                    });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        debugPrint(
                                            "${_nameController.text} ${_fromDateController.text} ${_toDateController.text} ${_selectedTechnologies.length} ${_selectedEmployees.length}");
                                        Navigator.of(context).pop();
                                        if (_nameController.text
                                                    .trim()
                                                    .toString() !=
                                                "" ||
                                            _fromDateController.text
                                                    .trim()
                                                    .toString() !=
                                                "" ||
                                            _toDateController.text
                                                    .trim()
                                                    .toString() !=
                                                "" ||
                                            _selectedEmployees.isNotEmpty ||
                                            _selectedTechnologies.isNotEmpty) {
                                          setState(() {
                                            showReset = true;
                                            nameController.text =
                                                _nameController.text;
                                            fromDateController.text =
                                                _fromDateController.text;
                                            toDateController.text =
                                                _toDateController.text;
                                            selectedEmployees =
                                                _selectedEmployees;
                                            selectedTechnologies =
                                                _selectedTechnologies;
                                            candidateProfilesList = [];
                                            pageCount = 0;
                                            getAllCandidateProfiles(pageCount);
                                          });
                                        }
                                      },
                                      child: const Padding(
                                        padding: EdgeInsets.all(8),
                                        child: Text(
                                          "Apply $filter",
                                          softWrap: true,
                                          style: TextStyle(
                                              letterSpacing: 0.9,
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w600,
                                              color: Colors.white,
                                              fontSize: 14),
                                        ),
                                      ),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(buttonBg),
                                        overlayColor: MaterialStateProperty.all(
                                            buttonBg.withOpacity(0.1)),
                                        side: MaterialStateProperty.all(
                                            const BorderSide(color: buttonBg)),
                                        elevation: MaterialStateProperty.all(1),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
            );
          });
        });
  }

  showDeleteDialog(ProfilesList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: '$profile?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteProfileAPI(itemData.id!)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              setState(() {
                                candidateProfilesList = [];
                                pageCount = 0;
                                getAllCandidateProfiles(pageCount);
                              });
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showScheduleInterviewDialog(ProfilesList? itemData) {
    showDialog(
      context: context,
      builder: (context) {
        int selectedPos = -1;
        final profileStatusViewModel =
            Provider.of<ProfileStatusNotifier>(context, listen: true);
        final interviewStatusViewModel =
            Provider.of<InterviewStatusNotifier>(context, listen: true);
        final interviewTypeViewModel =
            Provider.of<InterviewTypeNotifier>(context, listen: true);
        final profileViewModel =
            Provider.of<ProfileNotifier>(context, listen: true);
        TextEditingController dateController = TextEditingController();
        TextEditingController commentTextController = TextEditingController();
        TextEditingController mailSubjectController = TextEditingController();
        TextEditingController mailContentController = TextEditingController();
        final HtmlEditorController htmlController = HtmlEditorController();
        final _formKey = GlobalKey<FormState>();
        List<DropdownMenuItem<EmployeeDetails>> employeesMenuItems = [];
        List<DropdownMenuItem<InterviewTypesList>> interviewTypesMenuItems = [];
        List<DropdownMenuItem<InterviewStatusList>> interviewStatusMenuItems =
            [];
        List<DropdownMenuItem<ProfilesStatusList>> profileStatusMenuItems = [];
        List<EmployeeDetails> employeesList = [];
        EmployeeDetails employeeInitialValue =
            EmployeeDetails(name: selectEmployee, id: 'none');
        InterviewTypesList interviewTypesInitialValue = InterviewTypesList(
            interviewType: 'Select Interview Type',
            id: 'none',
            mailSent: false);
        InterviewStatusList interviewStatusInitialValue = InterviewStatusList(
            statusName: 'Select Interviewer Status', id: 'none');
        // ProfilesStatusList profileStatusInitialValue = ProfilesStatusList(statusName: 'Select Profile Status', id: 'none', mailSent: false);
        employeesMenuItems.add(DropdownMenuItem(
            child: const Text(selectEmployee), value: employeeInitialValue));
        interviewTypesMenuItems.add(DropdownMenuItem(
            child: const Text('Select Interview Type'),
            value: interviewTypesInitialValue));
        interviewStatusMenuItems.add(DropdownMenuItem(
            child: const Text('Select Interviewer Status'),
            value: interviewStatusInitialValue));
        // profileStatusMenuItems.add(DropdownMenuItem(child: const Text('Select Profile Status'), value: profileStatusInitialValue));
        bool showCalendar = false,
            timeSlotsAvailable = false,
            mailPreviewShowing = false,
            showEdit = true,
            showPreview = false,
            showBack = false,
            showCancel = false,
            showSave = false,
            isInEditMode = false;
        String selectedDate = '',
            outputDate = '',
            dateApi = '',
            _mailOriginalSubject = "",
            _mailOriginalContent = "";
        List<int> daysList = [];
        for (var element in employeeViewModel.getEmployees) {
          employeesList.add(element);
          employeesMenuItems.add(
            DropdownMenuItem(
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(text: element.name!, style: projectAddedByStyle),
                    TextSpan(
                        text:
                            ' (${commaSeparatedTechnologiesString(element.technologiesList!)})',
                        style: completedStyle)
                  ]),
                ),
                value: element),
          );
        }
        for (var element in interviewTypeViewModel.getInterviewTypes!) {
          interviewTypesMenuItems.add(DropdownMenuItem(
              child: Text(element.interviewType!, style: completedStyle),
              value: element));
        }
        for (var element in interviewStatusViewModel.getInterviewStatuses!) {
          interviewStatusMenuItems.add(DropdownMenuItem(
              child: Text(element.statusName!, style: completedStyle),
              value: element));
        }
        for (var element in profileStatusViewModel.getProfileStatuses!) {
          profileStatusMenuItems.add(DropdownMenuItem(
              child: Text(element.statusName!, style: completedStyle),
              value: element));
        }
        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('$schedule Interview', style: profilepopHeader),
                        Text(
                            profileViewModel.getProfileResponse != null
                                ? "to ${profileViewModel.getProfileResponse!.name}"
                                : "",
                            style: addRoleSubStyle.copyWith(fontSize: 15)),
                      ],
                    ),
                  ),
                  IconButton(
                      icon: const Icon(Icons.close_rounded,
                          color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(30, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              content: Container(
                width: ScreenConfig.width(context) / 1.8,
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(minHeight: 200),
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: dashBoardCardBorderTile, width: 1),
                              color: addNewContainerBg),
                          child: SingleChildScrollView(
                            controller: ScrollController(),
                            child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 2),
                                                  child: Text(employee,
                                                      style: addRoleSubStyle
                                                          .copyWith(
                                                              fontSize: 14)),
                                                ),
                                                DropdownButtonFormField<
                                                    EmployeeDetails>(
                                                  items: employeesMenuItems,
                                                  decoration:
                                                      editTextRoleDecoration,
                                                  style: textFieldStyle,
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value: employeeInitialValue,
                                                  validator:
                                                      employeeDropDownValidator,
                                                  autovalidateMode:
                                                      AutovalidateMode
                                                          .onUserInteraction,
                                                  onChanged:
                                                      (EmployeeDetails? value) {
                                                    assignState(() {
                                                      daysList = [];
                                                      employeeInitialValue =
                                                          value!;
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .mondayAvailability!) {
                                                        daysList.add(
                                                            DateTime.monday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .tuesdayAvailability!) {
                                                        daysList.add(
                                                            DateTime.tuesday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .wednesdayAvailability!) {
                                                        daysList.add(
                                                            DateTime.wednesday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .thursdayAvailability!) {
                                                        daysList.add(
                                                            DateTime.thursday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .fridayAvailability!) {
                                                        daysList.add(
                                                            DateTime.friday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .saturdayAvailability!) {
                                                        daysList.add(
                                                            DateTime.saturday);
                                                      }
                                                      if (employeeInitialValue
                                                          .availabilitySchedule!
                                                          .sundayAvailability!) {
                                                        daysList.add(
                                                            DateTime.sunday);
                                                      }
                                                      showCalendar = false;
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 2),
                                                  child: Text('Interview Type',
                                                      style: addRoleSubStyle
                                                          .copyWith(
                                                              fontSize: 14)),
                                                ),
                                                DropdownButtonFormField<
                                                    InterviewTypesList>(
                                                  items:
                                                      interviewTypesMenuItems,
                                                  decoration:
                                                      editTextRoleDecoration,
                                                  style: textFieldStyle,
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value:
                                                      interviewTypesInitialValue,
                                                  validator:
                                                      interviewTypeDropDownValidator,
                                                  autovalidateMode:
                                                      AutovalidateMode
                                                          .onUserInteraction,
                                                  onChanged:
                                                      (InterviewTypesList?
                                                          value) {
                                                    assignState(() {
                                                      interviewTypesInitialValue =
                                                          value!;
                                                      mailPreviewShowing =
                                                          false;
                                                      if (interviewTypesInitialValue
                                                          .mailSent!) {
                                                        mailSubjectController
                                                                .text =
                                                            interviewTypesInitialValue
                                                                .mailSubject!;
                                                        mailContentController
                                                                .text =
                                                            interviewTypesInitialValue
                                                                .mailContent!;
                                                        _mailOriginalSubject =
                                                            interviewTypesInitialValue
                                                                .mailSubject!;
                                                        _mailOriginalContent =
                                                            interviewTypesInitialValue
                                                                .mailContent!;
                                                      }
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 2),
                                                  child: Text("Date & Time",
                                                      style: addRoleSubStyle
                                                          .copyWith(
                                                              fontSize: 14)),
                                                ),
                                                TextFormField(
                                                    controller: dateController,
                                                    maxLines: 1,
                                                    textInputAction:
                                                        TextInputAction.done,
                                                    textCapitalization:
                                                        TextCapitalization
                                                            .sentences,
                                                    obscureText: false,
                                                    validator:
                                                        emptyTextValidator,
                                                    autovalidateMode:
                                                        AutovalidateMode
                                                            .onUserInteraction,
                                                    decoration: InputDecoration(
                                                      fillColor:
                                                          CupertinoColors.white,
                                                      filled: true,
                                                      border: border,
                                                      isDense: true,
                                                      enabledBorder: border,
                                                      focusedBorder: border,
                                                      errorBorder: errorBorder,
                                                      focusedErrorBorder:
                                                          errorBorder,
                                                      contentPadding:
                                                          editTextPadding,
                                                      hintStyle:
                                                          textFieldHintStyle,
                                                      hintText: selectDate,
                                                      suffixIconConstraints:
                                                          const BoxConstraints(
                                                              maxHeight: 40,
                                                              maxWidth: 40),
                                                      suffixIcon: Padding(
                                                          child: Image.asset(
                                                              "images/calendar.png",
                                                              color:
                                                                  CupertinoColors
                                                                      .black),
                                                          padding:
                                                              dateUploadIconsPadding),
                                                    ),
                                                    style: textFieldStyle,
                                                    readOnly: true,
                                                    enabled: true,
                                                    onTap: () {
                                                      if (employeeInitialValue
                                                              .id !=
                                                          'none') {
                                                        assignState(() {
                                                          showCalendar =
                                                              !showCalendar;
                                                          if (!showCalendar) {
                                                            timeSlotsAvailable =
                                                                false;
                                                            selectedPos = -1;
                                                          }
                                                        });
                                                      } else {
                                                        showToast(
                                                            'Please select an Interviewer.');
                                                      }
                                                    }),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 10, 0, 10),
                                        child: Visibility(
                                          visible: showCalendar,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Material(
                                                elevation: 2,
                                                color: Colors.transparent,
                                                child: Container(
                                                  width: 300,
                                                  height: 250,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      border: Border.all(
                                                          color: buttonBg,
                                                          width: 1)),
                                                  child: Center(
                                                    child: CalendarDatePicker(
                                                      initialDate:
                                                          DateTime.now(),
                                                      firstDate: DateTime.now(),
                                                      lastDate: DateTime(2100),
                                                      onDateChanged: (DateTime
                                                          value) async {
                                                        var inputDate = '';
                                                        assignState(() {
                                                          var inputFormat =
                                                              DateFormat(
                                                                  'yyyy-MM-dd');
                                                          var _date = inputFormat
                                                              .parse(value
                                                                      .toLocal()
                                                                      .toString()
                                                                      .split(
                                                                          ' ')[
                                                                  0]); // removing time from date
                                                          var outputFormat =
                                                              DateFormat(
                                                                  'dd/MM/yyyy');
                                                          var outputFormatApi =
                                                              DateFormat(
                                                                  'dd-MM-yyyy');
                                                          var outputFormatDisplay =
                                                              DateFormat(
                                                                  'EEE, MMM dd');
                                                          selectedDate =
                                                              outputFormatDisplay
                                                                  .format(
                                                                      _date);
                                                          outputDate =
                                                              outputFormat
                                                                  .format(
                                                                      _date);
                                                          inputDate =
                                                              outputFormatApi
                                                                  .format(
                                                                      _date);
                                                          dateApi = inputDate;
                                                        });
                                                        showDialog(
                                                            context: context,
                                                            barrierDismissible:
                                                                false,
                                                            builder:
                                                                (BuildContext
                                                                    context) {
                                                              return loader();
                                                            });
                                                        await employeeViewModel
                                                            .viewDayBasedEmployeeScheduleAPI(
                                                                employeeInitialValue
                                                                    .id!,
                                                                inputDate)
                                                            .then((value) {
                                                          Navigator.of(context)
                                                              .pop();
                                                          if (employeeViewModel
                                                                  .dayBasedEmployeeSchedulesModel !=
                                                              null) {
                                                            if (employeeViewModel
                                                                    .dayBasedEmployeeSchedulesModel!
                                                                    .responseStatus ==
                                                                1) {
                                                              assignState(() {
                                                                timeSlotsAvailable =
                                                                    true;
                                                                selectedPos =
                                                                    -1;
                                                              });
                                                            } else {
                                                              showToast(
                                                                  employeeViewModel
                                                                      .dayBasedEmployeeSchedulesModel!
                                                                      .result!);
                                                            }
                                                          } else {
                                                            showToast(
                                                                pleaseTryAgain);
                                                          }
                                                        });
                                                      },
                                                      selectableDayPredicate:
                                                          (day) {
                                                        if (daysList
                                                            .isNotEmpty) {
                                                          for (var element
                                                              in daysList) {
                                                            if (element !=
                                                                DateTime.now()
                                                                    .weekday) {
                                                              daysList.add(
                                                                  DateTime.now()
                                                                      .weekday);
                                                            } else {
                                                              return daysList
                                                                  .contains(day
                                                                      .weekday);
                                                            }
                                                          }
                                                        } else {
                                                          return true;
                                                        }
                                                        return true;
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Visibility(
                                                visible: timeSlotsAvailable,
                                                child: Material(
                                                  elevation: 2,
                                                  color: Colors.transparent,
                                                  child: Container(
                                                    width: 120,
                                                    height: 250,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        border: Border.all(
                                                            color: buttonBg,
                                                            width: 1)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(4),
                                                          child: Center(
                                                            child: Text(
                                                              selectedDate,
                                                              style: const TextStyle(
                                                                  color:
                                                                      availableDateText,
                                                                  fontFamily:
                                                                      "Poppins",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize: 14),
                                                            ),
                                                          ),
                                                        ),
                                                        Flexible(
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    bottom: 5),
                                                            child: employeeViewModel
                                                                    .getAvailableSchedules
                                                                    .isNotEmpty
                                                                ? ListView
                                                                    .builder(
                                                                        shrinkWrap:
                                                                            true,
                                                                        controller:
                                                                            ScrollController(),
                                                                        physics:
                                                                            const ScrollPhysics(),
                                                                        itemCount: employeeViewModel
                                                                            .getAvailableSchedules
                                                                            .length,
                                                                        padding: const EdgeInsets.only(
                                                                            left:
                                                                                10,
                                                                            right:
                                                                                10),
                                                                        itemBuilder:
                                                                            (ctx,
                                                                                index) {
                                                                          return OnHoverCard(builder:
                                                                              (onHovered) {
                                                                            return Padding(
                                                                              padding: const EdgeInsets.only(top: 4, bottom: 4),
                                                                              child: Material(
                                                                                elevation: 2,
                                                                                color: Colors.transparent,
                                                                                child: ClipRRect(
                                                                                  borderRadius: BorderRadius.circular(3),
                                                                                  child: InkWell(
                                                                                    onTap: () {
                                                                                      assignState(() {
                                                                                        dateController.text = '$outputDate ${employeeViewModel.getAvailableSchedules[index]}';
                                                                                        dateApi = '${employeeViewModel.getAvailableSchedules[index]},$dateApi';
                                                                                        selectedPos = index;
                                                                                        showCalendar = false;
                                                                                        timeSlotsAvailable = false;
                                                                                      });
                                                                                    },
                                                                                    child: Container(
                                                                                      decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(3),
                                                                                          border: Border.all(color: buttonBg, width: 1),
                                                                                          color: onHovered
                                                                                              ? buttonBg
                                                                                              : selectedPos != index
                                                                                                  ? Colors.white
                                                                                                  : buttonBg),
                                                                                      padding: const EdgeInsets.all(5),
                                                                                      child: Center(
                                                                                        child: Text(
                                                                                          employeeViewModel.getAvailableSchedules[index]!,
                                                                                          style: TextStyle(
                                                                                              letterSpacing: 0.48,
                                                                                              color: onHovered
                                                                                                  ? Colors.white
                                                                                                  : selectedPos != index
                                                                                                      ? availableTimesText
                                                                                                      : Colors.white,
                                                                                              fontFamily: "Poppins",
                                                                                              fontWeight: FontWeight.w500,
                                                                                              fontSize: 14),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            );
                                                                          });
                                                                        })
                                                                : const Center(
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child:
                                                                          Text(
                                                                        noSlotsAvailable,
                                                                        style: TextStyle(
                                                                            letterSpacing:
                                                                                0.48,
                                                                            color:
                                                                                availableTimesText,
                                                                            fontFamily:
                                                                                "Poppins",
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                            fontSize: 14),
                                                                      ),
                                                                    ),
                                                                  ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    mailPreviewShowing
                                        ? Flexible(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(4),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: [
                                                      showCancel
                                                          ? Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(
                                                                      () {
                                                                    showEdit =
                                                                        true;
                                                                    showSave =
                                                                        false;
                                                                    showPreview =
                                                                        false;
                                                                    showCancel =
                                                                        false;
                                                                    showBack =
                                                                        false;
                                                                    isInEditMode =
                                                                        false;
                                                                    Future.delayed(
                                                                        const Duration(
                                                                            milliseconds:
                                                                                500),
                                                                        () {
                                                                      htmlController
                                                                          .insertHtml(
                                                                              _mailOriginalContent);
                                                                    });
                                                                    mailSubjectController
                                                                            .text =
                                                                        _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child:
                                                                    const Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  child: Text(
                                                                    cancel,
                                                                    softWrap:
                                                                        true,
                                                                    style: TextStyle(
                                                                        letterSpacing:
                                                                            0.9,
                                                                        fontFamily:
                                                                            "Poppins",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color:
                                                                            editText,
                                                                        fontSize:
                                                                            13),
                                                                  ),
                                                                ),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty.all(
                                                                          Colors
                                                                              .white),
                                                                  overlayColor:
                                                                      MaterialStateProperty.all(
                                                                          totalProfilesCardColor
                                                                              .withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(
                                                                      const BorderSide(
                                                                          color:
                                                                              buttonBg)),
                                                                  elevation:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showPreview
                                                          ? Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(
                                                                      () {
                                                                    showEdit =
                                                                        false;
                                                                    showSave =
                                                                        true;
                                                                    showPreview =
                                                                        false;
                                                                    showCancel =
                                                                        false;
                                                                    showBack =
                                                                        true;
                                                                    isInEditMode =
                                                                        false;
                                                                    mailSubjectController
                                                                            .text =
                                                                        mailSubjectController
                                                                            .text
                                                                            .trim()
                                                                            .toString();
                                                                    htmlController
                                                                        .getText()
                                                                        .then(
                                                                            (value) {
                                                                      assignState(
                                                                          () {
                                                                        mailContentController.text =
                                                                            value;
                                                                      });
                                                                    });
                                                                  });
                                                                },
                                                                child:
                                                                    const Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  child: Text(
                                                                    'Preview',
                                                                    softWrap:
                                                                        true,
                                                                    style: TextStyle(
                                                                        letterSpacing:
                                                                            0.9,
                                                                        fontFamily:
                                                                            "Poppins",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color:
                                                                            editText,
                                                                        fontSize:
                                                                            13),
                                                                  ),
                                                                ),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty.all(
                                                                          Colors
                                                                              .white),
                                                                  overlayColor:
                                                                      MaterialStateProperty.all(
                                                                          totalProfilesCardColor
                                                                              .withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(
                                                                      const BorderSide(
                                                                          color:
                                                                              buttonBg)),
                                                                  elevation:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showBack
                                                          ? Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(
                                                                      () {
                                                                    showEdit =
                                                                        false;
                                                                    showSave =
                                                                        true;
                                                                    showPreview =
                                                                        true;
                                                                    showCancel =
                                                                        true;
                                                                    showBack =
                                                                        false;
                                                                    isInEditMode =
                                                                        true;
                                                                    Future.delayed(
                                                                        const Duration(
                                                                            milliseconds:
                                                                                500),
                                                                        () {
                                                                      htmlController
                                                                          .insertHtml(
                                                                              _mailOriginalContent);
                                                                    });
                                                                    mailSubjectController
                                                                            .text =
                                                                        _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child:
                                                                    const Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  child: Text(
                                                                    back,
                                                                    softWrap:
                                                                        true,
                                                                    style: TextStyle(
                                                                        letterSpacing:
                                                                            0.9,
                                                                        fontFamily:
                                                                            "Poppins",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color:
                                                                            editText,
                                                                        fontSize:
                                                                            13),
                                                                  ),
                                                                ),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty.all(
                                                                          Colors
                                                                              .white),
                                                                  overlayColor:
                                                                      MaterialStateProperty.all(
                                                                          totalProfilesCardColor
                                                                              .withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(
                                                                      const BorderSide(
                                                                          color:
                                                                              buttonBg)),
                                                                  elevation:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showSave
                                                          ? Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(
                                                                      () {
                                                                    showEdit =
                                                                        true;
                                                                    showSave =
                                                                        false;
                                                                    showPreview =
                                                                        false;
                                                                    showCancel =
                                                                        false;
                                                                    showBack =
                                                                        false;
                                                                    isInEditMode =
                                                                        false;
                                                                    mailSubjectController
                                                                            .text =
                                                                        mailSubjectController
                                                                            .text
                                                                            .trim()
                                                                            .toString();
                                                                    _mailOriginalSubject =
                                                                        mailSubjectController
                                                                            .text
                                                                            .trim()
                                                                            .toString();
                                                                    htmlController
                                                                        .getText()
                                                                        .then(
                                                                            (value) {
                                                                      assignState(
                                                                          () {
                                                                        mailContentController.text =
                                                                            value;
                                                                        _mailOriginalContent =
                                                                            value;
                                                                      });
                                                                    });
                                                                  });
                                                                },
                                                                child:
                                                                    const Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  child: Text(
                                                                    save,
                                                                    softWrap:
                                                                        true,
                                                                    style: TextStyle(
                                                                        letterSpacing:
                                                                            0.9,
                                                                        fontFamily:
                                                                            "Poppins",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            13),
                                                                  ),
                                                                ),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              buttonBg),
                                                                  overlayColor:
                                                                      MaterialStateProperty.all(
                                                                          totalProfilesCardColor
                                                                              .withOpacity(0.3)),
                                                                  elevation:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showEdit
                                                          ? Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(
                                                                      () {
                                                                    showEdit =
                                                                        false;
                                                                    showSave =
                                                                        true;
                                                                    showPreview =
                                                                        true;
                                                                    showCancel =
                                                                        true;
                                                                    showBack =
                                                                        false;
                                                                    isInEditMode =
                                                                        true;
                                                                    Future.delayed(
                                                                        const Duration(
                                                                            milliseconds:
                                                                                500),
                                                                        () {
                                                                      htmlController
                                                                          .insertHtml(
                                                                              _mailOriginalContent);
                                                                    });
                                                                    mailSubjectController
                                                                            .text =
                                                                        _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child:
                                                                    const Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3),
                                                                  child: Text(
                                                                    edit,
                                                                    softWrap:
                                                                        true,
                                                                    style: TextStyle(
                                                                        letterSpacing:
                                                                            0.9,
                                                                        fontFamily:
                                                                            "Poppins",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            13),
                                                                  ),
                                                                ),
                                                                style:
                                                                    ButtonStyle(
                                                                  backgroundColor:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              buttonBg),
                                                                  overlayColor:
                                                                      MaterialStateProperty.all(
                                                                          totalProfilesCardColor
                                                                              .withOpacity(0.3)),
                                                                  elevation:
                                                                      MaterialStateProperty
                                                                          .all(
                                                                              1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0, 10, 0, 20),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Expanded(
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .fromLTRB(
                                                                  8, 0, 0, 0),
                                                          child: Text(
                                                              '$email subject',
                                                              style:
                                                                  addRoleSubStyle),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 3,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .fromLTRB(
                                                                  20, 0, 0, 0),
                                                          child: TextFormField(
                                                              minLines: 1,
                                                              maxLines: 10,
                                                              controller:
                                                                  mailSubjectController,
                                                              textInputAction:
                                                                  TextInputAction
                                                                      .next,
                                                              keyboardType:
                                                                  TextInputType
                                                                      .multiline,
                                                              obscureText:
                                                                  false,
                                                              enabled:
                                                                  isInEditMode,
                                                              decoration: InputDecoration(
                                                                  fillColor:
                                                                      CupertinoColors
                                                                          .white,
                                                                  filled: true,
                                                                  border:
                                                                      border,
                                                                  isDense: true,
                                                                  enabledBorder:
                                                                      border,
                                                                  focusedBorder:
                                                                      border,
                                                                  errorBorder:
                                                                      errorBorder,
                                                                  focusedErrorBorder:
                                                                      errorBorder,
                                                                  contentPadding:
                                                                      editTextPadding,
                                                                  hintStyle:
                                                                      textFieldHintStyle,
                                                                  hintText:
                                                                      '$email subject'),
                                                              style:
                                                                  textFieldStyle,
                                                              validator:
                                                                  emptyTextValidator,
                                                              autovalidateMode:
                                                                  AutovalidateMode
                                                                      .onUserInteraction),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                isInEditMode
                                                    ? Container(
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                color:
                                                                    textFieldHint,
                                                                width: 1),
                                                            color:
                                                                Colors.white),
                                                        child: HtmlEditor(
                                                          controller:
                                                              htmlController,
                                                          htmlEditorOptions:
                                                              const HtmlEditorOptions(
                                                            hint:
                                                                'Your text here...',
                                                            shouldEnsureVisible:
                                                                true,
                                                            spellCheck: true,
                                                          ),
                                                          htmlToolbarOptions:
                                                              HtmlToolbarOptions(
                                                            toolbarPosition:
                                                                ToolbarPosition
                                                                    .aboveEditor,
                                                            toolbarType: ToolbarType
                                                                .nativeExpandable,
                                                            defaultToolbarButtons: const [
                                                              StyleButtons(),
                                                              FontSettingButtons(
                                                                  fontSizeUnit:
                                                                      false),
                                                              FontButtons(
                                                                  clearAll:
                                                                      false),
                                                              ColorButtons(),
                                                              ListButtons(
                                                                  listStyles:
                                                                      false),
                                                              InsertButtons(
                                                                  video: false,
                                                                  audio: false,
                                                                  table: false,
                                                                  hr: false,
                                                                  otherFile:
                                                                      false),
                                                            ],
                                                            onButtonPressed:
                                                                (ButtonType type,
                                                                    bool?
                                                                        status,
                                                                    Function()?
                                                                        updateStatus) {
                                                              debugPrint(
                                                                  "button '${describeEnum(type)}' pressed, the current selected status is $status");
                                                              return true;
                                                            },
                                                            onDropdownChanged:
                                                                (DropdownType
                                                                        type,
                                                                    dynamic
                                                                        changed,
                                                                    Function(
                                                                            dynamic)?
                                                                        updateSelectedItem) {
                                                              debugPrint(
                                                                  "dropdown '${describeEnum(type)}' changed to $changed");
                                                              return true;
                                                            },
                                                            mediaLinkInsertInterceptor:
                                                                (String url,
                                                                    InsertFileType
                                                                        type) {
                                                              debugPrint(url);
                                                              return true;
                                                            },
                                                            mediaUploadInterceptor:
                                                                (PlatformFile
                                                                        file,
                                                                    InsertFileType
                                                                        type) async {
                                                              debugPrint(file
                                                                  .name); //filename
                                                              debugPrint(file
                                                                  .size
                                                                  .toString()); //size in bytes
                                                              debugPrint(file
                                                                  .extension); //file extension (eg jpeg or mp4)
                                                              return true;
                                                            },
                                                          ),
                                                          otherOptions:
                                                              const OtherOptions(
                                                                  height: 250),
                                                          callbacks: Callbacks(
                                                              onBeforeCommand:
                                                                  (String?
                                                                      currentHtml) {},
                                                              onChangeContent:
                                                                  (String?
                                                                      changed) {},
                                                              onChangeCodeview:
                                                                  (String?
                                                                      changed) {},
                                                              onChangeSelection:
                                                                  (EditorSettings
                                                                      settings) {},
                                                              onDialogShown:
                                                                  () {},
                                                              onEnter: () {},
                                                              onFocus: () {},
                                                              onBlur: () {},
                                                              onBlurCodeview:
                                                                  () {},
                                                              onInit: () {},
                                                              onImageUploadError:
                                                                  (FileUpload? file,
                                                                      String?
                                                                          base64Str,
                                                                      UploadError
                                                                          error) {
                                                                if (file !=
                                                                    null) {
                                                                  debugPrint(
                                                                      file.name);
                                                                  debugPrint(file
                                                                      .size
                                                                      .toString());
                                                                  debugPrint(
                                                                      file.type);
                                                                }
                                                              },
                                                              onKeyDown: (int?
                                                                  keyCode) {},
                                                              onKeyUp: (int?
                                                                  keyCode) {},
                                                              onMouseDown:
                                                                  () {},
                                                              onMouseUp: () {},
                                                              onNavigationRequestMobile:
                                                                  (String url) {
                                                                return NavigationActionPolicy
                                                                    .ALLOW;
                                                              },
                                                              onPaste: () {},
                                                              onScroll: () {}),
                                                          plugins: [
                                                            SummernoteAtMention(
                                                                getSuggestionsMobile:
                                                                    (String
                                                                        value) {
                                                                  var mentions =
                                                                      <String>[
                                                                    'test1',
                                                                    'test2',
                                                                    'test3'
                                                                  ];
                                                                  return mentions
                                                                      .where((element) =>
                                                                          element
                                                                              .contains(value))
                                                                      .toList();
                                                                },
                                                                mentionsWeb: [
                                                                  'test1',
                                                                  'test2',
                                                                  'test3'
                                                                ],
                                                                onSelect:
                                                                    (String
                                                                        value) {
                                                                  debugPrint(
                                                                      value);
                                                                }),
                                                          ],
                                                        ),
                                                      )
                                                    : Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(10),
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            border: Border.all(
                                                                color:
                                                                    mailPreviewBorder),
                                                            color:
                                                                mailPreviewBg),
                                                        child: HtmlWidget(
                                                          mailContentController
                                                              .text
                                                              .trim(),
                                                          buildAsync: true,
                                                          enableCaching: false,
                                                          renderMode:
                                                              RenderMode.column,
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          )
                                        : const SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        interviewTypesInitialValue.mailSent!
                            ? Padding(
                                padding:
                                    const EdgeInsets.only(left: 2, right: 2),
                                child: TextButton(
                                  onPressed: () {
                                    assignState(() {
                                      mailPreviewShowing = !mailPreviewShowing;
                                      showEdit = true;
                                      showSave = false;
                                      showPreview = false;
                                      showCancel = false;
                                      showBack = false;
                                      isInEditMode = false;
                                    });
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Text(
                                      'Preview Mail',
                                      softWrap: true,
                                      style: TextStyle(
                                          letterSpacing: 0.9,
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w600,
                                          color: buttonBg,
                                          fontSize: 14),
                                    ),
                                  ),
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all(Colors.white),
                                    overlayColor: MaterialStateProperty.all(
                                        totalProfilesCardColor
                                            .withOpacity(0.3)),
                                    side: MaterialStateProperty.all(
                                        const BorderSide(color: buttonBg)),
                                    elevation: MaterialStateProperty.all(2),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                        Padding(
                          padding: const EdgeInsets.only(left: 2, right: 2),
                          child: TextButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) {
                                      return loader();
                                    });
                                await viewModel
                                    .addInterviewScheduleAPI(
                                        employeeId,
                                        employeeId,
                                        itemData!.id!,
                                        interviewTypesInitialValue.id!,
                                        dateApi,
                                        commentTextController.text.trim(),
                                        _mailOriginalSubject,
                                        _mailOriginalContent)
                                    .then((value) {
                                  Navigator.of(context).pop();
                                  if (viewModel.interviewScheduleResponse!
                                          .responseStatus ==
                                      1) {
                                    showToast(viewModel
                                        .interviewScheduleResponse!.result!);
                                    setState(() {
                                      candidateProfilesList = [];
                                      pageCount = 0;
                                      Navigator.of(context).pop();
                                      getAllCandidateProfiles(pageCount);
                                    });
                                  } else {
                                    showToast(viewModel
                                        .interviewScheduleResponse!.result!);
                                  }
                                });
                              }
                            },
                            child: const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                schedule,
                                softWrap: true,
                                style: TextStyle(
                                    letterSpacing: 0.9,
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,
                                    fontSize: 14),
                              ),
                            ),
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(buttonBg),
                              overlayColor: MaterialStateProperty.all(
                                  totalProfilesCardColor.withOpacity(0.3)),
                              elevation: MaterialStateProperty.all(2),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  showStatusUpdateDialog(ProfilesList itemData) {
    showDialog(
      context: context,
      builder: (context) {
        final profileStatusViewModel =
            Provider.of<ProfileStatusNotifier>(context, listen: true);
        final _formKey = GlobalKey<FormState>();
        TextEditingController commentTextController = TextEditingController();
        List<DropdownMenuItem<ProfilesStatusList>> profileStatusMenuItems = [];
        ProfilesStatusList profileStatusInitialValue =
            ProfilesStatusList(statusName: 'Select Profile Status', id: 'none');
        profileStatusMenuItems.add(DropdownMenuItem(
            child: const Text('Select Profile Status'),
            value: profileStatusInitialValue));
        for (var element in profileStatusViewModel.getProfileStatuses!) {
          profileStatusMenuItems.add(DropdownMenuItem(
              child: Text(element.statusName!, style: completedStyle),
              value: element));
        }
        return StatefulBuilder(
          builder: (context, statusState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Status Update', style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded,
                          color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 2,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(
                    border:
                        Border.all(color: dashBoardCardBorderTile, width: 1),
                    color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 0, 30, 0),
                                            child: Text(status,
                                                style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 0, 20, 0),
                                            child: DropdownButtonFormField<
                                                ProfilesStatusList>(
                                              items: profileStatusMenuItems,
                                              decoration:
                                                  editTextRoleDecoration,
                                              style: textFieldStyle,
                                              value: profileStatusInitialValue,
                                              validator:
                                                  profileStatusDropDownValidator,
                                              autovalidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              onChanged:
                                                  (ProfilesStatusList? value) {
                                                statusState(() {
                                                  profileStatusInitialValue =
                                                      value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 0, 30, 0),
                                            child: Text(comment,
                                                style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 0, 20, 0),
                                            child: ConstrainedBox(
                                              constraints: const BoxConstraints(
                                                  minHeight: 5 * 5),
                                              child: IntrinsicWidth(
                                                child: TextFormField(
                                                    minLines: 5,
                                                    maxLines: 10,
                                                    controller:
                                                        commentTextController,
                                                    enabled: true,
                                                    textInputAction:
                                                        TextInputAction.done,
                                                    obscureText: false,
                                                    // validator: emptyTextValidator,
                                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                                    decoration: InputDecoration(
                                                        fillColor: CupertinoColors
                                                            .white,
                                                        filled: true,
                                                        isDense: true,
                                                        border: OutlineInputBorder(
                                                            borderSide:
                                                                const BorderSide(
                                                                    color:
                                                                        textFieldHint,
                                                                    width: 1),
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    5)),
                                                        enabledBorder: OutlineInputBorder(
                                                            borderSide:
                                                                const BorderSide(
                                                                    color:
                                                                        textFieldHint,
                                                                    width: 1),
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    5)),
                                                        focusedBorder: OutlineInputBorder(
                                                            borderSide:
                                                                const BorderSide(
                                                                    color:
                                                                        textFieldHint,
                                                                    width: 1),
                                                            borderRadius:
                                                                BorderRadius.circular(5)),
                                                        errorBorder: OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1), borderRadius: BorderRadius.circular(5)),
                                                        focusedErrorBorder: OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1), borderRadius: BorderRadius.circular(5)),
                                                        contentPadding: editTextPadding,
                                                        hintStyle: textFieldTodoCommentHintStyle,
                                                        hintText: addComment),
                                                    style: textFieldTodoCommentHintStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin:
                                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) {
                                                return loader();
                                              });
                                          viewModel
                                              .updateProfileStatusAPI(
                                                  employeeId,
                                                  itemData.id!,
                                                  profileStatusInitialValue.id!,
                                                  commentTextController.text
                                                      .trim())
                                              .then((value) {
                                            Navigator.of(context).pop();
                                            if (viewModel
                                                    .statusUpdateResponse !=
                                                null) {
                                              if (viewModel
                                                      .statusUpdateResponse!
                                                      .responseStatus ==
                                                  1) {
                                                setState(() {
                                                  candidateProfilesList = [];
                                                  pageCount = 0;
                                                  getAllCandidateProfiles(
                                                      pageCount);
                                                });
                                                showToast(viewModel
                                                    .statusUpdateResponse!
                                                    .result!);
                                                Navigator.of(context).pop();
                                              } else {
                                                showToast(viewModel
                                                    .statusUpdateResponse!
                                                    .result!);
                                              }
                                            } else {
                                              showToast(viewModel
                                                  .statusUpdateResponse!
                                                  .result!);
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            16, 8, 16, 8),
                                        child: FittedBox(
                                            fit: BoxFit.scaleDown,
                                            child: Text(update,
                                                style: newButtonsStyle)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
