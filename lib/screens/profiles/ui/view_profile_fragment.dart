import 'dart:convert';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_interview_status_response_model.dart';
import 'package:aem/model/all_interview_type_response_model.dart';
import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/model/view_profile_details_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/interview_status/view_model/interview_status_notifier.dart';
import 'package:aem/screens/interview_types/view_model/interview_types_notifier.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/screens/profiles/view_model/profile_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/selectable_text_factory_companion_html.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';
class ViewProfileFragment extends StatefulWidget {
  final String? profileId;
  const ViewProfileFragment({this.profileId, Key? key}) : super(key: key);
  @override
  _ViewProfileFragmentState createState() => _ViewProfileFragmentState();
}
class _ViewProfileFragmentState extends State<ViewProfileFragment> {
  String employeeId = "";
  String uploadFile = "";
  late ProfileNotifier viewModel;
  late EmployeeNotifier employeeViewModel;
  TextEditingController commentTextController = TextEditingController();
  bool editPermission = false,
      addPermission = false,
      viewPermission = false,
      assignPermission = false,
      fullAccessPermission = false,
      updateStatus = false,
      commentFieldVisible = true;
  final HtmlEditorController htmlController = HtmlEditorController();
  @override
  void initState() {
    employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: false);
    viewModel = Provider.of<ProfileNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.profileEditPermission;
    addPermission = userViewModel.profileAddPermission;
    viewPermission = userViewModel.profileViewPermission;
    assignPermission = userViewModel.profileAssignPermission;
    fullAccessPermission = userViewModel.profileFullAccessPermission;
    updateStatus = userViewModel.profileUpdateStatusPermission;
    getProfileData();
    debugPrint(widget.profileId);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.profileHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }
  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuProfiles > View Profile",
                    maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                                        flex: 3,
                                                        child: profileDetails(),
                                                      ),
                                    Expanded(
                                      flex: 1,
                                      child: profileActions(),
                                    )
                                  ]),
                            ),

                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                width: ScreenConfig.width(context),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [

                                              Flexible(
                                                fit: FlexFit.loose,
                                                child: interviewScheduleList(),
                                              )
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          child: profileComments(),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
        ),
      ],
    );
  }
  Widget profileDetails() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: true);
    AgeDuration? exp;
    if (viewModel.getProfileResponse != null) {
      if (viewModel.getProfileResponse!.experienceStatus == "experienced") {
        DateTime tempDate = DateFormat(inputDateFormat).parse(viewModel.getProfileResponse!.careerStartedOn!, true).toLocal();
        exp = Age.dateDifference(fromDate: tempDate, toDate: DateTime.now(), includeToDate: false);
      }
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(5),
              child: Text(
                viewModel.getProfileResponse != null ? viewModel.getProfileResponse!.name! : "",
                style: const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 22),
              ),
            ),
            viewModel.getProfileResponse != null ?  Padding(
              padding: const EdgeInsets.all(5),
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/experience.png',
                    width: 15,
                    height: 15,
                  ),
                  viewModel.getProfileResponse!.experienceStatus == "experienced" ? Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      "${exp!.years.toString()} years ${double.parse(exp.months.toString())} months",
                      style: const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14),
                    ),
                  ) : Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      viewModel.getProfileResponse!.experienceStatus!.inCaps,
                      style: const TextStyle(letterSpacing: 1.2, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14),
                    ),
                  ),
                ],
              ),
            )  : SizedBox(),
          ],
        ),
        viewModel.getProfileResponse != null ? Padding(
          padding: const EdgeInsets.only(left: 10,bottom: 5),
          child: Text(
            formatAgo(viewModel.getProfileResponse!.createdOn!),
            style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 15),
          ),
        ) : SizedBox(),
        Padding(
          padding: const EdgeInsets.all(5),
          child: Wrap(
            alignment: WrapAlignment.start,
            crossAxisAlignment: WrapCrossAlignment.start,
            runAlignment: WrapAlignment.start,
            runSpacing: 5,
            children: [
              viewModel.getProfileResponse != null
                  ? DecoratedBox(
                      decoration: BoxDecoration(
                        color: CupertinoColors.white,
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(color: dashBoardCardBorderTile, width: 1),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Image.asset(
                                "assets/images/profile_email_2x.png",
                                width: 20,
                                height: 15,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Text(
                              viewModel.getProfileResponse!.email!,
                              style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox(),
              /*viewModel.getProfileResponse != null
                  ? viewModel.getProfileResponse!.experienceStatus == "experienced"
                      ? DecoratedBox(
                          decoration: BoxDecoration(
                            color: CupertinoColors.white,
                            borderRadius: BorderRadius.circular(0),
                            border: Border.all(color: dashBoardCardBorderTile, width: 1),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 5, right: 5),
                                  child: Image.asset(
                                    "assets/images/profile_email_2x.png",
                                    width: 20,
                                    height: 15,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Text(
                                  "${exp!.years.toString()} years ${double.parse(exp.months.toString())} months",
                                  style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 15),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox()
                  : const SizedBox(),*/
              viewModel.getProfileResponse != null
                  ? DecoratedBox(
                      decoration: BoxDecoration(
                        color: CupertinoColors.white,
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(color: dashBoardCardBorderTile, width: 1),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Image.asset(
                                "assets/images/profile_phone_2x.png",
                                width: 20,
                                height: 20,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Text(
                              viewModel.getProfileResponse!.phoneNumber!,
                              style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox(),
              viewModel.getProfileResponse != null
                  ? DecoratedBox(
                      decoration: BoxDecoration(
                        color: CupertinoColors.white,
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(color: dashBoardCardBorderTile, width: 1),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Image.asset(
                                "assets/images/profile_technology_2x.png",
                                width: 20,
                                height: 20,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Text(
                              viewModel.getProfileResponse!.mainTechnologyName!,
                              style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox(),
              viewModel.getProfileResponse != null
                  ? DecoratedBox(
                      decoration: BoxDecoration(
                        color: CupertinoColors.white,
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(color: dashBoardCardBorderTile, width: 1),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 7, 15, 7),
                        child: viewModel.getProfileResponse!.resume! != ""
                            ? InkWell(
                                onTap: () async {
                                  if (viewModel.getProfileResponse != null) {
                                    var resume = viewModel.getProfileResponse!.resume!;
                                    if (resume.contains(".doc") || resume.contains(".docx") || resume.contains(".docs")) {
                                      // opens docs ext in new page
                                      final Uri url =
                                          Uri.parse("https://docs.google.com/gview?embedded=true&url=" + viewModel.getProfileResponse!.resume!);
                                      if (await canLaunchUrl(url)) {
                                        await launchUrl(url);
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    } else {
                                      // opens pdf in dialog
                                      showResumeDialog(viewModel.getProfileResponse!.name!, resume);
                                    }
                                  }
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5, right: 5),
                                      child: Image.asset(
                                        "assets/images/profile_resume_2x.png",
                                        width: 15,
                                        height: 20,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Text(
                                      viewModel.getProfileResponse!.resumeFileName! != ""
                                          ? viewModel.getProfileResponse!.resumeFileName!
                                          : "Resume",
                                      style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 15),
                                    ),
                                  ],
                                ),
                              )
                            : const Text(
                                "Not Attached",
                                style: TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 15),
                              ),
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
          child: Container(
            width: 400,
            decoration: BoxDecoration(
              color: CupertinoColors.white,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: newOnGoing, width: 1.5),
            ),
            child: Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: Text(
                      status,
                      style: TextStyle(fontSize: 16, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: DecoratedBox(
                    decoration: const BoxDecoration(color: newOnGoing),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Text(
                        viewModel.getProfileResponse != null ? viewModel.getProfileResponse!.employmentStatusName! : "",
                        style: const TextStyle(fontSize: 16, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
  Widget profileActions() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: true);
    return Column(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.start, children: [
      fullAccessPermission
          ? Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: OnHoverCard(builder: (isHovered) {
                return Container(
                  width: ScreenConfig.width(context) / 2,
                  decoration: BoxDecoration(
                      color: isHovered ? dropDownColor : CupertinoColors.white,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: dropDownColor, width: 1.5)),
                  child: TextButton(
                    child: const Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Text(
                        "$schedule Interview",
                        style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        commentFieldVisible = false;
                      });
                      showScheduleInterviewDialog();
                    },
                  ),
                );
              }),
            )
          : assignPermission
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: OnHoverCard(builder: (isHovered) {
                    return Container(
                      width: ScreenConfig.width(context) / 2,
                      decoration: BoxDecoration(
                          color: isHovered ? dropDownColor : CupertinoColors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: dropDownColor, width: 1.5)),
                      child: TextButton(
                        child: const Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                            assign,
                            style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            commentFieldVisible = false;
                          });
                          showScheduleInterviewDialog();
                        },
                      ),
                    );
                  }),
                )
              : const SizedBox(),
      viewModel.getProfileResponse != null
          ? viewModel.getProfileResponse!.createdBy! == employeeId
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: OnHoverCard(builder: (isHovered) {
                    return Container(
                      width: ScreenConfig.width(context) / 2,
                      decoration: BoxDecoration(
                          color: isHovered ? newCompleted : CupertinoColors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: newCompleted, width: 1.5)),
                      child: TextButton(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                            "$update $resume",
                            style: TextStyle(
                                fontSize: 15,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                color: isHovered ? CupertinoColors.white : addRoleText),
                          ),
                        ),
                        onPressed: () async {
                          await viewModel.requestResumeAPI(widget.profileId!).then((value) {
                            if (viewModel.requestResumeResponse != null) {
                              if (viewModel.requestResumeResponse!.responseStatus == 1) {
                                showToast(viewModel.requestResumeResponse!.result!);
                              } else {
                                showToast(viewModel.requestResumeResponse!.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        },
                      ),
                    );
                  }),
                )
              : const SizedBox()
          : const SizedBox(),
      viewModel.getProfileResponse != null
          ? viewModel.getProfileResponse!.employmentStatusSystemTest!
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: OnHoverCard(builder: (isHovered) {
                    return Container(
                      width: ScreenConfig.width(context) / 2,
                      decoration: BoxDecoration(
                          color: isHovered ? dropDownColor : CupertinoColors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: dropDownColor, width: 1.5)),
                      child: TextButton(
                        child: const Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                            "System Test",
                            style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            commentFieldVisible = false;
                          });
                          showSystemTestDialog();
                        },
                      ),
                    );
                  }),
                )
              : const SizedBox()
          : const SizedBox()
    ]);
  }
  Widget interviewScheduleList() {
    final viewModel = Provider.of<ProfileNotifier>(context, listen: true);
    var data = viewModel.getInterviewSchedulesList;
    return data.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.fromLTRB(5, 15, 5, 10),
            child: Theme(
              data: Theme.of(context).copyWith(dividerColor: buttonBg.withOpacity(0.2)),
              child: DataTable2(
                columnSpacing: 44,
                dividerThickness: 1,
                border: TableBorder.all(width: 1, color: buttonBg.withOpacity(0.2)),
                headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                headingRowHeight: 46,
                smRatio: 0.4,
                lmRatio: 1.2,
                columns: <DataColumn2>[
                  DataColumn2(
                    label: Text(tableRounds, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    size: ColumnSize.S,
                  ),
                  DataColumn2(
                    label: Text("Interview type", softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                    size: ColumnSize.L,
                  ),
                  DataColumn2(
                    label: Text(tableInterviewer, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                    size: ColumnSize.L,
                  ),
                  DataColumn2(
                    label: Text(tableScheduleDate, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                    size: ColumnSize.M,
                  ),
                  DataColumn2(
                    label: Text(tableStatus, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                    size: ColumnSize.L,
                  ),
                  DataColumn2(
                    label: Center(child: Text(tableActions, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center)),
                    size: ColumnSize.S,
                  ),
                ],
                rows: listItems(data),
              ),
            ),
          )
        : const SizedBox();
  }
  List<DataRow> listItems(List<InterviewSchedulesList?> data) {
    return data.map<DataRow2>((e) {
      return DataRow2(color: MaterialStateColor.resolveWith((states) => CupertinoColors.white), specificRowHeight: 60, cells: [
        DataCell(
          Text((data.length - data.indexOf(e)).toString(), softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
        ),
        DataCell(
          Text(e!.interviewTypename!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
        ),
        DataCell(
          Text(e.employeeName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center),
        ),
        DataCell(
          Text(dateFormatFullDateDMYHyphen(e.interviewTime!), softWrap: true, style: listItemsStyle, textAlign: TextAlign.center),
        ),
        DataCell(
          Text(e.interviewStatusName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center),
        ),
        DataCell(
          Center(
            child: fullAccessPermission
                ? e.interviewStatusName! == ""
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                        child: Tooltip(
                          message: '$status $update',
                          preferBelow: false,
                          decoration: toolTipDecoration,
                          textStyle: toolTipStyle,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Material(
                            elevation: 0.0,
                            shape: const CircleBorder(),
                            clipBehavior: Clip.hardEdge,
                            color: Colors.transparent,
                            child: Ink.image(
                              image: const AssetImage('assets/images/status_update_rounded.png'),
                              fit: BoxFit.cover,
                              width: 35,
                              height: 35,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    commentFieldVisible = false;
                                  });
                                  showStatusUpdateDialog(e);
                                },
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox()
                : e.employeeId == employeeId
                    ? e.interviewStatusName! == ""
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                            child: Tooltip(
                              message: '$status $update',
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Ink.image(
                                  image: const AssetImage('assets/images/status_update_rounded.png'),
                                  fit: BoxFit.cover,
                                  width: 35,
                                  height: 35,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        commentFieldVisible = true;
                                      });
                                      showStatusUpdateDialog(e);
                                    },
                                  ),
                                ),
                              ),
                            ),
                          )
                        : const SizedBox()
                    : const SizedBox(),
          ),
        )
      ]);
    }).toList();
  }
  Widget profileComments() {
    return Container(
      margin: const EdgeInsets.only(top: 15, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
            child: Text(
              '$comments:',
              style: TextStyle(letterSpacing: 1, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: newCompleted, fontSize: 18),
            ),
          ),
          Visibility(
            visible: commentFieldVisible,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
              child: Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: textFieldHint, width: 1), color: Colors.white),
                height: 300,
                child: HtmlEditor(
                  controller: htmlController,
                  htmlEditorOptions: const HtmlEditorOptions(
                    hint: 'Your text here...',
                    shouldEnsureVisible: true,
                    spellCheck: true,
                  ),
                  htmlToolbarOptions: HtmlToolbarOptions(
                    toolbarPosition: ToolbarPosition.aboveEditor,
                    toolbarType: ToolbarType.nativeExpandable,
                    defaultToolbarButtons: const [
                      StyleButtons(),
                      FontSettingButtons(fontSizeUnit: false),
                      FontButtons(clearAll: false),
                      ColorButtons(),
                      ListButtons(listStyles: false),
                      InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                    ],
                    onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                      debugPrint("button '${describeEnum(type)}' pressed, the current selected status is $status");
                      return true;
                    },
                    onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                      debugPrint("dropdown '${describeEnum(type)}' changed to $changed");
                      return true;
                    },
                    mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                      debugPrint(url);
                      return true;
                    },
                    mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                      debugPrint(file.name); //filename
                      debugPrint(file.size.toString()); //size in bytes
                      debugPrint(file.extension); //file extension (eg jpeg or mp4)
                      return true;
                    },
                  ),
                  otherOptions: const OtherOptions(height: 250),
                  callbacks: Callbacks(
                      onBeforeCommand: (String? currentHtml) {},
                      onChangeContent: (String? changed) {},
                      onChangeCodeview: (String? changed) {},
                      onChangeSelection: (EditorSettings settings) {},
                      onDialogShown: () {},
                      onEnter: () {},
                      onFocus: () {},
                      onBlur: () {},
                      onBlurCodeview: () {},
                      onInit: () {},
                      onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {
                        if (file != null) {
                          debugPrint(file.name);
                          debugPrint(file.size.toString());
                          debugPrint(file.type);
                        }
                      },
                      onKeyDown: (int? keyCode) {},
                      onKeyUp: (int? keyCode) {},
                      onMouseDown: () {},
                      onMouseUp: () {},
                      onNavigationRequestMobile: (String url) {
                        return NavigationActionPolicy.ALLOW;
                      },
                      onPaste: () {},
                      onScroll: () {}),
                  plugins: [
                    SummernoteAtMention(
                        getSuggestionsMobile: (String value) {
                          var mentions = <String>['test1', 'test2', 'test3'];
                          return mentions
                              .where(
                                (element) => element.contains(value),
                              )
                              .toList();
                        },
                        mentionsWeb: ['test1', 'test2', 'test3'],
                        onSelect: (String value) {
                          debugPrint(value);
                        }),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
            child: Container(
              decoration: buttonDecorationGreen,
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: TextButton(
                onPressed: () async {
                  htmlController.getText().then((value) {
                    if (value != "") {
                      viewModel.addProfileCommentAPI(employeeId, widget.profileId!, value).then((value) {
                        if (viewModel.addCommentResponse != null) {
                          if (viewModel.addCommentResponse!.responseStatus == 1) {
                            showToast(viewModel.addCommentResponse!.result!);
                            htmlController.clear();
                            getProfileData();
                          } else {
                            showToast(viewModel.addCommentResponse!.result!);
                          }
                        } else {
                          showToast(pleaseTryAgain);
                        }
                      });
                    }
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                  child: Text(submit.toUpperCase(), style: newButtonsStyle),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: const ScrollPhysics(),
                itemCount: viewModel.getProfileResponse != null ? viewModel.getProfileResponse!.profileCommentsList!.length : 0,
                itemBuilder: (context, index) {
                  return commentsView(viewModel.getProfileResponse!.profileCommentsList![index]);
                }),
          ),
        ],
      ),
    );
  }
  Widget commentsView(ProfileComment profileComment) {
    return Container(
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: profileCommentsBg, borderRadius: BorderRadius.circular(5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 5, 0),
              child: CircleAvatar(
                backgroundColor: getNameBasedColor(profileComment.scheduledByName!.toUpperCase()[0]),
                radius: 14,
                child: Center(
                  child: Text(profileComment.scheduledByName!.toUpperCase()[0], style: projectIconTextStyle),
                ),
              ),
              alignment: Alignment.centerLeft),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      RichText(
                        textAlign: TextAlign.start,
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                            text: profileComment.scheduledByName!,
                            style: const TextStyle(fontFamily: "Raleway", fontWeight: FontWeight.w600, color: editText, fontSize: 15),
                          ),
                          TextSpan(
                            text: ' (${dateFormatFullDate(profileComment.createdOn!)})',
                            style: const TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 14),
                          )
                        ]),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: HtmlWidget(
                    profileComment.comment!,
                    buildAsync: true,
                    enableCaching: false,
                    factoryBuilder: () => SelectableTextCompanion(),
                    onTapUrl: (url) async {
                      if (await canLaunchUrl(Uri.parse(url))) {
                        await launchUrl(Uri.parse(url));
                        return true;
                      } else {
                        throw 'Could not launch $url';
                      }
                    },
                    renderMode: RenderMode.column,
                    textStyle: const TextStyle(
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w400,
                      color: editText,
                      fontSize: 14,
                    ),
                    onTapImage: (imageData) {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  showResumeDialog(String name, String resumeFile) {
    PdfViewerController _pdfViewerController = PdfViewerController();
    OverlayEntry? _overlayEntry;
    void _showContextMenu(BuildContext context, PdfTextSelectionChangedDetails details) {
      final OverlayState? _overlayState = Overlay.of(context);
      _overlayEntry = OverlayEntry(
        builder: (context) => Positioned(
          top: details.globalSelectedRegion!.center.dy - 55,
          left: details.globalSelectedRegion!.bottomLeft.dx,
          child: Card(
            elevation: 3,
            color: Colors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            child: TextButton(
              child: const Padding(
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Text(
                  'Copy',
                  style: TextStyle(fontSize: 17),
                ),
              ),
              onPressed: () {
                Clipboard.setData(
                  ClipboardData(text: details.selectedText),
                );
                _pdfViewerController.clearSelection();
              },
            ),
          ),
        ),
      );
      _overlayState!.insert(_overlayEntry!);
    }

    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              actionsPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                height: ScreenConfig.height(context),
                width: ScreenConfig.width(context) / 1.7,
                // margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: SfPdfViewer.network(
                  resumeFile,
                  onDocumentLoadFailed: (fail) {
                    debugPrint("fail--- $fail");
                    showToast(fail.description);
                  },
                  initialZoomLevel: 1.5,
                  enableDoubleTapZooming: true,
                  onTextSelectionChanged: (PdfTextSelectionChangedDetails details) {
                    if (details.selectedText == null && _overlayEntry != null) {
                      _overlayEntry!.remove();
                      _overlayEntry = null;
                    } else if (details.selectedText != null && _overlayEntry == null) {
                      _showContextMenu(context, details);
                    }
                  },
                  controller: _pdfViewerController,
                ),
              ),
            );
          },
        );
      },
    );
  }
  showScheduleInterviewDialog() {
    showDialog(
      context: context,
      builder: (context) {
        int selectedPos = -1;
        final profileStatusViewModel = Provider.of<ProfileStatusNotifier>(context, listen: true);
        final interviewStatusViewModel = Provider.of<InterviewStatusNotifier>(context, listen: true);
        final interviewTypeViewModel = Provider.of<InterviewTypeNotifier>(context, listen: true);
        final profileViewModel = Provider.of<ProfileNotifier>(context, listen: true);
        TextEditingController dateController = TextEditingController();
        TextEditingController commentTextController = TextEditingController();
        TextEditingController mailSubjectController = TextEditingController();
        TextEditingController mailContentController = TextEditingController();
        final HtmlEditorController htmlController = HtmlEditorController();
        final _formKey = GlobalKey<FormState>();
        List<DropdownMenuItem<EmployeeDetails>> employeesMenuItems = [];
        List<DropdownMenuItem<InterviewTypesList>> interviewTypesMenuItems = [];
        List<DropdownMenuItem<InterviewStatusList>> interviewStatusMenuItems = [];
        List<DropdownMenuItem<ProfilesStatusList>> profileStatusMenuItems = [];
        List<EmployeeDetails> employeesList = [];
        EmployeeDetails employeeInitialValue = EmployeeDetails(name: selectEmployee, id: 'none');
        InterviewTypesList interviewTypesInitialValue = InterviewTypesList(interviewType: 'Select Interview Type', id: 'none', mailSent: false);
        InterviewStatusList interviewStatusInitialValue = InterviewStatusList(statusName: 'Select Interviewer Status', id: 'none');
        // ProfilesStatusList profileStatusInitialValue = ProfilesStatusList(statusName: 'Select Profile Status', id: 'none', mailSent: false);
        employeesMenuItems.add(DropdownMenuItem(child: const Text(selectEmployee), value: employeeInitialValue));
        interviewTypesMenuItems.add(DropdownMenuItem(child: const Text('Select Interview Type'), value: interviewTypesInitialValue));
        interviewStatusMenuItems.add(DropdownMenuItem(child: const Text('Select Interviewer Status'), value: interviewStatusInitialValue));
        // profileStatusMenuItems.add(DropdownMenuItem(child: const Text('Select Profile Status'), value: profileStatusInitialValue));
        bool showCalendar = false,
            timeSlotsAvailable = false,
            mailPreviewShowing = false,
            showEdit = true,
            showPreview = false,
            showBack = false,
            showCancel = false,
            showSave = false,
            isInEditMode = false;
        String selectedDate = '', outputDate = '', dateApi = '', _mailOriginalSubject = "", _mailOriginalContent = "";
        List<int> daysList = [];
        for (var element in employeeViewModel.getEmployees) {
          employeesList.add(element);
          employeesMenuItems.add(
            DropdownMenuItem(
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(text: element.name!, style: projectAddedByStyle),
                    TextSpan(text: ' (${commaSeparatedTechnologiesString(element.technologiesList!)})', style: completedStyle)
                  ]),
                ),
                value: element),
          );
        }
        for (var element in interviewTypeViewModel.getInterviewTypes!) {
          interviewTypesMenuItems.add(DropdownMenuItem(child: Text(element.interviewType!, style: completedStyle), value: element));
        }
        for (var element in interviewStatusViewModel.getInterviewStatuses!) {
          interviewStatusMenuItems.add(DropdownMenuItem(child: Text(element.statusName!, style: completedStyle), value: element));
        }
        for (var element in profileStatusViewModel.getProfileStatuses!) {
          profileStatusMenuItems.add(DropdownMenuItem(child: Text(element.statusName!, style: completedStyle), value: element));
        }
        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('$schedule Interview', style: profilepopHeader),
                        Text(profileViewModel.getProfileResponse != null ? "to ${profileViewModel.getProfileResponse!.name}" : "",
                            style: addRoleSubStyle.copyWith(fontSize: 15)),
                      ],
                    ),
                  ),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(30, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                width: ScreenConfig.width(context) / 1.8,
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(minHeight: 200),
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                          child: SingleChildScrollView(
                            controller: ScrollController(),
                            child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(bottom: 2),
                                                  child: Text(employee, style: addRoleSubStyle.copyWith(fontSize: 14)),
                                                ),
                                                DropdownButtonFormField<EmployeeDetails>(
                                                  items: employeesMenuItems,
                                                  decoration: editTextRoleDecoration,
                                                  style: textFieldStyle,
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value: employeeInitialValue,
                                                  validator: employeeDropDownValidator,
                                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                                  onChanged: (EmployeeDetails? value) {
                                                    assignState(() {
                                                      daysList = [];
                                                      employeeInitialValue = value!;
                                                      if (employeeInitialValue.availabilitySchedule!.mondayAvailability!) {
                                                        daysList.add(DateTime.monday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.tuesdayAvailability!) {
                                                        daysList.add(DateTime.tuesday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.wednesdayAvailability!) {
                                                        daysList.add(DateTime.wednesday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.thursdayAvailability!) {
                                                        daysList.add(DateTime.thursday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.fridayAvailability!) {
                                                        daysList.add(DateTime.friday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.saturdayAvailability!) {
                                                        daysList.add(DateTime.saturday);
                                                      }
                                                      if (employeeInitialValue.availabilitySchedule!.sundayAvailability!) {
                                                        daysList.add(DateTime.sunday);
                                                      }
                                                      showCalendar = false;
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(bottom: 2),
                                                  child: Text('Interview Type', style: addRoleSubStyle.copyWith(fontSize: 14)),
                                                ),
                                                DropdownButtonFormField<InterviewTypesList>(
                                                  items: interviewTypesMenuItems,
                                                  decoration: editTextRoleDecoration,
                                                  style: textFieldStyle,
                                                  isDense: true,
                                                  isExpanded: true,
                                                  value: interviewTypesInitialValue,
                                                  validator: interviewTypeDropDownValidator,
                                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                                  onChanged: (InterviewTypesList? value) {
                                                    assignState(() {
                                                      interviewTypesInitialValue = value!;
                                                      mailPreviewShowing = false;
                                                      if (interviewTypesInitialValue.mailSent!) {
                                                        mailSubjectController.text = interviewTypesInitialValue.mailSubject!;
                                                        mailContentController.text = interviewTypesInitialValue.mailContent!;
                                                        _mailOriginalSubject = interviewTypesInitialValue.mailSubject!;
                                                        _mailOriginalContent = interviewTypesInitialValue.mailContent!;
                                                      }
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(bottom: 2),
                                                  child: Text("Date & Time", style: addRoleSubStyle.copyWith(fontSize: 14)),
                                                ),
                                                TextFormField(
                                                    controller: dateController,
                                                    maxLines: 1,
                                                    textInputAction: TextInputAction.done,
                                                    textCapitalization: TextCapitalization.sentences,
                                                    obscureText: false,
                                                    validator: emptyTextValidator,
                                                    autovalidateMode: AutovalidateMode.onUserInteraction,
                                                    decoration: InputDecoration(
                                                      fillColor: CupertinoColors.white,
                                                      filled: true,
                                                      border: border,
                                                      isDense: true,
                                                      enabledBorder: border,
                                                      focusedBorder: border,
                                                      errorBorder: errorBorder,
                                                      focusedErrorBorder: errorBorder,
                                                      contentPadding: editTextPadding,
                                                      hintStyle: textFieldHintStyle,
                                                      hintText: selectDate,
                                                      suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                      suffixIcon: Padding(
                                                          child: Image.asset("images/calendar.png", color: CupertinoColors.black),
                                                          padding: dateUploadIconsPadding),
                                                    ),
                                                    style: textFieldStyle,
                                                    readOnly: true,
                                                    enabled: true,
                                                    onTap: () {
                                                      if (employeeInitialValue.id != 'none') {
                                                        assignState(() {
                                                          showCalendar = !showCalendar;
                                                          if (!showCalendar) {
                                                            timeSlotsAvailable = false;
                                                            selectedPos = -1;
                                                          }
                                                        });
                                                      } else {
                                                        showToast('Please select an Interviewer.');
                                                      }
                                                    }),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                        child: Visibility(
                                          visible: showCalendar,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Material(
                                                elevation: 2,
                                                color: Colors.transparent,
                                                child: Container(
                                                  width: 300,
                                                  height: 250,
                                                  decoration: BoxDecoration(color: Colors.white, border: Border.all(color: buttonBg, width: 1)),
                                                  child: Center(
                                                    child: CalendarDatePicker(
                                                      initialDate: DateTime.now(),
                                                      firstDate: DateTime.now(),
                                                      lastDate: DateTime(2100),
                                                      onDateChanged: (DateTime value) async {
                                                        var inputDate = '';
                                                        assignState(() {
                                                          var inputFormat = DateFormat('yyyy-MM-dd');
                                                          var _date =
                                                              inputFormat.parse(value.toLocal().toString().split(' ')[0]); // removing time from date
                                                          var outputFormat = DateFormat('dd/MM/yyyy');
                                                          var outputFormatApi = DateFormat('dd-MM-yyyy');
                                                          var outputFormatDisplay = DateFormat('EEE, MMM dd');
                                                          selectedDate = outputFormatDisplay.format(_date);
                                                          outputDate = outputFormat.format(_date);
                                                          inputDate = outputFormatApi.format(_date);
                                                          dateApi = inputDate;
                                                        });
                                                        showDialog(
                                                            context: context,
                                                            barrierDismissible: false,
                                                            builder: (BuildContext context) {
                                                              return loader();
                                                            });
                                                        await employeeViewModel
                                                            .viewDayBasedEmployeeScheduleAPI(employeeInitialValue.id!, inputDate)
                                                            .then((value) {
                                                          Navigator.of(context).pop();
                                                          if (employeeViewModel.dayBasedEmployeeSchedulesModel != null) {
                                                            if (employeeViewModel.dayBasedEmployeeSchedulesModel!.responseStatus == 1) {
                                                              assignState(() {
                                                                timeSlotsAvailable = true;
                                                                selectedPos = -1;
                                                              });
                                                            } else {
                                                              showToast(employeeViewModel.dayBasedEmployeeSchedulesModel!.result!);
                                                            }
                                                          } else {
                                                            showToast(pleaseTryAgain);
                                                          }
                                                        });
                                                      },
                                                      selectableDayPredicate: (day) {
                                                        if (daysList.isNotEmpty) {
                                                          for (var element in daysList) {
                                                            if (element != DateTime.now().weekday) {
                                                              daysList.add(DateTime.now().weekday);
                                                            } else {
                                                              return daysList.contains(day.weekday);
                                                            }
                                                          }
                                                        } else {
                                                          return true;
                                                        }
                                                        return true;
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Visibility(
                                                visible: timeSlotsAvailable,
                                                child: Material(
                                                  elevation: 2,
                                                  color: Colors.transparent,
                                                  child: Container(
                                                    width: 120,
                                                    height: 250,
                                                    decoration: BoxDecoration(color: Colors.white, border: Border.all(color: buttonBg, width: 1)),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        Padding(
                                                          padding: const EdgeInsets.all(4),
                                                          child: Center(
                                                            child: Text(
                                                              selectedDate,
                                                              style: const TextStyle(
                                                                  color: availableDateText,
                                                                  fontFamily: "Poppins",
                                                                  fontWeight: FontWeight.w500,
                                                                  fontSize: 14),
                                                            ),
                                                          ),
                                                        ),
                                                        Flexible(
                                                          child: Padding(
                                                            padding: const EdgeInsets.only(bottom: 5),
                                                            child: employeeViewModel.getAvailableSchedules.isNotEmpty
                                                                ? ListView.builder(
                                                                    shrinkWrap: true,
                                                                    controller: ScrollController(),
                                                                    physics: const ScrollPhysics(),
                                                                    itemCount: employeeViewModel.getAvailableSchedules.length,
                                                                    padding: const EdgeInsets.only(left: 10, right: 10),
                                                                    itemBuilder: (ctx, index) {
                                                                      return OnHoverCard(builder: (onHovered) {
                                                                        return Padding(
                                                                          padding: const EdgeInsets.only(top: 4, bottom: 4),
                                                                          child: Material(
                                                                            elevation: 2,
                                                                            color: Colors.transparent,
                                                                            child: ClipRRect(
                                                                              borderRadius: BorderRadius.circular(3),
                                                                              child: InkWell(
                                                                                onTap: () {
                                                                                  assignState(() {
                                                                                    dateController.text =
                                                                                        '$outputDate ${employeeViewModel.getAvailableSchedules[index]}';
                                                                                    dateApi =
                                                                                        '${employeeViewModel.getAvailableSchedules[index]},$dateApi';
                                                                                    selectedPos = index;
                                                                                    showCalendar = false;
                                                                                    timeSlotsAvailable = false;
                                                                                  });
                                                                                },
                                                                                child: Container(
                                                                                  decoration: BoxDecoration(
                                                                                      borderRadius: BorderRadius.circular(3),
                                                                                      border: Border.all(color: buttonBg, width: 1),
                                                                                      color: onHovered
                                                                                          ? buttonBg
                                                                                          : selectedPos != index
                                                                                              ? Colors.white
                                                                                              : buttonBg),
                                                                                  padding: const EdgeInsets.all(5),
                                                                                  child: Center(
                                                                                    child: Text(
                                                                                      employeeViewModel.getAvailableSchedules[index]!,
                                                                                      style: TextStyle(
                                                                                          letterSpacing: 0.48,
                                                                                          color: onHovered
                                                                                              ? Colors.white
                                                                                              : selectedPos != index
                                                                                                  ? availableTimesText
                                                                                                  : Colors.white,
                                                                                          fontFamily: "Poppins",
                                                                                          fontWeight: FontWeight.w500,
                                                                                          fontSize: 14),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        );
                                                                      });
                                                                    })
                                                                : const Center(
                                                                    child: Padding(
                                                                      padding: EdgeInsets.all(5),
                                                                      child: Text(
                                                                        noSlotsAvailable,
                                                                        style: TextStyle(
                                                                            letterSpacing: 0.48,
                                                                            color: availableTimesText,
                                                                            fontFamily: "Poppins",
                                                                            fontWeight: FontWeight.w500,
                                                                            fontSize: 14),
                                                                      ),
                                                                    ),
                                                                  ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    mailPreviewShowing
                                        ? Flexible(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.all(4),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: [
                                                      showCancel
                                                          ? Padding(
                                                              padding: const EdgeInsets.only(left: 2, right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(() {
                                                                    showEdit = true;
                                                                    showSave = false;
                                                                    showPreview = false;
                                                                    showCancel = false;
                                                                    showBack = false;
                                                                    isInEditMode = false;
                                                                    Future.delayed(const Duration(milliseconds: 500), () {
                                                                      htmlController.insertHtml(_mailOriginalContent);
                                                                    });
                                                                    mailSubjectController.text = _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child: const Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    cancel,
                                                                    softWrap: true,
                                                                    style: TextStyle(
                                                                        letterSpacing: 0.9,
                                                                        fontFamily: "Poppins",
                                                                        fontWeight: FontWeight.w600,
                                                                        color: editText,
                                                                        fontSize: 13),
                                                                  ),
                                                                ),
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(Colors.white),
                                                                  overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
                                                                  elevation: MaterialStateProperty.all(1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showPreview
                                                          ? Padding(
                                                              padding: const EdgeInsets.only(left: 2, right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(() {
                                                                    showEdit = false;
                                                                    showSave = true;
                                                                    showPreview = false;
                                                                    showCancel = false;
                                                                    showBack = true;
                                                                    isInEditMode = false;
                                                                    mailSubjectController.text = mailSubjectController.text.trim().toString();
                                                                    htmlController.getText().then((value) {
                                                                      assignState(() {
                                                                        mailContentController.text = value;
                                                                      });
                                                                    });
                                                                  });
                                                                },
                                                                child: const Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    'Preview',
                                                                    softWrap: true,
                                                                    style: TextStyle(
                                                                        letterSpacing: 0.9,
                                                                        fontFamily: "Poppins",
                                                                        fontWeight: FontWeight.w600,
                                                                        color: editText,
                                                                        fontSize: 13),
                                                                  ),
                                                                ),
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(Colors.white),
                                                                  overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
                                                                  elevation: MaterialStateProperty.all(1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showBack
                                                          ? Padding(
                                                              padding: const EdgeInsets.only(left: 2, right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(() {
                                                                    showEdit = false;
                                                                    showSave = true;
                                                                    showPreview = true;
                                                                    showCancel = true;
                                                                    showBack = false;
                                                                    isInEditMode = true;
                                                                    Future.delayed(const Duration(milliseconds: 500), () {
                                                                      htmlController.insertHtml(_mailOriginalContent);
                                                                    });
                                                                    mailSubjectController.text = _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child: const Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    back,
                                                                    softWrap: true,
                                                                    style: TextStyle(
                                                                        letterSpacing: 0.9,
                                                                        fontFamily: "Poppins",
                                                                        fontWeight: FontWeight.w600,
                                                                        color: editText,
                                                                        fontSize: 13),
                                                                  ),
                                                                ),
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(Colors.white),
                                                                  overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                                                  side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
                                                                  elevation: MaterialStateProperty.all(1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showSave
                                                          ? Padding(
                                                              padding: const EdgeInsets.only(left: 2, right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(() {
                                                                    showEdit = true;
                                                                    showSave = false;
                                                                    showPreview = false;
                                                                    showCancel = false;
                                                                    showBack = false;
                                                                    isInEditMode = false;
                                                                    mailSubjectController.text = mailSubjectController.text.trim().toString();
                                                                    _mailOriginalSubject = mailSubjectController.text.trim().toString();
                                                                    htmlController.getText().then((value) {
                                                                      assignState(() {
                                                                        mailContentController.text = value;
                                                                        _mailOriginalContent = value;
                                                                      });
                                                                    });
                                                                  });
                                                                },
                                                                child: const Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    save,
                                                                    softWrap: true,
                                                                    style: TextStyle(
                                                                        letterSpacing: 0.9,
                                                                        fontFamily: "Poppins",
                                                                        fontWeight: FontWeight.w600,
                                                                        color: Colors.white,
                                                                        fontSize: 13),
                                                                  ),
                                                                ),
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(buttonBg),
                                                                  overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                                                  elevation: MaterialStateProperty.all(1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                      showEdit
                                                          ? Padding(
                                                              padding: const EdgeInsets.only(left: 2, right: 2),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  assignState(() {
                                                                    showEdit = false;
                                                                    showSave = true;
                                                                    showPreview = true;
                                                                    showCancel = true;
                                                                    showBack = false;
                                                                    isInEditMode = true;
                                                                    Future.delayed(const Duration(milliseconds: 500), () {
                                                                      htmlController.insertHtml(_mailOriginalContent);
                                                                    });
                                                                    mailSubjectController.text = _mailOriginalSubject;
                                                                  });
                                                                },
                                                                child: const Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    edit,
                                                                    softWrap: true,
                                                                    style: TextStyle(
                                                                        letterSpacing: 0.9,
                                                                        fontFamily: "Poppins",
                                                                        fontWeight: FontWeight.w600,
                                                                        color: Colors.white,
                                                                        fontSize: 13),
                                                                  ),
                                                                ),
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(buttonBg),
                                                                  overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                                                  elevation: MaterialStateProperty.all(1),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox(),
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Expanded(
                                                        child: Padding(
                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                          child: Text('$email subject', style: addRoleSubStyle),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 3,
                                                        child: Padding(
                                                          padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                                                          child: TextFormField(
                                                              minLines: 1,
                                                              maxLines: 10,
                                                              controller: mailSubjectController,
                                                              textInputAction: TextInputAction.next,
                                                              keyboardType: TextInputType.multiline,
                                                              obscureText: false,
                                                              enabled: isInEditMode,
                                                              decoration: InputDecoration(
                                                                  fillColor: CupertinoColors.white,
                                                                  filled: true,
                                                                  border: border,
                                                                  isDense: true,
                                                                  enabledBorder: border,
                                                                  focusedBorder: border,
                                                                  errorBorder: errorBorder,
                                                                  focusedErrorBorder: errorBorder,
                                                                  contentPadding: editTextPadding,
                                                                  hintStyle: textFieldHintStyle,
                                                                  hintText: '$email subject'),
                                                              style: textFieldStyle,
                                                              validator: emptyTextValidator,
                                                              autovalidateMode: AutovalidateMode.onUserInteraction),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                isInEditMode
                                                    ? Container(
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(5),
                                                            border: Border.all(color: textFieldHint, width: 1),
                                                            color: Colors.white),
                                                        child: HtmlEditor(
                                                          controller: htmlController,
                                                          htmlEditorOptions: const HtmlEditorOptions(
                                                            hint: 'Your text here...',
                                                            shouldEnsureVisible: true,
                                                            spellCheck: true,
                                                          ),
                                                          htmlToolbarOptions: HtmlToolbarOptions(
                                                            toolbarPosition: ToolbarPosition.aboveEditor,
                                                            toolbarType: ToolbarType.nativeExpandable,
                                                            defaultToolbarButtons: const [
                                                              StyleButtons(),
                                                              FontSettingButtons(fontSizeUnit: false),
                                                              FontButtons(clearAll: false),
                                                              ColorButtons(),
                                                              ListButtons(listStyles: false),
                                                              InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                                                            ],
                                                            onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                                                              debugPrint(
                                                                  "button '${describeEnum(type)}' pressed, the current selected status is $status");
                                                              return true;
                                                            },
                                                            onDropdownChanged:
                                                                (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                                                              debugPrint("dropdown '${describeEnum(type)}' changed to $changed");
                                                              return true;
                                                            },
                                                            mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                                                              debugPrint(url);
                                                              return true;
                                                            },
                                                            mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                                                              debugPrint(file.name); //filename
                                                              debugPrint(file.size.toString()); //size in bytes
                                                              debugPrint(file.extension); //file extension (eg jpeg or mp4)
                                                              return true;
                                                            },
                                                          ),
                                                          otherOptions: const OtherOptions(height: 250),
                                                          callbacks: Callbacks(
                                                              onBeforeCommand: (String? currentHtml) {},
                                                              onChangeContent: (String? changed) {},
                                                              onChangeCodeview: (String? changed) {},
                                                              onChangeSelection: (EditorSettings settings) {},
                                                              onDialogShown: () {},
                                                              onEnter: () {},
                                                              onFocus: () {},
                                                              onBlur: () {},
                                                              onBlurCodeview: () {},
                                                              onInit: () {},
                                                              onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {
                                                                if (file != null) {
                                                                  debugPrint(file.name);
                                                                  debugPrint(file.size.toString());
                                                                  debugPrint(file.type);
                                                                }
                                                              },
                                                              onKeyDown: (int? keyCode) {},
                                                              onKeyUp: (int? keyCode) {},
                                                              onMouseDown: () {},
                                                              onMouseUp: () {},
                                                              onNavigationRequestMobile: (String url) {
                                                                return NavigationActionPolicy.ALLOW;
                                                              },
                                                              onPaste: () {},
                                                              onScroll: () {}),
                                                          plugins: [
                                                            SummernoteAtMention(
                                                                getSuggestionsMobile: (String value) {
                                                                  var mentions = <String>['test1', 'test2', 'test3'];
                                                                  return mentions.where((element) => element.contains(value)).toList();
                                                                },
                                                                mentionsWeb: ['test1', 'test2', 'test3'],
                                                                onSelect: (String value) {
                                                                  debugPrint(value);
                                                                }),
                                                          ],
                                                        ),
                                                      )
                                                    : Container(
                                                        padding: const EdgeInsets.all(10),
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(5),
                                                            border: Border.all(color: mailPreviewBorder),
                                                            color: mailPreviewBg),
                                                        child: HtmlWidget(
                                                          mailContentController.text.trim(),
                                                          buildAsync: true,
                                                          enableCaching: false,
                                                          renderMode: RenderMode.column,
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          )
                                        : const SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        interviewTypesInitialValue.mailSent!
                            ? Padding(
                                padding: const EdgeInsets.only(left: 2, right: 2),
                                child: TextButton(
                                  onPressed: () {
                                    assignState(() {
                                      mailPreviewShowing = !mailPreviewShowing;
                                      showEdit = true;
                                      showSave = false;
                                      showPreview = false;
                                      showCancel = false;
                                      showBack = false;
                                      isInEditMode = false;
                                    });
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Text(
                                      'Preview Mail',
                                      softWrap: true,
                                      style: TextStyle(
                                          letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: buttonBg, fontSize: 14),
                                    ),
                                  ),
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.white),
                                    overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                                    side: MaterialStateProperty.all(const BorderSide(color: buttonBg)),
                                    elevation: MaterialStateProperty.all(2),
                                  ),
                                ),
                              )
                            : const SizedBox(),
                        Padding(
                          padding: const EdgeInsets.only(left: 2, right: 2),
                          child: TextButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) {
                                      return loader();
                                    });
                                await viewModel
                                    .addInterviewScheduleAPI(employeeId, employeeId, widget.profileId!, interviewTypesInitialValue.id!, dateApi,
                                        commentTextController.text.trim(), _mailOriginalSubject, _mailOriginalContent)
                                    .then((value) {
                                  Navigator.of(context).pop();
                                  if (viewModel.interviewScheduleResponse!.responseStatus == 1) {
                                    showToast(viewModel.interviewScheduleResponse!.result!);
                                    getProfileData();
                                    Navigator.of(context).pop();
                                  } else {
                                    showToast(viewModel.interviewScheduleResponse!.result!);
                                  }
                                });
                              }
                            },
                            child: const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                schedule,
                                softWrap: true,
                                style: TextStyle(
                                    letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: Colors.white, fontSize: 14),
                              ),
                            ),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(buttonBg),
                              overlayColor: MaterialStateProperty.all(totalProfilesCardColor.withOpacity(0.3)),
                              elevation: MaterialStateProperty.all(2),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    ).then((value) {
      setState(() {
        commentFieldVisible = true;
      });
    });
  }
  showStatusUpdateDialog(InterviewSchedulesList interviewSchedulesList) {
    showDialog(
      context: context,
      builder: (context) {
        final interviewStatusViewModel = Provider.of<InterviewStatusNotifier>(context, listen: true);
        // interviewStatusViewModel.getInterviewStatusesAPI();
        final _formKey = GlobalKey<FormState>();
        TextEditingController commentTextController = TextEditingController();
        List<DropdownMenuItem<InterviewStatusList>> interviewStatusMenuItems = [];
        InterviewStatusList interviewStatusInitialValue = InterviewStatusList(statusName: 'Select Interviewer Status', id: 'none');
        interviewStatusMenuItems.add(DropdownMenuItem(child: const Text('Select Interviewer Status'), value: interviewStatusInitialValue));
        for (var element in interviewStatusViewModel.getInterviewStatuses!) {
          interviewStatusMenuItems.add(DropdownMenuItem(child: Text(element.statusName!, style: completedStyle), value: element));
        }
        return StatefulBuilder(
          builder: (context, statusState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Status Update', style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 2,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text(status, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: DropdownButtonFormField<InterviewStatusList>(
                                              items: interviewStatusMenuItems,
                                              decoration: editTextRoleDecoration,
                                              style: textFieldStyle,
                                              value: interviewStatusInitialValue,
                                              validator: interviewStatusDropDownValidator,
                                              autovalidateMode: AutovalidateMode.onUserInteraction,
                                              onChanged: (InterviewStatusList? value) {
                                                statusState(() {
                                                  interviewStatusInitialValue = value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text(comment, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: ConstrainedBox(
                                              constraints: const BoxConstraints(minHeight: 5 * 5),
                                              child: IntrinsicWidth(
                                                child: TextFormField(
                                                    minLines: 5,
                                                    maxLines: 10,
                                                    controller: commentTextController,
                                                    enabled: true,
                                                    textInputAction: TextInputAction.done,
                                                    obscureText: false,
                                                    // validator: emptyTextValidator,
                                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                                    decoration: InputDecoration(
                                                        fillColor: CupertinoColors.white,
                                                        filled: true,
                                                        isDense: true,
                                                        border: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        enabledBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        focusedBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        errorBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        focusedErrorBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        contentPadding: editTextPadding,
                                                        hintStyle: textFieldTodoCommentHintStyle,
                                                        hintText: addComment),
                                                    style: textFieldTodoCommentHintStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) {
                                                return loader();
                                              });
                                          viewModel
                                              .updateInterviewScheduleAPI(employeeId, interviewSchedulesList.id!, interviewStatusInitialValue.id!,
                                                  commentTextController.text.trim())
                                              .then((value) {
                                            Navigator.of(context).pop();
                                            if (viewModel.updateInterviewScheduleResponse != null) {
                                              if (viewModel.updateInterviewScheduleResponse!.responseStatus == 1) {
                                                showToast(viewModel.updateInterviewScheduleResponse!.result!);
                                                getProfileData();
                                                Navigator.of(context).pop();
                                              } else {
                                                showToast(viewModel.updateInterviewScheduleResponse!.result!);
                                              }
                                            } else {
                                              showToast(viewModel.updateInterviewScheduleResponse!.result!);
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(update, style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    ).then((value) {
      setState(() {
        commentFieldVisible = true;
      });
    });
  }
  showSystemTestDialog() {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController timeController = TextEditingController();
        TextEditingController dateController = TextEditingController();
        TextEditingController commentTextController = TextEditingController();
        TextEditingController resumeUploadController = TextEditingController();
        List<String> supportingDocs = [];
        final _formKey = GlobalKey<FormState>();
        var sDate = "DD-MM-YYYY", sTime = "00:00 AM";
        DateTime selectedDate = DateTime.now();
        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('System test', style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 1.5,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text("Date & Time", style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(right: 5),
                                                    child: TextFormField(
                                                        controller: dateController,
                                                        maxLines: 1,
                                                        textInputAction: TextInputAction.done,
                                                        textCapitalization: TextCapitalization.sentences,
                                                        obscureText: false,
                                                        validator: emptyTextValidator,
                                                        autovalidateMode: AutovalidateMode.onUserInteraction,
                                                        decoration: InputDecoration(
                                                          fillColor: CupertinoColors.white,
                                                          filled: true,
                                                          border: border,
                                                          isDense: true,
                                                          enabledBorder: border,
                                                          focusedBorder: border,
                                                          errorBorder: errorBorder,
                                                          focusedErrorBorder: errorBorder,
                                                          contentPadding: editTextPadding,
                                                          hintStyle: textFieldHintStyle,
                                                          hintText: "DD/MM/YYYY",
                                                          suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                          suffixIcon: Padding(
                                                              child: Image.asset("images/calendar.png", color: CupertinoColors.black),
                                                              padding: dateUploadIconsPadding),
                                                        ),
                                                        style: textFieldStyle,
                                                        readOnly: true,
                                                        enabled: true,
                                                        onTap: () async {
                                                          final DateTime? picked = await showDatePicker(
                                                              context: context,
                                                              initialDate: DateTime.now(),
                                                              firstDate: DateTime.now(),
                                                              lastDate: DateTime(2100));
                                                          if (picked != null) {
                                                            assignState(() {
                                                              selectedDate = picked;
                                                              var inputFormat = DateFormat('yyyy-MM-dd');
                                                              var inputDate = inputFormat
                                                                  .parse(selectedDate.toLocal().toString().split(' ')[0]); // removing time from date
                                                              var outputFormat = DateFormat('dd/MM/yyyy');
                                                              var outputFormatServer = DateFormat('dd-MM-yyyy');
                                                              var outputDate = outputFormat.format(inputDate);
                                                              var outputDateServer = outputFormatServer.format(inputDate);
                                                              sDate = outputDateServer;
                                                              dateController.text = outputDate;
                                                            });
                                                          }
                                                        }),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left: 5),
                                                    child: TextFormField(
                                                        controller: timeController,
                                                        maxLines: 1,
                                                        textInputAction: TextInputAction.done,
                                                        textCapitalization: TextCapitalization.sentences,
                                                        obscureText: false,
                                                        validator: emptyTextValidator,
                                                        autovalidateMode: AutovalidateMode.onUserInteraction,
                                                        decoration: InputDecoration(
                                                          fillColor: CupertinoColors.white,
                                                          filled: true,
                                                          border: border,
                                                          isDense: true,
                                                          enabledBorder: border,
                                                          focusedBorder: border,
                                                          errorBorder: errorBorder,
                                                          focusedErrorBorder: errorBorder,
                                                          contentPadding: editTextPadding,
                                                          hintStyle: textFieldHintStyle,
                                                          hintText: "00:00 AM",
                                                          prefixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                          prefixIcon: Padding(
                                                              child: Image.asset("images/calendar.png", color: CupertinoColors.black),
                                                              padding: dateUploadIconsPadding),
                                                        ),
                                                        style: textFieldStyle,
                                                        readOnly: true,
                                                        enabled: true,
                                                        onTap: () async {
                                                          TimeOfDay selectedTime = TimeOfDay.now();
                                                          final TimeOfDay? timeOfDay = await showTimePicker(
                                                            context: context,
                                                            initialTime: selectedTime,
                                                            initialEntryMode: TimePickerEntryMode.dial,
                                                            confirmText: "CONFIRM",
                                                            cancelText: "NOT NOW",
                                                            helpText: "AVAILABLE TIME",
                                                          );
                                                          if (timeOfDay != null) {
                                                            assignState(() {
                                                              selectedTime = timeOfDay;
                                                              sTime = formatTimeOfDay(selectedTime);
                                                              timeController.text = sTime;
                                                            });
                                                          }
                                                        }),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text(comment, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: ConstrainedBox(
                                              constraints: const BoxConstraints(minHeight: 5 * 5),
                                              child: IntrinsicWidth(
                                                child: TextFormField(
                                                    minLines: 5,
                                                    maxLines: 10,
                                                    controller: commentTextController,
                                                    enabled: true,
                                                    textInputAction: TextInputAction.done,
                                                    obscureText: false,
                                                    // validator: emptyTextValidator,
                                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                                    decoration: InputDecoration(
                                                        fillColor: CupertinoColors.white,
                                                        filled: true,
                                                        isDense: true,
                                                        border: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        enabledBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        focusedBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        errorBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        focusedErrorBorder: OutlineInputBorder(
                                                          borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        contentPadding: editTextPadding,
                                                        hintStyle: textFieldTodoCommentHintStyle,
                                                        hintText: addComment),
                                                    style: textFieldTodoCommentHintStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: SizedBox(
                                              width: 150,
                                              child: Text(upload, style: addRoleSubStyle),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: TextFormField(
                                                maxLines: 1,
                                                controller: resumeUploadController,
                                                textInputAction: TextInputAction.done,
                                                obscureText: false,
                                                decoration: InputDecoration(
                                                  fillColor: CupertinoColors.white,
                                                  filled: true,
                                                  border: border,
                                                  isDense: true,
                                                  enabledBorder: border,
                                                  focusedBorder: border,
                                                  errorBorder: errorBorder,
                                                  focusedErrorBorder: errorBorder,
                                                  contentPadding: editTextPadding,
                                                  hintStyle: textFieldHintStyle,
                                                  hintText: "Browse",
                                                  suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                  suffixIcon: Padding(
                                                      child: Image.asset("assets/images/upload.png", color: buttonBg),
                                                      padding: dateUploadIconsPadding),
                                                ),
                                                style: textFieldStyle,
                                                onTap: () async {
                                                  try {
                                                    supportingDocs = [];
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles(
                                                        type: FileType.any,
                                                        //allowedExtensions: ['pdf', 'doc', 'docx', 'docs'],
                                                        allowMultiple: true,
                                                        onFileLoading: (FilePickerStatus status) => {
                                                              if (status == FilePickerStatus.picking)
                                                                {
                                                                  showDialog(
                                                                      context: context,
                                                                      barrierDismissible: false,
                                                                      builder: (BuildContext context) {
                                                                        return loader();
                                                                      })
                                                                }
                                                              else if (status == FilePickerStatus.done)
                                                                {Navigator.of(context).pop()}
                                                              else
                                                                {Navigator.of(context).pop()}
                                                            });
                                                    if (result != null) {
                                                      List<PlatformFile?> files = result.files.map((path) => path).toList();
                                                      List<String?> fileNames = [];
                                                      // debugPrint('length--- ${files.length}');
                                                      for (var element in files) {
                                                        // debugPrint("==== ${element!.name}");
                                                        String base64string = base64Encode(element!.bytes!);
                                                        // debugPrint("=== $base64string");
                                                        fileNames.add(element.name);
                                                        supportingDocs.add(base64string);
                                                      }
                                                      setState(() {
                                                        resumeUploadController.text = fileNames.join(', ');
                                                      });
                                                    } else {
                                                      Navigator.of(context).pop();
                                                    }
                                                  } on PlatformException catch (e) {
                                                    debugPrint(e.toString());
                                                  } catch (e) {
                                                    debugPrint(e.toString());
                                                  }
                                                },
                                                enableInteractiveSelection: false,
                                                focusNode: FocusNode(),
                                                readOnly: true,
                                                validator: emptyTextValidator),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) {
                                                return loader();
                                              });
                                          await viewModel
                                              .addSystemTestAPI(employeeId, employeeId, widget.profileId!, commentTextController.text.trim(),
                                                  "$sTime,$sDate", supportingDocs)
                                              .then((value) {
                                            Navigator.of(context).pop();
                                            if (viewModel.addSystemTestResponse != null) {
                                              if (viewModel.addSystemTestResponse!.responseStatus == 1) {
                                                Navigator.of(context).pop();
                                                showToast(viewModel.addSystemTestResponse!.result!);
                                                getProfileData();
                                              } else {
                                                showToast(viewModel.addSystemTestResponse!.result!);
                                              }
                                            } else {
                                              showToast(pleaseTryAgain);
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(schedule, style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    ).then((value) {
      setState(() {
        commentFieldVisible = true;
      });
    });
  }
  getProfileData() {
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
    });
    viewModel.viewProfileAPI(widget.profileId!).then((value) {
      Navigator.of(context).pop();
    });
  }
}
