import 'package:aem/model/all_stages_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/get_multiple_sprints_data_model.dart';
import 'package:aem/model/project_bug_list_response.dart';
import 'package:aem/model/project_details_response_model.dart';
import 'package:aem/model/project_logs_response_model.dart';
import 'package:aem/model/project_wise_modules_tasks_for_sprints.dart';
import 'package:aem/model/task_detail_response_model.dart';
import 'package:aem/model/task_duration_track_response.dart';
import 'package:aem/model/tasks_based_on_status_response_model.dart';
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/project_details/ui/sprints_screen.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../model/task_logs_response_model.dart';

class ProjectDetailsNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  ProjectDetailsResponseModel? _projectDetailsResponseModel;

  ProjectDetailsResponseModel? get projectDetailsResponseModel {
    return _projectDetailsResponseModel;
  }

  List<Logslist>? get getProjectLogs {
    List<Logslist>? list = [];
    if (projectDetailsResponseModel != null) {
      if (projectDetailsResponseModel!.responseStatus == 1) {
        if (projectDetailsResponseModel!.projectDict != null) {
          if (projectDetailsResponseModel!.projectDict!.logslist!.isNotEmpty) {
            if (getAllProjectLogs!.isEmpty) {
              // latest logs
              list = projectDetailsResponseModel!.projectDict!.logslist!;
            } else {
              // all logs
              list = getAllProjectLogs!;
            }
          }
        }
      }
    }
    return list;
  }

  Future<ProjectDetailsResponseModel?> projectViewAPI(String employeeId, String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    _projectDetailsResponseModel = ProjectDetailsResponseModel();
    _projectLogsResponseModel = ProjectLogsResponseModel();

    try {
      dynamic response = await Repository().projectView(employeeId, projectId);
      if (response != null) {
        _projectDetailsResponseModel = ProjectDetailsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectDetailsResponseModel");
    notifyListeners();
    return _projectDetailsResponseModel;
  }

  ProjectLogsResponseModel? _projectLogsResponseModel;

  ProjectLogsResponseModel? get projectLogsResponseModel {
    return _projectLogsResponseModel;
  }

  List<Logslist>? get getAllProjectLogs {
    List<Logslist>? list = [];
    if (projectLogsResponseModel != null) {
      if (projectLogsResponseModel!.responseStatus == 1) {
        if (projectLogsResponseModel!.logslist!.isNotEmpty) {
          list = projectLogsResponseModel!.logslist!;
        }
      }
    }
    return list;
  }

  Future<ProjectLogsResponseModel?> projectLogsListAPI(String employeeId, String projectId, String searchType, String searchElement) async {
    _isFetching = true;
    _isHavingData = false;
    _projectLogsResponseModel = ProjectLogsResponseModel();

    try {
      dynamic response = await Repository().projectLogsList(employeeId, projectId, searchType, searchElement);
      if (response != null) {
        _projectLogsResponseModel = ProjectLogsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectLogsResponseModel");
    notifyListeners();
    return _projectLogsResponseModel;
  }

  resetProjectDetails() {
    _projectDetailsResponseModel = ProjectDetailsResponseModel();
    _projectLogsResponseModel = ProjectLogsResponseModel();
  }

  // tasks apis
  Future<BaseResponse?> addTaskAPI(String name, String description, String addedBy, String startDate, String endDate, String notes, String projectId,
      String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationTime) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();
    try {
      dynamic response = await Repository()
          .addTask(name, description, addedBy, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester,estimationTime);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateTaskAPI(String taskId, String name, String description, String addedBy, String startDate, String endDate, String notes,
      String projectId, String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationtime) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateTask(
          taskId, name, description, addedBy, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester,estimationtime);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<TaskResponseModel?> viewTaskAPI(String taskId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    TaskResponseModel? _taskResponse = TaskResponseModel();

    try {
      dynamic response = await Repository().viewTask(taskId, employeeId);
      if (response != null) {
        _taskResponse = TaskResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_taskResponse");
    notifyListeners();
    return _taskResponse;
  }

  Future<BaseResponse?> deleteTaskAPI(String taskId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _deleteTaskResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteTask(taskId, employeeId);
      if (response != null) {
        _deleteTaskResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteTaskResponse");
    notifyListeners();
    return _deleteTaskResponse;
  }

  Future<BaseResponse?> updateTaskEstimationTimeApi(String employeeId, List taskEstimations) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _deleteTaskResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateTaskEstimationTimeApi(employeeId, taskEstimations);
      if (response != null) {
        _deleteTaskResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteTaskResponse");
    notifyListeners();
    return _deleteTaskResponse;
  }

  Future<BaseResponse?> addCommentToTaskAPI(String taskId, String comment, String moduleId, String projectId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().addCommentToTask(taskId, comment, moduleId, projectId, employeeId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> updateTaskCommentAPI(String taskCommentId, String comment) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateTaskComment(taskCommentId, comment);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> deleteTaskCommentAPI(String taskCommentId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteTaskComment(taskCommentId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<TaskListStatusBasedResponseModel?> getTasksBasedOnStatusAPI(String projectId, String employeeId, String statusText) async {
    _isFetching = true;
    _isHavingData = false;
    TaskListStatusBasedResponseModel? _tasksResponse = TaskListStatusBasedResponseModel();

    try {
      dynamic response = await Repository().getTasksBasedOnStatus(projectId, employeeId, statusText);
      if (response != null) {
        _tasksResponse = TaskListStatusBasedResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_tasksResponse");
    notifyListeners();
    return _tasksResponse;
  }

  Future<BaseResponse?> updateTasksBasedOnStatusAPI(String employeeId, String taskId, String statusText,String comment,int percentageComplete) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _tasksResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateTasksBasedOnStatus(employeeId, taskId, statusText,comment,percentageComplete);
      if (response != null) {
        _tasksResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_tasksResponse");
    notifyListeners();
    return _tasksResponse;
  }

  // todos apis
  Future<BaseResponse?> createTodoAPI(String name, String description, String startDate, String endDate, String notes, String projectId,
      String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response =
          await Repository().createModule(name, description, startDate, endDate, notes, projectId, createdBy, assignedTo, notifyTo, assignedToTester);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<BaseResponse?> updateTodoAPI(String name, String description, String startDate, String endDate, String notes, String projectId,
      String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository()
          .updateModule(name, description, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

  Future<TodosListResponseModel?> viewAllTodosAPI(String employeeId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    TodosListResponseModel? _todosListResponse = TodosListResponseModel();

    try {
      dynamic response = await Repository().viewAllModules(employeeId, createdBy);
      if (response != null) {
        _todosListResponse = TodosListResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_todosListResponse");
    notifyListeners();
    return _todosListResponse;
  }

  Future<TodosListResponseModel?> viewModuleAPI(String moduleId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    TodosListResponseModel? _todosListResponse = TodosListResponseModel();

    try {
      dynamic response = await Repository().viewModule(moduleId, createdBy);
      if (response != null) {
        _todosListResponse = TodosListResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_todosListResponse");
    notifyListeners();
    return _todosListResponse;
  }

  Future<BaseResponse?> deleteModuleAPI(String moduleId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _deleteModuleResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteModule(moduleId, employeeId);
      if (response != null) {
        _deleteModuleResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteModuleResponse");
    notifyListeners();
    return _deleteModuleResponse;
  }

  Future<BaseResponse?> addCommentToModuleAPI(String comment, String moduleId, String projectId, String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().addCommentToModule(comment, moduleId, projectId, employeeId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> updateModuleCommentAPI(String moduleCommentId, String comment) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateModuleComment(moduleCommentId, comment);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> deleteModuleCommentAPI(String moduleCommentId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteModuleComment(moduleCommentId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> addaTestCaseAPI(String title, String description, String steps, String expectedresults,String actualResults, String employeeId, String projectId,
      String moduleId, String taskId, String tcStatus) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response =
          await Repository().addaTestCase(title, description, steps, expectedresults, actualResults, employeeId, projectId, moduleId, taskId, tcStatus);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> updateTestCaseAPI(
      String testcaseid, String title, String description, String steps, String expectedresults,String actualResults, String employeeId, String tcStatus) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateaTestCase(testcaseid, title, description, steps, expectedresults, actualResults, employeeId, tcStatus);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> deleteTestCaseAPI(String employeeId, String testCaseId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _deleteTestCaseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteTestCase(employeeId, testCaseId);
      if (response != null) {
        _deleteTestCaseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteTestCaseResponse");
    notifyListeners();
    return _deleteTestCaseResponse;
  }

  Future<BaseResponse?> raiseaBugAPI(String taskId, String employeeId, String projectId, String moduleId, String title, attachments,
      String description, String bugStatus, String priority, List assignedEmployeesList,String stageId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository()
          .raiseaBug(taskId, employeeId, projectId, moduleId, title, attachments, description, bugStatus, priority, assignedEmployeesList,stageId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<BaseResponse?> updateraiseaBugAPI(
      String title, String bugid, String employeeId, String description, String bugStatus, String priority, List assignedEmployeesList, attachments,String stageId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _commentResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateraiseaBug(title, bugid, employeeId, description, bugStatus, priority, assignedEmployeesList,attachments,stageId);
      if (response != null) {
        _commentResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_commentResponse");
    notifyListeners();
    return _commentResponse;
  }

  Future<ProjectBugsListResponse?> getbugslistbyprojectAPI(String employeeId, String projectId, String bugStatus, String title,String priority,String devStageId,String createdBy,List assignedTo) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectBugsListResponse? _projectBugsListResponse = ProjectBugsListResponse();

    try {
      dynamic response = await Repository().getbugslistbyproject(employeeId, projectId, bugStatus, title,priority,devStageId,createdBy,assignedTo);
      if (response != null) {
        _projectBugsListResponse = ProjectBugsListResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectBugsListResponse");
    notifyListeners();
    return _projectBugsListResponse;
  }

  Future<ProjectBugsListResponse?> viewBugAPI(String employeeId, String bugId) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectBugsListResponse? _projectBugsListResponse = ProjectBugsListResponse();

    try {
      dynamic response = await Repository().viewBug(employeeId, bugId);
      if (response != null) {
        _projectBugsListResponse = ProjectBugsListResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectBugsListResponse");
    notifyListeners();
    return _projectBugsListResponse;
  }

  Future<BaseResponse?> deleteBugAPI(String employeeId, String bugId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _deleteBugResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteBug(employeeId, bugId);
      if (response != null) {
        _deleteBugResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteBugResponse");
    notifyListeners();
    return _deleteBugResponse;
  }

  Future<TaskLogsListResponseModel?> taskLogsListAPI(String employeeId, String taskId, String searchType, String searchElement) async {
    _isFetching = true;
    _isHavingData = false;
    TaskLogsListResponseModel _projectLogsResponseModel = TaskLogsListResponseModel();

    try {
      dynamic response = await Repository().taskLogsList(employeeId, taskId, searchType, searchElement);
      if (response != null) {
        _projectLogsResponseModel = TaskLogsListResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_projectLogsResponseModel");
    notifyListeners();
    return _projectLogsResponseModel;
  }

  TaskDurationTrackResponse? _taskDurationTrackResponse;

  List<TaskDurationData> taskDurationList() {
    List<TaskDurationData> list = [];
    if (_taskDurationTrackResponse != null) {
      if (_taskDurationTrackResponse!.responseStatus == 1) {
        if (_taskDurationTrackResponse!.tasksData!.isNotEmpty) {
          list.addAll(_taskDurationTrackResponse!.tasksData!);
        }
      }
    }
    return list;
  }

  List<TaskDurationData> bugsDurationList() {
    List<TaskDurationData> list = [];
    if (_taskDurationTrackResponse != null) {
      if (_taskDurationTrackResponse!.responseStatus == 1) {
        if (_taskDurationTrackResponse!.bugsData!.isNotEmpty) {
          list.addAll(_taskDurationTrackResponse!.bugsData!);
        }
      }
    }
    return list;
  }

  Future<TaskDurationTrackResponse?> taskDurationTrackingAPI(String employeeId, String taskId) async {
    _isFetching = true;
    _isHavingData = false;

    try {
      dynamic response = await Repository().taskDurationTracking(employeeId, taskId);
      if (response != null) {
        _taskDurationTrackResponse = TaskDurationTrackResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_taskDurationTrackResponse");
    notifyListeners();
    return _taskDurationTrackResponse;
  }

  Future<BaseResponse?> taskDurationUpdateAPI(String employeeId, String activityId, String startTime, String endTime) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse _baseResponse = BaseResponse();
    try {
      dynamic response = await Repository().taskDurationUpdate(employeeId, activityId, startTime, endTime);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<BaseResponse?> addSprintAPI(String userid, String sprintname, String sprintdesc, String sprintStartdate, String sprintEnddate, String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _sprintResponse = BaseResponse();

    try {
      dynamic response =
      await Repository().addSprint(userid, sprintname, sprintdesc, sprintStartdate, sprintEnddate, projectId);
      if (response != null) {
        _sprintResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_sprintResponse");
    notifyListeners();
    return _sprintResponse;
  }
  Future<ProjectBasedModulesTasksforSprints?> getproject_wise_modules_tasksAPI(String sprintId,String employeeId, String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    ProjectBasedModulesTasksforSprints? _taskstoaddsprintsListResponse = ProjectBasedModulesTasksforSprints();

    try {
      dynamic response = await Repository().getProjectModulewisetasksprints(sprintId, employeeId,projectId);
      if (response != null) {
        _taskstoaddsprintsListResponse = ProjectBasedModulesTasksforSprints.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_taskstoaddsprintsListResponse");
    notifyListeners();
    return _taskstoaddsprintsListResponse;
  }

  Future<MultipleSprintsDataModel?> get_multiple_sprints_dataAPI(String employeeId, String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    MultipleSprintsDataModel? _getmultiplesprintsListResponse = MultipleSprintsDataModel();

    try {
      dynamic response = await Repository().getMultipleSprintsdata(employeeId,projectId);
      if (response != null) {
        _getmultiplesprintsListResponse = MultipleSprintsDataModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_getmultiplesprintsListResponse");
    notifyListeners();
    return _getmultiplesprintsListResponse;
  }

  Future<BaseResponse?> assign_tasks_to_sprintAPI( String sprintId,String employeeid, String projectId, List<TaskID> taskslistid) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _assigntasksprintResponse = BaseResponse();

    try {
      dynamic response =
      await Repository().assign_tasks_to_sprint(sprintId,employeeid, projectId, taskslistid);
      if (response != null) {
        _assigntasksprintResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_assigntasksprintResponse");
    notifyListeners();
    return _assigntasksprintResponse;
  }
  Future<BaseResponse?> updateBugStatusAPI(String bugId, String employeeId, String bugStatus) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _bugResponse = BaseResponse();

    try {
      dynamic response = await Repository().updateBugStatus(bugId, employeeId, bugStatus);
      if (response != null) {
        _bugResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_bugResponse");
    notifyListeners();
    return _bugResponse;
  }

  Future<BaseResponse?> deleteSprintApi(String sprintId, String employeeId, String projectId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _bugResponse = BaseResponse();

    try {
      dynamic response = await Repository().deleteSprintApi(sprintId,  employeeId,  projectId);
      if (response != null) {
        _bugResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_bugResponse");
    notifyListeners();
    return _bugResponse;
  }
  AllStagesResponseModel? _allStagesResponseModel = AllStagesResponseModel();
  List<DevelopmentStagesList> developmentStagesList() {
    List<DevelopmentStagesList> list = [];
    if (_allStagesResponseModel != null) {
      if (_allStagesResponseModel!.responseStatus == 1) {
        if (_allStagesResponseModel!.developmentStagesList!.isNotEmpty) {
          list.addAll(_allStagesResponseModel!.developmentStagesList!);
        }
      }
    }
    return list;
  }
  Future<AllStagesResponseModel?> viewactivedevelopmentstagesApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;

    try {
      dynamic response = await Repository().viewactivedevelopmentstagesApi(employeeId);
      if (response != null) {
        _allStagesResponseModel = AllStagesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allStagesResponseModel");
    notifyListeners();
    return _allStagesResponseModel;
  }

  Future<TaskResponseModel?> getBugsListApi(String employeeId,String taskId, String priority,String devStageId) async {
    _isFetching = true;
    _isHavingData = false;
    TaskResponseModel _taskResponseModel = TaskResponseModel();

    try {
      dynamic response = await Repository().getBugsListApi(employeeId, taskId,  priority, devStageId);
      if (response != null) {
        _taskResponseModel = TaskResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_taskResponseModel");
    notifyListeners();
    return _taskResponseModel;
  }
}
