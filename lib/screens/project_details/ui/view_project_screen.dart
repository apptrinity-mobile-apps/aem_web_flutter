import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/project_details_response_model.dart';
import 'package:aem/model/project_item.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/bugs_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/completed_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/on_going_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/over_due_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/new_tasks_screen.dart';
import 'package:aem/screens/project_details/ui/sprints_screen.dart';
import 'package:aem/screens/project_details/ui/timeestimation_screen.dart';
import 'package:aem/screens/project_details/ui/todos_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';

class ViewProjectDetailedScreen extends StatefulWidget {
  final String? projectId;

  const ViewProjectDetailedScreen({this.projectId, Key? key}) : super(key: key);

  @override
  _ViewProjectDetailedScreenState createState() => _ViewProjectDetailedScreenState();
}

class _ViewProjectDetailedScreenState extends State<ViewProjectDetailedScreen> {
  List<ProjectItem> projectItems = [];
  late ProjectDetailsNotifier viewModel;
  String employeeId = "", projectName = "", projectDescription = "";
  late List<ProjectEmployees> projectEmployees;
  List<EmployeeDetails> employeesList = [];
  bool showViewMoreButton = true;
  late Widget animatedButton;
  TextEditingController taskCommentController = TextEditingController();

  @override
  void initState() {
    projectItems.add(ProjectItem(count: "0", title: projectToDo, description: descriptionTodos, icon: "assets/images/project_todo.png"));
    projectItems.add(ProjectItem(count: "0", title: projectNew, description: descriptionTasks, icon: "assets/images/project_new.png"));
    projectItems.add(ProjectItem(count: "0", title: projectOnGoing, description: descriptionOnGoing, icon: "assets/images/project_on_going.png"));
    projectItems.add(ProjectItem(count: "0", title: projectOverdue, description: descriptionOverdue, icon: "assets/images/project_overdue.png"));
    projectItems
        .add(ProjectItem(count: "0", title: projectCompleted, description: descriptionCompleted, icon: "assets/images/project_completed.png"));
    projectItems.add(ProjectItem(count: "0", title: bugs, description: descriptionBugs, icon: "assets/images/project_bug.png"));
    projectItems.add(
        ProjectItem(count: "0", title: projectTaskTimeEstimation, description: projectDescriptions, icon: "assets/images/project_estimation.png"));
    projectItems.add(ProjectItem(count: "0", title: projectTaskSprints, description: projectDescriptions, icon: "assets/images/project_sprints.png"));
    projectEmployees = [];
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    viewModel.resetProjectDetails();
    animatedButton = showMore();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getEmployeesData();
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), body()],
                      ),
                    )
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }

  Widget body() {
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: true);
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $projectName", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(5, 15, 5, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: SingleChildScrollView(
                                controller: ScrollController(),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 25, 20, 0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: GridView.builder(
                                            shrinkWrap: true,
                                            controller: ScrollController(),
                                            physics: const ScrollPhysics(),
                                            scrollDirection: Axis.vertical,
                                            itemCount: projectItems.length,
                                            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                                                childAspectRatio: 1.8, mainAxisSpacing: 8, crossAxisSpacing: 8, maxCrossAxisExtent: 500),
                                            itemBuilder: (BuildContext context, int index) {
                                              return projectPlansList(index);
                                            }),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 30, 0, 10),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(projectActivity, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                                                  Padding(
                                                    padding: const EdgeInsets.only(bottom: 3),
                                                    child: Text(descriptionProjectActivity, softWrap: true, style: fragmentDescStyle),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      viewModel.getProjectLogs!.isNotEmpty
                                          ? Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                ListView.builder(
                                                    shrinkWrap: true,
                                                    physics: const ScrollPhysics(),
                                                    itemCount: viewModel.getProjectLogs!.length,
                                                    itemBuilder: (context, index) {
                                                      var alternate = index % 2;

                                                      return projectLogs(viewModel.getProjectLogs![index], alternate);
                                                    }),
                                                Visibility(
                                                  visible: showViewMoreButton,
                                                  child: AnimatedSwitcher(
                                                    duration: const Duration(milliseconds: 500),
                                                    transitionBuilder: (Widget child, Animation<double> animation) {
                                                      return ScaleTransition(scale: animation, child: child);
                                                    },
                                                    child: animatedButton,
                                                  ),
                                                )
                                              ],
                                            )
                                          : Center(
                                              child: Padding(
                                                padding: const EdgeInsets.only(top: 100, bottom: 100),
                                                child: Text(noDataAvailable, style: logoutHeader),
                                              ),
                                            )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(projectName == "" ? 'Project' : projectName, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                    HtmlWidget(
                      projectDescription == "" ? description : parseHtmlString(projectDescription),
                      buildAsync: true,
                      enableCaching: false,
                      // factoryBuilder: () => SelectableTextCompanion(),
                      renderMode: RenderMode.column,
                      textStyle: fragmentDescStyle,
                    ),
                    // Text(projectDescription == "" ? description : projectDescription,
                    //     maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle)
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 40,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: projectEmployees.length > 5 ? 5 : projectEmployees.length,
                          itemBuilder: (context, index) {
                            return index == 0
                                ? Align(
                                    widthFactor: 1,
                                    child: Tooltip(
                                      message: projectEmployees[index].employeeName!,
                                      decoration: toolTipDecoration,
                                      textStyle: toolTipStyle,
                                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: CircleAvatar(
                                          backgroundColor: getNameBasedColor(projectEmployees[index].employeeName!.toUpperCase()[0]),
                                          child: Text(projectEmployees[index].employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : Align(
                                    widthFactor: 0.8,
                                    child: Tooltip(
                                      message: projectEmployees[index].employeeName!,
                                      decoration: toolTipDecoration,
                                      textStyle: toolTipStyle,
                                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: CircleAvatar(
                                          backgroundColor: getNameBasedColor(projectEmployees[index].employeeName!.toUpperCase()[0]),
                                          child: Text(projectEmployees[index].employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                                        ),
                                      ),
                                    ),
                                  );
                          }),
                    ),
                    /*Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () {
                          debugPrint("listtt -- $employeesList");
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(inviteSomePeople, style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                    )*/
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget projectPlansList(int position) {
    var alternate = position % 2;
    return OnHoverCard(builder: (isHovered) {
      return SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: isHovered ? 5 : 1,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0), side: BorderSide(color: isHovered ? buttonBg : dashBoardCardBorderTile, width: 1)),
          color: CupertinoColors.white,
          child: InkWell(
            onTap: () {
              if (projectItems[position].title! == projectToDo) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => TodosScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectNew) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => NewTasksScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectOnGoing) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => OnGoingTasksScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectOverdue) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => OverDueTasksScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectCompleted) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => CompletedTasksScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == bugs) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => BugsTasksScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectTaskTimeEstimation) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => TimeEstimationScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              } else if (projectItems[position].title! == projectTaskSprints) {
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => SprintsScreen(projectId: widget.projectId, projectName: projectName),
                    transitionDuration: const Duration(seconds: 0),
                    reverseTransitionDuration: const Duration(seconds: 0),
                  ),
                ).then((value) {
                  reload();
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      color: alternate == 0 ? planColor1 : planColor2,
                      height: ScreenConfig.height(context),
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Image.asset(projectItems[position].icon!, height: 40, width: 40),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                              child: Text(
                                  projectItems[position].title == projectTaskTimeEstimation
                                      ? projectItems[position].count!
                                      : int.parse(projectItems[position].count!) > 99
                                          ? '99+'
                                          : projectItems[position].count!,
                                  style: projectCountStyle),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(projectItems[position].title!, style: projectTextStyle),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                              child: Text(projectItems[position].description!, style: projectDescriptionStyle),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget projectLogs(Logslist list, int alignPosition) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          child: Text(list.day!, style: projectDateStyle),
        ),
        list.logsData!.isNotEmpty
            ? ListView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemCount: list.logsData!.length,
                itemBuilder: (context, index) {
                  return activitiesList(index, list.logsData![index], alignPosition);
                })
            : const SizedBox()
      ],
    );
  }

  Widget activitiesList(int position, LogsData itemData, int alignPosition) {
    // var alternate = position % 2;
    return itemData.moduleId == ""
        ? SizedBox(
            width: ScreenConfig.width(context),
            height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                    color: Colors.white,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(text: itemData.projectName, style: projectAddedByNameStyle),
                        TextSpan(text: ', created by ', style: projectAddedByStyle),
                        TextSpan(text: '${itemData.employeeName}.', style: projectAddedByNameStyle)
                      ]),
                    ),
                  ),
                ),
              ],
            ),
          )
        : alignPosition == 0
            ? IntrinsicHeight(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                        color: Colors.white,
                        child: leftItem(itemData),
                      ),
                    ),
                    const VerticalDivider(thickness: 1.5, color: dashBoardCardBorderTile),
                    const Expanded(
                      child: SizedBox(),
                    ),
                  ],
                ),
              )
            : IntrinsicHeight(
                child: Row(
                  children: [
                    const Expanded(
                      child: SizedBox(),
                    ),
                    const VerticalDivider(thickness: 1.5, color: dashBoardCardBorderTile),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                        color: Colors.white,
                        child: rightItem(itemData),
                      ),
                    ),
                  ],
                ),
              );
  }

  Widget leftItem(LogsData itemData) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
            child: Tooltip(
              message: itemData.employeeName,
              decoration: toolTipDecoration,
              textStyle: toolTipStyle,
              preferBelow: false,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  backgroundColor: getNameBasedColor(itemData.employeeName!.toUpperCase()[0]),
                  child: Text(itemData.employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                ),
              ),
            ),
            alignment: Alignment.topLeft),
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(children: <TextSpan>[
                    /*TextSpan(text: 'on ', style: projectAddedByStyle),*/

                    if (itemData.logType == 'ems_bugs') ...[
                      TextSpan(
                          text: itemData.bugName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'bug', itemData.bugId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_tasks') ...[
                      TextSpan(
                          text: itemData.taskName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'task', itemData.taskId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_module') ...[
                      TextSpan(
                          text: itemData.moduleName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'module', itemData.moduleId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ],
                    TextSpan(text: ' ${itemData.description} by ', style: projectAddedByStyle),
                    TextSpan(
                        text: '${itemData.employeeName}',
                        style: projectAddedByNameStyle,
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'employee', itemData.employeeId!).then((value) {
                              setState(() {
                                showViewMoreButton = false;
                              });
                            });
                          }),

                    /* if (itemData.taskStatus == 0) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 1) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 2) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 3) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 4) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 5) ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ] else if (itemData.taskStatus == 6) ...[
                      TextSpan(text: ', ${itemData.employeeName} deleted', style: projectAddedByStyle)
                    ] else ...[
                      TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle)
                    ],*/
                  ]),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Row(
                    children: [
                      itemData.taskId != ""
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                taskCheckBox(itemData),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                                  child: Text(itemData.taskName!, textAlign: TextAlign.start, style: projectDateMenuStyle),
                                )
                              ],
                            )
                          : const SizedBox(),
                      Image.asset("assets/images/calendar.png", color: buttonBg, height: 20, width: 20),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: itemData.taskId != ""
                            ? Text("${itemData.taskStartDate!} - ${itemData.taskLastDate!}", style: projectDateMenuStyle)
                            : Text("${itemData.moduleStartDate!} - ${itemData.moduleEndDate!}", style: projectDateMenuStyle),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.topRight,
            child: Text(timeFormatHMS12hr(itemData.createdOn!), style: projectTimeStyle, maxLines: 1, overflow: TextOverflow.ellipsis),
          ),
        ),
      ],
    );
  }

  Widget rightItem(LogsData itemData) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.topLeft,
            child: Text(timeFormatHMS12hr(itemData.createdOn!), style: projectTimeStyle),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.centerRight,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                RichText(
                  textAlign: TextAlign.end,
                  text: TextSpan(children: <TextSpan>[
                    if (itemData.logType == 'ems_bugs') ...[
                      TextSpan(
                          text: itemData.bugName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'bug', itemData.bugId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_tasks') ...[
                      TextSpan(
                          text: itemData.taskName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'task', itemData.taskId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_module') ...[
                      TextSpan(
                          text: itemData.moduleName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'module', itemData.moduleId!).then((value) {
                                setState(() {
                                  showViewMoreButton = false;
                                });
                              });
                            }),
                    ],
                    TextSpan(text: ' ${itemData.description} by ', style: projectAddedByStyle),
                    TextSpan(
                        text: '${itemData.employeeName}',
                        style: projectAddedByNameStyle,
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            await viewModel.projectLogsListAPI(employeeId, widget.projectId!, 'employee', itemData.employeeId!).then((value) {
                              setState(() {
                                showViewMoreButton = false;
                              });
                            });
                          }),
                  ]),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Row(
                    children: [
                      itemData.taskId != ""
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                taskCheckBox(itemData),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                                  child: Text(itemData.taskName!, textAlign: TextAlign.start, style: projectDateMenuStyle),
                                )
                              ],
                            )
                          : const SizedBox(),
                      Image.asset("assets/images/calendar.png", color: buttonBg, height: 20, width: 20),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: itemData.taskId != ""
                            ? Text("${itemData.taskStartDate!} - ${itemData.taskLastDate!}", style: projectDateMenuStyle)
                            : Text("${itemData.moduleStartDate!} - ${itemData.moduleEndDate!}", style: projectDateMenuStyle),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
            margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
            child: Tooltip(
              message: itemData.employeeName,
              decoration: toolTipDecoration,
              textStyle: toolTipStyle,
              preferBelow: false,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  backgroundColor: getNameBasedColor(itemData.employeeName!.toUpperCase()[0]),
                  child: Text(itemData.employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                ),
              ),
            ),
            alignment: Alignment.topLeft),
      ],
    );
  }

  Widget taskCheckBox(LogsData itemData) {
    return Checkbox(
        visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
        activeColor: buttonBg,
        hoverColor: buttonBg.withOpacity(0.2),
        value: itemData.taskStatus == 3 ? true : false,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        side: const BorderSide(color: buttonBg),
        onChanged: (value) async {
          if (value!) {
            int involvedMins = Duration(seconds: itemData.involvedTime!).inMinutes;

            print("inv time - ${involvedMins} -- est time -- ${itemData.estimationTime}");
            if (itemData.estimationTime! < involvedMins) {
              taskCommentController.text = "";
              showTaskCommentDialog(itemData);
            } else {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return loader();
                  });
              await viewModel.updateTasksBasedOnStatusAPI(employeeId, itemData.taskId!, StatusText.completeStr.name!, '',0).then((value) {
                Navigator.of(context).pop();
                if (value != null) {
                  if (value.responseStatus == 1) {
                    getData();
                    showToast(value.result!);
                  } else {
                    showToast(value.result!);
                  }
                } else {
                  showToast(pleaseTryAgain);
                }
              });
            }
          } else {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return loader();
                });
            await viewModel.updateTasksBasedOnStatusAPI(employeeId, itemData.taskId!, StatusText.reopenStr.name!, '',0).then((value) {
              Navigator.of(context).pop();
              if (value != null) {
                if (value.responseStatus == 1) {
                  getData();
                  showToast(value.result!);
                } else {
                  showToast(value.result!);
                }
              } else {
                showToast(pleaseTryAgain);
              }
            });
          }
        });
  }

  Widget showMore() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: TextButton(
        onPressed: () async {
          setState(() {
            animatedButton = showLoading();
          });
          await viewModel.projectLogsListAPI(employeeId, widget.projectId!, '', '').then((value) {
            setState(() {
              showViewMoreButton = false;
            });
          });
        },
        child: const Padding(
          padding: EdgeInsets.fromLTRB(13, 7, 13, 7),
          child: Text(
            'View more',
            style: TextStyle(fontFamily: "Poppins", fontWeight: FontWeight.w500, color: buttonBg, fontSize: 14),
          ),
        ),
      ),
    );
  }

  Widget showLoading() {
    return const Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Center(
        child: SizedBox(
          height: 20,
          width: 20,
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  reload() {
    setState(() {
      showViewMoreButton = true;
      animatedButton = showMore();
      getData();
    });
  }

  showTaskCommentDialog(LogsData itemData) {
    final _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 700, maxWidth: 700, minHeight: 5 * 24, maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    validator: emptyTextValidator,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(hintText: reasonExtraTime),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await viewModel
                              .updateTasksBasedOnStatusAPI(employeeId, itemData.taskId!, StatusText.completeStr.name!, taskCommentController.text,0)
                              .then((value) {
                            Navigator.of(context).pop();
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                setState(() {});
                                showToast(value.result!);
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void getEmployeesData() {
    final employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: true);
    if (employeeViewModel.employeesResponse == null) {
      employeeViewModel.getEmployeesAPI("project");
      employeesList = employeeViewModel.getEmployees;
    } else {
      employeesList = employeeViewModel.getEmployees;
    }
  }

  void getData() {
    Future.delayed(const Duration(milliseconds: 0), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.projectViewAPI(employeeId, widget.projectId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            if (value.projectDict != null) {
              var project = value.projectDict!;
              for (var element in projectItems) {
                if (element.title! == projectToDo) {
                  element.count = project.todosCount.toString();
                } else if (element.title! == projectNew) {
                  element.count = project.newCount.toString();
                } else if (element.title! == projectOnGoing) {
                  element.count = project.ongoingCount.toString();
                } else if (element.title! == projectOverdue) {
                  element.count = project.overDueCount.toString();
                } else if (element.title! == projectCompleted) {
                  element.count = project.completeCount.toString();
                } else if (element.title! == projectTaskSprints) {
                  element.count = project.sprintsCount.toString();
                }else if (element.title! == projectReOpened) {
                  element.count = project.reopenCount.toString();
                } else if (element.title! == bugs) {
                  element.count = project.bugsCount.toString();
                } else if (element.title! == projectTaskTimeEstimation) {
                  element.count = durationToString(project.totalEstimateHoursCount!).toString() + " hrs";

                  // element.count = project.totalEstimateHoursCount.toString() + " minutes";

                  //element.count = project.totalEstimateHoursCount;
                }
              }

              projectName = project.name!;
              projectDescription = project.description!;
              projectEmployees = project.employees!;
            }
            setState(() {});
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
}
