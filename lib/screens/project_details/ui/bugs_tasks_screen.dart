import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_stages_response_model.dart';
import 'package:aem/model/project_bug_list_response.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/app_scroll_behaviour.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/image_carousel_dialog.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BugsTasksScreen extends StatefulWidget {
  final String? projectId, projectName;

  const BugsTasksScreen({required this.projectId, required this.projectName, Key? key}) : super(key: key);

  @override
  _BugsTasksScreenState createState() => _BugsTasksScreenState();
}

class _BugsTasksScreenState extends State<BugsTasksScreen> {
  late ProjectDetailsNotifier viewModel;
  String employeeId = "";
  TextEditingController nameController = TextEditingController();
  String statusInitialValue = 'Status';
  final _statusKey = GlobalKey<FormFieldState>();
  final _priorityBugKey = GlobalKey<FormFieldState>();
  String priorityInitialValue = '';
  final _stageBugKey = GlobalKey<FormFieldState>();
  final _createEmployeeKey = GlobalKey<FormFieldState>();
  final _assainEmployeeKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  List employeeIds = [];
  DevelopmentStagesList stageBugInitialValue = DevelopmentStagesList(stage: selectStage, id: 'none');
  List<DropdownMenuItem<DevelopmentStagesList>> developmentStagesListMenuItems = [];
  List<DevelopmentStagesList> stagesList = [];
  late EmployeeNotifier employeeViewModel = EmployeeNotifier();
  List<EmployeeDetails> employeesList = [], selectedEmployees = [];
  List<DropdownMenuItem<EmployeeDetails>> employeeListMenuItems = [];
  EmployeeDetails employeeInitialValue = EmployeeDetails(name: selectEmployee, id: 'none');
  @override
  void initState() {
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    developmentStagesListMenuItems.add(DropdownMenuItem(
        child: const Text(selectStage), value: stageBugInitialValue));
    employeeListMenuItems.add(DropdownMenuItem(
        child: const Text(selectEmployee), value: employeeInitialValue));
    employeesList.add(EmployeeDetails(name: assainEmployee));
    getData();
    super.initState();
  }
  void getData() {
    Future.delayed(const Duration(milliseconds: 00), () async {

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });


      await viewModel.viewactivedevelopmentstagesApi(employeeId).then((value) {
        Navigator.pop(context);
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {
              stagesList = viewModel.developmentStagesList();
              for (var element in stagesList) {
                developmentStagesListMenuItems.add(DropdownMenuItem(
                    child: Text(element.stage!), value: element));
              }
            } else {
              showToast(value.result!);
            }
          }
        });
      });

      await employeeViewModel.getEmployeesAPI("project").then((value) {
        // Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.employeeDetails!.isNotEmpty) {
                employeesList = value.employeeDetails!;
                for (var element in employeesList) {
                  employeeListMenuItems.add(DropdownMenuItem(
                      child: Text(element.name!), value: element));
                }
              }
            });
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });


    });
  }
  Future getAllBugs() {
    debugPrint("--- $employeeId, ${widget.projectId!}, $statusInitialValue, ${nameController.text.toString()}");


    String statusValue;
    String priority;
    String stageId;
    String createEmployeeId;
    if(statusInitialValue == "Status"){
      statusValue = '';
    }else{
      statusValue = statusInitialValue;
    }

    if(priorityInitialValue == ''){
      priority = '';
    }else{
      priority = priorityInitialValue;
    }
    if(stageBugInitialValue.id == 'none'){
      stageId = '';
    }else{
      stageId = stageBugInitialValue.id!;
    }
    if(employeeInitialValue.id == 'none'){
      createEmployeeId = '';
    }else{
      createEmployeeId = employeeInitialValue.id!;
    }

    for (var element in selectedEmployees) {
      employeeIds.add(element.id!);
    }

    return viewModel.getbugslistbyprojectAPI(employeeId, widget.projectId!, statusValue, nameController.text.toString(),priority,stageId,createEmployeeId,employeeIds);





   /* if (nameController.text.trim() != "" && statusInitialValue == "Status") {
      debugPrint("--- name");
      return viewModel.getbugslistbyprojectAPI(employeeId, widget.projectId!, "", nameController.text.toString());
    } else if (nameController.text.trim() == "" && statusInitialValue != "Status") {
      debugPrint("--- dropdown");
      return viewModel.getbugslistbyprojectAPI(employeeId, widget.projectId!, statusInitialValue, "");
    } else if (nameController.text.trim() != "" && statusInitialValue != "Status") {
      debugPrint("--- all");
      return viewModel.getbugslistbyprojectAPI(employeeId, widget.projectId!, statusInitialValue, nameController.text.trim().toString());
    } else {
      debugPrint("--- none");
      return viewModel.getbugslistbyprojectAPI(employeeId, widget.projectId!, "", "");
    }*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), body()],
                      ),
                    ),
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > ${widget.projectName} > $bugs", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                              child: Wrap(
                                spacing: 5,
                                runSpacing: 10,
                                alignment: WrapAlignment.start,
                                children: [
                                  nameField(),
                                  statusField(),
                                  priorField(),
                                  stageField(),
                                  createEmployeeField(),
                                  assainEmployeeField(),
                                  OnHoverCard(builder: (isHovered) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5), color: isHovered ? buttonBg : buttonBg.withOpacity(0.1)),
                                      child: InkWell(
                                        onTap: () {
                                         // if (nameController.text.trim() != "" || statusInitialValue != "Status") {
                                            setState(() {});
                                         // }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                                          child: Text(
                                            filter,
                                            style: TextStyle(
                                                letterSpacing: 0.9,
                                                fontFamily: "Poppins",
                                                fontWeight: FontWeight.w500,
                                                color: isHovered ? CupertinoColors.white : buttonBg),
                                          ),
                                        ),
                                      ),
                                    );
                                  }),
                                  nameController.text.trim() != "" || priorityInitialValue != '' || statusInitialValue != "Status"  || stageBugInitialValue.id != 'none' || employeeIds.length > 0 || employeeInitialValue.id != 'none'
                                      ? Padding(
                                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          child: OnHoverCard(builder: (isHovered) {
                                            return Container(
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: isHovered ? dashBoardCardOverDueText : dashBoardCardOverDueText.withOpacity(0.1)),
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    // resetting filter
                                                    nameController.text = "";
                                                    statusInitialValue = "Status";
                                                    employeeIds = [];
                                                    _stageBugKey.currentState!.reset();
                                                    _priorityBugKey.currentState!.reset();
                                                    _statusKey.currentState!.reset();
                                                    _createEmployeeKey.currentState!.reset();
                                                    _assainEmployeeKey.currentState!.clear();
                                                    employeeInitialValue = EmployeeDetails(name: selectEmployee, id: 'none');
                                                    stageBugInitialValue = DevelopmentStagesList(stage: selectStage, id: 'none');
                                                    priorityInitialValue = '';
                                                  });
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                                                  child: Text(
                                                    reset,
                                                    style: TextStyle(
                                                        letterSpacing: 0.9,
                                                        fontFamily: "Poppins",
                                                        fontWeight: FontWeight.w500,
                                                        color: isHovered ? CupertinoColors.white : dashBoardCardOverDueText),
                                                  ),
                                                ),
                                              ),
                                            );
                                          }),
                                        )
                                      : const SizedBox(),
                                ],
                              ),
                            ),
                            Flexible(child: Container(margin: const EdgeInsets.fromLTRB(0, 10, 0, 10), child: projectBugsList())),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${widget.projectName}", maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Text(" (Bugs List)", maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                ],
              ),
            ),
          ],
        ),
        Text(descriptionBugs, softWrap: true, style: fragmentDescStyle),
      ],
    );
  }

  Widget projectBugsList() {
    return FutureBuilder(
        future: getAllBugs(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loader());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(child: Text('error ${snapshot.error}', style: logoutHeader));
            } else if (snapshot.hasData) {
              var data = snapshot.data as ProjectBugsListResponse;
              debugPrint(data.bugsList!.length.toString());
              if (data.responseStatus == 1) {
                if (data.bugsList!.isNotEmpty) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                      child: DataTable2(
                          columnSpacing: 14,
                          dividerThickness: 1,
                          decoration: BoxDecoration(border: Border.all(color: sideMenuSelected, width: 1)),
                          headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                          headingRowHeight: 46,
                          // dataRowHeight: 80,
                          smRatio: 0.6,
                          lmRatio: 1.5,
                          columns: <DataColumn2>[
                            DataColumn2(
                              label: Text(tableSNo, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                              size: ColumnSize.S,
                            ),
                            DataColumn2(
                              label: Text(tableBugsTitle, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                              size: ColumnSize.L,
                            ),
                            DataColumn2(
                              label: Text(tableTask, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                              size: ColumnSize.L,
                            ),
                            DataColumn2(
                              label: Text(tableModuleName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                              size: ColumnSize.L,
                            ),
                            DataColumn2(
                              label: Text(tableCreatedBy, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                              size: ColumnSize.L,
                            ),
                            DataColumn2(
                              label: Text(stage, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
                              size: ColumnSize.M,
                            ),
                            DataColumn2(
                              label: Text(priority, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
                              size: ColumnSize.M,
                            ),
                            DataColumn2(
                              label: Text(tableStatus, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                              size: ColumnSize.M,
                            ),
                            DataColumn2(
                              label: Text(tableActions, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
                              size: ColumnSize.M,
                            ),
                          ],
                          rows: data.bugsList!.map<DataRow2>((e) {
                            return DataRow2(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      pageBuilder: (context, animation1, animation2) =>
                                          ViewTaskScreen(
                                              projectId: e.projectId!,
                                              moduleId: e.moduleId!,
                                              taskId: e.taskId!),
                                      transitionDuration: const Duration(seconds: 0),
                                      reverseTransitionDuration: const Duration(seconds: 0),
                                    ),
                                  ).then((value) {
                                    getAllBugs();
                                  });
                                },
                                specificRowHeight: 60,
                                color: MaterialStateColor.resolveWith(
                                    (states) => /*data.profilesList!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1*/ CupertinoColors.white),
                                cells: [
                                  DataCell(
                                    Text((data.bugsList!.indexOf(e) + 1).toString(),
                                        softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.title!, softWrap: true, style: projectAddedByStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.taskName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.moduleName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text("${e.employeeName!}\n${e.createdOn!}",
                                        softWrap: true,
                                        style: listItemsStyle,
                                        textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.devStage!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.priority!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Text(e.bugStatus!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
                                  ),
                                  DataCell(
                                    Wrap(
                                      // mainAxisAlignment: MainAxisAlignment.end,
                                      alignment: WrapAlignment.end,
                                      crossAxisAlignment: WrapCrossAlignment.end,
                                      runAlignment: WrapAlignment.end,
                                      runSpacing: 4,
                                      spacing: 4,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Tooltip(
                                            message: view,
                                            preferBelow: false,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: Material(
                                              elevation: 0.0,
                                              shape: const CircleBorder(),
                                              clipBehavior: Clip.hardEdge,
                                              color: Colors.transparent,
                                              child: Ink.image(
                                                image: const AssetImage('assets/images/view_rounded.png'),
                                                fit: BoxFit.cover,
                                                width: 35,
                                                height: 35,
                                                child: InkWell(
                                                  onTap: () {
                                                    showRaisedBugDialog(e);
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(0),
                                          child: Tooltip(
                                            message: '$status $update',
                                            preferBelow: false,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: Material(
                                              elevation: 0.0,
                                              shape: const CircleBorder(),
                                              clipBehavior: Clip.hardEdge,
                                              color: Colors.transparent,
                                              child: Ink.image(
                                                image: const AssetImage('assets/images/status_update_rounded.png'),
                                                fit: BoxFit.cover,
                                                width: 35,
                                                height: 35,
                                                child: InkWell(
                                                  onTap: () {
                                                    //if (fullAccessPermission) {
                                                    showStatusUpdateDialog(e);
                                                    /* } else {
                                                      showToast(noPermissionToAccess);
                                                    }*/
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ]);
                          }).toList()),
                    ),
                  );
                } else {
                  return Center(child: Text(noDataAvailable, style: logoutHeader));
                }
              } else {
                return Center(child: Text(data.result!, style: logoutHeader));
              }
            } else {
              return Center(child: Text(pleaseTryAgain, style: logoutHeader));
            }
          } else {
            return Center(child: loader());
          }
        });
  }

  Widget nameField() {
    return ConstrainedBox(
        constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
        child: Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
      child: TextFormField(
        maxLines: 1,
        controller: nameController,
        textInputAction: TextInputAction.done,
        textCapitalization: TextCapitalization.sentences,
        obscureText: false,
        decoration: InputDecoration(
            fillColor: CupertinoColors.white,
            filled: true,
            isDense: true,
            border: border,
            enabledBorder: border,
            focusedBorder: border,
            errorBorder: errorBorder,
            focusedErrorBorder: errorBorder,
            contentPadding: editTextPadding,
            hintStyle: textFieldHintStyle,
            hintText: 'Search title'),
        style: textFieldStyle,
      ),
    ));
  }

  Widget statusField() {
    return ConstrainedBox(
        constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
        child: Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
      child: SizedBox(
        width: 150,
        child: DropdownButtonFormField<String>(
          key: _statusKey,
          items: bugStatuses.map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
          decoration: editTextStatusDecoration,
          style: textFieldStyle,
          value: statusInitialValue,
          onChanged: (String? value) {
            // setState(() {
            statusInitialValue = value!;
            // });
          },
        ),
      ),
    ));
  }
  Widget priorField() {
    return  ConstrainedBox(
        constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
        child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                child: SizedBox(
                  width: 150,
                  child: DropdownButtonFormField<String>(
                    items: bugPriorities.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: editTextPriorityDecoration,
                    style: textFieldStyle,
                    key: _priorityBugKey,
                    //value: priorityInitialValue,
                    onChanged: (String? value) {
                      setState(() {
                        priorityInitialValue = value!;
                      });
                    },
                  ),
                ),
              ));

  }
  Widget stageField() {
    return ConstrainedBox(
        constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
        child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                child: SizedBox(
                  width: 150,
                  child: DropdownButtonFormField<DevelopmentStagesList>(
                    key: _stageBugKey,
                    items: developmentStagesListMenuItems,
                    isExpanded: true,
                    decoration: editTextPriorityDecoration.copyWith(hintText: selectStage),
                    style: textFieldStyle,
                    // value: stageBugInitialValue,
                    validator: stageDropDownValidator,
                    onChanged: (DevelopmentStagesList? value) {
                      setState(() {
                        stageBugInitialValue = value!;
                      });
                    },
                  ),
                ),
              ));

  }
  Widget createEmployeeField() {
    return ConstrainedBox(
        constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
          child: SizedBox(
            width: 150,
            child: DropdownButtonFormField<EmployeeDetails>(
              key: _createEmployeeKey,
              items: employeeListMenuItems,
              isExpanded: true,
              decoration: editTextPriorityDecoration.copyWith(hintText: selectEmployee),
              style: textFieldStyle,
              // value: stageBugInitialValue,
              //validator: stageDropDownValidator,
              onChanged: (EmployeeDetails? value) {
                setState(() {
                  employeeInitialValue = value!;
                });
              },
            ),
          ),
        ));

  }

  Widget assainEmployeeField() {
    return ConstrainedBox(
      constraints: const BoxConstraints(minWidth:230,maxWidth: 230),
      child: IntrinsicWidth(
        child: DropdownSearch<EmployeeDetails>.multiSelection(
          key: _assainEmployeeKey,
          mode: Mode.MENU,
          showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
          compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
          items: employeesList,
          showClearButton: true,
          onChanged: (data) {
            selectedEmployees = data;
          },
          clearButtonSplashRadius: 10,
          selectedItems: selectedEmployees,
          showSearchBox: true,
          dropdownSearchDecoration: editTextEmployeesDecoration.copyWith(labelText: assainEmployee),
          //validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
          dropdownBuilder: (context, selectedItems) {
            return Wrap(
                children: selectedItems
                    .map(
                      (e) => selectedItem(e.name!),
                )
                    .toList());
          },
          filterFn: (EmployeeDetails? employee, name) {
            return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
          },
          popupItemBuilder: (_, text, isSelected) {
            return Container(
              padding: const EdgeInsets.all(10),
              child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
            );
          },
          popupSelectionWidget: (cnt, item, bool isSelected) {
            return Checkbox(
                activeColor: buttonBg,
                hoverColor: buttonBg.withOpacity(0.2),
                value: isSelected,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                side: const BorderSide(color: buttonBg),
                onChanged: (value) {});
          },
          onPopupDismissed: () {

          },
        ),
      ),
    );

  }

  showStatusUpdateDialog(BugsList data) {
    showDialog(
      context: context,
      builder: (context) {
        final _formKey = GlobalKey<FormState>();
        TextEditingController commentTextController = TextEditingController();
        TextEditingController titleController = TextEditingController();
        TextEditingController descController = TextEditingController();
        String bugstatusInitialValue = "Status";
        titleController.text = data.moduleName!;
        descController.text = data.description!;
        for (var e in bugStatuses) {
          if (e == data.bugStatus) {
            bugstatusInitialValue = e;
          }
        }
        return StatefulBuilder(
          builder: (context, statusState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Status Update', style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 2,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                    child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                          child: SizedBox(
                                            child: Text('Title', style: addRoleSubStyle),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                          child: TextFormField(
                                            // maxLines: 1,
                                            controller: titleController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            readOnly: true,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Title'),
                                            style: textFieldStyle,
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                          child: SizedBox(
                                            child: Text('Description', style: addRoleSubStyle),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                          child: TextFormField(
                                            // maxLines: 5,
                                            controller: descController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            readOnly: true,
                                            enabled: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Description here'),
                                            style: textFieldStyle,
                                          ),
                                        ),
                                      )
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text(status, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: SizedBox(
                                              width: 150,
                                              child: DropdownButtonFormField<String>(
                                                items: bugStatuses.map((String value) {
                                                  return DropdownMenuItem<String>(
                                                    value: value,
                                                    child: Text(value),
                                                  );
                                                }).toList(),
                                                decoration: editTextStatusDecoration,
                                                style: textFieldStyle,
                                                value: bugstatusInitialValue,
                                                onChanged: (String? value) {
                                                  statusState(() {
                                                    bugstatusInitialValue = value!;
                                                  });
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text(comment, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: ConstrainedBox(
                                              constraints: const BoxConstraints(minHeight: 5 * 5),
                                              child: IntrinsicWidth(
                                                child: TextFormField(
                                                    minLines: 5,
                                                    maxLines: 10,
                                                    controller: commentTextController,
                                                    enabled: true,
                                                    textInputAction: TextInputAction.done,
                                                    obscureText: false,
                                                    // validator: emptyTextValidator,
                                                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                                                    decoration: InputDecoration(
                                                        fillColor: CupertinoColors.white,
                                                        filled: true,
                                                        isDense: true,
                                                        border: OutlineInputBorder(
                                                            borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                            borderRadius: BorderRadius.circular(5)),
                                                        enabledBorder: OutlineInputBorder(
                                                            borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                            borderRadius: BorderRadius.circular(5)),
                                                        focusedBorder: OutlineInputBorder(
                                                            borderSide: const BorderSide(color: textFieldHint, width: 1),
                                                            borderRadius: BorderRadius.circular(5)),
                                                        errorBorder: OutlineInputBorder(
                                                            borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                            borderRadius: BorderRadius.circular(5)),
                                                        focusedErrorBorder: OutlineInputBorder(
                                                            borderSide: const BorderSide(color: Colors.redAccent, width: 1),
                                                            borderRadius: BorderRadius.circular(5)),
                                                        contentPadding: editTextPadding,
                                                        hintStyle: textFieldTodoCommentHintStyle,
                                                        hintText: addComment),
                                                    style: textFieldTodoCommentHintStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          await viewModel.updateraiseaBugAPI(data.title!, data.bugId!, employeeId, data.description!,
                                              bugstatusInitialValue, data.priority!, [], [],'').then((value) {
                                            Navigator.of(context).pop();
                                            if (value != null) {
                                              if (value.responseStatus == 1) {
                                                //Navigator.of(context).pop();
                                                showToast(value.result!);
                                                // ProjectBugsList();
                                                setState(() {});
                                              } else {
                                                showToast(value.result!);
                                              }
                                            } else {
                                              showToast(pleaseTryAgain);
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(fit: BoxFit.scaleDown, child: Text(update, style: newButtonsStyle)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  showRaisedBugDialog(BugsList data) {
    showDialog(
      context: context,
      builder: (context) {
        final _formKey = GlobalKey<FormState>();
        TextEditingController titleController = TextEditingController();
        TextEditingController descController = TextEditingController();
        return StatefulBuilder(
          builder: (context, assignState) {
            assignState(() {
              titleController.text = data.title!;
              descController.text = data.description!;
            });
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(data.title!, style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context),
                width: ScreenConfig.width(context) / 2,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                  child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                      child: SizedBox(
                                        width: 150,
                                        child: Text('Title', style: addRoleSubStyle),
                                      ),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        // maxLines: 1,
                                        controller: titleController,
                                        textInputAction: TextInputAction.done,
                                        obscureText: false,
                                        readOnly: true,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            fillColor: CupertinoColors.white,
                                            filled: true,
                                            border: border,
                                            isDense: true,
                                            enabledBorder: border,
                                            focusedBorder: border,
                                            errorBorder: errorBorder,
                                            focusedErrorBorder: errorBorder,
                                            contentPadding: editTextPadding,
                                            hintStyle: textFieldHintStyle,
                                            hintText: 'Title'),
                                        style: textFieldStyle,
                                      ),
                                    ),
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                  child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                      child: SizedBox(
                                        width: 150,
                                        child: Text('Description', style: addRoleSubStyle),
                                      ),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        // maxLines: 5,
                                        controller: descController,
                                        textInputAction: TextInputAction.done,
                                        obscureText: false,
                                        readOnly: true,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            fillColor: CupertinoColors.white,
                                            filled: true,
                                            border: border,
                                            isDense: true,
                                            enabledBorder: border,
                                            focusedBorder: border,
                                            errorBorder: errorBorder,
                                            focusedErrorBorder: errorBorder,
                                            contentPadding: editTextPadding,
                                            hintStyle: textFieldHintStyle,
                                            hintText: 'Description here'),
                                        style: textFieldStyle,
                                      ),
                                    )
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                        child: SizedBox(
                                          width: 150,
                                          child: Text('Attachments', style: addRoleSubStyle),
                                        ),
                                      ),
                                      data.attachments!.isNotEmpty
                                          ? Flexible(
                                              fit: FlexFit.loose,
                                              child: SizedBox(
                                                height: 80,
                                                child: MaterialApp(
                                                  debugShowCheckedModeBanner: false,
                                                  scrollBehavior: AppScrollBehavior(),
                                                  home: ListView.builder(
                                                    scrollDirection: Axis.horizontal,
                                                    padding: const EdgeInsets.only(right: 10),
                                                    shrinkWrap: true,
                                                    itemCount: data.attachments!.length,
                                                    itemBuilder: (context, index) {
                                                      return InkWell(
                                                        onTap: () {
                                                          showAttachmentsDialog(context, data.attachments!, index);
                                                        },
                                                        child: Image.network(data.attachments![index],
                                                            width: 80, height: 80, filterQuality: FilterQuality.medium),
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ),
                                            )
                                          : const SizedBox()
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                        child: SizedBox(
                                          width: 150,
                                          child: Text(status, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                        child: Text(data.bugStatus!, style: textFieldStyle),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                        child: SizedBox(
                                          width: 150,
                                          child: Text(priority, style: addRoleSubStyle),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                        child: Text(data.priority!, style: textFieldStyle),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
