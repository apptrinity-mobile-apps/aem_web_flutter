import 'dart:async';
import 'dart:convert';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_stages_response_model.dart';
import 'package:aem/model/task_detail_response_model.dart';
import 'package:aem/model/task_logs_response_model.dart' as LogList;
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/selectable_text_factory_companion_html.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/adaptable_text.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/image_carousel_dialog.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:aem/widgets/working_hours_dialog.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
class ViewTaskScreen extends StatefulWidget {
  final String? projectId, moduleId, taskId;
  const ViewTaskScreen({required this.projectId, required this.moduleId, required this.taskId, Key? key}) : super(key: key);
  @override
  _ViewTaskScreenState createState() => _ViewTaskScreenState();
}
class _ViewTaskScreenState extends State<ViewTaskScreen> {
  bool isCompleted = false, showTaskOnHoldButton = false, showTaskStartedButton = false;
  TextEditingController commentTextController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  late ProjectDetailsNotifier viewModel;
  final _multiKey = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  List<EmployeeDetails> selectedEmployees = [];
  List<String> employeeIds = [];
  bool taskEditPermission = false;
  bool taskEmployeePermission = false;
  bool bugEmployeePermission = false;
  bool testerEmployeePermission = false;
  TextEditingController taskCommentController = TextEditingController();
  bool bugAddPermission = false, bugEditPermission = false;
  TextEditingController holdCommentController = TextEditingController();
  TextEditingController holdPercentageController = TextEditingController();
  String priorityInitialValue = selectPriority;
  final _stageBugKey = GlobalKey<FormFieldState>();
  final _priorityBugKey = GlobalKey<FormFieldState>();
  DevelopmentStagesList stageBugInitialValue = DevelopmentStagesList(stage: selectStage, id: 'none');
  String employeeId = "",
      employeeName = "",
      endDate = "",
      startDate = "",
      addedByName = "",
      dueDate = "",
      involvedTime = "",
      _estimationTime = "",
      taskCreatedDate = "",
      endDateApi = "",
      startDateApi = "",
      extraNotes = "",
      taskName = "",
      createdBy = "", 
      taskDescription = "",
      testCaseDescription = "";
  List<String> notifyEmployeeIds = [], assignedEmployeeIds = [];
  List<AssignedTo> notifyEmployeesList = [], assignedEmployeesList = [], assignedTestersEmployeesList = [];
  List<dynamic> timersList = [];
  List<TaskCommentsList> taskCommentsList = [];
  List<LogList.TaskLogsList> taskHistorysList = [];
  List<TaskBugsList>? taskBugsList = [];
  List<TaskTestCasesList>? taskTestCasesList;
  bool showReset = false;
  List<DropdownMenuItem<DevelopmentStagesList>> developmentStagesListMenuItems = [];
  List<DevelopmentStagesList> stagesList = [];
  List<EmployeeDetails> employeesList = [],
      notifyEmployees = [],
      assignedEmployees = [],
      notifyEmployeesApi = [],
      assignedEmployeesApi = [],
      testerEmployees = [],
      testerEmployeesApi = [];
  int involvedDuration = 0;
  TaskData? taskData;
  final _commentFormKey = GlobalKey<FormState>();
  late Timer _timer = Timer(Duration(seconds: 0), () {});
  int _start = 0;
  int taskEstimationTime = 0;
  late Color timerColor;
  late Color bugtimerColor;
  String durationToString(int minutes) {
    var d = Duration(minutes: minutes);
    List<String> parts = d.toString().split(':');
    return '${parts[0].padLeft(2, '0')} : ${parts[1].padLeft(2, '0')} : ${parts[2].padLeft(2, '0')} mins';
  }
  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
  @override
  void initState() {

    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    taskEditPermission = userViewModel.tasksEditPermission;
    bugAddPermission = userViewModel.bugsAddPermission;
    bugEditPermission = userViewModel.bugsEditPermission;


    developmentStagesListMenuItems.add(DropdownMenuItem(
        child: const Text(selectStage), value: stageBugInitialValue));

    print("bug permissions -- ${bugAddPermission} ${bugEditPermission}");
    employeeId = userViewModel.employeeId!;
    employeeName = userViewModel.employeeName!;
    //projectActivities = [];
    getData();
    super.initState();
  }
  @override
  void dispose() {
    print("disposed");
    _timer.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    getEmployeesData();
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), taskData != null ? body() : const SizedBox()],
                      ),
                    ),
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }
  Widget body() {
    return Expanded(
      child: Row(
        children: [bodyColumn1(), bodyColumn2()],
      ),
    );
  }
  Widget bodyColumn1() {
    return Expanded(
      flex: 2,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: cardBg,
        child: Container(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionTasks, softWrap: true, style: fragmentDescStyle),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Card(
                    elevation: 3,
                    color: CupertinoColors.white,
                    child: Container(
                      width: ScreenConfig.width(context),
                      margin: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          header(),
                          Flexible(
                            child: Container(
                              margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child: ListView(
                                shrinkWrap: true,
                                controller: ScrollController(),
                                padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                                children: [
                                  taskDetails(),
                                  SizedBox(
                                    height: ScreenConfig.height(context),
                                    child: DefaultTabController(
                                      length: 3,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          TabBar(
                                            indicatorWeight: 0.1,
                                            unselectedLabelStyle:
                                                const TextStyle(color: loginPaneLeft, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                            unselectedLabelColor: loginPaneLeft,
                                            indicatorPadding: EdgeInsets.zero,
                                            labelPadding: EdgeInsets.zero,
                                            indicator: const BoxDecoration(
                                                borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.circular(0.00)),
                                                color: buttonBg),
                                            tabs: [
                                              Tab(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: dashBoardCardBorderTile,
                                                    ),
                                                  ),
                                                  child: const Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      "Test Cases",
                                                      style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Tab(
                                                child: Container(
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: dashBoardCardBorderTile,
                                                    ),
                                                  ),
                                                  child: const Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      "Comments",
                                                      style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Tab(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: dashBoardCardBorderTile,
                                                    ),
                                                  ),
                                                  child: const Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      "Activity Log",
                                                      style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Flexible(
                                            child: TabBarView(
                                              physics: const NeverScrollableScrollPhysics(),
                                              children: [testcaseslist(), commentslist(), historylist()],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget bodyColumn2() {
    return Expanded(
      flex: 1,
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: cardBg,
        child: Container(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("", softWrap: true, style: fragmentDescStyle),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Card(
                    elevation: 3,
                    color: CupertinoColors.white,
                    child: Container(
                      width: ScreenConfig.width(context),
                      margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          header2(),
                          Flexible(
                            child: Container(
                              margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child: SingleChildScrollView(
                                controller: ScrollController(),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [bugsFilter(),bugstasklist()],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget bugsFilter(){
    return Row(
      children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 10, 10),
              child: SizedBox(
                width: 200,
                child: DropdownButtonFormField<String>(
                  items: bugPriorities.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  decoration: editTextPriorityDecoration,
                  style: textFieldStyle,
                  key: _priorityBugKey,
                  //value: priorityInitialValue,
                  onChanged: (String? value) {
                    setState(() {
                      priorityInitialValue = value!;
                    });
                  },
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: SizedBox(
                width: 200,
                child: DropdownButtonFormField<DevelopmentStagesList>(
                  key: _stageBugKey,
                  items: developmentStagesListMenuItems,
                  isExpanded: true,
                  decoration: editTextPriorityDecoration.copyWith(hintText: selectStage),
                  style: textFieldStyle,
                 // value: stageBugInitialValue,
                  validator: stageDropDownValidator,
                  onChanged: (DevelopmentStagesList? value) {
                    setState(() {
                      stageBugInitialValue = value!;
                    });
                  },
                ),
              ),
            )

          ],
        ),
        Expanded(
          flex: 1,
          child: Wrap(
            runSpacing: 5,
            spacing: 5,
            alignment: WrapAlignment.end,
            crossAxisAlignment: WrapCrossAlignment.start,
            runAlignment: WrapAlignment.start,
            children: [
              TextButton(
                onPressed: () {
                  setState(() {
                    showReset = true;
                    String priority;
                    String stageId;
                    if(priorityInitialValue == selectPriority){
                      priority = '';
                    }else{
                      priority = priorityInitialValue;
                    }
                    if(stageBugInitialValue.id == 'none'){
                      stageId = '';
                    }else{
                      stageId = stageBugInitialValue.id!;
                    }

                      getBugsData(priority,stageId);


                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Wrap(
                    alignment: WrapAlignment.spaceBetween,
                    crossAxisAlignment:
                    WrapCrossAlignment.center,
                    spacing: 10,
                    children: [
                      Image.asset(
                        "assets/images/filter_2x.png",
                        height: 15,
                        width: 15,
                        fit: BoxFit.fill,
                      ),
                      const Text(
                        filter,
                        softWrap: true,
                        style: TextStyle(
                            letterSpacing: 0.9,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w600,
                            color: buttonBg,
                            fontSize: 14),
                      ),
                    ],
                  ),
                ),
                style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all(filterBg),
                  overlayColor: MaterialStateProperty.all(
                      buttonBg.withOpacity(0.1)),
                  side: MaterialStateProperty.all(
                      const BorderSide(color: filterBg)),
                  elevation: MaterialStateProperty.all(1),
                ),
              ),
              showReset
                  ? TextButton(
                onPressed: () {
                  setState(() {
                    // resetting filter
                    showReset = false;

                    stageBugInitialValue =
                        DevelopmentStagesList(
                            stage:
                            selectStage,
                            id: 'none');

                     _stageBugKey.currentState!.reset();
                     _priorityBugKey.currentState!.reset();
                    priorityInitialValue = selectPriority;
                    getBugsData('','');
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Wrap(
                    alignment:
                    WrapAlignment.spaceBetween,
                    crossAxisAlignment:
                    WrapCrossAlignment.center,
                    spacing: 10,
                    children: [
                      const Text(
                        reset,
                        softWrap: true,
                        style: TextStyle(
                            letterSpacing: 0.9,
                            fontFamily: "Poppins",
                            fontWeight:
                            FontWeight.w600,
                            color:
                            dashBoardCardOverDueText,
                            fontSize: 14),
                      ),
                      Image.asset(
                        "assets/images/cancel_red_2x.png",
                        height: 15,
                        width: 15,
                        fit: BoxFit.fill,
                      ),
                    ],
                  ),
                ),
                style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all(
                      resetBg),
                  overlayColor:
                  MaterialStateProperty.all(
                      dashBoardCardOverDueText
                          .withOpacity(0.1)),
                  side: MaterialStateProperty.all(
                      const BorderSide(
                          color: resetBg)),
                  elevation:
                  MaterialStateProperty.all(1),
                ),
              )
                  : const SizedBox()
            ],
          ),
        )
      ],
    );
  }
  /*Widget stagesField() {
    return DropdownSearch<DevelopmentStagesList>.multiSelection(
      key: _stageRoleKey,
      mode: Mode.MENU,
      showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
      compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
      items: stagesList,
      showClearButton: true,
      onChanged: (data) {
        selectedStages = data;
      },
      clearButtonSplashRadius: 10,
      selectedItems: selectedStages,
      showSearchBox: true,
      dropdownSearchDecoration:
      editTextEmployeesDecoration.copyWith(labelText: jobRole),
      validator: (list) =>
      list == null || list.isEmpty ? emptyFieldError : null,
      dropdownBuilder: (context, selectedItems) {
        return Wrap(
            children: selectedItems
                .map(
                  (e) => selectedItem(e.stage!),
            )
                .toList());
      },
      filterFn: (DevelopmentStagesList? employee, name) {
        return employee!.stage!.toLowerCase().contains(name!.toLowerCase())
            ? true
            : false;
      },
      popupItemBuilder: (_, text, isSelected) {
        return Container(
          padding: const EdgeInsets.all(10),
          child: Text(text.stage!,
              style: isSelected ? dropDownSelected : dropDown),
        );
      },
      popupSelectionWidget: (cnt, item, bool isSelected) {
        return Checkbox(
            activeColor: buttonBg,
            hoverColor: buttonBg.withOpacity(0.2),
            value: isSelected,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)),
            side: const BorderSide(color: buttonBg),
            onChanged: (value) {});
      },
      onPopupDismissed: () {},
    );
  }*/
  ///body_column1
  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  // if(mounted){
                  //   for(int i=0;i<timersList.length;i++){
                  //     print("timersList -- ${timersList} ${timersList[i]['timer'].tick}");
                  //    // timersList[i]['timer'].cancel();
                  //   }
                  //
                  // }
                  Navigator.of(context).pop();
                },
                child: backButton(),
              ),
              Row(
                children: [
                  Text("", maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  //Text("${durationToString(_start)}", maxLines: 1, softWrap: true, style: fragmentHeaderStyle.copyWith(color: timerColor)),

                  Text("${_printDuration(Duration(seconds: _start))}",
                      maxLines: 1, softWrap: true, style: fragmentHeaderStyle.copyWith(color: timerColor)),
                  IconButton(
                    onPressed: () {
                      showWorkingHoursDialog();
                    },
                    iconSize: 24,
                    padding: const EdgeInsets.all(4),
                    splashRadius: 24,
                    icon: const Icon(Icons.remove_red_eye_outlined),
                  ),
                ],
              ),
              taskStartHold()
            ],
          ),
          Row(
            children: [
              FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.contain,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: InkWell(
                    onTap: () async {
                      if (taskEmployeePermission) {
                        if (!isCompleted) {
                          int involvedMins = Duration(seconds: _start).inMinutes;
                          if (taskEstimationTime < involvedMins) {
                            taskCommentController.text = "";

                            showTaskCommentDialog();
                          } else {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            await viewModel
                                .updateTasksBasedOnStatusAPI(employeeId, widget.taskId!, StatusText.completeStr.name!, commentTextController.text,0)
                                .then((value) {
                              Navigator.of(context).pop();
                              if (value != null) {
                                if (value.responseStatus == 1) {
                                  setState(() {
                                    isCompleted = true;
                                    _timer.cancel();
                                  });
                                  getData();
                                  showToast(value.result!);
                                } else {
                                  showToast(value.result!);
                                }
                              } else {
                                showToast(pleaseTryAgain);
                              }
                            });
                          }
                        }
                      }
                    },
                    child: isCompleted
                        ? Container(
                            height: 55,
                            width: 55,
                            decoration: markDoneIconDecoration,
                            child: const Center(
                              child: Icon(Icons.done_rounded, color: CupertinoColors.white, size: 35),
                            ),
                          )
                        : Container(
                            height: 55,
                            width: 55,
                            decoration: markDoneDecoration,
                            padding: const EdgeInsets.all(1),
                            child: Center(
                              child: Text(markComplete, textAlign: TextAlign.center, maxLines: 3, style: markDoneStyle),
                            ),
                          ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(taskName == "" ? taskNameStr : taskName, maxLines: 2, softWrap: true, style: fragmentHeaderStyle),
                    Text(taskDescription == "" ? taskDescriptionStr : taskDescription, softWrap: true, style: fragmentDescStyle)
                  ],
                ),
              ),
              Row(
                children: [

                  isCompleted
                      ? FittedBox(
                          alignment: Alignment.centerRight,
                          fit: BoxFit.scaleDown,
                          child: Row(
                            children: [
                              Container(
                                decoration: buttonDecorationGreen,
                                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: TextButton(
                                  onPressed: () async {
                                    if (taskEmployeePermission) {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return loader();
                                          });
                                      await viewModel
                                          .updateTasksBasedOnStatusAPI(
                                              employeeId, widget.taskId!, StatusText.reopenStr.name!, commentTextController.text,0)
                                          .then((value) {
                                        Navigator.of(context).pop();
                                        if (value != null) {
                                          if (value.responseStatus == 1) {
                                            setState(() {
                                              isCompleted = false;
                                            });
                                            getData();
                                            showToast(value.result!);
                                          } else {
                                            showToast(value.result!);
                                          }
                                        } else {
                                          showToast(pleaseTryAgain);
                                        }
                                      });
                                    } else {
                                      showToast(noPermissionToAccess);
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(reOpen, style: newButtonsStyle),
                                    ),
                                  ),
                                ),
                              ),
                              testerEmployeePermission ? Container(
                                decoration: buttonBorderGreen,
                                margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: TextButton(
                                  onPressed: () async {
                                    await viewModel
                                        .updateTasksBasedOnStatusAPI(employeeId, widget.taskId!, StatusText.closedStr.name!, '',0)
                                        .then((value) {
                                      Navigator.of(context).pop();
                                      if (value != null) {
                                        if (value.responseStatus == 1) {
                                          setState(() {
                                            isCompleted = true;
                                            _timer.cancel();
                                          });
                                          getData();
                                          showToast(value.result!);
                                        } else {
                                          showToast(value.result!);
                                        }
                                      } else {
                                        showToast(pleaseTryAgain);
                                      }
                                    });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(close, style: cancelButtonStyle),
                                    ),
                                  ),
                                ),
                              ) : SizedBox(),
                            ],
                          ),
                        )
                      : const SizedBox(),
                  Container(
                    decoration: buttonDecorationRed,
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: TextButton(
                      onPressed: () {
                        if (taskEmployeePermission) {
                          showTaskDeleteDialog(taskData!);
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text('$delete Task', style: newButtonsStyle),
                        ),
                      ),
                    ),
                  ),
                  /*PopupMenuButton<int>(
                      offset: const Offset(0, 30),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      tooltip: "",
                      child: Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            border: Border.all(color: buttonBg),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white),
                        child: const Center(
                          child: Icon(Icons.more_vert),
                        ),
                      ),
                      onSelected: (value) => onSelected(context, value),
                      itemBuilder: (context) => [
                            PopupMenuItem(
                                height: 30,
                                padding:
                                    const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: Text(workingHours, style: popMenuHeader),
                                value: 0),
                          ]),*/
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget taskDetails() {
    return Container(
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      decoration: cardBorder,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Material(
                  elevation: 0,
                  shape: const CircleBorder(),
                  clipBehavior: Clip.hardEdge,
                  color: Colors.transparent,
                  child: Ink.image(
                    image: const AssetImage('assets/images/edit_rounded.png'),
                    fit: BoxFit.cover,
                    width: 35,
                    height: 35,
                    child: InkWell(
                      onTap: () {
                        if (taskEditPermission) {
                          showEditDetailsDialog();
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(addedBy, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: Text(addedByName, maxLines: 1, style: textFieldTodoFieldsStyle),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(assignedTo, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(maxHeight: 25),
                      child: assignedEmployeesList.isNotEmpty
                          ? ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              physics: const ScrollPhysics(),
                              itemCount: assignedEmployeesList.length,
                              itemBuilder: (context, index) {
                                return employeeIcons(assignedEmployeesList[index]);
                              })
                          : const SizedBox(),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(assignedToTester, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(maxHeight: 25),
                      child: assignedTestersEmployeesList.isNotEmpty
                          ? ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              physics: const ScrollPhysics(),
                              itemCount: assignedTestersEmployeesList.length,
                              itemBuilder: (context, index) {
                                return employeeIcons(assignedTestersEmployeesList[index]);
                              })
                          : const SizedBox(),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(whenDone, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(maxHeight: 25),
                      child: notifyEmployeesList.isNotEmpty
                          ? ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              physics: const ScrollPhysics(),
                              itemCount: notifyEmployeesList.length,
                              itemBuilder: (context, index) {
                                return employeeIcons(notifyEmployeesList[index]);
                              })
                          : const SizedBox(),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(createDate + ":", style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: Text(taskCreatedDate, maxLines: 1, style: textFieldTodoFieldsStyle),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(estimationTime, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: Text(_estimationTime, maxLines: 1, style: textFieldTodoFieldsStyle),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(dueOn, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: Text(dueDate, maxLines: 1, style: textFieldTodoFieldsStyle),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(notes, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: Text(parseHtmlString(extraNotes), maxLines: 1, style: textFieldTodoFieldsStyle),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget commentsDetails() {
    return taskCommentsList.isNotEmpty
        ? Flexible(
            child: ListView.separated(
              shrinkWrap: true,
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              scrollDirection: Axis.vertical,
              physics: const ScrollPhysics(),
              itemCount: taskCommentsList.length,
              itemBuilder: (context, index) {
                return comments(taskCommentsList[index]);
              },
              separatorBuilder: (context, index) {
                return const Divider(color: Colors.transparent);
              },
            ),
          )
        : const SizedBox();
  }
  Widget commentField() {
    return Form(
      key: _commentFormKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            decoration: /*isReopened ? buttonBorderGreen : cardBorder*/ buttonBorderGreen,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                      child: CircleAvatar(
                        backgroundColor: getNameBasedColor(employeeName.toUpperCase()[0]),
                        child: Text(employeeName.toUpperCase()[0], style: projectIconTextStyle),
                      ),
                      alignment: Alignment.centerLeft),
                  Expanded(
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(minHeight: 5 * 24),
                      child: IntrinsicWidth(
                        child: TextFormField(
                            minLines: 5,
                            maxLines: 15,
                            controller: commentTextController,
                            enabled: /*isReopened ? true : false*/ true,
                            textInputAction: TextInputAction.done,
                            obscureText: false,
                            // validator: emptyTextValidator,
                            // autovalidateMode: AutovalidateMode.onUserInteraction,
                            decoration: editTextTodoCommentDecoration,
                            style: textFieldTodoCommentHintStyle),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            decoration: buttonDecorationGreen,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: TextButton(
              onPressed: () async {
                if (_commentFormKey.currentState!.validate()) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return loader();
                      });
                  await viewModel
                      .addCommentToTaskAPI(
                          widget.taskId!, commentTextController.text.toString().trim(), widget.moduleId!, widget.projectId!, employeeId)
                      .then((value) {
                    Navigator.of(context).pop();
                    if (value != null) {
                      if (value.responseStatus == 1) {
                        commentTextController.clear();
                        getData();
                        showToast(value.result!);
                      } else {
                        showToast(value.result!);
                      }
                    } else {
                      showToast(pleaseTryAgain);
                    }
                  });
                }
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                child: Text(submit.toUpperCase(), style: newButtonsStyle),
              ),
            ),
          )
        ],
      ),
    );
  }
  Future<void> showRangeCalendar(TextEditingController textController) async {
    final DateTimeRange? result = await showDateRangePicker(
        context: context,
        firstDate: DateTime.now(),
        lastDate: DateTime(2100),
        builder: (context, child) {
          return Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400, maxHeight: 500),
              child: child,
            ),
          );
        });
    setState(() {
      if (result != null) {
        DateTime _startDate = result.start;
        DateTime _endDate = result.end;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputStartDate = inputFormat.parse(_startDate.toLocal().toString().split(' ')[0]); // removing time from date
        var inputEndDate = inputFormat.parse(_endDate.toLocal().toString().split(' ')[0]); // removing time from date
        startDate = DateFormat('yyyy-MM-dd').format(inputStartDate);
        endDate = DateFormat('yyyy-MM-dd').format(inputEndDate);
        var displayStartDate = DateFormat('E, MMM dd').format(inputStartDate);
        var displayEndDate = DateFormat('E, MMM dd').format(inputEndDate);
        textController.text = "$displayStartDate - $displayEndDate";
      }
    });
  }
  Widget employeeIcons(AssignedTo list) {
    return Padding(
      padding: const EdgeInsets.only(right: 5),
      child: Row(children: [
        CircleAvatar(
          backgroundColor: getNameBasedColor(list.name![0]),
          radius: 10,
          child: Center(
            child: Text(list.name!.toUpperCase()[0], style: taskIconTextStyle),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
          child: Text(list.name!, maxLines: 1, style: textFieldTodoFieldsStyle),
        )
      ]),
    );
  }
  Widget comments(TaskCommentsList itemData) {
    return Container(
      width: ScreenConfig.width(context),
      padding: const EdgeInsets.all(10),
      color: profileCommentsBg,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: CircleAvatar(
                    backgroundColor: getNameBasedColor(itemData.emoployeeName!.toUpperCase()[0]),
                    child: Text(itemData.emoployeeName!.toUpperCase()[0], style: projectIconTextStyle),
                  ),
                  alignment: Alignment.center),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AdaptableText(
                      itemData.emoployeeName!,
                      style: const TextStyle(letterSpacing: 1, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 16),
                      textAlign: TextAlign.start,
                    ),
                    HtmlWidget(
                      itemData.comment!,
                      buildAsync: true,
                      enableCaching: false,
                      factoryBuilder: () => SelectableTextCompanion(),
                      onTapUrl: (url) async {
                        if (await canLaunchUrl(Uri.parse(url))) {
                          await launchUrl(Uri.parse(url));
                          return true;
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      renderMode: RenderMode.column,
                      textStyle: completedStyle,
                      onTapImage: (imageData) {},
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AdaptableText(
                    dayAndTime(itemData.createdOn!),
                    style: projectDateMenuStyle,
                    textAlign: TextAlign.center,
                  ),
                  itemData.employeeId == employeeId
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 0, 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                child: Tooltip(
                                  message: edit,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.none,
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        showEditCommentDialog(itemData);
                                      },
                                      child: OnHoverCard(builder: (isHovered) {
                                        return CircleAvatar(
                                          backgroundColor: CupertinoColors.black,
                                          radius: 14,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 13,
                                            child: Center(
                                              child: Image.asset('assets/images/edit_icon.png',
                                                  width: 12, height: 12, color: isHovered ? buttonBg : CupertinoColors.black),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                child: Tooltip(
                                  message: delete,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.none,
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        showDeleteDialog(itemData);
                                      },
                                      child: OnHoverCard(builder: (isHovered) {
                                        return CircleAvatar(
                                          backgroundColor: CupertinoColors.black,
                                          radius: 14,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 13,
                                            child: Center(
                                              child: Image.asset('assets/images/delete_icon.png',
                                                  width: 12, height: 12, color: isHovered ? dashBoardCardOverDueText : CupertinoColors.black),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox()
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
  showDeleteDialog(TaskCommentsList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Text('$doYouWantDelete this $comment?', style: logoutContentHeader),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteTaskCommentAPI(itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  showEditCommentDialog(TaskCommentsList itemData) {
    TextEditingController commentController = TextEditingController();
    final _formKey = GlobalKey<FormState>();
    commentController.text = itemData.comment!;
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$edit $comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 700, maxWidth: 700, minHeight: 5 * 24, maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: commentController,
                    validator: emptyTextValidator,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration,
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          await viewModel.updateTaskCommentAPI(itemData.id!, commentController.text.trim()).then((value) {
                            Navigator.pop(context);
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                showToast(value.result!);
                                Navigator.pop(context);
                                getData();
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  showTaskCommentDialog() {
    final _formKey = GlobalKey<FormState>();
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 700, maxWidth: 700, minHeight: 5 * 24, maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(hintText: reasonExtraTime),
                    style: textFieldStyle,
                    validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await viewModel
                              .updateTasksBasedOnStatusAPI(employeeId, widget.taskId!, StatusText.completeStr.name!, commentTextController.text,0)
                              .then((value) {
                            Navigator.of(context).pop();
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                setState(() {
                                  isCompleted = true;
                                  _timer.cancel();
                                });
                                getData();
                                showToast(value.result!);
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  Widget taskStartHold() {
    if (isCompleted) {
      return const SizedBox();
    } else {
      if (showTaskOnHoldButton) {
        return Container(
          decoration: buttonDecorationRed,
          margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: TextButton(
            onPressed: () async {
              showHoldTaskCommentDialog(widget.taskId!);
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text('Hold Task', style: newButtonsStyle),
              ),
            ),
          ),
        );
      } else if (showTaskStartedButton) {
        return Container(
          decoration: buttonDecorationGreen,
          margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: TextButton(
            onPressed: () async {
              if (taskEmployeePermission) {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return loader();
                    });
                await viewModel
                    .updateTasksBasedOnStatusAPI(employeeId, widget.taskId!, StatusText.openStr.name!, commentTextController.text,0)
                    .then((value) {
                  Navigator.of(context).pop();
                  if (value != null) {
                    if (value.responseStatus == 1) {
                      setState(() {
                        isCompleted = false;
                      });
                      getData();
                      showToast(value.result!);
                    } else {
                      showToast(value.result!);
                    }
                  } else {
                    showToast(pleaseTryAgain);
                  }
                });
              } else {
                showToast(noPermissionToAccess);
              }
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text('Start Task', style: newButtonsStyle),
              ),
            ),
          ),
        );
      } else {
        return const SizedBox();
      }
    }
  }
  ///body_column2
  Widget header2() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(bugs, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Text(tasks_bugs, softWrap: true, style: fragmentDescStyle)
            ],
          ),
        ),
        Container(
          decoration: buttonDecorationRed,
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: TextButton(
            onPressed: () async {
              if (bugAddPermission) {
                showRaisedBugDialog(null, false);
              } else {
                showToast(noPermissionToAccess);
              }
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(raise_bug, style: newButtonsStyle),
              ),
            ),
          ),
        ),
      ],
    );
  }
  Widget bugstasklist() {
    return taskBugsList!.isNotEmpty
        ? ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: const ScrollPhysics(),
            itemCount: taskBugsList!.length,
            itemBuilder: (context, index) {
              int bugTime = taskBugsList![index].involvedTime!;
              // setState(() {

              //  });

              return OnHoverCard(builder: (isHovered) {
                return InkWell(
                  onTap: () {
                    showRaisedBugDialog(taskBugsList![index], true);
                  },
                  child: Container(
                    width: ScreenConfig.width(context),
                    padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: isHovered ? buttonBg : dashBoardCardBorderTile,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(0)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 2,
                              child: AdaptableText(
                                taskBugsList![index].title!,
                                style: buglisttitleHeader,
                                textAlign: TextAlign.start,
                              ),
                            ),
                            Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    if (bugEditPermission) {
                                      showRaisedBugDialog(taskBugsList![index], false);
                                    } else {
                                      showToast(noPermissionToAccess);
                                    }
                                  },
                                  iconSize: 18,
                                  padding: const EdgeInsets.all(4),
                                  splashRadius: 20,
                                  icon: const Icon(Icons.edit),
                                ),
                                IconButton(
                                  onPressed: () {
                                    if (bugEditPermission) {
                                      showBugDeleteDialog(taskBugsList![index]);
                                    } else {
                                      showToast(noPermissionToAccess);
                                    }
                                  },
                                  iconSize: 24,
                                  padding: const EdgeInsets.all(4),
                                  splashRadius: 20,
                                  icon: const Icon(Icons.delete_forever_outlined, color: newReOpened),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 20, 5),
                          child: Row(
                            children: [
                              /*Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                                  child: Text(create + "d:", style: buglisttitleHeader, textAlign: TextAlign.end),
                                ),
                              ),*/
                              Expanded(
                                flex: 4,
                                child: ConstrainedBox(
                                  constraints: const BoxConstraints(),
                                  child: Text(formatAgo(taskBugsList![index].createdOn), maxLines: 1, style: buglisttitleHeader),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 80,
                              child: Text("${_printDuration(Duration(seconds: timersList[index]["sec"]))}", style: buglisttitleHeader),
                            ),
                            /*RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: '',
                                      style: buglisttitleHeader),
                                  TextSpan(
                                      text:
                                          "${_printDuration(Duration(seconds: timersList[index]["sec"]))}",
                                      style: buglisttitleHeader),
                                ],
                              ),
                            ),*/
                            taskBugsList![index].bugStatus! == "New"
                                ? Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "inProgress");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Start', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Hold"
                                ? Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "inProgress");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Start', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Closed"
                                ? Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "reopened");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Reopen', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Inprogress"
                                ? Container(
                                    decoration: buttonDecorationRed,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "hold");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Hold', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Inprogress"
                                ? Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "closed");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Done', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Reopened"
                                ? Container(
                                    decoration: buttonDecorationRed,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "hold");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Hold', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            taskBugsList![index].bugStatus!.toCapitalized() == "Reopened"
                                ? Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (checkAccessForBug(taskBugsList![index])) {
                                          updateBugStatus(taskBugsList![index].id!, "closed");
                                        } else {
                                          showToast(noPermissionToAccess);
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text('Done', style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Wrap(
                            spacing: 4,
                            runSpacing: 4,
                            alignment: WrapAlignment.start,
                            runAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            children: [
                              Material(
                                elevation: 1,
                                borderRadius: const BorderRadius.all(Radius.circular(0)),
                                child: Container(
                                  padding: const EdgeInsets.fromLTRB(5, 2, 5, 2),
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(0)),
                                  ),
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(text: 'Priority : ', style: bugstatustitle),
                                        TextSpan(text: taskBugsList![index].priority!, style: bugstatus),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Material(
                                elevation: 1,
                                borderRadius: const BorderRadius.all(Radius.circular(0)),
                                child: Container(
                                  padding: const EdgeInsets.fromLTRB(5, 2, 5, 2),
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(0)),
                                  ),
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(text: 'Status : ', style: bugstatustitle),
                                        TextSpan(text: taskBugsList![index].bugStatus!, style: bugstatus),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              });
            },
            separatorBuilder: (context, index) {
              return const Divider(color: Colors.transparent);
            },
          )
        :  Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Center(
      child: Text(noDataAvailable, style: logoutHeader),
    ),
        );
  }
  Widget testcaseslist() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: dashBoardCardBorderTile,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(descriptionTestcase, softWrap: true, style: fragmentDescStyle),
          ),
          Container(
            decoration: buttonBorderGreen,
            margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: TextButton(
              onPressed: () {
                setState(() {
                  if (bugAddPermission) {
                    showAddTestCaseDialog(null, false);
                  } else {
                    showToast(noPermissionToAccess);
                  }
                });
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text('+ ' + addtestcase, style: cancelButtonStyle),
                ),
              ),
            ),
          ),
          taskTestCasesList!.isNotEmpty
              ? Flexible(
                  child: ListView.separated(
                    padding: const EdgeInsets.all(10),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: taskTestCasesList!.length,
                    itemBuilder: (context, index) {
                      return OnHoverCard(builder: (isHovered) {
                        return InkWell(
                          onTap: () {
                            showAddTestCaseDialog(taskTestCasesList![index], true);
                          },
                          child: Container(
                            width: ScreenConfig.width(context),
                            padding: const EdgeInsets.fromLTRB(10, 5, 5, 10),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: isHovered ? buttonBg : dashBoardCardBorderTile,
                              ),
                              borderRadius: const BorderRadius.all(Radius.circular(0)),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: AdaptableText(taskTestCasesList![index].title!, style: buglisttitleHeader, textAlign: TextAlign.start),
                                    ),
                                    Row(
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            if (bugEditPermission) {
                                              showAddTestCaseDialog(taskTestCasesList![index], false);
                                            } else {
                                              showToast(noPermissionToAccess);
                                            }
                                          },
                                          iconSize: 18,
                                          padding: const EdgeInsets.all(4),
                                          splashRadius: 20,
                                          icon: const Icon(Icons.edit),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            if (bugEditPermission) {
                                              showTestCasesDeleteDialog(taskTestCasesList![index]);
                                            } else {
                                              showToast(noPermissionToAccess);
                                            }
                                          },
                                          iconSize: 24,
                                          padding: const EdgeInsets.all(4),
                                          splashRadius: 20,
                                          icon: const Icon(Icons.delete_forever_outlined, color: newReOpened),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                  child: Material(
                                    elevation: 1,
                                    borderRadius: const BorderRadius.all(Radius.circular(0)),
                                    child: Container(
                                      padding: const EdgeInsets.fromLTRB(5, 2, 5, 2),
                                      decoration: const BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(0)),
                                      ),
                                      child: RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(text: 'Status : ', style: bugstatustitle),
                                            TextSpan(text: taskTestCasesList![index].tcStatus!, style: bugstatus),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                //const Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 5), child: Divider(height: 1, color: dashBoardCardBorderTile))
                              ],
                            ),
                          ),
                        );
                      });
                    },
                    separatorBuilder: (context, index) {
                      return const Divider(color: Colors.transparent);
                    },
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
  Widget commentslist() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: dashBoardCardBorderTile,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(descriptionComments, softWrap: true, style: fragmentDescStyle),
          ),
          commentsDetails(),
          commentField(),
        ],
      ),
    );
  }
  Widget historylist() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: dashBoardCardBorderTile,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(descriptionActivityLog, softWrap: true, style: fragmentDescStyle),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: taskHistorysList.isNotEmpty
                  ? ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      physics: const ScrollPhysics(),
                      itemCount: taskHistorysList.length,
                      itemBuilder: (context, index) {
                        return projectLogs(taskHistorysList[index]);
                      })
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 100, bottom: 100),
                        child: Text(noDataAvailable, style: logoutHeader),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
  Widget projectLogs(LogList.TaskLogsList list) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
          child: Text(list.day!, style: projectDateStyle),
        ),
        list.logsData!.isNotEmpty
            ? ListView.builder(
                shrinkWrap: true,
                primary: false,
                physics: const ScrollPhysics(),
                itemCount: list.logsData!.length,
                itemBuilder: (context, index) {
                  return activitiesList(index, list.logsData![index]);
                })
            : const SizedBox()
      ],
    );
  }
  Widget activitiesList(int position, LogList.LogsData itemData) {
    var alternate = position % 2;
    return itemData.moduleId == ""
        ? SizedBox(
            width: ScreenConfig.width(context),
            // height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                    color: Colors.white,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(text: itemData.moduleName, style: projectAddedByNameStyle),
                        TextSpan(text: ', created by ', style: projectAddedByStyle),
                        TextSpan(text: '${itemData.employeeName}.', style: projectAddedByNameStyle)
                      ]),
                    ),
                  ),
                ),
              ],
            ),
          )
        : alternate == 0
            ? IntrinsicHeight(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                        color: Colors.white,
                        child: leftItem(itemData),
                      ),
                    ),
                    const VerticalDivider(thickness: 1.5, color: dashBoardCardBorderTile),
                    const Expanded(
                      child: SizedBox(),
                    ),
                  ],
                ),
              )
            : IntrinsicHeight(
                child: Row(
                  children: [
                    const Expanded(
                      child: SizedBox(),
                    ),
                    const VerticalDivider(thickness: 1.5, color: dashBoardCardBorderTile),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                        color: Colors.white,
                        child: rightItem(itemData),
                      ),
                    ),
                  ],
                ),
              );
  }
  showHoldTaskCommentDialog(String taskId) {
    final _formKey = GlobalKey<FormState>();
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$commentHold', style: newCompletedHeaderStyle),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: 700,
                    maxWidth: 700,
                    minHeight: 9 * 24,
                    maxHeight: 9 * 24),
                child: IntrinsicWidth(
                  child: Column(
                    children: [
                      SizedBox(height: 10,),
                      TextFormField(
                        maxLines: 5,
                        controller: holdCommentController,
                        textInputAction: TextInputAction.done,
                        validator: emptyTextValidator,
                        obscureText: false,
                        decoration: editTextProjectDescriptionDecoration.copyWith(
                            hintText: comment),
                        style: textFieldStyle,
                        // validator: emptyTextValidator,
                        // autovalidateMode: AutovalidateMode.onUserInteraction
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          children: [

                            Text(
                              'Percentage',
                              style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                              textAlign: TextAlign.center,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(20, 5, 5, 5),
                              child: OnHoverCard(builder: (isHovered) {
                                return Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: CupertinoColors.white, borderRadius: BorderRadius.circular(5), border: Border.all(color: editTextHint, width: 1.5)),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: TextFormField(
                                              controller: holdPercentageController,
                                              maxLines: 1,
                                              textInputAction: TextInputAction.done,
                                              obscureText: false,
                                              decoration: editTextProjectDescriptionDecoration.copyWith(hintText: ''),
                                              style: textFieldStyle,
                                              // validator: emptyTextValidator,
                                              // autovalidateMode: AutovalidateMode.onUserInteraction
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                            child: Text(
                                              '%',
                                              style: TextStyle(fontSize: 15, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ));
                              }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          if(holdPercentageController.text != ""){
                            if (taskEmployeePermission) {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              await viewModel
                                  .updateTasksBasedOnStatusAPI(employeeId, widget.taskId!, StatusText.holdStr.name!, holdCommentController.text,int.parse(holdPercentageController.text))
                                  .then((value) {
                                Navigator.of(context).pop();
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    setState(() {
                                      isCompleted = false;
                                    });
                                    getData();
                                    showToast(value.result!);
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            } else {
                              showToast(noPermissionToAccess);
                            }
                           // updateTaskStatus(taskId, StatusText.holdStr.name!, holdCommentController.text,int.parse(holdPercentageController.text));

                            Navigator.of(context).pop();
                            holdCommentController.text = "";
                            holdPercentageController.text = "";

                          }else{
                            showToast("enter percentage");
                          }

                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  bool checkAccessForBug(TaskBugsList? taskbuglistData) {
    List<AssignedEmployeesList> employeeList1 = taskbuglistData!.assignedEmployeesList!;
    for (var element in employeeList1) {
      print("employeeId -- ${employeeId} tester ${element.id}");

      if (element.id == employeeId) {
        bugEmployeePermission = true;
      } else {
        bugEmployeePermission = false;
      }
    }
    return bugEmployeePermission;
  }
  showRaisedBugDialog(TaskBugsList? taskbuglistData, bool type) {

    print('taskbuglistData ${taskbuglistData.toString()}');


    selectedEmployees.clear();


    showDialog(
      context: context,
      builder: (context) {
        String statusInitialValue = "New";
        String priorityInitialValue = "Low";
        //_stageKey.currentState!.clear();
        final _stageKey = GlobalKey<DropdownSearchState<DevelopmentStagesList>>();
        DevelopmentStagesList stageInitialValue = DevelopmentStagesList(stage: selectStage, id: 'none');
        developmentStagesListMenuItems = [];
        developmentStagesListMenuItems.add(DropdownMenuItem(
            child: const Text(selectStage), value: stageInitialValue));
        for (var element in viewModel.developmentStagesList()) {
          developmentStagesListMenuItems.add(DropdownMenuItem(
              child: Text(element.stage!), value: element));
        }
        TextEditingController titleController = TextEditingController();
        TextEditingController descController = TextEditingController();
        List<String> bugsAttachments = [];
        if (taskbuglistData != null) {
          titleController.text = taskbuglistData.title!;
          descController.text = taskbuglistData.description!;
          if (taskbuglistData.bugStatus!.toCapitalized() == "Inprogress") {
            statusInitialValue = 'InProgress';
          } else {
            statusInitialValue = taskbuglistData.bugStatus!.toCapitalized();
          }
          print("statusInitialValue -- ${statusInitialValue}");

          priorityInitialValue = taskbuglistData.priority!;
          bugsAttachments.addAll(taskbuglistData.attachments!);

          List<AssignedEmployeesList> employeeList1 = taskbuglistData.assignedEmployeesList!;


          for (var element in stagesList) {
            if(element.id == taskbuglistData.devStageId!){

              stageInitialValue = element;

            }

          }


          for (int i = 0; i < employeeList1.length; i++) {
            String employeeid = employeeList1[i].id!;

            for (int j = 0; j < employeesList.length; j++) {
              String testerid = employeesList[j].id!;

              if (employeeid == testerid) {
                selectedEmployees.add(employeesList[j]);
                print("selectedEmployees list ${selectedEmployees}");
              }
            }
          }
        }
        List<String> imgAttachments = [];
        final _formKey = GlobalKey<FormState>();
        return IgnorePointer(
          ignoring: type,
          child: StatefulBuilder(
            builder: (context, assignState) {
              return AlertDialog(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 2),
                      child: Text(taskName, style: projectCountStyle),
                    ),
                    IconButton(
                        icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })
                  ],
                ),
                titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
                contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                content: Container(
                  width: ScreenConfig.width(context) / 2,
                  margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                          child: SingleChildScrollView(
                            controller: ScrollController(),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(taskbuglistData != null ? taskbuglistData.title! : raise_bug, style: profilepopHeader),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Title', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 1,
                                            controller: titleController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Title'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Description', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 5,
                                            controller: descController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Description here'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      )
                                    ]),
                                  ),
                                  assignEmployeeField(),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(5, 5, 15, 0),
                                          child: Text('Attachments', style: addRoleSubStyle),
                                        ),
                                        Flexible(
                                          fit: FlexFit.loose,
                                          child: SizedBox(
                                            height: imgAttachments.isEmpty ? 20 : 116,
                                            child: ListView.builder(
                                              padding: const EdgeInsets.only(right: 10),
                                              scrollDirection: Axis.horizontal,
                                              shrinkWrap: true,
                                              itemCount: imgAttachments.length,
                                              itemBuilder: (context, index) {
                                                debugPrint("attach---- " + imgAttachments[index]);
                                                return Padding(
                                                  padding: const EdgeInsets.only(right: 15),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      InkWell(
                                                        onTap: () {
                                                          showLocalAttachmentsDialog(context, imgAttachments, index);
                                                        },
                                                        child: Image.memory(
                                                          base64Decode(imgAttachments[index]),
                                                          width: 70,
                                                          height: 60,
                                                          filterQuality: FilterQuality.high,
                                                          fit: BoxFit.contain,
                                                          errorBuilder: (context, url, error) => const Icon(Icons.error),
                                                        ),
                                                      ),
                                                      IconButton(
                                                        onPressed: () {
                                                          imgAttachments.removeAt(index);
                                                          assignState(() {});
                                                        },
                                                        //padding: EdgeInsets.zero,
                                                        splashRadius: 1,
                                                        icon: const Icon(Icons.delete_forever_outlined, color: newReOpened),
                                                      )
                                                    ],
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          fit: FlexFit.loose,
                                          child: SizedBox(
                                            height: bugsAttachments.isEmpty ? 20 : 70,
                                            child: ListView.builder(
                                              padding: const EdgeInsets.only(right: 10),
                                              scrollDirection: Axis.horizontal,
                                              shrinkWrap: true,
                                              itemCount: bugsAttachments.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding: const EdgeInsets.only(right: 20),
                                                  child: InkWell(
                                                    onTap: () {
                                                      showAttachmentsDialog(context, bugsAttachments, index);
                                                    },
                                                    child: Image.network(
                                                      bugsAttachments[index],
                                                      width: 70,
                                                      height: 60,
                                                      filterQuality: FilterQuality.medium,
                                                      fit: BoxFit.contain,
                                                      errorBuilder: (context, url, error) => const Icon(Icons.error),
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () async {
                                            try {
                                              FilePickerResult? result = await FilePicker.platform.pickFiles(
                                                  type: FileType.image,
                                                  allowMultiple: true,
                                                  onFileLoading: (FilePickerStatus status) => {
                                                        if (status == FilePickerStatus.picking)
                                                          {
                                                            showDialog(
                                                                context: context,
                                                                barrierDismissible: false,
                                                                builder: (BuildContext context) {
                                                                  return loader();
                                                                })
                                                          }
                                                        else if (status == FilePickerStatus.done)
                                                          {Navigator.of(context).pop()}
                                                        else
                                                          {Navigator.of(context).pop()}
                                                      });
                                              if (result != null) {
                                                List<PlatformFile?> files = result.files.map((path) => path).toList();
                                                List<String?> fileNames = [];
                                                for (var element in files) {
                                                  String base64string = base64Encode(element!.bytes!);
                                                  fileNames.add(element.name);
                                                  imgAttachments.add(base64string);
                                                }
                                                debugPrint("attach ${imgAttachments.length}");
                                                assignState(() {});
                                              } else {
                                                Navigator.of(context).pop();
                                              }
                                            } on PlatformException catch (e) {
                                              debugPrint(e.toString());
                                            } catch (e) {
                                              debugPrint(e.toString());
                                            }
                                          },
                                          child: Container(
                                            decoration: attahcmentIconDecoration,
                                            alignment: Alignment.centerLeft,
                                            padding: const EdgeInsets.all(5),
                                            child: const Icon(
                                              Icons.attachment,
                                              color: CupertinoColors.white,
                                              size: 18,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: SizedBox(
                                            width: 100,
                                            child: Text(status, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: SizedBox(
                                            width: 150,
                                            child: DropdownButtonFormField<String>(
                                              items: bugStatuses.map((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                              decoration: editTextStatusDecoration,
                                              style: textFieldStyle,
                                              value: statusInitialValue,
                                              onChanged: (String? value) {
                                                setState(() {
                                                  statusInitialValue = value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: SizedBox(
                                            width: 100,
                                            child: Text(priority, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: SizedBox(
                                            width: 150,
                                            child: DropdownButtonFormField<String>(
                                              items: bugPriorities.map((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                              decoration: editTextPriorityDecoration,
                                              style: textFieldStyle,
                                              value: priorityInitialValue,
                                              onChanged: (String? value) {
                                                setState(() {
                                                  priorityInitialValue = value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: SizedBox(
                                            width: 100,
                                            child: Text(stage, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: SizedBox(
                                            width: 200,
                                            child: DropdownButtonFormField<DevelopmentStagesList>(
                                              key: _stageKey,
                                              items: developmentStagesListMenuItems,
                                              isExpanded: true,
                                              decoration: editTextPriorityDecoration.copyWith(hintText: selectStage),
                                              style: textFieldStyle,
                                              value: stageInitialValue,
                                              validator: stageDropDownValidator,
                                              onChanged: (DevelopmentStagesList? value) {
                                                setState(() {
                                                  stageInitialValue = value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  type
                                      ? SizedBox()
                                      : Container(
                                          alignment: Alignment.center,
                                          decoration: buttonDecorationGreen,
                                          width: 120,
                                          margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                          child: TextButton(
                                            onPressed: () async {
                                              for (var element in selectedEmployees) {
                                                employeeIds.add(element.id!);
                                              }
                                              if (_formKey.currentState!.validate()) {
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: false,
                                                    builder: (BuildContext context) {
                                                      return loader();
                                                    });

                                                if (taskbuglistData != null) {
                                                  employeeIds = [];
                                                  await viewModel
                                                      .updateraiseaBugAPI(
                                                          titleController.text.toString().trim(),
                                                          taskbuglistData.id!,
                                                          employeeId,
                                                          descController.text.toString().trim(),
                                                          statusInitialValue,
                                                          priorityInitialValue,
                                                          employeeIds,
                                                          imgAttachments,stageInitialValue.id!)
                                                      .then((value) {
                                                    Navigator.of(context).pop();
                                                    if (value != null) {
                                                      if (value.responseStatus == 1) {
                                                        titleController.clear();
                                                        descController.clear();
                                                        Navigator.of(context).pop();
                                                        getData();
                                                        showToast(value.result!);
                                                      } else {
                                                        showToast(value.result!);
                                                      }
                                                    } else {
                                                      showToast(pleaseTryAgain);
                                                    }
                                                  });
                                                } else {
                                                  await viewModel
                                                      .raiseaBugAPI(
                                                          widget.taskId!,
                                                          employeeId,
                                                          widget.projectId!,
                                                          widget.moduleId!,
                                                          titleController.text.toString().trim(),
                                                          imgAttachments,
                                                          descController.text.toString().trim(),
                                                          statusInitialValue,
                                                          priorityInitialValue,
                                                          employeeIds,stageInitialValue.id!)
                                                      .then((value) {
                                                    Navigator.of(context).pop();
                                                    if (value != null) {
                                                      if (value.responseStatus == 1) {
                                                        titleController.clear();
                                                        descController.clear();
                                                        Navigator.of(context).pop();
                                                        getData();
                                                        showToast(value.result!);
                                                      } else {
                                                        showToast(value.result!);
                                                      }
                                                    } else {
                                                      showToast(pleaseTryAgain);
                                                    }
                                                  });
                                                }
                                              }
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                              child: FittedBox(
                                                fit: BoxFit.scaleDown,
                                                child: Text(submit.toUpperCase(), style: newButtonsStyle),
                                              ),
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
  showAddTestCaseDialog(TaskTestCasesList? taskTestCasesListData, bool type) {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (context) {
        String testcasestatusInitialValue = "New";
        TextEditingController titletextController = TextEditingController();
        TextEditingController desctextController = TextEditingController();
        TextEditingController stepstextController = TextEditingController();
        TextEditingController expectedresultsController = TextEditingController();
        TextEditingController actualresultsController = TextEditingController();
        final HtmlEditorController htmlController = HtmlEditorController();
        if (taskTestCasesListData != null) titletextController.text = taskTestCasesListData.title!;
        if (taskTestCasesListData != null) testCaseDescription = taskTestCasesListData.description!;
        if (taskTestCasesListData != null) stepstextController.text = taskTestCasesListData.steps!;
        if (taskTestCasesListData != null) {
          expectedresultsController.text = taskTestCasesListData.expectedResults!;
          actualresultsController.text = taskTestCasesListData.actualResults!;
        }

        if (taskTestCasesListData != null) testcasestatusInitialValue = taskTestCasesListData.tcStatus!;

        final _formKey = GlobalKey<FormState>();

        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(taskTestCasesListData != null ? taskTestCasesListData.title! : addtestcase, style: profilepopHeader),
                  IconButton(
                      icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(35, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: IgnorePointer(
                ignoring: type,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context) / 2,
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  //decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(25, 0, 25, 15),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                // mainAxisSize: MainAxisSize.min,
                                //mainAxisAlignment: MainAxisAlignment.start,
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Title', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 1,
                                            controller: titletextController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Title'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Description', style: addRoleSubStyle),
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            border: Border.all(color: textFieldBorder, width: 1.5),
                                            color: Colors.white),
                                        height: 250,
                                        child: HtmlEditor(
                                          controller: htmlController,
                                          htmlEditorOptions: HtmlEditorOptions(
                                            hint: 'Your text here...',
                                            shouldEnsureVisible: true,
                                            spellCheck: true,
                                            initialText: testCaseDescription,
                                          ),
                                          htmlToolbarOptions: HtmlToolbarOptions(
                                            toolbarPosition: ToolbarPosition.aboveEditor,
                                            toolbarType: ToolbarType.nativeExpandable,
                                            initiallyExpanded: true,
                                            defaultToolbarButtons: const [
                                              StyleButtons(),
                                              FontSettingButtons(fontSizeUnit: false),
                                              FontButtons(clearAll: false),
                                              ColorButtons(),
                                              ListButtons(listStyles: false),
                                              InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                                            ],
                                            onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                                              return true;
                                            },
                                            onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                                              return true;
                                            },
                                            mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                                              return true;
                                            },
                                            mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                                              return true;
                                            },
                                          ),
                                          otherOptions: const OtherOptions(height: 200),
                                          callbacks: Callbacks(
                                              onBeforeCommand: (String? currentHtml) {},
                                              onChangeContent: (String? changed) {},
                                              onChangeCodeview: (String? changed) {},
                                              onChangeSelection: (EditorSettings settings) {},
                                              onDialogShown: () {},
                                              onEnter: () {},
                                              onFocus: () {},
                                              onBlur: () {},
                                              onBlurCodeview: () {},
                                              onInit: () {},
                                              onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                                              onKeyDown: (int? keyCode) {},
                                              onKeyUp: (int? keyCode) {},
                                              onMouseDown: () {},
                                              onMouseUp: () {},
                                              onNavigationRequestMobile: (String url) {
                                                return NavigationActionPolicy.ALLOW;
                                              },
                                              onPaste: () {},
                                              onScroll: () {}),
                                          plugins: [
                                            SummernoteAtMention(
                                                getSuggestionsMobile: (String value) {
                                                  var mentions = <String>['test1', 'test2', 'test3'];
                                                  return mentions.where((element) => element.contains(value)).toList();
                                                },
                                                mentionsWeb: ['test1', 'test2', 'test3'],
                                                onSelect: (String value) {}),
                                          ],
                                        ),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Steps', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 5,
                                            controller: stepstextController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Steps here'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child:
                                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Text('Expected Results', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 5,
                                            controller: expectedresultsController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Expected Results here'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                      Text('Actual Results', style: addRoleSubStyle),
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: TextFormField(
                                            maxLines: 5,
                                            controller: actualresultsController,
                                            textInputAction: TextInputAction.done,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: 'Actual Results here'),
                                            style: textFieldStyle,
                                            validator: emptyTextValidator,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                          child: SizedBox(
                                            width: 100,
                                            child: Text(status, style: addRoleSubStyle),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                          child: SizedBox(
                                            width: 150,
                                            child: DropdownButtonFormField<String>(
                                              items: <String>['New', 'Pass', 'Fail'].map((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                              decoration: editTextStatusDecoration,
                                              style: textFieldStyle,
                                              value: testcasestatusInitialValue,
                                              onChanged: (String? value) {
                                                setState(() {
                                                  testcasestatusInitialValue = value!;
                                                });
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    decoration: buttonDecorationGreen,
                                    width: 120,
                                    margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          htmlController.getText().then((value) async {
                                            showDialog(
                                                context: context,
                                                barrierDismissible: false,
                                                builder: (BuildContext context) {
                                                  return loader();
                                                });
                                            if (taskTestCasesListData != null) {
                                              await viewModel
                                                  .updateTestCaseAPI(
                                                      taskTestCasesListData.id!,
                                                      taskTestCasesListData.title!,
                                                      value,
                                                      stepstextController.text.toString().trim(),
                                                      expectedresultsController.text.toString().trim(),
                                                      actualresultsController.text.toString().trim(),
                                                      employeeId,
                                                      testcasestatusInitialValue)
                                                  .then((value) {
                                                Navigator.of(context).pop();
                                                if (value != null) {
                                                  if (value.responseStatus == 1) {
                                                    titletextController.clear();
                                                    desctextController.clear();
                                                    stepstextController.clear();
                                                    expectedresultsController.clear();
                                                    actualresultsController.clear();
                                                    Navigator.of(context).pop();
                                                    getData();
                                                    showToast(value.result!);
                                                  } else {
                                                    showToast(value.result!);
                                                  }
                                                } else {
                                                  showToast(pleaseTryAgain);
                                                }
                                              });
                                            } else {
                                              await viewModel
                                                  .addaTestCaseAPI(
                                                      titletextController.text.toString().trim(),
                                                      value,
                                                      stepstextController.text.toString().trim(),
                                                      expectedresultsController.text.toString().trim(),
                                                      actualresultsController.text.toString().trim(),
                                                      employeeId,
                                                      widget.projectId!,
                                                      widget.moduleId!,
                                                      widget.taskId!,
                                                      testcasestatusInitialValue)
                                                  .then((value) {
                                                Navigator.of(context).pop();
                                                if (value != null) {
                                                  if (value.responseStatus == 1) {
                                                    titletextController.clear();
                                                    desctextController.clear();
                                                    stepstextController.clear();
                                                    expectedresultsController.clear();
                                                    actualresultsController.clear();
                                                    Navigator.of(context).pop();
                                                    getData();
                                                    showToast(value.result!);
                                                  } else {
                                                    showToast(value.result!);
                                                  }
                                                } else {
                                                  showToast(pleaseTryAgain);
                                                }
                                              });
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(submit.toUpperCase(), style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
  Widget leftItem(LogList.LogsData itemData) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
            child: Tooltip(
              message: itemData.employeeName,
              decoration: toolTipDecoration,
              textStyle: toolTipStyle,
              preferBelow: false,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  backgroundColor: getNameBasedColor(itemData.employeeName!.toUpperCase()[0]),
                  child: Text(itemData.employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                ),
              ),
            ),
            alignment: Alignment.topLeft),
        Expanded(
          flex: 2,
          child: Container(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(children: <TextSpan>[
                    /*TextSpan(text: 'on ', style: projectAddedByStyle),
                    TextSpan(text: itemData.moduleName, style: projectAddedByNameStyle),
                    TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle),*/

                    if (itemData.logType == 'ems_bugs') ...[
                      TextSpan(
                          text: itemData.bugName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              taskHistorysList.clear();
                              await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'bug', itemData.bugId!).then((value) {
                                setState(() {
                                  if (value != null) {
                                    taskHistorysList = value.taskLogsList!;
                                  }
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_tasks') ...[
                      TextSpan(
                          text: itemData.taskName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              taskHistorysList.clear();
                              await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'task', itemData.taskId!).then((value) {
                                setState(() {
                                  if (value != null) {
                                    taskHistorysList = value.taskLogsList!;
                                  }
                                });
                              });
                            }),
                    ],
                    TextSpan(text: ' ${itemData.description} by ', style: projectAddedByStyle),
                    TextSpan(
                        text: '${itemData.employeeName}',
                        style: projectAddedByNameStyle,
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            taskHistorysList.clear();
                            await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'employee', itemData.employeeId!).then((value) {
                              setState(() {
                                if (value != null) {
                                  taskHistorysList = value.taskLogsList!;
                                }
                              });
                            });
                          }),
                  ]),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Row(
                    children: [
                      itemData.taskId != ""
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                //taskCheckBox(itemData),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                                  child: Text(itemData.taskName!, textAlign: TextAlign.start, style: projectDateMenuStyle),
                                )
                              ],
                            )
                          : const SizedBox(),
                      Image.asset("assets/images/calendar.png", color: buttonBg, height: 20, width: 20),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: /*itemData.taskId != ""
                            ? Text("${dayDate(itemData.taskStartDate!)} - ${dayDate(itemData.taskLastDate!)}", style: projectDateMenuStyle)
                            :*/
                            itemData.moduleEndDate! != ""
                                ? Text("${dayDate(itemData.moduleStartDate!)} - ${dayDate(itemData.moduleEndDate!)}", style: projectDateMenuStyle)
                                : Text(dayDate(itemData.moduleStartDate!), style: projectDateMenuStyle),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.topRight,
            child: Text(timeFormatHMS12hr(itemData.createdOn!), style: projectTimeStyle, maxLines: 1, overflow: TextOverflow.ellipsis),
          ),
        ),
      ],
    );
  }
  Widget rightItem(LogList.LogsData itemData) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.topLeft,
            child: Text(timeFormatHMS12hr(itemData.createdOn!), style: projectTimeStyle),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            alignment: Alignment.centerRight,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                RichText(
                  textAlign: TextAlign.end,
                  text: TextSpan(children: <TextSpan>[
                    /*TextSpan(text: 'on ', style: projectAddedByStyle),
                    TextSpan(text: itemData.moduleName, style: projectAddedByNameStyle),
                    TextSpan(text: ', ${itemData.employeeName} added', style: projectAddedByStyle),*/

                    if (itemData.logType == 'ems_bugs') ...[
                      TextSpan(
                          text: itemData.bugName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              taskHistorysList.clear();
                              await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'bug', itemData.bugId!).then((value) {
                                setState(() {
                                  if (value != null) {
                                    taskHistorysList = value.taskLogsList!;
                                  }
                                });
                              });
                            }),
                    ] else if (itemData.logType == 'ems_tasks') ...[
                      TextSpan(
                          text: itemData.taskName,
                          style: projectAddedByNameStyle,
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () async {
                              taskHistorysList.clear();
                              await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'task', itemData.taskId!).then((value) {
                                setState(() {
                                  if (value != null) {
                                    taskHistorysList = value.taskLogsList!;
                                  }
                                });
                              });
                            }),
                    ],
                    TextSpan(text: ' ${itemData.description} by ', style: projectAddedByStyle),
                    TextSpan(
                        text: '${itemData.employeeName}',
                        style: projectAddedByNameStyle,
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            taskHistorysList.clear();
                            await viewModel.taskLogsListAPI(employeeId, widget.taskId!, 'employee', itemData.employeeId!).then((value) {
                              setState(() {
                                if (value != null) {
                                  taskHistorysList = value.taskLogsList!;
                                }
                              });
                            });
                          }),
                  ]),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Row(
                    children: [
                      itemData.taskId != ""
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                //taskCheckBox(itemData),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                                  child: Text(itemData.taskName!, textAlign: TextAlign.start, style: projectDateMenuStyle),
                                )
                              ],
                            )
                          : const SizedBox(),
                      Image.asset("assets/images/calendar.png", color: buttonBg, height: 20, width: 20),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: /*itemData.taskId != ""
                            ? Text("${dayDate(itemData.taskStartDate!)} - ${dayDate(itemData.taskLastDate!)}", style: projectDateMenuStyle)
                            :*/
                            itemData.moduleEndDate! != ""
                                ? Text("${dayDate(itemData.moduleStartDate!)} - ${dayDate(itemData.moduleEndDate!)}", style: projectDateMenuStyle)
                                : Text(dayDate(itemData.moduleStartDate!), style: projectDateMenuStyle),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
            margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
            child: Tooltip(
              message: itemData.employeeName,
              decoration: toolTipDecoration,
              textStyle: toolTipStyle,
              preferBelow: false,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  backgroundColor: getNameBasedColor(itemData.employeeName!.toUpperCase()[0]),
                  child: Text(itemData.employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                ),
              ),
            ),
            alignment: Alignment.topLeft),
      ],
    );
  }
  Widget assignEmployeeField() {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
              child: SizedBox(
                width: 150,
                child: Text(assignEmployee, style: addRoleSubStyle),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 450, maxWidth: 450),
                child: IntrinsicWidth(
                  child: DropdownSearch<EmployeeDetails>.multiSelection(
                    key: _multiKey,
                    mode: Mode.MENU,
                    showSelectedItems: selectedEmployees.isNotEmpty ? true : false,
                    compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                    items: employeesList,
                    showClearButton: true,
                    onChanged: (data) {
                      selectedEmployees = data;
                    },
                    clearButtonSplashRadius: 10,
                    selectedItems: selectedEmployees,
                    showSearchBox: true,
                    dropdownSearchDecoration: editTextEmployeesDecoration,
                    //validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                    dropdownBuilder: (context, selectedItems) {
                      return Wrap(
                          children: selectedItems
                              .map(
                                (e) => selectedItem(e.name!),
                              )
                              .toList());
                    },
                    filterFn: (EmployeeDetails? employee, name) {
                      return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                    },
                    popupItemBuilder: (_, text, isSelected) {
                      return Container(
                        padding: const EdgeInsets.all(10),
                        child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                      );
                    },
                    popupSelectionWidget: (cnt, item, bool isSelected) {
                      return Checkbox(
                          activeColor: buttonBg,
                          hoverColor: buttonBg.withOpacity(0.2),
                          value: isSelected,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                          side: const BorderSide(color: buttonBg),
                          onChanged: (value) {});
                    },
                    onPopupDismissed: () {
                      setState(() {});
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        showWorkingHoursDialog();
        break;
    }
  }
  showWorkingHoursDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return Dialog(
              backgroundColor: Colors.transparent,
              elevation: 10,
              insetPadding: const EdgeInsets.symmetric(horizontal: 40, vertical: 40),
              child: WorkingHoursWidget(
                taskId: widget.taskId,
                taskName: taskName,
                estimatedTime: involvedDuration,
              ));
        }).then((value) {
      getData();
    });
  }
  showEditDetailsDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          final _formKey = GlobalKey<FormState>();
          final _multiKeyWhenDone = GlobalKey<DropdownSearchState<EmployeeDetails>>();
          final _multiKeyAssignedTo = GlobalKey<DropdownSearchState<EmployeeDetails>>();
          final _multiKeyAssignedToTester = GlobalKey<DropdownSearchState<EmployeeDetails>>();
          TextEditingController nameController = TextEditingController();
          TextEditingController extraDetailsController = TextEditingController();
          TextEditingController descriptionController = TextEditingController();
          TextEditingController dateController = TextEditingController();
          TextEditingController estimationController = TextEditingController();
          List<String> notifyEmployeeIds = [], assignedEmployeeIds = [], testerEmployeesIds = [];
          return StatefulBuilder(builder: (context, _setState) {
            _setState(() {
              nameController.text = taskName;
              descriptionController.text = taskDescription;
              extraDetailsController.text = parseHtmlString(extraNotes);
              estimationController.text = taskEstimationTime.toString();


              if (startDateApi == "" && endDateApi == "") {
                dateController.text = " - ";
              } else if (startDateApi != "" && endDateApi == "") {
                dateController.text = "${dayDate(startDateApi)} - ";
              } else if (startDateApi == "" && endDateApi != "") {
                dateController.text = "${dayDate(endDateApi)} - ";
              } else if (startDateApi == "" && endDateApi == "") {
                dateController.text = "${dayDate(startDateApi)} - ${dayDate(endDateApi)}";
              }
            });
            return AlertDialog(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(edit, style: profilepopHeader),
                    IconButton(
                        icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })
                  ],
                ),
                titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
                contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                content: SizedBox(
                  // height: ScreenConfig.height(context) / 2.5,
                  width: ScreenConfig.width(context) / 2,
                  child: SingleChildScrollView(
                    controller: ScrollController(),
                    child: Form(
                      key: _formKey,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        decoration: cardBorder,
                        child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                          TextFormField(
                              autofocus: true,
                              maxLines: 1,
                              controller: nameController,
                              textInputAction: TextInputAction.done,
                              obscureText: false,
                              decoration: editTextTodoDecoration,
                              style: textFieldTodoNameStyle,
                              validator: emptyTextValidator,
                              autovalidateMode: AutovalidateMode.onUserInteraction),
                          const Divider(height: 1, color: dashBoardCardBorderTile),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(describe, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: TextFormField(
                                        minLines: 1,
                                        maxLines: 5,
                                        controller: descriptionController,
                                        obscureText: false,
                                        keyboardType: TextInputType.multiline,
                                        decoration: editTextTodoDescriptionDecoration,
                                        style: textFieldTodoHintStyle,
                                        validator: emptyTextValidator,
                                        autovalidateMode: AutovalidateMode.onUserInteraction),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                            const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        8, 8, 8, 8),
                                    child: Text(estimationTime,
                                        style: textFieldTodoTitlesStyle,
                                        textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                      maxLines: 1,
                                      controller: estimationController,
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.text,
                                      obscureText: false,
                                      decoration: InputDecoration(
                                          fillColor: CupertinoColors.white,
                                          filled: true,
                                          border: border,
                                          isDense: true,
                                          enabledBorder: border,
                                          focusedBorder: border,
                                          errorBorder: errorBorder,
                                          focusedErrorBorder: errorBorder,
                                          contentPadding: editTextPadding,
                                          hintStyle: textFieldHintStyle,
                                          hintText: estimationTime),
                                      style: textFieldStyle,

                                      autovalidateMode: AutovalidateMode.onUserInteraction),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(assignedTo, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: DropdownSearch<EmployeeDetails>.multiSelection(
                                        key: _multiKeyAssignedTo,
                                        mode: Mode.MENU,
                                        showSelectedItems: assignedEmployees.isNotEmpty ? true : false,
                                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                                        items: employeesList,
                                        showClearButton: true,
                                        onChanged: (data) {
                                          assignedEmployees = data;
                                        },
                                        clearButtonSplashRadius: 10,
                                        selectedItems: assignedEmployees,
                                        showSearchBox: true,
                                        dropdownSearchDecoration: editTextEmployeesDecoration,
                                        // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                                        dropdownBuilder: (context, selectedItems) {
                                          return Wrap(
                                              children: selectedItems
                                                  .map(
                                                    (e) => selectedItem(e.name!),
                                                  )
                                                  .toList());
                                        },
                                        filterFn: (EmployeeDetails? employee, name) {
                                          return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                                        },
                                        popupItemBuilder: (_, text, isSelected) {
                                          return Container(
                                            padding: const EdgeInsets.all(10),
                                            child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                                          );
                                        },
                                        popupSelectionWidget: (cnt, item, bool isSelected) {
                                          return Checkbox(
                                              activeColor: buttonBg,
                                              hoverColor: buttonBg.withOpacity(0.2),
                                              value: isSelected,
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                              side: const BorderSide(color: buttonBg),
                                              onChanged: (value) {});
                                        }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(assignedToTester, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: DropdownSearch<EmployeeDetails>.multiSelection(
                                        key: _multiKeyAssignedToTester,
                                        mode: Mode.MENU,
                                        showSelectedItems: testerEmployees.isNotEmpty ? true : false,
                                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                                        items: employeesList,
                                        showClearButton: true,
                                        onChanged: (data) {
                                          testerEmployees = data;
                                        },
                                        clearButtonSplashRadius: 10,
                                        selectedItems: testerEmployees,
                                        showSearchBox: true,
                                        dropdownSearchDecoration: editTextEmployeesDecoration,
                                        // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                                        dropdownBuilder: (context, selectedItems) {
                                          return Wrap(
                                              children: selectedItems
                                                  .map(
                                                    (e) => selectedItem(e.name!),
                                                  )
                                                  .toList());
                                        },
                                        filterFn: (EmployeeDetails? employee, name) {
                                          return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                                        },
                                        popupItemBuilder: (_, text, isSelected) {
                                          return Container(
                                            padding: const EdgeInsets.all(10),
                                            child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                                          );
                                        },
                                        popupSelectionWidget: (cnt, item, bool isSelected) {
                                          return Checkbox(
                                              activeColor: buttonBg,
                                              hoverColor: buttonBg.withOpacity(0.2),
                                              value: isSelected,
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                              side: const BorderSide(color: buttonBg),
                                              onChanged: (value) {});
                                        }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(whenDone, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: DropdownSearch<EmployeeDetails>.multiSelection(
                                        key: _multiKeyWhenDone,
                                        mode: Mode.MENU,
                                        showSelectedItems: notifyEmployees.isNotEmpty ? true : false,
                                        compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                                        items: employeesList,
                                        showClearButton: true,
                                        onChanged: (data) {
                                          notifyEmployees = data;
                                        },
                                        clearButtonSplashRadius: 10,
                                        selectedItems: notifyEmployees,
                                        showSearchBox: true,
                                        dropdownSearchDecoration: editTextEmployeesDecoration,
                                        // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                        // autoValidateMode: AutovalidateMode.onUserInteraction,
                                        dropdownBuilder: (context, selectedItems) {
                                          return Wrap(
                                              children: selectedItems
                                                  .map(
                                                    (e) => selectedItem(e.name!),
                                                  )
                                                  .toList());
                                        },
                                        filterFn: (EmployeeDetails? employee, name) {
                                          return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                                        },
                                        popupItemBuilder: (_, text, isSelected) {
                                          return Container(
                                            padding: const EdgeInsets.all(10),
                                            child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                                          );
                                        },
                                        popupSelectionWidget: (cnt, item, bool isSelected) {
                                          return Checkbox(
                                              activeColor: buttonBg,
                                              hoverColor: buttonBg.withOpacity(0.2),
                                              value: isSelected,
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                              side: const BorderSide(color: buttonBg),
                                              onChanged: (value) {});
                                        }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(dueOn, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: TextFormField(
                                      maxLines: 1,
                                      controller: dateController,
                                      textInputAction: TextInputAction.done,
                                      obscureText: false,
                                      decoration: editTextTodoDateDecoration,
                                      style: textFieldTodoHintStyle,
                                      onTap: () {
                                        // showCalendar(dateController);
                                        showRangeCalendar(dateController);
                                      },
                                      enableInteractiveSelection: false,
                                      focusNode: FocusNode(),
                                      readOnly: true,
                                      // validator: emptyTextValidator,
                                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 20, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                                    child: Text(notes, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: ConstrainedBox(
                                    constraints: const BoxConstraints(),
                                    child: TextFormField(
                                        minLines: 1,
                                        maxLines: 5,
                                        controller: extraDetailsController,
                                        textInputAction: TextInputAction.done,
                                        obscureText: false,
                                        keyboardType: TextInputType.multiline,
                                        decoration: editTextTodoExtraDetailsDecoration,
                                        style: textFieldTodoHintStyle),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ]),
                      ),
                    ),
                  ),
                ),
                actions: [
                  Container(
                    decoration: buttonDecorationGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          assignedEmployeeIds = [];
                          notifyEmployeeIds = [];
                          for (var element in assignedEmployees) {
                            assignedEmployeeIds.add(element.id!);
                          }
                          for (var element in notifyEmployees) {
                            notifyEmployeeIds.add(element.id!);
                          }
                          for (var element in testerEmployees) {
                            testerEmployeesIds.add(element.id!);
                          }
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          await viewModel
                              .updateTaskAPI(
                                  widget.taskId!,
                                  nameController.text.trim(),
                                  descriptionController.text.trim(),
                                  employeeName,
                                  startDate,
                                  endDate,
                                  extraDetailsController.text.trim(),
                                  widget.projectId!,
                                  widget.moduleId!,
                                  employeeId,
                                  assignedEmployeeIds,
                                  notifyEmployeeIds,
                                  testerEmployeesIds,int.parse(estimationController.text != '' ? estimationController.text : '0'))
                              .then((value) {
                            Navigator.pop(context);
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                Navigator.pop(context);
                                showToast(value.result!);
                                getData();
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: Text(update, style: newButtonsStyle),
                      ),
                    ),
                  ),
                  Container(
                    decoration: buttonBorderGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () {
                        _setState(() {
                          notifyEmployees = [];
                          assignedEmployees = [];
                          testerEmployees = [];
                          dateController.clear();
                        });
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: Text(cancel, style: cancelButtonStyle),
                      ),
                    ),
                  ),
                ]);
          });
        });
  }
  showBugDeleteDialog(TaskBugsList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: "$doYouWantDelete bug - ", style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.title}?',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteBugAPI(employeeId, itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              getData();
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
  showTestCasesDeleteDialog(TaskTestCasesList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: "$doYouWantDelete test case - ", style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.title}?',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteTestCaseAPI(employeeId, itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              getData();
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
  showTaskDeleteDialog(TaskData itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'task?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteTaskAPI(itemData.id!, employeeId).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              Navigator.of(context).pop();
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  startBugTimer(dynamic timerObj) {
    const oneSec = const Duration(seconds: 1);

    // var dict = {"sec":bugSeconds,"timer":_timer};
    var obj = timerObj;
    Timer timer3 = obj['timer'];
    int seconds = obj['sec'];
    String status = obj['status'];
    //int second = taskBugsList![position].involvedTime!;
    timer3 = new Timer.periodic(oneSec, (timer3) {
      if (status == "inProgress" || status == "reopened") {
        seconds++;
      } else {
        print("cancel --------");
        timer3.cancel();
      }
      //setState(() {

      obj["sec"] = seconds;
    });
  }
  void startTimer() {
    const oneSec = const Duration(seconds: 1);

    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        // if (_start == 0) {
        //   setState(() {
        //     timer.cancel();
        //   });
        // } else {
        setState(() {
          _start++;

          int mins = Duration(seconds: _start).inMinutes;

          print("${taskEstimationTime} -- ${mins}");

          if (taskEstimationTime != 0) {
            if (mins >= taskEstimationTime) {
              timerColor = newOverDue;
            } else {
              timerColor = newCompleted;
            }
          } else {
            timerColor = newCompleted;
          }
        });
        // }
      },
    );
  }
  void getEmployeesData() {
    final employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: true);
    if (employeeViewModel.employeesResponse == null) {
      employeeViewModel.getEmployeesAPI("project");
      employeesList = employeeViewModel.getEmployees;
    } else {
      employeesList = employeeViewModel.getEmployees;
    }
  }


  void getData() {
    Future.delayed(const Duration(milliseconds: 00), () async {
      notifyEmployees = [];
      assignedEmployees = [];
      testerEmployees = [];
      notifyEmployeesApi = [];
      assignedEmployeesApi = [];
      testerEmployeesApi = [];
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.taskLogsListAPI(employeeId, widget.taskId!, '', '').then((value) {
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {
              taskHistorysList = value.taskLogsList!;
            } else {
              showToast(value.result!);
            }
          }
        });
      });

      await viewModel.viewactivedevelopmentstagesApi(employeeId).then((value) {
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {
              stagesList = viewModel.developmentStagesList();


              for (var element in stagesList) {
                developmentStagesListMenuItems.add(DropdownMenuItem(
                    child: Text(element.stage!), value: element));
              }


            } else {
              showToast(value.result!);
            }
          }
        });
      });

      await viewModel.getBugsListApi(employeeId,widget.taskId!,'','').then((value) {
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {

              taskBugsList = value.taskBugsList!;

            } else {
              showToast(value.result!);
            }
          }
        });
      });
      await viewModel.viewTaskAPI(widget.taskId!, employeeId).then((value) {
        taskCommentController.text = "";
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            taskData = value.taskData!;
            addedByName = taskData!.addedBy!;
            createdBy = taskData!.createdBy!;
            taskName = taskData!.name!;
            taskDescription = taskData!.description!;
            extraNotes = taskData!.notes!;
            // isCompleted = taskData!.status == 3 ? true : false;
            notifyEmployeesList = taskData!.notifyTo!;
            assignedEmployeesList = taskData!.assignedTo!;
            taskCreatedDate = formatAgo(taskData!.createdOn!);
            assignedTestersEmployeesList = taskData!.assignedToTester!;
            taskCommentsList = value.taskCommentsList!;
            // taskHistorysList = value.taskHistoryList!;

            timersList.clear();
            for (var bug in taskBugsList!) {
              Timer _timer = Timer(Duration(seconds: 0), () {});
              var bugSeconds = bug.involvedTime;
              var dict = {"sec": bugSeconds, "timer": _timer, "status": bug.bugStatus};
              timersList.add(dict);
              // startBugTimer(dict);
              // if(bug.bugStatus == "inProgress" || bug.bugStatus == "reopened"){
              //   // bugTime = startBugTimer(taskBugsList![index].involvedTime!,index);
              //   startBugTimer(dict);
              // }else{
              //   //taskBugsList![index].timer!.cancel();
              //   // bugTime = taskBugsList![index].involvedTime!;
              //
              //   //timersList[]
              //
              //
              //
              // }
            }
            taskTestCasesList = value.taskTestCasesList!;
            if (taskData!.endDate! == "" && taskData!.startDate! == "") {
              startDateApi = "";
              startDate = "";
              endDateApi = "";
              endDate = "";
              dueDate = "";
            } else if (taskData!.endDate! == "" && taskData!.startDate! != "") {
              startDateApi = taskData!.startDate!;
              startDate = dateFormatYMDHyphen(taskData!.startDate!);
              endDateApi = "";
              endDate = "";
              dueDate = taskData!.startDate!;
            } else if (taskData!.endDate! != "" && taskData!.startDate! == "") {
              startDateApi = "";
              startDate = "";
              endDateApi = taskData!.endDate!;
              endDate = dateFormatYMDHyphen(taskData!.endDate!);
              dueDate = taskData!.endDate!;
            } else if (taskData!.endDate! != "" && taskData!.startDate! == "") {
              startDateApi = taskData!.startDate!;
              startDate = dateFormatYMDHyphen(taskData!.startDate!);
              endDateApi = taskData!.endDate!;
              endDate = dateFormatYMDHyphen(taskData!.endDate!);
            }
            dueDate = "${taskData!.startDate!} - ${taskData!.endDate!}";
            print("dueDate ---- ${taskData!.startDate} - ${taskData!.endDate}");
            _start = value.taskData!.involvedTime!;
            taskEstimationTime = value.taskEstimationTime!;
            int mins = Duration(seconds: value.taskActivityTime!).inMinutes;
            if (value.taskEstimationTime! != 0) {
              if (value.taskEstimationTime! >= mins) {
                timerColor = newCompleted;
              } else {
                timerColor = newOverDue;
              }
            } else {
              timerColor = newCompleted;
            }
            involvedTime = minutesToHrs(taskData!.involvedTime!);
            _estimationTime = minutesToHrs(taskEstimationTime);
            involvedDuration = taskData!.involvedTime!;
            if (taskData!.status == 3) {
              isCompleted = true;
            } else if (taskData!.status == 1 || taskData!.status == 7 || taskData!.status == 5) {
              // task is in progress
              _timer.cancel();
              showTaskStartedButton = true;
              showTaskOnHoldButton = false;
            } else if (taskData!.status == 2 /* || taskData!.status == 5*/) {
              // task on hold
              showTaskStartedButton = false;
              showTaskOnHoldButton = true;
              _timer.cancel();
              startTimer();
            }
            for (var employee in employeesList) {
              for (var element in notifyEmployeesList) {
                if (employee.id == element.id) {
                  notifyEmployees.add(employee);
                  notifyEmployeesApi.add(employee);
                }
              }
              for (var element in assignedEmployeesList) {
                if (employee.id == element.id) {
                  assignedEmployees.add(employee);
                  assignedEmployeesApi.add(employee);
                  if (element.id == employeeId) {
                    taskEmployeePermission = true;
                  }
                }
              }

              if(assignedTestersEmployeesList.isEmpty){
                testerEmployeePermission = false;
              }else{
                for (var element in assignedTestersEmployeesList) {

                  if(employeeId == element.id){
                    testerEmployeePermission = true;
                  }else{
                    testerEmployeePermission = false;
                  }
                  if (employee.id == element.id) {
                    testerEmployees.add(employee);
                    testerEmployeesApi.add(employee);
                  }
                }
              }

            }
            setState(() {});
          } else {
            showToast(value.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
      });
    });
  }
  void getBugsData(String priority,stageId) {
    Future.delayed(const Duration(milliseconds: 00), () async {

      await viewModel.getBugsListApi(employeeId,widget.taskId!,priority,stageId).then((value) {
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {

              taskBugsList = value.taskBugsList!;

            } else {
              showToast(value.result!);
            }
          }
        });
      });

    });
  }
  updateBugStatus(String bugId, String status) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return loader();
        });
    final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    viewModel.updateBugStatusAPI(bugId, employeeId, status).then((value) {
      Navigator.of(context).pop();
      if (value != null) {
        if (value.responseStatus == 1) {
          getData();
          showToast(value.result!);
        } else {
          showToast(value.result!);
        }
      } else {
        showToast(pleaseTryAgain);
      }
    });
  }
}
extension StringCasingExtension on String {
  String toCapitalized() => length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ').split(' ').map((str) => str.toCapitalized()).join(' ');
}
