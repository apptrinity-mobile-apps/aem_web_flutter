import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/selectable_text_factory_companion_html.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/adaptable_text.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:aem/widgets/on_hover_card.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewTodoScreen extends StatefulWidget {
  final String? projectId, moduleId, projectName;

  const ViewTodoScreen({required this.projectId, required this.moduleId, required this.projectName, Key? key}) : super(key: key);

  @override
  _ViewTodoScreenState createState() => _ViewTodoScreenState();
}

class _ViewTodoScreenState extends State<ViewTodoScreen> {
  String initialViewType = viewAs;
  TextEditingController todoNameController = TextEditingController();
  TextEditingController todoDescriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  late ProjectDetailsNotifier viewModel;
  HtmlEditorController htmlController = HtmlEditorController();
  HtmlEditorController htmlCommentController = HtmlEditorController();
  bool editorVisible = true, commentEditorVisible = true;
  TextEditingController taskCommentController = TextEditingController();
  String employeeId = "",
      employeeName = "",
      endDate = "",
      startDate = "",
      endDateApi = "",
      startDateApi = "",
      todoNameApi = "",
      todoName = "",
      todoDescApi = "",
      todoDesc = "",
      todoNotesApi = "",
      todoNotes = "";
  List<EmployeeDetails> employeesList = [],
      notifyEmployees = [],
      assignedEmployees = [],
      testerEmployees = [],
      notifyEmployeesApi = [],
      assignedEmployeesApi = [],
      testerEmployeesApi = [];
  List<String> notifyEmployeeIds = [], assignedEmployeeIds = [], testerEmployeesIds = [];
  List<AssignedTo> notifyEmployeesList = [], assignedEmployeesList = [], testerEmployeesList = [];
  List<TasksList>? todoTasksList = [];
  List<ModuleComments>? moduleCommentsList = [];
  ModuleDetails? moduleData;
  final _formKey = GlobalKey<FormState>();
  final _multiKeyWhenDone = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _multiKeyAssignedTo = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _multiKeyAssignedToTester = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  bool moduleEditPermission = false, moduleViewPermission = false;

  @override
  void initState() {
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);

    moduleEditPermission = userViewModel.modulesEditPermission;
    moduleViewPermission = userViewModel.modulesViewPermission;
    employeeId = userViewModel.employeeId!;
    employeeName = userViewModel.employeeName!;
    employeesList.add(EmployeeDetails(name: selectEmployee));
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getEmployeesData();
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: IgnorePointer(
            ignoring: moduleEditPermission ? false : true,
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      LeftDrawer(
                        size: sideMenuMaxWidth,
                        onSelectedChanged: (value) {
                          setState(() {
                            // currentRoute = value;
                          });
                        },
                        selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                      ),
                      Expanded(
                        flex: 4,
                        child: Column(
                          children: [const Header(), body()],
                        ),
                      )
                    ],
                  ),
                ),
                const Footer()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > ${widget.projectName} > $projectToDo",
                    maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: [viewTodo(), taskDetails(), commentsDetails()],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(
            children: [
              FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.contain,
                child: Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: CircleAvatar(backgroundColor: newCompleted.withOpacity(0.3), maxRadius: 13, minRadius: 10),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(todoName == "" ? projectToDo : todoName, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                    Text(todoDesc == "" ? description : todoDesc, softWrap: true, style: fragmentDescStyle)
                  ],
                ),
              ),
              Container(
                decoration: buttonDecorationRed,
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: TextButton(
                  onPressed: () {
                    showModuleDeleteDialog(moduleData!);
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text('$delete To-do', style: newButtonsStyle),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget viewTodo() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
      child: Form(
        key: _formKey,
        child: Container(
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          decoration: cardBorder,
          child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
            TextFormField(
                maxLines: 1,
                autofocus: true,
                controller: todoNameController,
                textInputAction: TextInputAction.done,
                obscureText: false,
                decoration: editTextTodoDecoration,
                style: textFieldTodoNameStyle,
                validator: emptyTextValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
            const Divider(height: 1, color: dashBoardCardBorderTile),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(describe, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: TextFormField(
                          minLines: 1,
                          maxLines: 5,
                          controller: todoDescriptionController,
                          obscureText: false,
                          keyboardType: TextInputType.multiline,
                          decoration: editTextTodoDescriptionDecoration,
                          style: textFieldTodoHintStyle,
                          validator: emptyTextValidator,
                          autovalidateMode: AutovalidateMode.onUserInteraction),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(notes, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Visibility(
                      visible: editorVisible,
                      maintainState: true,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5), border: Border.all(color: textFieldBorder, width: 1), color: Colors.white),
                        height: 300,
                        child: HtmlEditor(
                          controller: htmlController,
                          htmlEditorOptions: const HtmlEditorOptions(
                            hint: addExtraDetails,
                            shouldEnsureVisible: false,
                            spellCheck: true,
                          ),
                          htmlToolbarOptions: HtmlToolbarOptions(
                            toolbarPosition: ToolbarPosition.aboveEditor,
                            toolbarType: ToolbarType.nativeExpandable,
                            initiallyExpanded: false,
                            defaultToolbarButtons: const [
                              StyleButtons(),
                              FontSettingButtons(fontSizeUnit: false),
                              FontButtons(clearAll: false),
                              ColorButtons(),
                              ListButtons(listStyles: false),
                              InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                            ],
                            onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                              return true;
                            },
                            onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                              return true;
                            },
                            mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                              return true;
                            },
                            mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                              return true;
                            },
                          ),
                          otherOptions: const OtherOptions(height: 250),
                          callbacks: Callbacks(
                              onBeforeCommand: (String? currentHtml) {},
                              onChangeContent: (String? changed) {},
                              onChangeCodeview: (String? changed) {},
                              onChangeSelection: (EditorSettings settings) {},
                              onDialogShown: () {},
                              onEnter: () {},
                              onFocus: () {},
                              onBlur: () {},
                              onBlurCodeview: () {},
                              onInit: () {},
                              onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                              onKeyDown: (int? keyCode) {},
                              onKeyUp: (int? keyCode) {},
                              onMouseDown: () {},
                              onMouseUp: () {},
                              onNavigationRequestMobile: (String url) {
                                return NavigationActionPolicy.ALLOW;
                              },
                              onPaste: () {},
                              onScroll: () {}),
                          plugins: [
                            SummernoteAtMention(
                                getSuggestionsMobile: (String value) {
                                  var mentions = <String>['test1', 'test2', 'test3'];
                                  return mentions
                                      .where(
                                        (element) => element.contains(value),
                                      )
                                      .toList();
                                },
                                mentionsWeb: ['test1', 'test2', 'test3'],
                                onSelect: (String value) {
                                  debugPrint(value);
                                }),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(assignedTo, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: DropdownSearch<EmployeeDetails>.multiSelection(
                          key: _multiKeyAssignedTo,
                          mode: Mode.MENU,
                          showSelectedItems: assignedEmployees.isNotEmpty ? true : false,
                          compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                          items: employeesList,
                          showClearButton: true,
                          onChanged: (data) {
                            assignedEmployees = data;
                          },
                          clearButtonSplashRadius: 10,
                          selectedItems: assignedEmployees,
                          showSearchBox: true,
                          dropdownSearchDecoration: editTextEmployeesDecoration,
                          // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                          // autoValidateMode: AutovalidateMode.onUserInteraction,
                          dropdownBuilder: (context, selectedItems) {
                            return Wrap(
                                children: selectedItems
                                    .map(
                                      (e) => selectedItem(e.name!),
                                    )
                                    .toList());
                          },
                          filterFn: (EmployeeDetails? employee, name) {
                            return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                          },
                          popupItemBuilder: (_, text, isSelected) {
                            return Container(
                              padding: const EdgeInsets.all(10),
                              child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                            );
                          },
                          popupSelectionWidget: (cnt, item, bool isSelected) {
                            return Checkbox(
                                activeColor: buttonBg,
                                hoverColor: buttonBg.withOpacity(0.2),
                                value: isSelected,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                side: const BorderSide(color: buttonBg),
                                onChanged: (value) {});
                          }),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(assignedToTester, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: DropdownSearch<EmployeeDetails>.multiSelection(
                          key: _multiKeyAssignedToTester,
                          mode: Mode.MENU,
                          showSelectedItems: testerEmployees.isNotEmpty ? true : false,
                          compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                          items: employeesList,
                          showClearButton: true,
                          onChanged: (data) {
                            testerEmployees = data;
                          },
                          clearButtonSplashRadius: 10,
                          selectedItems: testerEmployees,
                          showSearchBox: true,
                          dropdownSearchDecoration: editTextEmployeesDecoration,
                          // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                          // autoValidateMode: AutovalidateMode.onUserInteraction,
                          dropdownBuilder: (context, selectedItems) {
                            return Wrap(
                                children: selectedItems
                                    .map(
                                      (e) => selectedItem(e.name!),
                                    )
                                    .toList());
                          },
                          filterFn: (EmployeeDetails? employee, name) {
                            return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                          },
                          popupItemBuilder: (_, text, isSelected) {
                            return Container(
                              padding: const EdgeInsets.all(10),
                              child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                            );
                          },
                          popupSelectionWidget: (cnt, item, bool isSelected) {
                            return Checkbox(
                                activeColor: buttonBg,
                                hoverColor: buttonBg.withOpacity(0.2),
                                value: isSelected,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                side: const BorderSide(color: buttonBg),
                                onChanged: (value) {});
                          }),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(whenDone, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: DropdownSearch<EmployeeDetails>.multiSelection(
                          key: _multiKeyWhenDone,
                          mode: Mode.MENU,
                          showSelectedItems: notifyEmployees.isNotEmpty ? true : false,
                          compareFn: (item, selectedItem) => item?.id == selectedItem?.id,
                          items: employeesList,
                          showClearButton: true,
                          onChanged: (data) {
                            notifyEmployees = data;
                          },
                          clearButtonSplashRadius: 10,
                          selectedItems: notifyEmployees,
                          showSearchBox: true,
                          dropdownSearchDecoration: editTextEmployeesDecoration,
                          // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                          // autoValidateMode: AutovalidateMode.onUserInteraction,
                          dropdownBuilder: (context, selectedItems) {
                            return Wrap(
                                children: selectedItems
                                    .map(
                                      (e) => selectedItem(e.name!),
                                    )
                                    .toList());
                          },
                          filterFn: (EmployeeDetails? employee, name) {
                            return employee!.name!.toLowerCase().contains(name!.toLowerCase()) ? true : false;
                          },
                          popupItemBuilder: (_, text, isSelected) {
                            return Container(
                              padding: const EdgeInsets.all(10),
                              child: Text(text.name!, style: isSelected ? dropDownSelected : dropDown),
                            );
                          },
                          popupSelectionWidget: (cnt, item, bool isSelected) {
                            return Checkbox(
                                activeColor: buttonBg,
                                hoverColor: buttonBg.withOpacity(0.2),
                                value: isSelected,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                                side: const BorderSide(color: buttonBg),
                                onChanged: (value) {});
                          }),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Text(dueOn, style: textFieldTodoTitlesStyle, textAlign: TextAlign.end),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(),
                      child: TextFormField(
                        maxLines: 1,
                        controller: dateController,
                        textInputAction: TextInputAction.done,
                        obscureText: false,
                        decoration: editTextTodoDateDecoration,
                        style: textFieldTodoHintStyle,
                        onTap: () {
                          setState(() {
                            editorVisible = false;
                            commentEditorVisible = false;
                          });
                          // showCalendar(dateController);
                          showRangeCalendar(dateController);
                        },
                        enableInteractiveSelection: false,
                        focusNode: FocusNode(),
                        readOnly: true,
                        // validator: emptyTextValidator,
                        // autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 20, 10),
              child: Row(
                children: [
                  Container(
                    decoration: buttonDecorationGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          assignedEmployeeIds = [];
                          notifyEmployeeIds = [];
                          testerEmployeesIds = [];
                          for (var element in assignedEmployees) {
                            assignedEmployeeIds.add(element.id!);
                          }
                          for (var element in notifyEmployees) {
                            notifyEmployeeIds.add(element.id!);
                          }
                          for (var element in testerEmployees) {
                            testerEmployeesIds.add(element.id!);
                          }
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          htmlController.getText().then((value) async {
                            await viewModel
                                .updateTodoAPI(todoNameController.text.trim(), todoDescriptionController.text.trim(), startDate, endDate, value,
                                    widget.projectId!, widget.moduleId!, employeeId, assignedEmployeeIds, notifyEmployeeIds, testerEmployeesIds)
                                .then((value) {
                              Navigator.pop(context);
                              if (value != null) {
                                if (value.responseStatus == 1) {
                                  showToast(value.result!);
                                  getData();
                                } else {
                                  showToast(value.result!);
                                }
                              } else {
                                showToast(pleaseTryAgain);
                              }
                            });
                          });
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: Text(update, style: newButtonsStyle),
                      ),
                    ),
                  ),
                  Container(
                    decoration: buttonBorderGreen,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () {
                        setState(() {
                          setState(() {
                            notifyEmployees = [];
                            assignedEmployees = [];
                            testerEmployees = [];
                            dateController.clear();
                          });
                          Navigator.of(context).pop();
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: Text(cancel, style: cancelButtonStyle),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }

  Widget taskDetails() {
    return todoTasksList!.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: todoTasksList!.length,
                itemBuilder: (context, index) {
                  return tasksList(todoTasksList![index]);
                }),
          )
        : const SizedBox();
  }

  Widget commentsDetails() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5),
              child: Text(
                "Comments",
                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
          ),
          commentField(),
          Flexible(
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: const ScrollPhysics(),
                itemCount: moduleCommentsList!.length,
                itemBuilder: (context, index) {
                  return comments(moduleCommentsList![index]);
                }),
          )
        ],
      ),
    );
  }

  Widget commentField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          width: ScreenConfig.width(context),
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          decoration: cardBorder,
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                    child: CircleAvatar(
                      backgroundColor: getNameBasedColor(employeeName.toUpperCase()[0]),
                      child: Text(employeeName.toUpperCase()[0], style: projectIconTextStyle),
                    ),
                    alignment: Alignment.centerLeft),
                Expanded(
                  child: Visibility(
                    visible: commentEditorVisible,
                    maintainState: true,
                    child: SizedBox(
                      height: 200,
                      child: HtmlEditor(
                        controller: htmlCommentController,
                        htmlEditorOptions: const HtmlEditorOptions(
                          hint: addExtraDetails,
                          shouldEnsureVisible: false,
                          spellCheck: true,
                        ),
                        htmlToolbarOptions: HtmlToolbarOptions(
                          toolbarPosition: ToolbarPosition.aboveEditor,
                          toolbarType: ToolbarType.nativeExpandable,
                          initiallyExpanded: false,
                          defaultToolbarButtons: const [
                            StyleButtons(),
                            FontSettingButtons(fontSizeUnit: false),
                            FontButtons(clearAll: false),
                            ColorButtons(),
                            ListButtons(listStyles: false),
                            InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                          ],
                          onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                            return true;
                          },
                          onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                            return true;
                          },
                          mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                            return true;
                          },
                          mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                            return true;
                          },
                        ),
                        otherOptions: const OtherOptions(height: 150),
                        callbacks: Callbacks(
                            onBeforeCommand: (String? currentHtml) {},
                            onChangeContent: (String? changed) {},
                            onChangeCodeview: (String? changed) {},
                            onChangeSelection: (EditorSettings settings) {},
                            onDialogShown: () {},
                            onEnter: () {},
                            onFocus: () {},
                            onBlur: () {},
                            onBlurCodeview: () {},
                            onInit: () {},
                            onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                            onKeyDown: (int? keyCode) {},
                            onKeyUp: (int? keyCode) {},
                            onMouseDown: () {},
                            onMouseUp: () {},
                            onNavigationRequestMobile: (String url) {
                              return NavigationActionPolicy.ALLOW;
                            },
                            onPaste: () {},
                            onScroll: () {}),
                        plugins: [
                          SummernoteAtMention(
                              getSuggestionsMobile: (String value) {
                                var mentions = <String>['test1', 'test2', 'test3'];
                                return mentions
                                    .where(
                                      (element) => element.contains(value),
                                    )
                                    .toList();
                              },
                              mentionsWeb: ['test1', 'test2', 'test3'],
                              onSelect: (String value) {
                                debugPrint(value);
                              }),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          decoration: buttonDecorationGreen,
          margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: TextButton(
            onPressed: () async {
              htmlCommentController.getText().then((value) async {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return loader();
                    });
                await viewModel.addCommentToModuleAPI(value, widget.moduleId!, widget.projectId!, employeeId).then((value) {
                  Navigator.of(context).pop();
                  if (value != null) {
                    if (value.responseStatus == 1) {
                      htmlCommentController.clear();
                      showToast(value.result!);
                      getData();
                    } else {
                      showToast(value.result!);
                    }
                  } else {
                    showToast(pleaseTryAgain);
                  }
                });
              });
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
              child: Text(submit.toUpperCase(), style: newButtonsStyle),
            ),
          ),
        )
      ]),
    );
  }

  Widget tasksList(TasksList itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Checkbox(
              visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: itemData.status == 3 ? true : false,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) async {
                if (value!) {
                  int involvedMins = Duration(seconds: itemData.involvedTime!).inMinutes;
                  if (itemData.estimationTime! < involvedMins) {
                    taskCommentController.text = "";
                    showTaskCommentDialog(itemData);
                  } else {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return loader();
                        });
                    await viewModel.updateTasksBasedOnStatusAPI(employeeId, itemData.id!, StatusText.completeStr.name!, '',0).then((value) {
                      Navigator.of(context).pop();
                      if (value != null) {
                        if (value.responseStatus == 1) {
                          setState(() {
                            getData();
                          });
                          showToast(value.result!);
                        } else {
                          showToast(value.result!);
                        }
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    });
                  }
                } else {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return loader();
                      });
                  await viewModel.updateTasksBasedOnStatusAPI(employeeId, itemData.id!, StatusText.reopenStr.name!, '',0).then((value) {
                    Navigator.of(context).pop();
                    if (value != null) {
                      if (value.responseStatus == 1) {
                        setState(() {
                          getData();
                        });
                        showToast(value.result!);
                      } else {
                        showToast(value.result!);
                      }
                    } else {
                      showToast(pleaseTryAgain);
                    }
                  });
                }
              }),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: Text(itemData.name!, style: completedStyle),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                      child: Tooltip(
                        message: '$delete task',
                        preferBelow: false,
                        decoration: toolTipDecoration,
                        textStyle: toolTipStyle,
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Material(
                          elevation: 0.0,
                          shape: const CircleBorder(),
                          clipBehavior: Clip.none,
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              showTaskDeleteDialog(itemData);
                            },
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 13,
                              child: Center(
                                child: Image.asset('assets/images/delete_icon.png', width: 15, height: 15, color: Colors.black87),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 10, 0),
                      child: SizedBox(
                        height: 25,
                        child: itemData.assignedTo!.isNotEmpty
                            ? ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: itemData.assignedTo!.length > 5 ? 5 : itemData.assignedTo!.length,
                                itemBuilder: (context, index) {
                                  return index == 0
                                      ? Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 1,
                                          child: Tooltip(
                                            message: itemData.assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor: getNameBasedColor(itemData.assignedTo![index].name!.toUpperCase()[0]),
                                                child: Text(itemData.assignedTo![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 0.5,
                                          child: Tooltip(
                                            message: itemData.assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor: getNameBasedColor(itemData.assignedTo![index].name!.toUpperCase()[0]),
                                                child: Text(itemData.assignedTo![index].name!.toUpperCase()[0], style: projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        );
                                })
                            : const SizedBox(),
                      ),
                    ),
                    Image.asset("assets/images/calendar.png", color: buttonBg, height: 20, width: 20),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                      child: Text("${itemData.startDate!} - ${itemData.endDate!}", style: projectDateMenuStyle),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  showTaskCommentDialog(TasksList itemData) {
    final _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 700, maxWidth: 700, minHeight: 5 * 24, maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    validator: emptyTextValidator,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(hintText: reasonExtraTime),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await viewModel
                              .updateTasksBasedOnStatusAPI(employeeId, itemData.id!, StatusText.completeStr.name!, taskCommentController.text,0)
                              .then((value) {
                            Navigator.of(context).pop();
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                setState(() {
                                  getData();
                                });
                                showToast(value.result!);
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  Widget comments(ModuleComments itemData) {
    return Container(
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: CircleAvatar(
                    backgroundColor: getNameBasedColor(itemData.employeeName!.toUpperCase()[0]),
                    child: Text(itemData.employeeName!.toUpperCase()[0], style: projectIconTextStyle),
                  ),
                  alignment: Alignment.center),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AdaptableText(itemData.employeeName!,
                        style: projectAddedByStyle, textAlign: TextAlign.start),
                    HtmlWidget(
                      itemData.comment!,
                      buildAsync: true,
                      enableCaching: false,
                      factoryBuilder: () => SelectableTextCompanion(),
                      onTapUrl: (url) async {
                        if (await canLaunchUrl(Uri.parse(url))) {
                          await launchUrl(Uri.parse(url));
                          return true;
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      renderMode: RenderMode.column,
                      textStyle: completedStyle,
                      onTapImage: (imageData) {},
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AdaptableText(dayAndTime(itemData.createdOn!),
                      style: projectDateMenuStyle, textAlign: TextAlign.center),
                  itemData.employeeId == employeeId
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 0, 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                child: Tooltip(
                                  message: edit,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.none,
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        showEditCommentDialog(itemData);
                                      },
                                      child: OnHoverCard(builder: (isHovered) {
                                        return CircleAvatar(
                                          backgroundColor: CupertinoColors.black,
                                          radius: 15,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 14,
                                            child: Center(
                                              child: Image.asset('assets/images/edit_icon.png',
                                                  width: 13, height: 13, color: isHovered ? buttonBg : CupertinoColors.black),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                child: Tooltip(
                                  message: delete,
                                  preferBelow: false,
                                  decoration: toolTipDecoration,
                                  textStyle: toolTipStyle,
                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Material(
                                    elevation: 0.0,
                                    shape: const CircleBorder(),
                                    clipBehavior: Clip.none,
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        showDeleteDialog(itemData);
                                      },
                                      child: OnHoverCard(builder: (isHovered) {
                                        return CircleAvatar(
                                          backgroundColor: CupertinoColors.black,
                                          radius: 15,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 14,
                                            child: Center(
                                              child: Image.asset('assets/images/delete_icon.png',
                                                  width: 13, height: 13, color: isHovered ? dashBoardCardOverDueText : CupertinoColors.black),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox()
                ],
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
            child: Divider(height: 1, color: dashBoardCardBorderTile),
          )
        ],
      ),
    );
  }

  Future<void> showRangeCalendar(TextEditingController textController) async {
    final DateTimeRange? result = await showDateRangePicker(
        context: context,
        firstDate: DateTime.now(),
        lastDate: DateTime(2100),
        builder: (context, child) {
          return Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400, maxHeight: 500),
              child: child,
            ),
          );
        });
    setState(() {
      editorVisible = true;
      commentEditorVisible = true;
      if (result != null) {
        DateTime _startDate = result.start;
        DateTime _endDate = result.end;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputStartDate = inputFormat.parse(_startDate.toLocal().toString().split(' ')[0]); // removing time from date
        var inputEndDate = inputFormat.parse(_endDate.toLocal().toString().split(' ')[0]); // removing time from date
        startDate = DateFormat('yyyy-MM-dd').format(inputStartDate);
        endDate = DateFormat('yyyy-MM-dd').format(inputEndDate);
        var displayStartDate = DateFormat('E, MMM dd').format(inputStartDate);
        var displayEndDate = DateFormat('E, MMM dd').format(inputEndDate);
        textController.text = "$displayStartDate - $displayEndDate";
      }
    });
  }

  Future<void> showCalendar(TextEditingController textController) async {
    final DateTime? picked = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(2100));
    setState(() {
      editorVisible = true;
      commentEditorVisible = true;
      if (picked != null) {
        selectedDate = picked;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputDate = inputFormat.parse(selectedDate.toLocal().toString().split(' ')[0]); // removing time from date
        var outputFormat = DateFormat('dd/MM/yyyy');
        var outputDate = outputFormat.format(inputDate);
        textController.text = outputDate;
      }
    });
  }

  showDeleteDialog(ModuleComments itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Text('$doYouWantDelete this $comment?', style: logoutContentHeader),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteModuleCommentAPI(itemData.id!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showEditCommentDialog(ModuleComments itemData) {
    HtmlEditorController htmlEditCommentController = HtmlEditorController();
    Future.delayed(const Duration(milliseconds: 1500), () {
      htmlEditCommentController.insertHtml(itemData.comment!.toString());
    });
    setState(() {
      // editorVisible = false;
      commentEditorVisible = false;
    });
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$edit $comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: SizedBox(
              width: ScreenConfig.width(context) / 2.5,
              height: 300,
              child: HtmlEditor(
                controller: htmlEditCommentController,
                htmlEditorOptions: const HtmlEditorOptions(
                  hint: addExtraDetails,
                  shouldEnsureVisible: false,
                  spellCheck: true,
                ),
                htmlToolbarOptions: HtmlToolbarOptions(
                  toolbarPosition: ToolbarPosition.aboveEditor,
                  toolbarType: ToolbarType.nativeExpandable,
                  initiallyExpanded: false,
                  defaultToolbarButtons: const [
                    StyleButtons(),
                    FontSettingButtons(fontSizeUnit: false),
                    FontButtons(clearAll: false),
                    ColorButtons(),
                    ListButtons(listStyles: false),
                    InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                  ],
                  onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                    return true;
                  },
                  onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                    return true;
                  },
                  mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                    return true;
                  },
                  mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                    return true;
                  },
                ),
                otherOptions: const OtherOptions(height: 250),
                callbacks: Callbacks(
                    onBeforeCommand: (String? currentHtml) {},
                    onChangeContent: (String? changed) {},
                    onChangeCodeview: (String? changed) {},
                    onChangeSelection: (EditorSettings settings) {},
                    onDialogShown: () {},
                    onEnter: () {},
                    onFocus: () {},
                    onBlur: () {},
                    onBlurCodeview: () {},
                    onInit: () {},
                    onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                    onKeyDown: (int? keyCode) {},
                    onKeyUp: (int? keyCode) {},
                    onMouseDown: () {},
                    onMouseUp: () {},
                    onNavigationRequestMobile: (String url) {
                      return NavigationActionPolicy.ALLOW;
                    },
                    onPaste: () {},
                    onScroll: () {}),
                plugins: [
                  SummernoteAtMention(
                      getSuggestionsMobile: (String value) {
                        var mentions = <String>['test1', 'test2', 'test3'];
                        return mentions
                            .where(
                              (element) => element.contains(value),
                            )
                            .toList();
                      },
                      mentionsWeb: ['test1', 'test2', 'test3'],
                      onSelect: (String value) {
                        debugPrint(value);
                      }),
                ],
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        htmlEditCommentController.getText().then((value) async {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          await viewModel.updateModuleCommentAPI(itemData.id!, value).then((value) {
                            Navigator.pop(context);
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                showToast(value.result!);
                                setState(() {
                                  // editorVisible = true;
                                  commentEditorVisible = true;
                                });
                                htmlEditCommentController.clear();
                                Navigator.pop(context);
                                getData();
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        setState(() {
                          // editorVisible = true;
                          commentEditorVisible = true;
                        });
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showModuleDeleteDialog(ModuleDetails itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'module?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteModuleAPI(itemData.id!, employeeId).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              // go to back page
                              Navigator.of(context).pop();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showTaskDeleteDialog(TasksList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'task?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteTaskAPI(itemData.id!, employeeId).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void getEmployeesData() {
    final employeeViewModel = Provider.of<EmployeeNotifier>(context, listen: true);
    if (employeeViewModel.employeesResponse == null) {
      employeeViewModel.getEmployeesAPI("project");
      employeesList = employeeViewModel.getEmployees;
    } else {
      employeesList = employeeViewModel.getEmployees;
    }
  }

  void getData() {
    Future.delayed(const Duration(milliseconds: 05), () async {
      notifyEmployees = [];
      notifyEmployeesApi = [];
      assignedEmployees = [];
      assignedEmployeesApi = [];
      testerEmployees = [];
      testerEmployeesApi = [];
      htmlController.clear();
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewModuleAPI(widget.moduleId!, employeeId).then((value) {
        Navigator.of(context).pop();
        setState(() {
          if (value != null) {
            if (value.responseStatus == 1) {
              if (value.moduleData != null) {
                moduleData = value.moduleData!;
                todoNameController.text = moduleData!.name!;
                todoDescriptionController.text = moduleData!.description!;
                htmlController.insertHtml(moduleData!.notes!);
                todoNameApi = moduleData!.name!;
                todoDescApi = moduleData!.description!;
                todoNotesApi = moduleData!.notes!;
                endDateApi = moduleData!.endDate!;
                startDateApi = moduleData!.startDate!;
                todoName = todoNameApi;
                todoDesc = todoDescApi;
                todoNotes = todoNotesApi;
                endDate = moduleData!.endDate!;
                startDate = moduleData!.startDate!;
                var inputFormat = DateFormat('E, MMM dd');
                var inputStartDate = inputFormat.parse(moduleData!.startDate!);
                var inputEndDate = inputFormat.parse(moduleData!.endDate!);
                startDate = DateFormat('yyyy-MM-dd').format(inputStartDate);
                endDate = DateFormat('yyyy-MM-dd').format(inputEndDate);
                dateController.text = "${moduleData!.startDate!} - ${moduleData!.endDate!}";
                notifyEmployeesList = moduleData!.notifyTo!;
                assignedEmployeesList = moduleData!.assignedTo!;
                testerEmployeesList = moduleData!.assignedToTester!;
                todoTasksList = moduleData!.tasksList;
                moduleCommentsList = moduleData!.moduleComments;
                for (var employee in employeesList) {
                  if (notifyEmployeesList.isNotEmpty) {
                    for (var element in notifyEmployeesList) {
                      if (employee.id == element.employeeId) {
                        notifyEmployees.add(employee);
                        notifyEmployeesApi.add(employee);
                      }
                    }
                  }
                  if (assignedEmployeesList.isNotEmpty) {
                    for (var element in assignedEmployeesList) {
                      if (employee.id == element.employeeId) {
                        assignedEmployees.add(employee);
                        assignedEmployeesApi.add(employee);
                      }
                    }
                  }
                  if (testerEmployeesList.isNotEmpty) {
                    for (var element in testerEmployeesList) {
                      if (employee.id == element.employeeId) {
                        testerEmployees.add(employee);
                        testerEmployeesApi.add(employee);
                      }
                    }
                  }
                }
                _multiKeyWhenDone.currentState!.changeSelectedItems(notifyEmployees);
                _multiKeyAssignedTo.currentState!.changeSelectedItems(assignedEmployees);
                _multiKeyAssignedToTester.currentState!.changeSelectedItems(testerEmployees);
              }
            } else {
              moduleData = null;
            }
          } else {
            moduleData = null;
          }
        });
      });
    });
  }
}
