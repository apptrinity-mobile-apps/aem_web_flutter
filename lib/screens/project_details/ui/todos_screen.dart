import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/todos_list_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/ui/view_todo_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TodosScreen extends StatefulWidget {
  final String? projectId, projectName;

  const TodosScreen(
      {required this.projectId, required this.projectName, Key? key})
      : super(key: key);

  @override
  _TodosScreenState createState() => _TodosScreenState();
}

class _TodosScreenState extends State<TodosScreen> {
  String initialViewType = viewAs;
  bool addNewTaskVisible = false;
  TextEditingController todoNameController = TextEditingController();
  TextEditingController todoExtraDetailsController = TextEditingController();
  TextEditingController todoDescriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController dateDialogController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  late ProjectDetailsNotifier viewModel;
  String employeeId = "", employeeName = "", endDate = "", startDate = "";
  List<EmployeeDetails> employeesList = [],
      notifyEmployees = [],
      assignedEmployees = [],
      testerEmployees = [];
  List<String> notifyEmployeeIds = [],
      assignedEmployeeIds = [],
      testerEmployeesIds = [];
  List<ModuleDetails> moduleDetails = [];
  final _formKey = GlobalKey<FormState>();
  final _multiKeyWhenDone = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _multiKeyAssignedTo = GlobalKey<DropdownSearchState<EmployeeDetails>>();
  final _multiKeyAssignedToTester =
      GlobalKey<DropdownSearchState<EmployeeDetails>>();
  HtmlEditorController htmlController = HtmlEditorController();
  bool editorVisible = true, isLoadingCompleted = false;
  bool moduleAddPermission = false,
      moduleEditPermission = false,
      moduleViewPermission = false;
  bool taskAddPermission = false,
      taskEditPermission = false,
      taskViewPermission = false;
  TextEditingController taskCommentController = TextEditingController();

  @override
  void initState() {
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    moduleAddPermission = userViewModel.modulesAddPermission;
    moduleEditPermission = userViewModel.modulesEditPermission;
    moduleViewPermission = userViewModel.modulesViewPermission;

    taskAddPermission = userViewModel.tasksAddPermission;
    taskEditPermission = userViewModel.tasksEditPermission;
    taskViewPermission = userViewModel.tasksViewPermission;
    print(
        "permissions ${taskAddPermission} ${taskEditPermission} ${taskViewPermission}");

    employeeId = userViewModel.employeeId!;
    employeeName = userViewModel.employeeName!;
    employeesList.add(EmployeeDetails(name: selectEmployee));
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getEmployeesData();
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), body()],
                      ),
                    )
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > ${widget.projectName} > $projectToDo",
                    maxLines: 1,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Divider(color: dashBoardCardBorderTile),
                            ),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Column(
                                    children: [
                                      addNewTaskVisible
                                          ? addNewTODO()
                                          : const SizedBox(),
                                      Visibility(
                                        visible: isLoadingCompleted,
                                        child: moduleDetails.isNotEmpty
                                            ? ListView.builder(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        25, 0, 25, 0),
                                                shrinkWrap: true,
                                                physics: const ScrollPhysics(),
                                                itemCount: moduleDetails.length,
                                                itemBuilder: (context, index) {
                                                  return todosList(index,
                                                      moduleDetails[index]);
                                                })
                                            : Center(
                                                child: Text(noDataAvailable,
                                                    style: logoutHeader),
                                              ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: backButton(),
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(projectToDo,
                        maxLines: 1,
                        softWrap: true,
                        style: fragmentHeaderStyle),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 3),
                      child: Text(descriptionTodos,
                          softWrap: true, style: fragmentDescStyle),
                    )
                  ],
                ),
              ),
              Expanded(
                child: FittedBox(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.scaleDown,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      /*Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      alignment: Alignment.center,
                      child: viewAsDropDown(),
                    ),*/
                      Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              if (moduleAddPermission) {
                                addNewTaskVisible = true;
                                notifyEmployees = [];
                                assignedEmployees = [];
                                testerEmployees = [];
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(newList, style: newButtonsStyle),
                            ),
                          ),
                        ),
                      ),
                      /*InkWell(
                      onTap: () {},
                      child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                    )*/
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget todosList(int position, ModuleDetails itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: CircleAvatar(
                      backgroundColor: newCompleted.withOpacity(0.3),
                      maxRadius: 13,
                      minRadius: 10)),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            child: InkWell(
                                onTap: () {
                                  if (moduleViewPermission) {
                                    Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                        pageBuilder: (context, animation1,
                                                animation2) =>
                                            ViewTodoScreen(
                                                projectId:
                                                    widget.projectId!,
                                                moduleId: itemData.id,
                                                projectName: itemData.name),
                                        transitionDuration:
                                            const Duration(seconds: 0),
                                        reverseTransitionDuration:
                                            const Duration(seconds: 0),
                                      ),
                                    ).then((value) {
                                      getData();
                                    });
                                  } else {
                                    showToast(noPermissionToAccess);
                                  }
                                },
                                child: RichText(
                                  textAlign: TextAlign.start,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                        text: itemData.name!,
                                        style: newCompletedHeaderStyle),
                                    TextSpan(
                                      text: "   ${formatAgo(itemData.createdOn!)}",
                                      style: projectDateMenuStyle,
                                    ),
                                  ]),
                                )
                                //Text(itemData.name!, style: newCompletedHeaderStyle, softWrap: true,),
                                ),
                          ),
                          moduleEditPermission
                              ? Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(4, 0, 4, 0),
                                  child: Tooltip(
                                    message: delete,
                                    preferBelow: false,
                                    decoration: toolTipDecoration,
                                    textStyle: toolTipStyle,
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Material(
                                      elevation: 0.0,
                                      shape: const CircleBorder(),
                                      clipBehavior: Clip.none,
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          showModuleDeleteDialog(itemData);
                                        },
                                        child: CircleAvatar(
                                          backgroundColor:
                                              CupertinoColors.black,
                                          radius: 14,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 13,
                                            child: Center(
                                              child: Image.asset(
                                                  'assets/images/delete_icon.png',
                                                  width: 12,
                                                  height: 12,
                                                  color: CupertinoColors.black),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                      Text(
                        "${itemData.completedTasksCount}/${itemData.taskCount} $projectCompleted",
                        style: completedStyle,
                        softWrap: true,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: itemData.tasksList!.isNotEmpty
                            ? ListView.builder(
                                shrinkWrap: true,
                                itemCount: itemData.tasksList!.length,
                                itemBuilder: (context, index) {
                                  return tasksList(
                                      itemData, itemData.tasksList![index]);
                                })
                            : const SizedBox(),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: buttonBg, width: 1),
                                borderRadius: BorderRadius.circular(50)),
                            child: TextButton(
                              onPressed: () {
                                if (taskAddPermission) {
                                  showAddTaskDialog(itemData.id);
                                } else {
                                  showToast(noPermissionToAccess);
                                }
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Text(addTask, style: addTodoStyle),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget tasksList(ModuleDetails moduleDetails, TasksList itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Checkbox(
              visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: itemData.status == 3 ? true : false,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) async {
                if (taskEditPermission) {
                  if (value!) {
                    int involvedMins =
                        Duration(seconds: itemData.involvedTime!).inMinutes;
                    if (itemData.estimationTime! < involvedMins) {
                      taskCommentController.text = "";
                      showTaskCommentDialog(itemData);
                    } else {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return loader();
                          });
                      await viewModel
                          .updateTasksBasedOnStatusAPI(employeeId, itemData.id!,
                              StatusText.completeStr.name!, '',0)
                          .then((value) {
                        Navigator.of(context).pop();
                        if (value != null) {
                          if (value.responseStatus == 1) {
                            getData();
                            showToast(value.result!);
                          } else {
                            showToast(value.result!);
                          }
                        } else {
                          showToast(pleaseTryAgain);
                        }
                      });
                    }
                  } else {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return loader();
                        });
                    await viewModel
                        .updateTasksBasedOnStatusAPI(employeeId, itemData.id!,
                            StatusText.reopenStr.name!, '',0)
                        .then((value) {
                      Navigator.of(context).pop();
                      if (value != null) {
                        if (value.responseStatus == 1) {
                          getData();
                          showToast(value.result!);
                        } else {
                          showToast(value.result!);
                        }
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    });
                  }
                } else {
                  showToast(noPermissionToAccess);
                }
              }),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: TextButton(
                              onPressed: () {
                                if (taskViewPermission) {
                                  Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      pageBuilder:
                                          (context, animation1, animation2) =>
                                              ViewTaskScreen(
                                                  projectId: widget.projectId!,
                                                  moduleId: moduleDetails.id,
                                                  taskId: itemData.id),
                                      transitionDuration:
                                          const Duration(seconds: 0),
                                      reverseTransitionDuration:
                                          const Duration(seconds: 0),
                                    ),
                                  ).then((value) {
                                    getData();
                                  });
                                } else {
                                  showToast(noPermissionToAccess);
                                }
                              },
                              child:
                                  Text(itemData.name!, style: completedStyle),
                            ),
                          ),
                          if (itemData.status == 0) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(inActive,
                                  style: completedStyle.copyWith(
                                      color: footerText)),
                            )
                          ] else if (itemData.status == 1) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(newStr,
                                  style: completedStyle.copyWith(
                                      color: newTaskText)),
                            )
                          ] else if (itemData.status == 2) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(open,
                                  style: completedStyle.copyWith(
                                      color: newOnGoing)),
                            )
                          ] else if (itemData.status == 3) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(projectCompleted,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 4) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(close,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 5) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(reOpen,
                                  style: completedStyle.copyWith(
                                      color: newReOpened)),
                            )
                          ] else if (itemData.status == 6) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(delete,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 7) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(hold,
                                  style: completedStyle.copyWith(
                                      color: dashBoardCardOverDueText)),
                            )
                          ],
                        ],
                      ),
                    ),
                    taskEditPermission
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                            child: Tooltip(
                              message: '$delete task',
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.none,
                                color: Colors.transparent,
                                child: InkWell(
                                  onTap: () {
                                    showTaskDeleteDialog(itemData);
                                  },
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 13,
                                    child: Center(
                                      child: Image.asset(
                                          'assets/images/delete_icon.png',
                                          width: 15,
                                          height: 15,
                                          color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),

                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                      child: Text(
                        formatAgo(itemData.createdOn!),
                        style: projectDateMenuStyle,
                        softWrap: true,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 8, 10),
                      child: Text(
                          "Estimated Time - ${minutesToHrs(itemData.estimationTime!)}",
                          style: completedStyle),
                    ),
                    itemData.involvedTime != 0
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 8, 10),
                            child: Text(
                                "Task Time - ${minutesToHrs(Duration(seconds: itemData.involvedTime!).inMinutes).toString()}",
                                style: completedStyle),
                          )
                        : SizedBox(),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 10, 0),
                      child: SizedBox(
                        height: 25,
                        child: itemData.assignedTo!.isNotEmpty
                            ? ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: itemData.assignedTo!.length > 5
                                    ? 5
                                    : itemData.assignedTo!.length,
                                itemBuilder: (context, index) {
                                  return index == 0
                                      ? Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 1,
                                          child: Tooltip(
                                            message: itemData
                                                .assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                    getNameBasedColor(itemData
                                                        .assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0]),
                                                child: Text(
                                                    itemData.assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0],
                                                    style:
                                                        projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 0.5,
                                          child: Tooltip(
                                            message: itemData
                                                .assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                    getNameBasedColor(itemData
                                                        .assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0]),
                                                child: Text(
                                                    itemData.assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0],
                                                    style:
                                                        projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        );
                                })
                            : const SizedBox(),
                      ),
                    ),
                    Image.asset("assets/images/calendar.png",
                        color: buttonBg, height: 20, width: 20),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                      child: Text(
                          "${itemData.startDate!} - ${itemData.endDate!}",
                          style: projectDateMenuStyle),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget viewAsDropDown() {
    return ConstrainedBox(
      constraints: const BoxConstraints(
          minWidth: 120, maxWidth: 120, minHeight: 40, maxHeight: 40),
      child: IntrinsicWidth(
        child: DropdownButtonFormField(
          items: getViewTypesList,
          isExpanded: true,
          isDense: true,
          decoration: dropDownDecoration,
          style: textFieldStyle,
          value: initialViewType,
          onChanged: (String? value) {
            setState(() {
              initialViewType = value!;
            });
          },
        ),
      ),
    );
  }

  Widget addNewTODO() {
    return Form(
      key: _formKey,
      child: Container(
        width: ScreenConfig.width(context),
        margin: const EdgeInsets.fromLTRB(25, 10, 25, 10),
        decoration: cardBorder,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                  maxLines: 1,
                  controller: todoNameController,
                  autofocus: true,
                  textInputAction: TextInputAction.done,
                  obscureText: false,
                  decoration: editTextTodoDecoration,
                  style: textFieldTodoNameStyle,
                  validator: emptyTextValidator,
                  autovalidateMode: AutovalidateMode.onUserInteraction),
              const Divider(height: 1, color: dashBoardCardBorderTile),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(describe,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(),
                        child: TextFormField(
                            minLines: 1,
                            maxLines: 5,
                            controller: todoDescriptionController,
                            obscureText: false,
                            keyboardType: TextInputType.multiline,
                            decoration: editTextTodoDescriptionDecoration,
                            style: textFieldTodoHintStyle,
                            validator: emptyTextValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(notes,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Visibility(
                        visible: editorVisible,
                        maintainState: true,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border:
                                  Border.all(color: textFieldBorder, width: 1),
                              color: Colors.white),
                          height: 300,
                          child: HtmlEditor(
                            controller: htmlController,
                            htmlEditorOptions: const HtmlEditorOptions(
                              hint: addExtraDetails,
                              shouldEnsureVisible: false,
                              spellCheck: true,
                            ),
                            htmlToolbarOptions: HtmlToolbarOptions(
                              toolbarPosition: ToolbarPosition.aboveEditor,
                              toolbarType: ToolbarType.nativeExpandable,
                              initiallyExpanded: false,
                              defaultToolbarButtons: const [
                                StyleButtons(),
                                FontSettingButtons(fontSizeUnit: false),
                                FontButtons(clearAll: false),
                                ColorButtons(),
                                ListButtons(listStyles: false),
                                InsertButtons(
                                    video: false,
                                    audio: false,
                                    table: false,
                                    hr: false,
                                    otherFile: false),
                              ],
                              onButtonPressed: (ButtonType type, bool? status,
                                  Function()? updateStatus) {
                                return true;
                              },
                              onDropdownChanged: (DropdownType type,
                                  dynamic changed,
                                  Function(dynamic)? updateSelectedItem) {
                                return true;
                              },
                              mediaLinkInsertInterceptor:
                                  (String url, InsertFileType type) {
                                return true;
                              },
                              mediaUploadInterceptor: (PlatformFile file,
                                  InsertFileType type) async {
                                return true;
                              },
                            ),
                            otherOptions: const OtherOptions(height: 250),
                            callbacks: Callbacks(
                                onBeforeCommand: (String? currentHtml) {},
                                onChangeContent: (String? changed) {},
                                onChangeCodeview: (String? changed) {},
                                onChangeSelection: (EditorSettings settings) {},
                                onDialogShown: () {},
                                onEnter: () {},
                                onFocus: () {},
                                onBlur: () {},
                                onBlurCodeview: () {},
                                onInit: () {},
                                onImageUploadError: (FileUpload? file,
                                    String? base64Str, UploadError error) {},
                                onKeyDown: (int? keyCode) {},
                                onKeyUp: (int? keyCode) {},
                                onMouseDown: () {},
                                onMouseUp: () {},
                                onNavigationRequestMobile: (String url) {
                                  return NavigationActionPolicy.ALLOW;
                                },
                                onPaste: () {},
                                onScroll: () {}),
                            plugins: [
                              SummernoteAtMention(
                                  getSuggestionsMobile: (String value) {
                                    var mentions = <String>[
                                      'test1',
                                      'test2',
                                      'test3'
                                    ];
                                    return mentions
                                        .where((element) =>
                                            element.contains(value))
                                        .toList();
                                  },
                                  mentionsWeb: ['test1', 'test2', 'test3'],
                                  onSelect: (String value) {
                                    debugPrint(value);
                                  }),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(assignedTo,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(),
                        child: DropdownSearch<EmployeeDetails>.multiSelection(
                            key: _multiKeyAssignedTo,
                            mode: Mode.MENU,
                            showSelectedItems:
                                assignedEmployees.isNotEmpty ? true : false,
                            compareFn: (item, selectedItem) =>
                                item?.id == selectedItem?.id,
                            items: employeesList,
                            showClearButton: true,
                            onChanged: (data) {
                              assignedEmployees = data;
                            },
                            clearButtonSplashRadius: 10,
                            selectedItems: assignedEmployees,
                            showSearchBox: true,
                            dropdownSearchDecoration:
                                editTextEmployeesDecoration,
                            // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                            // autoValidateMode: AutovalidateMode.onUserInteraction,
                            dropdownBuilder: (context, selectedItems) {
                              return Wrap(
                                  children: selectedItems
                                      .map((e) => selectedItem(e.name!))
                                      .toList());
                            },
                            filterFn: (EmployeeDetails? employee, name) {
                              return employee!.name!
                                      .toLowerCase()
                                      .contains(name!.toLowerCase())
                                  ? true
                                  : false;
                            },
                            popupItemBuilder: (_, text, isSelected) {
                              return Container(
                                padding: const EdgeInsets.all(10),
                                child: Text(text.name!,
                                    style: isSelected
                                        ? dropDownSelected
                                        : dropDown),
                              );
                            },
                            popupSelectionWidget: (cnt, item, bool isSelected) {
                              return Checkbox(
                                  activeColor: buttonBg,
                                  hoverColor: buttonBg.withOpacity(0.2),
                                  value: isSelected,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4)),
                                  side: const BorderSide(color: buttonBg),
                                  onChanged: (value) {});
                            }),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(assignedToTester,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(),
                        child: DropdownSearch<EmployeeDetails>.multiSelection(
                            key: _multiKeyAssignedToTester,
                            mode: Mode.MENU,
                            showSelectedItems:
                                testerEmployees.isNotEmpty ? true : false,
                            compareFn: (item, selectedItem) =>
                                item?.id == selectedItem?.id,
                            items: employeesList,
                            showClearButton: true,
                            onChanged: (data) {
                              testerEmployees = data;
                            },
                            clearButtonSplashRadius: 10,
                            selectedItems: testerEmployees,
                            showSearchBox: true,
                            dropdownSearchDecoration:
                                editTextEmployeesDecoration,
                            // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                            // autoValidateMode: AutovalidateMode.onUserInteraction,
                            dropdownBuilder: (context, selectedItems) {
                              return Wrap(
                                  children: selectedItems
                                      .map((e) => selectedItem(e.name!))
                                      .toList());
                            },
                            filterFn: (EmployeeDetails? employee, name) {
                              return employee!.name!
                                      .toLowerCase()
                                      .contains(name!.toLowerCase())
                                  ? true
                                  : false;
                            },
                            popupItemBuilder: (_, text, isSelected) {
                              return Container(
                                padding: const EdgeInsets.all(10),
                                child: Text(text.name!,
                                    style: isSelected
                                        ? dropDownSelected
                                        : dropDown),
                              );
                            },
                            popupSelectionWidget: (cnt, item, bool isSelected) {
                              return Checkbox(
                                  activeColor: buttonBg,
                                  hoverColor: buttonBg.withOpacity(0.2),
                                  value: isSelected,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4)),
                                  side: const BorderSide(color: buttonBg),
                                  onChanged: (value) {});
                            }),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(whenDone,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(),
                        child: DropdownSearch<EmployeeDetails>.multiSelection(
                            key: _multiKeyWhenDone,
                            mode: Mode.MENU,
                            showSelectedItems:
                                notifyEmployees.isNotEmpty ? true : false,
                            compareFn: (item, selectedItem) =>
                                item?.id == selectedItem?.id,
                            items: employeesList,
                            showClearButton: true,
                            onChanged: (data) {
                              notifyEmployees = data;
                            },
                            clearButtonSplashRadius: 10,
                            selectedItems: notifyEmployees,
                            showSearchBox: true,
                            dropdownSearchDecoration:
                                editTextEmployeesDecoration,
                            // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                            // autoValidateMode: AutovalidateMode.onUserInteraction,
                            dropdownBuilder: (context, selectedItems) {
                              return Wrap(
                                  children: selectedItems
                                      .map((e) => selectedItem(e.name!))
                                      .toList());
                            },
                            filterFn: (EmployeeDetails? employee, name) {
                              return employee!.name!
                                      .toLowerCase()
                                      .contains(name!.toLowerCase())
                                  ? true
                                  : false;
                            },
                            popupItemBuilder: (_, text, isSelected) {
                              return Container(
                                padding: const EdgeInsets.all(10),
                                child: Text(text.name!,
                                    style: isSelected
                                        ? dropDownSelected
                                        : dropDown),
                              );
                            },
                            popupSelectionWidget: (cnt, item, bool isSelected) {
                              return Checkbox(
                                  activeColor: buttonBg,
                                  hoverColor: buttonBg.withOpacity(0.2),
                                  value: isSelected,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4)),
                                  side: const BorderSide(color: buttonBg),
                                  onChanged: (value) {});
                            }),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Text(dueOn,
                            style: textFieldTodoTitlesStyle,
                            textAlign: TextAlign.end),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(),
                        child: TextFormField(
                          maxLines: 1,
                          controller: dateController,
                          textInputAction: TextInputAction.done,
                          obscureText: false,
                          decoration: editTextTodoDateDecoration,
                          style: textFieldTodoHintStyle,
                          onTap: () {
                            // showCalendar(dateController);
                            showRangeCalendar(dateController);
                            setState(() {
                              editorVisible = false;
                            });
                          },
                          enableInteractiveSelection: false,
                          focusNode: FocusNode(),
                          readOnly: true,
                          // validator: emptyTextValidator,
                          // autovalidateMode: AutovalidateMode.onUserInteraction,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 10, 20, 10),
                child: Row(
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            assignedEmployeeIds = [];
                            notifyEmployeeIds = [];
                            testerEmployeesIds = [];
                            for (var element in assignedEmployees) {
                              assignedEmployeeIds.add(element.id!);
                            }
                            for (var element in notifyEmployees) {
                              notifyEmployeeIds.add(element.id!);
                            }
                            for (var element in testerEmployees) {
                              testerEmployeesIds.add(element.id!);
                            }
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            htmlController.getText().then((value) async {
                              await viewModel
                                  .createTodoAPI(
                                      todoNameController.text.trim(),
                                      todoDescriptionController.text.trim(),
                                      startDate,
                                      endDate,
                                      value,
                                      widget.projectId!,
                                      employeeId,
                                      assignedEmployeeIds,
                                      notifyEmployeeIds,
                                      testerEmployeesIds)
                                  .then((value) {
                                Navigator.pop(context);
                                if (value != null) {
                                  if (value.responseStatus == 1) {
                                    showToast(value.result!);
                                    getData();
                                    setState(() {
                                      addNewTaskVisible = false;
                                      notifyEmployees = [];
                                      assignedEmployees = [];
                                      testerEmployees = [];
                                      dateController.clear();
                                    });
                                  } else {
                                    showToast(value.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            });
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(addThisList, style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: buttonBorderGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            addNewTaskVisible = false;
                            notifyEmployees = [];
                            assignedEmployees = [];
                            testerEmployees = [];
                            dateController.clear();
                            _formKey.currentState!.reset();
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(cancel, style: cancelButtonStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ]),
      ),
    );
  }

  showTaskCommentDialog(TasksList itemData) {
    final _formKey = GlobalKey<FormState>();
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: 700,
                    maxWidth: 700,
                    minHeight: 5 * 24,
                    maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    validator: emptyTextValidator,
                    decoration: editTextProjectDescriptionDecoration.copyWith(
                        hintText: reasonExtraTime),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await viewModel
                              .updateTasksBasedOnStatusAPI(
                                  employeeId,
                                  itemData.id!,
                                  StatusText.completeStr.name!,
                                  '',0)
                              .then((value) {
                            Navigator.of(context).pop();
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                getData();
                                showToast(value.result!);
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showAddTaskDialog(String? moduleId) {
    TextEditingController nameController = TextEditingController();
    TextEditingController descriptionController = TextEditingController();
    TextEditingController dateController = TextEditingController();
    TextEditingController estimationController = TextEditingController();
    HtmlEditorController htmlController = HtmlEditorController();
    bool editorVisible = true;
    final _formKey = GlobalKey<FormState>();
    final _multiKeyWhenDone = GlobalKey<DropdownSearchState<EmployeeDetails>>();
    final _multiKeyAssignedTo =
        GlobalKey<DropdownSearchState<EmployeeDetails>>();
    final _multiKeyAssignedToTester =
        GlobalKey<DropdownSearchState<EmployeeDetails>>();
    List<EmployeeDetails> notifyEmployees = [],
        assignedEmployees = [],
        testerEmployees = [];
    List<String> notifyEmployeeIds = [],
        assignedEmployeeIds = [],
        testerEmployeesIds = [];
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext _context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: StatefulBuilder(builder: (context, _setState) {
              return SimpleDialog(
                  backgroundColor: Colors.white,
                  contentPadding: const EdgeInsets.all(16),
                  title: const Text('Add a Task'),
                  titleTextStyle: textFieldTodoNameStyle,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  children: <Widget>[
                    SizedBox(
                      width: ScreenConfig.width(context) / 2.25,
                      child: Form(
                        key: _formKey,
                        child: Container(
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          decoration: cardBorder,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                    autofocus: true,
                                    maxLines: 1,
                                    controller: nameController,
                                    textInputAction: TextInputAction.done,
                                    obscureText: false,
                                    decoration: editTextTodoDecoration,
                                    style: textFieldTodoNameStyle,
                                    validator: emptyTextValidator,
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction),
                                const Divider(
                                    height: 1, color: dashBoardCardBorderTile),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(describe,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: TextFormField(
                                              minLines: 1,
                                              maxLines: 5,
                                              controller: descriptionController,
                                              obscureText: false,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              decoration:
                                                  editTextTodoDescriptionDecoration,
                                              style: textFieldTodoHintStyle,
                                              validator: emptyTextValidator,
                                              autovalidateMode: AutovalidateMode
                                                  .onUserInteraction),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(notes,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Visibility(
                                          visible: editorVisible,
                                          maintainState: true,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                border: Border.all(
                                                    color: textFieldBorder,
                                                    width: 1),
                                                color: Colors.white),
                                            height: 300,
                                            child: HtmlEditor(
                                              controller: htmlController,
                                              htmlEditorOptions:
                                                  const HtmlEditorOptions(
                                                hint: addExtraDetails,
                                                shouldEnsureVisible: false,
                                                spellCheck: true,
                                              ),
                                              htmlToolbarOptions:
                                                  HtmlToolbarOptions(
                                                toolbarPosition:
                                                    ToolbarPosition.aboveEditor,
                                                toolbarType: ToolbarType
                                                    .nativeExpandable,
                                                initiallyExpanded: true,
                                                defaultToolbarButtons: const [
                                                  StyleButtons(),
                                                  FontSettingButtons(
                                                      fontSizeUnit: false),
                                                  FontButtons(clearAll: false),
                                                  ColorButtons(),
                                                  ListButtons(
                                                      listStyles: false),
                                                  InsertButtons(
                                                      video: false,
                                                      audio: false,
                                                      table: false,
                                                      hr: false,
                                                      otherFile: false),
                                                ],
                                                onButtonPressed: (ButtonType
                                                        type,
                                                    bool? status,
                                                    Function()? updateStatus) {
                                                  return true;
                                                },
                                                onDropdownChanged: (DropdownType
                                                        type,
                                                    dynamic changed,
                                                    Function(dynamic)?
                                                        updateSelectedItem) {
                                                  return true;
                                                },
                                                mediaLinkInsertInterceptor:
                                                    (String url,
                                                        InsertFileType type) {
                                                  return true;
                                                },
                                                mediaUploadInterceptor:
                                                    (PlatformFile file,
                                                        InsertFileType
                                                            type) async {
                                                  return true;
                                                },
                                              ),
                                              otherOptions: const OtherOptions(
                                                  height: 250),
                                              callbacks: Callbacks(
                                                  onBeforeCommand:
                                                      (String? currentHtml) {},
                                                  onChangeContent:
                                                      (String? changed) {},
                                                  onChangeCodeview:
                                                      (String? changed) {},
                                                  onChangeSelection:
                                                      (EditorSettings
                                                          settings) {},
                                                  onDialogShown: () {},
                                                  onEnter: () {},
                                                  onFocus: () {},
                                                  onBlur: () {},
                                                  onBlurCodeview: () {},
                                                  onInit: () {},
                                                  onImageUploadError:
                                                      (FileUpload? file,
                                                          String? base64Str,
                                                          UploadError error) {},
                                                  onKeyDown: (int? keyCode) {},
                                                  onKeyUp: (int? keyCode) {},
                                                  onMouseDown: () {},
                                                  onMouseUp: () {},
                                                  onNavigationRequestMobile:
                                                      (String url) {
                                                    return NavigationActionPolicy
                                                        .ALLOW;
                                                  },
                                                  onPaste: () {},
                                                  onScroll: () {}),
                                              plugins: [
                                                SummernoteAtMention(
                                                    getSuggestionsMobile:
                                                        (String value) {
                                                      var mentions = <String>[
                                                        'test1',
                                                        'test2',
                                                        'test3'
                                                      ];
                                                      return mentions
                                                          .where((element) =>
                                                              element.contains(
                                                                  value))
                                                          .toList();
                                                    },
                                                    mentionsWeb: [
                                                      'test1',
                                                      'test2',
                                                      'test3'
                                                    ],
                                                    onSelect: (String value) {
                                                      debugPrint(value);
                                                    }),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(estimationTime,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                            maxLines: 1,
                                            controller: estimationController,
                                            textInputAction: TextInputAction.next,
                                            keyboardType: TextInputType.text,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                                fillColor: CupertinoColors.white,
                                                filled: true,
                                                border: border,
                                                isDense: true,
                                                enabledBorder: border,
                                                focusedBorder: border,
                                                errorBorder: errorBorder,
                                                focusedErrorBorder: errorBorder,
                                                contentPadding: editTextPadding,
                                                hintStyle: textFieldHintStyle,
                                                hintText: estimationTimeHint),
                                            style: textFieldStyle,
                                            autovalidateMode: AutovalidateMode.onUserInteraction),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(assignedTo,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyAssignedTo,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  assignedEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                assignedEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: assignedEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(assignedToTester,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyAssignedToTester,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  testerEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                testerEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: testerEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(whenDone,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyWhenDone,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  notifyEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                notifyEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: notifyEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(dueOn,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: TextFormField(
                                            maxLines: 1,
                                            controller: dateController,
                                            textInputAction:
                                                TextInputAction.done,
                                            obscureText: false,
                                            decoration:
                                                editTextTodoDateDecoration,
                                            style: textFieldTodoHintStyle,
                                            onTap: () async {
                                              _setState(() {
                                                editorVisible = false;
                                              });
                                              final DateTimeRange? result =
                                                  await showDateRangePicker(
                                                      context: context,
                                                      firstDate: DateTime.now(),
                                                      lastDate: DateTime(2100),
                                                      builder:
                                                          (context, child) {
                                                        return Center(
                                                          child: ConstrainedBox(
                                                            constraints:
                                                                const BoxConstraints(
                                                                    maxWidth:
                                                                        400,
                                                                    maxHeight:
                                                                        500),
                                                            child: child,
                                                          ),
                                                        );
                                                      });
                                              _setState(() {
                                                editorVisible = true;
                                                if (result != null) {
                                                  DateTime _startDate =
                                                      result.start;
                                                  DateTime _endDate =
                                                      result.end;
                                                  var inputFormat =
                                                      DateFormat('yyyy-MM-dd');
                                                  var inputStartDate =
                                                      inputFormat.parse(_startDate
                                                              .toLocal()
                                                              .toString()
                                                              .split(' ')[
                                                          0]); // removing time from date
                                                  var inputEndDate =
                                                      inputFormat.parse(_endDate
                                                              .toLocal()
                                                              .toString()
                                                              .split(' ')[
                                                          0]); // removing time from date
                                                  startDate = DateFormat(
                                                          'yyyy-MM-dd')
                                                      .format(inputStartDate);
                                                  endDate =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(inputEndDate);
                                                  var displayStartDate =
                                                      DateFormat('E, MMM dd')
                                                          .format(
                                                              inputStartDate);
                                                  var displayEndDate =
                                                      DateFormat('E, MMM dd')
                                                          .format(inputEndDate);
                                                  dateController.text =
                                                      "$displayStartDate - $displayEndDate";
                                                }
                                              });
                                            },
                                            enableInteractiveSelection: false,
                                            focusNode: FocusNode(),
                                            readOnly: true,
                                            // validator: emptyTextValidator,
                                            // autovalidateMode: AutovalidateMode.onUserInteraction,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(30, 10, 20, 10),
                                  child: Row(
                                    children: [
                                      Container(
                                        decoration: buttonDecorationGreen,
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: TextButton(
                                          onPressed: () async {
                                            if (_formKey.currentState!
                                                .validate()) {
                                              assignedEmployeeIds = [];
                                              notifyEmployeeIds = [];
                                              testerEmployeesIds = [];
                                              for (var element
                                                  in assignedEmployees) {
                                                assignedEmployeeIds
                                                    .add(element.id!);
                                              }
                                              for (var element
                                                  in notifyEmployees) {
                                                notifyEmployeeIds
                                                    .add(element.id!);
                                              }
                                              for (var element
                                                  in testerEmployees) {
                                                testerEmployeesIds
                                                    .add(element.id!);
                                              }
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder:
                                                      (BuildContext context) {
                                                    return loader();
                                                  });
                                              htmlController
                                                  .getText()
                                                  .then((value) async {
                                                await viewModel
                                                    .addTaskAPI(
                                                        nameController.text
                                                            .trim(),
                                                        descriptionController
                                                            .text
                                                            .trim(),
                                                        employeeName,
                                                        startDate,
                                                        endDate,
                                                        value,
                                                        widget.projectId!,
                                                        moduleId!,
                                                        employeeId,
                                                        assignedEmployeeIds,
                                                        notifyEmployeeIds,
                                                        testerEmployeesIds,int.parse(estimationController.text != '' ? estimationController.text : '0'))
                                                    .then((value) {
                                                  Navigator.pop(context);
                                                  if (value != null) {
                                                    if (value.responseStatus ==
                                                        1) {
                                                      showToast(value.result!);
                                                      _setState(() {
                                                        notifyEmployees = [];
                                                        assignedEmployees = [];
                                                        testerEmployees = [];
                                                        dateController.clear();
                                                      });
                                                      Navigator.pop(context);
                                                      getData();
                                                    } else {
                                                      showToast(value.result!);
                                                    }
                                                  } else {
                                                    showToast(pleaseTryAgain);
                                                  }
                                                });
                                              });
                                            }
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 8, 16, 8),
                                            child: FittedBox(
                                              fit: BoxFit.scaleDown,
                                              child: Text(addThisList,
                                                  style: newButtonsStyle),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: buttonBorderGreen,
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: TextButton(
                                          onPressed: () {
                                            _setState(() {
                                              notifyEmployees = [];
                                              assignedEmployees = [];
                                              testerEmployees = [];
                                              dateController.clear();
                                            });
                                            Navigator.of(context).pop();
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 8, 16, 8),
                                            child: FittedBox(
                                              fit: BoxFit.scaleDown,
                                              child: Text(cancel,
                                                  style: cancelButtonStyle),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ]),
                        ),
                      ),
                    )
                  ]);
            }),
          );
        });
  }

  List<DropdownMenuItem<String>> get getViewTypesList {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(
          child: Text(viewAs, maxLines: 1, softWrap: true), value: viewAs),
    ];
    return menuItems;
  }

  showModuleDeleteDialog(ModuleDetails itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: 'module?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteModuleAPI(itemData.id!, employeeId)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showTaskDeleteDialog(TasksList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: 'task?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteTaskAPI(itemData.id!, employeeId)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  Future<void> showRangeCalendar(TextEditingController textController) async {
    final DateTimeRange? result = await showDateRangePicker(
        context: context,
        firstDate: DateTime.now(),
        lastDate: DateTime(2100),
        builder: (context, child) {
          return Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 400, maxHeight: 500),
              child: child,
            ),
          );
        });
    setState(() {
      editorVisible = true;
      if (result != null) {
        DateTime _startDate = result.start;
        DateTime _endDate = result.end;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputStartDate = inputFormat.parse(_startDate
            .toLocal()
            .toString()
            .split(' ')[0]); // removing time from date
        var inputEndDate = inputFormat.parse(_endDate
            .toLocal()
            .toString()
            .split(' ')[0]); // removing time from date
        startDate = DateFormat('yyyy-MM-dd').format(inputStartDate);
        endDate = DateFormat('yyyy-MM-dd').format(inputEndDate);
        var displayStartDate = DateFormat('E, MMM dd').format(inputStartDate);
        var displayEndDate = DateFormat('E, MMM dd').format(inputEndDate);
        textController.text = "$displayStartDate - $displayEndDate";
      }
    });
  }

  void getEmployeesData() {
    final employeeViewModel =
        Provider.of<EmployeeNotifier>(context, listen: true);
    if (employeeViewModel.employeesResponse == null) {
      employeeViewModel.getEmployeesAPI("project");
      employeesList = employeeViewModel.getEmployees;
    } else {
      employeesList = employeeViewModel.getEmployees;
    }
  }

  void getData() {
    Future.delayed(const Duration(milliseconds: 00), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel
          .viewAllTodosAPI(widget.projectId!, employeeId)
          .then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.moduleDetails!.isNotEmpty) {
                moduleDetails = value.moduleDetails!;
              }
            });
          } else {
            moduleDetails = [];
            // showToast(value.result!);
          }
        } else {
          moduleDetails = [];
          // showToast(pleaseTryAgain);
        }
        setState(() {
          isLoadingCompleted = true;
        });
      });
    });
  }
}
