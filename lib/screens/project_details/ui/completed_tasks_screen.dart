import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/tasks_based_on_status_response_model.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/ui/view_todo_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/status_text_enum.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/multi_select_selected_item_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CompletedTasksScreen extends StatefulWidget {
  final String? projectId, projectName;

  const CompletedTasksScreen(
      {required this.projectId, required this.projectName, Key? key})
      : super(key: key);

  @override
  _CompletedTasksScreenState createState() => _CompletedTasksScreenState();
}

class _CompletedTasksScreenState extends State<CompletedTasksScreen> {
  TextEditingController taskCommentController = TextEditingController();
  String initialViewType = viewAs;
  late ProjectDetailsNotifier viewModel;
  String employeeId = "", projectName = "", projectDescription = "";
  DateTime selectedDate = DateTime.now();
  String endDate = "", startDate = "", employeeName = "";
  List<EmployeeDetails> employeesList = [];

  @override
  void initState() {
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    employeeName = userViewModel.employeeName!;
    employeesList.add(EmployeeDetails(name: selectEmployee));
    startDate = getTodayDate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getEmployeesData();
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), body()],
                      ),
                    )
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > ${widget.projectName} > $projectCompleted",
                    maxLines: 1,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Divider(color: dashBoardCardBorderTile),
                            ),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: allTasksList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: Image.asset("assets/images/completed.png",
                    width: 30, height: 30),
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(projectCompleted,
                      maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionCompleted,
                        softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            /*Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      alignment: Alignment.center,
                      child: viewAsDropDown(),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                    )
                  ],
                ),
              ),
            )*/
          ],
        ),
      ],
    );
  }

  Widget allTasksList() {
    return FutureBuilder(
        future: viewModel.getTasksBasedOnStatusAPI(
            widget.projectId!, employeeId, StatusText.completeStr.name!),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              showToast('error ${snapshot.error}');
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as TaskListStatusBasedResponseModel;
              if (data.responseStatus == 1) {
                if (data.modulesData!.isNotEmpty) {
                  return ListView.builder(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      controller: ScrollController(),
                      itemCount: data.modulesData!.length,
                      itemBuilder: (context, index) {
                        return todosList(index, data.modulesData![index]);
                      });
                } else {
                  showToast(data.result!);
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                showToast(data.result!);
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              showToast(pleaseTryAgain);
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            showToast(pleaseTryAgain);
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget todosList(int position, ModulesData itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: CircleAvatar(
                      backgroundColor: newCompleted.withOpacity(0.3),
                      maxRadius: 13,
                      minRadius: 10)),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder: (context, animation1,
                                            animation2) =>
                                        ViewTodoScreen(
                                            projectId: widget.projectId!,
                                            moduleId: itemData.moduleId,
                                            projectName: itemData.moduleName),
                                    transitionDuration:
                                        const Duration(seconds: 0),
                                    reverseTransitionDuration:
                                        const Duration(seconds: 0),
                                  ),
                                ).then((value) {
                                  // set state to reload data , reloads data in futureBuilder
                                  setState(() {});
                                });
                              },
                              child: RichText(
                                textAlign: TextAlign.start,
                                text: TextSpan(children: <TextSpan>[
                                  TextSpan(
                                      text: itemData.moduleName!,
                                      style: newCompletedHeaderStyle),
                                  TextSpan(
                                    text: "   ${formatAgo(itemData.createdOn!)}",
                                    style: projectDateMenuStyle,
                                  ),
                                ]),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                            child: Tooltip(
                              message: delete,
                              preferBelow: false,
                              decoration: toolTipDecoration,
                              textStyle: toolTipStyle,
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(),
                                clipBehavior: Clip.none,
                                color: Colors.transparent,
                                child: InkWell(
                                  onTap: () {
                                    showModuleDeleteDialog(itemData);
                                  },
                                  child: CircleAvatar(
                                    backgroundColor: CupertinoColors.black,
                                    radius: 14,
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 13,
                                      child: Center(
                                        child: Image.asset(
                                            'assets/images/delete_icon.png',
                                            width: 12,
                                            height: 12,
                                            color: CupertinoColors.black),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Text(
                        "${itemData.completedTasksCount}/${itemData.totalTasksCount} $projectCompleted",
                        style: completedStyle,
                        softWrap: true,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: itemData.tasksData!.isNotEmpty
                            ? ListView.builder(
                                shrinkWrap: true,
                                itemCount: itemData.tasksData!.length,
                                itemBuilder: (context, index) {
                                  return tasksList(
                                      itemData, itemData.tasksData![index]);
                                })
                            : const SizedBox(),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: buttonBg, width: 1),
                                borderRadius: BorderRadius.circular(50)),
                            child: TextButton(
                              onPressed: () {
                                showAddTaskDialog(itemData.moduleId);
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Text(addTask, style: addTodoStyle),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget tasksList(ModulesData moduleDetails, TasksData itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Checkbox(
              visualDensity: const VisualDensity(horizontal: -4, vertical: -4),
              activeColor: buttonBg,
              hoverColor: buttonBg.withOpacity(0.2),
              value: itemData.status == 3 ? true : false,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              side: const BorderSide(color: buttonBg),
              onChanged: (value) async {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return loader();
                    });
                if (value!) {
                  int involvedMins =
                      Duration(seconds: itemData.involvedTime!).inMinutes;
                  if (itemData.estimationTime! < involvedMins) {
                    taskCommentController.text = "";
                    showTaskCommentDialog(itemData);
                  } else {
                    await viewModel
                        .updateTasksBasedOnStatusAPI(employeeId, itemData.id!,
                            StatusText.completeStr.name!, '',0)
                        .then((value) {
                      Navigator.of(context).pop();
                      if (value != null) {
                        if (value.responseStatus == 1) {
                          setState(() {});
                          showToast(value.result!);
                        } else {
                          showToast(value.result!);
                        }
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    });
                  }
                } else {
                  await viewModel
                      .updateTasksBasedOnStatusAPI(employeeId, itemData.id!,
                          StatusText.reopenStr.name!, '',0)
                      .then((value) {
                    Navigator.of(context).pop();
                    if (value != null) {
                      if (value.responseStatus == 1) {
                        setState(() {});
                        showToast(value.result!);
                      } else {
                        showToast(value.result!);
                      }
                    } else {
                      showToast(pleaseTryAgain);
                    }
                  });
                }
              }),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder: (context, animation1,
                                            animation2) =>
                                        ViewTaskScreen(
                                            projectId: widget.projectId!,
                                            moduleId: moduleDetails.moduleId,
                                            taskId: itemData.id),
                                    transitionDuration:
                                        const Duration(seconds: 0),
                                    reverseTransitionDuration:
                                        const Duration(seconds: 0),
                                  ),
                                ).then((value) {
                                  // set state to reload data , reloads data in futureBuilder
                                  setState(() {});
                                });
                              },
                              child:
                                  Text(itemData.name!, style: completedStyle),
                            ),
                          ),
                          if (itemData.status == 0) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(inActive,
                                  style: completedStyle.copyWith(
                                      color: footerText)),
                            )
                          ] else if (itemData.status == 1) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(newStr,
                                  style: completedStyle.copyWith(
                                      color: newTaskText)),
                            )
                          ] else if (itemData.status == 2) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(open,
                                  style: completedStyle.copyWith(
                                      color: newOnGoing)),
                            )
                          ] else if (itemData.status == 3) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(projectCompleted,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 4) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(close,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 5) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(reOpen,
                                  style: completedStyle.copyWith(
                                      color: newReOpened)),
                            )
                          ] else if (itemData.status == 6) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(delete,
                                  style: completedStyle.copyWith(
                                      color: newCompleted)),
                            )
                          ] else if (itemData.status == 7) ...[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Text(hold,
                                  style: completedStyle.copyWith(
                                      color: dashBoardCardOverDueText)),
                            )
                          ],
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                      child: Tooltip(
                        message: '$delete task',
                        preferBelow: false,
                        decoration: toolTipDecoration,
                        textStyle: toolTipStyle,
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Material(
                          elevation: 0.0,
                          shape: const CircleBorder(),
                          clipBehavior: Clip.none,
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              showTaskDeleteDialog(itemData);
                            },
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 13,
                              child: Center(
                                child: Image.asset(
                                    'assets/images/delete_icon.png',
                                    width: 15,
                                    height: 15,
                                    color: Colors.black87),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                      child: Text(
                        formatAgo(itemData.createdOn!),
                        style: projectDateMenuStyle,
                        softWrap: true,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 8, 10),
                      child: Text(
                          "Estimated Time - ${minutesToHrs(itemData.estimationTime!)}",
                          style: completedStyle),
                    ),
                    itemData.involvedTime != 0
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 8, 10),
                            child: Text(
                                "Task Time - ${minutesToHrs(Duration(seconds: itemData.involvedTime!).inMinutes).toString()}",
                                style: completedStyle),
                          )
                        : SizedBox(),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 10, 0),
                      child: SizedBox(
                        height: 25,
                        child: itemData.assignedTo!.isNotEmpty
                            ? ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: itemData.assignedTo!.length > 5
                                    ? 5
                                    : itemData.assignedTo!.length,
                                itemBuilder: (context, index) {
                                  return index == 0
                                      ? Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 1,
                                          child: Tooltip(
                                            message: itemData
                                                .assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                    getNameBasedColor(itemData
                                                        .assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0]),
                                                child: Text(
                                                    itemData.assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0],
                                                    style:
                                                        projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Align(
                                          alignment: Alignment.centerRight,
                                          widthFactor: 0.5,
                                          child: Tooltip(
                                            message: itemData
                                                .assignedTo![index].name!,
                                            decoration: toolTipDecoration,
                                            textStyle: toolTipStyle,
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                    getNameBasedColor(itemData
                                                        .assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0]),
                                                child: Text(
                                                    itemData.assignedTo![index]
                                                        .name!
                                                        .toUpperCase()[0],
                                                    style:
                                                        projectIconTextStyle),
                                              ),
                                            ),
                                          ),
                                        );
                                })
                            : const SizedBox(),
                      ),
                    ),
                    Image.asset("assets/images/calendar.png",
                        color: buttonBg, height: 20, width: 20),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                        child: Text(
                            "${itemData.startDate!} - ${itemData.endDate!}",
                            style: projectDateMenuStyle))
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget viewAsDropDown() {
    return ConstrainedBox(
      constraints: const BoxConstraints(
          minWidth: 120, maxWidth: 120, minHeight: 40, maxHeight: 40),
      child: IntrinsicWidth(
        child: DropdownButtonFormField(
          items: getViewTypesList,
          isExpanded: true,
          isDense: true,
          decoration: dropDownDecoration,
          style: textFieldStyle,
          value: initialViewType,
          onChanged: (String? value) {
            setState(() {
              initialViewType = value!;
            });
          },
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> get getViewTypesList {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(
          child: Text(viewAs, softWrap: true), value: viewAs),
    ];
    return menuItems;
  }

  showAddTaskDialog(String? moduleId) {
    TextEditingController nameController = TextEditingController();
    TextEditingController descriptionController = TextEditingController();
    TextEditingController extraDetailsController = TextEditingController();
    TextEditingController dateController = TextEditingController();
    final _formKey = GlobalKey<FormState>();
    final _multiKeyWhenDone = GlobalKey<DropdownSearchState<EmployeeDetails>>();
    final _multiKeyAssignedTo =
        GlobalKey<DropdownSearchState<EmployeeDetails>>();
    final _multiKeyAssignedToTester =
        GlobalKey<DropdownSearchState<EmployeeDetails>>();
    List<EmployeeDetails> notifyEmployees = [],
        assignedEmployees = [],
        testerEmployees = [];
    List<String> notifyEmployeeIds = [],
        assignedEmployeeIds = [],
        testerEmployeesIds = [];
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext _context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: StatefulBuilder(builder: (context, _setState) {
              return SimpleDialog(
                  backgroundColor: Colors.white,
                  contentPadding: const EdgeInsets.all(16),
                  title: const Text('Add a Task'),
                  titleTextStyle: textFieldTodoNameStyle,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  children: <Widget>[
                    SizedBox(
                      width: ScreenConfig.width(context) / 2.25,
                      child: Form(
                        key: _formKey,
                        child: Container(
                          width: ScreenConfig.width(context),
                          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                          decoration: cardBorder,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                    autofocus: true,
                                    maxLines: 1,
                                    controller: nameController,
                                    textInputAction: TextInputAction.done,
                                    obscureText: false,
                                    decoration: editTextTodoDecoration,
                                    style: textFieldTodoNameStyle,
                                    validator: emptyTextValidator,
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction),
                                const Divider(
                                    height: 1, color: dashBoardCardBorderTile),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(describe,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: TextFormField(
                                              minLines: 1,
                                              maxLines: 5,
                                              controller: descriptionController,
                                              obscureText: false,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              decoration:
                                                  editTextTodoDescriptionDecoration,
                                              style: textFieldTodoHintStyle,
                                              validator: emptyTextValidator,
                                              autovalidateMode: AutovalidateMode
                                                  .onUserInteraction),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(assignedTo,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyAssignedTo,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  assignedEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                assignedEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: assignedEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(assignedToTester,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyAssignedToTester,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  testerEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                testerEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: testerEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(whenDone,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: DropdownSearch<
                                                  EmployeeDetails>.multiSelection(
                                              key: _multiKeyWhenDone,
                                              mode: Mode.MENU,
                                              showSelectedItems:
                                                  notifyEmployees.isNotEmpty
                                                      ? true
                                                      : false,
                                              compareFn: (item, selectedItem) =>
                                                  item?.id == selectedItem?.id,
                                              items: employeesList,
                                              showClearButton: true,
                                              onChanged: (data) {
                                                notifyEmployees = data;
                                              },
                                              clearButtonSplashRadius: 10,
                                              selectedItems: notifyEmployees,
                                              showSearchBox: true,
                                              dropdownSearchDecoration:
                                                  editTextEmployeesDecoration,
                                              // validator: (list) => list == null || list.isEmpty ? emptyFieldError : null,
                                              // autoValidateMode: AutovalidateMode.onUserInteraction,
                                              dropdownBuilder:
                                                  (context, selectedItems) {
                                                return Wrap(
                                                    children: selectedItems
                                                        .map((e) =>
                                                            selectedItem(
                                                                e.name!))
                                                        .toList());
                                              },
                                              filterFn:
                                                  (EmployeeDetails? employee,
                                                      name) {
                                                return employee!.name!
                                                        .toLowerCase()
                                                        .contains(
                                                            name!.toLowerCase())
                                                    ? true
                                                    : false;
                                              },
                                              popupItemBuilder:
                                                  (_, text, isSelected) {
                                                return Container(
                                                  padding:
                                                      const EdgeInsets.all(10),
                                                  child: Text(text.name!,
                                                      style: isSelected
                                                          ? dropDownSelected
                                                          : dropDown),
                                                );
                                              },
                                              popupSelectionWidget:
                                                  (cnt, item, bool isSelected) {
                                                return Checkbox(
                                                    activeColor: buttonBg,
                                                    hoverColor: buttonBg
                                                        .withOpacity(0.2),
                                                    value: isSelected,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        4)),
                                                    side: const BorderSide(
                                                        color: buttonBg),
                                                    onChanged: (value) {});
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(dueOn,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: TextFormField(
                                            maxLines: 1,
                                            controller: dateController,
                                            textInputAction:
                                                TextInputAction.done,
                                            obscureText: false,
                                            decoration:
                                                editTextTodoDateDecoration,
                                            style: textFieldTodoHintStyle,
                                            onTap: () async {
                                              final DateTime? picked =
                                                  await showDatePicker(
                                                      context: context,
                                                      initialDate:
                                                          DateTime.now(),
                                                      firstDate: DateTime.now(),
                                                      lastDate: DateTime(2100));
                                              if (picked != null) {
                                                _setState(() {
                                                  selectedDate = picked;
                                                  var inputFormat =
                                                      DateFormat('yyyy-MM-dd');
                                                  var inputDate = inputFormat
                                                      .parse(selectedDate
                                                              .toLocal()
                                                              .toString()
                                                              .split(' ')[
                                                          0]); // removing time from date
                                                  var outputFormat =
                                                      DateFormat('dd/MM/yyyy');
                                                  var outputDate = outputFormat
                                                      .format(inputDate);
                                                  endDate =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(inputDate);
                                                  dateController.text =
                                                      outputDate;
                                                });
                                              }
                                            },
                                            enableInteractiveSelection: false,
                                            focusNode: FocusNode(),
                                            readOnly: true,
                                            // validator: emptyTextValidator,
                                            // autovalidateMode: AutovalidateMode.onUserInteraction,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 20, 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 8, 8, 8),
                                          child: Text(notes,
                                              style: textFieldTodoTitlesStyle,
                                              textAlign: TextAlign.end),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: ConstrainedBox(
                                          constraints: const BoxConstraints(),
                                          child: TextFormField(
                                              minLines: 1,
                                              maxLines: 5,
                                              controller:
                                                  extraDetailsController,
                                              textInputAction:
                                                  TextInputAction.done,
                                              obscureText: false,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              decoration:
                                                  editTextTodoExtraDetailsDecoration,
                                              style: textFieldTodoHintStyle),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(30, 10, 20, 10),
                                  child: Row(
                                    children: [
                                      Container(
                                        decoration: buttonDecorationGreen,
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: TextButton(
                                          onPressed: () async {
                                            if (_formKey.currentState!
                                                .validate()) {
                                              assignedEmployeeIds = [];
                                              notifyEmployeeIds = [];
                                              for (var element
                                                  in assignedEmployees) {
                                                assignedEmployeeIds
                                                    .add(element.id!);
                                              }
                                              for (var element
                                                  in notifyEmployees) {
                                                notifyEmployeeIds
                                                    .add(element.id!);
                                              }
                                              for (var element
                                                  in testerEmployees) {
                                                testerEmployeesIds
                                                    .add(element.id!);
                                              }
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder:
                                                      (BuildContext context) {
                                                    return loader();
                                                  });
                                              await viewModel
                                                  .addTaskAPI(
                                                      nameController.text
                                                          .trim(),
                                                      descriptionController.text
                                                          .trim(),
                                                      employeeName,
                                                      startDate,
                                                      endDate,
                                                      extraDetailsController
                                                          .text
                                                          .trim(),
                                                      widget.projectId!,
                                                      moduleId!,
                                                      employeeId,
                                                      assignedEmployeeIds,
                                                      notifyEmployeeIds,
                                                      testerEmployeesIds,0)
                                                  .then((value) {
                                                Navigator.pop(context);
                                                if (value != null) {
                                                  if (value.responseStatus ==
                                                      1) {
                                                    showToast(value.result!);
                                                    _setState(() {
                                                      notifyEmployees = [];
                                                      assignedEmployees = [];
                                                      testerEmployees = [];
                                                      dateController.clear();
                                                    });
                                                    Navigator.pop(context);
                                                    setState(() {});
                                                  } else {
                                                    showToast(value.result!);
                                                  }
                                                } else {
                                                  showToast(pleaseTryAgain);
                                                }
                                              });
                                            }
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 8, 16, 8),
                                            child: FittedBox(
                                              fit: BoxFit.scaleDown,
                                              child: Text(addThisList,
                                                  style: newButtonsStyle),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: buttonBorderGreen,
                                        margin: const EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: TextButton(
                                          onPressed: () {
                                            _setState(() {
                                              notifyEmployees = [];
                                              assignedEmployees = [];
                                              testerEmployees = [];
                                              dateController.clear();
                                            });
                                            Navigator.of(context).pop();
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 8, 16, 8),
                                            child: FittedBox(
                                              fit: BoxFit.scaleDown,
                                              child: Text(cancel,
                                                  style: cancelButtonStyle),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ]),
                        ),
                      ),
                    )
                  ]);
            }),
          );
        });
  }

  showModuleDeleteDialog(ModulesData itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.moduleName} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: 'module?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteModuleAPI(itemData.moduleId!, employeeId)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showTaskCommentDialog(TasksData itemData) {
    final _formKey = GlobalKey<FormState>();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('$comment', style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Form(
              key: _formKey,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: 700,
                    maxWidth: 700,
                    minHeight: 5 * 24,
                    maxHeight: 5 * 24),
                child: IntrinsicWidth(
                  child: TextFormField(
                    maxLines: 5,
                    controller: taskCommentController,
                    validator: emptyTextValidator,
                    textInputAction: TextInputAction.done,
                    obscureText: false,
                    decoration: editTextProjectDescriptionDecoration.copyWith(
                        hintText: reasonExtraTime),
                    style: textFieldStyle,
                    // validator: emptyTextValidator,
                    // autovalidateMode: AutovalidateMode.onUserInteraction
                  ),
                ),
              ),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await viewModel
                              .updateTasksBasedOnStatusAPI(
                                  employeeId,
                                  itemData.id!,
                                  StatusText.completeStr.name!,
                                  taskCommentController.text,0)
                              .then((value) {
                            Navigator.of(context).pop();
                            if (value != null) {
                              if (value.responseStatus == 1) {
                                setState(() {});
                                showToast(value.result!);
                              } else {
                                showToast(value.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                        }
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  showTaskDeleteDialog(TasksData itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: 'task?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteTaskAPI(itemData.id!, employeeId)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void getEmployeesData() {
    final employeeViewModel =
        Provider.of<EmployeeNotifier>(context, listen: true);
    if (employeeViewModel.employeesResponse == null) {
      employeeViewModel.getEmployeesAPI("project");
      employeesList = employeeViewModel.getEmployees;
    } else {
      employeesList = employeeViewModel.getEmployees;
    }
  }
}
