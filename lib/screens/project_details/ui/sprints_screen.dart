import 'dart:convert';

import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/get_multiple_sprints_data_model.dart';
import 'package:aem/model/project_wise_modules_tasks_for_sprints.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SprintsScreen extends StatefulWidget {
  final String? projectId, projectName;

  const SprintsScreen({required this.projectId, required this.projectName, Key? key}) : super(key: key);

  @override
  _SprintsScreenState createState() => _SprintsScreenState();
}

class _SprintsScreenState extends State<SprintsScreen> {
  bool editPermission = false, addPermission = false;
  String initialViewType = viewAs;
  bool addNewTaskVisible = false;
  TextEditingController todoNameController = TextEditingController();
  TextEditingController todoExtraDetailsController = TextEditingController();
  TextEditingController todoDescriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController dateDialogController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  late ProjectDetailsNotifier viewModel;
  String employeeId = "", employeeName = "", endDate = "", startDate = "";
  List<EmployeeDetails> employeesList = [], notifyEmployees = [], assignedEmployees = [], testerEmployees = [];
  List<String> notifyEmployeeIds = [], assignedEmployeeIds = [], testerEmployeesIds = [];
  List<ModulesList> moduleDetails = [];
  List<SprintsList> Sprintlist = [];
  List<List<TextEditingController>> mainlisttimes = [];
  int sprintSelected = -1;
  HtmlEditorController htmlController = HtmlEditorController();
  bool editorVisible = true, isLoadingCompleted = false;
  List estimatedTimeList = [];
  int selected = -1;
  int module_view = 0;
  String selected_sprintid = "";
  String selected_sprintname = "";

  @override
  void initState() {
    viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);

    editPermission = userViewModel.sprintEditPermission;
    addPermission = userViewModel.sprintAddPermission;
    employeeId = userViewModel.employeeId!;
    employeeName = userViewModel.employeeName!;
    employeesList.add(EmployeeDetails(name: selectEmployee));
    startDate = getTodayDate();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          backgroundColor: appBackground,
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    LeftDrawer(
                      size: sideMenuMaxWidth,
                      onSelectedChanged: (value) {
                        setState(() {
                          // currentRoute = value;
                        });
                      },
                      selectedMenu: "${RouteNames.dashBoardRoute}-innerpage",
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [const Header(), body()],
                      ),
                    )
                  ],
                ),
              ),
              const Footer()
            ],
          ),
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > ${widget.projectName} > $projectTaskSprints",
                    maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(0, 15, 0, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Divider(color: dashBoardCardBorderTile),
                            ),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Column(
                                    children: [
                                      module_view == 0
                                          ? Visibility(
                                              visible: isLoadingCompleted,
                                              child: Sprintlist.isNotEmpty
                                                  ? ListView.builder(
                                                      key: Key('builder ${sprintSelected.toString()}'),
                                                      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                                                      shrinkWrap: true,
                                                      physics: const ScrollPhysics(),
                                                      itemCount: Sprintlist.length,
                                                      itemBuilder: (context, index) {
                                                        return ExpansionTile(
                                                          initiallyExpanded: index == sprintSelected,
                                                          title: Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  Padding(
                                                                    padding: const EdgeInsets.only(left: 15),
                                                                    child: /*Text(Sprintlist[index].name!+"(${Sprintlist[index].tasksData!.length} Sprints)",style: fragmentHeaderStyle),*/
                                                                        RichText(
                                                                      textAlign: TextAlign.start,
                                                                      text: TextSpan(children: <TextSpan>[
                                                                        TextSpan(
                                                                          text: ' ${Sprintlist[index].name!} Sprint',
                                                                          style: const TextStyle(
                                                                              letterSpacing: 0.3,
                                                                              fontFamily: "Poppins",
                                                                              fontWeight: FontWeight.w700,
                                                                              color: editText,
                                                                              fontSize: 16),
                                                                        ),
                                                                        TextSpan(
                                                                            text: '(${Sprintlist[index].tasksData!.length} Tasks)',
                                                                            style: logoutContentHeader),
                                                                      ]),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              editPermission
                                                                  ? Row(
                                                                      children: [
                                                                        Padding(
                                                                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                                                                          child: ClipRRect(
                                                                            borderRadius: BorderRadius.circular(0),
                                                                            child: Container(
                                                                              decoration: BoxDecoration(
                                                                                  border: Border.all(color: buttonBg, width: 1),
                                                                                  borderRadius: BorderRadius.circular(4)),
                                                                              child: TextButton(
                                                                                onPressed: () async {
                                                                                  selected_sprintid = Sprintlist[index].id!;
                                                                                  selected_sprintname = Sprintlist[index].name!;
                                                                                  await viewModel
                                                                                      .getproject_wise_modules_tasksAPI(
                                                                                    Sprintlist[index].id!,
                                                                                    employeeId,
                                                                                    widget.projectId!,
                                                                                  )
                                                                                      .then((value) {
                                                                                    //Navigator.of(context).pop();
                                                                                    if (value != null) {
                                                                                      if (value.responseStatus == 1) {
                                                                                        setState(() {
                                                                                          if (value.modulesList!.isNotEmpty) {
                                                                                            moduleDetails = value.modulesList!;
                                                                                            module_view = 1;

                                                                                          }
                                                                                        });
                                                                                      } else {
                                                                                        moduleDetails = [];
                                                                                        // showToast(value.result!);
                                                                                      }
                                                                                    } else {
                                                                                      moduleDetails = [];
                                                                                      // showToast(pleaseTryAgain);
                                                                                    }
                                                                                    setState(() {
                                                                                      isLoadingCompleted = true;
                                                                                    });
                                                                                  });
                                                                                },
                                                                                child: Padding(
                                                                                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                                                  child: Text(assign + " " + tableTask, style: addTodoStyle),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () {
                                                                            showTaskDeleteDialog(Sprintlist[index]);
                                                                          },
                                                                          child: Padding(
                                                                            padding: const EdgeInsets.only(left: 20),
                                                                            child: Center(
                                                                              child: Image.asset('assets/images/delete_icon.png',
                                                                                  width: 15, height: 15, color: Colors.black87),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    )
                                                                  : SizedBox()
                                                            ],
                                                          ),
                                                          children: [sprintsList(index, Sprintlist[index])],
                                                          onExpansionChanged: ((newState) {
                                                            if (newState)
                                                              setState(() {
                                                                Duration(seconds: 20000);
                                                                sprintSelected = index;
                                                              });
                                                            else
                                                              setState(() {
                                                                sprintSelected = -1;
                                                              });
                                                          }),
                                                        );

                                                        /* return todosList(index, moduleDetails[index]);*/
                                                      })
                                                  : Center(
                                                      child: Text(noDataAvailable, style: logoutHeader),
                                                    ),
                                            )
                                          : Visibility(
                                              visible: isLoadingCompleted,
                                              child: moduleDetails.isNotEmpty
                                                  ? ListView.builder(
                                                      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                                                      shrinkWrap: true,
                                                      physics: const ScrollPhysics(),
                                                      itemCount: moduleDetails.length,
                                                      itemBuilder: (context, index) {
                                                        return ExpansionTile(
                                                          title: Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  CircleAvatar(
                                                                      backgroundColor: newCompleted.withOpacity(0.3), maxRadius: 13, minRadius: 10),
                                                                  Padding(
                                                                    padding: const EdgeInsets.only(left: 15),
                                                                    child: Text(moduleDetails[index].name!),
                                                                  ),
                                                                ],
                                                              ),
                                                              /*Padding(
                                                      padding: const EdgeInsets.only(left: 15),
                                                      child: Text("${moduleTime.toString()} mins"),
                                                    ),*/
                                                            ],
                                                          ),
                                                          children: [moduleList(index, moduleDetails[index])],
                                                        );

                                                        /* return todosList(index, moduleDetails[index]);*/
                                                      })
                                                  : Center(
                                                      child: Text(noDataAvailable, style: logoutHeader),
                                                    ),
                                            )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  showTaskDeleteDialog(SprintsList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.name} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'Sprint?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteSprintApi(itemData.id!, employeeId, widget.projectId!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              Navigator.pop(context);
                              getData();
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              if (module_view == 1) {
                setState(() {
                  module_view = 0;
                });
              } else {
                Navigator.of(context).pop();
              }
            },
            child: backButton(),
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(module_view == 0 ? projectTaskSprints + "(${widget.projectName})" : selected_sprintname + " Tasks",
                        maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 3),
                      child: Text(dummy, softWrap: true, style: fragmentDescStyle),
                    )
                  ],
                ),
              ),
              Expanded(
                child: FittedBox(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.scaleDown,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      module_view == 0
                          ? Container(
                              decoration: buttonDecorationGreen,
                              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: TextButton(
                                onPressed: () {
                                  if (addPermission) {
                                    showAddSprintTypeDialog();
                                  } else {
                                    showToast(noPermissionToAccess);
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                  child: FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text(addsprint, style: newButtonsStyle),
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              decoration: buttonDecorationGreen,
                              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: TextButton(
                                onPressed: () async {
                                  print(moduleDetails.length);
                                  List<TaskID> tasksidlist = [];
                                  for (int i = 0; i < moduleDetails.length; i++) {
                                    print(moduleDetails[i].tasksList!.length);
                                    for (int j = 0; j < moduleDetails[i].tasksList!.length; j++) {
                                      print(moduleDetails[i].tasksList![j].taskSprintFlag);
                                      if (moduleDetails[i].tasksList![j].taskSprintFlag == true) {
                                        tasksidlist.add(TaskID(moduleDetails[i].tasksList![j].id!));
                                      }
                                    }
                                  }
                                  print(json.encode(tasksidlist));
                                  await viewModel
                                      .assign_tasks_to_sprintAPI(selected_sprintid, employeeId, widget.projectId!, tasksidlist)
                                      .then((value) {
                                    //Navigator.of(context).pop();
                                    if (value != null) {
                                      if (value.responseStatus == 1) {
                                        setState(() {
                                          showToast(value.result!);
                                          module_view = 0;
                                          getData();
                                        });
                                      } else {
                                        moduleDetails = [];
                                        // showToast(value.result!);
                                      }
                                    } else {
                                      moduleDetails = [];
                                      // showToast(pleaseTryAgain);
                                    }
                                    setState(() {
                                      isLoadingCompleted = true;
                                    });
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                  child: FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text(submit, style: newButtonsStyle),
                                  ),
                                ),
                              ),
                            ),
                      /*InkWell(
                      onTap: () {},
                      child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                    )*/
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  showAddSprintTypeDialog() {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController sprintnameController = TextEditingController();
        TextEditingController sprint_fromdatecontroller = TextEditingController();
        TextEditingController sprint_todatecontroller = TextEditingController();
        TextEditingController sprint_goal_controller = TextEditingController();

        final _formKey = GlobalKey<FormState>();

        return StatefulBuilder(
          builder: (context, assignState) {
            return AlertDialog(
              title: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('$addsprint', style: profilepopHeader),
                      IconButton(
                          icon: const Icon(Icons.close_rounded, color: CupertinoColors.black),
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                  /*Text(descriptionAddInterviewType, maxLines: 4, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),*/
                ],
              ),
              titlePadding: const EdgeInsets.fromLTRB(40, 15, 15, 10),
              contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              content: Container(
                // height: ScreenConfig.height(context) / 2.3,
                width: ScreenConfig.width(context) / 2.5,
                margin: const EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: SingleChildScrollView(
                          controller: ScrollController(),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text('Sprint Name', style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: TextFormField(
                                                maxLines: 1,
                                                controller: sprintnameController,
                                                textInputAction: TextInputAction.done,
                                                keyboardType: TextInputType.text,
                                                inputFormatters: [UpperCaseTextFormatter()],
                                                textCapitalization: TextCapitalization.sentences,
                                                obscureText: false,
                                                decoration: InputDecoration(
                                                    fillColor: CupertinoColors.white,
                                                    filled: true,
                                                    border: border,
                                                    isDense: true,
                                                    enabledBorder: border,
                                                    focusedBorder: border,
                                                    errorBorder: errorBorder,
                                                    focusedErrorBorder: errorBorder,
                                                    contentPadding: editTextPadding,
                                                    hintStyle: textFieldHintStyle,
                                                    hintText: 'Sprint name'),
                                                style: textFieldStyle,
                                                validator: emptyTextValidator,
                                                autovalidateMode: AutovalidateMode.onUserInteraction),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text('', style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(18, 0, 0, 0),
                                            child: Text('From Date', style: textFieldHintStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(18, 0, 0, 0),
                                            child: Text('To Date', style: textFieldHintStyle),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text('Sprint Duration', style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                    maxLines: 1,
                                                    controller: sprint_fromdatecontroller,
                                                    textInputAction: TextInputAction.done,
                                                    obscureText: false,
                                                    decoration: InputDecoration(
                                                      fillColor: CupertinoColors.white,
                                                      filled: true,
                                                      border: border,
                                                      isDense: true,
                                                      enabledBorder: border,
                                                      focusedBorder: border,
                                                      errorBorder: errorBorder,
                                                      focusedErrorBorder: errorBorder,
                                                      contentPadding: editTextPadding,
                                                      hintStyle: textFieldHintStyle,
                                                      hintText: 'DD / MM / YYYY',
                                                      suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                      suffixIcon: Padding(
                                                          child: Image.asset("images/calendar.png", color: textFieldBorder),
                                                          padding: dateUploadIconsPadding),
                                                    ),
                                                    style: textFieldStyle,
                                                    onTap: () {
                                                      showCalendar(sprint_fromdatecontroller);
                                                    },
                                                    enableInteractiveSelection: false,
                                                    focusNode: FocusNode(),
                                                    readOnly: true,
                                                    validator: emptyTextValidator),
                                                /* exp == null
                                                    ? const SizedBox()
                                                    : Padding(
                                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                                  child: Text("${exp!.years.toString()} years ${double.parse(exp!.months.toString())} months"),
                                                )*/
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                TextFormField(
                                                    maxLines: 1,
                                                    controller: sprint_todatecontroller,
                                                    textInputAction: TextInputAction.done,
                                                    obscureText: false,
                                                    decoration: InputDecoration(
                                                      fillColor: CupertinoColors.white,
                                                      filled: true,
                                                      border: border,
                                                      isDense: true,
                                                      enabledBorder: border,
                                                      focusedBorder: border,
                                                      errorBorder: errorBorder,
                                                      focusedErrorBorder: errorBorder,
                                                      contentPadding: editTextPadding,
                                                      hintStyle: textFieldHintStyle,
                                                      hintText: 'DD / MM / YYYY',
                                                      suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
                                                      suffixIcon: Padding(
                                                          child: Image.asset("images/calendar.png", color: textFieldBorder),
                                                          padding: dateUploadIconsPadding),
                                                    ),
                                                    style: textFieldStyle,
                                                    onTap: () {
                                                      showCalendar(sprint_todatecontroller);
                                                    },
                                                    enableInteractiveSelection: false,
                                                    focusNode: FocusNode(),
                                                    readOnly: true,
                                                    validator: emptyTextValidator),
                                                /* exp == null
                                                    ? const SizedBox()
                                                    : Padding(
                                                  padding: const EdgeInsets.only(left: 10, top: 5),
                                                  child: Text("${exp!.years.toString()} years ${double.parse(exp!.months.toString())} months"),
                                                )*/
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 30, 0),
                                            child: Text('Sprint Goal', style: addRoleSubStyle),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                            child: TextFormField(
                                              maxLines: 5,
                                              controller: sprint_goal_controller,
                                              textInputAction: TextInputAction.done,
                                              obscureText: false,
                                              enabled: true,
                                              decoration: InputDecoration(
                                                  fillColor: CupertinoColors.white,
                                                  filled: true,
                                                  border: border,
                                                  isDense: true,
                                                  enabledBorder: border,
                                                  focusedBorder: border,
                                                  errorBorder: errorBorder,
                                                  focusedErrorBorder: errorBorder,
                                                  contentPadding: editTextPadding,
                                                  hintStyle: textFieldHintStyle,
                                                  hintText: 'Add your goal here'),
                                              style: textFieldStyle,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  Container(
                                    decoration: buttonDecorationGreen,
                                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: TextButton(
                                      onPressed: () async {
                                        if (_formKey.currentState!.validate()) {
                                          var inputFormat = DateFormat('dd/MM/yyyy');
                                          // removing time from date
                                          var outputFormat = DateFormat('yyyy-MM-dd');

                                          var frominputdate = inputFormat.parse(sprint_fromdatecontroller.text);
                                          var fromDate = outputFormat.format(frominputdate);

                                          var toinputdate = inputFormat.parse(sprint_todatecontroller.text);
                                          var toDate = outputFormat.format(toinputdate);
                                          print(fromDate + "--" + toDate);
                                          await viewModel
                                              .addSprintAPI(employeeId, sprintnameController.text, sprint_goal_controller.text, fromDate, toDate,
                                                  widget.projectId!)
                                              .then((value) {
                                            if (value!.responseStatus == 1) {
                                              showToast(value.result!);
                                              getData();
                                              // set state to reload data , reloads data in futureBuilder
                                              setState(() {});
                                              Navigator.of(context).pop();
                                            } else {
                                              showToast(value.result!);
                                            }
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          child: Text(submit.toUpperCase(), style: newButtonsStyle),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  Widget sprintsList(int position, SprintsList itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              child: DataTable2(
                  columnSpacing: 14,
                  dividerThickness: 1,
                  decoration: BoxDecoration(border: Border.all(color: sideMenuSelected, width: 1)),
                  headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                  headingRowHeight: 46,
                  // dataRowHeight: 80,
                  smRatio: 0.6,
                  lmRatio: 1.5,
                  columns: <DataColumn2>[
                    DataColumn2(
                      label: Text(tasks, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                      size: ColumnSize.L,
                    ),
                    DataColumn2(
                      label: Text(modules, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
                      size: ColumnSize.L,
                    ),
                    DataColumn2(
                      label: Text(status, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
                      size: ColumnSize.S,
                    ),
                  ],
                  rows: itemData.tasksData!.map<DataRow2>((e) {
                    String status = "";
                    if (e.taskStatus == 0) {
                      status = "Inactive";
                    } else if (e.taskStatus == 1) {
                      status = "New";
                    } else if (e.taskStatus == 2) {
                      status = "Open";
                    } else if (e.taskStatus == 3) {
                      status = "Complete";
                    } else if (e.taskStatus == 4) {
                      status = "Close";
                    } else if (e.taskStatus == 5) {
                      status = "Reopen";
                    } else if (e.taskStatus == 6) {
                      status = "Delete";
                    } else {
                      status = "Hold";
                    }
                    return DataRow2(
                        specificRowHeight: 60,
                        color: MaterialStateColor.resolveWith(
                            (states) => /*data.profilesList!.indexOf(e) % 2 == 0 ? listItemColor : listItemColor1*/ CupertinoColors.white),
                        cells: [
                          DataCell(
                            Text(e.taskName!, softWrap: true, style: projectAddedByStyle, textAlign: TextAlign.start),
                          ),
                          DataCell(
                            Text(e.moduleName!, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center),
                          ),
                          DataCell(
                            Text(status, softWrap: true, style: listItemsStyle, textAlign: TextAlign.center),
                          ),
                        ]);
                  }).toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget moduleList(int position, ModulesList itemData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: itemData.tasksList!.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: itemData.tasksList!.length,
                    itemBuilder: (context, index) {
                      return tasksList(itemData, itemData.tasksList![index], index);
                    })
                : const SizedBox(),
          ),
        ],
      ),
    );
  }

  Widget tasksList(ModulesList moduleDetails, TasksList itemData, int pos) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Checkbox(
                          activeColor: buttonBg,
                          hoverColor: buttonBg.withOpacity(0.2),
                          value: itemData.taskSprintFlag,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                          side: const BorderSide(color: buttonBg),
                          onChanged: (value) {
                            setState(() {
                              itemData.taskSprintFlag = value;
                            });
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 8, 0),
                      child: Text(itemData.name!, style: completedStyle),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showCalendar(TextEditingController textController) async {
    final DateTime? picked = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(2100));
    setState(() {
      editorVisible = true;
      if (picked != null) {
        selectedDate = picked;
        var inputFormat = DateFormat('yyyy-MM-dd');
        var inputDate = inputFormat.parse(selectedDate.toLocal().toString().split(' ')[0]); // removing time from date
        var outputFormat = DateFormat('dd/MM/yyyy');
        var outputDate = outputFormat.format(inputDate);
        endDate = DateFormat('yyyy-MM-dd').format(inputDate);
        textController.text = outputDate;
      }
    });
  }

  void getData() {
    Future.delayed(const Duration(milliseconds: 00), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.get_multiple_sprints_dataAPI(employeeId, widget.projectId!).then((value) {
        Navigator.of(context).pop();
        if (value != null) {
          if (value.responseStatus == 1) {
            setState(() {
              if (value.sprintsList!.isNotEmpty) {
                Sprintlist = value.sprintsList!;
              }
            });
          } else {
            Sprintlist = [];
            // showToast(value.result!);
          }
        } else {
          Sprintlist = [];
          // showToast(pleaseTryAgain);
        }
        setState(() {
          isLoadingCompleted = true;
        });
      });
    });
  }
}

class TaskID {
  final String taskId;

  TaskID(this.taskId);

  TaskID.fromJson(Map<String, dynamic> json) : taskId = json['taskId'];

  Map<String, dynamic> toJson() => {'taskId': taskId};
}
