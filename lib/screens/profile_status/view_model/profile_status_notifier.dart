import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/profile_statuses_detail_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class ProfileStatusNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  AllProfileStatusResponseModel? _allProfileStatusResponseModel;

  List<ProfilesStatusList>? get viewAllProfileStatuses {
    if (_allProfileStatusResponseModel != null) {
      if (_allProfileStatusResponseModel!.responseStatus == 1) {
        return _allProfileStatusResponseModel!.profilesStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<AllProfileStatusResponseModel?> viewAllProfileStatusesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allProfileStatusResponseModel = AllProfileStatusResponseModel();
    try {
      dynamic response = await Repository().viewAllProfileStatuses(createdBy);
      if (response != null) {
        _allProfileStatusResponseModel = AllProfileStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allProfileStatusResponseModel");
    notifyListeners();
    return _allProfileStatusResponseModel;
  }

  AllProfileStatusResponseModel? _allActiveProfileStatusResponseModel;
  AllProfileStatusResponseModel? _profileStatusResponseModel;

  List<ProfilesStatusList>? get getAllActiveProfileStatuses {
    if (_allActiveProfileStatusResponseModel != null) {
      if (_allActiveProfileStatusResponseModel!.responseStatus == 1) {
        return _allActiveProfileStatusResponseModel!.profilesStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
  List<ProfilesStatusList>? get getProfileStatuses {
    if (_profileStatusResponseModel != null) {
      if (_profileStatusResponseModel!.responseStatus == 1) {
        return _profileStatusResponseModel!.profilesStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
  List<DisplayEnableProfilesStatusList>? get getdisplayEnableProfilesStatusList {
    if (_profileStatusResponseModel != null) {
      if (_profileStatusResponseModel!.responseStatus == 1) {
        return _profileStatusResponseModel!.displayEnableProfilesStatusList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }
  Future<AllProfileStatusResponseModel?> getAllActiveProfileStatusesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allActiveProfileStatusResponseModel = AllProfileStatusResponseModel();
    try {
      dynamic response = await Repository().getAllActiveProfileStatuses(createdBy);
      if (response != null) {
        _allActiveProfileStatusResponseModel = AllProfileStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allActiveProfileStatusResponseModel");
    notifyListeners();
    return _allActiveProfileStatusResponseModel;
  }

  Future<AllProfileStatusResponseModel?> getProfileStatusesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _profileStatusResponseModel = AllProfileStatusResponseModel();
    try {
      dynamic response = await Repository().getProfileStatuses();
      if (response != null) {
        _profileStatusResponseModel = AllProfileStatusResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_profileStatusResponseModel");
    notifyListeners();
    return _profileStatusResponseModel;
  }

  BaseResponse? _addProfileStatusResponse;

  BaseResponse? get addProfileStatusResponse {
    return _addProfileStatusResponse;
  }

  Future<BaseResponse?> addProfileStatusesAPI(String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _addProfileStatusResponse = BaseResponse();
    try {
      dynamic response = await Repository().addProfileStatuses(statusName,  mailSent, mailSubject, mailContent, createdBy);
      if (response != null) {
        _addProfileStatusResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addProfileStatusResponse");
    notifyListeners();
    return _addProfileStatusResponse;
  }

  BaseResponse? _updateProfileStatusResponse;

  BaseResponse? get updateProfileStatusResponse {
    return _updateProfileStatusResponse;
  }

  Future<BaseResponse?> updateProfileStatusesAPI(
      String profileStatusId, String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _updateProfileStatusResponse = BaseResponse();
    try {
      dynamic response = await Repository().updateProfileStatuses(profileStatusId, statusName, mailSent, mailSubject, mailContent, createdBy);
      if (response != null) {
        _updateProfileStatusResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateProfileStatusResponse");
    notifyListeners();
    return _updateProfileStatusResponse;
  }

  ProfileStatusDetailResponseModel? _statusDetailResponseModel;

  ProfileStatusDetailResponseModel? get statusDetailResponseModel {
    return _statusDetailResponseModel;
  }

  Future<ProfileStatusDetailResponseModel?> viewProfileStatusesAPI(String profileStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _statusDetailResponseModel = ProfileStatusDetailResponseModel();
    try {
      dynamic response = await Repository().viewProfileStatuses(profileStatusId, createdBy);
      if (response != null) {
        _statusDetailResponseModel = ProfileStatusDetailResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_statusDetailResponseModel");
    notifyListeners();
    return _statusDetailResponseModel;
  }

  BaseResponse? _deleteStatusResponse;

  BaseResponse? get deleteStatusResponse {
    return _deleteStatusResponse;
  }

  Future<BaseResponse?> deleteProfileStatusesAPI(String profileStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _deleteStatusResponse = BaseResponse();
    try {
      dynamic response = await Repository().deleteProfileStatuses(profileStatusId, createdBy);
      if (response != null) {
        _deleteStatusResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteStatusResponse");
    notifyListeners();
    return _deleteStatusResponse;
  }

  BaseResponse? _activateDeActivateProfileStatusesResponse;

  BaseResponse? get activateDeActivateProfileStatusesResponse {
    return _activateDeActivateProfileStatusesResponse;
  }

  Future<BaseResponse?> activateOrDeActivateProfileStatusesAPI(String profileStatusId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _activateDeActivateProfileStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().activateOrDeActivateProfileStatuses(profileStatusId, createdBy);
      if (response != null) {
        _activateDeActivateProfileStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_activateDeActivateProfileStatusesResponse");
    notifyListeners();
    return _activateDeActivateProfileStatusesResponse;
  }
  Future updateprofilestatusorderApi(String employeeId,List profilesStatusList) async {
    _isFetching = true;
    _isHavingData = false;
    _activateDeActivateProfileStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().updateprofilestatusorderApi(employeeId, profilesStatusList);
      if (response != null) {
        _activateDeActivateProfileStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_activateDeActivateProfileStatusesResponse");
    notifyListeners();
    return _activateDeActivateProfileStatusesResponse;
  }

  Future profilestatusdisplaymodificationApi(String employeeId,String profileStatusId) async {
    _isFetching = true;
    _isHavingData = false;
    _activateDeActivateProfileStatusesResponse = BaseResponse();
    try {
      dynamic response = await Repository().profilestatusdisplaymodificationApi(employeeId, profileStatusId);
      if (response != null) {
        _activateDeActivateProfileStatusesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_activateDeActivateProfileStatusesResponse");
    notifyListeners();
    return _activateDeActivateProfileStatusesResponse;
  }
}
