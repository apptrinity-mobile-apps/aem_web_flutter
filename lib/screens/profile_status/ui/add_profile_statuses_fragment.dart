import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/utils/upper_case_formatter.dart';
import 'package:aem/utils/validators.dart';
import 'package:aem/widgets/animated_toggle_switch.dart';
import 'package:aem/widgets/back_button.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/footer.dart';
import 'package:aem/widgets/header.dart';
import 'package:aem/widgets/left_drawer_panel.dart';
import 'package:aem/widgets/loader.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';

class AddProfileStatusesFragment extends StatefulWidget {
  final String? profileStatusId;
  final String? createdBy;

  const AddProfileStatusesFragment({this.profileStatusId, this.createdBy, Key? key}) : super(key: key);

  @override
  _AddProfileStatusesFragmentState createState() => _AddProfileStatusesFragmentState();
}

class _AddProfileStatusesFragmentState extends State<AddProfileStatusesFragment> {
  TextEditingController nameController = TextEditingController();
  TextEditingController mailSubjectController = TextEditingController();
  final HtmlEditorController htmlController = HtmlEditorController();
  String employeeId = "", createdBy = "", emailContent = "";
  final _formKey = GlobalKey<FormState>();
  final _mailSubjectKey = GlobalKey<FormFieldState>();
  bool isSendEmailSelected = true;
  late ProfileStatusNotifier viewModel;

  @override
  void initState() {
    viewModel = Provider.of<ProfileStatusNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    if (widget.profileStatusId != null) {
      getProfileStatusData();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  LeftDrawer(
                    size: sideMenuMaxWidth,
                    onSelectedChanged: (value) {
                      setState(() {
                        // currentRoute = value;
                      });
                    },
                    selectedMenu: "${RouteNames.profileStatusesHomeRoute}-innerpage",
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: [const Header(), body()],
                    ),
                  )
                ],
              ),
            ),
            const Footer()
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Expanded(
      child: SizedBox(
        height: ScreenConfig.height(context),
        width: ScreenConfig.width(context),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: cardBg,
          child: Container(
            height: ScreenConfig.height(context),
            width: ScreenConfig.width(context),
            margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("$home > $navMenuProfileStatus", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Card(
                      elevation: 3,
                      color: CupertinoColors.white,
                      child: Container(
                        width: ScreenConfig.width(context),
                        margin: const EdgeInsets.fromLTRB(25, 15, 25, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            header(),
                            Flexible(
                              child: Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                                decoration: BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg),
                                child: SingleChildScrollView(
                                  controller: ScrollController(),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: SingleChildScrollView(
                                            controller: ScrollController(),
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(15, 25, 15, 25),
                                              child: Form(
                                                key: _formKey,
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: nameField(),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                      child: sendEmailField(),
                                                    ),
                                                    isSendEmailSelected
                                                        ? Column(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: [
                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                                child: sendMailSubjectField(),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                                child: sendMailContentField(),
                                                              ),
                                                            ],
                                                          )
                                                        : const SizedBox()
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: backButton(),
        ),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.profileStatusId == null ? addProfileStatus : updateProfileStatus,
                      maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(descriptionAddProfileStatus, softWrap: true, style: fragmentDescStyle),
                  )
                ],
              ),
            ),
            Expanded(
              child: FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.scaleDown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: buttonDecorationGreen,
                      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: TextButton(
                        onPressed: () async {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          if (widget.profileStatusId != null) {
                            if (isSendEmailSelected) {
                              htmlController.getText().then((value) async {
                                await viewModel
                                    .updateProfileStatusesAPI(widget.profileStatusId!, nameController.text.trim(), isSendEmailSelected,
                                        mailSubjectController.text.trim(), value, createdBy)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (viewModel.updateProfileStatusResponse != null) {
                                    if (viewModel.updateProfileStatusResponse!.responseStatus == 1) {
                                      showToast(viewModel.updateProfileStatusResponse!.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(viewModel.updateProfileStatusResponse!.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              });
                            } else {
                              await viewModel
                                  .updateProfileStatusesAPI(widget.profileStatusId!, nameController.text.trim(), isSendEmailSelected,
                                      mailSubjectController.text.trim(), "", createdBy)
                                  .then((value) {
                                Navigator.pop(context);
                                if (viewModel.updateProfileStatusResponse != null) {
                                  if (viewModel.updateProfileStatusResponse!.responseStatus == 1) {
                                    showToast(viewModel.updateProfileStatusResponse!.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(viewModel.updateProfileStatusResponse!.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          } else {
                            if (isSendEmailSelected) {
                              htmlController.getText().then((value) async {
                                await viewModel
                                    .addProfileStatusesAPI(
                                        nameController.text.trim(), isSendEmailSelected, mailSubjectController.text.trim(), value, employeeId)
                                    .then((value) {
                                  Navigator.pop(context);
                                  if (viewModel.addProfileStatusResponse != null) {
                                    if (viewModel.addProfileStatusResponse!.responseStatus == 1) {
                                      showToast(viewModel.addProfileStatusResponse!.result!);
                                      Navigator.pop(context);
                                    } else {
                                      showToast(viewModel.addProfileStatusResponse!.result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });
                              });
                            } else {
                              await viewModel
                                  .addProfileStatusesAPI(
                                      nameController.text.trim(), isSendEmailSelected, mailSubjectController.text.trim(), "", employeeId)
                                  .then((value) {
                                Navigator.pop(context);
                                if (viewModel.addProfileStatusResponse != null) {
                                  if (viewModel.addProfileStatusResponse!.responseStatus == 1) {
                                    showToast(viewModel.addProfileStatusResponse!.result!);
                                    Navigator.pop(context);
                                  } else {
                                    showToast(viewModel.addProfileStatusResponse!.result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                            }
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(submit.toUpperCase(), style: newButtonsStyle),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget nameField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: Text(statusName, style: addRoleSubStyle),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: TextFormField(
                maxLines: 1,
                controller: nameController,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.text,
                inputFormatters: [UpperCaseTextFormatter()],
                textCapitalization: TextCapitalization.sentences,
                obscureText: false,
                decoration: editTextNameDecoration,
                style: textFieldStyle,
                validator: emptyTextValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction),
          ),
        )
      ],
    );
  }

  Widget sendEmailField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: Text('Send $email', style: addRoleSubStyle),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: AnimatedToggleSwitch(
                initialValue: isSendEmailSelected,
                width: ScreenConfig.width(context) / 9,
                values: const ['Yes', 'No'],
                onToggleCallback: (bool value) {
                  setState(() {
                    isSendEmailSelected = value;
                  });
                },
                buttonColor: buttonBg,
                backgroundColor: sideMenuSelected,
                textColor: CupertinoColors.white,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget sendMailSubjectField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: Text('$email subject', style: addRoleSubStyle),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: TextFormField(
              key: _mailSubjectKey,
              minLines: 1,
              maxLines: 5,
              controller: mailSubjectController,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.multiline,
              obscureText: false,
              decoration: InputDecoration(
                  fillColor: CupertinoColors.white,
                  filled: true,
                  border: border,
                  isDense: true,
                  enabledBorder: border,
                  focusedBorder: border,
                  errorBorder: errorBorder,
                  focusedErrorBorder: errorBorder,
                  contentPadding: editTextPadding,
                  hintStyle: textFieldHintStyle,
                  hintText: '$email subject'),
              style: textFieldStyle,
              /*validator: emptyTextValidator,
                autovalidateMode: AutovalidateMode.onUserInteraction*/
            ),
          ),
        )
      ],
    );
  }

  Widget sendMailContentField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 30, 8),
            child: Text('$email content', style: addRoleSubStyle),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Container(
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: textFieldBorder, width: 1.5), color: Colors.white),
              height: 300,
              child: HtmlEditor(
                controller: htmlController,
                htmlEditorOptions:
                    HtmlEditorOptions(hint: 'Your text here...', shouldEnsureVisible: true, spellCheck: true, initialText: emailContent),
                htmlToolbarOptions: HtmlToolbarOptions(
                  toolbarPosition: ToolbarPosition.aboveEditor,
                  toolbarType: ToolbarType.nativeExpandable,
                  initiallyExpanded: true,
                  defaultToolbarButtons: const [
                    StyleButtons(),
                    FontSettingButtons(fontSizeUnit: false),
                    FontButtons(clearAll: false),
                    ColorButtons(),
                    ListButtons(listStyles: false),
                    InsertButtons(video: false, audio: false, table: false, hr: false, otherFile: false),
                  ],
                  onButtonPressed: (ButtonType type, bool? status, Function()? updateStatus) {
                    return true;
                  },
                  onDropdownChanged: (DropdownType type, dynamic changed, Function(dynamic)? updateSelectedItem) {
                    return true;
                  },
                  mediaLinkInsertInterceptor: (String url, InsertFileType type) {
                    return true;
                  },
                  mediaUploadInterceptor: (PlatformFile file, InsertFileType type) async {
                    return true;
                  },
                ),
                otherOptions: const OtherOptions(height: 250),
                callbacks: Callbacks(
                    onBeforeCommand: (String? currentHtml) {},
                    onChangeContent: (String? changed) {},
                    onChangeCodeview: (String? changed) {},
                    onChangeSelection: (EditorSettings settings) {},
                    onDialogShown: () {},
                    onEnter: () {},
                    onFocus: () {},
                    onBlur: () {},
                    onBlurCodeview: () {},
                    onInit: () {},
                    onImageUploadError: (FileUpload? file, String? base64Str, UploadError error) {},
                    onKeyDown: (int? keyCode) {},
                    onKeyUp: (int? keyCode) {},
                    onMouseDown: () {},
                    onMouseUp: () {},
                    onNavigationRequestMobile: (String url) {
                      return NavigationActionPolicy.ALLOW;
                    },
                    onPaste: () {},
                    onScroll: () {}),
                plugins: [
                  SummernoteAtMention(
                      getSuggestionsMobile: (String value) {
                        var mentions = <String>['test1', 'test2', 'test3'];
                        return mentions.where((element) => element.contains(value)).toList();
                      },
                      mentionsWeb: ['test1', 'test2', 'test3'],
                      onSelect: (String value) {}),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  getProfileStatusData() {
    Future.delayed(Duration.zero, () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return loader();
          });
      await viewModel.viewProfileStatusesAPI(widget.profileStatusId!, widget.createdBy!).then((value) {
        if (viewModel.statusDetailResponseModel != null) {
          if (viewModel.statusDetailResponseModel!.responseStatus == 1) {
            nameController.text = viewModel.statusDetailResponseModel!.profileStatusDetails!.statusName!;
            htmlController.insertHtml(viewModel.statusDetailResponseModel!.profileStatusDetails!.mailContent!);
            mailSubjectController.text = viewModel.statusDetailResponseModel!.profileStatusDetails!.mailSubject!;
            setState(() {
              emailContent = parseHtmlString(viewModel.statusDetailResponseModel!.profileStatusDetails!.mailContent!);
              isSendEmailSelected = viewModel.statusDetailResponseModel!.profileStatusDetails!.mailSent!;
              createdBy = viewModel.statusDetailResponseModel!.profileStatusDetails!.createdBy!;
            });
          } else {
            showToast(viewModel.statusDetailResponseModel!.result!);
          }
        } else {
          showToast(pleaseTryAgain);
        }
        Navigator.of(context).pop();
      });
    });
  }
}
