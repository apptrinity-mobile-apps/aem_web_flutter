import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/profile_status/ui/add_profile_statuses_fragment.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/snack_bars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class ProfileStatusesFragment extends StatefulWidget {
  const ProfileStatusesFragment({Key? key}) : super(key: key);

  @override
  _ProfileStatusesFragmentState createState() =>
      _ProfileStatusesFragmentState();
}
class _ProfileStatusesFragmentState extends State<ProfileStatusesFragment> {
  late ProfileStatusNotifier viewModel;
  String employeeId = "";
  bool editPermission = false, addPermission = false, viewPermission = false;
  List<TextEditingController> listOrderBy = [];
  bool orderByReadOnly = false;
  List<ProfilesStatusList>? profilesStatusList;
  @override
  void initState() {
    viewModel = Provider.of<ProfileStatusNotifier>(context, listen: false);
    final LoginNotifier userViewModel =
        Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.profileStatusEditPermission;
    addPermission = userViewModel.profileStatusAddPermission;
    viewPermission = userViewModel.profileStatusViewPermission;
    // getProfileStatusData(true);
    super.initState();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("$home > $navMenuProfileStatus",
              maxLines: 1,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: fragmentDescStyle),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                              child: allStatusesList())),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuProfileStatus,
                  maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionProfileStatus,
                    softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                editPermission
                    ? Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 15, 0),
                        child: TextButton(
                          onPressed: () async {
                            if (orderByReadOnly == false) {
                              setState(() {
                                orderByReadOnly = true;
                              });
                            } else {
                              List orderBy = [];
                              for (int i = 0; i < listOrderBy.length; i++) {
                                String text = listOrderBy[i].text;
                                // print("controller text ${text}");

                                orderBy.add(text);
                              }
                              print("orderBy -- ${orderBy}");
                              bool checkDuplicate = false;
                              for (int i = 0; i < orderBy.length; i++) {
                                var value = int.parse(orderBy[i]);
                                if (value == 0) {
                                  checkDuplicate = true;
                                  print('incorrect 1');
                                } else if (value > orderBy.length) {
                                  checkDuplicate = true;
                                  print('incorrect 2');
                                } else {
                                  List distinctList = orderBy.toSet().toList();
                                  print(
                                      'length -- ${listOrderBy.length} -- ${distinctList.length}');
                                  if (listOrderBy.length !=
                                      distinctList.length) {
                                    checkDuplicate = true;
                                  } else {}
                                }
                              }
                              if (checkDuplicate) {
                                showToast(
                                    'Please enter order by values in format');
                              } else {
                                List orderIds = [];



                       for (int i = 0; i < profilesStatusList!.length; i++) {

                         var obj = {"id":profilesStatusList![i].id!,"orderValue":int.parse(orderBy[i])};
                         orderIds.add(obj);

                       }
                                await viewModel
                                    .updateprofilestatusorderApi(
                                    employeeId, orderIds)
                                    .then((value) {
                                  if (viewModel
                                      .updateprofilestatusorderApi !=
                                      null) {
                                    if (value
                                        .responseStatus ==
                                        1) {
                                      showToast(value
                                          .result!);
                                      setState(() {

                                        orderByReadOnly = false;
                                      });

                                    } else {
                                      showToast(viewModel
                                          .activateDeActivateProfileStatusesResponse!
                                          .result!);
                                    }
                                  } else {
                                    showToast(pleaseTryAgain);
                                  }
                                });

                              }
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                  orderByReadOnly
                                      ? 'Update Order By'
                                      : 'Edit Order By',
                                  style: newButtonsStyle),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                addPermission
                    ? Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                        const AddProfileStatusesFragment(),
                                transitionDuration: const Duration(seconds: 0),
                                reverseTransitionDuration:
                                    const Duration(seconds: 0),
                              ),
                            ).then((value) {
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(addProfileStatus,
                                  style: newButtonsStyle),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                /*InkWell(
                  onTap: () {},
                  child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                )*/
              ],
            ),
          ),
        )
      ],
    );
  }
  Widget allStatusesList() {
    return FutureBuilder(
        future: viewModel.viewAllProfileStatusesAPI(employeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loader());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                  child: Text('error ${snapshot.error}', style: logoutHeader));
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllProfileStatusResponseModel;
              if (data.responseStatus == 1) {
                profilesStatusList = data.profilesStatusList!;
                if (data.profilesStatusList!.isNotEmpty) {
                  return Column(
                    children: [
                      listHeader(),
                      Flexible(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            controller: ScrollController(),
                            itemCount: data.profilesStatusList!.length,
                            itemBuilder: (context, index) {
                              listOrderBy = [];
                              for (var element in data.profilesStatusList!) {
                                TextEditingController orderByControllers =
                                    TextEditingController();
                                orderByControllers.text =
                                    element.orderValue!.toString();
                                listOrderBy.add(orderByControllers);
                              }
                              return listItems(data.profilesStatusList![index],
                                  index, listOrderBy[index]);
                            }),
                      )
                    ],
                  );
                } else {
                  return Center(
                      child: Text(noDataAvailable, style: logoutHeader));
                }
              } else {
                return Center(child: Text(data.result!, style: logoutHeader));
              }
            } else {
              return Center(child: Text(pleaseTryAgain, style: logoutHeader));
            }
          } else {
            return Center(child: loader());
          }
        });
  }
  Widget listHeader() {
    return Container(
      decoration: buttonTopCircularDecorationGreen,
      child: Row(
        children: [
          Container(
            width: 90,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text(orderBy,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 10, 25, 10),
              child: Text(statusName,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.start),
            ),
          ),
          /*Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text('System Test', softWrap: true, overflow: TextOverflow.ellipsis, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),*/
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(email,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(displayEnable,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(status,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(action,
                  softWrap: true,
                  style: listHeaderStyle,
                  textAlign: TextAlign.end),
            ),
          ),
        ],
      ),
    );
  }
  Widget listItems(ProfilesStatusList itemData, int index,
      TextEditingController controller) {
    var alternate = index % 2;
    bool isActivated = false;
    bool isEnabledDisplay = false;
    if (itemData.status == 1) {
      isActivated = true;
    } else if (itemData.status == 0) {
      isActivated = false;
    } else {
      isActivated = false;
    }


      isEnabledDisplay = itemData.displayEnable!;

    return Container(
      color: alternate == 0 ? listItemColor : listItemColor1,
      child: Row(
        children: [
          Container(
            width: 90,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: TextFormField(
                controller: controller,
                textAlign: TextAlign.center,
                maxLines: 1,
                textInputAction: TextInputAction.done,
                textCapitalization: TextCapitalization.sentences,
                obscureText: false,
                // validator: emptyTextValidator,
                // autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                    fillColor: CupertinoColors.white,
                    filled: true,
                    border: border,
                    isDense: true,
                    enabledBorder: border,
                    focusedBorder: border,
                    errorBorder: errorBorder,
                    focusedErrorBorder: errorBorder,
                    contentPadding: editTextPadding,
                    hintStyle: textFieldHintStyle,
                    hintText: ''),
                style: textFieldStyle,
                textAlignVertical: TextAlignVertical.center,
                readOnly: !orderByReadOnly ? true : false,
                //enabled: true,
                onChanged: (val) {
                  print("index ${index} -- text ${val}");
                  listOrderBy[index].text = val;
                },

                onTap: () async {},
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 25, 5),
              child: Text(itemData.statusName!.inCaps,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
          ),
          /*Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(itemData.systemTestRequired! ? yes : no,
                  softWrap: true, overflow: TextOverflow.ellipsis, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),*/
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(itemData.mailSent! ? yes : no,
                  softWrap: true,
                  style: listItemsStyle,
                  textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 5, 30, 5),
              child: StatefulBuilder(
                builder: (context, _setState) {
                  return OutlinedButton(
                    onPressed: () {
                      if (editPermission) {
                        _setState(() {
                          isEnabledDisplay = !isEnabledDisplay;
                        });
                        Future.delayed(const Duration(milliseconds: 0),
                                () async {
                              await viewModel
                                  .profilestatusdisplaymodificationApi(
                                itemData.createdBy!,itemData.id!, )
                                  .then((value) {
                                if (value !=
                                    null) {
                                  if (value
                                      .responseStatus ==
                                      1) {
                                    showToast(value
                                        .result!);
                                  } else {
                                    showToast(value
                                        .result!);
                                  }
                                } else {
                                  showToast(pleaseTryAgain);
                                }
                              });
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                            });
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        isEnabledDisplay ? "Yes" : "No",
                        style:  TextStyle(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w600,
                          color: isEnabledDisplay ? buttonBg : availableTimesText,
                        ),
                      ),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          isEnabledDisplay ? filterBg : sideMenuSelected),
                      /*overlayColor: MaterialStateProperty.all(isEnabledDisplay
                          ? addRoleText.withOpacity(0.1)
                          : dashBoardCardOverDueText.withOpacity(0.1)),
                      side: MaterialStateProperty.all(BorderSide(
                          color: isEnabledDisplay
                              ? buttonBg
                              : filterBg)
                      ),*/
                      elevation: MaterialStateProperty.all(2),
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 5, 30, 5),
              child: StatefulBuilder(
                builder: (context, _setState) {
                  return OutlinedButton(
                    onPressed: () {
                      if (editPermission) {
                        _setState(() {
                          isActivated = !isActivated;
                        });
                        Future.delayed(const Duration(milliseconds: 0),
                            () async {
                          await viewModel
                              .activateOrDeActivateProfileStatusesAPI(
                                  itemData.id!, itemData.createdBy!)
                              .then((value) {
                            if (viewModel
                                    .activateDeActivateProfileStatusesResponse !=
                                null) {
                              if (viewModel
                                      .activateDeActivateProfileStatusesResponse!
                                      .responseStatus ==
                                  1) {
                                showToast(viewModel
                                    .activateDeActivateProfileStatusesResponse!
                                    .result!);
                              } else {
                                showToast(viewModel
                                    .activateDeActivateProfileStatusesResponse!
                                    .result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                          // set state to reload data , reloads data in futureBuilder
                          setState(() {});
                        });
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        isActivated ? "Active" : "Inactive",
                        style: const TextStyle(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          isActivated ? buttonBg : dashBoardCardOverDueText),
                      overlayColor: MaterialStateProperty.all(isActivated
                          ? buttonBg.withOpacity(0.1)
                          : dashBoardCardOverDueText.withOpacity(0.1)),
                      side: MaterialStateProperty.all(BorderSide(
                          color: isActivated
                              ? buttonBg
                              : dashBoardCardOverDueText)),
                      elevation: MaterialStateProperty.all(2),
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 10, 5),
              child: Wrap(
                // mainAxisAlignment: MainAxisAlignment.end,
                alignment: WrapAlignment.end,
                crossAxisAlignment: WrapCrossAlignment.end,
                runAlignment: WrapAlignment.end,
                runSpacing: 4,
                spacing: 4,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: edit,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage(
                              'assets/images/edit_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (itemData.status == 1) {
                                if (editPermission) {
                                  Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      pageBuilder: (context, animation1,
                                              animation2) =>
                                          AddProfileStatusesFragment(
                                              profileStatusId: itemData.id!,
                                              createdBy: itemData.createdBy!),
                                      transitionDuration:
                                          const Duration(seconds: 0),
                                      reverseTransitionDuration:
                                          const Duration(seconds: 0),
                                    ),
                                  ).then((value) {
                                    // set state to reload data , reloads data in futureBuilder
                                    setState(() {});
                                  });
                                } else {
                                  showToast(noPermissionToAccess);
                                }
                              } else {
                                snackBarDark(context,
                                    "Profile status must be 'active' for updating.");
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: delete,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage(
                              'assets/images/delete_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (editPermission) {
                                showDeleteDialog(itemData);
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  showDeleteDialog(ProfilesStatusList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.statusName} ',
                  style: const TextStyle(
                      letterSpacing: 0.3,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: editText,
                      fontSize: 16),
                ),
                TextSpan(text: '$status?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel
                            .deleteProfileStatusesAPI(
                                itemData.id!, itemData.createdBy!)
                            .then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(
                            letterSpacing: 0.3,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  getProfileStatusData(bool showLoader) {
    Future.delayed(Duration.zero, () async {
      if (showLoader) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return loader();
            });
      }
      await viewModel.viewAllProfileStatusesAPI(employeeId).then((value) {
        if (showLoader) {
          Navigator.of(context).pop();
        }
      });
    });
  }
}
