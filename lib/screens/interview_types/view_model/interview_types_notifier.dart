import 'package:aem/model/all_interview_type_response_model.dart';
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/interview_type_details_response_model.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class InterviewTypeNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  AllInterviewTypeResponseModel? _allInterviewTypeResponseModel;
  AllInterviewTypeResponseModel? _interviewTypesResponseModel;

  List<InterviewTypesList>? get viewAllInterviewTypes {
    if (_allInterviewTypeResponseModel != null) {
      if (_allInterviewTypeResponseModel!.responseStatus == 1) {
        return _allInterviewTypeResponseModel!.interviewTypesList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  List<InterviewTypesList>? get getInterviewTypes {
    if (_interviewTypesResponseModel != null) {
      if (_interviewTypesResponseModel!.responseStatus == 1) {
        return _interviewTypesResponseModel!.interviewTypesList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<AllInterviewTypeResponseModel?> viewAllInterviewTypesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allInterviewTypeResponseModel = AllInterviewTypeResponseModel();
    try {
      dynamic response = await Repository().viewAllInterviewTypes(createdBy);
      if (response != null) {
        _allInterviewTypeResponseModel = AllInterviewTypeResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allInterviewTypeResponseModel");
    notifyListeners();
    return _allInterviewTypeResponseModel;
  }

  Future<AllInterviewTypeResponseModel?> getInterviewTypesAPI() async {
    _isFetching = true;
    _isHavingData = false;
    _interviewTypesResponseModel = AllInterviewTypeResponseModel();
    try {
      dynamic response = await Repository().getInterviewTypes();
      if (response != null) {
        _interviewTypesResponseModel = AllInterviewTypeResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_interviewTypesResponseModel");
    notifyListeners();
    return _interviewTypesResponseModel;
  }

  AllInterviewTypeResponseModel? _allActiveInterviewTypeResponseModel;

  List<InterviewTypesList>? get getAllActiveInterviewTypes {
    if (_allActiveInterviewTypeResponseModel != null) {
      if (_allActiveInterviewTypeResponseModel!.responseStatus == 1) {
        return _allActiveInterviewTypeResponseModel!.interviewTypesList;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  Future<AllInterviewTypeResponseModel?> getAllActiveInterviewTypesAPI(String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _allActiveInterviewTypeResponseModel = AllInterviewTypeResponseModel();
    try {
      dynamic response = await Repository().getAllActiveInterviewTypes(createdBy);
      if (response != null) {
        _allActiveInterviewTypeResponseModel = AllInterviewTypeResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_allActiveInterviewTypeResponseModel");
    notifyListeners();
    return _allActiveInterviewTypeResponseModel;
  }

  BaseResponse? _addInterviewTypesResponse;

  BaseResponse? get addInterviewTypesResponse {
    return _addInterviewTypesResponse;
  }

  Future<BaseResponse?> addInterviewTypesAPI(
      String interviewType, String createdBy, bool systemTestRequired, bool mailSent, String mailSubject, String mailContent) async {
    _isFetching = true;
    _isHavingData = false;
    _addInterviewTypesResponse = BaseResponse();
    try {
      dynamic response = await Repository().addInterviewTypes(interviewType, createdBy, systemTestRequired, mailSent, mailSubject, mailContent);
      if (response != null) {
        _addInterviewTypesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_addInterviewTypesResponse");
    notifyListeners();
    return _addInterviewTypesResponse;
  }

  InterviewTypeDetailsResponseModel? _interviewTypeDetailsResponseModel;

  InterviewTypeDetailsResponseModel? get getInterviewTypeDetails {
    return _interviewTypeDetailsResponseModel;
  }

  Future<InterviewTypeDetailsResponseModel?> viewInterviewTypesAPI(String interviewTypeId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _interviewTypeDetailsResponseModel = InterviewTypeDetailsResponseModel();
    try {
      dynamic response = await Repository().viewInterviewTypes(interviewTypeId, createdBy);
      if (response != null) {
        _interviewTypeDetailsResponseModel = InterviewTypeDetailsResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_interviewTypeDetailsResponseModel");
    notifyListeners();
    return _interviewTypeDetailsResponseModel;
  }

  BaseResponse? _updateInterviewTypesResponse;

  BaseResponse? get updateInterviewTypesResponse {
    return _updateInterviewTypesResponse;
  }

  Future<BaseResponse?> updateInterviewTypesAPI(String interviewType, String interviewTypeId, String createdBy, bool systemTestRequired,
      bool mailSent, String mailSubject, String mailContent) async {
    _isFetching = true;
    _isHavingData = false;
    _updateInterviewTypesResponse = BaseResponse();
    try {
      dynamic response =
          await Repository().updateInterviewTypes(interviewType, interviewTypeId, createdBy, systemTestRequired, mailSent, mailSubject, mailContent);
      if (response != null) {
        _updateInterviewTypesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_updateInterviewTypesResponse");
    notifyListeners();
    return _updateInterviewTypesResponse;
  }

  BaseResponse? _deleteInterviewTypesResponse;

  BaseResponse? get deleteInterviewTypesResponse {
    return _deleteInterviewTypesResponse;
  }

  Future<BaseResponse?> deleteInterviewTypesAPI(String interviewTypeId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _deleteInterviewTypesResponse = BaseResponse();
    try {
      dynamic response = await Repository().deleteInterviewTypes(interviewTypeId, createdBy);
      if (response != null) {
        _deleteInterviewTypesResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_deleteInterviewTypesResponse");
    notifyListeners();
    return _deleteInterviewTypesResponse;
  }

  BaseResponse? _activateDeActivateInterviewTypeResponse;

  BaseResponse? get activateDeActivateInterviewTypeResponse {
    return _activateDeActivateInterviewTypeResponse;
  }

  Future<BaseResponse?> activateOrDeActivateInterviewTypesAPI(String interviewTypeId, String createdBy) async {
    _isFetching = true;
    _isHavingData = false;
    _activateDeActivateInterviewTypeResponse = BaseResponse();
    try {
      dynamic response = await Repository().activateOrDeActivateInterviewTypes(interviewTypeId, createdBy);
      if (response != null) {
        _activateDeActivateInterviewTypeResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_activateDeActivateInterviewTypeResponse");
    notifyListeners();
    return _activateDeActivateInterviewTypeResponse;
  }
}
