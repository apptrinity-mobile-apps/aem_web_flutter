import 'package:aem/model/all_interview_type_response_model.dart';
import 'package:aem/screens/interview_types/ui/add_interview_types_fragment.dart';
import 'package:aem/screens/interview_types/view_model/interview_types_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:aem/widgets/snack_bars.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InterviewTypesFragment extends StatefulWidget {
  const InterviewTypesFragment({Key? key}) : super(key: key);

  @override
  _InterviewTypesFragmentState createState() => _InterviewTypesFragmentState();
}

class _InterviewTypesFragmentState extends State<InterviewTypesFragment> {
  late InterviewTypeNotifier viewModel;
  String employeeId = "";
  bool editPermission = false, addPermission = false, viewPermission = false;
  ScrollController tableScrollController = ScrollController();

  @override
  void initState() {
    viewModel = Provider.of<InterviewTypeNotifier>(context, listen: false);
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    editPermission = userViewModel.interviewTypeEditPermission;
    addPermission = userViewModel.interviewTypeAddPermission;
    viewPermission = userViewModel.interviewTypeViewPermission;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      margin: const EdgeInsets.fromLTRB(25, 25, 20, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("$home > $navMenuInterviewTypes", maxLines: 1, softWrap: true, overflow: TextOverflow.ellipsis, style: fragmentDescStyle),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Card(
                elevation: 3,
                color: CupertinoColors.white,
                child: Container(
                  height: ScreenConfig.height(context),
                  width: ScreenConfig.width(context),
                  margin: const EdgeInsets.fromLTRB(25, 20, 25, 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      header(),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                          child: allStatusesList(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(navMenuInterviewTypes, maxLines: 1, softWrap: true, style: fragmentHeaderStyle),
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(descriptionInterviewType, softWrap: true, style: fragmentDescStyle),
              )
            ],
          ),
        ),
        Expanded(
          child: FittedBox(
            alignment: Alignment.centerRight,
            fit: BoxFit.scaleDown,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                addPermission
                    ? Container(
                        decoration: buttonDecorationGreen,
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextButton(
                          onPressed: () {
                            // showAddInterviewTypeDialog(null);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => const AddInterviewTypesFragment(),
                                transitionDuration: const Duration(seconds: 0),
                                reverseTransitionDuration: const Duration(seconds: 0),
                              ),
                            ).then((value) {
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(addInterviewType, style: newButtonsStyle),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                /*InkWell(
                  onTap: () {},
                  child: Image.asset("assets/images/menu_rounded.png", color: CupertinoColors.black.withOpacity(0.5)),
                )*/
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget allStatusesList() {
    return FutureBuilder(
        future: viewModel.viewAllInterviewTypesAPI(employeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: loader(),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text('error ${snapshot.error}', style: logoutHeader),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as AllInterviewTypeResponseModel;
              if (data.responseStatus == 1) {
                if (data.interviewTypesList!.isNotEmpty) {
                  return Column(
                    children: [
                      listHeader(),
                      Flexible(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            controller: ScrollController(),
                            itemCount: data.interviewTypesList!.length,
                            itemBuilder: (context, index) {
                              return listItems(data.interviewTypesList![index], index);
                            }),
                      )
                    ],
                  );
                } else {
                  return Center(
                    child: Text(noDataAvailable, style: logoutHeader),
                  );
                }
              } else {
                return Center(
                  child: Text(data.result!, style: logoutHeader),
                );
              }
            } else {
              return Center(
                child: Text(pleaseTryAgain, style: logoutHeader),
              );
            }
          } else {
            return Center(
              child: loader(),
            );
          }
        });
  }

  Widget listHeader() {
    return Container(
      decoration: buttonTopCircularDecorationGreen,
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(statusName, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text('System Test', softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(email, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(status, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.center),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(action, softWrap: true, style: listHeaderStyle, textAlign: TextAlign.end),
            ),
          ),
        ],
      ),
    );
  }

  Widget listItems(InterviewTypesList itemData, int index) {
    var alternate = index % 2;
    bool isActivated = false;
    if (itemData.status == 1) {
      isActivated = true;
    } else if (itemData.status == 0) {
      isActivated = false;
    } else {
      isActivated = false;
    }
    return Container(
      color: alternate == 0 ? listItemColor : listItemColor1,
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 25, 5),
              child: Text(itemData.interviewType!.inCaps, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(itemData.systemTestRequired! ? yes : no, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
              child: Text(itemData.mailSent! ? yes : no, softWrap: true, style: listItemsStyle, textAlign: TextAlign.start),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: StatefulBuilder(
                builder: (context, _setState) {
                  return /*AnimatedToggleSwitch(
                      initialValue: isActivated,
                      width: ScreenConfig.width(context) / 9.5,
                      values: const ['Active', 'Inactive'],
                      onToggleCallback: (bool value) {
                        if (editPermission) {
                          _setState(() {
                            isActivated = value;
                          });
                          Future.delayed(const Duration(milliseconds: 160), () async {
                            await viewModel.activateOrDeActivateInterviewTypesAPI(itemData.id!, itemData.createdBy!).then((value) {
                              if (viewModel.activateDeActivateInterviewTypeResponse != null) {
                                if (viewModel.activateDeActivateInterviewTypeResponse!.responseStatus == 1) {
                                  showToast(viewModel.activateDeActivateInterviewTypeResponse!.result!);
                                } else {
                                  showToast(viewModel.activateDeActivateInterviewTypeResponse!.result!);
                                }
                              } else {
                                showToast(pleaseTryAgain);
                              }
                            });
                            // set state to reload data , reloads data in futureBuilder
                            setState(() {});
                          });
                        } else {
                          showToast(noPermissionToAccess);
                        }
                      },
                      buttonColor: buttonBg,
                      backgroundColor: sideMenuSelected,
                      textColor: CupertinoColors.white,
                    );*/
                      OutlinedButton(
                    onPressed: () {
                      if (editPermission) {
                        _setState(() {
                          isActivated = !isActivated;
                        });
                        Future.delayed(const Duration(milliseconds: 160), () async {
                          await viewModel.activateOrDeActivateInterviewTypesAPI(itemData.id!, itemData.createdBy!).then((value) {
                            if (viewModel.activateDeActivateInterviewTypeResponse != null) {
                              if (viewModel.activateDeActivateInterviewTypeResponse!.responseStatus == 1) {
                                showToast(viewModel.activateDeActivateInterviewTypeResponse!.result!);
                              } else {
                                showToast(viewModel.activateDeActivateInterviewTypeResponse!.result!);
                              }
                            } else {
                              showToast(pleaseTryAgain);
                            }
                          });
                          // set state to reload data , reloads data in futureBuilder
                          setState(() {});
                        });
                      } else {
                        showToast(pleaseTryAgain);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        isActivated ? "Active" : "Inactive",
                        style: const TextStyle(
                          letterSpacing: 0.9,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(isActivated ? buttonBg : dashBoardCardOverDueText),
                      overlayColor: MaterialStateProperty.all(isActivated ? buttonBg.withOpacity(0.1) : dashBoardCardOverDueText.withOpacity(0.1)),
                      side: MaterialStateProperty.all(BorderSide(color: isActivated ? buttonBg : dashBoardCardOverDueText)),
                      elevation: MaterialStateProperty.all(2),
                    ),
                  );
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 5, 10, 5),
              child: Wrap(
                // mainAxisAlignment: MainAxisAlignment.end,
                alignment: WrapAlignment.end,
                crossAxisAlignment: WrapCrossAlignment.end,
                runAlignment: WrapAlignment.end,
                runSpacing: 4,
                spacing: 4,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: edit,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/edit_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (itemData.status == 1) {
                                if (editPermission) {
                                  // showAddInterviewTypeDialog(itemData);
                                  Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      pageBuilder: (context, animation1, animation2) =>
                                          AddInterviewTypesFragment(interviewTypeId: itemData.id!, createdBy: itemData.createdBy!),
                                      transitionDuration: const Duration(seconds: 0),
                                      reverseTransitionDuration: const Duration(seconds: 0),
                                    ),
                                  ).then((value) {
                                    // set state to reload data , reloads data in futureBuilder
                                    setState(() {});
                                  });
                                } else {
                                  showToast(noPermissionToAccess);
                                }
                              } else {
                                snackBarDark(context, "Interview type must be 'active' for updating.");
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Tooltip(
                      message: delete,
                      preferBelow: false,
                      decoration: toolTipDecoration,
                      textStyle: toolTipStyle,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Material(
                        elevation: 0.0,
                        shape: const CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage('assets/images/delete_rounded.png'),
                          fit: BoxFit.cover,
                          width: 35,
                          height: 35,
                          child: InkWell(
                            onTap: () {
                              if (editPermission) {
                                showDeleteDialog(itemData);
                              } else {
                                showToast(noPermissionToAccess);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  showDeleteDialog(InterviewTypesList itemData) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(delete, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(text: doYouWantDelete, style: logoutContentHeader),
                TextSpan(
                  text: ' ${itemData.interviewType} ',
                  style: const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16),
                ),
                TextSpan(text: 'Interview Type?', style: logoutContentHeader),
              ]),
            ),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () async {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        await viewModel.deleteInterviewTypesAPI(itemData.id!, itemData.createdBy!).then((value) {
                          Navigator.pop(context);
                          if (value != null) {
                            if (value.responseStatus == 1) {
                              showToast(value.result!);
                              // set state to reload data , reloads data in futureBuilder
                              setState(() {});
                              Navigator.pop(context);
                            } else {
                              showToast(value.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
}
