import 'dart:html';

import 'package:aem/global_view_model/global_notifier.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/login/ui/login_screen.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/navigation_service.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quiver/async.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with WidgetsBindingObserver {
  late LoginNotifier viewModel;
  late CountdownTimer _countdownTimer;
  String employeeId = "";

  @override
  void initState() {
    _countdownTimer = CountdownTimer(const Duration(seconds: 30), const Duration(seconds: 1));
    if (kIsWeb) {
      window.addEventListener('focus', onFocus);
      window.addEventListener('blur', onBlur);
    } else {
      WidgetsBinding.instance.addObserver(this);
    }
    viewModel = Provider.of<LoginNotifier>(context, listen: false);
    viewModel.getEmployeeDetails();
    employeeId = viewModel.employeeId.toString();
    Provider.of<GlobalNotifier>(context, listen: false).setExpandableTilesStates();
    Future.delayed(const Duration(seconds: 1), () {
      if (viewModel.isLogged) {
        // Navigator.pushNamedAndRemoveUntil(context, RouteNames.homeRoute, (route) => false, arguments: {'menu': RouteNames.dashBoardRoute});
        Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => HomeScreen(menu: RouteNames.dashBoardRoute),
                transitionDuration: const Duration(seconds: 0),
                reverseTransitionDuration: const Duration(seconds: 0)),
            (route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => const LoginPage(),
                transitionDuration: const Duration(seconds: 0),
                reverseTransitionDuration: const Duration(seconds: 0)),
            (route) => false);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appBackground,
        body: Container(
          height: ScreenConfig.height(context),
          width: ScreenConfig.width(context),
          color: loginPaneLeft,
          child: Column(
            children: [
              Expanded(child: Image.asset("assets/images/logo_2x.png")),
              Expanded(child: Image.asset("assets/images/logo_bg.png")),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (kIsWeb) {
      window.removeEventListener('focus', onFocus);
      window.removeEventListener('blur', onBlur);
    } else {
      WidgetsBinding.instance.removeObserver(this);
    }
    super.dispose();
  }

  void onFocus(Event e) {
    didChangeAppLifecycleState(AppLifecycleState.resumed);
  }

  void onBlur(Event e) {
    didChangeAppLifecycleState(AppLifecycleState.paused);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state)  {
    if (viewModel.isLogged) {
      if (state == AppLifecycleState.paused) {
        _countdownTimer = CountdownTimer(const Duration(hours: 12), const Duration(seconds: 1));
      } else if (state == AppLifecycleState.resumed) {
        debugPrint("Ignore log AppLifecycleState timer - ${_countdownTimer.remaining.toString()}");
        if (_countdownTimer.remaining > const Duration(seconds: 0)) {
          debugPrint("Ignore log AppLifecycleState timer - running");
        } else {
          debugPrint(_countdownTimer.remaining.toString());
          debugPrint("Ignore log AppLifecycleState - timeout");
          // logout code
          Provider.of<LoginNotifier>(context, listen: false).employeeLogoutAPI(employeeId);
          SessionManager sessionManager = SessionManager();
          sessionManager.clearSession();
          Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).resetExpandedState();
          Provider.of<LoginNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).resetValues();
          Navigator.pushAndRemoveUntil(
              NavigationService.navigatorKey.currentContext!,
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => const SplashScreen(),
                transitionDuration: const Duration(seconds: 0),
                reverseTransitionDuration: const Duration(seconds: 0),
              ),
                  (route) => false);
        }
        if (mounted) {
          _countdownTimer.cancel();
        }
      }
    }
    super.didChangeAppLifecycleState(state);
  }
}
