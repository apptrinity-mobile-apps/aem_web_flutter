
import 'package:aem/model/base_response_model.dart';
import 'package:aem/model/job_roles_list_response_mode.dart';
import 'package:aem/services/repositories.dart';
import 'package:flutter/cupertino.dart';

class JobRoleNotifier with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;


  Future<BaseResponse?> addJobRole(String employeeId, String role) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().addJobRole(employeeId,role);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<JobRolesResponseModel?> viewalljobrolesApi(String employeeId) async {
    _isFetching = true;
    _isHavingData = false;
    JobRolesResponseModel? _jobRolesResponseModel = JobRolesResponseModel();

    try {
      dynamic response = await Repository().viewalljobrolesApi(employeeId);
      if (response != null) {
        _jobRolesResponseModel = JobRolesResponseModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_jobRolesResponseModel");
    notifyListeners();
    return _jobRolesResponseModel;
  }
  Future<BaseResponse?> singlejobroleApi(String employeeId, String jobRoleId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().singlejobroleApi(employeeId,jobRoleId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<BaseResponse?> activatedeactivatejobroleApi(String employeeId, String jobRoleId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().activatedeactivatejobroleApi(employeeId,jobRoleId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<BaseResponse?> editjobroleApi(String employeeId, String jobRoleId,String role) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().editjobroleApi(employeeId,jobRoleId,role);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }
  Future<BaseResponse?> deletejobroleApi(String employeeId, String jobRoleId) async {
    _isFetching = true;
    _isHavingData = false;
    BaseResponse? _baseResponse = BaseResponse();

    try {
      dynamic response = await Repository().deletejobroleApi(employeeId,jobRoleId);
      if (response != null) {
        _baseResponse = BaseResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      debugPrint(e.toString());
    }
    _isFetching = false;
    debugPrint("fetch $_isFetching $_isHavingData $_baseResponse");
    notifyListeners();
    return _baseResponse;
  }

}