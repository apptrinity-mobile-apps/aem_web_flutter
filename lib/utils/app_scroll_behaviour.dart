import 'dart:ui';
import 'package:flutter/material.dart';

// for scrolling using mouse in pageView - web
class AppScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}