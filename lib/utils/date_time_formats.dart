import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const inputDateFormat = "E, dd MMM yyyy HH:mm:ss ZZZ";

String dateFormatDMYHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd-MM-yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatDMYSlash(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd/MM/yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatYMDHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('yyyy-MM-dd');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatMDYHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('MM-dd-yyyy');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatFullDate(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd MMMM yyyy, hh:mm a');
  var output = outputFormat.format(tempDate);
  return output;
}

String dateFormatFullDateDMYHyphen(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('dd-MM-yyyy, hh:mm a');
  var output = outputFormat.format(tempDate);
  return output;
}

String timeFormatHMS12hr(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('hh:mma');
  var output = outputFormat.format(tempDate).toLowerCase();
  return output;
}

String timeFormatHMS24hr(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('HH:mm');
  var output = outputFormat.format(tempDate).toLowerCase();
  return output;
}

String dayDate(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  var outputFormat = DateFormat('E, MMM dd');
  var outputDate = outputFormat.format(tempDate);
  return outputDate;
}

String getTodayDate() {
  String date = "";
  var dateNow = DateTime.now();
  var outputFormat = DateFormat('yyyy-MM-dd');
  date = outputFormat.format(dateNow);
  return date;
}

String dayAndTime(String date) {
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  String time = timeFormatHMS12hr(inputDate);
  String date1 = dateFormatDMYSlash(inputDate);
  var outputDate = "";
  if (daysDifference(tempDate) == -1) {
    // yesterday
    outputDate = "Yesterday, $time";
  } else if (daysDifference(tempDate) == 0) {
    // today
    outputDate = "Today, $time";
  } else {
    outputDate = "$date1, $time";
  }
  return outputDate;
}

int daysDifference(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
}

String formatTimeOfDay(TimeOfDay tod) {
  final now = DateTime.now();
  final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
  final format = DateFormat('hh:mm a');
  return format.format(dt);
}

String dateTimeHMSDateFormat(String date) {
  // returns hh:mm ss (dd th MMM yyyy)
  var outputDate = "";
  // var day = 1;
  String inputDate = date;
  DateTime tempDate = DateFormat(inputDateFormat).parse(inputDate, true).toLocal();
  // var dayFormat = DateFormat('dd');
  // day = int.parse(dayFormat.format(tempDate));
  // var outputFormat = DateFormat('hh:mm aa ($day${getDayOfMonthSuffix(day)} MMM yyyy)');
  var outputFormat = DateFormat('hh:mm aa (dd MMM yyyy)');
  outputDate = outputFormat.format(tempDate);
  return outputDate;
}

String secondsToHrs(int seconds) {
  // returns hh:mm
  int hours = (seconds / 3600).truncate();
  seconds = (seconds % 3600).truncate();
  int minutes = (seconds / 60).truncate();

  String hoursStr = (hours).toString().padLeft(2, '0');
  String minutesStr = (minutes).toString().padLeft(2, '0');
  // String secondsStr = (seconds % 60).toString().padLeft(2, '0');

  // return "$hoursStr:$minutesStr:$secondsStr hrs";
  return "$hoursStr:$minutesStr hrs";
}

String minutesToHrs(int minutes) {
  // returns hh:mm
  int hours = (minutes / 60).truncate();
  minutes = (minutes % 60).truncate();

  String hoursStr = (hours).toString().padLeft(2, '0');
  String minutesStr = (minutes).toString().padLeft(2, '0');

  return "$hoursStr:$minutesStr hrs";
}
String minutesToHrs1(double minutes) {
  // returns hh:mm
  double hours = (minutes / 60).truncate().toDouble();
  minutes = (minutes % 60).truncate().toDouble();

  String hoursStr = (hours).toString().padLeft(2, '0');
  String minutesStr = (minutes).toString().padLeft(2, '0');

  return "$hoursStr:$minutesStr hrs";
}
String convertTimeStampToDateTime(String date) {
  DateTime tempDate = DateFormat(inputDateFormat).parse(date, true).toLocal();
  return tempDate.toString();
}

String getTodayDateDDMMYYYY() {
  String date = "";
  var dateNow = DateTime.now();
  var outputFormat = DateFormat('dd-MM-yyyy');
  date = outputFormat.format(dateNow);
  return date;
}

class AgeDuration {
  int days;
  int months;
  int years;

  AgeDuration({this.days = 0, this.months = 0, this.years = 0});

  @override
  String toString() {
    return 'Years: $years, Months: $months, Days: $days';
  }
}

/// Age Class
class Age {
  /// _daysInMonth cost contains days per months; daysInMonth method to be used instead.
  static const List<int> _daysInMonth = [
    31, // Jan
    28, // Feb, it varies from 28 to 29
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31 // Dec
  ];

  /// isLeapYear method
  static bool isLeapYear(int year) => (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0));

  /// daysInMonth method
  static int daysInMonth(int year, int month) => (month == DateTime.february && isLeapYear(year)) ? 29 : _daysInMonth[month - 1];

  /// dateDifference method
  static AgeDuration dateDifference({required DateTime fromDate, required DateTime toDate, bool includeToDate = false}) {
    // Check if toDate to be included in the calculation
    DateTime endDate = (includeToDate) ? toDate.add(const Duration(days: 1)) : toDate;
    int years = endDate.year - fromDate.year;
    int months = 0;
    int days = 0;
    if (fromDate.month > endDate.month) {
      years--;
      months = (DateTime.monthsPerYear + endDate.month - fromDate.month);
      if (fromDate.day > endDate.day) {
        months--;
        days = daysInMonth(fromDate.year + years, ((fromDate.month + months - 1) % DateTime.monthsPerYear) + 1) + endDate.day - fromDate.day;
      } else {
        days = endDate.day - fromDate.day;
      }
    } else if (endDate.month == fromDate.month) {
      if (fromDate.day > endDate.day) {
        years--;
        months = DateTime.monthsPerYear - 1;
        days = daysInMonth(fromDate.year + years, ((fromDate.month + months - 1) % DateTime.monthsPerYear) + 1) + endDate.day - fromDate.day;
      } else {
        days = endDate.day - fromDate.day;
      }
    } else {
      months = (endDate.month - fromDate.month);
      if (fromDate.day > endDate.day) {
        months--;
        days = daysInMonth(fromDate.year + years, (fromDate.month + months)) + endDate.day - fromDate.day;
      } else {
        days = endDate.day - fromDate.day;
      }
    }
    return AgeDuration(days: days, months: months, years: years);
  }

  /// add method
  static DateTime add({required DateTime date, required AgeDuration duration}) {
    int years = date.year + duration.years;
    years += (date.month + duration.months) ~/ DateTime.monthsPerYear;
    int months = ((date.month + duration.months) % DateTime.monthsPerYear);
    int days = date.day + duration.days - 1;
    return DateTime(years, months, 1).add(Duration(days: days));
  }

  /// subtract methos
  static DateTime subtract({required DateTime date, required AgeDuration duration}) {
    duration.days *= -1;
    duration.months *= -1;
    duration.years *= -1;

    return add(date: date, duration: duration);
  }
}
