enum StatusText {
  newStr,
  ongoingStr,
  overdueStr,
  completeStr,
  reopenedStr,
  closedStr,
  openStr,
  closeStr,
  reopenStr,
  deleteStr,
  activateStr,
  inactivateStr,
  startStr,
  holdStr
}

extension StatusTextExtension on StatusText {
  String? get name {
    switch (this) {
      case StatusText.newStr:
        return "new";
      case StatusText.ongoingStr:
        return "ongoing";
      case StatusText.overdueStr:
        return "overdue";
      case StatusText.completeStr:
        return "complete";
      case StatusText.reopenedStr:
        return "reopened";
      case StatusText.closedStr:
        return "close";
      case StatusText.openStr:
        return "open";
      case StatusText.closeStr:
        return "close";
      case StatusText.reopenStr:
        return "reopen";
      case StatusText.deleteStr:
        return "delete";
      case StatusText.activateStr:
        return "activate";
      case StatusText.inactivateStr:
        return "inactivate";
      case StatusText.startStr:
        return "start";
      case StatusText.holdStr:
        return "hold";
      default:
        return null;
    }
  }
}
