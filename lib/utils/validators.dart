import 'package:aem/model/all_boardings_response_model.dart';
import 'package:aem/model/all_dashboard_boardings_model.dart';
import 'package:aem/model/all_designations_response_model.dart';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_interview_status_response_model.dart';
import 'package:aem/model/all_interview_type_response_model.dart';
import 'package:aem/model/all_profile_status_model_reponse.dart';
import 'package:aem/model/all_roles_response_model.dart';
import 'package:aem/model/all_stages_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/job_roles_list_response_mode.dart';
import 'package:aem/utils/strings.dart';
import 'package:html/parser.dart';

String? passwordValidator(String? value) {
  // RegExp regExp = RegExp(passwordPattern);
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  }
  /*else if (!regExp.hasMatch(value)) {
    return "Enter a valid password";
  } */
  else {
    return null;
  }
}

String? emailValidator(String? value) {
  RegExp regex = RegExp(emailPattern);
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (!regex.hasMatch(value)) {
    return enterValidEmail;
  } else {
    return null;
  }
}

String? phoneValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.length < 10) {
    return phone10Digits;
  } else {
    return null;
  }
}

String? emptyTextValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else {
    return null;
  }
}

String? rolesDropDownValidator(RolesDetails? value) {
  if (value == null || value.role == null) {
    return emptyFieldError;
  } else if (value.role.toString() == selectRole) {
    return pleaseSelectRole;
  } else {
    return null;
  }
}

String? profileStatusDropDownValidator(ProfilesStatusList? value) {
  if (value == null || value.id == null) {
    return emptyFieldError;
  } else if (value.id!.toLowerCase() == 'none') {
    return pleaseSelectStatus;
  } else {
    return null;
  }
}
String? jobRoleDropDownValidator(JobRolesList? value) {
  if (value == null || value.id == null) {
    return emptyFieldError;
  } else if (value.id!.toLowerCase() == 'none') {
    return pleaseSelectJobRole;
  } else {
    return null;
  }
}
String? stageDropDownValidator(DevelopmentStagesList? value) {
  if (value == null || value.id == null) {
    return emptyFieldError;
  } else if (value.id!.toLowerCase() == 'none') {
    return pleaseSelectJobRole;
  } else {
    return null;
  }
}
String? employeeDropDownValidator(EmployeeDetails? value) {
  if (value == null || value.name == null) {
    return emptyFieldError;
  } else if (value.name.toString() == selectEmployee) {
    return pleaseSelectEmployee;
  } else {
    return null;
  }
}

String? employeesDropDownValidator(List<EmployeeDetails?> value) {
  if (value.isEmpty) {
    return emptyFieldError;
  } else {
    return null;
  }
}

String? boardingDropDownValidator(BoardingsList? value) {
  if (value == null || value.boardName == null) {
    return emptyFieldError;
  } else if (value.boardName.toString() == selectTechnology) {
    return pleaseSelectTechnology;
  } else {
    return null;
  }
}
String? dashBoardBoardingDropDownValidator(BoardingsData? value) {
  if (value == null || value.boardName == null) {
    return emptyFieldError;
  } else if (value.boardName.toString() == selectTechnology) {
    return pleaseSelectTechnology;
  } else {
    return null;
  }
}

String? designationsDropDownValidator(DesignationsList? value) {
  if (value == null || value.name == null) {
    return emptyFieldError;
  } else if (value.name.toString() == selectDesignation) {
    return pleaseSelectDesignation;
  } else {
    return null;
  }
}

String? technologiesDropDownValidator(TechnologiesList? value) {
  if (value == null || value.name == null) {
    return emptyFieldError;
  } else if (value.name.toString() == selectTechnology) {
    return pleaseSelectTechnology;
  } else {
    return null;
  }
}

String? interviewStatusDropDownValidator(InterviewStatusList? value) {
  if (value == null || value.id == null) {
    return emptyFieldError;
  } else if (value.id.toString() == 'none') {
    return pleaseSelectInterviewStatus;
  } else {
    return null;
  }
}

String? interviewTypeDropDownValidator(InterviewTypesList? value) {
  if (value == null || value.id == null) {
    return emptyFieldError;
  } else if (value.id.toString() == 'none') {
    return pleaseSelectInterviewType;
  } else {
    return null;
  }
}

String? timeScheduleValidator(String? value) {
  print(value);
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.toLowerCase() == "00:00 AM".toLowerCase()) {
    return selectValidTime;
  } else {
    return null;
  }
}

String? pinPutValidator(String? value) {
  if (value == null || value.isEmpty) {
    return emptyFieldError;
  } else if (value.length != 6) {
    return pinPutLengthError;
  } else {
    return null;
  }
}

String parseHtmlString(String htmlString) {
  final document = parse(htmlString);
  final String parsedString = parse(document.body!.text).documentElement!.text;

  return parsedString;
}