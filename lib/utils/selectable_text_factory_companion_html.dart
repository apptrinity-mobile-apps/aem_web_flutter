import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fwfh_selectable_text/fwfh_selectable_text.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

class SelectableTextCompanion extends WidgetFactory with SelectableTextFactory {
  @override
  SelectionChangedCallback? get selectableTextOnChanged => (selection, cause) {
        // do something when the selection changes
  };
}
