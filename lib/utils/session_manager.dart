import 'dart:convert';
import 'package:aem/model/employee_permissions_response.dart';
import 'package:aem/model/login_response_model.dart';
import 'package:aem/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  // saving employee details
  saveLoginEmployeeData(String employeeData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(keyEmployeeData, employeeData);
  }

  Future<EmployeeData> getLoginEmployeeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = prefs.getString(keyEmployeeData);
    if (data == "" || data == null) {
      return EmployeeData();
    } else {
      return EmployeeData.fromJson(jsonDecode(data));
    }
  }

  void clearEmployeeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(keyEmployeeData, "");
  }

  isEmployeeLogin(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(keyEmployeeLoggedIn, isLogin);
  }

  Future<bool?> isEmployeeLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(keyEmployeeLoggedIn) ?? false;
  }

  saveEmployeePermissions(String permissions) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(keyEmployeePermissions, permissions);
  }

  Future<EmployeePermissions> getEmployeePermissions() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = prefs.getString(keyEmployeePermissions);
    if (data == "" || data == null) {
      return EmployeePermissions();
    } else {
      return EmployeePermissions.fromJson(jsonDecode(data));
    }
  }

  // clearing total session data
  Future clearSession() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Set<String> keys = preferences.getKeys();
    for (String key in keys) {
      preferences.remove(key);
    }
  }
}
