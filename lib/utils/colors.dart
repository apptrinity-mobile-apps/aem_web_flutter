import 'package:flutter/material.dart';

const Color loginPaneRight = Color(0xFFFEFFF9);
const Color loginPaneLeft = Color(0xFF1C2B2E);
const Color editText = Color(0xFF1C2B2E);
const Color editTextHint = Color(0x801C2B2E);
const Color editTextBorder = Color(0xFFC7C9BE);
const Color buttonBg = Color(0xFF7AA500);
const Color appBarBackground = Color(0xFF132023);
const Color appBackground = Color(0xFF1C2B2E);
const Color dropDownColor = Color(0xFFFFC809);
const Color dropDownTextColor = Color(0xFF91A5A8);
const Color cardBg = Color(0xFFF2F6F9);
const Color circleBorder = Color(0xFFE8E9E3);
const Color footerText = Color(0xFF758B8F);
const Color sideMenuSelected = Color(0xFFD8EAEE);
const Color sideMenuUnSelected = Color(0xFF83A1A7);
const Color dashBoardCardTile = Color(0xFFF6FBFF);
const Color dashBoardCardHoverTile = Color(0xFF7AA500);
const Color dashBoardCardBorderTile = Color(0xFFD2D7DB);
const Color dashBoardCardNewTile = Color(0xFF7AA500);
const Color dashBoardCardOverDueText = Color(0xFFFF6E6E);
const Color dashBoardCardCountText = Color(0xFF3F484A);
const Color listItemColor = Color(0xFFF3F7FA);
const Color listItemColor1 = Color(0xFFFBFDFF);
const Color addNewContainerBg = Color(0xFFFEFEFE);
const Color addRoleText = Color(0xFF29324A);
const Color textFieldBorder = Color(0xFFC9CED9);
const Color textFieldHint = Color(0xFFA0A5AF);
const Color planColor1 = Color(0xFFE7EED5);
const Color planColor2 = Color(0xFFF6EDD2);
const Color timeText = Color(0xFF7B7B7B);
const Color newTaskText = Color(0xFF6283CF);
const Color newOnGoing = Color(0xFFFF9710);
const Color holdBgColor = Color(0xFFE58211);
const Color newOverDue = Color(0xFFFF0000);
const Color newCompleted = Color(0xFF7AA500);
const Color newReOpened = Color(0xFFAC4848);
const Color profileCommentsBg = Color(0xFFF2F4F5);
const Color buttonraisebug = Color(0xFFDF3A3A);
const Color tabsborder = Color(0xFFD7E8A3);
const Color addScheduleButton = Color(0xFF707070);
const Color availableTimesText = Color(0xFF4B503F);
const Color availableDateText = Color(0xFF35383A);
const Color searchGoButtonBg = Color(0xFFFFF5D5);
const Color totalProfilesCardColor = Color(0xFFD0F6A2);
const Color totalProfilesCardBgColor = Color(0xFFF9FFF2);
const Color todayInterviewsCardColor = Color(0xFFFFF3CE);
const Color todayInterviewsCardBgColor = Color(0xFFFFFEFB);
const Color filterBg = Color(0xFFF1F8DE);
const Color resetBg = Color(0xFFFFE7E2);
const Color filterByBorder = Color(0xFFE8E9E3);
const Color mailPreviewBorder = Color(0xFFE5E9D5);
const Color mailPreviewBg = Color(0xFFFCFCFC);
Color? getNameBasedColor(String char) {
  switch (char.toLowerCase()) {
    case 'a':
      {
        return Colors.deepOrangeAccent[400];
      }
    case 'b':
      {
        return Colors.deepOrangeAccent;
      }
    case 'c':
      {
        return Colors.pink[300];
      }
    case 'd':
      {
        return Colors.orange[700];
      }
    case 'e':
      {
        return Colors.lightGreen[700];
      }
    case 'f':
      {
        return Colors.lightGreen[700];
      }
    case 'g':
      {
        return Colors.purple[300];
      }
    case 'h':
      {
        return Colors.purple;
      }
    case 'i':
      {
        return Colors.purple[700];
      }
    case 'j':
      {
        return Colors.deepPurple[300];
      }
    case 'k':
      {
        return Colors.deepPurple;
      }
    case 'l':
      {
        return Colors.deepPurple[700];
      }
    case 'm':
      {
        return Colors.red[300];
      }
    case 'n':
      {
        return Colors.red;
      }
    case 'o':
      {
        return Colors.red[700];
      }
    case 'p':
      {
        return Colors.redAccent[400];
      }
    case 'q':
      {
        return Colors.blue;
      }
    case 'r':
      {
        return Colors.blue[700];
      }
    case 's':
      {
        return Colors.lightBlue[600];
      }
    case 't':
      {
        return Colors.lightBlue[800];
      }
    case 'u':
      {
        return Colors.brown[300];
      }
    case 'v':
      {
        return Colors.brown;
      }
    case 'w':
      {
        return Colors.blueGrey;
      }
    case 'x':
      {
        return Colors.blueGrey[700];
      }
    case 'y':
      {
        return Colors.blueGrey[900];
      }
    case 'z':
      {
        return Colors.pink;
      }
    default:
      return buttonBg;
  }
  // return buttonBg;
}
