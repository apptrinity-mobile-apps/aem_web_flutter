import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/model/all_technologies_response_model.dart';
import 'package:aem/model/view_profile_details_model.dart';
import 'package:jiffy/jiffy.dart';

const String appName = "aem";
const String adminLogin = "Login";
const String userName = "User Name";
const String incorrectUserName = "Incorrect $userName";
const String emailUserName = "Email or User Name";
const String invalidEmailUserName = "Invalid $emailUserName";
const String email = "Email";
const String invalidEmail = "Invalid $email";
const String incorrectEmail = "Incorrect $email";
const String password = "Password";
const String incorrectPassword = "Incorrect $password";
const String login = "Login";
const String loginSuccess = "Logged in Successfully!";
const String logout = "Logout";
const String logoutSuccess = "Logged out Successfully!";
const String newStr = "New";
const String add = "Add";
const String hr = "Hr";
const String estimation = "Estimation";
const String estimationTime = "$estimation Time:";
const String estimationTimeHint = "$estimation Time in Minutes";
const String estimationTimeScrum = "$estimation Time";
const String fullAccess = "Full Access";
const String updateStatus = "$update $status";
const String create = "Create";
const String created = "Created";
const String update = "Update";
const String overDue = "Overdue";
const String employee = "Employee";
const String technology = "Technology";
const String project = "Project";
const String designation = "Designation";
const String role = "Role";
const String profile = "Profile";
const String home = "Home";
const String stage = "Stage";
const String createNewEmployee = "$create $newStr $employee";
const String createEmployee = "$create $employee";
const String addNewTechnology = "$add $newStr $technology";
const String addNewProject = "$add $newStr $project";
const String addDesignation = "$add $designation";
const String addStage = "$add $stage";
const String addRole = "$add $role";
const String addProfile = "$add $profile";
const String addProfileStatus = "$add $profile $status";
const String addInterviewType = "$add Interview type";
const String updateInterviewType = "$update Interview type";
const String addInterviewerStatus = "$add Interviewer $status";
const String addJobRole = "$add $jobRole";
const String jobRole = "Job $role";
const String updateInterviewerStatus = "$update Interviewer $status";
const String updateProfileStatus = "$update $profile $status";
const String updateEmployee = "$update $employee";
const String updateTechnology = "$update $technology";
const String updateProject = "$update $project";
const String updateDesignation = "$update $designation";
const String updateStage = "$update $stage";
const String updateRole = "$update $role";
const String updateProfile = "$update $profile";
const String viewProfile = "$view $profile";
const String name = "Name";
const String phone = "Phone Number";
const String uploadResume = "Upload Resume";
const String resume = "Resume";
const String roleName = "Role $name";
const String statusName = "$status $name";
const String orderBy = "Order By";
const String createDate = "${create}d date";
const String action = "Action";
const String delete = "Delete";
const String edit = "Edit";
const String view = "View";
const String select = "Select";
const String status = "Status";
const String displayEnable = "Display Enable";
const String sno = "S.No";
const String schedule = "Schedule";
const String date = "Date";
const String dateFormat = "DD/MM/YYYY";
const String submit = "Submit";
const String permissions = "Permissions";
const String designationName = "$designation $name";
const String stageName = "$stage $name";
const String technologyName = "$technology $name";
const String uploadImage = "Upload Image";
const String uploadTechLogo = "Upload Technology Image";
const String selectDesignation = "$select $designation";
const String selectTechnology = "$select $technology";
const String selectRole = "$select $role";
const String projectName = "$project $name";
const String description = "Description";
const String projectDescription = "Type a description here";
const String selectEmployee = "Select Employee";
const String assainEmployee = "Assained Employees";
const String selectTechnology1 = "Select Technology";
const String selectStatus = "Select Status";
const String selectjobRole = "Select $jobRole";
const String selectStage = "Select $stage";
const String assignEmployee = "Assign $employee";
const String inviteSomePeople = "Invite some people";
const String projectToDo = "To-dos";
const String projectNew = "New";
const String projectOnGoing = "OnGoing";
const String projectOverdue = "Overdue";
const String projectCompleted = "Done";
const String projectReOpened = "Reopened";
const String projectDescriptions = "Post announcements, pitch ideas, progress updates, etc.";
const String projectActivity = "$project activity";
const String newTasks = "New Tasks";
const String viewAs = "View As";
const String markComplete = "Mark this Complete";
const String newList = "New List +";
const String addThisList = "Add this list";
const String addTodo = "Add a to-do";
const String addTask = "Add a task";
const String nameThisList = "Name this list...";
const String addExtraDetails = "Add extra details...";
const String cancel = "Cancel";
const String ok = "Ok";
const String yes = "Yes";
const String no = "No";
const String describe = "Describe:";
const String describeTodo = "Describe this to-do...";
const String assignedTo = "Assigned to:";
const String assignedToTester = "Assigned to tester:";
const String whenDone = "When done, Notify:";
const String dueOn = "Due on:";
const String taskTime = "Task time:";
const String taskTimeScrum = "Task time";
const String notes = "Notes:";
const String addComment = "Add a comment here...";
const String addedBy = "Added by:";
const String typeNamesAssign = "Type names to assign...";
const String typeNamesNotify = "Type names to notify...";
const String selectDate = "Select a date...";
const String reOpen = "Reopen";
const String confirmLogout = "Are you sure you want to $logout?";
const String pleaseTryAgain = "Please try again!";
const String noDataAvailable = "No data available";
const String noSlotsAvailable = "No available slots";
const String noPermissionToAccess = "You don't have permission to access!";
const String emptyFieldError = "This field cannot be empty!";
const String emailPattern = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
const String passwordPattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
const String pleaseSelectRole = "Please select a Role!";
const String pleaseSelectEmployee = "Please select a Employee!";
const String pleaseSelectEmployeeStatus = "Please select Employee Status!";
const String pleaseSelectInterviewStatus = "Please select Interviewer Status!";
const String pleaseSelectInterviewType = "Please select Interview Type!";
const String pleaseSelectDesignation = "Please select a Designation!";
const String pleaseSelectTechnology = "Please select a Technology!";
const String pleaseSelectStatus = "Please select a Status!";
const String pleaseSelectJobRole = "Please select a Job Role!";
const String enterValidEmail = "Enter valid $email address";
const String phone10Digits = "Phone number must be 10 digits!";
const String taskDescriptionStr = "Task Description";
const String taskNameStr = "Task Name";
const String doYouWantDelete = "Do you want to delete";
const String maxCount = "99+";
const String unsupportedImageFormat = "Unsupported image format!";
const String commentHold = "Add Comment for Hold";
const String comment = "Comment";
const String comments = "Comments";
const String upload = "Upload";
const String filter = "Filter";
const String reset = "Reset";
const String closeDay = "Close Day";
const String experience = "Experience";
const String tableSNo = "S.No";
const String inProgress = "In Progress";
const String tableName = "Name";
const String tableEmail = "Email";
const String tablePhoneNumber = "Phone Number";
const String tableCreatedBy = "Created By";
const String tableStatus = "Status";
const String tableActions = "Actions";
const String tableRounds = "Rounds";
const String tableInterviewer = "Interviewer";
const String tableScheduleDate = "Schedule date";
const String bugs = "Bugs";
const String tasks_bugs = "This is Task Bugs List";
const String raise_bug = "Raise a bug";
const String modules = "Modules";
const String tasks = "Tasks";
const String addtestcase = "Add Test Case";
const String priority = "Priority";
const String tableBugsTitle = "Bugs Title";
const String tableModuleName = "Module Name";
const String tableTask = "Task";
const String unavailable = "Unavailable";
const String back = "Back";
const String myProfile = "My $profile";
const String save = "Save";
const String source = "Source";
const String assign = "Assign";
const String currentPassword = "Current $password";
const String newPassword = "New $password";
const String confirmPassword = "Confirm $password";
const String forgotPassword = "Forgot $password";
const String resetPassword = "Reset $password";
const String selectValidTime = "Select valid time";
const String pinPutLengthError = "OTP must be 6 characters";
const String boarding = "Boarding";
const String addNewBoarding = "$add $newStr $boarding";
const String updateBoarding = "$update $boarding";
const String selectBoarding = "$select $boarding";
const String boardingName = "$boarding $name";
const navMenuBoarding = "Boardings";
const String projectTaskTimeEstimation = "Time Estimation";
const String workingHours = "Working Hours";
const String totalWorkingDuration = "Total Working Duration : ";
const String totalDuration = "Total Duration";
const String tableEmpName = "Emp Name";
const String tableStartTime = "Start Time";
const String tableEndTime = "End Time";
const String duration = "Duration";
const String bugName = "Bug Name";
const String bugsWorkingDetails = "Bugs Working Details";
const String inActive = "In Active";
const String open = "Open";
const String close = "Close";
const String hold = "Hold";
const String today = "Today";
const String daily = "Daily";
const String monthly = "Monthly";
const String yearly ="Yearly";
const String searchGo = "Go";
const String start = "Start";
const String complete = "Done";
const String pending = "Pending";
const String profilesLoading="Please wait, Loading more profiles.";
const String teamcount = "Team Count";
const String reasonExtraTime = "Reason for extra time...";
const String commentForcloseDay = "Comment for Closing Day...";
const String selectProject = "Select Project";
const String selectPriority= "Select Priority";
const String selectModule = "Select Module";
const String selectTask = "Select Task";
const String other = "Other";
const String addNewTask = "Add New Task";
const String is_superior = "Is Superior";
const String empScrumReport = "Emp Scrum Report";
const String bug_high = "High";
const String bug_medium = "Medium";
const String bug_low = "Low";
const String bug_total = "Total";
const String closeDayReason= 'Closing Reason : ';
String durationToString(int minutes) {
  var d = Duration(minutes:minutes);
  List<String> parts = d.toString().split(':');
  return '${parts[0].padLeft(2, '0')}.${parts[1].padLeft(2, '0')}';
}
// content descriptions
const String descriptionEmployees =
    "Under this section we get the comprehensive records of all the employees/team members. The overall details about the employees/team members get displayed once they are added using the “create new” option tab.";
const String descriptionAddEmployee =
    "Relevant information about each employees/team members based on their designation, technology, role, availability in work and its duration will be updated under this section. It provides an overall detail about the employees/team members inclusive of their designation, technology and role.";
const String descriptionTechnology =
    "Ultimate update about the designated technology assigned to the employees/team members will be done under this section. The technologies are assigned as per the employees/team members’ specializations.";
const String descriptionAddTechnology = "Simply, the new technology and its relevant image will get updated under this section.";
const String descriptionProject =
    "Comprehensive list of the ongoing/new projects from all specializations gets updated under this section once it gets assigned to the relevant employees";
const String descriptionAddProject =
    "Using this section we smoothly assign a project to the employees/team members based on their specializations, with clear work related description and name of the assigned employee/employees’.";
const String descriptionDesignation =
    "Using this section we update the all-inclusive names of the designations of all the employees/team members. The information about relevant specializations and its date of creation gets clearly displayed.";
const String descriptionAddDesignation = "The employees/team members name gets updated here for better clarity under this section.";
const String descriptionRole =
    "The information regarding all employees/team members’ responsibilities as per their relevant expertise will be updated under this section.";
const String descriptionAddRole =
    "This section will provide a comprehensive list of the roles as per the employees/team members’ specializations along with level of permissions granted for full access.";
const String descriptionProfile =
    "Under this section we keep a track of all the interviews being conducted for the candidates and their selection. We update the overall information about the interviewee and the scenario/status of the development_stages of candidates’ interview rounds until its completion.";
const String descriptionAddProfile =
    "The overall information of the interviewee gets updated under this section including his specializations and profile status for future reference of the interviewer. It also includes the name of the interviewer/ interviewers and their remarks on the candidature.";
const String descriptionProfileStatus =
    "Under this section we add the current status of the interviewees’ candidature. It enables us to get an updated overview of the interview being conducted.";
const String descriptionAddProfileStatus =
    "We provide a complete status about the interviewee inclusive of all relevant actions the candidate had to pass through. It enables the candidate to receive an email if he/she gets selected, if not, there will be no email sent.";
const String descriptionInterviewType =
    "Under this section we update clear information about the candidates’ present status in the interview process. It enables us to understand how exactly the interview went through leading to its final result.";
const String descriptionAddInterviewType = "Simply, the name of the interview round will be updated.";
const String descriptionInterviewStatus =
    "Once the candidate’s interview gets over, the overall final status of the interviewee will be updated under this section. It will enable us to understand and keep a track of the final interview results.";
const String descriptionAddInterviewStatus = "Simply, includes the interviewer status name.";
const String descriptionDashboard = "The projects will appear under this section as they get added.";
const String descriptionTodos = "Under this section we will see all the activities that people should or shouldn’t perform. It happens after adding new list while we assign certain work to one or more team members.";
const String descriptionProjectActivity = "The comprehensive list of activities undertaken by employees/team members will be displayed with the relevant dates for future reference.";
const String descriptionNewTask = "Under this section we update all information regarding an assigned new task. The entire list of tasks will later appear along with deadlines for future reference.";
const String descriptionOnGoing = "The day-to-day activities, with a defined start or end date and an estimated effort. This section will be utilized to count the amount of time you’ve invested on frequent tasks that have a delivery date.";
const String descriptionOverdue = "The day-to-day activities, with a defined start or end date and an estimated effort if not met successfully then it comes under this section. It directly demonstrates the overall productivity of the team members within the organization.";
const String descriptionCompleted = "The day-to-day activities, with a defined start or end date and an estimated effort if gets completed with desire outcome then it comes under section. It directly reflects the overall productivity of the team members within the organization.";
const String descriptionBugs="The day-to-day activities with a defined start or end date and an estimated effort if falls into an error, it must appear under this section. The team members resolving the bugs and its status will also be updated in bugs list for future reference.";
const String descriptionTasks = "The relevant activities based on a specific project will appear under this section. The tasks will keep on changing as per the type of projects.";
const String descriptionTestcase = "The significant requirements of the project will appear under this section. This communicates about the unique requirements for the tasks.";
const String descriptionComments = "Discussion based on the project/task will be taking place under this section. This section aims at updating all the day-to-day communications happening related to the tasks.";
const String descriptionActivityLog = "The comprehensive information of all the activities and discussions related to every task taking place under the specific project will be updated under this section. The idea is to utilize it for future reference.";
const String descriptionBugsList = "Once a task gets done by the team members, if in case it has any issue, it will appear under bugs. The team members will then work on resolving the bugs issue as per the task bugs list.";
// navigation menu keys
const navMenuChat = "Chat";
const String navMenuMainHRMgmt = "HR Management";
const String navMenuMainProjectMgmt = "Project Management";
const navMenuDashboard = "Dashboard";
const navMenuScrum = "Scrum";
const navMenuSprint = "Sprint";
const navMenuEmployees = "Employees";
const navMenuTechnologies = "Technologies";
const navMenuProjects = "Projects";
const navMenuDesignations = "Designations";
const navMenuStages = "Development Stages";
const navMenuRoles = "Roles";
const navMenuProfiles = "Profiles";
const navMenuInterviewTypes = "Interview Types";
const navMenuProfileStatus = "Profile Status";
const navMenuInterviewerStatus = "Interviewer Status";
const navMenuJobRole = "Job Role";
const String projectTaskSprints = "Sprints";
const String addsprint = "Add Sprint";
// session manager keys
const String keyEmployeeData = "login_details";
const String keyEmployeeLoggedIn = "is_employee_login";
const String keyEmployeePermissions = "employee_permissions";
const String copyRightsLine = "Term & Conditions | Privacy Policy © 2022 AEM. All rights reserved";
const String dummy = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's since the 1500s";
// durations
const String min15 = '15 Minutes';
const String min30 = '30 Minutes';
const String min45 = '45 Minutes';
const String min60 = '60 Minutes';
const String custom = 'Custom';
const String fixed = 'Fixed';
const String mon = 'Monday';
const String tue = 'Tuesday';
const String wed = 'Wednesday';
const String thu = 'Thursday';
const String fri = 'Friday';
const String sat = 'Saturday';
const String sun = 'Sunday';
String getScrumFilterType(int type) {
  switch (type) {
    case 0:
      {
        return today;
      }
    case 1:
      {
        return daily;
      }
    case 2:
      {
        return monthly;
      }
    case 3:
      {
        return yearly;
      }
    default:
      return today;
  }
}
String commaSeparatedTechnologiesString(List<TechnologyList> list) {
  List<String> names = [];
  String returnValue = "";
  if (list.isNotEmpty) {
    for (var element in list) {
      names.add(element.technologyName!);
    }
    returnValue = names.join(', ');
  }
  return returnValue;
}
String formatAgo (dynamic date){
 //print("date ${Jiffy(date + " 04:11 PM", "MM-dd-yyyy hh:mm a").fromNow()}");

 // return Jiffy(date, "MM-dd-yyyy").fromNow();
 return Jiffy(date,"MM-dd-yyyy hh:mm a").fromNow();
}
String commaSeparatedDesignationsString(List<DesignationList> list) {
  List<String> names = [];
  String returnValue = "";
  if (list.isNotEmpty) {
    for (var element in list) {
      names.add(element.designationName!);
    }
    returnValue = names.join(', ');
  }
  return returnValue;
}
String commaSeparatedTechnologyString(List<Technology> list) {
  List<String> names = [];
  String returnValue = "";
  if (list.isNotEmpty) {
    for (var element in list) {
      names.add(element.name!);
    }
    returnValue = names.join(', ');
  }
  return returnValue;
}
extension CapExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${substring(1)}';
  String get allInCaps => toUpperCase();
}
List<String> bugStatuses = ['Status', 'New', 'InProgress', 'Hold', 'Done', 'In Review', 'Reopened', 'Closed'];
List<String> bugPriorities = ['Low', 'Medium', 'High'];
String getDayOfMonthSuffix(int dayNum) {
  if(!(dayNum >= 1 && dayNum <= 31)) {
    throw Exception('Invalid day of month');
  }
  if(dayNum >= 11 && dayNum <= 13) {
    return 'th';
  }
  switch(dayNum % 10) {
    case 1: return 'st';
    case 2: return 'nd';
    case 3: return 'rd';
    default: return 'th';
  }
}
const String search = "Search";
String commaSeparatedTechnologiessString(List<TechnologiesList> list) {
  List<String> names = [];
  String returnValue = "";
  if (list.isNotEmpty) {
    for (var element in list) {
      names.add(element.name!);
    }
    returnValue = names.join(', ');
  }
  return returnValue;
}

String commaSeparatedEmployeesString(List<EmployeeDetails> list) {
  List<String> names = [];
  String returnValue = "";
  if (list.isNotEmpty) {
    for (var element in list) {
      names.add(element.name!);
    }
    returnValue = names.join(', ');
  }
  return returnValue;
}
const String totalProfiles = "Total Profiles";
const String todayInterviews = "Today Interviews";
const String upcomingInterviews = "Upcoming Interviews";
const String todaysInterviews = "Today's Interviews";
const String noInterviewsToday = "No Interviews Schedule for today";
