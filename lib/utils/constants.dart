import 'package:aem/utils/colors.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

const double sideMenuMaxWidth = 250.0;
const double sideMenuMinWidth = 50.0;
const EdgeInsets editTextPadding = EdgeInsets.fromLTRB(10, 10, 10, 10);
const EdgeInsets editTextLoginPadding = EdgeInsets.fromLTRB(15, 15, 15, 15);
const EdgeInsets loginIconsPadding = EdgeInsets.fromLTRB(25, 0, 10, 0);
const EdgeInsets dateUploadIconsPadding = EdgeInsets.fromLTRB(10, 5, 10, 5);

/* decorators for containers */
Decoration buttonCircularDecorationGreen = BoxDecoration(borderRadius: BorderRadius.circular(50), color: buttonBg);
Decoration buttonDecorationGreen = BoxDecoration(borderRadius: BorderRadius.circular(5), color: buttonBg);
Decoration buttonDecorationRed = BoxDecoration(borderRadius: BorderRadius.circular(5), color: buttonraisebug);
Decoration buttonTopCircularDecorationGreen =
    const BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)), color: buttonBg);
Decoration toolTipDecoration =
    BoxDecoration(borderRadius: BorderRadius.circular(5), color: CupertinoColors.white, border: Border.all(color: editText, width: 0.25));
Decoration markDoneDecoration =
    BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: editText, width: 1), color: addNewContainerBg);
Decoration markDoneIconDecoration = BoxDecoration(borderRadius: BorderRadius.circular(5), color: buttonBg);
Decoration cardBorder = BoxDecoration(border: Border.all(color: dashBoardCardBorderTile, width: 1), color: addNewContainerBg);
Decoration buttonBorderGreen =
    BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: buttonBg, width: 1), color: CupertinoColors.white);
Decoration attahcmentIconDecoration = BoxDecoration(borderRadius: BorderRadius.circular(25), color: buttonBg);
Decoration buttonBorderGray =
BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: editTextBorder, width: 1), color: CupertinoColors.white);
/* decorators for edit texts */
InputDecoration editTextUserNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: loginRoundedBorder,
    isDense: true,
    enabledBorder: loginRoundedBorder,
    focusedBorder: loginRoundedBorder,
    errorBorder: loginRoundedErrorBorder,
    focusedErrorBorder: loginRoundedErrorBorder,
    contentPadding: editTextLoginPadding,
    hintStyle: editTextUserNameHintStyle,
    hintText: userName,
    prefixIcon: Padding(child: Image.asset("assets/images/user_name.png"), padding: loginIconsPadding));
InputDecoration editTextPasswordDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: loginRoundedBorder,
    isDense: true,
    enabledBorder: loginRoundedBorder,
    focusedBorder: loginRoundedBorder,
    errorBorder: loginRoundedErrorBorder,
    focusedErrorBorder: loginRoundedErrorBorder,
    contentPadding: editTextLoginPadding,
    hintStyle: editTextPasswordHintStyle,
    hintText: password,
    prefixIcon: Padding(child: Image.asset("assets/images/password.png"), padding: loginIconsPadding));
InputDecoration editTextRoleNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: roleName);
InputDecoration editTextDesignationNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: designationName);
InputDecoration editTextTechnologyNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: technologyName);
InputDecoration editTextProjectNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: projectName);
InputDecoration editTextProjectDescriptionDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: border,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: projectDescription);
InputDecoration editTextDateDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: dateFormat,
    suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
    suffixIcon: Padding(child: Image.asset("assets/images/calendar.png", color: textFieldBorder), padding: dateUploadIconsPadding));
InputDecoration editTextTechnologyImageDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: uploadTechLogo,
    suffixIconConstraints: const BoxConstraints(maxHeight: 40, maxWidth: 40),
    suffixIcon: Padding(child: Image.asset("assets/images/upload.png", color: textFieldBorder), padding: dateUploadIconsPadding));
InputDecoration editTextNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: border,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: name);
InputDecoration editTextDesignationDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: designation);
InputDecoration editTextEmailDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: border,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: email);
InputDecoration editTextTechnologyDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: technology);
InputDecoration editTextPhoneNumberDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: phone);
InputDecoration editTextRoleDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: role);
InputDecoration editTextEmployeesDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: const EdgeInsets.all(5),
    hintStyle: textFieldHintStyle,
    hintText: employee);

InputDecoration dropDownDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: dropDownBorderGreen,
    enabledBorder: dropDownBorderGreen,
    focusedBorder: dropDownBorderGreen,
    errorBorder: dropDownBorderGreen,
    focusedErrorBorder: dropDownBorderGreen,
    contentPadding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    hintStyle: textFieldHintStyle);
InputDecoration editTextTodoDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: const EdgeInsets.fromLTRB(30, 5, 20, 5),
    hintStyle: textFieldTodoNameStyle,
    hintText: nameThisList);
InputDecoration editTextTodoHintDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: const EdgeInsets.fromLTRB(30, 15, 20, 15),
    hintStyle: textFieldTodoHintStyle,
    hintText: nameThisList);
InputDecoration editTextTodoDescriptionDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoHintStyle,
    hintText: describeTodo);
InputDecoration editTextTodoAssignDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoHintStyle,
    hintText: typeNamesAssign);
InputDecoration editTextTodoNotifyDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoHintStyle,
    hintText: typeNamesNotify);
InputDecoration editTextTodoDateDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoHintStyle,
    hintText: selectDate);
InputDecoration editTextBoardingNameDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: boardingName);
InputDecoration editTextTodoExtraDetailsDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoHintStyle,
    hintText: addExtraDetails);
InputDecoration editTextTodoCommentDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    isDense: true,
    border: InputBorder.none,
    enabledBorder: InputBorder.none,
    focusedBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    focusedErrorBorder: InputBorder.none,
    contentPadding: editTextPadding,
    hintStyle: textFieldTodoCommentHintStyle,
    hintText: addComment);
InputDecoration editTextStatusDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: status);
InputDecoration editTextPriorityDecoration = InputDecoration(
    fillColor: CupertinoColors.white,
    filled: true,
    border: border,
    isDense: true,
    enabledBorder: border,
    focusedBorder: border,
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
    contentPadding: editTextPadding,
    hintStyle: textFieldHintStyle,
    hintText: priority);
PinTheme defaultPinTheme = PinTheme(
  width: 52,
  height: 52,
  margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
  textStyle: fragmentHeaderStyle,
  decoration: BoxDecoration(
    color: Colors.white,
    border: Border.all(color: editTextBorder, width: 1.5),
    borderRadius: const BorderRadius.all(
      Radius.circular(5),
    ),
  ),
);

/* text styles */
TextStyle editTextUserNameHintStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editTextHint, fontSize: 18);
TextStyle editTextPasswordHintStyle =
    const TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editTextHint, fontSize: 18);
TextStyle editTextUserNameStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 18);
TextStyle editTextPasswordStyle =
    const TextStyle(letterSpacing: 1.25, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 18);
TextStyle submitButtonStyle =
    const TextStyle(letterSpacing: 2.7, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: CupertinoColors.white, fontSize: 18);
TextStyle loginInfoStyle = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w300, color: editText, fontSize: 16);
TextStyle loginStyle = const TextStyle(letterSpacing: 2.15, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 28);
TextStyle dropDownStyle =
    const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: dropDownTextColor, fontSize: 14);
TextStyle footerStyle = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: footerText, fontSize: 12);
TextStyle sideMenuUnSelectedStyle =
    const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: sideMenuUnSelected, fontSize: 14);
TextStyle sideMenuSelectedStyle =
    const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: sideMenuSelected, fontSize: 14);
TextStyle fragmentHeaderStyle =
    const TextStyle(letterSpacing: 1.75, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 24);
TextStyle fragmentDescStyle = const TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 12);
TextStyle dashBoardTileStyle = const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w800, color: editText, fontSize: 18);
TextStyle dashBoardTileHoveredStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w800, color: CupertinoColors.white, fontSize: 18);
TextStyle tileCountStyle =
    const TextStyle(letterSpacing: 1.55, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: dashBoardCardCountText, fontSize: 15);
TextStyle tileOverDueStyle =
    const TextStyle(letterSpacing: 0.95, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: dashBoardCardOverDueText, fontSize: 14);
TextStyle tileNewStyle =
    const TextStyle(letterSpacing: 0.95, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: dashBoardCardNewTile, fontSize: 14);
TextStyle newButtonsStyle =
    const TextStyle(letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: CupertinoColors.white, fontSize: 15);
TextStyle cancelButtonStyle =  TextStyle(letterSpacing: 0.9, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 15);
TextStyle listHeaderStyle =
    const TextStyle(letterSpacing: 0.85, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 14);
TextStyle listItemsStyle = const TextStyle(letterSpacing: 0.51, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14);
TextStyle technologyItemStyle =
    const TextStyle(letterSpacing: 0.66, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 18);
TextStyle technologyItemHoveredStyle =
    const TextStyle(letterSpacing: 0.66, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 18);
TextStyle projectTitleStyle = const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w800, color: editText, fontSize: 18);
TextStyle projectTitleHoveredStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w800, color: CupertinoColors.white, fontSize: 18);
TextStyle projectDescStyle = const TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14);
TextStyle projectDescHoveredStyle =
    const TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: CupertinoColors.white, fontSize: 14);
TextStyle projectMembersStyle =
    const TextStyle(letterSpacing: 0.57, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 16);
TextStyle toolTipStyle = const TextStyle(letterSpacing: 0.57, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 12);
TextStyle addRoleSubStyle = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: addRoleText, fontSize: 16);
TextStyle addRoleSubBoldStyle =
    const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: addRoleText, fontSize: 16);
TextStyle textFieldHintStyle =
    const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: textFieldHint, fontSize: 16);
TextStyle textFieldStyle = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 16);
TextStyle projectCountStyle = const TextStyle(letterSpacing: 2, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 22);
TextStyle projectCountHoveredStyle =
    const TextStyle(letterSpacing: 2, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: CupertinoColors.white, fontSize: 22);
TextStyle projectTextStyle = const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 16);
TextStyle projectTextHoveredStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: CupertinoColors.white, fontSize: 16);
TextStyle projectDescriptionStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 12);
TextStyle projectDescriptionHoveredStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 12);
TextStyle projectDateStyle = const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16);
TextStyle projectIconTextStyle =
    const TextStyle(letterSpacing: 0.81, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 16);
TextStyle listItemsheaderStyle = const TextStyle(letterSpacing: 0.51, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: buttonBg, fontSize: 14);
TextStyle taskIconTextStyle =
    const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 12);
TextStyle projectTimeStyle = const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: timeText, fontSize: 12);
TextStyle projectDateMenuStyle =
    const TextStyle(letterSpacing: 0.8, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: timeText, fontSize: 14);
TextStyle projectAddedByStyle = const TextStyle(letterSpacing: 1, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: editText, fontSize: 15);
TextStyle projectAddedByNameStyle =
    const TextStyle(letterSpacing: 1, fontFamily: "Poppins", fontWeight: FontWeight.w600, color: buttonBg, fontSize: 15);
TextStyle completedStyle = const TextStyle(letterSpacing: 0.6, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 14);
TextStyle newTaskHeaderStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: newTaskText, fontSize: 16);
TextStyle newOnGoingHeaderStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: newOnGoing, fontSize: 16);
TextStyle newOverDueHeaderStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: newOverDue, fontSize: 16);
TextStyle newCompletedHeaderStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: newCompleted, fontSize: 16);
TextStyle newReOpenedHeaderStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: newReOpened, fontSize: 16);
TextStyle newTaskEmployeesStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 14);
TextStyle newTaskEmployeesIconStyle =
    const TextStyle(letterSpacing: 0.39, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 10);
TextStyle markDoneStyle = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 10);
TextStyle addTodoStyle = const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 12);
TextStyle textFieldTodoNameStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 16);
TextStyle textFieldTodoHintStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 14);
TextStyle textFieldTodoTitlesStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 13);
TextStyle textFieldTodoFieldsStyle =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 13);
TextStyle textFieldTodoCommentHintStyle =
    const TextStyle(letterSpacing: 1.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14);
TextStyle logoutHeader = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 18);
TextStyle logoutContentHeader =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 16);
TextStyle logoutButtonHeader = const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14);
TextStyle popMenuHeader =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: CupertinoColors.black, fontSize: 14);
TextStyle dropDown =
    TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText.withOpacity(0.8), fontSize: 14);
TextStyle dropDownSelected =
    TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText.withOpacity(1), fontSize: 14);
TextStyle snackBarDarkText =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.black, fontSize: 14);
TextStyle snackBarLightText =
    const TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: CupertinoColors.white, fontSize: 14);
TextStyle profilepopHeader = const TextStyle(letterSpacing: 0, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: buttonBg, fontSize: 18);
TextStyle buglisttitleHeader =
    const TextStyle(letterSpacing: 0.54, fontFamily: "Poppins", fontWeight: FontWeight.w500, color: editText, fontSize: 16);
TextStyle bugstatustitle = const TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14);
TextStyle bugstatus = const TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w700, color: editText, fontSize: 14);

/* borders for edit texts */
OutlineInputBorder loginRoundedBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: editTextBorder, width: 1), borderRadius: BorderRadius.circular(50));
OutlineInputBorder loginRoundedErrorBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1), borderRadius: BorderRadius.circular(50));
OutlineInputBorder border =
    OutlineInputBorder(borderSide: const BorderSide(color: textFieldBorder, width: 1.5), borderRadius: BorderRadius.circular(5));
OutlineInputBorder errorBorder =
    OutlineInputBorder(borderSide: const BorderSide(color: Colors.redAccent, width: 1.5), borderRadius: BorderRadius.circular(5));
OutlineInputBorder dropDownBorderGreen =
    OutlineInputBorder(borderSide: const BorderSide(color: buttonBg, width: 1.5), borderRadius: BorderRadius.circular(5));
