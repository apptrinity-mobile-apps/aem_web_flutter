import 'package:aem/model/employee_availability_model.dart';

class AllEmployeesResponse {
  List<EmployeeDetails>? employeeDetails;
  int? responseStatus;
  String? result;
  EmployeeDetails? employeeData;

  AllEmployeesResponse({this.employeeDetails, this.responseStatus, this.result, this.employeeData});

  AllEmployeesResponse.fromJson(Map<String, dynamic> json) {
    if (json['employeeDetails'] != null) {
      employeeDetails = <EmployeeDetails>[];
      json['employeeDetails'].forEach((v) {
        employeeDetails!.add(EmployeeDetails.fromJson(v));
      });
    } else {
      employeeDetails = <EmployeeDetails>[];
    }
    employeeData = json['employeeData'] != null ? EmployeeDetails.fromJson(json['employeeData']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (employeeDetails != null) {
      data['employeeDetails'] = employeeDetails!.map((v) => v.toJson()).toList();
    }
    if (employeeData != null) {
      data['employeeData'] = employeeData!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}
class EmployeeDetails {
  String? createdOn;
  List<DesignationList>? designationsList;
  List<Employees>? employees;
  String? email;
  String? id;
  String? name;
  String? phoneNumber;
  String? roleId;
  String? roleName;
  int? status;
  List<TechnologyList>? technologiesList;
  int? duration;
  String? durationType;
  AvailabilitySchedules? availabilitySchedule;
  bool? isSuperiorEmployee;
  EmployeeDetails(
      {this.createdOn,
      this.designationsList,
      this.email,
      this.id,
      this.name,
      this.phoneNumber,
      this.roleId,
      this.roleName,
      this.status,
      this.technologiesList,
      this.duration,
      this.durationType,
      this.availabilitySchedule,
      this.isSuperiorEmployee,
      });
  EmployeeDetails.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'] ?? "";
    if (json['designationsList'] != null) {
      designationsList = <DesignationList>[];
      json['designationsList'].forEach((v) {
        designationsList!.add(DesignationList.fromJson(v));
      });
    } else {
      designationsList = <DesignationList>[];
    }
    if (json['assignableEmployees'] != null) {
      employees = <Employees>[];
      json['assignableEmployees'].forEach((v) {
        employees!.add(Employees.fromJson(v));
      });
    } else {
      employees = <Employees>[];
    }
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    roleId = json['roleId'] ?? "";
    roleName = json['roleName'] ?? "";
    status = json['status'] ?? 0;
    if (json['technologiesList'] != null) {
      technologiesList = <TechnologyList>[];
      json['technologiesList'].forEach((v) {
        technologiesList!.add(TechnologyList.fromJson(v));
      });
    } else {
      technologiesList = <TechnologyList>[];
    }
    duration = json['duration'] ?? 0;
    durationType = json['durationType'] ?? "";
    isSuperiorEmployee = json['isSuperiorEmployee'] ?? false;
    availabilitySchedule = json['availabilitySchedule'] != null ? AvailabilitySchedules.fromJson(json['availabilitySchedule']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdOn'] = createdOn;
    if (designationsList != null) {
      data['designationsList'] = designationsList!.map((v) => v.toJson()).toList();
    }
    data['email'] = email;
    data['id'] = id;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['roleId'] = roleId;
    data['roleName'] = roleName;
    data['status'] = status;
    if (technologiesList != null) {
      data['technologiesList'] = technologiesList!.map((v) => v.toJson()).toList();
    }
    data['duration'] = duration;
    data['durationType'] = durationType;
    data['isSuperiorEmployee'] = isSuperiorEmployee;
    if (availabilitySchedule != null) {
      data['availabilitySchedule'] = availabilitySchedule!.toJson();
    }
    return data;
  }

}

class DesignationList {
  String? designationId;
  String? designationName;

  DesignationList({this.designationId, this.designationName});

  DesignationList.fromJson(Map<String, dynamic> json) {
    designationId = json['designationId'] ?? "";
    designationName = json['designationName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['designationId'] = designationId;
    data['designationName'] = designationName;
    return data;
  }
}
class Employees {
  String? id;
  String? name;

  Employees({this.id, this.name});

  Employees.fromJson(Map<String, dynamic> json) {
    id = json['empId'];
    name = json['empName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.id;
    data['empName'] = this.name;
    return data;
  }
}
class TechnologyList {
  String? technologyId;
  String? technologyName;

  TechnologyList({this.technologyId, this.technologyName});

  TechnologyList.fromJson(Map<String, dynamic> json) {
    technologyId = json['technologyId'] ?? "";
    technologyName = json['technologyName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['technologyId'] = technologyId;
    data['technologyName'] = technologyName;
    return data;
  }
}

class AvailabilitySchedules {
  bool? sundayAvailability;
  List<Durations>? sundayTimings;
  bool? mondayAvailability;
  List<Durations>? mondayTimings;
  bool? tuesdayAvailability;
  List<Durations>? tuesdayTimings;
  bool? wednesdayAvailability;
  List<Durations>? wednesdayTimings;
  bool? thursdayAvailability;
  List<Durations>? thursdayTimings;
  bool? fridayAvailability;
  List<Durations>? fridayTimings;
  bool? saturdayAvailability;
  List<Durations>? saturdayTimings;

  AvailabilitySchedules(
      {this.sundayAvailability,
        this.sundayTimings,
        this.mondayAvailability,
        this.mondayTimings,
        this.tuesdayAvailability,
        this.tuesdayTimings,
        this.wednesdayAvailability,
        this.wednesdayTimings,
        this.thursdayAvailability,
        this.thursdayTimings,
        this.fridayAvailability,
        this.fridayTimings,
        this.saturdayAvailability,
        this.saturdayTimings});

  AvailabilitySchedules.fromJson(Map<String, dynamic> json) {
    sundayAvailability = json['sunday_availability'] ?? false;
    if (json['sunday_timings'] != null) {
      sundayTimings = <Durations>[];
      json['sunday_timings'].forEach((v) {
        sundayTimings!.add(Durations.fromJson(v));
      });
    } else {
      sundayTimings = <Durations>[];
    }
    mondayAvailability = json['monday_availability'] ?? false;
    if (json['monday_timings'] != null) {
      mondayTimings = <Durations>[];
      json['monday_timings'].forEach((v) {
        mondayTimings!.add(Durations.fromJson(v));
      });
    } else {
      mondayTimings = <Durations>[];
    }
    tuesdayAvailability = json['tuesday_availability'] ?? false;
    if (json['tuesday_timings'] != null) {
      tuesdayTimings = <Durations>[];
      json['tuesday_timings'].forEach((v) {
        tuesdayTimings!.add(Durations.fromJson(v));
      });
    } else {
      tuesdayTimings = <Durations>[];
    }
    wednesdayAvailability = json['wednesday_availability'] ?? false;
    if (json['wednesday_timings'] != null) {
      wednesdayTimings = <Durations>[];
      json['wednesday_timings'].forEach((v) {
        wednesdayTimings!.add(Durations.fromJson(v));
      });
    } else {
      wednesdayTimings = <Durations>[];
    }
    thursdayAvailability = json['thursday_availability'] ?? false;
    if (json['thursday_timings'] != null) {
      thursdayTimings = <Durations>[];
      json['thursday_timings'].forEach((v) {
        thursdayTimings!.add(Durations.fromJson(v));
      });
    } else {
      thursdayTimings = <Durations>[];
    }
    fridayAvailability = json['friday_availability'] ?? false;
    if (json['friday_timings'] != null) {
      fridayTimings = <Durations>[];
      json['friday_timings'].forEach((v) {
        fridayTimings!.add(Durations.fromJson(v));
      });
    } else {
      fridayTimings = <Durations>[];
    }
    saturdayAvailability = json['saturday_availability'] ?? false;
    if (json['saturday_timings'] != null) {
      saturdayTimings = <Durations>[];
      json['saturday_timings'].forEach((v) {
        saturdayTimings!.add(Durations.fromJson(v));
      });
    } else {
      saturdayTimings = <Durations>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['sunday_availability'] = sundayAvailability;
    if (sundayTimings != null) {
      data['sunday_timings'] = sundayTimings!.map((v) => v.toJson()).toList();
    }
    data['monday_availability'] = mondayAvailability;
    if (mondayTimings != null) {
      data['monday_timings'] = mondayTimings!.map((v) => v.toJson()).toList();
    }
    data['tuesday_availability'] = tuesdayAvailability;
    if (tuesdayTimings != null) {
      data['tuesday_timings'] = tuesdayTimings!.map((v) => v.toJson()).toList();
    }
    data['wednesday_availability'] = wednesdayAvailability;
    if (wednesdayTimings != null) {
      data['wednesday_timings'] = wednesdayTimings!.map((v) => v.toJson()).toList();
    }
    data['thursday_availability'] = thursdayAvailability;
    if (thursdayTimings != null) {
      data['thursday_timings'] = thursdayTimings!.map((v) => v.toJson()).toList();
    }
    data['friday_availability'] = fridayAvailability;
    if (fridayTimings != null) {
      data['friday_timings'] = fridayTimings!.map((v) => v.toJson()).toList();
    }
    data['saturday_availability'] = saturdayAvailability;
    if (saturdayTimings != null) {
      data['saturday_timings'] = saturdayTimings!.map((v) => v.toJson()).toList();
    }
    return data;
  }

  bool checkIfAnyIsNull() {
    return [sundayAvailability,
      sundayTimings,
      mondayAvailability,
      mondayTimings,
      tuesdayAvailability,
      tuesdayTimings,
      wednesdayAvailability,
      wednesdayTimings,
      thursdayAvailability,
      thursdayTimings,
      fridayAvailability,
      fridayTimings,
      saturdayAvailability,
      saturdayTimings].contains(null);
  }
}
