class NavMenuItem {
  final String title;
  final String? route;
  final String? icon;
  final String? tooltip_title;

  const NavMenuItem({
    required this.title,
    this.route,
    this.icon,
    this.tooltip_title,
  });
}
