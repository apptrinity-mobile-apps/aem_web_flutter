class JobRolesResponseModel {
  List<JobRolesList>? jobRolesList;
  int? responseStatus;
  String? result;

  JobRolesResponseModel({this.jobRolesList, this.responseStatus, this.result});

  JobRolesResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['jobRolesList'] != null) {
      jobRolesList = <JobRolesList>[];
      json['jobRolesList'].forEach((v) {
        jobRolesList!.add(new JobRolesList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.jobRolesList != null) {
      data['jobRolesList'] = this.jobRolesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class JobRolesList {
  String? createdBy;
  String? createdOn;
  String? id;
  String? role;
  int? status;

  JobRolesList(
      {this.createdBy, this.createdOn, this.id, this.role, this.status});

  JobRolesList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    id = json['id'];
    role = json['role'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['role'] = this.role;
    data['status'] = this.status;
    return data;
  }
}