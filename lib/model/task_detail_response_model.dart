import 'dart:async';

import 'package:aem/model/all_employees_response_model.dart';

class TaskResponseModel {
  int? responseStatus;
  String? result;
  List<TaskBugsList>? taskBugsList;
  List<TaskCommentsList>? taskCommentsList;
  TaskData? taskData;
  List<TaskHistoryList>? taskHistoryList;
  List<TaskTestCasesList>? taskTestCasesList;
  int? taskEstimationTime;
  int? taskActivityTime;
  TaskResponseModel(
      {this.responseStatus,
        this.result,
        this.taskBugsList,
        this.taskCommentsList,
        this.taskData,
        this.taskHistoryList,
        this.taskTestCasesList,
        this.taskEstimationTime,
        this.taskActivityTime
      });
  TaskResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    taskEstimationTime = json['taskEstimationTime']??0;
    taskActivityTime = json['taskActivityTime']??0;
    if (json['taskBugsList'] != null) {
      taskBugsList = <TaskBugsList>[];
      json['taskBugsList'].forEach((v) {
        taskBugsList!.add(new TaskBugsList.fromJson(v));
      });
    }
    if (json['taskCommentsList'] != null) {
      taskCommentsList = <TaskCommentsList>[];
      json['taskCommentsList'].forEach((v) {
        taskCommentsList!.add(new TaskCommentsList.fromJson(v));
      });
    }
    taskData = json['taskData'] != null
        ? new TaskData.fromJson(json['taskData'])
        : null;
    if (json['taskHistoryList'] != null) {
      taskHistoryList = <TaskHistoryList>[];
      json['taskHistoryList'].forEach((v) {
        taskHistoryList!.add(new TaskHistoryList.fromJson(v));
      });
    }
    if (json['taskTestCasesList'] != null) {
      taskTestCasesList = <TaskTestCasesList>[];
      json['taskTestCasesList'].forEach((v) {
        taskTestCasesList!.add(new TaskTestCasesList.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['taskEstimationTime'] = this.taskEstimationTime ?? 0;
    data['taskActivityTime'] = this.taskActivityTime ?? 0;
    if (this.taskBugsList != null) {
      data['taskBugsList'] = this.taskBugsList!.map((v) => v.toJson()).toList();
    }
    if (this.taskCommentsList != null) {
      data['taskCommentsList'] =
          this.taskCommentsList!.map((v) => v.toJson()).toList();
    }
    if (this.taskData != null) {
      data['taskData'] = this.taskData!.toJson();
    }
    if (this.taskHistoryList != null) {
      data['taskHistoryList'] =
          this.taskHistoryList!.map((v) => v.toJson()).toList();
    }
    if (this.taskTestCasesList != null) {
      data['taskTestCasesList'] =
          this.taskTestCasesList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
class TaskBugsList {
  List<AssignedEmployeesList>? assignedEmployeesList;
  List<String>? attachments;
  String? bugStatus;
  String? createdOn;
  String? description;
  String? emoployeeName;
  String? employeeId;
  String? devStageId;
  String? id;
  String? moduleId;
  String? moduleName;
  String? priority;
  String? projectId;
  String? projectName;
  String? taskId;
  String? taskName;
  String? title;
  int? involvedTime;
  Timer? timer;



  TaskBugsList(
      {this.assignedEmployeesList,
        this.attachments,
        this.bugStatus,
        this.createdOn,
        this.description,
        this.emoployeeName,
        this.employeeId,
        this.id,
        this.moduleId,
        this.moduleName,
        this.priority,
        this.projectId,
        this.projectName,
        this.taskId,
        this.taskName,
        this.title,
      this.involvedTime,
      this.timer,
        this.devStageId
      });

  TaskBugsList.fromJson(Map<String, dynamic> json) {
    attachments = json['attachments'].cast<String>();
    if (json['assignedEmployeesList'] != null) {
      assignedEmployeesList = <AssignedEmployeesList>[];
      json['assignedEmployeesList'].forEach((v) {
        assignedEmployeesList!.add(new AssignedEmployeesList.fromJson(v));
      });
    }
    bugStatus = json['bugStatus'];
    createdOn = json['createdOn'];
    description = json['description'];
    emoployeeName = json['emoployeeName'];
    employeeId = json['employeeId'];
    id = json['id'];
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    priority = json['priority'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    title = json['title'];
    involvedTime = json['involvedTime'];
    devStageId = json['devStageId'] ?? "";

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedEmployeesList != null) {
      data['assignedEmployeesList'] =
          this.assignedEmployeesList!.map((v) => v.toJson()).toList();
    }
    data['attachments'] = this.attachments;
    data['bugStatus'] = this.bugStatus;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['emoployeeName'] = this.emoployeeName;
    data['employeeId'] = this.employeeId;
    data['id'] = this.id;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['priority'] = this.priority;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['title'] = this.title;
    data['involvedTime'] = this.involvedTime;
    data['devStageId'] = this.devStageId;

    return data;
  }
}
class AssignedEmployeesList {
  String? id;
  String? name;

  AssignedEmployeesList({this.id, this.name});

  AssignedEmployeesList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
class TaskCommentsList {
  String? comment;
  String? createdOn;
  String? emoployeeName;
  String? employeeId;
  String? id;
  String? moduleId;
  String? moduleName;
  String? projectId;
  String? projectName;
  String? taskId;
  String? taskName;

  TaskCommentsList(
      {this.comment,
        this.createdOn,
        this.emoployeeName,
        this.employeeId,
        this.id,
        this.moduleId,
        this.moduleName,
        this.projectId,
        this.projectName,
        this.taskId,
        this.taskName});

  TaskCommentsList.fromJson(Map<String, dynamic> json) {
    comment = json['comment'];
    createdOn = json['createdOn'];
    emoployeeName = json['emoployeeName'];
    employeeId = json['employeeId'];
    id = json['id'];
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['comment'] = this.comment;
    data['createdOn'] = this.createdOn;
    data['emoployeeName'] = this.emoployeeName;
    data['employeeId'] = this.employeeId;
    data['id'] = this.id;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    return data;
  }
}

class TaskData {
  String? addedBy;
  List<AssignedTo>? assignedTo;
  String? createdBy;
  String? createdOn;
  String? description;
  String? endDate;
  String? id;
  String? moduleId;
  String? name;
  String? notes;
  List<AssignedTo>? notifyTo;
  List<AssignedTo>? assignedToTester;
  String? projectId;
  String? startDate;
  int? involvedTime;
  int? status;

  TaskData(
      {this.addedBy,
        this.assignedTo,
        this.createdBy,
        this.createdOn,
        this.description,
        this.endDate,
        this.id,
        this.moduleId,
        this.name,
        this.notes,
        this.notifyTo,
        this.assignedToTester,
        this.projectId,
        this.startDate,
        this.status,
      this.involvedTime});

  TaskData.fromJson(Map<String, dynamic> json) {
    addedBy = json['addedBy'];
    if (json['assignedTo'] != null) {
      assignedTo = <AssignedTo>[];
      json['assignedTo'].forEach((v) {
        assignedTo!.add(new AssignedTo.fromJson(v));
      });
    }
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    description = json['description'];
    involvedTime = json['involvedTime']??0;
    endDate = json['endDate']??"";
    id = json['id'];
    moduleId = json['moduleId'];
    name = json['name'];
    notes = json['notes'] ?? "";
    if (json['notifyTo'] != null) {
      notifyTo = <AssignedTo>[];
      json['notifyTo'].forEach((v) {
        notifyTo!.add(new AssignedTo.fromJson(v));
      });
    }
    if (json['assignedToTester'] != null) {
      assignedToTester = <AssignedTo>[];
      json['assignedToTester'].forEach((v) {
        assignedToTester!.add(new AssignedTo.fromJson(v));
      });
    }
    projectId = json['projectId'];
    startDate = json['startDate']??"";
    status = json['status'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addedBy'] = this.addedBy;
    if (this.assignedTo != null) {
      data['assignedTo'] = this.assignedTo!.map((v) => v.toJson()).toList();
    }
    data['createdBy'] = this.createdBy;
    data['involvedTime'] = this.involvedTime;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['endDate'] = this.endDate;
    data['id'] = this.id;
    data['moduleId'] = this.moduleId;
    data['name'] = this.name;
    data['notes'] = this.notes;
    if (this.notifyTo != null) {
      data['notifyTo'] = this.notifyTo!.map((v) => v.toJson()).toList();
    }
    if (this.assignedToTester != null) {
      data['assignedToTester'] = this.assignedToTester!.map((v) => v.toJson()).toList();
    }
    data['projectId'] = this.projectId;
    data['startDate'] = this.startDate;
    data['status'] = this.status;
    return data;
  }
}

class AssignedTo {
  String? id;
  String? name;

  AssignedTo({this.id, this.name});

  AssignedTo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class TaskHistoryList {
  String? day;
  String? dayForSorting;
  List<LogsData>? logsData;

  TaskHistoryList({this.day, this.dayForSorting, this.logsData});

  TaskHistoryList.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    dayForSorting = json['day_for_sorting'];
    if (json['logs_data'] != null) {
      logsData = <LogsData>[];
      json['logs_data'].forEach((v) {
        logsData!.add(new LogsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['day_for_sorting'] = this.dayForSorting;
    if (this.logsData != null) {
      data['logs_data'] = this.logsData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogsData {
  String? createdOn;
  String? description;
  String? employeeId;
  String? employeeName;
  String? logType;
  String? moduleCreatedBy;
  String? moduleCreatedByName;
  String? moduleEndDate;
  String? moduleId;
  String? moduleName;
  String? moduleStartDate;
  String? taskId;
  String? taskName;

  LogsData(
      {this.createdOn,
        this.description,
        this.employeeId,
        this.employeeName,
        this.logType,
        this.moduleCreatedBy,
        this.moduleCreatedByName,
        this.moduleEndDate,
        this.moduleId,
        this.moduleName,
        this.moduleStartDate,
        this.taskId,
        this.taskName});

  LogsData.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'];
    description = json['description'];
    employeeId = json['employeeId'];
    employeeName = json['employeeName'];
    logType = json['logType'];
    moduleCreatedBy = json['moduleCreatedBy'];
    moduleCreatedByName = json['moduleCreatedByName'];
    moduleEndDate = json['moduleEndDate']??"";
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    moduleStartDate = json['moduleStartDate'];
    taskId = json['taskId'];
    taskName = json['taskName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['logType'] = this.logType;
    data['moduleCreatedBy'] = this.moduleCreatedBy;
    data['moduleCreatedByName'] = this.moduleCreatedByName;
    data['moduleEndDate'] = this.moduleEndDate;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['moduleStartDate'] = this.moduleStartDate;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    return data;
  }
}

class TaskTestCasesList {
  String? createdOn;
  String? description;
  String? emoployeeName;
  String? employeeId;
  String? expectedResults;
  String? actualResults;
  String? id;
  String? moduleId;
  String? moduleName;
  String? projectId;
  String? projectName;
  String? steps;
  String? taskId;
  String? taskName;
  String? tcStatus;
  String? title;

  TaskTestCasesList(
      {this.createdOn,
        this.description,
        this.emoployeeName,
        this.employeeId,
        this.expectedResults,
        this.actualResults,
        this.id,
        this.moduleId,
        this.moduleName,
        this.projectId,
        this.projectName,
        this.steps,
        this.taskId,
        this.taskName,
        this.tcStatus,
        this.title});

  TaskTestCasesList.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'];
    description = json['description'];
    emoployeeName = json['emoployeeName'];
    employeeId = json['employeeId'];
    expectedResults = json['expectedResults'];
    actualResults = json['actualResults'] ?? "";
    id = json['id'];
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    steps = json['steps'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    tcStatus = json['tcStatus'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['emoployeeName'] = this.emoployeeName;
    data['employeeId'] = this.employeeId;
    data['expectedResults'] = this.expectedResults;
    data['actualResults'] = this.actualResults;
    data['id'] = this.id;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['steps'] = this.steps;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['tcStatus'] = this.tcStatus;
    data['title'] = this.title;
    return data;
  }
}
