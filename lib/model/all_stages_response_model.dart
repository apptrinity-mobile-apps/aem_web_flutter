class AllStagesResponseModel {
  List<DevelopmentStagesList>? developmentStagesList;
  int? responseStatus;
  String? result;

  AllStagesResponseModel(
      {this.developmentStagesList, this.responseStatus, this.result});

  AllStagesResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['developmentStagesList'] != null) {
      developmentStagesList = <DevelopmentStagesList>[];
      json['developmentStagesList'].forEach((v) {
        developmentStagesList!.add(new DevelopmentStagesList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.developmentStagesList != null) {
      data['developmentStagesList'] =
          this.developmentStagesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DevelopmentStagesList {
  String? createdBy;
  String? createdOn;
  String? id;
  String? stage;
  int? status;

  DevelopmentStagesList(
      {this.createdBy, this.createdOn, this.id, this.stage, this.status});

  DevelopmentStagesList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    id = json['id'];
    stage = json['stage'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['stage'] = this.stage;
    data['status'] = this.status;
    return data;
  }
}