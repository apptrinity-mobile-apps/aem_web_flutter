class ScrumTeamListResponseModel {
  List<EmployeesList>? employeesList;
  int? responseStatus;
  String? result;

  ScrumTeamListResponseModel({this.employeesList, this.responseStatus, this.result});

  ScrumTeamListResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['employeesList'] != null) {
      employeesList = <EmployeesList>[];
      json['employeesList'].forEach((v) {
        employeesList!.add(new EmployeesList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.employeesList != null) {
      data['employeesList'] =
          this.employeesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class EmployeesList {
  String? employeeId;
  String? employeeName;
  int? employeesCount;

  EmployeesList({this.employeeId, this.employeeName, this.employeesCount});

  EmployeesList.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'];
    employeeName = json['employeeName'];
    employeesCount = json['employeesCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['employeesCount'] = this.employeesCount;
    return data;
  }
}