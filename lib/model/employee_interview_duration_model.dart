class EmployeeInterviewDurationModel {
  String? displayTitle;
  int? value;

  EmployeeInterviewDurationModel({this.displayTitle, this.value});

  EmployeeInterviewDurationModel.fromJson(Map<String, dynamic> json) {
    displayTitle = json['displayTitle'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['displayTitle'] = displayTitle;
    data['value'] = value;
    return data;
  }
}