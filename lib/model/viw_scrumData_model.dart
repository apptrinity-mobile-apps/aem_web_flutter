class ViewScrumDataModel {
  List<ProjectsData>? projectsData;
  int? responseStatus;
  String? result;
  ScrumData? scrumData;

  ViewScrumDataModel(
      {this.projectsData, this.responseStatus, this.result, this.scrumData});

  ViewScrumDataModel.fromJson(Map<String, dynamic> json) {
    if (json['projectsData'] != null) {
      projectsData = <ProjectsData>[];
      json['projectsData'].forEach((v) {
        projectsData!.add(new ProjectsData.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
    scrumData = json['scrumData'] != null
        ? new ScrumData.fromJson(json['scrumData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.projectsData != null) {
      data['projectsData'] = this.projectsData!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.scrumData != null) {
      data['scrumData'] = this.scrumData!.toJson();
    }
    return data;
  }
}

class ProjectsData {
  List<BugsList>? bugsList;
  String? id;
  String? name;
  List<OtherTasksList>? otherTasksList;
  List<SprintsList>? sprintsList;

  ProjectsData(
      {this.bugsList,
        this.id,
        this.name,
        this.otherTasksList,
        this.sprintsList});

  ProjectsData.fromJson(Map<String, dynamic> json) {
    if (json['bugsList'] != null) {
      bugsList = <BugsList>[];
      json['bugsList'].forEach((v) {
        bugsList!.add(new BugsList.fromJson(v));
      });
    }
    id = json['id'];
    name = json['name'];
    if (json['otherTasksList'] != null) {
      otherTasksList = <OtherTasksList>[];
      json['otherTasksList'].forEach((v) {
        otherTasksList!.add(new OtherTasksList.fromJson(v));
      });
    }
    if (json['sprintsList'] != null) {
      sprintsList = <SprintsList>[];
      json['sprintsList'].forEach((v) {
        sprintsList!.add(new SprintsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bugsList != null) {
      data['bugsList'] = this.bugsList!.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.otherTasksList != null) {
      data['otherTasksList'] =
          this.otherTasksList!.map((v) => v.toJson()).toList();
    }
    if (this.sprintsList != null) {
      data['sprintsList'] = this.sprintsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BugsList {
  String? bugId;
  String? bugStatus;
  String? bugTitle;
  String? employeeId;
  String? employeeName;
  String? moduleId;
  String? moduleName;
  String? taskId;
  String? taskName;
  int? taskStatus;

  BugsList(
      {this.bugId,
        this.bugStatus,
        this.bugTitle,
        this.employeeId,
        this.employeeName,
        this.moduleId,
        this.moduleName,
        this.taskId,
        this.taskName,
        this.taskStatus});

  BugsList.fromJson(Map<String, dynamic> json) {
    bugId = json['bugId'];
    bugStatus = json['bugStatus'];
    bugTitle = json['bugTitle'];
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";;
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskStatus = json['taskStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bugId'] = this.bugId;
    data['bugStatus'] = this.bugStatus;
    data['bugTitle'] = this.bugTitle;
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['taskStatus'] = this.taskStatus;
    return data;
  }
}

class OtherTasksList {
  String? employeeId;
  String? employeeName;
  String? moduleId;
  String? moduleName;
  String? taskId;
  String? taskName;
  int? taskStatus;

  OtherTasksList(
      {this.employeeId,
        this.employeeName,
        this.moduleId,
        this.moduleName,
        this.taskId,
        this.taskName,
        this.taskStatus});

  OtherTasksList.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";;
    employeeName = json['employeeName'] ?? "";;
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskStatus = json['taskStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['taskStatus'] = this.taskStatus;
    return data;
  }
}

class SprintsList {
  String? createdBy;
  String? createdByName;
  String? createdOn;
  String? description;
  String? endDate;
  String? id;
  String? name;
  String? projectId;
  String? projectName;
  String? startDate;
  int? status;
  List<TasksData>? tasksData;

  SprintsList(
      {this.createdBy,
        this.createdByName,
        this.createdOn,
        this.description,
        this.endDate,
        this.id,
        this.name,
        this.projectId,
        this.projectName,
        this.startDate,
        this.status,
        this.tasksData});

  SprintsList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdByName = json['createdByName'];
    createdOn = json['createdOn'];
    description = json['description'];
    endDate = json['endDate'];
    id = json['id'];
    name = json['name'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    startDate = json['startDate'];
    status = json['status'];
    if (json['tasksData'] != null) {
      tasksData = <TasksData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(new TasksData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdByName'] = this.createdByName;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['endDate'] = this.endDate;
    data['id'] = this.id;
    data['name'] = this.name;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['startDate'] = this.startDate;
    data['status'] = this.status;
    if (this.tasksData != null) {
      data['tasksData'] = this.tasksData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScrumData {
  String? createdOn;
  String? description;
  List<ProjectsList>? projectsList;
  String? scrumDate;

  ScrumData(
      {this.createdOn, this.description, this.projectsList, this.scrumDate});

  ScrumData.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'];
    description = json['description'];
    if (json['projectsList'] != null) {
      projectsList = <ProjectsList>[];
      json['projectsList'].forEach((v) {
        projectsList!.add(new ProjectsList.fromJson(v));
      });
    }
    scrumDate = json['scrumDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    if (this.projectsList != null) {
      data['projectsList'] = this.projectsList!.map((v) => v.toJson()).toList();
    }
    data['scrumDate'] = this.scrumDate;
    return data;
  }
}

class ProjectsList {
  String? id;
  String? name;

  ProjectsList({this.id, this.name});

  ProjectsList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class TasksData {
  String? employeeId;
  String? employeeName;
  String? moduleId;
  String? moduleName;
  String? taskId;
  String? taskName;
  int? taskStatus;
  TasksData(
      {this.employeeId, this.employeeName, this.moduleId, this.moduleName, this.taskId, this.taskName, this.taskStatus});

  TasksData.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskStatus = json['taskStatus'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['taskStatus'] = this.taskStatus;

    return data;
  }
}