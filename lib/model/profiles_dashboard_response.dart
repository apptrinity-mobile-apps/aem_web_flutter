class ProfilesDashboardResponse {
  CountsData? countsData;
  List<InterviewsList>? interviewsList;
  int? responseStatus;
  String? result;

  ProfilesDashboardResponse({this.countsData, this.interviewsList, this.responseStatus, this.result});

  ProfilesDashboardResponse.fromJson(Map<String, dynamic> json) {
    countsData = json['countsData'] != null ? CountsData.fromJson(json['countsData']) : null;
    if (json['interviewsList'] != null) {
      interviewsList = <InterviewsList>[];
      json['interviewsList'].forEach((v) {
        interviewsList!.add(InterviewsList.fromJson(v));
      });
    } else {
      interviewsList = <InterviewsList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (countsData != null) {
      data['countsData'] = countsData!.toJson();
    }
    if (interviewsList != null) {
      data['interviewsList'] = interviewsList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class CountsData {
  int? profilesCount;
  int? todayInterviewsCount;
  int? upcomingInterviewsCount;

  CountsData({this.profilesCount, this.todayInterviewsCount, this.upcomingInterviewsCount});

  CountsData.fromJson(Map<String, dynamic> json) {
    profilesCount = json['profiles_count'] ?? 0;
    todayInterviewsCount = json['today_interviews_count'] ?? 0;
    upcomingInterviewsCount = json['upcoming_interviews_count'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['profiles_count'] = profilesCount;
    data['today_interviews_count'] = todayInterviewsCount;
    data['upcoming_interviews_count'] = upcomingInterviewsCount;
    return data;
  }
}

class InterviewsList {
  String? email;
  String? interviewTime;
  String? interviewerName;
  String? mainTechnology;
  String? name;
  String? round;
  String? id;
  String? profileId;

  InterviewsList({this.email, this.interviewTime, this.interviewerName, this.mainTechnology, this.name, this.round, this.id, this.profileId});

  InterviewsList.fromJson(Map<String, dynamic> json) {
    email = json['email'] ?? "";
    interviewTime = json['interviewTime'] ?? "";
    interviewerName = json['interviewerName'] ?? "";
    mainTechnology = json['mainTechnology'] ?? "";
    name = json['name'] ?? "";
    round = json['round'] ?? "";
    id = json['id'] ?? "";
    profileId = json['profileId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['interviewTime'] = interviewTime;
    data['interviewerName'] = interviewerName;
    data['mainTechnology'] = mainTechnology;
    data['name'] = name;
    data['round'] = round;
    data['id'] = id;
    data['profileId'] = profileId;
    return data;
  }
}
