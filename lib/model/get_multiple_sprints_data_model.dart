class MultipleSprintsDataModel {
  int? responseStatus;
  String? result;
  List<SprintsList>? sprintsList;

  MultipleSprintsDataModel(
      {this.responseStatus, this.result, this.sprintsList});

  MultipleSprintsDataModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['sprintsList'] != null) {
      sprintsList = <SprintsList>[];
      json['sprintsList'].forEach((v) {
        sprintsList!.add(new SprintsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.sprintsList != null) {
      data['sprintsList'] = this.sprintsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SprintsList {
  List<TasksData>? tasksData;
  String? createdBy;
  String? createdByName;
  String? createdOn;
  String? description;
  String? endDate;
  String? id;
  String? name;
  String? projectId;
  String? projectName;
  String? startDate;
  int? status;

  SprintsList(
      {this.tasksData,
        this.createdBy,
        this.createdByName,
        this.createdOn,
        this.description,
        this.endDate,
        this.id,
        this.name,
        this.projectId,
        this.projectName,
        this.startDate,
        this.status});

  SprintsList.fromJson(Map<String, dynamic> json) {
    if (json['tasksData'] != null) {
      tasksData = <TasksData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(new TasksData.fromJson(v));
      });
    }
    createdBy = json['createdBy'];
    createdByName = json['createdByName'];
    createdOn = json['createdOn'];
    description = json['description'];
    endDate = json['endDate'];
    id = json['id'];
    name = json['name'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    startDate = json['startDate'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tasksData != null) {
      data['tasksData'] = this.tasksData!.map((v) => v.toJson()).toList();
    }
    data['createdBy'] = this.createdBy;
    data['createdByName'] = this.createdByName;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['endDate'] = this.endDate;
    data['id'] = this.id;
    data['name'] = this.name;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['startDate'] = this.startDate;
    data['status'] = this.status;
    return data;
  }
}

class TasksData {
  String? moduleId;
  String? moduleName;
  String? taskId;
  String? taskName;
  int? taskStatus;

  TasksData(
      {this.moduleId,
        this.moduleName,
        this.taskId,
        this.taskName,
        this.taskStatus});

  TasksData.fromJson(Map<String, dynamic> json) {
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskStatus = json['taskStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['taskStatus'] = this.taskStatus;
    return data;
  }
}