class EmployeeAssociatedChatsResponse {
  List<AssociatedEmployeeDetails>? employeeDetails;
  int? responseStatus;
  String? result;

  EmployeeAssociatedChatsResponse({this.employeeDetails, this.responseStatus, this.result});

  EmployeeAssociatedChatsResponse.fromJson(Map<String, dynamic> json) {
    if (json['employeeDetails'] != null) {
      employeeDetails = <AssociatedEmployeeDetails>[];
      json['employeeDetails'].forEach((v) {
        employeeDetails!.add(AssociatedEmployeeDetails.fromJson(v));
      });
    } else {
      employeeDetails = <AssociatedEmployeeDetails>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (employeeDetails != null) {
      data['employeeDetails'] = employeeDetails!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class AssociatedEmployeeDetails {
  String? chatId;
  String? createdOn;
  String? email;
  String? id;
  bool? isSuperiorEmployee;
  String? name;
  String? phoneNumber;
  int? status;

  AssociatedEmployeeDetails({this.chatId, this.createdOn, this.email, this.id, this.isSuperiorEmployee, this.name, this.phoneNumber, this.status});

  AssociatedEmployeeDetails.fromJson(Map<String, dynamic> json) {
    chatId = json['chatId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    isSuperiorEmployee = json['isSuperiorEmployee'] ?? false;
    name = json['name'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['chatId'] = chatId;
    data['createdOn'] = createdOn;
    data['email'] = email;
    data['id'] = id;
    data['isSuperiorEmployee'] = isSuperiorEmployee;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['status'] = status;
    return data;
  }
}
