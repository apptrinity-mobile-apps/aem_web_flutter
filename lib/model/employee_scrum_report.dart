class EmployeeScrumReportResponse {
  int? responseStatus;
  String? result;
  List<TasksCounts>? tasksCount;

  EmployeeScrumReportResponse({this.responseStatus, this.result, this.tasksCount});

  EmployeeScrumReportResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['tasksCount'] != null) {
      tasksCount = <TasksCounts>[];
      json['tasksCount'].forEach((v) {
        tasksCount!.add(TasksCounts.fromJson(v));
      });
    } else {
      tasksCount = <TasksCounts>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (tasksCount != null) {
      data['tasksCount'] = tasksCount!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksCounts {
  int? completedTasksCount;
  String? employeeId;
  String? employeeName;
  int? newTasksCount;
  int? pendingTasksCount;
  int? totalTasksCount;

  TasksCounts({this.completedTasksCount, this.employeeId, this.employeeName, this.newTasksCount, this.pendingTasksCount, this.totalTasksCount});

  TasksCounts.fromJson(Map<String, dynamic> json) {
    completedTasksCount = json['completed_tasks_count'] ?? 0;
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    newTasksCount = json['new_tasks_count'] ?? 0;
    pendingTasksCount = json['pending_tasks_count'] ?? 0;
    totalTasksCount = json['total_tasks_count'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['completed_tasks_count'] = completedTasksCount;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['new_tasks_count'] = newTasksCount;
    data['pending_tasks_count'] = pendingTasksCount;
    data['total_tasks_count'] = totalTasksCount;
    return data;
  }
}
