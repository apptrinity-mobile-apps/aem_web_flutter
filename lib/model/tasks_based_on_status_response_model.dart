class TaskListStatusBasedResponseModel {
  List<ModulesData>? modulesData;
  int? responseStatus;
  String? result;

  TaskListStatusBasedResponseModel({this.modulesData, this.responseStatus, this.result});

  TaskListStatusBasedResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['modulesData'] != null) {
      modulesData = <ModulesData>[];
      json['modulesData'].forEach((v) {
        modulesData!.add(ModulesData.fromJson(v));
      });
    } else {
      modulesData = <ModulesData>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (modulesData != null) {
      data['modulesData'] = modulesData!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ModulesData {
  List<String>? assignedTo;
  int? completedTasksCount;
  String? createdBy;
  String? createdOn;
  String? description;
  String? endDate;
  String? moduleId;
  String? moduleName;
  String? notes;
  List<String>? notifyTo;
  String? projectId;
  int? status;
  List<TasksData>? tasksData;
  int? totalTasksCount;

  ModulesData(
      {this.assignedTo,
      this.completedTasksCount,
      this.createdBy,
      this.createdOn,
      this.description,
      this.endDate,
      this.moduleId,
      this.moduleName,
      this.notes,
      this.notifyTo,
      this.projectId,
      this.status,
      this.tasksData,
      this.totalTasksCount});

  ModulesData.fromJson(Map<String, dynamic> json) {
    assignedTo = json['assignedTo'].cast<String>() ?? [];
    completedTasksCount = json['completedTasksCount'] ?? 0;
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    endDate = json['endDate'] ?? "";
    moduleId = json['moduleId'] ?? "";
    moduleName = json['moduleName'] ?? "";
    notes = json['notes'] ?? "";
    notifyTo = json['notifyTo'].cast<String>() ?? [];
    projectId = json['projectId'] ?? "";
    status = json['status'] ?? 0;
    if (json['tasksData'] != null) {
      tasksData = <TasksData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(TasksData.fromJson(v));
      });
    } else {
      tasksData = <TasksData>[];
    }
    totalTasksCount = json['totalTasksCount'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['assignedTo'] = assignedTo;
    data['completedTasksCount'] = completedTasksCount;
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['endDate'] = endDate;
    data['moduleId'] = moduleId;
    data['moduleName'] = moduleName;
    data['notes'] = notes;
    data['notifyTo'] = notifyTo;
    data['projectId'] = projectId;
    data['status'] = status;
    if (tasksData != null) {
      data['tasksData'] = tasksData!.map((v) => v.toJson()).toList();
    }
    data['totalTasksCount'] = totalTasksCount;
    return data;
  }
}

class TasksData {
  String? addedBy;
  List<AssignedTo>? assignedTo;
  String? createdBy;
  String? createdOn;
  String? description;
  String? endDate;
  String? id;
  String? moduleId;
  String? name;
  String? notes;
  int? estimationTime;
  int? involvedTime;
  List<AssignedTo>? notifyTo;
  String? projectId;
  String? startDate;
  int? status;

  TasksData(
      {this.addedBy,
      this.assignedTo,
      this.createdBy,
      this.createdOn,
      this.description,
      this.endDate,
      this.id,
      this.moduleId,
      this.name,
      this.notes,
      this.notifyTo,
        this.estimationTime,
        this.involvedTime,
      this.projectId,
      this.startDate,
      this.status});

  TasksData.fromJson(Map<String, dynamic> json) {
    addedBy = json['addedBy'] ?? "";
    if (json['assignedTo'] != null) {
      assignedTo = <AssignedTo>[];
      json['assignedTo'].forEach((v) {
        assignedTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      assignedTo = <AssignedTo>[];
    }
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    endDate = json['endDate'] ?? "";
    id = json['id'] ?? "";
    moduleId = json['moduleId'] ?? "";
    name = json['name'] ?? "";
    estimationTime = json['estimationTime'] ?? 0;
    involvedTime = json['involvedTime'] ?? 0;
    notes = json['notes'] ?? "";
    if (json['notifyTo'] != null) {
      notifyTo = <AssignedTo>[];
      json['notifyTo'].forEach((v) {
        notifyTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      notifyTo = <AssignedTo>[];
    }
    projectId = json['projectId'] ?? "";
    startDate = json['startDate'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['addedBy'] = addedBy;
    if (assignedTo != null) {
      data['assignedTo'] = assignedTo!.map((v) => v.toJson()).toList();
    }
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['endDate'] = endDate;
    data['id'] = id;
    data['moduleId'] = moduleId;
    data['name'] = name;
    data['estimationTime'] = estimationTime;
    data['involvedTime'] = involvedTime;
    data['notes'] = notes;
    if (notifyTo != null) {
      data['notifyTo'] = notifyTo!.map((v) => v.toJson()).toList();
    }
    data['projectId'] = projectId;
    data['startDate'] = startDate;
    data['status'] = status;
    return data;
  }
}

class AssignedTo {
  String? employeeId;
  String? name;

  AssignedTo({this.employeeId, this.name});

  AssignedTo.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";
    name = json['name'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['employeeId'] = employeeId;
    data['name'] = name;
    return data;
  }
}
