class TaskDurationTrackResponse {
  List<TaskDurationData>? bugsData;
  int? responseStatus;
  String? result;
  List<TaskDurationData>? tasksData;

  TaskDurationTrackResponse({this.bugsData, this.responseStatus, this.result, this.tasksData});

  TaskDurationTrackResponse.fromJson(Map<String, dynamic> json) {
    if (json['bugsData'] != null) {
      bugsData = <TaskDurationData>[];
      json['bugsData'].forEach((v) {
        bugsData!.add(TaskDurationData.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['tasksData'] != null) {
      tasksData = <TaskDurationData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(TaskDurationData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (bugsData != null) {
      data['bugsData'] = bugsData!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (tasksData != null) {
      data['tasksData'] = tasksData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TaskDurationData {
  String? bugId;
  String? bugName;
  String? createdOn;
  int? duration;
  String? employeeId;
  String? employeeName;
  String? endTime;
  String? id;
  String? startTime;
  String? statusText;
  String? taskId;
  String? taskName;
  String? taskType;
  bool? isInEditMode;

  TaskDurationData(
      {this.bugId,
      this.bugName,
      this.createdOn,
      this.duration,
      this.employeeId,
      this.employeeName,
      this.endTime,
      this.id,
      this.startTime,
      this.statusText,
      this.taskId,
      this.taskName,
      this.taskType,
      this.isInEditMode});

  TaskDurationData.fromJson(Map<String, dynamic> json) {
    bugId = json['bugId'] ?? "";
    bugName = json['bugName'] ?? "";
    createdOn = json['createdOn'] ?? "";
    duration = json['duration'] ?? 0;
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    endTime = json['endTime'] ?? "";
    id = json['id'] ?? "";
    startTime = json['startTime'] ?? "";
    statusText = json['statusText'] ?? "";
    taskId = json['taskId'] ?? "";
    taskName = json['taskName'] ?? "";
    taskType = json['taskType'] ?? "";
    isInEditMode = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bugId'] = bugId;
    data['bugName'] = bugName;
    data['createdOn'] = createdOn;
    data['duration'] = duration;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['endTime'] = endTime;
    data['id'] = id;
    data['startTime'] = startTime;
    data['statusText'] = statusText;
    data['taskId'] = taskId;
    data['taskName'] = taskName;
    data['taskType'] = taskType;
    return data;
  }
}
