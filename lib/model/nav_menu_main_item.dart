import 'package:aem/model/nav_menu_item.dart';

class MainNavMenu {
  final String? title;
  final List<NavMenuItem>? subMenus;

  const MainNavMenu({
    this.title,
    this.subMenus,
  });
}
