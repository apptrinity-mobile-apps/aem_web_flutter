class BaseResponse {
  int? responseStatus;
  String? result;
  String? roleId;
  String? moduleId;
  String? taskCommentId;

  BaseResponse({this.responseStatus, this.result, this.roleId, this.moduleId, this.taskCommentId});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'] ?? 0;
    result = json['result'] ?? "";
    roleId = json['roleId'] ?? "";
    roleId = json['moduleId'] ?? "";
    taskCommentId = json['taskCommentId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    data['roleId'] = roleId;
    data['moduleId'] = moduleId;
    data['taskCommentId'] = taskCommentId;
    return data;
  }
}
class TaskTimingsResponse {
  int? responseStatus;
  String? result;
  TaskTimings? taskTimings;

  TaskTimingsResponse({this.responseStatus, this.result, this.taskTimings});

  TaskTimingsResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    taskTimings = json['taskTimings'] != null
        ? new TaskTimings.fromJson(json['taskTimings'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.taskTimings != null) {
      data['taskTimings'] = this.taskTimings!.toJson();
    }
    return data;
  }
}

class TaskTimings {
  int? estimationTime;
  int? involvedTime;

  TaskTimings({this.estimationTime, this.involvedTime});

  TaskTimings.fromJson(Map<String, dynamic> json) {
    estimationTime = json['estimationTime'];
    involvedTime = json['involvedTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['estimationTime'] = this.estimationTime;
    data['involvedTime'] = this.involvedTime;
    return data;
  }
}