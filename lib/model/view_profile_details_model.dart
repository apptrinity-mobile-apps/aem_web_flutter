class ViewProfileDetailsModel {
  ProfilesDetails? profilesDetails;
  int? responseStatus;
  String? result;

  ViewProfileDetailsModel({this.profilesDetails, this.responseStatus, this.result});

  ViewProfileDetailsModel.fromJson(Map<String, dynamic> json) {
    profilesDetails = json['profilesDetails'] != null ? ProfilesDetails.fromJson(json['profilesDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (profilesDetails != null) {
      data['profilesDetails'] = profilesDetails!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ProfilesDetails {
  String? careerStartedOn;
  String? createdBy;
  String? createdByName;
  String? createdOn;
  String? email;
  String? employmentStatusId;
  String? jobRoleId;
  String? employmentStatusName;
  bool? employmentStatusSystemTest;
  bool? employmentStatusEmail;
  String? experienceStatus;
  String? mainTechnology;
  String? mainTechnologyName;
  String? id;
  List<InterviewSchedulesList>? interviewSchedulesList;
  String? name;
  String? source;
  String? phoneNumber;
  List<ProfileComment>? profileCommentsList;
  String? resume;
  String? fileExtension;
  String? resumeFileName;
  int? status;
  List<SuperiorEmployees>? superiorEmployees;
  List<Technology>? technologyList;

  ProfilesDetails(
      {this.careerStartedOn,
      this.createdBy,
      this.createdByName,
      this.createdOn,
      this.email,
      this.employmentStatusId,
        this.jobRoleId,
      this.employmentStatusName,
      this.employmentStatusSystemTest,
      this.employmentStatusEmail,
      this.experienceStatus,
      this.mainTechnology,
      this.mainTechnologyName,
      this.id,
      this.interviewSchedulesList,
      this.name,
      this.source,
      this.resumeFileName,
      this.phoneNumber,
      this.profileCommentsList,
      this.resume,
      this.fileExtension,
      this.status,
      this.superiorEmployees,
      this.technologyList});

  ProfilesDetails.fromJson(Map<String, dynamic> json) {
    careerStartedOn = json['careerStartedOn'] ?? "";
    createdBy = json['createdBy'] ?? "";
    createdByName = json['createdByName'] ?? "";
    createdOn = json['createdOn'] ?? "";
    email = json['email'] ?? "";
    jobRoleId = json['jobRoleId'] ?? "";
    source = json['source'] ?? "";
    employmentStatusId = json['employmentStatusId'] ?? "";
    employmentStatusName = json['employmentStatusName'] ?? "";
    employmentStatusSystemTest = json['employmentStatusSystemTest'] ?? false;
    employmentStatusEmail = json['employmentStatusEmail'] ?? false;
    experienceStatus = json['experienceStatus'] ?? "";
    mainTechnology = json['mainTechnology'] ?? "";
    mainTechnologyName = json['mainTechnologyName'] ?? "";
    id = json['id'] ?? "";
    if (json['interviewSchedulesList'] != null) {
      interviewSchedulesList = <InterviewSchedulesList>[];
      json['interviewSchedulesList'].forEach((v) {
        interviewSchedulesList!.add(InterviewSchedulesList.fromJson(v));
      });
    } else {
      interviewSchedulesList = <InterviewSchedulesList>[];
    }
    name = json['name'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    if (json['profileCommentsList'] != null) {
      profileCommentsList = <ProfileComment>[];
      json['profileCommentsList'].forEach((v) {
        profileCommentsList!.add(ProfileComment.fromJson(v));
      });
    } else {
      profileCommentsList = <ProfileComment>[];
    }
    resume = json['resume'] ?? "";
    fileExtension = json['fileExtension'] ?? "";
    resumeFileName = json['resumeFileName'] ?? "";
    status = json['status'] ?? 0;
    if (json['superiorEmployees'] != null) {
      superiorEmployees = <SuperiorEmployees>[];
      json['superiorEmployees'].forEach((v) {
        superiorEmployees!.add(SuperiorEmployees.fromJson(v));
      });
    } else {
      superiorEmployees = <SuperiorEmployees>[];
    }
    if (json['technologyList'] != null) {
      technologyList = <Technology>[];
      json['technologyList'].forEach((v) {
        technologyList!.add(Technology.fromJson(v));
      });
    } else {
      technologyList = <Technology>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['careerStartedOn'] = careerStartedOn;
    data['createdBy'] = createdBy;
    data['createdByName'] = createdByName;
    data['jobRoleId'] = jobRoleId;
    data['createdOn'] = createdOn;
    data['email'] = email;
    data['source'] = source;
    data['employmentStatusId'] = employmentStatusId;
    data['employmentStatusName'] = employmentStatusName;
    data['employmentStatusSystemTest'] = employmentStatusSystemTest;
    data['employmentStatusEmail'] = employmentStatusEmail;
    data['experienceStatus'] = experienceStatus;
    data['mainTechnology'] = mainTechnology;
    data['mainTechnologyName'] = mainTechnologyName;
    data['id'] = id;
    if (interviewSchedulesList != null) {
      data['interviewSchedulesList'] = interviewSchedulesList!.map((v) => v.toJson()).toList();
    }
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    if (profileCommentsList != null) {
      data['profileCommentsList'] = profileCommentsList!.map((v) => v.toJson()).toList();
    }
    data['resume'] = resume;
    data['fileExtension'] = fileExtension;
    data['resumeFileName'] = resumeFileName;
    data['status'] = status;
    if (superiorEmployees != null) {
      data['superiorEmployees'] = superiorEmployees!.map((v) => v.toJson()).toList();
    }
    if (technologyList != null) {
      data['technologyList'] = technologyList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InterviewSchedulesList {
  String? employeeId;
  String? employeeName;
  String? employmentStatusId;
  String? employmentStatusName;
  String? id;
  String? interviewStatusId;
  String? interviewStatusName;
  String? interviewTime;
  String? interviewTypeId;
  String? interviewTypename;
  String? profileId;
  String? profileName;
  String? scheduledBy;
  String? scheduledByName;

  InterviewSchedulesList(
      {this.employeeId,
      this.employeeName,
      this.employmentStatusId,
      this.employmentStatusName,
      this.id,
      this.interviewStatusId,
      this.interviewStatusName,
      this.interviewTime,
      this.interviewTypeId,
      this.interviewTypename,
      this.profileId,
      this.profileName,
      this.scheduledBy,
      this.scheduledByName});

  InterviewSchedulesList.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    employmentStatusId = json['employmentStatusId'] ?? "";
    employmentStatusName = json['employmentStatusName'] ?? "";
    id = json['id'] ?? "";
    interviewStatusId = json['interviewStatusId'] ?? "";
    interviewStatusName = json['interviewStatusName'] ?? "";
    interviewTime = json['interviewTime'] ?? "";
    interviewTypeId = json['interviewTypeId'] ?? "";
    interviewTypename = json['interviewTypename'] ?? "";
    profileId = json['profileId'] ?? "";
    profileName = json['profileName'] ?? "";
    scheduledBy = json['scheduledBy'] ?? "";
    scheduledByName = json['scheduledByName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['employmentStatusId'] = employmentStatusId;
    data['employmentStatusName'] = employmentStatusName;
    data['id'] = id;
    data['interviewStatusId'] = interviewStatusId;
    data['interviewStatusName'] = interviewStatusName;
    data['interviewTime'] = interviewTime;
    data['interviewTypeId'] = interviewTypeId;
    data['interviewTypename'] = interviewTypename;
    data['profileId'] = profileId;
    data['profileName'] = profileName;
    data['scheduledBy'] = scheduledBy;
    data['scheduledByName'] = scheduledByName;
    return data;
  }
}

class ProfileComment {
  String? comment;
  String? createdOn;
  String? scheduledBy;
  String? scheduledByName;
  String? employeeId;
  String? employeeName;
  String? id;
  String? profileId;
  int? status;

  ProfileComment(
      {this.comment,
      this.createdOn,
      this.scheduledBy,
      this.scheduledByName,
      this.employeeId,
      this.employeeName,
      this.id,
      this.profileId,
      this.status});

  ProfileComment.fromJson(Map<String, dynamic> json) {
    comment = json['comment'] ?? "";
    createdOn = json['createdOn'] ?? "";
    scheduledBy = json['scheduledBy'] ?? "";
    scheduledByName = json['scheduledByName'] ?? "";
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    id = json['id'] ?? "";
    profileId = json['profileId'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['comment'] = comment;
    data['createdOn'] = createdOn;
    data['scheduledByName'] = scheduledByName;
    data['scheduledBy'] = scheduledBy;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['id'] = id;
    data['profileId'] = profileId;
    data['status'] = status;
    return data;
  }
}

class SuperiorEmployees {
  String? superiorEmployeeId;
  String? superiorEmployeeName;

  SuperiorEmployees({this.superiorEmployeeId, this.superiorEmployeeName});

  SuperiorEmployees.fromJson(Map<String, dynamic> json) {
    superiorEmployeeId = json['superiorEmployeeId'] ?? "";
    superiorEmployeeName = json['superiorEmployeeName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['superiorEmployeeId'] = superiorEmployeeId;
    data['superiorEmployeeName'] = superiorEmployeeName;
    return data;
  }
}

class Technology {
  String? name;
  String? technologyId;

  Technology({this.name, this.technologyId});

  Technology.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? "";
    technologyId = json['technologyId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['technologyId'] = technologyId;
    return data;
  }
}
