class CheckBoxState {
  String? displayTitle;
  String? displayValue;
  bool? value;

  CheckBoxState({this.displayTitle, this.displayValue, this.value});

  CheckBoxState.fromJson(Map<String, dynamic> json) {
    displayTitle = json['displayTitle'];
    displayValue = json['displayValue'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['displayTitle'] = displayTitle;
    data['displayValue'] = displayValue;
    data['value'] = value;
    return data;
  }
}
