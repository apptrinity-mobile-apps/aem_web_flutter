class AllDesignationsResponse {
  List<DesignationsList>? designationsList;
  int? responseStatus;
  String? result;
  DesignationsList? designation;

  AllDesignationsResponse({this.designationsList, this.responseStatus, this.result, this.designation});

  AllDesignationsResponse.fromJson(Map<String, dynamic> json) {
    if (json['designationsList'] != null) {
      designationsList = <DesignationsList>[];
      json['designationsList'].forEach((v) {
        designationsList!.add(DesignationsList.fromJson(v));
      });
    } else {
      designationsList = <DesignationsList>[];
    }
    designation = json['designation_dict'] != null ? DesignationsList.fromJson(json['designation_dict']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (designationsList != null) {
      data['designationsList'] = designationsList!.map((v) => v.toJson()).toList();
    }
    if (designation != null) {
      data['designation_dict'] = designation!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class DesignationsList {
  String? createdBy;
  String? id;
  String? createdOn;
  String? name;
  int? status;

  DesignationsList({this.createdBy, this.id, this.createdOn, this.name, this.status});

  DesignationsList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    id = json['id'] ?? "";
    createdOn = json['createdOn'] ?? "";
    name = json['name'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['Id'] = id;
    data['createdOn'] = createdOn;
    data['name'] = name;
    data['status'] = status;
    return data;
  }
}
