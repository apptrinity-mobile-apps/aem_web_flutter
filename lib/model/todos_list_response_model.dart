class TodosListResponseModel {
  List<ModuleDetails>? moduleDetails;
  int? responseStatus;
  String? result;
  ModuleDetails? moduleData;

  TodosListResponseModel({this.moduleDetails, this.responseStatus, this.result, this.moduleData});

  TodosListResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['moduleDetails'] != null) {
      moduleDetails = <ModuleDetails>[];
      json['moduleDetails'].forEach((v) {
        moduleDetails!.add(ModuleDetails.fromJson(v));
      });
    } else {
      moduleDetails = <ModuleDetails>[];
    }
    moduleData = json['moduleData'] != null ? ModuleDetails.fromJson(json['moduleData']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (moduleDetails != null) {
      data['moduleDetails'] = moduleDetails!.map((v) => v.toJson()).toList();
    }
    if (moduleData != null) {
      data['moduleData'] = moduleData!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ModuleDetails {
  List<AssignedTo>? assignedTo;
  int? completedTasksCount;
  String? createdBy;
  String? createdOn;
  String? description;
  String? endDate;
  String? startDate;
  String? id;
  List<ModuleComments>? moduleComments;
  String? name;
  String? notes;
  List<AssignedTo>? notifyTo;
  List<AssignedTo>? assignedToTester;
  String? projectId;
  int? status;
  int? taskCount;
  List<TasksList>? tasksList;

  ModuleDetails(
      {this.assignedTo,
      this.completedTasksCount,
      this.createdBy,
      this.createdOn,
      this.description,
      this.endDate,
      this.startDate,
      this.id,
      this.moduleComments,
      this.name,
      this.notes,
      this.notifyTo,
      this.assignedToTester,
      this.projectId,
      this.status,
      this.taskCount,
      this.tasksList});

  ModuleDetails.fromJson(Map<String, dynamic> json) {
    if (json['assignedTo'] != null) {
      assignedTo = <AssignedTo>[];
      json['assignedTo'].forEach((v) {
        assignedTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      assignedTo = <AssignedTo>[];
    }
    completedTasksCount = json['completedTasksCount'] ?? 0;
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    endDate = json['endDate'] ?? "";
    startDate = json['startDate'] ?? "";
    id = json['id'] ?? "";
    if (json['moduleComments'] != null) {
      moduleComments = <ModuleComments>[];
      json['moduleComments'].forEach((v) {
        moduleComments!.add(ModuleComments.fromJson(v));
      });
    } else {
      moduleComments = <ModuleComments>[];
    }
    name = json['name'] ?? "";
    notes = json['notes'] ?? "";
    if (json['notifyTo'] != null) {
      notifyTo = <AssignedTo>[];
      json['notifyTo'].forEach((v) {
        notifyTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      notifyTo = <AssignedTo>[];
    }
    if (json['assignedToTester'] != null) {
      assignedToTester = <AssignedTo>[];
      json['assignedToTester'].forEach((v) {
        assignedToTester!.add(AssignedTo.fromJson(v));
      });
    } else {
      assignedToTester = <AssignedTo>[];
    }
    projectId = json['projectId'] ?? "";
    status = json['status'] ?? 0;
    taskCount = json['task_count'] ?? 0;
    if (json['tasksList'] != null) {
      tasksList = <TasksList>[];
      json['tasksList'].forEach((v) {
        tasksList!.add(TasksList.fromJson(v));
      });
    } else {
      tasksList = <TasksList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (assignedTo != null) {
      data['assignedTo'] = assignedTo!.map((v) => v.toJson()).toList();
    }
    data['completedTasksCount'] = completedTasksCount;
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['endDate'] = endDate;
    data['startDate'] = startDate;
    data['id'] = id;
    if (moduleComments != null) {
      data['moduleComments'] = moduleComments!.map((v) => v.toJson()).toList();
    }
    data['name'] = name;
    data['notes'] = notes;
    if (notifyTo != null) {
      data['notifyTo'] = notifyTo!.map((v) => v.toJson()).toList();
    }
    data['projectId'] = projectId;
    data['status'] = status;
    data['task_count'] = taskCount;
    if (tasksList != null) {
      data['tasksList'] = tasksList!.map((v) => v.toJson()).toList();
    }
    if (assignedToTester != null) {
      data['assignedToTester'] = assignedToTester!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AssignedTo {
  String? employeeId;
  String? name;

  AssignedTo({this.employeeId, this.name});

  AssignedTo.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";
    name = json['name'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['employeeId'] = employeeId;
    data['name'] = name;
    return data;
  }
}

class ModuleComments {
  String? comment;
  String? createdOn;
  String? id;
  String? moduleId;
  String? moduleName;
  String? projectId;
  String? projectName;
  String? employeeId;
  String? employeeName;

  ModuleComments(
      {this.comment, this.createdOn, this.id, this.moduleId, this.moduleName, this.projectId, this.projectName, this.employeeId, this.employeeName});

  ModuleComments.fromJson(Map<String, dynamic> json) {
    comment = json['comment'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    moduleId = json['moduleId'] ?? "";
    moduleName = json['moduleName'] ?? "";
    projectId = json['projectId'] ?? "";
    projectName = json['projectName'] ?? "";
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['comment'] = comment;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['moduleId'] = moduleId;
    data['moduleName'] = moduleName;
    data['projectId'] = projectId;
    data['projectName'] = projectName;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    return data;
  }
}

class TasksList {
  String? addedBy;
  List<AssignedTo>? assignedTo;
  String? createdBy;
  String? createdOn;
  String? createdByName;
  int? estimationTime;
  int? involvedTime;
  String? description;
  String? endDate;
  String? id;
  String? name;
  String? notes;
  List<AssignedTo>? notifyTo;
  String? projectId;
  String? startDate;
  int? status;

  TasksList(
      {this.addedBy,
      this.assignedTo,
      this.createdBy,
        this.createdOn,
      this.createdByName,
        this.estimationTime,
        this.involvedTime,
      this.description,
      this.endDate,
      this.id,
      this.name,
      this.notes,
      this.notifyTo,
      this.projectId,
      this.startDate,
      this.status});

  TasksList.fromJson(Map<String, dynamic> json) {
    addedBy = json['addedBy'] ?? "";
    if (json['assignedTo'] != null) {
      assignedTo = <AssignedTo>[];
      json['assignedTo'].forEach((v) {
        assignedTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      assignedTo = <AssignedTo>[];
    }
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    createdByName = json['createdByName'] ?? "";
    estimationTime = json['estimationTime'] ?? 0;
    involvedTime = json['involvedTime']  ?? 0;
    description = json['description'] ?? "";
    endDate = json['endDate'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    notes = json['notes'] ?? "";
    if (json['notifyTo'] != null) {
      notifyTo = <AssignedTo>[];
      json['notifyTo'].forEach((v) {
        notifyTo!.add(AssignedTo.fromJson(v));
      });
    } else {
      notifyTo = <AssignedTo>[];
    }
    projectId = json['projectId'] ?? "";
    startDate = json['startDate'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['addedBy'] = addedBy;
    if (assignedTo != null) {
      data['assignedTo'] = assignedTo!.map((v) => v.toJson()).toList();
    }
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['createdByName'] = createdByName;
    data['estimationTime'] = estimationTime;
    data['involvedTime'] = involvedTime;
    data['description'] = description;
    data['endDate'] = endDate;
    data['id'] = id;
    data['name'] = name;
    data['notes'] = notes;
    if (notifyTo != null) {
      data['notifyTo'] = notifyTo!.map((v) => v.toJson()).toList();
    }
    data['projectId'] = projectId;
    data['startDate'] = startDate;
    data['status'] = status;
    return data;
  }
}
