class AllProfileStatusResponseModel {
  List<DisplayEnableProfilesStatusList>? displayEnableProfilesStatusList;
  List<ProfilesStatusList>? profilesStatusList;
  int? responseStatus;
  String? result;

  AllProfileStatusResponseModel(
      {this.displayEnableProfilesStatusList,
        this.profilesStatusList,
        this.responseStatus,
        this.result});

  AllProfileStatusResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['displayEnableProfilesStatusList'] != null) {
      displayEnableProfilesStatusList = <DisplayEnableProfilesStatusList>[];
      json['displayEnableProfilesStatusList'].forEach((v) {
        displayEnableProfilesStatusList!
            .add(new DisplayEnableProfilesStatusList.fromJson(v));
      });
    }
    if (json['profilesStatusList'] != null) {
      profilesStatusList = <ProfilesStatusList>[];
      json['profilesStatusList'].forEach((v) {
        profilesStatusList!.add(new ProfilesStatusList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.displayEnableProfilesStatusList != null) {
      data['displayEnableProfilesStatusList'] =
          this.displayEnableProfilesStatusList!.map((v) => v.toJson()).toList();
    }
    if (this.profilesStatusList != null) {
      data['profilesStatusList'] =
          this.profilesStatusList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DisplayEnableProfilesStatusList {
  String? createdBy;
  String? createdOn;
  bool? displayEnable;
  String? id;
  String? mailContent;
  bool? mailSent;
  String? mailSubject;
  int? profilesCount;
  int? status;
  String? statusName;

  DisplayEnableProfilesStatusList(
      {this.createdBy,
        this.createdOn,
        this.displayEnable,
        this.id,
        this.mailContent,
        this.mailSent,
        this.mailSubject,
        this.profilesCount,
        this.status,
        this.statusName});

  DisplayEnableProfilesStatusList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    displayEnable = json['displayEnable'];
    id = json['id'];
    mailContent = json['mailContent'];
    mailSent = json['mailSent'];
    mailSubject = json['mailSubject'];
    profilesCount = json['profilesCount'];
    status = json['status'];
    statusName = json['statusName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['displayEnable'] = this.displayEnable;
    data['id'] = this.id;
    data['mailContent'] = this.mailContent;
    data['mailSent'] = this.mailSent;
    data['mailSubject'] = this.mailSubject;
    data['profilesCount'] = this.profilesCount;
    data['status'] = this.status;
    data['statusName'] = this.statusName;
    return data;
  }
}
class ProfilesStatusList {
  String? createdBy;
  String? createdOn;
  String? id;
  int? status;
  bool? displayEnable;
  String? statusName;
  String? statusNameWithCount;

  bool? mailSent;
  String? mailSubject;
  String? mailContent;
  int? orderValue;

  ProfilesStatusList(
      {this.createdBy,
      this.createdOn,
      this.id,
      this.status,
        this.displayEnable,
      this.mailSent,
      this.statusName,
      this.mailSubject,
      this.mailContent,
      this.orderValue,this.statusNameWithCount});

  ProfilesStatusList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    status = json['status'] ?? 0;
    displayEnable = json['displayEnable'] ?? false;
    statusNameWithCount = json['statusName']  + " (${json['profilesCount'].toString()})" ?? "";
    statusName = json['statusName'] ;
    mailSent = json['mailSent'] ?? false;
    mailSubject = json['mailSubject'] ?? "";
    mailContent = json['mailContent'] ?? "";
    orderValue = json['orderValue'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['status'] = status;
    data['displayEnable'] = displayEnable;
    data['statusName'] = statusName;
    data['mailSent'] = mailSent;
    data['mailSubject'] = mailSubject;
    data['mailContent'] = mailContent;
    data['orderValue'] = orderValue;
    return data;
  }
}
