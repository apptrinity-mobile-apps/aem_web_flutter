class EmployeeAvailabilityModel {
  String? displayTitle;
  String? displayValue;
  List<Durations>? durationsList;
  bool? value;

  EmployeeAvailabilityModel({this.displayTitle, this.displayValue, this.durationsList, this.value});

  EmployeeAvailabilityModel.fromJson(Map<String, dynamic> json) {
    displayTitle = json['displayTitle'];
    displayValue = json['displayValue'];
    durationsList = json['durationsList'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['displayTitle'] = displayTitle;
    data['displayValue'] = displayValue;
    data['durationsList'] = durationsList;
    data['value'] = value;
    return data;
  }
}

class Durations {
  String? startTime;
  String? endTime;

  Durations({this.startTime, this.endTime});

  Durations.fromJson(Map<String, dynamic> json) {
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['startTime'] = startTime;
    data['endTime'] = endTime;
    return data;
  }
}
