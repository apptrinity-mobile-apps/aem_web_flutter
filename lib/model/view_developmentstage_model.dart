class SingleViewDevelopmentStageModel {
  DevelopmentStageDetails? developmentStageDetails;
  int? responseStatus;
  String? result;

  SingleViewDevelopmentStageModel(
      {this.developmentStageDetails, this.responseStatus, this.result});

  SingleViewDevelopmentStageModel.fromJson(Map<String, dynamic> json) {
    developmentStageDetails = json['developmentStageDetails'] != null
        ? new DevelopmentStageDetails.fromJson(json['developmentStageDetails'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.developmentStageDetails != null) {
      data['developmentStageDetails'] = this.developmentStageDetails!.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DevelopmentStageDetails {
  String? createdBy;
  String? createdOn;
  String? id;
  String? stage;
  int? status;

  DevelopmentStageDetails(
      {this.createdBy, this.createdOn, this.id, this.stage, this.status});

  DevelopmentStageDetails.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    id = json['id'];
    stage = json['stage'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['stage'] = this.stage;
    data['status'] = this.status;
    return data;
  }
}

