class DayBasedEmployeeSchedulesModel {
  String? result;
  int? responseStatus;
  /*List<AvailableSchedule>? availableSchedule;*/
  List<String>? availableSchedule;

  DayBasedEmployeeSchedulesModel({this.result, this.responseStatus, this.availableSchedule});

  DayBasedEmployeeSchedulesModel.fromJson(Map<String, dynamic> json) {
    result = json['result'] ?? "";
    responseStatus = json['responseStatus'] ?? 0;
    availableSchedule = json['availableSchedule'].cast<String>() ?? [];
    /*if (json['availableSchedule'] != null) {
      availableSchedule = <AvailableSchedule>[];
      json['availableSchedule'].forEach((v) {
        availableSchedule!.add(AvailableSchedule.fromJson(v));
      });
    } else {
      availableSchedule = <AvailableSchedule>[];
    }*/
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['result'] = result;
    data['responseStatus'] = responseStatus;
    data['availableSchedule'] = availableSchedule;
    /*if (availableSchedule != null) {
      data['availableSchedule'] = availableSchedule!.map((v) => v.toJson()).toList();
    }*/
    return data;
  }
}

class AvailableSchedule {
  String? startTime;
  String? endTime;

  AvailableSchedule({this.startTime, this.endTime});

  AvailableSchedule.fromJson(Map<String, dynamic> json) {
    startTime = json['startTime'] ?? "";
    endTime = json['endTime'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['startTime'] = startTime;
    data['endTime'] = endTime;
    return data;
  }
}
