class TaskLogsListResponseModel {
  int? responseStatus;
  String? result;
  List<TaskLogsList>? taskLogsList;

  TaskLogsListResponseModel(
      {this.responseStatus, this.result, this.taskLogsList});

  TaskLogsListResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['taskLogsList'] != null) {
      taskLogsList = <TaskLogsList>[];
      json['taskLogsList'].forEach((v) {
        taskLogsList!.add(new TaskLogsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.taskLogsList != null) {
      data['taskLogsList'] = this.taskLogsList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TaskLogsList {
  String? day;
  String? dayForSorting;
  List<LogsData>? logsData;

  TaskLogsList({this.day, this.dayForSorting, this.logsData});

  TaskLogsList.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    dayForSorting = json['day_for_sorting'];
    if (json['logsData'] != null) {
      logsData = <LogsData>[];
      json['logsData'].forEach((v) {
        logsData!.add(new LogsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['day_for_sorting'] = this.dayForSorting;
    if (this.logsData != null) {
      data['logsData'] = this.logsData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogsData {
  String? bugId;
  String? bugName;
  String? createdOn;
  String? description;
  String? employeeId;
  String? employeeName;
  String? logType;
  String? moduleCreatedBy;
  String? moduleCreatedByName;
  String? moduleEndDate;
  String? moduleId;
  String? moduleName;
  String? moduleStartDate;
  String? taskId;
  String? taskName;
  int? taskStatus;
  int? bugStatus;

  LogsData(
      {this.bugId,
        this.bugName,
        this.createdOn,
        this.description,
        this.employeeId,
        this.employeeName,
        this.logType,
        this.moduleCreatedBy,
        this.moduleCreatedByName,
        this.moduleEndDate,
        this.moduleId,
        this.moduleName,
        this.moduleStartDate,
        this.taskId,
        this.taskName,
        this.taskStatus,
        this.bugStatus});

  LogsData.fromJson(Map<String, dynamic> json) {
    bugId = json['bugId'];
    bugName = json['bugName'];
    createdOn = json['createdOn'];
    description = json['description'];
    employeeId = json['employeeId'];
    employeeName = json['employeeName'];
    logType = json['logType'];
    moduleCreatedBy = json['moduleCreatedBy'];
    moduleCreatedByName = json['moduleCreatedByName'];
    moduleEndDate = json['moduleEndDate']??"";
    moduleId = json['moduleId'];
    moduleName = json['moduleName'];
    moduleStartDate = json['moduleStartDate'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskStatus = json['taskStatus'];
    bugStatus = json['bugStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bugId'] = this.bugId;
    data['bugName'] = this.bugName;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;
    data['logType'] = this.logType;
    data['moduleCreatedBy'] = this.moduleCreatedBy;
    data['moduleCreatedByName'] = this.moduleCreatedByName;
    data['moduleEndDate'] = this.moduleEndDate;
    data['moduleId'] = this.moduleId;
    data['moduleName'] = this.moduleName;
    data['moduleStartDate'] = this.moduleStartDate;
    data['taskId'] = this.taskId;
    data['taskName'] = this.taskName;
    data['taskStatus'] = this.taskStatus;
    data['bugStatus'] = this.bugStatus;
    return data;
  }
}