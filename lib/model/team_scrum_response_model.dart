class TeamScrumCountResponseModel {
  int? responseStatus;
  String? result;
  List<ScrumData>? scrumData;

  TeamScrumCountResponseModel(
      {this.responseStatus, this.result, this.scrumData});

  TeamScrumCountResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['scrumData'] != null) {
      scrumData = <ScrumData>[];
      json['scrumData'].forEach((v) {
        scrumData!.add(new ScrumData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.scrumData != null) {
      data['scrumData'] = this.scrumData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScrumData {
  String? heading;
  List<TasksCount>? tasksCount;

  ScrumData({this.heading, this.tasksCount});

  ScrumData.fromJson(Map<String, dynamic> json) {
    heading = json['heading'];
    if (json['tasksCount'] != null) {
      tasksCount = <TasksCount>[];
      json['tasksCount'].forEach((v) {
        tasksCount!.add(new TasksCount.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['heading'] = this.heading;
    if (this.tasksCount != null) {
      data['tasksCount'] = this.tasksCount!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksCount {
  int? completedTasksCount;
  String? employeeName;
  String? employeeId;
  int? newTasksCount;
  int? pendingTasksCount;
  int? totalTasksCount;

  TasksCount(
      {this.completedTasksCount,
        this.employeeName,
        this.employeeId,
        this.newTasksCount,
        this.pendingTasksCount,
        this.totalTasksCount});

  TasksCount.fromJson(Map<String, dynamic> json) {
    completedTasksCount = json['completed_tasks_count'];
    employeeName = json['employeeName'];
    employeeId = json['employeeId'];
    newTasksCount = json['new_tasks_count'];
    pendingTasksCount = json['pending_tasks_count'];
    totalTasksCount = json['total_tasks_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['completed_tasks_count'] = this.completedTasksCount;
    data['employeeName'] = this.employeeName;
    data['employeeId'] = this.employeeId;
    data['new_tasks_count'] = this.newTasksCount;
    data['pending_tasks_count'] = this.pendingTasksCount;
    data['total_tasks_count'] = this.totalTasksCount;
    return data;
  }
}