class AllProfilesResponse {
  List<ProfilesList>? profilesList;
  int? responseStatus;
  String? result;

  AllProfilesResponse({this.profilesList, this.responseStatus, this.result});

  AllProfilesResponse.fromJson(Map<String, dynamic> json) {
    if (json['profilesList'] != null) {
      profilesList = <ProfilesList>[];
      json['profilesList'].forEach((v) {
        profilesList!.add(ProfilesList.fromJson(v));
      });
    } else {
      profilesList = <ProfilesList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (profilesList != null) {
      data['profilesList'] = profilesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ProfilesList {
  String? createdBy;
  String? createdByName;
  String? createdOn;
  String? email;
  String? employmentStatus;
  String? employmentStatusName;
  String? interviewTypeId;
  String? interviewTypeName;
  String? id;
  String? name;
  String? phoneNumber;
  String? jobRole;
  List<ProfileCommentsList>? profileCommentsList;
  bool? profileInterviewStatus;
  String? resume;
  String? mainTechnology;
  int? status;
  List<SuperiorEmployees>? superiorEmployees;
  List<TechnologyList>? technologyList;

  ProfilesList(
      {this.createdBy,
      this.createdByName,
      this.createdOn,
      this.email,
      this.employmentStatus,
      this.employmentStatusName,
      this.interviewTypeId,
      this.interviewTypeName,
      this.id,
      this.name,
      this.phoneNumber,
      this.profileCommentsList,
      this.profileInterviewStatus,
      this.resume,
      this.status,
      this.superiorEmployees,
      this.technologyList,this.mainTechnology,this.jobRole});

  ProfilesList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdByName = json['createdByName'] ?? "";
    createdOn = json['createdOn'] ?? "";
    email = json['email'] ?? "";
    jobRole = json['jobRole'] ?? "";
    employmentStatus = json['employmentStatus'] ?? "";
    employmentStatusName = json['employmentStatusName'] ?? "";
    interviewTypeId = json['interviewTypeId'] ?? "";
    interviewTypeName = json['interviewTypeName'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    mainTechnology = json['mainTechnology'] ?? "";
    phoneNumber = json['phoneNumber'] ?? "";
    if (json['profileCommentsList'] != null) {
      profileCommentsList = <ProfileCommentsList>[];
      json['profileCommentsList'].forEach((v) {
        profileCommentsList!.add(ProfileCommentsList.fromJson(v));
      });
    } else {
      profileCommentsList = <ProfileCommentsList>[];
    }
    profileInterviewStatus = json['profileInterviewStatus'];
    resume = json['resume'] ?? "";
    status = json['status'] ?? 0;
    if (json['superiorEmployees'] != null) {
      superiorEmployees = <SuperiorEmployees>[];
      json['superiorEmployees'].forEach((v) {
        superiorEmployees!.add(SuperiorEmployees.fromJson(v));
      });
    } else {
      superiorEmployees = <SuperiorEmployees>[];
    }
    if (json['technologyList'] != null) {
      technologyList = <TechnologyList>[];
      json['technologyList'].forEach((v) {
        technologyList!.add(TechnologyList.fromJson(v));
      });
    } else {
      technologyList = <TechnologyList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdByName'] = createdByName;
    data['jobRole'] = jobRole;
    data['createdOn'] = createdOn;
    data['email'] = email;
    data['employmentStatus'] = employmentStatus;
    data['employmentStatusName'] = employmentStatusName;
    data['interviewTypeId'] = interviewTypeId;
    data['interviewTypeName'] = interviewTypeName;
    data['id'] = id;
    data['name'] = name;
    data['mainTechnology'] = mainTechnology;
    data['phoneNumber'] = phoneNumber;
    if (profileCommentsList != null) {
      data['profileCommentsList'] = profileCommentsList!.map((v) => v.toJson()).toList();
    }
    data['profileInterviewStatus'] = profileInterviewStatus;
    data['resume'] = resume;
    data['status'] = status;
    if (superiorEmployees != null) {
      data['superiorEmployees'] = superiorEmployees!.map((v) => v.toJson()).toList();
    }
    if (technologyList != null) {
      data['technologyList'] = technologyList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SuperiorEmployees {
  String? superiorEmployeeId;
  String? superiorEmployeeName;

  SuperiorEmployees({this.superiorEmployeeId, this.superiorEmployeeName});

  SuperiorEmployees.fromJson(Map<String, dynamic> json) {
    superiorEmployeeId = json['superiorEmployeeId'] ?? "";
    superiorEmployeeName = json['superiorEmployeeName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['superiorEmployeeId'] = superiorEmployeeId;
    data['superiorEmployeeName'] = superiorEmployeeName;
    return data;
  }
}

class ProfileCommentsList {
  String? comment;
  String? createdOn;
  String? employeeId;
  String? employeeName;
  String? id;
  String? profileId;
  int? status;

  ProfileCommentsList({this.comment, this.createdOn, this.employeeId, this.employeeName, this.id, this.profileId, this.status});

  ProfileCommentsList.fromJson(Map<String, dynamic> json) {
    comment = json['comment'] ?? "";
    createdOn = json['createdOn'] ?? "";
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    id = json['id'] ?? "";
    profileId = json['profileId'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['comment'] = comment;
    data['createdOn'] = createdOn;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['id'] = id;
    data['profileId'] = profileId;
    data['status'] = status;
    return data;
  }
}

class TechnologyList {
  String? name;
  String? technologyId;

  TechnologyList({this.name, this.technologyId});

  TechnologyList.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? "";
    technologyId = json['technologyId'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['technologyId'] = technologyId;
    return data;
  }
}
