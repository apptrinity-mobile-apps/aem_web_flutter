class DashboardProjectsResponse {
  List<ProjectsData>? projectsData;
  int? responseStatus;
  String? result;

  DashboardProjectsResponse({this.projectsData, this.responseStatus, this.result});

  DashboardProjectsResponse.fromJson(Map<String, dynamic> json) {
    if (json['projectsData'] != null) {
      projectsData = <ProjectsData>[];
      json['projectsData'].forEach((v) {
        projectsData!.add(ProjectsData.fromJson(v));
      });
    } else {
      projectsData = <ProjectsData>[];
    }
    responseStatus = json['responseStatus'] ?? "";
    result = json['result'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (projectsData != null) {
      data['projectsData'] = projectsData!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ProjectsData {
  String? createdBy;
  String? createdOn;
  String? description;
  List<String>? employees;
  String? id;
  String? name;
  int? newCount;
  int? overDueCount;
  int? status;

  ProjectsData({this.createdBy, this.createdOn, this.description, this.employees, this.id, this.name, this.newCount, this.overDueCount, this.status});

  ProjectsData.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    employees = json['employees'].cast<String>() ?? [];
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    newCount = json['newCount'] ?? 0;
    overDueCount = json['overDueCount'] ?? 0;
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['employees'] = employees;
    data['id'] = id;
    data['name'] = name;
    data['newCount'] = newCount;
    data['overDueCount'] = overDueCount;
    data['status'] = status;
    return data;
  }
}
