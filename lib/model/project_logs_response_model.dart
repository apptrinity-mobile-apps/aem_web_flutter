import 'package:aem/model/project_details_response_model.dart';

class ProjectLogsResponseModel {
  List<Logslist>? logslist;
  int? responseStatus;
  String? result;

  ProjectLogsResponseModel({this.logslist, this.responseStatus, this.result});

  ProjectLogsResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['logslist'] != null) {
      logslist = <Logslist>[];
      json['logslist'].forEach((v) {
        logslist!.add(Logslist.fromJson(v));
      });
    } else {
      logslist = <Logslist>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (logslist != null) {
      data['logslist'] = logslist!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}
