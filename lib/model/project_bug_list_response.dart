class ProjectBugsListResponse {
  List<BugsList>? bugsList;
  int? responseStatus;
  String? result;
  BugsList? bugDetails;
  ProjectBugsListResponse({this.bugsList, this.responseStatus, this.result, this.bugDetails});
  ProjectBugsListResponse.fromJson(Map<String, dynamic> json) {
    if (json['bugsList'] != null) {
      bugsList = <BugsList>[];
      json['bugsList'].forEach((v) {
        bugsList!.add(BugsList.fromJson(v));
      });
    }
    bugDetails = json['bugDetails'] != null ? BugsList.fromJson(json['bugDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (bugsList != null) {
      data['bugsList'] = bugsList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    data['bugDetails'] = bugDetails;
    return data;
  }
}
class BugsList {
  List<String>? attachments;
  String? bugId;
  String? bugStatus;
  String? createdOn;
  String? description;
  String? employeeId;
  String? employeeName;
  String? moduleId;
  String? moduleName;
  String? priority;
  String? projectId;
  String? projectName;
  int? status;
  String? taskId;
  String? taskName;
  String? title;
  String? devStageId;
  String? devStage;
  BugsList(
      {this.attachments,
      this.bugId,
      this.bugStatus,
      this.createdOn,
      this.description,
      this.employeeId,
      this.employeeName,
      this.moduleId,
      this.moduleName,
      this.priority,
      this.projectId,
      this.projectName,
      this.status,
      this.taskId,
      this.taskName,
      this.title,
      this.devStageId,
      this.devStage});
  BugsList.fromJson(Map<String, dynamic> json) {
    attachments = json['attachments'].cast<String>() ?? [];
    bugId = json['bugId'] ?? "";
    bugStatus = json['bugStatus'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    moduleId = json['moduleId'] ?? "";
    moduleName = json['moduleName'] ?? "";
    priority = json['priority'] ?? "";
    projectId = json['projectId'] ?? "";
    projectName = json['projectName'] ?? "";
    status = json['status'] ?? 0;
    taskId = json['taskId'] ?? "";
    taskName = json['taskName'] ?? "";
    title = json['title'] ?? "";
    devStageId = json['devStageId'] ?? "";
    devStage = json['devStage'] ?? "";
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['attachments'] = attachments;
    data['bugId'] = bugId;
    data['bugStatus'] = bugStatus;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['moduleId'] = moduleId;
    data['moduleName'] = moduleName;
    data['priority'] = priority;
    data['projectId'] = projectId;
    data['projectName'] = projectName;
    data['status'] = status;
    data['taskId'] = taskId;
    data['taskName'] = taskName;
    data['title'] = title;
    data['devStageId'] = devStageId;
    data['devStage'] = devStage;
    return data;
  }
}
