class InterviewTypeDetailsResponseModel {
  InterviewTypeDetails? interviewTypeDetails;
  int? responseStatus;
  String? result;

  InterviewTypeDetailsResponseModel({this.interviewTypeDetails, this.responseStatus, this.result});

  InterviewTypeDetailsResponseModel.fromJson(Map<String, dynamic> json) {
    interviewTypeDetails = json['interviewTypeDetails'] != null ? InterviewTypeDetails.fromJson(json['interviewTypeDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (interviewTypeDetails != null) {
      data['interviewTypeDetails'] = interviewTypeDetails!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class InterviewTypeDetails {
  String? createdBy;
  String? createdOn;
  String? id;
  String? interviewType;
  int? status;
  bool? systemTestRequired;
  bool? mailSent;
  String? mailContent;
  String? mailSubject;

  InterviewTypeDetails(
      {this.createdBy,
      this.createdOn,
      this.id,
      this.interviewType,
      this.status,
      this.systemTestRequired,
      this.mailSent,
      this.mailContent,
      this.mailSubject});

  InterviewTypeDetails.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    interviewType = json['interviewType'] ?? "";
    status = json['status'] ?? 0;
    systemTestRequired = json['systemTestRequired'] ?? false;
    mailSent = json['mailSent'] ?? false;
    mailContent = json['mailContent'] ?? "";
    mailSubject = json['mailSubject'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['interviewType'] = interviewType;
    data['status'] = status;
    data['systemTestRequired'] = systemTestRequired;
    data['mailSent'] = mailSent;
    data['mailContent'] = mailContent;
    data['mailSubject'] = mailSubject;
    return data;
  }
}
