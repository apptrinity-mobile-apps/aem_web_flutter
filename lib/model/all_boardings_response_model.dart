class AllBoardingsResponse {
  List<BoardingsList>? boardingsList;
  int? responseStatus;
  String? result;

  AllBoardingsResponse({this.boardingsList, this.responseStatus, this.result});

  AllBoardingsResponse.fromJson(Map<String, dynamic> json) {
    if (json['boardingsList'] != null) {
      boardingsList = <BoardingsList>[];
      json['boardingsList'].forEach((v) {
        boardingsList!.add(new BoardingsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.boardingsList != null) {
      data['boardingsList'] =
          this.boardingsList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
class BoardingsList {
  String? boardDescription;
  String? boardName;
  String? createdOn;
  String? employeeId;
  String? id;
  int? status;
  BoardingsList(
      {this.boardDescription,
        this.boardName,
        this.createdOn,
        this.employeeId,
        this.id,
        this.status});
  BoardingsList.fromJson(Map<String, dynamic> json) {
    boardDescription = json['boardDescription'];
    boardName = json['boardName'];
    createdOn = json['createdOn'];
    employeeId = json['employeeId'];
    id = json['id'];
    status = json['status'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['boardDescription'] = this.boardDescription;
    data['boardName'] = this.boardName;
    data['createdOn'] = this.createdOn;
    data['employeeId'] = this.employeeId;
    data['id'] = this.id;
    data['status'] = this.status;
    return data;
  }
}