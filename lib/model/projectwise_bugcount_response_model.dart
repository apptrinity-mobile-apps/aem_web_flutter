class ProjectwiseBugCountResponse {
  List<ProjectsList>? projectsList;
  int? responseStatus;
  String? result;

  ProjectwiseBugCountResponse(
      {this.projectsList, this.responseStatus, this.result});

  ProjectwiseBugCountResponse.fromJson(Map<String, dynamic> json) {
    if (json['projectsList'] != null) {
      projectsList = <ProjectsList>[];
      json['projectsList'].forEach((v) {
        projectsList!.add(new ProjectsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.projectsList != null) {
      data['projectsList'] = this.projectsList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ProjectsList {
  int? highBugsCount;
  int? lowBugsCount;
  int? mediumBugsCount;
  String? projectId;
  String? projectName;
  int? totalBugsCount;

  ProjectsList(
      {this.highBugsCount,
        this.lowBugsCount,
        this.mediumBugsCount,
        this.projectId,
        this.projectName,
        this.totalBugsCount});

  ProjectsList.fromJson(Map<String, dynamic> json) {
    highBugsCount = json['highBugsCount'];
    lowBugsCount = json['lowBugsCount'];
    mediumBugsCount = json['mediumBugsCount'];
    projectId = json['projectId'];
    projectName = json['projectName'];
    totalBugsCount = json['totalBugsCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['highBugsCount'] = this.highBugsCount;
    data['lowBugsCount'] = this.lowBugsCount;
    data['mediumBugsCount'] = this.mediumBugsCount;
    data['projectId'] = this.projectId;
    data['projectName'] = this.projectName;
    data['totalBugsCount'] = this.totalBugsCount;
    return data;
  }
}