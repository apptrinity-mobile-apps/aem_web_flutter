class ProjectItem {
   String? count;
   String? countMessage;
   String? title;
   String? description;
   String? icon;

   ProjectItem({this.count, this.title, this.description, this.icon,this.countMessage});
}
