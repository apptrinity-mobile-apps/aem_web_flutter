class ProjectDetailModel {
  ProjectDict? projectDict;
  int? responseStatus;
  String? result;

  ProjectDetailModel({this.projectDict, this.responseStatus, this.result});

  ProjectDetailModel.fromJson(Map<String, dynamic> json) {
    projectDict = json['project_dict'] != null
        ? new ProjectDict.fromJson(json['project_dict'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.projectDict != null) {
      data['project_dict'] = this.projectDict!.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ProjectDict {
  String? boardingId;
  String? createdBy;
  String? createdByName;
  String? description;
  List<Employees>? employees;
  String? id;
  String? name;
  int? status;
  List<TechnologiesList>? technologies;

  ProjectDict(
      {this.boardingId,
        this.createdBy,
        this.createdByName,
        this.description,
        this.employees,
        this.id,
        this.name,
        this.status,
        this.technologies});

  ProjectDict.fromJson(Map<String, dynamic> json) {
    boardingId = json['boardingId'];
    createdBy = json['createdBy'];
    createdByName = json['createdByName'];
    description = json['description'];
    if (json['employees'] != null) {
      employees = <Employees>[];
      json['employees'].forEach((v) {
        employees!.add(new Employees.fromJson(v));
      });
    }
    id = json['id'];
    name = json['name'];
    status = json['status'];
    if (json['technologies'] != null) {
      technologies = <TechnologiesList>[];
      json['technologies'].forEach((v) {
        technologies!.add(new TechnologiesList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['boardingId'] = this.boardingId;
    data['createdBy'] = this.createdBy;
    data['createdByName'] = this.createdByName;
    data['description'] = this.description;
    if (this.employees != null) {
      data['employees'] = this.employees!.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['name'] = this.name;
    data['status'] = this.status;
    if (this.technologies != null) {
      data['technologies'] = this.technologies!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Employees {
  String? id;
  String? name;

  Employees({this.id, this.name});

  Employees.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
class TechnologiesList {
  String? id;
  String? name;

  TechnologiesList({this.id, this.name});

  TechnologiesList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}