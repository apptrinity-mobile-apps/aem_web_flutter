class ProfileStatusDetailResponseModel {
  ProfileStatusDetails? profileStatusDetails;
  int? responseStatus;
  String? result;

  ProfileStatusDetailResponseModel({this.profileStatusDetails, this.responseStatus, this.result});

  ProfileStatusDetailResponseModel.fromJson(Map<String, dynamic> json) {
    profileStatusDetails = json['profileStatusDetails'] != null ? ProfileStatusDetails.fromJson(json['profileStatusDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (profileStatusDetails != null) {
      data['profileStatusDetails'] = profileStatusDetails!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ProfileStatusDetails {
  String? createdBy;
  String? createdOn;
  String? id;
  int? status;
  String? statusName;
  String? mailSubject;
  String? mailContent;
  bool? systemTestRequired;
  bool? mailSent;

  ProfileStatusDetails({this.createdBy, this.createdOn, this.id, this.status, this.statusName, this.mailSubject, this.mailContent, this.systemTestRequired, this.mailSent});

  ProfileStatusDetails.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? '';
    status = json['status'] ?? 0;
    statusName = json['statusName'] ?? "";
    mailSubject = json['mailSubject'] ?? "";
    mailContent = json['mailContent'] ?? "";
    systemTestRequired = json['systemTestRequired'] ?? false;
    mailSent = json['mailSent'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['status'] = status;
    data['statusName'] = statusName;
    data['mailSubject'] = mailSubject;
    data['mailContent'] = mailContent;
    data['systemTestRequired'] = systemTestRequired;
    data['mailSent'] = mailSent;
    return data;
  }
}
