class DashBoardBoardings {
  List<BoardingsData>? boardingsData;
  int? responseStatus;
  String? result;
  DashBoardBoardings({this.boardingsData, this.responseStatus, this.result});
  DashBoardBoardings.fromJson(Map<String, dynamic> json) {
    if (json['boardingsData'] != null) {
      boardingsData = <BoardingsData>[];
      json['boardingsData'].forEach((v) {
        boardingsData!.add(new BoardingsData.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.boardingsData != null) {
      data['boardingsData'] =
          this.boardingsData!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class BoardingsData {
  String? boardDescription;
  String? boardName;
  String? createdOn;
  String? employeeId;
  String? id;
  int? status;
  BoardingsData(
      {this.boardDescription,
        this.boardName,
        this.createdOn,
        this.employeeId,
        this.id,
        this.status});

  BoardingsData.fromJson(Map<String, dynamic> json) {
    boardDescription = json['boardDescription'];
    boardName = json['boardName'];
    createdOn = json['createdOn'];
    employeeId = json['employeeId'];
    id = json['id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['boardDescription'] = this.boardDescription;
    data['boardName'] = this.boardName;
    data['createdOn'] = this.createdOn;
    data['employeeId'] = this.employeeId;
    data['id'] = this.id;
    data['status'] = this.status;
    return data;
  }
}