class AllTechnologiesResponse {
  List<TechnologiesList>? technologyList;
  int? responseStatus;
  String? result;
  TechnologiesList? technology;

  AllTechnologiesResponse({this.technologyList, this.responseStatus, this.result, this.technology});

  AllTechnologiesResponse.fromJson(Map<String, dynamic> json) {
    if (json['technologyList'] != null) {
      technologyList = <TechnologiesList>[];
      json['technologyList'].forEach((v) {
        technologyList!.add(TechnologiesList.fromJson(v));
      });
    } else {
      technologyList = <TechnologiesList>[];
    }
    technology = json['technologyDetails'] != null ? TechnologiesList.fromJson(json['technologyDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (technologyList != null) {
      data['technologyList'] = technologyList!.map((v) => v.toJson()).toList();
    }
    if (technology != null) {
      data['technologyDetails'] = technology!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class TechnologiesList {
  String? createdBy;
  String? id;
  String? image;
  String? name;
  int? status;

  TechnologiesList({this.createdBy, this.id, this.image, this.name, this.status});

  TechnologiesList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    id = json['id'] ?? "";
    image = json['image'] ?? "";
    name = json['name'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['id'] = id;
    data['image'] = image;
    data['name'] = name;
    data['status'] = status;
    return data;
  }
}
