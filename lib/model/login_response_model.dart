class LoginResponse {
  EmployeeData? employeeData;
  int? responseStatus;
  String? result;

  LoginResponse({this.employeeData, this.responseStatus, this.result});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    employeeData = json['employeeData'] != null ? EmployeeData.fromJson(json['employeeData']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (employeeData != null) {
      data['employeeData'] = employeeData!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class EmployeeData {
  String? createdOn;
  String? email;
  String? id;
  bool? isSuperAdmin;
  bool? isSuperiorEmployee;
  String? name;
  String? chatId;
  int? status;
  EmployeeData({this.createdOn, this.email, this.id, this.isSuperAdmin, this.name,this.chatId, this.status,this.isSuperiorEmployee});
  EmployeeData.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'] ?? "";
    email = json['email'] ?? "";
    id = json['id'] ?? "";
    chatId = json['chatId'] ?? "";
    isSuperAdmin = json['isSuperAdmin'] ?? false;
    isSuperiorEmployee = json['isSuperiorEmployee'] ?? false;
    name = json['name'] ?? "";
    status = json['status'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdOn'] = createdOn;
    data['email'] = email;
    data['id'] = id;
    data['chatId'] = chatId;
    data['isSuperAdmin'] = isSuperAdmin;
    data['name'] = name;
    data['status'] = status;
    data['isSuperiorEmployee'] = isSuperiorEmployee;
    return data;
  }
}
