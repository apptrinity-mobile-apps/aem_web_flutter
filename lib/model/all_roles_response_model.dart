class AllRolesResponse {
  int? responseStatus;
  String? result;
  List<RolesDetails>? rolesDetails;
  RolesDetails? roleData;

  AllRolesResponse({this.responseStatus, this.result, this.rolesDetails, this.roleData});

  AllRolesResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    roleData = json['roleData'] != null ? RolesDetails.fromJson(json['roleData']) : null;
    if (json['rolesDetails'] != null) {
      rolesDetails = <RolesDetails>[];
      json['rolesDetails'].forEach((v) {
        rolesDetails!.add(RolesDetails.fromJson(v));
      });
    } else {
      rolesDetails = <RolesDetails>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    if (roleData != null) {
      data['roleData'] = roleData!.toJson();
    }
    if (rolesDetails != null) {
      data['rolesDetails'] = rolesDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RolesDetails {
  String? createdBy;
  String? createdOn;
  String? id;
  String? role;
  int? status;
  List<String>? designationPermissions;
  List<String>? sprintPermissions;
  List<String>? scrumPermissions;
  List<String>? employeePermissions;
  List<String>? projectPermissions;
  List<String>? boardingPermissions;
  List<String>? rolePermissions;
  List<String>? technologyPermissions;
  List<String>? interviewTypesPermissions;
  List<String>? profileStatusPermissions;
  List<String>? interviewStatusPermissions;
  List<String>? profilePermissions;
  List<String>? taskPermissions;
  List<String>? bugPermissions;
  List<String>? modulePermissions;
  List<String>? dashboardPermissions;
  List<String>? stagesPermissions;

  RolesDetails(
      {this.createdBy,
      this.createdOn,
      this.id,
      this.role,
      this.status,
      this.designationPermissions,
      this.employeePermissions,
        this.sprintPermissions,
        this.scrumPermissions,
      this.projectPermissions,
        this.boardingPermissions,
      this.rolePermissions,
      this.technologyPermissions,
      this.interviewTypesPermissions,
      this.profileStatusPermissions,
      this.interviewStatusPermissions,
      this.profilePermissions,
      this.taskPermissions,
        this.bugPermissions,
      this.modulePermissions,
      this.dashboardPermissions,
      this.stagesPermissions});

  RolesDetails.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    role = json['role'] ?? "";
    status = json['status'] ?? 0;
    sprintPermissions = json['sprintPermissions'].cast<String>() ?? [];
    scrumPermissions = json['scrumPermissions'].cast<String>() ?? [];
    designationPermissions = json['designationPermissions'].cast<String>() ?? [];
    employeePermissions = json['employeePermissions'].cast<String>() ?? [];
    projectPermissions = json['projectPermissions'].cast<String>() ?? [];
    boardingPermissions = json['boardingPermissions'].cast<String>() ?? [];
    rolePermissions = json['rolePermissions'].cast<String>() ?? [];
    technologyPermissions = json['technologyPermissions'].cast<String>() ?? [];
    interviewTypesPermissions = json['interviewTypesPermissions'].cast<String>() ?? [];
    profileStatusPermissions = json['profileStatusPermissions'].cast<String>() ?? [];
    interviewStatusPermissions = json['interviewStatusPermissions'].cast<String>() ?? [];
    profilePermissions = json['profilePermissions'].cast<String>() ?? [];
    taskPermissions = json['taskPermissions'].cast<String>() ?? [];
    bugPermissions = json['bugPermissions'].cast<String>() ?? [];
    modulePermissions = json['modulePermissions'].cast<String>() ?? [];
    dashboardPermissions = json['dashboardPermissions'].cast<String>() ?? [];
    stagesPermissions = json['stagesPermissions'].cast<String>() ?? [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['role'] = role;
    data['status'] = status;
    data['sprintPermissions'] = sprintPermissions;
    data['scrumPermissions'] = scrumPermissions;
    data['designationPermissions'] = designationPermissions;
    data['employeePermissions'] = employeePermissions;
    data['projectPermissions'] = projectPermissions;
    data['boardingPermissions'] = boardingPermissions;

    data['rolePermissions'] = rolePermissions;
    data['technologyPermissions'] = technologyPermissions;
    data['interviewTypesPermissions'] = interviewTypesPermissions;
    data['profileStatusPermissions'] = profileStatusPermissions;
    data['interviewStatusPermissions'] = interviewStatusPermissions;
    data['profilePermissions'] = profilePermissions;
    data['taskPermissions'] = taskPermissions;
    data['bugPermissions'] = bugPermissions;
    data['modulePermissions'] = modulePermissions;
    data['dashboardPermissions'] = dashboardPermissions;
    data['stagesPermissions'] = stagesPermissions;
    return data;
  }
}
