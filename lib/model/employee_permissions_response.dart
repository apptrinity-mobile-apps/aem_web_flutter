class EmployeePermissionsResponse {
  EmployeePermissions? employeePermissions;
  String? result;
  int? responseStatus;

  EmployeePermissionsResponse({this.employeePermissions, this.responseStatus, this.result});

  EmployeePermissionsResponse.fromJson(Map<String, dynamic> json) {
    employeePermissions = json['employeePermissions'] != null ? EmployeePermissions.fromJson(json['employeePermissions']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (employeePermissions != null) {
      data['employeePermissions'] = employeePermissions!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class EmployeePermissions {
  List<String>? designationPermissions;
  String? employeeId;
  List<String>? employeePermissions;
  List<String>? developmentStatgesPermissions;
  List<String>? sprintPermissions;
  List<String>? scrumPermissions;
  List<String>? projectPermissions;
  List<String>? boardingPermissions;
  List<String>? rolePermissions;
  List<String>? technologyPermissions;
  List<String>? interviewTypesPermissions;
  List<String>? profileStatusPermissions;
  List<String>? interviewStatusPermissions;
  List<String>? jobRolePermissions;
  List<String>? profilePermissions;
  List<String>? taskPermissions;
  List<String>? modulePermissions;
  List<String>? bugsPermissions;

  EmployeePermissions(
      {this.designationPermissions,
        this.developmentStatgesPermissions,
      this.employeeId,
        this.sprintPermissions,
        this.scrumPermissions,
      this.employeePermissions,
      this.projectPermissions,
      this.rolePermissions,
      this.technologyPermissions,
      this.interviewTypesPermissions,
      this.profileStatusPermissions,
      this.interviewStatusPermissions,
        this.jobRolePermissions,
      this.profilePermissions,
        this.taskPermissions,
        this.modulePermissions,
      this.boardingPermissions,
        this.bugsPermissions});
  EmployeePermissions.fromJson(Map<String, dynamic> json) {
    designationPermissions = json['designationPermissions'].cast<String>() ?? [];
    developmentStatgesPermissions = json['stagesPermissions'].cast<String>() ?? [];
    employeeId = json['employeeId'];
    sprintPermissions = json['sprintPermissions'].cast<String>() ?? [];
    scrumPermissions = json['scrumPermissions'].cast<String>() ?? [];
    employeePermissions = json['employeePermissions'].cast<String>() ?? [];
    projectPermissions = json['projectPermissions'].cast<String>() ?? [];
    rolePermissions = json['rolePermissions'].cast<String>() ?? [];
    technologyPermissions = json['technologyPermissions'].cast<String>() ?? [];
    interviewTypesPermissions = json['interviewTypesPermissions'].cast<String>() ?? [];
    profileStatusPermissions = json['profileStatusPermissions'].cast<String>() ?? [];
    jobRolePermissions = json['jobRolePermissions'].cast<String>() ?? [];
    interviewStatusPermissions = json['interviewStatusPermissions'].cast<String>() ?? [];
    profilePermissions = json['profilePermissions'].cast<String>() ?? [];
    taskPermissions = json['taskPermissions'].cast<String>() ?? [];
    modulePermissions = json['modulePermissions'].cast<String>() ?? [];
    boardingPermissions = json['boardingPermissions'].cast<String>() ?? [];
    bugsPermissions = json['bugPermissions'].cast<String>() ?? [];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['designationPermissions'] = designationPermissions;
    data['employeeId'] = employeeId;
    data['stagesPermissions'] = developmentStatgesPermissions;
    data['sprintPermissions'] = sprintPermissions;
    data['scrumPermissions'] = scrumPermissions;
    data['employeePermissions'] = employeePermissions;
    data['projectPermissions'] = projectPermissions;
    data['rolePermissions'] = rolePermissions;
    data['technologyPermissions'] = technologyPermissions;
    data['interviewTypesPermissions'] = interviewTypesPermissions;
    data['profileStatusPermissions'] = profileStatusPermissions;
    data['interviewStatusPermissions'] = interviewStatusPermissions;
    data['jobRolePermissions'] = jobRolePermissions;
    data['profilePermissions'] = profilePermissions;
    data['taskPermissions'] = taskPermissions;
    data['modulePermissions'] = modulePermissions;
    data['boardingPermissions'] = boardingPermissions;
    data['bugPermissions'] = bugsPermissions;
    return data;
  }
}
