class ScrumTeamwiseListResponseModel {
  int? responseStatus;
  String? result;
  List<ScrumData>? scrumData;

  ScrumTeamwiseListResponseModel(
      {this.responseStatus, this.result, this.scrumData});

  ScrumTeamwiseListResponseModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['scrumData'] != null) {
      scrumData = <ScrumData>[];
      json['scrumData'].forEach((v) {
        scrumData!.add(new ScrumData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.scrumData != null) {
      data['scrumData'] = this.scrumData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScrumData {
  String? heading;
  String? sortingDate;
  List<TasksData>? tasksData;

  ScrumData({this.heading, this.sortingDate, this.tasksData});

  ScrumData.fromJson(Map<String, dynamic> json) {
    heading = json['heading'];
    sortingDate = json['sortingDate'];
    if (json['tasksData'] != null) {
      tasksData = <TasksData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(new TasksData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['heading'] = this.heading;
    data['sortingDate'] = this.sortingDate;
    if (this.tasksData != null) {
      data['tasksData'] = this.tasksData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksData {
  List<EmpTasksData>? empTasksData;
  String? employeeId;
  String? employeeName;


  TasksData({this.empTasksData, this.employeeId, this.employeeName});

  TasksData.fromJson(Map<String, dynamic> json) {
    if (json['emp_tasks_data'] != null) {
      empTasksData = <EmpTasksData>[];
      json['emp_tasks_data'].forEach((v) {
        empTasksData!.add(new EmpTasksData.fromJson(v));
      });
    }
    employeeId = json['employeeId'];
    employeeName = json['employeeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.empTasksData != null) {
      data['emp_tasks_data'] =
          this.empTasksData!.map((v) => v.toJson()).toList();
    }
    data['employeeId'] = this.employeeId;
    data['employeeName'] = this.employeeName;

    return data;
  }
}

class EmpTasksData {
  int? scrumTaskStatus;
  String? taskId;
  String? moduleId;
  String? moduleName;
  String? projectId;
  String? projectName;
  String? taskName;
  String? closeComment;
  String? taskAddedIn;
  String? bugTitle;
  int? taskStatus;
  int? estimationTime;
  int? involvedTime;
  int? percentageComplete;
  String? holdComment;


  EmpTasksData(
      {this.scrumTaskStatus, this.taskId, this.taskName,this.bugTitle, this.taskStatus,this.estimationTime,
        this.involvedTime,this.moduleId,this.projectId,this.taskAddedIn,this.percentageComplete,this.holdComment,this.moduleName,this.projectName});

  EmpTasksData.fromJson(Map<String, dynamic> json) {
    scrumTaskStatus = json['scrumTaskStatus'];
    taskId = json['taskId'];
    taskName = json['taskName'];
    taskAddedIn = json['taskAddedIn'];
    closeComment = json['closeComment'];
    bugTitle = json['bugTitle'];
    taskStatus = json['taskStatus'];
    estimationTime = json['estimationTime'] ?? 0;
    involvedTime = json['involvedTime'] ?? 0;
    moduleId = json['moduleId'];
    projectId = json['projectId'];
    percentageComplete = json['percentageComplete'] ?? 0;
    holdComment = json['holdComment'] ?? '';
    moduleName = json['moduleName'] ?? '';
    projectName = json['projectName'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['scrumTaskStatus'] = this.scrumTaskStatus;
    data['taskId'] = this.taskId;
    data['taskAddedIn'] = this.taskAddedIn;
    data['taskName'] = this.taskName;
    data['closeComment'] = this.closeComment;
    data['bugTitle'] = this.bugTitle;
    data['taskStatus'] = this.taskStatus;
    data['estimationTime'] = this.estimationTime;
    data['involvedTime'] = this.involvedTime;
    data['moduleId'] = this.moduleId;
    data['projectId'] = this.projectId;
    data['percentageComplete'] = this.percentageComplete;
    data['holdComment'] = this.holdComment;
    data['moduleName'] = this.moduleName;
    data['projectName'] = this.projectName;
    return data;
  }
}