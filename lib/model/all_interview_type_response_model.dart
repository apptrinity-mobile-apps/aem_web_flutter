class AllInterviewTypeResponseModel {
  List<InterviewTypesList>? interviewTypesList;
  int? responseStatus;
  String? result;

  AllInterviewTypeResponseModel({this.interviewTypesList, this.responseStatus, this.result});

  AllInterviewTypeResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['interviewTypesList'] != null) {
      interviewTypesList = <InterviewTypesList>[];
      json['interviewTypesList'].forEach((v) {
        interviewTypesList!.add(InterviewTypesList.fromJson(v));
      });
    } else {
      interviewTypesList = <InterviewTypesList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (interviewTypesList != null) {
      data['interviewTypesList'] = interviewTypesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class InterviewTypesList {
  String? createdBy;
  String? createdOn;
  String? id;
  String? interviewType;
  int? status;
  bool? systemTestRequired;
  bool? mailSent;
  String? mailContent;
  String? mailSubject;

  InterviewTypesList(
      {this.createdBy,
      this.createdOn,
      this.id,
      this.interviewType,
      this.status,
      this.systemTestRequired,
      this.mailSent,
      this.mailContent,
      this.mailSubject});

  InterviewTypesList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    interviewType = json['interviewType'] ?? "";
    status = json['status'] ?? 0;
    systemTestRequired = json['systemTestRequired'] ?? false;
    mailSent = json['mailSent'] ?? false;
    mailContent = json['mailContent'] ?? "";
    mailSubject = json['mailSubject'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['interviewType'] = interviewType;
    data['status'] = status;
    data['systemTestRequired'] = systemTestRequired;
    data['mailSent'] = mailSent;
    data['mailContent'] = mailContent;
    data['mailSubject'] = mailSubject;
    return data;
  }
}
