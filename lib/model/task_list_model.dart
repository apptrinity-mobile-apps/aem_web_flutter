class ModuleBasedTaskListResponse {
  int? responseStatus;
  String? result;
  List<TasksData>? tasksData;

  ModuleBasedTaskListResponse({this.responseStatus, this.result, this.tasksData});

  ModuleBasedTaskListResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['tasksData'] != null) {
      tasksData = <TasksData>[];
      json['tasksData'].forEach((v) {
        tasksData!.add(new TasksData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.tasksData != null) {
      data['tasksData'] = this.tasksData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksData {
  String? addedBy;
  List<AssignedTo>? assignedTo;
  String? createdBy;
  String? createdOn;
  String? description;
  String? endDate;
  int? estimationTime;
  String? id;
  int? involvedTime;
  String? moduleId;
  String? name;
  String? notes;

  String? projectId;
  String? startDate;
  int? status;

  TasksData(
      {this.addedBy,
        this.assignedTo,
        this.createdBy,
        this.createdOn,
        this.description,
        this.endDate,
        this.estimationTime,
        this.id,
        this.involvedTime,
        this.moduleId,
        this.name,
        this.notes,

        this.projectId,
        this.startDate,
        this.status});

  TasksData.fromJson(Map<String, dynamic> json) {
    addedBy = json['addedBy'];
    if (json['assignedTo'] != null) {
      assignedTo = <AssignedTo>[];
      json['assignedTo'].forEach((v) {
        assignedTo!.add(new AssignedTo.fromJson(v));
      });
    }
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    description = json['description'];
    endDate = json['endDate'];
    estimationTime = json['estimationTime'];
    id = json['id'];
    involvedTime = json['involvedTime'];
    moduleId = json['moduleId'];
    name = json['name'];
    notes = json['notes'];

    projectId = json['projectId'];
    startDate = json['startDate'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addedBy'] = this.addedBy;
    if (this.assignedTo != null) {
      data['assignedTo'] = this.assignedTo!.map((v) => v.toJson()).toList();
    }
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['description'] = this.description;
    data['endDate'] = this.endDate;
    data['estimationTime'] = this.estimationTime;
    data['id'] = this.id;
    data['involvedTime'] = this.involvedTime;
    data['moduleId'] = this.moduleId;
    data['name'] = this.name;
    data['notes'] = this.notes;

    data['projectId'] = this.projectId;
    data['startDate'] = this.startDate;
    data['status'] = this.status;
    return data;
  }
}

class AssignedTo {
  String? employeeId;
  String? name;

  AssignedTo({this.employeeId, this.name});

  AssignedTo.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeId'] = this.employeeId;
    data['name'] = this.name;
    return data;
  }
}