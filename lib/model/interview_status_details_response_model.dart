class InterviewStatusDetailsResponseModel {
  InterviewStatusDetails? interviewStatusDetails;
  int? responseStatus;
  String? result;

  InterviewStatusDetailsResponseModel({this.interviewStatusDetails, this.responseStatus, this.result});

  InterviewStatusDetailsResponseModel.fromJson(Map<String, dynamic> json) {
    interviewStatusDetails = json['interviewStatusDetails'] != null ? InterviewStatusDetails.fromJson(json['interviewStatusDetails']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (interviewStatusDetails != null) {
      data['interviewStatusDetails'] = interviewStatusDetails!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class InterviewStatusDetails {
  String? createdBy;
  String? createdOn;
  String? id;
  int? status;
  String? statusName;

  InterviewStatusDetails({this.createdBy, this.createdOn, this.id, this.status, this.statusName});

  InterviewStatusDetails.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    status = json['status'] ?? 0;
    statusName = json['statusName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['status'] = status;
    data['statusName'] = statusName;
    return data;
  }
}
