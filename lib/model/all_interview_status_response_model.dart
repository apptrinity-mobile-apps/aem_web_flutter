class AllInterviewStatusResponseModel {
  List<InterviewStatusList>? interviewStatusList;
  int? responseStatus;
  String? result;

  AllInterviewStatusResponseModel({this.interviewStatusList, this.responseStatus, this.result});

  AllInterviewStatusResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['interviewStatusList'] != null) {
      interviewStatusList = <InterviewStatusList>[];
      json['interviewStatusList'].forEach((v) {
        interviewStatusList!.add(InterviewStatusList.fromJson(v));
      });
    } else {
      interviewStatusList = <InterviewStatusList>[];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (interviewStatusList != null) {
      data['interviewStatusList'] = interviewStatusList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}
class InterviewStatusList {
  String? createdBy;
  String? createdOn;
  String? id;
  int? status;
  String? statusName;
  InterviewStatusList({this.createdBy, this.createdOn, this.id, this.status, this.statusName});
  InterviewStatusList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    id = json['id'] ?? "";
    status = json['status'] ?? 0;
    statusName = json['statusName'] ?? "";
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['id'] = id;
    data['status'] = status;
    data['statusName'] = statusName;
    return data;
  }
}
