class MyScrumResponse {
  int? responseStatus;
  String? result;

  List<ScrumData>? scrumData;

  MyScrumResponse({this.responseStatus, this.result, this.scrumData});

  MyScrumResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['scrumData'] != null) {
      scrumData = <ScrumData>[];
      json['scrumData'].forEach((v) {
        scrumData!.add(ScrumData.fromJson(v));
      });
    } else {
      scrumData = <ScrumData>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;

    if (scrumData != null) {
      data['scrumData'] = scrumData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScrumData {
  List<TasksList>? othertasksList;
  String? heading;
  String? comment;
  List<TasksList>? tasksList;

  ScrumData({this.othertasksList, this.heading, this.tasksList,this.comment});

  ScrumData.fromJson(Map<String, dynamic> json) {
    if (json['OthertasksList'] != null) {
      othertasksList = <TasksList>[];
      json['OthertasksList'].forEach((v) {
        othertasksList!.add(TasksList.fromJson(v));
      });
    } else {
      othertasksList = <TasksList>[];
    }
    heading = json['heading'];
    comment = json['comment'] ?? "";
    if (json['tasksList'] != null) {
      tasksList = <TasksList>[];
      json['tasksList'].forEach((v) {
        tasksList!.add(TasksList.fromJson(v));
      });
    } else {
      tasksList = <TasksList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (othertasksList != null) {
      data['OthertasksList'] = othertasksList!.map((v) => v.toJson()).toList();
    }
    data['heading'] = heading;
    data['comment'] = comment;
    if (tasksList != null) {
      data['tasksList'] = tasksList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksList {
  String? bugId;
  String? bugStatus;
  String? bugTitle;
  String? createdOn;
  int? estimationTime;
  int? involvedTime;
  String? moduleId;
  String? moduleName;
  String? projectId;
  String? projectName;
  String? scrumDate;
  String? taskId;
  String? taskName;
  String? closeComment;
  int? taskStatus;
  String? taskType;
  int? percentageComplete;
  String? holdComment;
  String? taskAddType;


  TasksList(
      {this.bugId,
      this.bugStatus,
      this.bugTitle,
      this.createdOn,
      this.estimationTime,
      this.involvedTime,
      this.moduleId,
      this.moduleName,
      this.projectId,
      this.projectName,
      this.scrumDate,
      this.taskId,
      this.taskName,
        this.closeComment,
      this.taskStatus,
      this.taskType,
      this.percentageComplete,
      this.holdComment,
      this.taskAddType});

  TasksList.fromJson(Map<String, dynamic> json) {
    bugId = json['bugId'] ?? "";
    bugStatus = json['bugStatus'] ?? "";
    bugTitle = json['bugTitle'] ?? "";
    createdOn = json['createdOn'];
    estimationTime = json['estimationTime'] ?? 0;
    involvedTime = json['involvedTime'] ?? 0;
    moduleId = json['moduleId'] ?? "";
    moduleName = json['moduleName'] ?? "";
    projectId = json['projectId'] ?? "";
    projectName = json['projectName'] ?? "";
    scrumDate = json['scrumDate'] ?? "";
    taskId = json['taskId'] ?? "";
    taskName = json['taskName'] ?? "";
    closeComment = json['closeComment'] ?? "";
    taskStatus = json['taskStatus'] ?? 0;
    taskType = json['taskType'] ?? "";
    percentageComplete = json['percentageComplete'] ?? 0;
    holdComment = json['holdComment'] ?? "";
    taskAddType = json['taskAddType'] ?? "";
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bugId'] = bugId;
    data['bugStatus'] = bugStatus;
    data['bugTitle'] = bugTitle;
    data['createdOn'] = createdOn;
    data['estimationTime'] = estimationTime;
    data['involvedTime'] = involvedTime;
    data['moduleId'] = moduleId;
    data['moduleName'] = moduleName;
    data['projectId'] = projectId;
    data['projectName'] = projectName;
    data['scrumDate'] = scrumDate;
    data['taskId'] = taskId;
    data['taskName'] = taskName;
    data['closeComment'] = closeComment;
    data['taskStatus'] = taskStatus;
    data['taskType'] = taskType;
    data['percentageComplete'] = percentageComplete;
    data['holdComment'] = holdComment;
    data['taskAddType'] = taskAddType;
    return data;
  }
}
