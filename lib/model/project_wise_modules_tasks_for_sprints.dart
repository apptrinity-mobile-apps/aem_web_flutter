class ProjectBasedModulesTasksforSprints {
  List<ModulesList>? modulesList;
  int? responseStatus;
  String? result;

  ProjectBasedModulesTasksforSprints(
      {this.modulesList, this.responseStatus, this.result});

  ProjectBasedModulesTasksforSprints.fromJson(Map<String, dynamic> json) {
    if (json['modulesList'] != null) {
      modulesList = <ModulesList>[];
      json['modulesList'].forEach((v) {
        modulesList!.add(new ModulesList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modulesList != null) {
      data['modulesList'] = this.modulesList!.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ModulesList {
  String? id;
  String? name;
  List<TasksList>? tasksList;

  ModulesList({this.id, this.name, this.tasksList});

  ModulesList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['tasksList'] != null) {
      tasksList = <TasksList>[];
      json['tasksList'].forEach((v) {
        tasksList!.add(new TasksList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.tasksList != null) {
      data['tasksList'] = this.tasksList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TasksList {
  String? id;
  String? name;
  bool? taskSprintFlag;

  TasksList({this.id, this.name, this.taskSprintFlag});

  TasksList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    taskSprintFlag = json['taskSprintFlag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['taskSprintFlag'] = this.taskSprintFlag;
    return data;
  }
}