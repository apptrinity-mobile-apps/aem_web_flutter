class ProjectDetailsResponseModel {
  ProjectDict? projectDict;
  int? responseStatus;
  String? result;

  ProjectDetailsResponseModel({this.projectDict, this.responseStatus, this.result});

  ProjectDetailsResponseModel.fromJson(Map<String, dynamic> json) {
    projectDict = json['project_dict'] != null ? ProjectDict.fromJson(json['project_dict']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (projectDict != null) {
      data['project_dict'] = projectDict!.toJson();
    }
    data['responseStatus'] = responseStatus;
    data['result'] = result;
    return data;
  }
}

class ProjectDict {
  int? closeCount;
  int? completeCount;
  String? createdBy;
  String? createdOn;
  int? deleteCount;
  String? description;
  String? id;
  String? name;
  int? newCount;
  int? ongoingCount;
  int? reopenCount;
  int? overDueCount;
  int? bugsCount;
  int? sprintsCount;
  int? totalEstimateHoursCount;
  int? status;
  int? todosCount;
  List<ProjectEmployees>? employees;
  List<Logslist>? logslist;

  ProjectDict.fromJson(Map<String, dynamic> json) {
    closeCount = json['closeCount'] ?? 0;
    completeCount = json['completeCount'] ?? 0;
    createdBy = json['createdBy'] ?? "";
    createdOn = json['createdOn'] ?? "";
    deleteCount = json['deleteCount'] ?? 0;
    sprintsCount = json['sprintsCount'] ?? 0;
    description = json['description'] ?? "";
    id = json['id'] ?? "";
    name = json['name'] ?? "";
    newCount = json['newCount'] ?? 0;
    ongoingCount = json['ongoingCount'] ?? 0;
    reopenCount = json['reopenCount'] ?? 0;
    overDueCount = json['overDueCount'] ?? 0;
    bugsCount = json['bugsCount'] ?? 0;
    status = json['status'] ?? 0;
    todosCount = json['todosCount'] ?? 0;
    totalEstimateHoursCount = json['totalEstimateHoursCount'] ?? 0;
    if (json['employees'] != null) {
      employees = <ProjectEmployees>[];
      json['employees'].forEach((v) {
        employees!.add(ProjectEmployees.fromJson(v));
      });
    } else {
      employees = <ProjectEmployees>[];
    }
    if (json['logslist'] != null) {
      logslist = <Logslist>[];
      json['logslist'].forEach((v) {
        logslist!.add(Logslist.fromJson(v));
      });
    } else {
      logslist = <Logslist>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['closeCount'] = closeCount;
    data['completeCount'] = completeCount;
    data['createdBy'] = createdBy;
    data['createdOn'] = createdOn;
    data['deleteCount'] = deleteCount;
    data['description'] = description;
    data['id'] = id;
    data['name'] = name;
    data['newCount'] = newCount;
    data['ongoingCount'] = ongoingCount;
    data['reopenCount'] = reopenCount;
    data['overDueCount'] = overDueCount;
    data['bugsCount'] = bugsCount;
    data['status'] = status;
    data['todosCount'] = todosCount;
    data['totalEstimateHoursCount'] = totalEstimateHoursCount;
    if (employees != null) {
      data['employees'] = employees!.map((v) => v.toJson()).toList();
    }
    if (logslist != null) {
      data['logslist'] = logslist!.map((v) => v.toJson()).toList();
    }
    return data;
  }

  ProjectDict(
      {this.closeCount,
      this.completeCount,
      this.createdBy,
      this.createdOn,
      this.deleteCount,
      this.description,
      this.id,
      this.name,
      this.newCount,
      this.ongoingCount,
      this.reopenCount,
      this.overDueCount,
      this.status,
      this.todosCount,
      this.totalEstimateHoursCount,
      this.logslist,
      this.employees});
}

class Logslist {
  String? day;
  String? dayForSorting;
  List<LogsData>? logsData;

  Logslist({this.day, this.dayForSorting, this.logsData});

  Logslist.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    dayForSorting = json['day_for_sorting'];
    if (json['logs_data'] != null) {
      logsData = <LogsData>[];
      json['logs_data'].forEach((v) {
        logsData!.add(LogsData.fromJson(v));
      });
    } else {
      logsData = <LogsData>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['day'] = day;
    data['day_for_sorting'] = dayForSorting;
    if (logsData != null) {
      data['logs_data'] = logsData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogsData {
  String? bugName;
  String? bugId;
  String? createdOn;
  String? description;
  String? employeeId;
  String? employeeName;
  String? logType;
  String? moduleCreatedBy;
  String? moduleCreatedByName;
  String? moduleEndDate;
  String? moduleId;
  String? moduleName;
  String? moduleStartDate;
  String? projectId;
  String? projectName;
  String? taskCreatedBy;
  String? taskCreatedByName;
  String? taskId;
  String? taskLastDate;
  String? taskName;
  String? taskStartDate;
  int? taskStatus;
  int? estimationTime;
  int? involvedTime;


  LogsData(
      {
        this.bugName,
        this.bugId,
        this.createdOn,
      this.description,
      this.employeeId,
      this.employeeName,
      this.logType,
      this.moduleCreatedBy,
      this.moduleCreatedByName,
      this.moduleEndDate,
      this.moduleId,
      this.moduleName,
      this.moduleStartDate,
      this.projectId,
      this.projectName,
      this.taskCreatedBy,
      this.taskCreatedByName,
      this.taskId,
      this.taskLastDate,
      this.taskName,
      this.taskStartDate,
      this.taskStatus,
      this.estimationTime,
      this.involvedTime});

  LogsData.fromJson(Map<String, dynamic> json) {
    bugName = json['bugName'] ?? "";
    bugId = json['bugId'] ?? "";
    createdOn = json['createdOn'] ?? "";
    description = json['description'] ?? "";
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
    logType = json['logType'] ?? "";
    moduleCreatedBy = json['moduleCreatedBy'] ?? "";
    moduleCreatedByName = json['moduleCreatedByName'] ?? "";
    moduleEndDate = json['moduleEndDate'] ?? "";
    moduleId = json['moduleId'] ?? "";
    moduleName = json['moduleName'] ?? "";
    moduleStartDate = json['moduleStartDate'] ?? "";
    projectId = json['projectId'] ?? "";
    projectName = json['projectName'] ?? "";
    taskCreatedBy = json['taskCreatedBy'] ?? "";
    taskCreatedByName = json['taskCreatedByName'] ?? "";
    taskId = json['taskId'] ?? "";
    taskLastDate = json['taskLastDate'] ?? "";
    taskName = json['taskName'] ?? "";
    taskStartDate = json['taskStartDate'] ?? "";
    taskStatus = json['taskStatus'] ?? 0;
    estimationTime = json['estimationTime'] ?? 0;
    involvedTime = json['involvedTime'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bugName'] = bugName;
    data['bugId'] = bugId;
    data['createdOn'] = createdOn;
    data['description'] = description;
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    data['logType'] = logType;
    data['moduleCreatedBy'] = moduleCreatedBy;
    data['moduleCreatedByName'] = moduleCreatedByName;
    data['moduleEndDate'] = moduleEndDate;
    data['moduleId'] = moduleId;
    data['moduleName'] = moduleName;
    data['moduleStartDate'] = moduleStartDate;
    data['projectId'] = projectId;
    data['projectName'] = projectName;
    data['taskCreatedBy'] = taskCreatedBy;
    data['taskCreatedByName'] = taskCreatedByName;
    data['taskId'] = taskId;
    data['taskLastDate'] = taskLastDate;
    data['taskName'] = taskName;
    data['taskStartDate'] = taskStartDate;
    data['taskStatus'] = taskStatus;
    data['estimationTime'] = estimationTime;
    data['involvedTime'] = involvedTime;
    return data;
  }
}

class ProjectEmployees {
  String? employeeId;
  String? employeeName;

  ProjectEmployees({this.employeeId, this.employeeName});

  ProjectEmployees.fromJson(Map<String, dynamic> json) {
    employeeId = json['employeeId'] ?? "";
    employeeName = json['employeeName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['employeeId'] = employeeId;
    data['employeeName'] = employeeName;
    return data;
  }
}
