class ScrumCountsResponse {
  int? responseStatus;
  String? result;

  List<ScrumData>? scrumData;

  ScrumCountsResponse({this.responseStatus, this.result, this.scrumData});

  ScrumCountsResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];

    if (json['scrumData'] != null) {
      scrumData = <ScrumData>[];
      json['scrumData'].forEach((v) {
        scrumData!.add(ScrumData.fromJson(v));
      });
    } else {
      scrumData = <ScrumData>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['responseStatus'] = responseStatus;
    data['result'] = result;

    if (scrumData != null) {
      data['scrumData'] = scrumData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScrumData {
  String? heading;
  TasksCount? tasksCount;

  ScrumData({this.heading, this.tasksCount});

  ScrumData.fromJson(Map<String, dynamic> json) {
    heading = json['heading']??"";
    tasksCount = json['tasksCount'] != null ? TasksCount.fromJson(json['tasksCount']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['heading'] = heading;
    if (tasksCount != null) {
      data['tasksCount'] = tasksCount!.toJson();
    }
    return data;
  }
}

class TasksCount {
  int? completedTasksCount;
  int? newTasksCount;
  int? pendingTasksCount;

  TasksCount({this.completedTasksCount, this.newTasksCount, this.pendingTasksCount});

  TasksCount.fromJson(Map<String, dynamic> json) {
    completedTasksCount = json['completed_tasks_count']??0;
    newTasksCount = json['new_tasks_count']??0;
    pendingTasksCount = json['pending_tasks_count']??0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['completed_tasks_count'] = completedTasksCount;
    data['new_tasks_count'] = newTasksCount;
    data['pending_tasks_count'] = pendingTasksCount;
    return data;
  }
}
