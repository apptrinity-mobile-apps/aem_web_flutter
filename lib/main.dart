import 'package:aem/global_view_model/global_notifier.dart';
import 'package:aem/screens/chat/view_model/chat_notifier.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/dashboard/view_model/dashboard_notifier.dart';
import 'package:aem/screens/designations/view_model/designation_notifier.dart';
import 'package:aem/screens/employees/view_model/employees_notifier.dart';
import 'package:aem/screens/interview_status/view_model/interview_status_notifier.dart';
import 'package:aem/screens/interview_types/view_model/interview_types_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/my_profile/view_model/my_profile_notifier.dart';
import 'package:aem/screens/profile_status/view_model/profile_status_notifier.dart';
import 'package:aem/screens/profiles/view_model/profile_notifier.dart';
import 'package:aem/screens/project_details/ui/todos_screen.dart';
import 'package:aem/screens/project_details/ui/view_project_screen.dart';
import 'package:aem/screens/project_details/ui/view_task_screen.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/screens/projects/view_model/projects_notifier.dart';
import 'package:aem/screens/roles/view_model/roles_notifier.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/screens/splash/splash_screen.dart';
import 'package:aem/screens/technologies/view_model/technology_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/navigation_service.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:url_strategy/url_strategy.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

// https://rodydavis.medium.com/how-to-send-push-notifications-on-flutter-web-fcm-b3e64f1e2b76
// https://stackoverflow.com/questions/63799227/fcm-gettoken-failed-to-register-a-serviceworker-for-scope-error-flutter-web/63800226#63800226
// https://github.com/firebase/flutterfire/issues/8931 - notifications error

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // to remove # from urls
  setPathUrlStrategy();
  initializeDateFormatting('en');
  // await Firebase.initializeApp();

  firebaseNotifications();
  runApp(const MyApp());
}

firebaseNotifications() async {
  FirebaseMessaging.instance
      .requestPermission(sound: true, alert: true, criticalAlert: false, provisional: false, announcement: false, badge: false, carPlay: false)
      .then((value) async {
    if (value.authorizationStatus == AuthorizationStatus.authorized) {
      debugPrint('User granted permission');
      await FirebaseMessaging.instance.getToken().then((value) async {
        if (value != null) {
          final LoginNotifier userViewModel = Provider.of<LoginNotifier>(NavigationService.navigatorKey.currentContext!, listen: false);
          var employeeId = userViewModel.employeeId.toString();
          // saving device token for notifications
          Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).saveToken(value);
          if (employeeId != "" || employeeId != "null") {
            // updating token when user authorizes permission
            await Provider.of<LoginNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).updateTokenAPI(employeeId, value);
          }
        }
      });
    } else if (value.authorizationStatus == AuthorizationStatus.provisional) {
      debugPrint('User granted provisional permission');
    } else {
      debugPrint('User declined or has not accepted permission');
    }
  }).catchError((obj, stacktrace) {
    debugPrint("$obj === $stacktrace");
  });
  FirebaseMessaging.onMessage.listen(_firebaseMessagingBackgroundHandler);
  FirebaseMessaging.onMessageOpenedApp.listen(_firebaseMessagingBackgroundHandler);
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  debugPrint('Firebase _firebaseMessagingBackgroundHandler');
  if (message.notification != null) {
    debugPrint('Notification: title- ${message.notification!.title} body- ${message.notification!.body} data- ${message.data}');
    ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!).showMaterialBanner(
      MaterialBanner(
        elevation: 3,
        padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
        leading: const Icon(Icons.notifications_active_rounded),
        backgroundColor: searchGoButtonBg,
        forceActionsBelow: true,
        content: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              message.notification!.title!,
              style: textFieldTodoCommentHintStyle.copyWith(letterSpacing: 0.5, fontWeight: FontWeight.w500, fontSize: 15),
            ),
            Text(
              message.notification!.body!,
              maxLines: 3,
              style: textFieldTodoCommentHintStyle.copyWith(letterSpacing: 0.5, fontWeight: FontWeight.w400, fontSize: 13),
            ),
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!).hideCurrentMaterialBanner();
              if (message.data.isNotEmpty) {
                if (message.data['type'] == 'add_module') {
                  // expanding projects
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 0);
                  // navigating to todos screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) =>
                          TodosScreen(projectId: message.data['projectId'], projectName: message.data['projectName']),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                } else if (message.data['type'] == 'add_project') {
                  // expanding projects
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 0);
                  // navigating to project details screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => ViewProjectDetailedScreen(projectId: message.data['projectId']),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                } else if (message.data['type'] == 'add_task') {
                  // expanding projects
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 0);
                  // navigating to tasks details screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) =>
                          ViewTaskScreen(projectId: message.data['projectId'], moduleId: message.data['moduleId'], taskId: message.data['taskId']),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                } else if (message.data['type'] == 'add_bug') {
                  // expanding projects
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 0);
                  // navigating to task details screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) =>
                          ViewTaskScreen(projectId: message.data['projectId'], moduleId: message.data['moduleId'], taskId: message.data['taskId']),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                } else if (message.data['type'] == 'bug_status_update') {
                  // expanding projects
                  // message.data['bugId'] for bugId
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 0);
                  // navigating to task details screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) =>
                          ViewTaskScreen(projectId: message.data['projectId'], moduleId: message.data['moduleId'], taskId: message.data['taskId']),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                } else if (message.data['type'] == 'add_scrum') {
                  // expanding scrums
                  Provider.of<GlobalNotifier>(NavigationService.navigatorKey.currentContext!, listen: false).setExpandedState(true, 1);
                  // navigating to my scrum screen
                  Navigator.push(
                    NavigationService.navigatorKey.currentContext!,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => HomeScreen(menu: RouteNames.scrumMyScrumRoute),
                      transitionDuration: const Duration(seconds: 0),
                      reverseTransitionDuration: const Duration(seconds: 0),
                    ),
                  );
                }
              }
            },
            child: Text(
              view,
              style: logoutButtonHeader.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!).hideCurrentMaterialBanner();
            },
            child: Text(
              close,
              style: logoutButtonHeader.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GlobalNotifier()),
        ChangeNotifierProvider(create: (context) => LoginNotifier()),
        ChangeNotifierProvider(create: (context) => DashBoardNotifier()),
        ChangeNotifierProvider(create: (context) => EmployeeNotifier()),
        ChangeNotifierProvider(create: (context) => TechnologyNotifier()),
        ChangeNotifierProvider(create: (context) => ProjectNotifier()),
        ChangeNotifierProvider(create: (context) => DesignationNotifier()),
        ChangeNotifierProvider(create: (context) => RolesNotifier()),
        ChangeNotifierProvider(create: (context) => ProfileNotifier()),
        ChangeNotifierProvider(create: (context) => ProjectDetailsNotifier()),
        ChangeNotifierProvider(create: (context) => ProfileStatusNotifier()),
        ChangeNotifierProvider(create: (context) => InterviewStatusNotifier()),
        ChangeNotifierProvider(create: (context) => InterviewTypeNotifier()),
        ChangeNotifierProvider(create: (context) => MyProfileNotifier()),
        ChangeNotifierProvider(create: (context) => ScrumNotifier()),
        ChangeNotifierProvider(create: (context) => ChatNotifier()),
      ],
      child: MaterialApp(
        navigatorKey: NavigationService.navigatorKey,
        title: appName,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
          scrollbarTheme: ScrollbarThemeData(
            // showTrackOnHover: true,
            thumbVisibility: MaterialStateProperty.all(false),
            interactive: true,
            mainAxisMargin: 5,
            thickness: MaterialStateProperty.all(5),
            trackBorderColor: MaterialStateColor.resolveWith((states) => buttonBg.withOpacity(0.1)),
            trackColor: MaterialStateColor.resolveWith((states) => buttonBg.withOpacity(0.2)),
            thumbColor: MaterialStateColor.resolveWith((states) => buttonBg.withOpacity(0.8)),
            radius: const Radius.circular(10),
          ),
        ),
        /*routes: {
          RouteNames.initialRoute: (context) => const SplashScreen(),
          RouteNames.loginRoute: (context) => const LoginPage(),
          RouteNames.homeRoute: (context) => const HomeScreen(menu: RouteNames.dashBoardRoute),
          RouteNames.addRoleHomeRoute: (context) => const AddRolesFragment(),
          RouteNames.viewProjectRoute: (context) => const ViewProjectFragment(),
        },
        initialRoute: RouteNames.initialRoute,*/
        home: const SplashScreen(),
      ),
    );
  }
}
