import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/screens/project_details/ui/sprints_screen.dart';
import 'package:aem/services/api_services.dart';
import 'package:aem/services/base_services.dart';

class Repository {
  final BaseService _service = ApiServices();



  // profile changes
  Future<dynamic> requestResetPasswordOTP(String email) async {
    dynamic response = await _service.requestResetPasswordOTP(email);
    return response;
  }

  Future<dynamic> resetPassword(String otp, String email, String newPassword, String confirmNewPassword) async {
    dynamic response = await _service.resetPassword(otp, email, newPassword, confirmNewPassword);
    return response;
  }

  Future<dynamic> changePassword(String employeeId, String password, String newPassword, String confirmNewPassword) async {
    dynamic response = await _service.changePassword(employeeId, password, newPassword, confirmNewPassword);
    return response;
  }

  Future<dynamic> loginProfileUpdate(
      String name, String employeeId, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration, String durationType) async {
    dynamic response = await _service.loginProfileUpdate(name, employeeId, phoneNumber, availabilitySchedule, duration, durationType);
    return response;
  }

  // dashboard apis
  Future<dynamic> getDashboard(String employeeId, String boardingId) async {
    dynamic response = await _service.getDashboard(employeeId, boardingId);
    return response;
  }

  Future<dynamic> employeePermissions(String employeeId) async {
    dynamic response = await _service.employeePermissions(employeeId);
    return response;
  }

  // projects details apis
  Future<dynamic> projectView(String employeeId, String projectId) async {
    dynamic response = await _service.projectView(employeeId, projectId);
    return response;
  }

  Future<dynamic> projectLogsList(String employeeId, String projectId, String searchType, String searchElement) async {
    dynamic response = await _service.projectLogsList(employeeId, projectId, searchType, searchElement);
    return response;
  }

  Future<dynamic> viewAllModules(String projectId, String createdBy) async {
    dynamic response = await _service.viewAllModules(projectId, createdBy);
    return response;
  }

  Future<dynamic> viewModule(String moduleId, String createdBy) async {
    dynamic response = await _service.viewModule(moduleId, createdBy);
    return response;
  }

  Future<dynamic> deleteModule(String moduleId, String employeeId) async {
    dynamic response = await _service.deleteModule(moduleId, employeeId);
    return response;
  }

  Future<dynamic> createModule(String name, String description, String startDate, String endDate, String notes, String projectId, String createdBy,
      List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    dynamic response =
        await _service.createModule(name, description, startDate, endDate, notes, projectId, createdBy, assignedTo, notifyTo, assignedToTester);
    return response;
  }

  Future<dynamic> updateModule(String name, String description, String startDate, String endDate, String notes, String projectId, String moduleId,
      String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    dynamic response = await _service.updateModule(
        name, description, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester);
    return response;
  }

  Future<dynamic> addCommentToModule(String comment, String moduleId, String projectId, String employeeId) async {
    dynamic response = await _service.addCommentToModule(comment, moduleId, projectId, employeeId);
    return response;
  }

  Future<dynamic> updateModuleComment(String moduleCommentId, String comment) async {
    dynamic response = await _service.updateModuleComment(moduleCommentId, comment);
    return response;
  }

  Future<dynamic> deleteModuleComment(String moduleCommentId) async {
    dynamic response = await _service.deleteModuleComment(moduleCommentId);
    return response;
  }

  // tasks apis
  Future<dynamic> getTasksBasedOnStatus(String projectId, String employeeId, String statusText) async {
    dynamic response = await _service.getTasksBasedOnStatus(projectId, employeeId, statusText);
    return response;
  }

  Future<dynamic> updateTasksBasedOnStatus(String employeeId, String taskId, String statusText, String comment,int percentageComplete) async {
    dynamic response = await _service.updateTasksBasedOnStatus(employeeId, taskId, statusText, comment,percentageComplete);
    return response;
  }

  Future<dynamic> addTask(String name, String description, String addedBy, String startDate, String endDate, String notes, String projectId,
      String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationTime) async {
    dynamic response = await _service.addTask(
        name, description, addedBy, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester,estimationTime);
    return response;
  }

  Future<dynamic> updateTask(String taskId, String name, String description, String addedBy, String startDate, String endDate, String notes,
      String projectId, String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationtime) async {
    dynamic response = await _service.updateTask(
        taskId, name, description, addedBy, startDate, endDate, notes, projectId, moduleId, createdBy, assignedTo, notifyTo, assignedToTester,estimationtime);
    return response;
  }

  Future<dynamic> viewTask(String taskId, String employeeId) async {
    dynamic response = await _service.viewTask(taskId, employeeId);
    return response;
  }

  Future<dynamic> deleteTask(String taskId, String employeeId) async {
    dynamic response = await _service.deleteTask(taskId, employeeId);
    return response;
  }

  Future<dynamic> addCommentToTask(String taskId, String comment, String moduleId, String projectId, String employeeId) async {
    dynamic response = await _service.addCommentToTask(taskId, comment, moduleId, projectId, employeeId);
    return response;
  }

  Future<dynamic> updateTaskComment(String taskCommentId, String comment) async {
    dynamic response = await _service.updateTaskComment(taskCommentId, comment);
    return response;
  }

  Future<dynamic> deleteTaskComment(String taskCommentId) async {
    dynamic response = await _service.deleteTaskComment(taskCommentId);
    return response;
  }

  // employees apis
  Future<dynamic> viewAllEmployees(String employeeId) async {
    dynamic response = await _service.viewAllEmployees(employeeId);
    return response;
  }

  Future<dynamic> createEmployee(
      String name,
      String email,
      String password,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String createdBy,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperior) async {
    dynamic response = await _service.createEmployee(name, email, password, designationId, technologyId, roleId, createdBy, phoneNumber,
        availabilitySchedule, duration, durationType, assignableEmployees, isSuperior);
    return response;
  }

  Future<dynamic> deleteEmployee(String loginEmployeeId, String employeeId) async {
    dynamic response = await _service.deleteEmployee(loginEmployeeId, employeeId);
    return response;
  }

  Future<dynamic> updateEmployee(
      String name,
      String email,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String employeeId,
      String userId,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperior) async {
    dynamic response = await _service.updateEmployee(name, email, designationId, technologyId, roleId, employeeId, userId, phoneNumber,
        availabilitySchedule, duration, durationType, assignableEmployees, isSuperior);
    return response;
  }

  Future<dynamic> viewEmployee(String loginEmployeeId, String employeeId) async {
    dynamic response = await _service.viewEmployee(loginEmployeeId, employeeId);
    return response;
  }

  Future<dynamic> activateEmployee(String employeeId) async {
    dynamic response = await _service.activateEmployee(employeeId);
    return response;
  }

  Future<dynamic> deActivateEmployee(String employeeId) async {
    dynamic response = await _service.deActivateEmployee(employeeId);
    return response;
  }

  Future<dynamic> viewDayBasedEmployeeSchedule(String employeeId, String selectedDate) async {
    dynamic response = await _service.viewDayBasedEmployeeSchedule(employeeId, selectedDate);
    return response;
  }

  // technologies apis
  Future<dynamic> viewAllTechnologies(String employeeId) async {
    dynamic response = await _service.viewAllTechnologies(employeeId);
    return response;
  }

  Future<dynamic> createTechnology(String createdBy, String name, String image) async {
    dynamic response = await _service.createTechnology(createdBy, name, image);
    return response;
  }

  Future<dynamic> updateTechnology(String technologyId, String name, String image) async {
    dynamic response = await _service.updateTechnology(technologyId, name, image);
    return response;
  }

  Future<dynamic> deleteTechnology(String technologyId) async {
    dynamic response = await _service.deleteTechnology(technologyId);
    return response;
  }

  Future<dynamic> viewTechnology(String technologyId) async {
    dynamic response = await _service.viewTechnology(technologyId);
    return response;
  }

  // projects apis
  Future<dynamic> getAllProjects(String employeeId, String from) async {
    dynamic response = await _service.getAllProjects(employeeId, from);
    return response;
  }

  Future<dynamic> createProject(
      String name, String description, List<String> employees, String createdBy, String boardingId, List<String> technologyId) async {
    dynamic response = await _service.createProject(name, description, employees, createdBy, boardingId, technologyId);
    return response;
  }

  Future<dynamic> updateProject(
      String name, String description, List<String> employees, String projectId, String boardingId, List<String> technologyId) async {
    dynamic response = await _service.updateProject(name, description, employees, projectId, boardingId, technologyId);
    return response;
  }

  Future<dynamic> deleteProject(String projectId, String employeeId) async {
    dynamic response = await _service.deleteProject(projectId, employeeId);
    return response;
  }

  Future<dynamic> viewProject(String projectId) async {
    dynamic response = await _service.viewProject(projectId);
    return response;
  }

  Future<dynamic> activateProject(String projectId) async {
    dynamic response = await _service.activateProject(projectId);
    return response;
  }

  Future<dynamic> deActivateProject(String projectId) async {
    dynamic response = await _service.deActivateProject(projectId);
    return response;
  }

  // designations apis
  Future<dynamic> viewAllDesignations(String employeeId) async {
    dynamic response = await _service.viewAllDesignations(employeeId);
    return response;
  }

  Future<dynamic> createDesignation(String name, String createdBy) async {
    dynamic response = await _service.createDesignation(name, createdBy);
    return response;
  }

  Future<dynamic> updateDesignation(String designationId, String name) async {
    dynamic response = await _service.updateDesignation(designationId, name);
    return response;
  }

  Future<dynamic> deleteDesignation(String designationId) async {
    dynamic response = await _service.deleteDesignation(designationId);
    return response;
  }

  Future<dynamic> viewDesignation(String designationId) async {
    dynamic response = await _service.viewDesignation(designationId);
    return response;
  }

  // Roles apis
  Future<dynamic> viewAllRoles(String employeeId) async {
    dynamic response = await _service.viewAllRoles(employeeId);
    return response;
  }

  Future<dynamic> createRole(
      String employeeId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions) async {
    dynamic response = await _service.createRole(
        employeeId,
        roleName,
        dashboardPermissions,
        sprintPermissions,
        scrumPermissions,
        employeePermissions,
        projectPermissions,
        boardingPermissions,
        designationPermissions,
        technologyPermissions,
        rolePermissions,
        interviewTypesPermissions,
        profileStatusPermissions,
        interviewStatusPermissions,
        jobRolePermissions,
        profilePermissions,
        taskPermissions,
        bugPermissions,
        modulePermissions,
    stagespermissions);
    return response;
  }

  Future<dynamic> updateRole(
      String roleId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions) async {
    dynamic response = await _service.updateRole(
        roleId,
        roleName,
        dashboardPermissions,
        sprintPermissions,
        scrumPermissions,
        employeePermissions,
        projectPermissions,
        boardingPermissions,
        designationPermissions,
        technologyPermissions,
        rolePermissions,
        interviewTypesPermissions,
        profileStatusPermissions,
        interviewStatusPermissions,
        jobRolePermissions,
        profilePermissions,
        taskPermissions,
        bugPermissions,
        modulePermissions,
    stagespermissions);
    return response;
  }

  Future<dynamic> deleteRole(String roleId) async {
    dynamic response = await _service.deleteRole(roleId);
    return response;
  }

  Future<dynamic> viewRole(String roleId) async {
    dynamic response = await _service.viewRole(roleId);
    return response;
  }

  // profiles apis


  Future<dynamic> updateProfile(
      String createdBy,
      String profileId,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    dynamic response = await _service.updateProfile(createdBy, profileId, name, phoneNumber, careerStartedOn, experienceStatus, employmentStatus,
        email, mainTechnology, technology, superiorEmployees, resume, fileExtension, source, resumeFileName, jobRoleId);
    return response;
  }

  Future<dynamic> deleteProfile(String profileId) async {
    dynamic response = await _service.deleteProfile(profileId);
    return response;
  }

  Future<dynamic> viewProfile(String profileId) async {
    dynamic response = await _service.viewProfile(profileId);
    return response;
  }

  Future<dynamic> addProfile(
      String createdBy,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    dynamic response = await _service.addProfile(createdBy, name, phoneNumber, careerStartedOn, experienceStatus, employmentStatus, email,
        mainTechnology, technology, superiorEmployees, resume, fileExtension, source, resumeFileName, jobRoleId);
    return response;
  }

  Future<dynamic> addProfileComment(String employeeId, String profileId, String comment) async {
    dynamic response = await _service.addProfileComment(employeeId, profileId, comment);
    return response;
  }

  Future<dynamic> updateProfileStatus(String employeeId, String profileId, String employmentStatus, String comment) async {
    dynamic response = await _service.updateProfileStatus(employeeId, profileId, employmentStatus, comment);
    return response;
  }

  Future<dynamic> requestResume(String profileId) async {
    dynamic response = await _service.requestResume(profileId);
    return response;
  }

  Future<dynamic> addSystemTest(
      String employeeId, String scheduledBy, String profileId, String comment, String systemTestTime, List<String> supportingDocs) async {
    dynamic response = await _service.addSystemTest(employeeId, scheduledBy, profileId, comment, systemTestTime, supportingDocs);
    return response;
  }

  // interview schedule


  Future<dynamic> updateInterviewSchedule(String employeeId, String scheduledId, String interviewStatusId, String comments) async {
    dynamic response = await _service.updateInterviewSchedule(employeeId, scheduledId, interviewStatusId, comments);
    return response;
  }

  // profile status api's
  Future<dynamic> viewAllProfileStatuses(String createdBy) async {
    dynamic response = await _service.viewAllProfileStatuses(createdBy);
    return response;
  }

  Future<dynamic> getAllActiveProfileStatuses(String createdBy) async {
    dynamic response = await _service.getAllActiveProfileStatuses(createdBy);
    return response;
  }

  Future<dynamic> addProfileStatuses(String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    dynamic response = await _service.addProfileStatuses(statusName, mailSent, mailSubject, mailContent, createdBy);
    return response;
  }

  Future<dynamic> viewProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic response = await _service.viewProfileStatuses(profileStatusId, createdBy);
    return response;
  }

  Future<dynamic> updateProfileStatuses(
      String profileStatusId, String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    dynamic response = await _service.updateProfileStatuses(profileStatusId, statusName, mailSent, mailSubject, mailContent, createdBy);
    return response;
  }

  Future<dynamic> deleteProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic response = await _service.deleteProfileStatuses(profileStatusId, createdBy);
    return response;
  }

  Future<dynamic> activateOrDeActivateProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic response = await _service.activateOrDeActivateProfileStatuses(profileStatusId, createdBy);
    return response;
  }

  // interview types api's
  Future<dynamic> viewAllInterviewTypes(String createdBy) async {
    dynamic response = await _service.viewAllInterviewTypes(createdBy);
    return response;
  }

  Future<dynamic> getAllActiveInterviewTypes(String createdBy) async {
    dynamic response = await _service.getAllActiveInterviewTypes(createdBy);
    return response;
  }

  Future<dynamic> addInterviewTypes(
      String interviewType, String createdBy, bool systemTestRequired, bool mailSent, String mailSubject, String mailContent) async {
    dynamic response = await _service.addInterviewTypes(interviewType, createdBy, systemTestRequired, mailSent, mailSubject, mailContent);
    return response;
  }

  Future<dynamic> viewInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic response = await _service.viewInterviewTypes(interviewTypeId, createdBy);
    return response;
  }

  Future<dynamic> updateInterviewTypes(String interviewType, String interviewTypeId, String createdBy, bool systemTestRequired, bool mailSent,
      String mailSubject, String mailContent) async {
    dynamic response =
        await _service.updateInterviewTypes(interviewType, interviewTypeId, createdBy, systemTestRequired, mailSent, mailSubject, mailContent);
    return response;
  }

  Future<dynamic> deleteInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic response = await _service.deleteInterviewTypes(interviewTypeId, createdBy);
    return response;
  }

  Future<dynamic> activateOrDeActivateInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic response = await _service.activateOrDeActivateInterviewTypes(interviewTypeId, createdBy);
    return response;
  }

  // interview status api's
  Future<dynamic> viewAllInterviewStatuses(String createdBy) async {
    dynamic response = await _service.viewAllInterviewStatuses(createdBy);
    return response;
  }

  Future<dynamic> getAllActiveInterviewStatuses(String createdBy) async {
    dynamic response = await _service.getAllActiveInterviewStatuses(createdBy);
    return response;
  }

  Future<dynamic> addInterviewStatuses(String statusName, String createdBy) async {
    dynamic response = await _service.addInterviewStatuses(statusName, createdBy);
    return response;
  }

  Future<dynamic> viewInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic response = await _service.viewInterviewStatuses(interviewStatusId, createdBy);
    return response;
  }

  Future<dynamic> updateInterviewStatuses(String statusName, String interviewStatusId, String createdBy) async {
    dynamic response = await _service.updateInterviewStatuses(statusName, interviewStatusId, createdBy);
    return response;
  }

  Future<dynamic> deleteInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic response = await _service.deleteInterviewStatuses(interviewStatusId, createdBy);
    return response;
  }

  Future<dynamic> activateOrDeActivateInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic response = await _service.activateOrDeActivateInterviewStatuses(interviewStatusId, createdBy);
    return response;
  }

  Future<dynamic> updateraiseaBug(String title, String bugid, String employeeId, String description, String bugStatus, String priority,
      List assignedEmployeesList, List attachments,String stageId) async {
    dynamic response = await _service.updateraiseaBug(title, bugid, employeeId, description, bugStatus, priority, assignedEmployeesList, attachments,stageId);
    return response;
  }

  Future<dynamic> addaTestCase(String title, String description, String steps, String expectedresults, String actualResults, String employeeId,
      String projectId, String moduleId, String taskId, String tcStatus) async {
    dynamic response =
        await _service.addaTestCase(title, description, steps, expectedresults, actualResults, employeeId, projectId, moduleId, taskId, tcStatus);
    return response;
  }

  Future<dynamic> updateaTestCase(String testcaseid, String title, String description, String steps, String expectedresults, String actualResults,
      String employeeId, String tcStatus) async {
    dynamic response = await _service.updateaTestCase(testcaseid, title, description, steps, expectedresults, actualResults, employeeId, tcStatus);
    return response;
  }

  Future<dynamic> deleteTestCase(String employeeId, String testCaseId) async {
    dynamic response = await _service.deleteTestCase(employeeId, testCaseId);
    return response;
  }

  Future<dynamic> raiseaBug(String taskId, String employeeId, String projectId, String moduleId, String title, attachments, String description,
      String bugStatus, String priority, List assignedEmployeesList,String stageId) async {
    dynamic response = await _service.raiseaBug(
        taskId, employeeId, projectId, moduleId, title, attachments, description, bugStatus, priority, assignedEmployeesList,stageId);
    return response;
  }

  Future<dynamic> getbugslistbyproject(String employeeId, String projectId, String bugStatus, String title,String priority,String devStageId,String createdBy,List assignedTo) async {
    dynamic response = await _service.getbugslistbyproject(employeeId, projectId, bugStatus, title,priority,devStageId,createdBy,assignedTo);
    return response;
  }

  Future<dynamic> viewBug(String employeeId, String bugId) async {
    dynamic response = await _service.viewBug(employeeId, bugId);
    return response;
  }

  Future<dynamic> deleteBug(String employeeId, String bugId) async {
    dynamic response = await _service.deleteBug(employeeId, bugId);
    return response;
  }

  // get api's
  Future<dynamic> getDesignations() async {
    dynamic response = await _service.getDesignations();
    return response;
  }

  Future<dynamic> getRoles() async {
    dynamic response = await _service.getRoles();
    return response;
  }

  Future<dynamic> getTechnologies() async {
    dynamic response = await _service.getTechnologies();
    return response;
  }

  Future<dynamic> getEmployees(String type) async {
    dynamic response = await _service.getEmployees(type);
    return response;
  }

  Future<dynamic> getInterviewSchedules() async {
    dynamic response = await _service.getInterviewSchedules();
    return response;
  }

  Future<dynamic> getInterviewerStatuses() async {
    dynamic response = await _service.getInterviewerStatuses();
    return response;
  }

  Future<dynamic> getInterviewTypes() async {
    dynamic response = await _service.getInterviewTypes();
    return response;
  }

  Future<dynamic> getProfileStatuses() async {
    dynamic response = await _service.getProfileStatuses();
    return response;
  }

  Future<dynamic> createBoarding(String name, String description, String employeeId) async {
    dynamic response = await _service.createBoarding(name, description, employeeId);
    return response;
  }

  Future<dynamic> getAllBoardings(String employeeId) async {
    dynamic response = await _service.getAllBoardings(employeeId);
    return response;
  }

  Future<dynamic> deleteBoarding(String boardingId, String employeeId) async {
    dynamic response = await _service.deleteBoarding(boardingId, employeeId);
    return response;
  }

  Future<dynamic> updateBoarding(String boardingId, String employeeId, String boardName, String boardDescription) async {
    dynamic response = await _service.updateBoarding(boardingId, employeeId, boardName, boardDescription);
    return response;
  }

  Future<dynamic> getBoardings() async {
    dynamic response = await _service.getBoardings();
    return response;
  }

  Future<dynamic> getDashboardBoardingsApi(String employeeId) async {
    dynamic response = await _service.getDashboardBoardingsApi(employeeId);
    return response;
  }

  Future<dynamic> updateTaskEstimationTimeApi(String employeeId, List taskEstimations) async {
    dynamic response = await _service.updateTaskEstimationTimeApi(employeeId, taskEstimations);
    return response;
  }

  Future<dynamic> taskLogsList(String employeeId, String taskId, String searchType, String searchElement) async {
    dynamic response = await _service.taskLogsList(employeeId, taskId, searchType, searchElement);
    return response;
  }

  Future<dynamic> taskDurationTracking(String employeeId, String taskId) async {
    dynamic response = await _service.taskDurationTracking(employeeId, taskId);
    return response;
  }

  Future<dynamic> taskDurationUpdate(String employeeId, String activityId, String startTime, String endTime) async {
    dynamic response = await _service.taskDurationUpdate(employeeId, activityId, startTime, endTime);
    return response;
  }

  Future<dynamic> viewEmployeeProfile(String employeeId) async {
    dynamic response = await _service.viewEmployeeProfile(employeeId);
    return response;
  }

  Future<dynamic> addSprint(
      String userid, String sprintname, String sprintdesc, String sprintStartdate, String sprintEnddate, String projectId) async {
    dynamic response = await _service.addsprintapi(userid, sprintname, sprintdesc, sprintStartdate, sprintEnddate, projectId);
    return response;
  }

  Future<dynamic> getProjectModulewisetasksprints(String sprintId, String employeeId, String projectId) async {
    dynamic response = await _service.getProjectModulewisetasksprintsapi(sprintId, employeeId, projectId);
    return response;
  }

  Future<dynamic> getMultipleSprintsdata(String employeeId, String projectId) async {
    dynamic response = await _service.getMultipleSprintsdataapi(employeeId, projectId);
    return response;
  }

  Future<dynamic> assign_tasks_to_sprint(String sprintId, String employeeid, String projectId, List<TaskID> taskslistid) async {
    dynamic response = await _service.assigntaskstosprintapi(sprintId, employeeid, projectId, taskslistid);
    return response;
  }

  Future<dynamic> projectBasedSprintapi(String employeeId, List projectId) async {
    dynamic response = await _service.projectBasedSprintapi(employeeId, projectId);
    return response;
  }

  Future assianableEmployeesapi(String employeeId) async {
    dynamic response = await _service.assianableEmployeesapi(employeeId);
    return response;
  }

  Future addScrumapi(String employeeId, String scrumDate, String description, List projectId, List scrumList, String type) async {
    dynamic response = await _service.addScrumapi(employeeId, scrumDate, description, projectId, scrumList, type);
    return response;
  }

  Future getTeamScrumCountapi(String employeeId, String filterType, String startDate, String endDate) async {
    dynamic response = await _service.getTeamScrumCountapi(employeeId, filterType, startDate, endDate);
    return response;
  }

  Future getScrumDataapi(String employeeId, String scrumDate) async {
    dynamic response = await _service.getScrumDataapi(employeeId, scrumDate);
    return response;
  }

  Future<dynamic> viewMyScrum(String employeeId, String startDate, String endDate) async {
    dynamic response = await _service.viewMyScrum(employeeId, startDate, endDate);
    return response;
  }

  Future<dynamic> viewMyScrumCounts(String employeeId, String filterType) async {
    dynamic response = await _service.viewMyScrumCounts(employeeId, filterType);
    return response;
  }

  Future<dynamic> viewMyScrumYearly(String employeeId, String year) async {
    dynamic response = await _service.viewMyScrumYearly(employeeId, year);
    return response;
  }

  Future<dynamic> updateBugStatus(String bugId, String employeeId, String bugStatus) async {
    dynamic response = await _service.updateBugStatus(bugId, employeeId, bugStatus);
    return response;
  }

  Future deleteSprintApi(String sprintId, String employeeId, String projectId) async {
    dynamic response = await _service.deleteSprintApi(sprintId, employeeId, projectId);
    return response;
  }

  Future getAllTeamScrumCountapi(String employeeId) async {
    dynamic response = await _service.getAllTeamScrumCountapi(employeeId);
    return response;
  }

  Future<dynamic> viewteamwiseScrum(String employeeId, String startDate, String endDate,String filterMonth,String filterYear) async {
    dynamic response = await _service.viewteamwiseScrum(employeeId, startDate, endDate, filterMonth, filterYear);
    return response;
  }

  Future<dynamic> addNewtaskToScrum(String employeeId, String assignedEmployeeId, String projectId, String moduleId, String taskId, String taskType,
      String scrumDate, String taskName, String taskDescription, String estimationTime) async {
    dynamic response = await _service.addNewtaskToScrum(
        employeeId, assignedEmployeeId, projectId, moduleId, taskId, taskType, scrumDate, taskName, taskDescription, estimationTime);
    return response;
  }

  Future moduleBasedTaskListApi(String employeeId, String moduleId) async {
    dynamic response = await _service.moduleBasedTaskListApi(employeeId, moduleId);
    return response;
  }

  Future getEmployeeScrumReport(String employeeId, String filterType) async {
    dynamic response = await _service.getEmployeeScrumReport(employeeId, filterType);
    return response;
  }
  Future closeEmployeeScrumTasksApi(String employeeId,String comment) async {
    dynamic response = await _service.closeEmployeeScrumTasksApi(employeeId,comment);
    return response;
  }
  Future viewProjectWiseBugsCountApi(String employeeId) async {
    dynamic response = await _service.viewProjectWiseBugsCountApi(employeeId);
    return response;
  }
  Future gettasktimingsApi(String employeeId,String taskId) async {
    dynamic response = await _service.gettasktimingsApi(employeeId,taskId);
    return response;
  }
  Future<dynamic> getTodayInterviewDashboard(
      String employeeId, String name, List<String> technologiesList, List<String> employeesList, int pageNumber) async {
    dynamic response = await _service.getTodayInterviewDashboard(employeeId, name, technologiesList, employeesList, pageNumber);
    return response;
  }
  Future<dynamic> addInterviewSchedule(String employeeId, String scheduledBy, String profileId, String interviewType, String interviewTime,
      String comments, String mailSubject, String mailBody) async {
    dynamic response =
    await _service.addInterviewSchedule(employeeId, scheduledBy, profileId, interviewType, interviewTime, comments, mailSubject, mailBody);
    return response;
  }
  Future<dynamic> getAllProfiles(String createdBy, String name, String profileStatusId, List employeesList, List technologiesList, int pageCount,
      String startDate, String endDate,List jobRolesList) async {
    dynamic response =
    await _service.getAllProfiles(createdBy, name, profileStatusId, employeesList, technologiesList, pageCount, startDate, endDate,jobRolesList);
    return response;
  }
  Future<dynamic> addJobRole(String employeeId, String role) async {
    dynamic response =
    await _service.addJobRole(employeeId, role);
    return response;
  }
  Future<dynamic> viewalljobrolesApi(String employeeId) async {
    dynamic response =
    await _service.viewalljobrolesApi(employeeId);
    return response;
  }

  Future singlejobroleApi(String employeeId, String jobRoleId) async {
    dynamic response =
    await _service.singlejobroleApi(employeeId,jobRoleId);
    return response;
  }
  Future editjobroleApi(String employeeId, String jobRoleId,String role) async {
    dynamic response =
    await _service.editjobroleApi(employeeId,jobRoleId,role);
    return response;
  }
  Future deletejobroleApi(String employeeId, String jobRoleId) async {
    dynamic response =
    await _service.deletejobroleApi(employeeId,jobRoleId);
    return response;
  }
  Future activatedeactivatejobroleApi(String employeeId, String jobRoleId) async {
    dynamic response =
    await _service.activatedeactivatejobroleApi(employeeId,jobRoleId);
    return response;
  }
  Future viewactivejobrolesApi(String employeeId) async {
    dynamic response =
    await _service.viewactivejobrolesApi(employeeId);
    return response;
  }
  Future updateprofilestatusorderApi(String employeeId,List profilesStatusList) async {
    dynamic response =
    await _service.updateprofilestatusorderApi(employeeId,profilesStatusList);
    return response;
  }

  Future profilestatusdisplaymodificationApi(String employeeId,String profileStatusId) async {
    dynamic response =
    await _service.profilestatusdisplaymodificationApi(employeeId,profileStatusId);
    return response;
  }
  Future inferioremployeeslistApi(String employeeId) async {
    dynamic response =
    await _service.inferioremployeeslistApi(employeeId);
    return response;
  }
  Future viewAllStages(String employeeId) async {
    dynamic response =
    await _service.viewAllStages(employeeId);
    return response;
  }
  Future createStage(String name, String createdBy) async {
    dynamic response =
    await _service.createStage(name,createdBy);
    return response;
  }
  Future updateStage(String stageId, String name,String employeeId) async {
    dynamic response =
    await _service.updateStage(stageId,name,employeeId);
    return response;
  }

  Future deleteStage(String stageId,String employeeId) async {
    dynamic response =
    await _service.deleteStage(stageId,employeeId);
    return response;
  }
  Future viewStage(String stageId,String employeeId) async {
    dynamic response =
    await _service.viewStage(stageId,employeeId);
    return response;
  }
  Future activateDeactivateDevelopmentStageApi(String stageId,String employeeId) async {
    dynamic response =
    await _service.activateDeactivateDevelopmentStageApi(stageId,employeeId);
    return response;
  }
  Future viewactivedevelopmentstagesApi(String employeeId) async {
    dynamic response =
    await _service.viewactivedevelopmentstagesApi(employeeId);
    return response;
  }
  Future<dynamic> employeeLogin(String email, String password, String token) async {
    dynamic response = await _service.employeeLogin(email, password, token);
    return response;
  }

  Future<dynamic> employeeLogout(String employeeId) async {
    dynamic response = await _service.employeeLogout(employeeId);
    return response;
  }

  Future<dynamic> tokenUpdate(String employeeId, String token) async {
    dynamic response = await _service.tokenUpdate(employeeId, token);
    return response;
  }

  Future<dynamic> getBugsListApi(String employeeId,String taskId, String priority,String devStageId) async {
    dynamic response = await _service.getBugsListApi( employeeId, taskId,  priority, devStageId);
    return response;
  }
  Future<dynamic> createChatId(String employeeId,String chatId) async {
    dynamic response = await _service.createChatId(employeeId,chatId);
    return response;
  }

  Future<dynamic> getEmployeeAssociatedChats(String employeeId) async {
    dynamic response = await _service.getEmployeeAssociatedChats(employeeId);
    return response;
  }


}
