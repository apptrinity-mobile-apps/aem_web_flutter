import 'dart:convert';
import 'dart:io';
import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/screens/project_details/ui/sprints_screen.dart';
import 'package:aem/services/app_exceptions.dart';
import 'package:aem/services/base_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiServices extends BaseService {
  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw InternalServerException(response.body.toString());
      default:
        throw FetchDataException('Error occurred while communication with server with status code : ${response.statusCode}');
    }
  }



  // profile changes
  @override
  Future loginProfileUpdate(
      String name, String employeeId, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration, String durationType) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/login_profile_update");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "employeeId": employeeId,
            "phoneNumber": phoneNumber,
            "availabilitySchedule": availabilitySchedule,
            "duration": duration,
            "durationType": durationType,
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future requestResetPasswordOTP(String email) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/employee_password_reset");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"email": email}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future resetPassword(String otp, String email, String newPassword, String confirmNewPassword) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/employee_forgot_password");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"otp": otp, "email": email, "newPassword": newPassword, "confirmNewPassword": confirmNewPassword}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future changePassword(String employeeId, String password, String newPassword, String confirmNewPassword) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/change_password");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "password": password, "newPassword": newPassword, "confirmNewPassword": confirmNewPassword}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // dashboard apis
  @override
  Future getDashboard(String employeeId, String boardingId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_dashboard");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "boardingId": boardingId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeePermissions(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/employee_permissions");
    print("gettasktimingsApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId});

    print("gettasktimingsApi body is ${_body}");

    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // projects details apis
  @override
  Future projectView(String employeeId, String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/project_single_modification");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "projectId": projectId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future projectLogsList(String employeeId, String projectId, String searchType, String searchElement) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/project_logs_list");

    print("projectLogsList url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "projectId": projectId, "searchType": searchType, "searchElement": searchElement});
    print("projectLogsList body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createModule(String name, String description, String startDate, String endDate, String notes, String projectId, String createdBy,
      List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/create_module");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "description": description,
            "startDate": startDate,
            "endDate": endDate,
            "notes": notes,
            "projectId": projectId,
            "createdBy": createdBy,
            "assignedTo": assignedTo,
            "notifyTo": notifyTo,
            "assignedToTester": assignedToTester
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateModule(String name, String description, String startDate, String endDate, String notes, String projectId, String moduleId,
      String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_module");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "description": description,
            "startDate": startDate,
            "endDate": endDate,
            "notes": notes,
            "projectId": projectId,
            "moduleId": moduleId,
            "createdBy": createdBy,
            "assignedTo": assignedTo,
            "notifyTo": notifyTo,
            "assignedToTester": assignedToTester
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewAllModules(String projectId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_modules");
    print("viewAllModules url is ${url}");
    var _body = json.encode({"projectId": projectId, "createdBy": createdBy});
    print("viewAllModules body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewModule(String moduleId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_module");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"moduleId": moduleId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteModule(String moduleId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_module");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"moduleId": moduleId, "employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addCommentToModule(String comment, String moduleId, String projectId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/create_module_comment");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "comment": comment,
            "moduleId": moduleId,
            "projectId": projectId,
            "employeeId": employeeId,
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteModuleComment(String moduleCommentId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_comment");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"moduleCommentId": moduleCommentId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateModuleComment(String moduleCommentId, String comment) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_module_comment");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"comment": comment, "moduleCommentId": moduleCommentId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // tasks apis
  @override
  Future addTask(String name, String description, String addedBy, String startDate, String endDate, String notes, String projectId, String moduleId,
      String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_tasks");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "description": description,
            "addedBy": addedBy,
            "startDate": startDate,
            "endDate": endDate,
            "notes": notes,
            "projectId": projectId,
            "moduleId": moduleId,
            "createdBy": createdBy,
            "assignedTo": assignedTo,
            "notifyTo": notifyTo,
            "assignedToTester": assignedToTester,
            "estimationTime":estimationTime
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateTask(String taskId, String name, String description, String addedBy, String startDate, String endDate, String notes, String projectId,
      String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationtime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_task");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "taskId": taskId,
            "name": name,
            "description": description,
            "addedBy": addedBy,
            "startDate": startDate,
            "endDate": endDate,
            "notes": notes,
            "projectId": projectId,
            "moduleId": moduleId,
            "createdBy": createdBy,
            "assignedTo": assignedTo,
            "notifyTo": notifyTo,
            "assignedToTester": assignedToTester,
            "estimationTime":estimationtime
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewTask(String taskId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_task");
    print("viewTask url is ${url}");
    var _body = json.encode({"taskId": taskId, "createdBy": employeeId});
    print("viewTask body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteTask(String taskId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_task");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"taskId": taskId, "employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addCommentToTask(String taskId, String comment, String moduleId, String projectId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_task_comments");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "taskId": taskId,
            "comment": comment,
            "moduleId": moduleId,
            "projectId": projectId,
            "employeeId": employeeId,
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateTaskComment(String taskCommentId, String comment) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_task_comment");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"taskCommentId": taskCommentId, "comment": comment}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteTaskComment(String taskCommentId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_task_comment");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"taskCommentId": taskCommentId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getTasksBasedOnStatus(String projectId, String employeeId, String statusText) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/status_based_tasks_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "projectId": projectId, "statusText": statusText}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateTasksBasedOnStatus(String employeeId, String taskId, String statusText, String comment,int percentageComplete) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_task_basedon_status");
    print("updateTasksBasedOnStatus url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "taskId": taskId, "statusText": statusText, "comment": comment,"percentageComplete":percentageComplete});

    print("closeEmployeeScrumTasksApi body is ${_body}");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // employees apis
  @override
  Future viewAllEmployees(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_employees");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createEmployee(
      String name,
      String email,
      String password,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String createdBy,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperiorEmployee) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/create_employee");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "email": email,
            "password": password,
            "designationId": designationId,
            "technologyId": technologyId,
            "roleId": roleId,
            "createdBy": createdBy,
            "phoneNumber": phoneNumber,
            "availabilitySchedule": availabilitySchedule,
            "duration": duration,
            "durationType": durationType,
            "assignableEmployees": assignableEmployees,
            "isSuperiorEmployee": isSuperiorEmployee
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteEmployee(String loginEmployeeId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_employee");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "loginEmployeeId": loginEmployeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateEmployee(
      String name,
      String email,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String employeeId,
      String userId,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperiorEmployee) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_employee");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "email": email,
            "designationId": designationId,
            "technologyId": technologyId,
            "roleId": roleId,
            "employeeId": employeeId,
            "userId": userId,
            "phoneNumber": phoneNumber,
            "availabilitySchedule": availabilitySchedule,
            "duration": duration,
            "durationType": durationType,
            "assignableEmployees": assignableEmployees,
            "isSuperiorEmployee": isSuperiorEmployee
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewEmployee(String loginEmployeeId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_employee");

    print("viewEmployee url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "loginEmployeeId": loginEmployeeId});

    print("viewEmployee body is ${_body}");

    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activateEmployee(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_employee");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deActivateEmployee(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/deactivate_employee");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewDayBasedEmployeeSchedule(String employeeId, String selectedDate) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_day_based_employee_schedule");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "selectedDate": selectedDate}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // projects apis
  @override
  Future getAllProjects(String employeeId, String from) async {
    dynamic responseJson;
    Uri url;
    if (from == 'scrum') {
      url = Uri.parse(baseUrl + "/get_employee_accessible_projects_list");
    } else {
      url = Uri.parse(baseUrl + "/get_project");
    }

    print("getAllProjects url is ${url}");

    var _body = json.encode({"employeeId": employeeId});
    print("getAllProjects body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createProject(
      String name, String description, List<String> employees, String createdBy, String boardingId, List<String> technologyId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_emsprojects");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "name": name,
            "description": description,
            "employees": employees,
            "createdBy": createdBy,
            "boardingId": boardingId,
            "technologies": technologyId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateProject(
      String name, String description, List<String> employees, String projectId, String boardingId, List<String> technologyId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_emsprojects");

    print("updateProject url -- ${url}");

    var _body = json.encode({
      "name": name,
      "description": description,
      "employees": employees,
      "projectId": projectId,
      "boardingId": boardingId,
      "technologies": technologyId
    });
    print("updateProject _body -- ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewProject(String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_project");

    print(" view project  url is ${url}");

    var _body = json.encode({"projectId": projectId});
    print(" view project  _body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteProject(String projectId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_project");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"projectId": projectId, "employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activateProject(String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_project");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"projectId": projectId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deActivateProject(String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/deactivate_project");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"projectId": projectId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // technologies apis
  @override
  Future viewAllTechnologies(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_technology");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createTechnology(String createdBy, String name, String image) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_technologies");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy, "name": name, "image": image}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateTechnology(String technologyId, String name, String image) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_technology");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"technologyId": technologyId, "name": name, "image": image}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteTechnology(String technologyId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_technology");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"technologyId": technologyId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewTechnology(String technologyId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_technology");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"technologyId": technologyId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // designations apis
  @override
  Future viewAllDesignations(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_designations");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createDesignation(String name, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_designation");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"name": name, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateDesignation(String designationId, String name) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_designation");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"designationId": designationId, "name": name}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteDesignation(String designationId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_designation");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"designationId": designationId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewDesignation(String designationId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_designation");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"designationId": designationId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // roles apis
  @override
  Future viewAllRoles(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_roles");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createRole(
      String employeeId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_role");
    print("createRole url ${url}");

    var _body = json.encode({
      "role": roleName,
      "dashboardPermissions": dashboardPermissions,
      "sprintPermissions": sprintPermissions,
      "scrumPermissions": scrumPermissions,
      "employeePermissions": employeePermissions,
      "projectPermissions": projectPermissions,
      "boardingPermissions": boardingPermissions,
      "designationPermissions": designationPermissions,
      "technologyPermissions": technologyPermissions,
      "rolePermissions": rolePermissions,
      "interviewTypesPermissions": interviewTypesPermissions,
      "profileStatusPermissions": profileStatusPermissions,
      "interviewStatusPermissions": interviewStatusPermissions,
      "jobRolePermissions":jobRolePermissions,
      "profilePermissions": profilePermissions,
      "taskPermissions": taskPermissions,
      "bugPermissions": bugPermissions,
      "modulePermissions": modulePermissions,
      "stagesPermissions":stagespermissions,

      "createdBy": employeeId
    });

    print("createRole body ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateRole(
      String roleId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_role");
    print("createRole url ${url}");

    var _body = json.encode({
      "role": roleName,
      "dashboardPermissions": dashboardPermissions,
      "sprintPermissions": sprintPermissions,
      "scrumPermissions": scrumPermissions,
      "employeePermissions": employeePermissions,
      "projectPermissions": projectPermissions,
      "boardingPermissions": boardingPermissions,
      "designationPermissions": designationPermissions,
      "technologyPermissions": technologyPermissions,
      "rolePermissions": rolePermissions,
      "interviewTypesPermissions": interviewTypesPermissions,
      "profileStatusPermissions": profileStatusPermissions,
      "interviewStatusPermissions": interviewStatusPermissions,
      "jobRolePermissions": jobRolePermissions,
      "profilePermissions": profilePermissions,
      "taskPermissions": taskPermissions,
      "bugPermissions": bugPermissions,
      "modulePermissions": modulePermissions,
      "stagesPermissions":stagespermissions,
      "roleId": roleId
    });

    print("createRole body ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteRole(String roleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_role");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"roleId": roleId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewRole(String roleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_role");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"roleId": roleId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // profiles apis


  @override
  Future viewProfile(String profileId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_view_profile");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"profileId": profileId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteProfile(String profileId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_profile");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"profileId": profileId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addProfile(
      String createdBy,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_profile");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "createdBy": createdBy,
            "name": name,
            "phoneNumber": phoneNumber,
            "careerStartedOn": careerStartedOn,
            "experienceStatus": experienceStatus,
            "employmentStatus": employmentStatus,
            "email": email,
            "mainTechnology": mainTechnology,
            "technology": technology,
            "superiorEmployees": superiorEmployees,
            "resume": resume,
            "fileExtension": fileExtension,
            "source": source,
            "resumeFileName": resumeFileName,
            "jobRoleId":jobRoleId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateProfile(
      String createdBy,
      String profileId,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_profile");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "createdBy": createdBy,
            "profileId": profileId,
            "name": name,
            "phoneNumber": phoneNumber,
            "careerStartedOn": careerStartedOn,
            "experienceStatus": experienceStatus,
            "employmentStatus": employmentStatus,
            "email": email,
            "mainTechnology": mainTechnology,
            "technology": technology,
            "superiorEmployees": superiorEmployees,
            "resume": resume,
            "fileExtension": fileExtension,
            "source": source,
            "resumeFileName": resumeFileName,
            "jobRoleId":jobRoleId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addProfileComment(String employeeId, String profileId, String comment) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_profile_comments");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "profileId": profileId, "comment": comment}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateProfileStatus(String employeeId, String profileId, String employmentStatus, String comment) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "profileId": profileId, "employmentStatus": employmentStatus, "comment": comment}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future requestResume(String profileId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/send_mail_requesting_for_resume");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"profileId": profileId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addSystemTest(
      String employeeId, String scheduledBy, String profileId, String comment, String systemTestTime, List<String> supportingDocs) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_system_test");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "scheduledBy": scheduledBy,
            "profileId": profileId,
            "comment": comment,
            "systemTestTime": systemTestTime,
            "supportingDocs": supportingDocs
          }));
      debugPrint(json.encode({
        "employeeId": employeeId,
        "scheduledBy": scheduledBy,
        "profileId": profileId,
        "comment": comment,
        "systemTestTime": systemTestTime,
        "supportingDocs": supportingDocs
      }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // interview schedule



  @override
  Future updateInterviewSchedule(String employeeId, String scheduledId, String interviewStatusId, String comments) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_interview_schedule_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "scheduleId": scheduledId, "interviewStatusId": interviewStatusId, "comments": comments}));
      debugPrint(json.encode({"employeeId": employeeId, "scheduleId": scheduledId, "interviewStatusId": interviewStatusId, "comments": comments}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // profile status api's
  @override
  Future viewAllProfileStatuses(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_profiles_status");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllActiveProfileStatuses(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_active_profiles_status");


    print("getAllActiveProfileStatuses url is ${url}");

    var _body = json.encode({"createdBy": createdBy});
    print("getAllActiveProfileStatuses _body is ${_body}");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addProfileStatuses(String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode(
              {"statusName": statusName, "mailSent": mailSent, "mailSubject": mailSubject, "mailContent": mailContent, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"profileStatusId": profileStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateProfileStatuses(
      String profileStatusId, String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/edit_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "profileStatusId": profileStatusId,
            "statusName": statusName,
            "mailSent": mailSent,
            "mailSubject": mailSubject,
            "mailContent": mailContent,
            "createdBy": createdBy
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"profileStatusId": profileStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activateOrDeActivateProfileStatuses(String profileStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_deactivate_profile_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"profileStatusId": profileStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // interview types api's
  @override
  Future viewAllInterviewTypes(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_interview_types");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllActiveInterviewTypes(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_active_interview_types");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addInterviewTypes(
      String interviewType, String createdBy, bool systemTestRequired, bool mailSent, String mailSubject, String mailContent) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_interview_type");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "interviewType": interviewType,
            "createdBy": createdBy,
            "systemTestRequired": systemTestRequired,
            "mailSent": mailSent,
            "mailSubject": mailSubject,
            "mailContent": mailContent
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_interview_type");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewTypeId": interviewTypeId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateInterviewTypes(String interviewType, String interviewTypeId, String createdBy, bool systemTestRequired, bool mailSent,
      String mailSubject, String mailContent) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_interview_type");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "interviewType": interviewType,
            "interviewTypeId": interviewTypeId,
            "createdBy": createdBy,
            "systemTestRequired": systemTestRequired,
            "mailSent": mailSent,
            "mailSubject": mailSubject,
            "mailContent": mailContent
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_interview_type");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewTypeId": interviewTypeId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activateOrDeActivateInterviewTypes(String interviewTypeId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_deactivate_interview_type");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewTypeId": interviewTypeId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // interview status api's
  @override
  Future viewAllInterviewStatuses(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_interviews_status");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllActiveInterviewStatuses(String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_active_interviews_status");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addInterviewStatuses(String statusName, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_interview_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"statusName": statusName, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_interview_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewStatusId": interviewStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateInterviewStatuses(String statusName, String interviewStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/edit_interview_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"statusName": statusName, "interviewStatusId": interviewStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_interview_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewStatusId": interviewStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activateOrDeActivateInterviewStatuses(String interviewStatusId, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_deactivate_interview_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"interviewStatusId": interviewStatusId, "createdBy": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addaTestCase(String title, String description, String steps, String expectedresults, String actualResults, String employeeId,
      String projectId, String moduleId, String taskId, String tcStatus) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_test_cases");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "title": title,
            "description": description,
            "steps": steps,
            "expectedResults": expectedresults,
            "actualResults": actualResults,
            "employeeId": employeeId,
            "projectId": projectId,
            "moduleId": moduleId,
            "taskId": taskId,
            "tcStatus": tcStatus
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateaTestCase(String testcaseid, String title, String description, String steps, String expectedresults, String actualResults,
      String employeeId, String tcStatus) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_test_cases");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "testCaseId": testcaseid,
            "title": title,
            "description": description,
            "steps": steps,
            "expectedResults": expectedresults,
            "actualResults": actualResults,
            "employeeId": employeeId,
            "tcStatus": tcStatus
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteTestCase(String employeeId, String testCaseId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_test_cases");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "testCaseId": testCaseId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future raiseaBug(String taskId, String employeeId, String projectId, String moduleId, String title, attachments, String description,
      String bugStatus, String priority, List assignedEmployeesList,String stageId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_bug");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "taskId": taskId,
            "employeeId": employeeId,
            "projectId": projectId,
            "moduleId": moduleId,
            "title": title,
            "attachments": attachments,
            "description": description,
            "bugStatus": bugStatus,
            "priority": priority,
            "assignedEmployeesList": assignedEmployeesList,
            "devStageId": stageId,
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateraiseaBug(String title, String bugid, String employeeId, String description, String bugStatus, String priority,
      List assignedEmployeesList, List attachments,String stageId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_bug");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "title": title,
            "bugId": bugid,
            "employeeId": employeeId,
            "description": description,
            "bugStatus": bugStatus,
            "priority": priority,
            "assignedEmployeesList": assignedEmployeesList,
            "attachments": attachments,
            "devStageId": stageId,
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getbugslistbyproject(String employeeId, String projectId, String bugStatus, String title,String priority,String devStageId ,String createdBy,List assignedTo) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_bugs_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "projectId": projectId, "bugStatus": bugStatus, "title": title,"priority":priority,"devStageId":devStageId,"createdBy":createdBy,"assignedTo":assignedTo}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewBug(String employeeId, String bugId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_bug");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "bugId": bugId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteBug(String employeeId, String bugId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_bug");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "bugId": bugId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  // get api's
  @override
  Future getDesignations() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_designations");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getRoles() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_roles");

    print("getRoles api ${url}");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getTechnologies() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_technologies");

    print("getTechnologies url is ${url}");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getEmployees(String type) async {
    dynamic responseJson;

    Uri url;

    if (type == "project") {
      url = Uri.parse(baseUrl + "/get_project_related_employees");
    }else
   {
      url = Uri.parse(baseUrl + "/get_all_employees");
    }
    print("getEmployees url is ${url}");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future inferioremployeeslistApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/inferior_employees_list");
    print("inferioremployeeslistApi url is ${url}");

    var _body = json.encode({

      "employeeId": employeeId,
    });

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("inferioremployeeslistApi ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future getInterviewSchedules() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_interview_schedules");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getInterviewerStatuses() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_interviews_status");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getInterviewTypes() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_interview_types");
    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getProfileStatuses() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_active_profiles_status");
    print("getProfileStatuses url is ${url}");

    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createBoarding(String name, String description, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_boarding");
    print("add_boarding url is ${url}");

    var _body = json.encode({
      "boardName": name,
      "boardDescription": description,
      "employeeId": employeeId,
    });

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("add_boarding body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllBoardings(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_boardings");
    print("all Boarding url is ${url}");

    var _body = json.encode({
      "employeeId": employeeId,
    });

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("all Boarding body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteBoarding(String boardingId, String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_boarding");
    print(" Boarding delete url is ${url}");

    var _body = json.encode({
      "boardingId": boardingId,
      "employeeId": employeeId,
    });

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print(" Boarding delete  body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateBoarding(String boardingId, String employeeId, String boardName, String boardDescription) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_boarding");
    print(" update Boarding  url is ${url}");

    var _body = json.encode({"boardingId": boardingId, "employeeId": employeeId, "boardName": boardName, "boardDescription": boardDescription});

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print(" update Boarding   body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getBoardings() async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_all_boardings");
    print(" get_all_boardings url is ${url}");

    try {
      final response = await http.get(url, headers: {"Content-type": "application/json; charset=UTF-8"});
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getDashboardBoardingsApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_dashboard_boardings");
    print("  Boarding  url is ${url}");

    var _body = json.encode({"employeeId": employeeId});

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("  Boarding   body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateTaskEstimationTimeApi(String employeeId, List taskEstimations) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_task_estimation_time");
    print("  Boarding  url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "taskEstimations": taskEstimations});

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("  Boarding   body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future taskLogsList(String employeeId, String taskId, String searchType, String searchElement) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/task_specific_logs");

    print("projectLogsList url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "taskId": taskId, "searchType": searchType, "searchElement": searchElement});
    print("projectLogsList body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future taskDurationTracking(String employeeId, String taskId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/task_duration_tracking");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "taskId": taskId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future taskDurationUpdate(String employeeId, String activityId, String startTime, String endTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_task_bug_activity_duration");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "activityId": activityId, "startTime": startTime, "endTime": endTime}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewEmployeeProfile(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/my_profile_details");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addsprintapi(String employeeId, String sprintname, String sprintdesc, String sprintStartdate, String sprintEnddate, String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "add_sprint");
    print("  addsprint  url is ${url}");

    var _body = json.encode({
      "employeeId": employeeId,
      "name": sprintname,
      "description": sprintdesc,
      "startDate": sprintStartdate,
      "endDate": sprintEnddate,
      "projectId": projectId
    });

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      print("  addsprint   body is ${_body}");
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getProjectModulewisetasksprintsapi(String sprintId, String employeeId, String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "fetch_project_based_modules_tasks");

    print("getProjectModulewisetasksprints url is ${url}");
    var _body = json.encode({"sprintId": sprintId, "employeeId": employeeId, "projectId": projectId});
    print("getProjectModulewisetasksprints body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getMultipleSprintsdataapi(String employeeId, String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_multiple_sprints_data");

    print("getMultipleSprintsdata url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "projectId": projectId});
    print("getMultipleSprintsdata body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future assigntaskstosprintapi(String sprintId, String employeeId, String projectId, List<TaskID> taskslistid) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "assign_tasks_to_sprint");

    print("assign_tasks_to_sprint url is ${url}");
    var _body = json.encode({"sprintId": sprintId, "employeeId": employeeId, "projectId": projectId, "tasksList": taskslistid});
    print("assign_tasks_to_sprint body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future projectBasedSprintapi(String employeeId, List projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_multiple_projects_sprints_data");

    print("projectBasedSprintapi url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "projectsList": projectId});
    print("projectBasedSprintapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future assianableEmployeesapi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_employee_assignable_employees");

    print("assianableEmployeesapi url is ${url}");
    var _body = json.encode({"employeeId": employeeId});
    print("assianableEmployeesapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addScrumapi(String employeeId, String scrumDate, String description, List projectId, List scrumList, String type) async {
    dynamic responseJson;
    String method;
    if (type == "add") {
      method = "add_scrum";
    } else {
      method = "edit_scrum";
    }

    Uri url = Uri.parse(baseUrl + method);

    print("projectBasedSprintapi url is ${url}");
    var _body = json.encode(
        {"employeeId": employeeId, "scrumDate": scrumDate, "description": description, "projectsList": projectId, "scrumTasksList": scrumList});
    print("projectBasedSprintapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getTeamScrumCountapi(String employeeId, String filterType, String startDate, String endDate) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_team_scrum_tasks_counts");

    print("getTeamScrumCountapi url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "filterType": filterType, "startDate": startDate, "endDate": endDate});
    print("getTeamScrumCountapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getScrumDataapi(String employeeId, String scrumDate) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_scrum_data");

    print("getScrumDataapi url is ${url}");
    var _body = json.encode({"employeeId": employeeId, "scrumDate": scrumDate});
    print("getScrumDataapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

// my scrum api
  @override
  Future viewMyScrum(String employeeId, String startDate, String endDate) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_my_scrum_tasks_list");
    print("viewMyScrum url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "startDate": startDate, "endDate": endDate});

    print("viewMyScrum body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewMyScrumCounts(String employeeId, String filterType) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_my_scrum_tasks_counts");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "filterType": filterType}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewMyScrumYearly(String employeeId, String year) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_my_year_scrum_tasks_list");
    print("viewMyScrumYearly url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "year": year});

    print("viewMyScrumYearly body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateBugStatus(String bugId, String employeeId, String bugStatus) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_bug_status");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "bugStatus": bugStatus, "bugId": bugId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteSprintApi(String sprintId, String employeeId, String projectId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_sprint");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "sprintId": sprintId, "projectId": projectId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getAllTeamScrumCountapi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "superior_employees_count_details");

    print("getAllTeamScrumCountapi url is ${url}");
    var _body = json.encode({"employeeId": employeeId});
    print("getAllTeamScrumCountapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewteamwiseScrum(String employeeId, String selectedtype, String customdate, String filterMonth, String filterYear) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_employee_scrum_tasks_data");

    print("getScrumDataapi url is ${url}");
    var _body = json.encode(
        {"employeeId": employeeId, "filterType": selectedtype, "customDate": customdate, "filterMonth": filterMonth, "filterYear": filterYear});

    print("getScrumDataapi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addNewtaskToScrum(String employeeId, String assignedEmployeeId, String projectId, String moduleId, String taskId, String taskType,
      String scrumDate, String taskName, String taskDescription, String estimationTime) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "add_new_task_to_scrum");

    print("addNewtaskToScrum url is ${url}");

    var _body = json.encode({
      "employeeId": employeeId,
      "assignedEmployeeId": assignedEmployeeId,
      "projectId": projectId,
      "moduleId": moduleId,
      "taskId": taskId,
      "taskType": taskType,
      "scrumDate": scrumDate,
      "taskName": taskName,
      "taskDescription": taskDescription,
      "estimationTime": estimationTime
    });

    print("addNewtaskToScrum body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future moduleBasedTaskListApi(String employeeId, String moduleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "module_based_tasks_list");

    print("moduleBasedTaskListApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "moduleId": moduleId});

    print("moduleBasedTaskListApi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
      print("responseJson body is ${responseJson}");
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getEmployeeScrumReport(String employeeId, String filterType) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_employee_associated_scrum_tasks_counts");
    var _body = json.encode({"employeeId": employeeId, "filterType": filterType});
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future closeEmployeeScrumTasksApi(String employeeId,String comment) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "close_employee_scrum_tasks");
    print("closeEmployeeScrumTasksApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId,"comment":comment});

    print("closeEmployeeScrumTasksApi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewProjectWiseBugsCountApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "view_project_wise_bugs_count");
    print("viewProjectWiseBugsCountApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId});

    print("viewProjectWiseBugsCountApi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future gettasktimingsApi(String employeeId,String taskId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "get_task_timings");
    print("gettasktimingsApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId,"taskId":taskId});

    print("gettasktimingsApi body is ${_body}");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future getTodayInterviewDashboard(
      String employeeId, String name, List<String> technologiesList, List<String> employeesList, int pageNumber) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/profiles_dashboard_data");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "searchItem": name,
            "technologiesList": technologiesList,
            "employeesList": employeesList,
            "pageNumber": pageNumber,
            "pageLimit": 20
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future addInterviewSchedule(String employeeId, String scheduledBy, String profileId, String interviewType, String interviewTime, String comments,
      String mailSubject, String mailBody) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_interview_schedule");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "scheduledBy": scheduledBy,
            "profileId": profileId,
            "interviewType": interviewType,
            "interviewTime": interviewTime,
            "comments": comments,
            "mailSubject": mailSubject,
            "mailBody": mailBody
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future getAllProfiles(String createdBy, String name, String profileStatusId, List employeesList, List technologiesList, int pageCount,
      String startDate, String endDate,List jobRolesList) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_profiles");

    print("getAllProfiles url is ${url}");

    var _body = json.encode({
      "createdBy": createdBy,
      "name": name,
      "profileStatusId": profileStatusId,
      "employeesList": employeesList,
      "technologiesList": technologiesList,
      "pageNumber": pageCount,
      "pageLimit": 20,
      "startDate": startDate,
      "endDate": endDate,
      "jobRolesList":jobRolesList
    });

    print("getAllProfiles body is ${_body}");

    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }


  // Job Role api
  @override
  Future addJobRole(String employeeId, String role) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_job_role");
    print("addJobRole url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "role": role});

    print("addJobRole body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewalljobrolesApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_job_roles");
    print("viewalljobrolesApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId});

    print("viewalljobrolesApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future singlejobroleApi(String employeeId, String jobRoleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_job_role");
    print("singlejobroleApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "jobRoleId": jobRoleId});

    print("singlejobroleApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future editjobroleApi(String employeeId, String jobRoleId,String role) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/edit_job_role");
    print("editjobroleApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "jobRoleId": jobRoleId,"role":role});

    print("editjobroleApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deletejobroleApi(String employeeId, String jobRoleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_job_role");
    print("deletejobroleApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "jobRoleId": jobRoleId});

    print("deletejobroleApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future activatedeactivatejobroleApi(String employeeId, String jobRoleId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_deactivate_job_role");
    print("activatedeactivatejobroleApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId, "jobRoleId": jobRoleId});

    print("activatedeactivatejobroleApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewactivejobrolesApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_active_job_roles");
    print("viewactivejobrolesApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId});

    print("viewactivejobrolesApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateprofilestatusorderApi(String employeeId,List profilesStatusList) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_profile_status_order");
    print("updateprofilestatusorderApi url is ${url}");

    var _body = json.encode({"employeeId": employeeId,"profilesStatusList":profilesStatusList});

    print("updateprofilestatusorderApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future profilestatusdisplaymodificationApi(String employeeId,String profileStatusId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/profile_status_display_modification");
    print("updateprofilestatusorderApi url is ${url}");

    var _body = json.encode({"createdBy": employeeId,"profileStatusId":profileStatusId});

    print("updateprofilestatusorderApi body is ${_body}");

    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: _body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }


  // stages apis
  @override
  Future viewAllStages(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_development_stages");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future createStage(String name, String createdBy) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_development_stage");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"stage": name, "employeeId": createdBy}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future updateStage(String stageId, String name,String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/edit_development_stage");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"developmentStageId": stageId, "stage": name,"employeeId":employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future deleteStage(String stageId,String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/delete_development_stage");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"developmentStageId": stageId,"employeeId":employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewStage(String stageId,String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/single_development_stage");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"developmentStageId": stageId,"employeeId":employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future activateDeactivateDevelopmentStageApi(String stageId,String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/activate_deactivate_development_stage");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"developmentStageId": stageId,"employeeId":employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewactivedevelopmentstagesApi(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_active_development_stages");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId":employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeeLogin(String email, String password, String token) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/employee_login");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"email": email, "password": password, "token": token}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeeLogout(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/employee_logout");
    try {
      final response =
      await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future tokenUpdate(String employeeId, String token) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_employee_web_notification_token");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "token": token}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future<dynamic> getBugsListApi(String employeeId,String taskId, String priority,String devStageId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/task_based_bugs_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "taskId": taskId,"priority":priority,"devStageId":devStageId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
  @override
  Future createChatId(String employeeId, String chatId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/update_employee_chat_id");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId, "chatId": chatId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getEmployeeAssociatedChats(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_project_associated_employees_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

}
