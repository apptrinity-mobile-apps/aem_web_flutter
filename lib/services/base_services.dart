import 'package:aem/model/all_employees_response_model.dart';
import 'package:aem/screens/project_details/ui/sprints_screen.dart';
abstract class BaseService {
  // staging
 //static const String _url = "http://18.224.206.255/";
  // live
  static const String _url = "https://brio.one/";
  final String baseUrl = "${_url}api/frontend/";
  // profile changes
  Future<dynamic> changePassword(String employeeId, String password, String newPassword, String confirmNewPassword);
  Future<dynamic> requestResetPasswordOTP(String email);
  Future<dynamic> resetPassword(String otp, String email, String newPassword, String confirmNewPassword);
  Future<dynamic> loginProfileUpdate(String name, String employeeId, String phoneNumber, AvailabilitySchedules availabilitySchedule, int duration,
      String durationType);
  // dashboard apis
  Future<dynamic> employeePermissions(String employeeId);
  Future<dynamic> getDashboard(String employeeId, String boardingId);
  // projects details apis
  Future<dynamic> projectView(String employeeId, String projectId);
  Future<dynamic> projectLogsList(String employeeId, String projectId, String searchType, String searchElement);
  Future<dynamic> createModule(String name, String description, String startDate, String endDate, String notes, String projectId, String createdBy,
      List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester);
  Future<dynamic> updateModule(String name, String description, String startDate, String endDate, String notes, String projectId, String moduleId,
      String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester);
  Future<dynamic> viewAllModules(String projectId, String createdBy);
  Future<dynamic> viewModule(String moduleId, String createdBy);
  Future<dynamic> deleteModule(String moduleId, String employeeId);
  Future<dynamic> addCommentToModule(String comment, String moduleId, String projectId, String employeeId);
  Future<dynamic> deleteModuleComment(String moduleCommentId);
  Future<dynamic> updateModuleComment(String moduleCommentId, String comment);
  // tasks apis
  Future<dynamic> getTasksBasedOnStatus(String projectId, String employeeId, String statusText);
  Future<dynamic> updateTasksBasedOnStatus(String employeeId, String taskId, String statusText, String comment, int percentageComplete);
  Future<dynamic> addTask(String name, String description, String addedBy, String startDate, String endDate, String notes, String projectId,
      String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationTime);
  Future<dynamic> updateTask(String taskId, String name, String description, String addedBy, String startDate, String endDate, String notes,
      String projectId, String moduleId, String createdBy, List<String> assignedTo, List<String> notifyTo, List<String> assignedToTester,int estimationtime);
  Future<dynamic> viewTask(String taskId, String employeeId);
  Future<dynamic> deleteTask(String taskId, String employeeId);
  Future<dynamic> addCommentToTask(String taskId, String comment, String moduleId, String projectId, String employeeId);
  Future<dynamic> deleteTaskComment(String taskCommentId);
  Future<dynamic> updateTaskComment(String taskCommentId, String comment);
  // employees apis
  Future<dynamic> viewAllEmployees(String employeeId);
  Future<dynamic> createEmployee(String name,
      String email,
      String password,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String createdBy,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperiorEmployee);
  Future<dynamic> deleteEmployee(String loginEmployeeId, String employeeId);
  Future<dynamic> updateEmployee(String name,
      String email,
      List<String> designationId,
      List<String> technologyId,
      String roleId,
      String employeeId,
      String userId,
      String phoneNumber,
      AvailabilitySchedules availabilitySchedule,
      int duration,
      String durationType,
      List<String> assignableEmployees,
      bool isSuperiorEmployee);
  Future<dynamic> viewEmployee(String loginEmployeeId, String employeeId);
  Future<dynamic> activateEmployee(String employeeId);
  Future<dynamic> deActivateEmployee(String employeeId);
  Future<dynamic> viewDayBasedEmployeeSchedule(String employeeId, String selectedDate);
  // technologies apis
  Future<dynamic> viewAllTechnologies(String employeeId);
  Future<dynamic> createTechnology(String createdBy, String name, String image);
  Future<dynamic> updateTechnology(String technologyId, String name, String image);
  Future<dynamic> deleteTechnology(String technologyId);
  Future<dynamic> viewTechnology(String technologyId);
  // projects apis
  Future<dynamic> getAllProjects(String employeeId, String from);
  Future<dynamic> createProject(String name, String description, List<String> employees, String createdBy, String boardingId,
      List<String> technologyId);
  Future<dynamic> updateProject(String name, String description, List<String> employees, String projectId, String boardingId,
      List<String> technologyId);
  Future<dynamic> deleteProject(String projectId, String employeeId);
  Future<dynamic> viewProject(String projectId);
  Future<dynamic> activateProject(String projectId);
  Future<dynamic> deActivateProject(String projectId);
  // designations apis
  Future<dynamic> viewAllDesignations(String employeeId);
  Future<dynamic> createDesignation(String name, String createdBy);
  Future<dynamic> updateDesignation(String designationId, String name);
  Future<dynamic> deleteDesignation(String designationId);
  Future<dynamic> viewDesignation(String designationId);
  // roles apis
  Future<dynamic> viewAllRoles(String employeeId);
  Future<dynamic> createRole(String employeeId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions);
  Future<dynamic> updateRole(String roleId,
      String roleName,
      List<String> dashboardPermissions,
      List<String> sprintPermissions,
      List<String> scrumPermissions,
      List<String> employeePermissions,
      List<String> projectPermissions,
      List<String> boardingPermissions,
      List<String> designationPermissions,
      List<String> technologyPermissions,
      List<String> rolePermissions,
      List<String> interviewTypesPermissions,
      List<String> profileStatusPermissions,
      List<String> interviewStatusPermissions,
      List<String> jobRolePermissions,
      List<String> profilePermissions,
      List<String> taskPermissions,
      List<String> bugPermissions,
      List<String> modulePermissions,
      List<String> stagespermissions);
  Future<dynamic> deleteRole(String roleId);
  Future<dynamic> viewRole(String roleId);
  // profiles apis
     Future<dynamic> deleteProfile(String profileId);
     Future<dynamic> viewProfile(String profileId);
  Future<dynamic> addProfile(String createdBy,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId);
  Future<dynamic> updateProfile(String createdBy,
      String profileId,
      String name,
      String phoneNumber,
      String careerStartedOn,
      String experienceStatus,
      String employmentStatus,
      String email,
      String mainTechnology,
      List<String> technology,
      List<String> superiorEmployees,
      String resume,
      String fileExtension,
      String source,
      String resumeFileName,String jobRoleId);
  Future<dynamic> addProfileComment(String employeeId, String profileId, String comment);
  Future<dynamic> updateProfileStatus(String employeeId, String profileId, String employmentStatus, String comment);
  Future<dynamic> requestResume(String profileId);
  Future<dynamic> addSystemTest(String employeeId, String scheduledBy, String profileId, String comment, String systemTestTime,
      List<String> supportingDocs);
  // interview schedule
  Future<dynamic> updateInterviewSchedule(String employeeId, String scheduledId, String interviewStatusId, String comments);
  // profile status api's
  Future<dynamic> viewAllProfileStatuses(String createdBy);
  Future<dynamic> getAllActiveProfileStatuses(String createdBy);
  Future<dynamic> addProfileStatuses(String statusName, bool mailSent, String mailSubject, String mailContent, String createdBy);
  Future<dynamic> viewProfileStatuses(String profileStatusId, String createdBy);
  Future<dynamic> updateProfileStatuses(String profileStatusId, String statusName, bool mailSent, String mailSubject, String mailContent,
      String createdBy);
  Future<dynamic> deleteProfileStatuses(String profileStatusId, String createdBy);
  Future<dynamic> activateOrDeActivateProfileStatuses(String profileStatusId, String createdBy);
  // interview types api's
  Future<dynamic> viewAllInterviewTypes(String createdBy);
  Future<dynamic> getAllActiveInterviewTypes(String createdBy);
  Future<dynamic> addInterviewTypes(String interviewType, String createdBy, bool systemTestRequired, bool mailSent, String mailSubject,
      String mailContent);
  Future<dynamic> viewInterviewTypes(String interviewTypeId, String createdBy);
  Future<dynamic> updateInterviewTypes(String interviewType, String interviewTypeId, String createdBy, bool systemTestRequired, bool mailSent,
      String mailSubject, String mailContent);
  Future<dynamic> deleteInterviewTypes(String interviewTypeId, String createdBy);
  Future<dynamic> activateOrDeActivateInterviewTypes(String interviewTypeId, String createdBy);
  // interview types api's
  Future<dynamic> viewAllInterviewStatuses(String createdBy);
  Future<dynamic> getAllActiveInterviewStatuses(String createdBy);
  Future<dynamic> addInterviewStatuses(String statusName, String createdBy);
  Future<dynamic> viewInterviewStatuses(String interviewStatusId, String createdBy);
  Future<dynamic> updateInterviewStatuses(String statusName, String interviewStatusId, String createdBy);
  Future<dynamic> deleteInterviewStatuses(String interviewStatusId, String createdBy);
  Future<dynamic> activateOrDeActivateInterviewStatuses(String interviewStatusId, String createdBy);
  Future<dynamic> addaTestCase(String title, String description, String steps, String expectedresults, String actualResults, String employeeId,
      String projectId, String moduleId, String taskId, String tcStatus);
  Future<dynamic> updateaTestCase(String testcaseid, String title, String description, String steps, String expectedresults, String actualResults,
      String employeeId, String tcStatus);
  Future<dynamic> deleteTestCase(String employeeId, String testCaseId);
  Future<dynamic> raiseaBug(String taskId, String employeeId, String projectId, String moduleId, String title, attachments, String description,
      String bugStatus, String priority, List assignedEmployeesList,String stageId);
  Future<dynamic> updateraiseaBug(String title, String bugid, String employeeId, String description, String bugStatus, String priority,
      List assignedEmployeesList, List attachments,String stageId);
  Future<dynamic> getbugslistbyproject(String employeeId, String projectId, String bugStatus, String title,String priority,String devStageId,String createdBy,List assignedTo);
  Future<dynamic> viewBug(String employeeId, String bugId);
  Future<dynamic> deleteBug(String employeeId, String bugId);
  // get apis
  Future<dynamic> getDesignations();
  Future<dynamic> getRoles();
  Future<dynamic> getTechnologies();
  Future<dynamic> getEmployees(String type);
  Future<dynamic> getInterviewSchedules();
  Future<dynamic> getInterviewerStatuses();
  Future<dynamic> getInterviewTypes();
  Future<dynamic> getProfileStatuses();
  Future<dynamic> createBoarding(String name, String description, String employeeId);
  Future<dynamic> getAllBoardings(String employeeId);
  Future<dynamic> deleteBoarding(String boardingId, String employeeId);
  Future<dynamic> updateBoarding(String boardingId, String employeeId, String boardName, String boardDescription);
  Future<dynamic> getBoardings();
  Future<dynamic> getDashboardBoardingsApi(String employeeId);
  Future<dynamic> updateTaskEstimationTimeApi(String employeeId, List taskEstimations);
  Future<dynamic> taskLogsList(String employeeId, String taskId, String searchType, String searchElement);
  Future<dynamic> taskDurationTracking(String employeeId, String taskId);
  Future<dynamic> taskDurationUpdate(String employeeId, String activityId, String startTime, String endTime);
  Future<dynamic> viewEmployeeProfile(String employeeId);
  Future<dynamic> addsprintapi(String userid, String sprintname, String sprintdesc, String sprintStartdate, String sprintEnddate, String projectId);
  Future<dynamic> getProjectModulewisetasksprintsapi(String sprintId, String employeeId, String projectId);
  Future<dynamic> getMultipleSprintsdataapi(String employeeId, String projectId);
  Future<dynamic> assigntaskstosprintapi(String sprintId, String employeeid, String projectId, List<TaskID> taskslistid);
  Future<dynamic> projectBasedSprintapi(String employeeId, List projectId);
  Future assianableEmployeesapi(String employeeId);
  Future addScrumapi(String employeeId, String scrumDate, String description, List projectId, List scrumList, String type);
  Future getTeamScrumCountapi(String employeeId, String filterType, String startDate, String endDate);
  Future getScrumDataapi(String employeeId, String scrumDate);
  // my scrum api
  Future<dynamic> viewMyScrum(String employeeId, String startDate, String endDate);
  Future<dynamic> viewMyScrumCounts(String employeeId, String filterType);
  Future<dynamic> viewMyScrumYearly(String employeeId, String year);
  Future<dynamic> updateBugStatus(String bugId, String employeeId, String bugStatus);
  Future deleteSprintApi(String sprintId, String employeeId, String projectId);
  Future<dynamic> viewteamwiseScrum(String employeeId, String startDate, String endDate, String filterMonth, String filterYear);
  Future getAllTeamScrumCountapi(String employeeId);
  Future addNewtaskToScrum(String employeeId, String assignedEmployeeId, String projectId, String moduleId, String taskId, String taskType,
      String scrumDate, String taskName, String taskDescription, String estimationTime);
  Future moduleBasedTaskListApi(String employeeId, String moduleId);
  Future<dynamic> getEmployeeScrumReport(String employeeId, String filterType);
  Future closeEmployeeScrumTasksApi(String employeeId,String comment);
  Future viewProjectWiseBugsCountApi(String employeeId);
  Future gettasktimingsApi(String employeeId, String taskId);
  Future<dynamic> addInterviewSchedule(String employeeId, String scheduledBy, String profileId, String interviewType, String interviewTime,
      String comments, String mailSubject, String mailBody);
  Future<dynamic> getAllProfiles(String createdBy, String name, String profileStatusId, List employeesList, List technologiesList, int pageCount,
      String startDate, String endDate,List jobRolesList);
  Future<dynamic> getTodayInterviewDashboard(String employeeId, String name, List<String> technologiesList, List<String> employeesList,
      int pageNumber);
  Future addJobRole(String employeeId, String role);
  Future viewalljobrolesApi(String employeeId);
  Future singlejobroleApi(String employeeId, String jobRoleId);
  Future editjobroleApi(String employeeId, String jobRoleId,String role);
  Future deletejobroleApi(String employeeId, String jobRoleId);
  Future activatedeactivatejobroleApi(String employeeId, String jobRoleId);
  Future viewactivejobrolesApi(String employeeId);
  Future updateprofilestatusorderApi(String employeeId,List profilesStatusList);
  Future profilestatusdisplaymodificationApi(String employeeId,String profileStatusId);
  Future inferioremployeeslistApi(String employeeId);
  Future viewAllStages(String employeeId);
  Future createStage(String name, String createdBy);
  Future updateStage(String stageId, String name,String employeeId);
  Future deleteStage(String stageId,String employeeId);
  Future viewStage(String stageId,String employeeId);
  Future activateDeactivateDevelopmentStageApi(String stageId,String employeeId);
  Future viewactivedevelopmentstagesApi(String employeeId);
  Future<dynamic> employeeLogin(String email, String password, String token);
  Future<dynamic> employeeLogout(String employeeId);
  Future<dynamic> tokenUpdate(String employeeId, String token);
 Future<dynamic> getBugsListApi(String employeeId,String taskId, String priority,String devStageId);
  Future<dynamic> createChatId(String employeeId,String chatId);
  Future<dynamic> getEmployeeAssociatedChats(String employeeId);
}
