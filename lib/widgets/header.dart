import 'package:aem/global_view_model/global_notifier.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/my_profile/ui/my_profile_page.dart';
import 'package:aem/screens/splash/splash_screen.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Header extends StatefulWidget {
  const Header({Key? key}) : super(key: key);

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  int popMenuValue = 0;
  String name = "", employeeId = "";
  final double _height = 60;

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    name = userViewModel.employeeName!;
    employeeId = userViewModel.employeeId!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: appBarBackground,
      height: _height,
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          PopupMenuButton<int>(
              offset: Offset(0.0, _height / 2),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              tooltip: "",
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [Text(name, style: dropDownStyle), const Icon(Icons.arrow_drop_down, color: dropDownColor)],
                ),
              ),
              onSelected: (value) => onSelected(context, value),
              itemBuilder: (context) => [
                    PopupMenuItem(
                        height: 30, padding: const EdgeInsets.fromLTRB(10, 5, 10, 5), child: Text(myProfile, style: popMenuHeader), value: 0),
                    PopupMenuItem(height: 30, padding: const EdgeInsets.fromLTRB(10, 5, 10, 5), child: Text(logout, style: popMenuHeader), value: 1),
                  ]),
        ],
      ),
    );
  }

  onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Navigator.push(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => const MyProfilePage(),
            transitionDuration: const Duration(seconds: 0),
            reverseTransitionDuration: const Duration(seconds: 0),
          ),
        );
        break;
      case 1:
        logoutDialog(context);
        break;
    }
  }

  logoutDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          SessionManager sessionManager = SessionManager();
          return AlertDialog(
            title: Text(logout, style: logoutHeader),
            titlePadding: const EdgeInsets.fromLTRB(15, 15, 15, 10),
            contentPadding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
            actionsPadding: const EdgeInsets.fromLTRB(0, 5, 0, 15),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Text(confirmLogout, style: logoutContentHeader),
            actions: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted),
                    child: TextButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return loader();
                            });
                        var vm = Provider.of<LoginNotifier>(context, listen: false);
                        vm.employeeLogoutAPI(employeeId).then((value) {
                          Navigator.of(context).pop();
                          if (vm.logoutResponse != null) {
                            if (vm.logoutResponse!.responseStatus == 1) {
                              showToast(vm.logoutResponse!.result!);
                              sessionManager.clearSession();
                              Provider.of<GlobalNotifier>(context, listen: false).resetExpandedState();
                              Provider.of<LoginNotifier>(context, listen: false).resetValues();
                              Navigator.of(context).pop();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder: (context, animation1, animation2) => const SplashScreen(),
                                    transitionDuration: const Duration(seconds: 0),
                                    reverseTransitionDuration: const Duration(seconds: 0),
                                  ),
                                      (route) => false);
                            } else {
                              showToast(vm.logoutResponse!.result!);
                            }
                          } else {
                            showToast(pleaseTryAgain);
                          }
                        });
                      },
                      child: const Text(
                        ok,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Material(
                  elevation: 5,
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: dashBoardCardOverDueText),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(cancel,
                        style: TextStyle(letterSpacing: 0.3, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }

}


