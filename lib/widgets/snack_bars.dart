import 'package:aem/utils/constants.dart';
import 'package:flutter/material.dart';

void snackBarLight(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.white,
      behavior: SnackBarBehavior.floating,
      content: Text(message, style: snackBarDarkText, softWrap: true, maxLines: 1, overflow: TextOverflow.ellipsis));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void snackBarDark(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.black,
      behavior: SnackBarBehavior.floating,
      content: Text(message, style: snackBarLightText, softWrap: true, maxLines: 1, overflow: TextOverflow.ellipsis));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
