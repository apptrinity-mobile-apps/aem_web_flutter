import 'package:flutter/material.dart';

Widget loader() {
  return const Center(
    child: SizedBox(height: 30, width: 30, child: CircularProgressIndicator()),
  );
}