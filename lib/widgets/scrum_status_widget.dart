import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/material.dart';

Widget getTextFromScrumStatusWidget(int status) {
  if (status == 0) {
    return Text(inActive, style: listItemsStyle.copyWith(color: footerText, fontWeight: FontWeight.w700));
  } else if (status == 1) {
    return Text(newStr, style: listItemsStyle.copyWith(color: newTaskText, fontWeight: FontWeight.w700));
  } else if (status == 2) {
    return Text(open, style: listItemsStyle.copyWith(color: newOnGoing, fontWeight: FontWeight.w700));
  } else if (status == 3) {
    return Text(projectCompleted, style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else if (status == 4) {
    return Text(close, style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else if (status == 5) {
    return Text(reOpen, style: listItemsStyle.copyWith(color: newReOpened, fontWeight: FontWeight.w700));
  } else if (status == 6) {
    return Text(delete, style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else if (status == 7) {
    return Text(hold, style: listItemsStyle.copyWith(color: dashBoardCardOverDueText, fontWeight: FontWeight.w700));
  } else {
    return Text(inActive, style: listItemsStyle.copyWith(color: footerText, fontWeight: FontWeight.w700));
  }
}

Widget getTextFromBugStatusWidget(String status) {
  if (status == 'New') {
    return Text('New', style: listItemsStyle.copyWith(color: footerText, fontWeight: FontWeight.w700));
  } else if (status == 'InProgress') {
    return Text('InProgress', style: listItemsStyle.copyWith(color: newTaskText, fontWeight: FontWeight.w700));
  } else if (status == 'Hold') {
    return Text('Hold', style: listItemsStyle.copyWith(color: newOnGoing, fontWeight: FontWeight.w700));
  } else if (status == 'Done') {
    return Text('Done', style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else if (status == 'In Review') {
    return Text('In Review', style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else if (status == 'Reopened') {
    return Text('Reopened', style: listItemsStyle.copyWith(color: newReOpened, fontWeight: FontWeight.w700));
  } else if (status == 'Closed') {
    return Text('Closed', style: listItemsStyle.copyWith(color: newCompleted, fontWeight: FontWeight.w700));
  } else {
    return Text('New', style: listItemsStyle.copyWith(color: footerText, fontWeight: FontWeight.w700));
  }
}