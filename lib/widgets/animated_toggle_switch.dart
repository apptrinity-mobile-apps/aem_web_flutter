import 'package:flutter/material.dart';

class AnimatedToggleSwitch extends StatefulWidget {
  final List<String> values;
  final ValueChanged<bool> onToggleCallback;
  final double width;
  final bool initialValue;
  final Color backgroundColor;
  final Color buttonColor;
  final Color textColor;

  const AnimatedToggleSwitch(
      {Key? key,
      required this.initialValue,
      required this.values,
      required this.onToggleCallback,
      this.width = 100,
      this.backgroundColor = const Color(0xFFe7e7e8),
      this.buttonColor = const Color(0xFFFFFFFF),
      this.textColor = const Color(0xFF000000)})
      : super(key: key);

  @override
  _AnimatedToggleSwitchState createState() => _AnimatedToggleSwitchState();
}

class _AnimatedToggleSwitchState extends State<AnimatedToggleSwitch> {
  bool initialPosition = true;
  final _formKey = GlobalKey<FormFieldState>();

  @override
  Widget build(BuildContext context) {
    setState(() {
      initialPosition = widget.initialValue;
    });
    return Container(
      key: _formKey,
      width: widget.width,
      margin: const EdgeInsets.all(5),
      child: Stack(
        children: <Widget>[
          InkWell(
            onTap: () {
              initialPosition = !initialPosition;
              widget.onToggleCallback(initialPosition);
              setState(() {});
            },
            child: Container(
              decoration: ShapeDecoration(
                color: widget.backgroundColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: List.generate(
                  widget.values.length,
                  (index) => Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 7, 10, 7),
                        child: Text(
                          widget.values[index],
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xAA000000),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          AnimatedAlign(
            duration: const Duration(milliseconds: 150),
            curve: Curves.decelerate,
            alignment: initialPosition ? Alignment.centerLeft : Alignment.centerRight,
            child: Material(
              borderRadius: BorderRadius.circular(5),
              shadowColor: Colors.black,
              elevation: 3,
              child: Container(
                width: widget.width / 2,
                decoration: ShapeDecoration(
                  color: widget.buttonColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 7, 10, 7),
                  child: Text(
                    initialPosition ? widget.values[0] : widget.values[1],
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 14,
                      color: widget.textColor,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                alignment: Alignment.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
