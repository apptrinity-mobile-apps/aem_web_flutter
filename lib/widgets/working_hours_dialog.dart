import 'package:aem/model/task_duration_track_response.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/project_details/view_model/project_details_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/date_time_formats.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/loader.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class WorkingHoursWidget extends StatefulWidget {
  final String? taskId, taskName;
  final int? estimatedTime;

  const WorkingHoursWidget({required this.taskId, required this.taskName, required this.estimatedTime, Key? key}) : super(key: key);

  @override
  State<WorkingHoursWidget> createState() => _WorkingHoursWidgetState();
}

class _WorkingHoursWidgetState extends State<WorkingHoursWidget> {
  final scrollController = ScrollController();
  final bugsScrollController = ScrollController();
  String employeeId = "";
  final _dateFormat = "hh:mm aa (dd MMM yyyy)";
  bool isLoading = true;

  @override
  void initState() {
    final LoginNotifier userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    Provider.of<ProjectDetailsNotifier>(context, listen: false).taskDurationTrackingAPI(employeeId, widget.taskId!).then((value) {
      setState(() {
        isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: const Icon(
                Icons.close,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 15),
              padding: const EdgeInsets.all(15),
              color: Colors.white,
              child: isLoading
                  ? loader()
                  : Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 7),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            widget.taskName!,
                                            maxLines: 1,
                                            softWrap: true,
                                            style: fragmentHeaderStyle.copyWith(
                                                letterSpacing: 1.35, color: buttonBg, fontSize: 18, fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            "$totalWorkingDuration ${secondsToHrs(widget.estimatedTime!)}", // converting minutes to seconds
                                            maxLines: 1,
                                            softWrap: true,
                                            style: fragmentHeaderStyle.copyWith(letterSpacing: 1.35, fontSize: 18, fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(dividerColor: buttonBg.withOpacity(0.2)),
                                      child: DataTable2(
                                        columnSpacing: 44,
                                        dividerThickness: 1,
                                        decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
                                        headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                                        headingRowHeight: 46,
                                        scrollController: scrollController,
                                        headingTextStyle: listHeaderStyle,
                                        dataTextStyle: listItemsStyle.copyWith(fontWeight: FontWeight.w500),
                                        columns: const <DataColumn2>[
                                          DataColumn2(label: Text(tableEmpName, softWrap: true, textAlign: TextAlign.start), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableStartTime, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableEndTime, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableActions, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.M),
                                          DataColumn2(label: Text(duration, softWrap: true, textAlign: TextAlign.end), size: ColumnSize.M),
                                        ],
                                        rows: listItems(Provider.of<ProjectDetailsNotifier>(context, listen: true).taskDurationList()),
                                        empty: Center(
                                          child: Text(noDataAvailable, style: logoutHeader),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  color: listItemColor,
                                  padding: const EdgeInsets.all(10),
                                  child: Text(
                                    "$totalDuration : ${calculatingTotalTasksDuration()}",
                                    maxLines: 1,
                                    softWrap: true,
                                    style: fragmentHeaderStyle.copyWith(letterSpacing: 0.6, fontSize: 15, fontWeight: FontWeight.w600),
                                  ),
                                  alignment: Alignment.centerRight,
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.only(top: 7),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            bugsWorkingDetails,
                                            maxLines: 1,
                                            softWrap: true,
                                            style: fragmentHeaderStyle.copyWith(letterSpacing: 1.15, fontSize: 16, fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(dividerColor: buttonBg.withOpacity(0.2)),
                                      child: DataTable2(
                                        columnSpacing: 44,
                                        dividerThickness: 1,
                                        decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
                                        headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                                        headingRowHeight: 46,
                                        scrollController: bugsScrollController,
                                        headingTextStyle: listHeaderStyle,
                                        dataTextStyle: listItemsStyle.copyWith(letterSpacing: 0.6, fontWeight: FontWeight.w500),
                                        columns: const <DataColumn2>[
                                          DataColumn2(label: Text(tableEmpName, softWrap: true, textAlign: TextAlign.start), size: ColumnSize.L),
                                          DataColumn2(label: Text(bugName, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableStartTime, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableEndTime, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.L),
                                          DataColumn2(label: Text(tableActions, softWrap: true, textAlign: TextAlign.center), size: ColumnSize.M),
                                          DataColumn2(label: Text(duration, softWrap: true, textAlign: TextAlign.end), size: ColumnSize.M),
                                        ],
                                        rows: bugListItems(Provider.of<ProjectDetailsNotifier>(context, listen: true).bugsDurationList()),
                                        empty: Center(
                                          child: Text(noDataAvailable, style: logoutHeader),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  color: listItemColor,
                                  padding: const EdgeInsets.all(10),
                                  child: Text(
                                    "$totalDuration : ${calculatingTotalBugsDuration()}",
                                    maxLines: 1,
                                    softWrap: true,
                                    style: fragmentHeaderStyle.copyWith(letterSpacing: 0.6, fontSize: 15, fontWeight: FontWeight.w600),
                                  ),
                                  alignment: Alignment.centerRight,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
            ),
          )
        ],
      ),
    );
  }

  List<DataRow> listItems(List<TaskDurationData> items) {
    List<TextEditingController> startTimeTextControllersList = [];
    List<TextEditingController> endTimeTextControllersList = [];
    return items.map<DataRow2>((e) {
      startTimeTextControllersList.add(TextEditingController(
          text: e.startTime! != "" ? dateTimeHMSDateFormat(e.startTime!) : DateFormat(_dateFormat).format(DateTime.now()).toString()));
      endTimeTextControllersList.add(TextEditingController(
          text: e.endTime! != "" ? dateTimeHMSDateFormat(e.endTime!) : DateFormat(_dateFormat).format(DateTime.now()).toString()));
      return DataRow2(
          color: MaterialStateColor.resolveWith((states) {
            if (items.indexOf(e) % 2 == 0) {
              return listItemColor;
            } else {
              return listItemColor1;
            }
          }),
          cells: [
            DataCell(
              Text(e.employeeName!, softWrap: true, textAlign: TextAlign.start),
            ),
            DataCell(
              e.isInEditMode!
                  ? DateTimeField(
                      format: DateFormat(_dateFormat),
                      controller: startTimeTextControllersList[items.indexOf(e)],
                      decoration: editTextEmployeesDecoration.copyWith(
                          hintText: tableStartTime,
                          contentPadding: const EdgeInsets.all(10),
                          suffixIcon: const Icon(
                            Icons.calendar_today_rounded,
                            color: textFieldBorder,
                          )),
                      style: textFieldTodoHintStyle,
                      resetIcon: null,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context, firstDate: DateTime(1900), initialDate: currentValue ?? DateTime.now(), lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                    )
                  : Text(e.startTime! != "" ? dateTimeHMSDateFormat(e.startTime!) : "", softWrap: true, textAlign: TextAlign.center),
            ),
            DataCell(
              e.isInEditMode!
                  ? DateTimeField(
                      format: DateFormat(_dateFormat),
                      controller: endTimeTextControllersList[items.indexOf(e)],
                      decoration: editTextEmployeesDecoration.copyWith(
                          hintText: tableEndTime,
                          contentPadding: const EdgeInsets.all(10),
                          suffixIcon: const Icon(Icons.calendar_today_rounded, color: textFieldBorder)),
                      style: textFieldTodoHintStyle,
                      resetIcon: null,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context, firstDate: DateTime(1900), initialDate: currentValue ?? DateTime.now(), lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                    )
                  : Text(e.endTime! != "" ? dateTimeHMSDateFormat(e.endTime!) : "", softWrap: true, textAlign: TextAlign.center),
            ),
            DataCell(
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  !e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: edit,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.transparent,
                                child: IconButton(
                                  // to disable - set onPressed to 'null'
                                  onPressed: e.endTime! == ""
                                      ? null
                                      : () {
                                          setState(() {
                                            e.isInEditMode = true;
                                          });
                                        },
                                  icon: const Icon(
                                    Icons.edit,
                                    size: 18,
                                    color: newTaskText,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: submit,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.transparent,
                                child: IconButton(
                                  onPressed: () async {
                                    debugPrint(
                                        "bugs list -- ${startTimeTextControllersList[items.indexOf(e)].text} == ${endTimeTextControllersList[items.indexOf(e)].text}");
                                    if (DateFormat(_dateFormat).parse(endTimeTextControllersList[items.indexOf(e)].text, true).toLocal().isBefore(
                                        DateFormat(_dateFormat).parse(startTimeTextControllersList[items.indexOf(e)].text, true).toLocal())) {
                                      showToast("Dates mismatch");
                                    } else {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return loader();
                                          });
                                      final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
                                      await viewModel
                                          .taskDurationUpdateAPI(
                                              employeeId,
                                              e.id!,
                                              dateToUtc(startTimeTextControllersList[items.indexOf(e)].text.trim()),
                                              dateToUtc(endTimeTextControllersList[items.indexOf(e)].text.trim()))
                                          .then((value) {
                                        Navigator.of(context).pop();
                                        if (value != null) {
                                          if (value.responseStatus == 1) {
                                            showToast(value.result!);
                                            e.isInEditMode = false;
                                            Provider.of<ProjectDetailsNotifier>(context, listen: false)
                                                .taskDurationTrackingAPI(employeeId, widget.taskId!);
                                          } else {
                                            showToast(value.result!);
                                          }
                                        } else {
                                          showToast(pleaseTryAgain);
                                        }
                                      });
                                    }
                                  },
                                  icon: const Icon(
                                    Icons.done,
                                    size: 18,
                                    color: newCompleted,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: cancel,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.white,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      e.isInEditMode = false;
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.close,
                                    size: 18,
                                    color: newReOpened,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                ],
              ),
            ),
            DataCell(
              Text(secondsToHrs(e.duration!), softWrap: true, textAlign: TextAlign.end),
            ),
          ]);
    }).toList();
  }

  List<DataRow> bugListItems(List<TaskDurationData> items) {
    List<TextEditingController> startTimeTextControllersList = [];
    List<TextEditingController> endTimeTextControllersList = [];
    return items.map<DataRow2>((e) {
      startTimeTextControllersList.add(TextEditingController(
          text: e.startTime! != "" ? dateTimeHMSDateFormat(e.startTime!) : DateFormat(_dateFormat).format(DateTime.now()).toString()));
      endTimeTextControllersList.add(TextEditingController(
          text: e.endTime! != "" ? dateTimeHMSDateFormat(e.endTime!) : DateFormat(_dateFormat).format(DateTime.now()).toString()));
      return DataRow2(
          color: MaterialStateColor.resolveWith((states) {
            if (items.indexOf(e) % 2 == 0) {
              return listItemColor;
            } else {
              return listItemColor1;
            }
          }),
          cells: [
            DataCell(
              Text(e.employeeName!, softWrap: true, textAlign: TextAlign.start),
            ),
            DataCell(
              Text(e.bugName!, softWrap: true, textAlign: TextAlign.center),
            ),
            DataCell(
              e.isInEditMode!
                  ? DateTimeField(
                      format: DateFormat(_dateFormat),
                      controller: startTimeTextControllersList[items.indexOf(e)],
                      decoration: editTextEmployeesDecoration.copyWith(
                          hintText: tableStartTime,
                          contentPadding: const EdgeInsets.all(10),
                          suffixIcon: const Icon(Icons.calendar_today_rounded, color: textFieldBorder)),
                      style: textFieldTodoHintStyle,
                      resetIcon: null,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context, firstDate: DateTime(1900), initialDate: currentValue ?? DateTime.now(), lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                    )
                  : Text(e.startTime! != "" ? dateTimeHMSDateFormat(e.startTime!) : "", softWrap: true, textAlign: TextAlign.center),
            ),
            DataCell(
              e.isInEditMode!
                  ? DateTimeField(
                      format: DateFormat(_dateFormat),
                      controller: endTimeTextControllersList[items.indexOf(e)],
                      decoration: editTextEmployeesDecoration.copyWith(
                          hintText: tableEndTime,
                          contentPadding: const EdgeInsets.all(10),
                          suffixIcon: const Icon(Icons.calendar_today_rounded, color: textFieldBorder)),
                      style: textFieldTodoHintStyle,
                      resetIcon: null,
                      onShowPicker: (context, currentValue) async {
                        final date = await showDatePicker(
                            context: context, firstDate: DateTime(1900), initialDate: currentValue ?? DateTime.now(), lastDate: DateTime(2100));
                        if (date != null) {
                          final time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                          );
                          return DateTimeField.combine(date, time);
                        } else {
                          return currentValue;
                        }
                      },
                    )
                  : Text(e.endTime! != "" ? dateTimeHMSDateFormat(e.endTime!) : "", softWrap: true, textAlign: TextAlign.center),
            ),
            DataCell(
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  !e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: edit,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.transparent,
                                child: IconButton(
                                  // to disable - set onPressed to 'null'
                                  onPressed: e.endTime! == ""
                                      ? null
                                      : () {
                                          setState(() {
                                            e.isInEditMode = true;
                                          });
                                        },
                                  icon: const Icon(
                                    Icons.edit,
                                    size: 18,
                                    color: newTaskText,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: submit,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.transparent,
                                child: IconButton(
                                  onPressed: () async {
                                    debugPrint(
                                        "bugs list -- ${startTimeTextControllersList[items.indexOf(e)].text} == ${endTimeTextControllersList[items.indexOf(e)].text}");
                                    if (DateFormat(_dateFormat).parse(endTimeTextControllersList[items.indexOf(e)].text, true).toLocal().isBefore(
                                        DateFormat(_dateFormat).parse(startTimeTextControllersList[items.indexOf(e)].text, true).toLocal())) {
                                      showToast("Dates mismatch");
                                    } else {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return loader();
                                          });
                                      final viewModel = Provider.of<ProjectDetailsNotifier>(context, listen: false);
                                      await viewModel
                                          .taskDurationUpdateAPI(
                                              employeeId,
                                              e.id!,
                                              dateToUtc(startTimeTextControllersList[items.indexOf(e)].text.trim()),
                                              dateToUtc(endTimeTextControllersList[items.indexOf(e)].text.trim()))
                                          .then((value) {
                                        Navigator.of(context).pop();
                                        if (value != null) {
                                          if (value.responseStatus == 1) {
                                            showToast(value.result!);
                                            e.isInEditMode = false;
                                            Provider.of<ProjectDetailsNotifier>(context, listen: false)
                                                .taskDurationTrackingAPI(employeeId, widget.taskId!);
                                          } else {
                                            showToast(value.result!);
                                          }
                                        } else {
                                          showToast(pleaseTryAgain);
                                        }
                                      });
                                    }
                                  },
                                  icon: const Icon(
                                    Icons.done,
                                    size: 18,
                                    color: newCompleted,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                  e.isInEditMode!
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Tooltip(
                            message: cancel,
                            preferBelow: false,
                            decoration: toolTipDecoration,
                            textStyle: toolTipStyle,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 18,
                              child: Material(
                                elevation: 0.0,
                                shape: const CircleBorder(side: BorderSide(color: Colors.black)),
                                clipBehavior: Clip.antiAlias,
                                color: Colors.white,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      e.isInEditMode = false;
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.close,
                                    size: 18,
                                    color: newReOpened,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(),
                ],
              ),
            ),
            DataCell(
              Text(secondsToHrs(e.duration!), softWrap: true, textAlign: TextAlign.end),
            ),
          ]);
    }).toList();
  }

  String calculatingTotalTasksDuration() {
    var total = 0;
    Provider.of<ProjectDetailsNotifier>(context, listen: true).taskDurationList().forEach((element) {
      total = total + element.duration!;
    });
    return secondsToHrs(total);
  }

  String calculatingTotalBugsDuration() {
    var total = 0;
    Provider.of<ProjectDetailsNotifier>(context, listen: true).bugsDurationList().forEach((element) {
      total = total + element.duration!;
    });
    return secondsToHrs(total);
  }

  String dateToUtc(String date) {
    String _date = "";
    // converting to date to datetime format
    DateTime tempDate = DateFormat(_dateFormat).parse(date, false).toLocal();
    // converting to utc format
    var utcDate = tempDate.toUtc();
    // converting utc to [dateFormat] format
    _date = DateFormat(_dateFormat).format(utcDate);
    return _date;
  }
}
