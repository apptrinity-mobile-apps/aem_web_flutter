import 'package:aem/model/nav_menu_item.dart';
import 'package:aem/utils/constants.dart';
import 'package:flutter/material.dart';

class SideBarItem extends StatelessWidget {
  const SideBarItem({
    Key? key,
    required this.items,
    required this.index,
    this.onSelected,
    required this.selectedRoute,
    this.depth = 0,
    this.iconColor,
    this.activeIconColor,
    required this.textStyle,
    required this.activeTextStyle,
    required this.backgroundColor,
    required this.activeBackgroundColor,
    required this.borderColor,
  }) : super(key: key);

  final NavMenuItem items;
  final int index;
  final void Function(NavMenuItem item)? onSelected;
  final String selectedRoute;
  final int depth;
  final Color? iconColor;
  final Color? activeIconColor;
  final TextStyle textStyle;
  final TextStyle activeTextStyle;
  final Color backgroundColor;
  final Color activeBackgroundColor;
  final Color borderColor;

  // bool get isLast => index == items.length - 1;

  @override
  Widget build(BuildContext context) {
    // if (depth > 0 && isLast) {
    //   return _buildTiles(context, items[index]);
    // }
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: borderColor))),
      child: _buildTiles(context, items),
    );
  }

  Widget _buildTiles(BuildContext context, NavMenuItem item) {

    // bool selected = _isSelected(selectedRoute, [item]);
    bool selected = _isSelected(selectedRoute, [item]);
    return ListTile(
      contentPadding: _getTilePadding(depth),
      leading: _buildIcon(item.icon,item.tooltip_title,selected),
      title: _buildTitle(item.title, selected),
      selected: selected,
      tileColor: backgroundColor,
      selectedTileColor: activeBackgroundColor,
      onTap: () {
        if (onSelected != null) {
          onSelected!(item);
        }
      },
    );
  }

  bool _isSelected(String route, List<NavMenuItem> items) {
    for (final item in items) {
      if (item.route == route) {
        return true;
      }
    }
    return false;
  }

  Widget _buildIcon(String? icon,String? title,[bool selected = false]) {
    return icon != null ? Tooltip(message: title,
        preferBelow: false,
        decoration: toolTipDecoration,
        textStyle: toolTipStyle,child: Image.asset(icon, height: 50, width: 50,color: selected ?activeIconColor:iconColor,)) : const SizedBox();
  }

  Widget _buildTitle(String title, [bool selected = false]) {
    return Text(title, style: selected ? activeTextStyle : textStyle);
  }

  EdgeInsets _getTilePadding(int depth) {
    return EdgeInsets.only(left: 10.0 + 10.0 * depth, right: 10.0);
  }
}
