import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  const Footer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenConfig.width(context),
      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
      alignment: Alignment.center,
      child: SelectableText(
        copyRightsLine,
        style: footerStyle,
      ),
    );
  }
}
