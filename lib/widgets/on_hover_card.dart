import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnHoverCard extends StatefulWidget {
  final Widget Function(bool isHovered) builder;

  const OnHoverCard({Key? key, required this.builder}) : super(key: key);

  @override
  _OnHoverCardState createState() => _OnHoverCardState();
}

class _OnHoverCardState extends State<OnHoverCard> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => onEntered(true),
      onExit: (event) => onEntered(false),
      child: widget.builder(isHovered),
    );
  }

  onEntered(bool entered) {
    setState(() {
      isHovered = entered;
    });
  }
}
