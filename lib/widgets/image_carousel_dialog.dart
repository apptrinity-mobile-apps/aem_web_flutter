import 'dart:convert';

import 'package:aem/utils/app_scroll_behaviour.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

showAttachmentsDialog(BuildContext context, List<String>? attachments, int pos) {
  int currentPage = pos;
  PageController _pageController = PageController(initialPage: currentPage);
  List<Widget> _pages = [];
  List<String>? imageUrls = attachments;

  _pages = imageUrls!.map((url) {
    return imageViews(context, url);
  }).toList();

  showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(builder: (context, _setState) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(10),
            content: Container(
              color: CupertinoColors.white,
              height: ScreenConfig.height(context) / 1.25,
              width: ScreenConfig.width(context) / 1.35,
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                scrollBehavior: AppScrollBehavior(),
                home: PageView.builder(
                  physics: const BouncingScrollPhysics(),
                  controller: _pageController,
                  itemCount: _pages.length,
                  pageSnapping: true,
                  itemBuilder: (BuildContext context, int index) {
                    return _pages[index % _pages.length];
                  },
                  onPageChanged: (value) {
                    _setState(() {
                      currentPage = value;
                    });
                  },
                ),
              ),
            ),
          );
        });
      });
}

showLocalAttachmentsDialog(BuildContext context, List<String>? attachments, int pos) {
  int currentPage = pos;
  PageController _pageController = PageController(initialPage: currentPage);
  List<Widget> _pages = [];
  List<String>? imageUrls = attachments;

  _pages = imageUrls!.map((url) {
    return localImageViews(context, url);
  }).toList();

  showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(builder: (context, _setState) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(10),
            content: Container(
              color: CupertinoColors.white,
              height: ScreenConfig.height(context) / 1.25,
              width: ScreenConfig.width(context) / 1.35,
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                scrollBehavior: AppScrollBehavior(),
                home: PageView.builder(
                  physics: const BouncingScrollPhysics(),
                  controller: _pageController,
                  itemCount: _pages.length,
                  pageSnapping: true,
                  itemBuilder: (BuildContext context, int index) {
                    return _pages[index % _pages.length];
                  },
                  onPageChanged: (value) {
                    _setState(() {
                      currentPage = value;
                    });
                  },
                ),
              ),
            ),
          );
        });
      });
}

Widget imageViews(BuildContext context, String url) {
  return Image.network(
    url,
    height: ScreenConfig.height(context),
    width: ScreenConfig.width(context),
    fit: BoxFit.fill,
    errorBuilder: (context, url, error) => const Icon(Icons.error),
    loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
      if (loadingProgress == null) return child;
      return Center(
        child: CircularProgressIndicator(
          value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes! : null,
        ),
      );
    },
    filterQuality: FilterQuality.high,
  );
}

Widget localImageViews(BuildContext context, String url) {
  return Image.memory(
    base64Decode(url),
    height: ScreenConfig.height(context),
    width: ScreenConfig.width(context),
    fit: BoxFit.fill,
    errorBuilder: (context, url, error) => const Icon(Icons.error),
    filterQuality: FilterQuality.high,
  );
}
