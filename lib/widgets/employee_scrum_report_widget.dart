import 'package:aem/model/employee_scrum_report.dart';
import 'package:aem/screens/scrum/view_model/scrum_notifier.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/screen_config.dart';
import 'package:aem/utils/strings.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EmployeeScrumReportWidget extends StatefulWidget {
  final String? employeeId;

  const EmployeeScrumReportWidget({required this.employeeId, Key? key}) : super(key: key);

  @override
  State<EmployeeScrumReportWidget> createState() => _EmployeeScrumReportWidgetState();
}

class _EmployeeScrumReportWidgetState extends State<EmployeeScrumReportWidget> {
  final scrollController = ScrollController();
  final bugsScrollController = ScrollController();
  String employeeId = "";
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenConfig.height(context),
      width: ScreenConfig.width(context),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(
                empScrumReport,
                maxLines: 1,
                softWrap: true,
                style: fragmentHeaderStyle.copyWith(letterSpacing: 1.35, color: buttonBg, fontSize: 18, fontWeight: FontWeight.w600),
              ),
              Material(
                type: MaterialType.transparency,
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(
                    Icons.close,
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Container(
                      margin: const EdgeInsets.only(top: 7),
                      child: ClipRRect(
                        borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                        child: Theme(
                          data: Theme.of(context).copyWith(dividerColor: buttonBg.withOpacity(0.2)),
                          child: DataTable2(
                            columnSpacing: 20,
                            dividerThickness: 1,
                            decoration: BoxDecoration(border: Border.all(color: buttonBg.withOpacity(0.2), width: 1)),
                            headingRowColor: MaterialStateColor.resolveWith((states) => buttonBg),
                            headingRowHeight: 46,
                            scrollController: scrollController,
                            headingTextStyle: listHeaderStyle,
                            dataTextStyle: listItemsStyle.copyWith(fontWeight: FontWeight.w500),
                            smRatio: 0.4,
                            lmRatio: 2,
                            columns: const <DataColumn2>[
                              DataColumn2(label: Text(sno, softWrap: true, textAlign: TextAlign.start), size: ColumnSize.S),
                              DataColumn2(label: Text("$employee $name", softWrap: true, textAlign: TextAlign.start), size: ColumnSize.L),
                              DataColumn2(label: Center(child: Text(projectNew, softWrap: true, textAlign: TextAlign.center)), size: ColumnSize.S),
                              DataColumn2(
                                  label: Center(child: Text(projectOnGoing, softWrap: true, textAlign: TextAlign.center)), size: ColumnSize.S),
                              DataColumn2(
                                  label: Center(child: Text(projectCompleted, softWrap: true, textAlign: TextAlign.center)), size: ColumnSize.S),
                            ],
                            rows: listItems(Provider.of<ScrumNotifier>(context, listen: true).getEmployeeScrumReport),
                            empty: Center(
                              child: Text(noDataAvailable, style: logoutHeader),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  List<DataRow> listItems(List<TasksCounts> items) {
    return items.map<DataRow2>((e) {
      return DataRow2(specificRowHeight: 60, cells: [
        DataCell(
          Text((items.indexOf(e) + 1).toString(), softWrap: true, textAlign: TextAlign.start),
        ),
        DataCell(
          Text(e.employeeName!, softWrap: true, textAlign: TextAlign.start),
        ),
        DataCell(
          Center(child: Text(e.newTasksCount.toString(), softWrap: true, textAlign: TextAlign.center)),
        ),
        DataCell(
          Center(child: Text(e.pendingTasksCount.toString(), softWrap: true, textAlign: TextAlign.center)),
        ),
        DataCell(
          Center(child: Text(e.totalTasksCount.toString(), softWrap: true, textAlign: TextAlign.center)),
        ),
      ]);
    }).toList();
  }
}
