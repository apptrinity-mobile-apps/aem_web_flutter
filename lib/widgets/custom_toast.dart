import 'package:fluttertoast/fluttertoast.dart';

void showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      gravity: ToastGravity.BOTTOM_LEFT,
      toastLength: Toast.LENGTH_LONG,
      webShowClose: false,
      webPosition: "center",
      timeInSecForIosWeb: 5,
      webBgColor: "linear-gradient(to right, #6D6D6D, #6D6D6D)");
}
