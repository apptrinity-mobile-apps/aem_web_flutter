import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget selectedItem(String i) {
  return Container(
    padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
    margin: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: newCompleted.withOpacity(0.7)),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [Text(i, textAlign: TextAlign.center, style: textFieldStyle)],
    ),
  );
}