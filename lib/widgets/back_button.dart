import 'package:aem/utils/colors.dart';
import 'package:aem/utils/strings.dart';
import 'package:flutter/material.dart';

Widget backButton() {
  return Padding(
    padding: const EdgeInsets.only(top: 5, bottom: 5, right: 5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: const [
        Icon(
          Icons.arrow_back_ios_rounded,
          size: 15,
          color: editText,
        ),
        Text(
          back,
          style: TextStyle(letterSpacing: 0.48, fontFamily: "Poppins", fontWeight: FontWeight.w400, color: editText, fontSize: 14),
        )
      ],
    ),
  );
}
