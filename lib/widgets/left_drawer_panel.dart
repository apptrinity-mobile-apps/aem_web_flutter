import 'dart:convert';

import 'package:aem/global_view_model/global_notifier.dart';
import 'package:aem/model/nav_menu_item.dart';
import 'package:aem/model/nav_menu_main_item.dart';
import 'package:aem/screens/boardings/ui/boardings_fragment.dart';
import 'package:aem/screens/chat/ui/chat_fragment.dart';
import 'package:aem/screens/dashboard/ui/dashboard_fragment.dart';
import 'package:aem/screens/dashboard/ui/home_screen.dart';
import 'package:aem/screens/dashboard/view_model/dashboard_notifier.dart';
import 'package:aem/screens/designations/ui/designations_fragment.dart';
import 'package:aem/screens/development_stages/ui/stagesFragment.dart';
import 'package:aem/screens/employees/ui/employees_fragment.dart';
import 'package:aem/screens/interview_status/ui/interview_status_fragment.dart';
import 'package:aem/screens/interview_types/ui/interview_types_fragment.dart';
import 'package:aem/screens/job_role/ui/job_role_fragment.dart';
import 'package:aem/screens/login/view_model/login_notifier.dart';
import 'package:aem/screens/profile_status/ui/profile_statuses_fragment.dart';
import 'package:aem/screens/profiles/ui/profiles_dashboard_fragment.dart';
import 'package:aem/screens/profiles/ui/profiles_fragment.dart';
import 'package:aem/screens/projects/ui/projects_fragment.dart';
import 'package:aem/screens/roles/ui/roles_fragment.dart';
import 'package:aem/screens/scrum/ui/add_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/allteam_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/my_scrum_fragment.dart';
import 'package:aem/screens/scrum/ui/team_scrum_fragment.dart';
import 'package:aem/screens/technologies/ui/technologies_fragment.dart';
import 'package:aem/utils/colors.dart';
import 'package:aem/utils/constants.dart';
import 'package:aem/utils/routes.dart';
import 'package:aem/utils/session_manager.dart';
import 'package:aem/utils/strings.dart';
import 'package:aem/widgets/custom_toast.dart';
import 'package:aem/widgets/side_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LeftDrawer extends StatefulWidget {
  final double size;
  final String? selectedMenu;
  final Function(Widget) onSelectedChanged;

  const LeftDrawer({Key? key, required this.size, required this.onSelectedChanged, this.selectedMenu}) : super(key: key);

  @override
  _LeftDrawerState createState() => _LeftDrawerState();
}

class _LeftDrawerState extends State<LeftDrawer> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  List<NavMenuItem> navProjList = [], navHRList = [], navScrumList = [];
  List<MainNavMenu> mNavList = [];
  String currentRoute = RouteNames.dashBoardRoute, employeeId = "", mMainMenu = "", innerActive = "";
  bool hasRolesViewPer = false,
      hasSprintViewPer = false,
      hasScrumViewPer = false,
      hasEmployeesViewPer = false,
      hasProjectViewPer = false,
      hasBoardingViewPer = false,
      hasDesignationViewPer = false,
      hasTechnologyViewPer = false,
      hasInterviewTypesViewPer = false,
      hasProfileStatusViewPer = false,
      hasInterviewStatusViewPer = false,
      hasjobRoleViewPer = false,
      hasProfileViewPer = false,
      hasStagesViewPer = false;
  late SessionManager sessionManager;
  late DashBoardNotifier viewModel;
  late LoginNotifier userViewModel;
  double leftPanelSize = 0.00;
  @override
  bool get wantKeepAlive => true;
  int menu_expand_collapse = 0;
  int scrum_expand_collapse = 0;
  bool _isPlay = false;
  late AnimationController _controller;
  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<DashBoardNotifier>(context, listen: false);
    userViewModel = Provider.of<LoginNotifier>(context, listen: false);
    employeeId = userViewModel.employeeId!;
    //getEmployeePermissions();
    var arr = widget.selectedMenu!.split('-');
    mMainMenu = arr[0];
    innerActive = arr[1];
    currentRoute = mMainMenu;
    leftPanelSize = widget.size;
    _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _isPlay = Provider.of<GlobalNotifier>(context, listen: false).getNavMenuStyleStates()!;

    if (_isPlay == false) {
      _controller.forward();
      _isPlay = true;
      setState(() {
        menu_expand_collapse = 1;
        leftPanelSize = 110;
        mNavList.clear();
        navProjList.clear();
        navHRList.clear();
        navScrumList.clear();

        getEmployeePermissions();
      });
    } else {
      _controller.reverse();
      _isPlay = false;
      setState(() {
        menu_expand_collapse = 0;
        leftPanelSize = 250;
        mNavList.clear();
        navProjList.clear();
        navHRList.clear();
        navScrumList.clear();

        getEmployeePermissions();
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Container(
      width: leftPanelSize,
      color: appBackground,
      child: ClipRect(
        child: SizedOverflowBox(
          size: Size(leftPanelSize, double.infinity),
          child: sideBar(),
        ),
      ),
    );
  }

  Widget sideBar() {
    return SideBar(
      backgroundColor: appBackground,
      activeTextStyle: sideMenuSelectedStyle,
      textStyle: sideMenuUnSelectedStyle,
      activeBackgroundColor: Colors.transparent,
      borderColor: Colors.transparent,
      items: mNavList,
      selectedRoute: currentRoute,
      /*iconColor: Colors.transparent,*/
      activeIconColor: Colors.lime,
      onSelected: (item) {
        if (item.route != null) {
          setState(() {
            currentRoute = item.route!;
            widget.onSelectedChanged(fragments());
          });
        }
      },
      header: Container(
        height: 100,
        color: appBarBackground,
        child: Stack(
          children: [
            Positioned(
              child: InkWell(
                onTap: () {
                  setState(() {
                    currentRoute = RouteNames.dashBoardRoute;
                    widget.onSelectedChanged(fragments());
                  });
                },
                child: Row(
                  children: [
                    menu_expand_collapse == 0
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Image.asset(
                              "assets/images/logo.png",
                              width: 120,
                              height: 150,
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: Image.asset(
                              "assets/images/logo.png",
                              width: 60,
                              height: 60,
                            ),
                          ),
                    /*InkWell( onTap: () {
                 if(menu_expand_collapse == 0){
                    setState(() {
                      menu_expand_collapse = 1;
                      leftPanelSize = 110;
                      mNavList.clear();
                      navProjList.clear();
                      navHRList.clear();

                      getEmployeePermissions();
                    });
                  }else{
                    setState(() {
                      menu_expand_collapse = 0;
                      leftPanelSize = 250;
                      mNavList.clear();
                      navProjList.clear();
                      navHRList.clear();
                      getEmployeePermissions();
                    });
                  }
                },
                  child: */
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: GestureDetector(
                        onTap: () {
                          if (_isPlay == false) {
                            Provider.of<GlobalNotifier>(context, listen: false).setNavMenuStyleStates(false);
                            _controller.forward();
                            _isPlay = true;
                            setState(() {
                              menu_expand_collapse = 1;
                              leftPanelSize = 110;
                              mNavList.clear();
                              navProjList.clear();
                              navHRList.clear();
                              navScrumList.clear();
                              getEmployeePermissions();
                            });
                          } else {
                            Provider.of<GlobalNotifier>(context, listen: false).setNavMenuStyleStates(true);
                            _controller.reverse();
                            _isPlay = false;
                            setState(() {
                              menu_expand_collapse = 0;
                              leftPanelSize = 250;
                              mNavList.clear();
                              navProjList.clear();
                              navHRList.clear();
                              navScrumList.clear();
                              getEmployeePermissions();
                            });
                          }
                        },
                        child: AnimatedIcon(
                          icon: AnimatedIcons.close_menu,
                          progress: _controller,
                          size: 40,
                          color: sideMenuSelected,
                        ),
                      ),
                    ),
                    /*),*/
                  ],
                ),
              ),
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
            )
          ],
        ),
      ),
    );
  }

  Widget fragments() {
    // debugPrint(currentRoute);
    if (currentRoute == RouteNames.employeesHomeRoute) {
      if (hasEmployeesViewPer) {
        currentRoute = RouteNames.employeesHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.stagesRoute) {
      if (hasStagesViewPer) {
        currentRoute = RouteNames.stagesRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    }else if (currentRoute == RouteNames.scrumAddRoute) {
      if (hasScrumViewPer) {
        currentRoute = RouteNames.scrumAddRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.scrumTeamRoute) {
      if (hasScrumViewPer) {
        currentRoute = RouteNames.scrumTeamRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.technologyHomeRoute) {
      if (hasTechnologyViewPer) {
        currentRoute = RouteNames.technologyHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.boardingHomeRoute) {
      if (hasBoardingViewPer) {
        currentRoute = RouteNames.boardingHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.projectHomeRoute) {
      if (hasProjectViewPer) {
        currentRoute = RouteNames.projectHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.designationHomeRoute) {
      if (hasDesignationViewPer) {
        currentRoute = RouteNames.designationHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.roleHomeRoute) {
      if (hasRolesViewPer) {
        currentRoute = RouteNames.roleHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.profileHomeRoute) {
      if (hasProfileViewPer) {
        currentRoute = RouteNames.profileHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.profilesDashboardHomeRoute) {
      if (hasProfileViewPer) {
        currentRoute = RouteNames.profilesDashboardHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.profileStatusesHomeRoute) {
      if (hasProfileStatusViewPer) {
        currentRoute = RouteNames.profileStatusesHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.jobRoleHomeRoute) {
      if (hasjobRoleViewPer) {
        currentRoute = RouteNames.jobRoleHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.interviewerStatusHomeRoute) {
      if (hasInterviewStatusViewPer) {
        currentRoute = RouteNames.interviewerStatusHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.interviewTypesHomeRoute) {
      if (hasInterviewTypesViewPer) {
        currentRoute = RouteNames.interviewTypesHomeRoute;
      } else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }
    } else if (currentRoute == RouteNames.chatRoute) {
      // if (hasEmployeesViewPer) {
        currentRoute = RouteNames.chatRoute;
      /*} else {
        currentRoute = RouteNames.dashBoardRoute;
        showToast(noPermissionToAccess);
      }*/
    }
    if (innerActive == "innerpage") {
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => HomeScreen(menu: currentRoute),
            transitionDuration: const Duration(seconds: 0),
            reverseTransitionDuration: const Duration(seconds: 0),
          ),
          (route) => false);
    }
    switch (currentRoute) {
      case RouteNames.dashBoardRoute:
        {
          return const DashboardFragment();
        }
      case RouteNames.chatRoute:
        {
          return const ChatScreen();
        }
      case RouteNames.stagesRoute:
        {
          return DevelopmentStagesFragment();
        }
      case RouteNames.scrumAddRoute:
        {
          return AddScrumFragement();
        }
      case RouteNames.scrumMyScrumRoute:
        {
          return const MyScrumFragment(employeeId: "");
        }
      case RouteNames.scrumTeamRoute:
        {
          return const AllTeamScrumFragment(employeeId: "");
        }
      case RouteNames.scrumEmployeeRoute:
        {
          return const TeamScrumFragment(employeeId: "");
        }
      case RouteNames.employeesHomeRoute:
        {
          return const EmployeeFragment();
        }
      case RouteNames.technologyHomeRoute:
        {
          return const TechnologyFragment();
        }
      case RouteNames.boardingHomeRoute:
        {
          return const BoardingsFragment();
        }
      case RouteNames.projectHomeRoute:
        {
          return const ProjectFragment();
        }
      case RouteNames.designationHomeRoute:
        {
          return const DesignationsFragment();
        }
      case RouteNames.roleHomeRoute:
        {
          return const RolesFragment();
        }
      case RouteNames.profileHomeRoute:
        {
          return const ProfilesFragment();
        }
      case RouteNames.profilesDashboardHomeRoute:
        {
          return const ProfilesDashboardFragment();
        }
      case RouteNames.profileStatusesHomeRoute:
        {
          return const ProfileStatusesFragment();
        }
      case RouteNames.interviewerStatusHomeRoute:
        {
          return const InterviewStatusFragment();
        }
      case RouteNames.jobRoleHomeRoute:
        {
          return const JobRoleFragment();
        }
      case RouteNames.interviewTypesHomeRoute:
        {
          return const InterviewTypesFragment();
        }
      default:
        {
          return const DashboardFragment();
        }
    }
  }

  void getEmployeePermissions() {
    navProjList.add(NavMenuItem(
        title: menu_expand_collapse == 0 ? navMenuDashboard : "",
        icon: "assets/images/dashboard_rounded.png",
        route: RouteNames.dashBoardRoute,
        tooltip_title: navMenuDashboard));
    navProjList.add(NavMenuItem(
        title: menu_expand_collapse == 0 ? navMenuChat : "",
        icon: "assets/images/dashboard_rounded.png",
        route: RouteNames.chatRoute,
        tooltip_title: navMenuChat));
    navScrumList.add(NavMenuItem(
        title: menu_expand_collapse == 0 ? "My " + navMenuScrum : "",
        icon: "assets/images/employees_rounded.png",
        route: RouteNames.scrumMyScrumRoute,
        tooltip_title: "My " + navMenuScrum));
    Future.delayed(const Duration(milliseconds: 0), () async {
      await viewModel.employeePermissionsAPI(employeeId).then((value) {
        if (value != null) {
          if (value.responseStatus == 1) {
            sessionManager.saveEmployeePermissions(jsonEncode(value.employeePermissions!));
            userViewModel.getEmployeeDetails();
            setState(() {
              if (value.employeePermissions!.sprintPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.sprintPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasSprintViewPer = true;
                  }
                }
              } else {
                hasSprintViewPer = false;
              }

              if (value.employeePermissions!.developmentStatgesPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.developmentStatgesPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasStagesViewPer = true;
                  }
                }
              } else {
                hasStagesViewPer = false;
              }

              if (value.employeePermissions!.scrumPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.scrumPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasScrumViewPer = true;
                  }
                }
              } else {
                hasScrumViewPer = false;
              }
              if (value.employeePermissions!.employeePermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.employeePermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasEmployeesViewPer = true;
                  }
                }
              } else {
                hasEmployeesViewPer = false;
              }
              if (value.employeePermissions!.projectPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.projectPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasProjectViewPer = true;
                  }
                }
              } else {
                hasProjectViewPer = false;
              }
              if (value.employeePermissions!.boardingPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.boardingPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasBoardingViewPer = true;
                  }
                }
              } else {
                hasBoardingViewPer = false;
              }
              if (value.employeePermissions!.designationPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.designationPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasDesignationViewPer = true;
                  }
                }
              } else {
                hasDesignationViewPer = false;
              }
              if (value.employeePermissions!.technologyPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.technologyPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasTechnologyViewPer = true;
                  }
                }
              } else {
                hasTechnologyViewPer = false;
              }
              if (value.employeePermissions!.rolePermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.rolePermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasRolesViewPer = true;
                  }
                }
              } else {
                hasRolesViewPer = false;
              }
              if (value.employeePermissions!.interviewTypesPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.interviewTypesPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasInterviewTypesViewPer = true;
                  }
                }
              } else {
                hasInterviewTypesViewPer = false;
              }
              if (value.employeePermissions!.profileStatusPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.profileStatusPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasProfileStatusViewPer = true;
                  }
                }
              } else {
                hasProfileStatusViewPer = false;
              }
              if (value.employeePermissions!.interviewStatusPermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.interviewStatusPermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasInterviewStatusViewPer = true;
                  }
                }
              } else {
                hasInterviewStatusViewPer = false;
              }
              if (value.employeePermissions!.jobRolePermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.jobRolePermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasjobRoleViewPer = true;
                  }
                }
              } else {
                hasjobRoleViewPer = false;
              }
              if (value.employeePermissions!.profilePermissions!.isNotEmpty) {
                for (var element in value.employeePermissions!.profilePermissions!) {
                  if (element.toLowerCase() == view.toLowerCase()) {
                    hasProfileViewPer = true;
                  }
                }
              } else {
                hasProfileViewPer = false;
              }

              if (hasScrumViewPer) {
                navScrumList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? "Team " + navMenuScrum : "",
                    icon: "assets/images/employees_rounded.png",
                    route: RouteNames.scrumTeamRoute,
                    tooltip_title: "Team " + navMenuScrum));
                navScrumList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuEmployees + " " + navMenuScrum : "",
                    icon: "assets/images/roles_rounded.png",
                    route: RouteNames.scrumEmployeeRoute,
                    tooltip_title: navMenuEmployees + " " + navMenuScrum));
                navScrumList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? "Add " + navMenuScrum : "",
                    icon: "assets/images/employees_rounded.png",
                    route: RouteNames.scrumAddRoute,
                    tooltip_title: "Add " + navMenuScrum));
              }

              if (hasEmployeesViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuEmployees : "",
                    icon: "assets/images/employees_rounded.png",
                    route: RouteNames.employeesHomeRoute,
                    tooltip_title: navMenuEmployees));
              }
              if (hasTechnologyViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuTechnologies : "",
                    icon: "assets/images/technology_rounded.png",
                    route: RouteNames.technologyHomeRoute,
                    tooltip_title: navMenuTechnologies));
              }
              if (hasBoardingViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuBoarding : "",
                    icon: "assets/images/project_rounded.png",
                    route: RouteNames.boardingHomeRoute,
                    tooltip_title: navMenuBoarding));
              }
              if (hasProjectViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuProjects : "",
                    icon: "assets/images/project_rounded.png",
                    route: RouteNames.projectHomeRoute,
                    tooltip_title: navMenuProjects));
              }
              if (hasDesignationViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuDesignations : "",
                    icon: "assets/images/designations_rounded.png",
                    route: RouteNames.designationHomeRoute,
                    tooltip_title: navMenuDesignations));
              }
              if (hasDesignationViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuStages : "",
                    icon: "assets/images/developmentStages.png",
                    route: RouteNames.stagesRoute,
                    tooltip_title: navMenuStages));
              }
              if (hasRolesViewPer) {
                navProjList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuRoles : "",
                    icon: "assets/images/roles_rounded.png",
                    route: RouteNames.roleHomeRoute,
                    tooltip_title: navMenuRoles));
              }
              if (hasProfileViewPer) {
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuDashboard : "",
                    icon: "assets/images/dashboard_rounded.png",
                    route: RouteNames.profilesDashboardHomeRoute,
                    tooltip_title: navMenuDashboard));
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuProfiles : "",
                    icon: "assets/images/profiles_rounded.png",
                    route: RouteNames.profileHomeRoute,
                    tooltip_title: navMenuProfiles));
              }
              if (hasProfileStatusViewPer) {
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuProfileStatus : "",
                    icon: "assets/images/profileStatus_rounded.png",
                    route: RouteNames.profileStatusesHomeRoute,
                    tooltip_title: navMenuProfileStatus));
              }
              if (hasInterviewTypesViewPer) {
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuInterviewTypes : "",
                    icon: "assets/images/interviewTypes_rounded.png",
                    route: RouteNames.interviewTypesHomeRoute,
                    tooltip_title: navMenuInterviewTypes));
              }

              if (hasInterviewStatusViewPer) {
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuInterviewerStatus : "",
                    icon: "assets/images/interviewstatus_rounded.png",
                    route: RouteNames.interviewerStatusHomeRoute,
                    tooltip_title: navMenuInterviewerStatus));
              }
              if (hasjobRoleViewPer) {
                navHRList.add(NavMenuItem(
                    title: menu_expand_collapse == 0 ? navMenuJobRole : "",
                    icon: "assets/images/interviewstatus_rounded.png",
                    route: RouteNames.jobRoleHomeRoute,
                    tooltip_title: navMenuJobRole));
              }
            });
          } else {
            hasSprintViewPer = false;
            hasStagesViewPer = false;
            hasScrumViewPer = false;
            hasEmployeesViewPer = false;
            hasProjectViewPer = false;
            hasDesignationViewPer = false;
            hasTechnologyViewPer = false;
            hasRolesViewPer = false;
            hasBoardingViewPer = false;
            hasInterviewTypesViewPer = false;
            hasProfileStatusViewPer = false;
            hasjobRoleViewPer = false;
            hasInterviewStatusViewPer = false;
            hasProfileViewPer = false;
          }
        } else {
          hasSprintViewPer = false;
          hasScrumViewPer = false;
          hasStagesViewPer = false;
          hasEmployeesViewPer = false;
          hasProjectViewPer = false;
          hasDesignationViewPer = false;
          hasTechnologyViewPer = false;
          hasRolesViewPer = false;
          hasBoardingViewPer = false;
          hasInterviewTypesViewPer = false;
          hasProfileStatusViewPer = false;
          hasjobRoleViewPer = false;
          hasInterviewStatusViewPer = false;
          hasProfileViewPer = false;
        }
      });
      if (navProjList.isNotEmpty) {
        mNavList.add(MainNavMenu(title: menu_expand_collapse == 0 ? navMenuMainProjectMgmt : "PM", subMenus: navProjList));
      }
      if (navScrumList.isNotEmpty) {
        mNavList.add(MainNavMenu(title: menu_expand_collapse == 0 ? navMenuScrum : "SRM", subMenus: navScrumList));
      }
      if (navHRList.isNotEmpty) {
        mNavList.add(MainNavMenu(title: menu_expand_collapse == 0 ? navMenuMainHRMgmt : "HRM", subMenus: navHRList));
      }
    });
  }
}
