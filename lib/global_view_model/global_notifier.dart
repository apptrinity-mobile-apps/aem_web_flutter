import 'package:flutter/cupertino.dart';

class GlobalNotifier with ChangeNotifier {
  Map<int, bool> collapsingStatesList = {};
  bool nav_menu_expand = true;
  String _token = "";

  setExpandableTilesStates() {
    // setting ExpansionTile states default collapsed
    /*
    * 0 -> project
    * 1 -> scrum
    * 2 -> hr
    * */
    collapsingStatesList.addAll({0: true});
    collapsingStatesList.addAll({1: false});
    collapsingStatesList.addAll({2: false});
  }

  setExpandedState(bool expanded, int position) {
    // setting ExpansionTile state based on position
    collapsingStatesList[position] = expanded;
    notifyListeners();
  }

  bool? getExpandedState(int position) {
    // return ExpansionTile state based on position
    return collapsingStatesList[position];
  }

  resetExpandedState() {
    // resetting ExpansionTile states to false
    for (int i = 0; i < collapsingStatesList.length; i++) {
      collapsingStatesList[i] = false;
    }
    notifyListeners();
  }

  setNavMenuStyleStates(bool status) {
    // setting ExpansionTile states default collapsed
    nav_menu_expand = status;
    notifyListeners();
  }

  bool? getNavMenuStyleStates() {
    // return ExpansionTile state based on position
    return nav_menu_expand;
  }

  saveToken(String token) {
    print("token save == $token");
    _token = token;
    notifyListeners();
  }

  String getSavedToken() {
    print("token return == $_token");
    return _token;
  }
}
